/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.ktmhows.clip.pointserver.config.exceptions.CommonException;
import com.ktmhows.clip.pointserver.config.exceptions.NotFoundException;
import com.ktmhows.clip.pointserver.resources.mappers.ACLInfoMapper;
import com.ktmhows.clip.pointserver.resources.mappers.PointHistMapper;
import com.ktmhows.clip.pointserver.resources.mappers.PointInfoMapper;
import com.ktmhows.clip.pointserver.resources.mappers.UserInfoMapper;
import com.ktmhows.clip.pointserver.resources.repositories.CLiPPointMapperRepository;
import com.ktmhows.clip.pointserver.utils.ConvertUtil;
import com.ktmhows.clip.pointserver.utils.DateUtil;
import com.ktmhows.clip.pointserver.utils.HttpNetwork;
import com.ktmhows.clip.pointserver.utils.HttpNetworkHTTPS;
import com.ktmhows.clip.pointserver.vo.ACLInfoVo;
import com.ktmhows.clip.pointserver.vo.PointHistVo;
import com.ktmhows.clip.pointserver.vo.PointInfoVo;
import com.ktmhows.clip.pointserver.vo.UserInfoVo;

import com.ktmhows.clip.pointserver.utils.AES256CipherClip;


@Service
public class CLiPPointService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserInfoMapper userInfoMapper;
	
	@Autowired
	private PointInfoMapper pointInfoMapper;

	@Autowired
	private PointHistMapper pointHistMapper;

	@Autowired
	private ACLInfoMapper aclInfoMapper;
	
	@Autowired
	private HttpNetworkHTTPS httpNetworkHTTPS;
	
	@Autowired
	private HttpNetwork httpNetwork;
	
	
	@Autowired
	private CLiPPointMapperRepository clipPointMapperRepository;
	
	@Autowired
	private Environment env;
	
	/** welcome point  적립 사용 유무  */
	private String welcomeYn;
	
	/** welcome point  적립 매체 코드  */
	private String pointRequester;
	
	/** welcome point  적립 포인  */
	private String intsertPoint
	
	/** welcome point 적립 내용   */;
	private String description;
	
	/**
	 *  초기화 메서드
	 */
	@PostConstruct
	public void init(){
		pointRequester =  Objects.toString(this.env.getProperty("event.welcome.requester"),"");
		intsertPoint =  Objects.toString(this.env.getProperty("event.welcome.point"),"0");
		description =  Objects.toString(this.env.getProperty("event.welcome.description"),"");
		welcomeYn =   Objects.toString(this.env.getProperty("event.welcome.useYn"), "N") ;
		
		logger.info("welcomePoint welcomeYn : {}", welcomeYn );
		logger.info("welcomePoint pointRequester : {}", pointRequester );
		logger.info("welcomePoint intsertPoint : {}", intsertPoint );
		logger.info("welcomePoint description : {}", description );
		
	}
	
	
	public UserInfoVo getUserInfo(String userCi) throws java.lang.Exception {
		Map<String, String> param = new HashMap<String, String>();
		param.put("user_ci", userCi);
		
		return this.userInfoMapper.getUserInfo(param);
	}
	
	public List<PointInfoVo> getPointInfoList(Map<String, String> parameters) throws java.lang.Exception {
		return this.pointInfoMapper.getPointInfoList(parameters);
	}
	
	public List<PointInfoVo> getNewPointInfoList(PointInfoVo historyParamVo) throws java.lang.Exception {
		return this.pointInfoMapper.getNewPointInfoList(historyParamVo);
	}
	
	public PointInfoVo getPointInfo(Map<String, String> parameters) throws java.lang.Exception {
		StringBuffer sbLog = new StringBuffer();
		sbLog.append("CLiPPointService : getPointInfo Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

		// 1. 사용자 정보 확인
		sbLog.append("CLiPPointService : UserCheck Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		UserInfoVo uiv = null;
		try{
			Map<String, String> param = new HashMap<String, String>();
			param.put("user_ci", parameters.get("user_ci"));
			
			uiv = this.userInfoMapper.getUserInfo(param);
				
			// 사용자 정보가 없는 경우 익셉션 발생, 사용자 정보가 있고 값이 하나라도 다르다면 else 진입
			if(uiv == null){
				// throw new NotFoundException("404", "사용자 정보 없음!");
				if( "Y".equals(welcomeYn) &&  !"0".equals(intsertPoint) ) {
					
					sbLog.append("welcomepoint : start" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
					
					UserInfoVo newUser = new UserInfoVo();
					newUser.setUserCi(parameters.get("user_ci"));
					newUser.setCustId(parameters.get("cust_id"));
					newUser.setCtn(parameters.get("ctn"));
					newUser.setGaId(parameters.get("ga_id"));
					
					if(this.userInfoMapper.insertUserInfo(newUser) > 0){
						newUser = this.userInfoMapper.getUserInfo(parameters);
						newUser.setUpdate_gbn("I");
						this.userInfoMapper.insertUserInfoLog(newUser);
					}
					
					try {		
						
						String transaction_id = String.valueOf(System.currentTimeMillis());
						
						parameters.put("reg_source", pointRequester);
						parameters.put("point_value", intsertPoint);
						parameters.put("description", description);
						parameters.put("transaction_id",transaction_id);
						
												
						ACLInfoVo aiv = this.aclInfoMapper.getACLInfo(parameters.get("reg_source"));
						PointHistVo phv =null;
						if( aiv != null ){
							parameters.put("acl_limit_yn", aiv.acl_limit_yn);
							parameters.put("acl_limit_cnt", aiv.acl_limit_cnt);
							phv = this.plusPoint(parameters);

						}
						
						if( phv == null ) {
							logger.error("welcome point insert fail, result null , user_ci : {} , ctn :{} , cust_id :{} , ga_id :{}"
									,parameters.get("user_ci"),parameters.get("ctn") ,parameters.get("cust_id"),parameters.get("ga_id") );
						}
						sbLog.append("welcomepoint : info : user_ci : "+  parameters.get("user_ci") + " "+ DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
						
						//사용자 정보 재 조회  ( 기존 로직 처리를 위해)
						uiv = this.userInfoMapper.getUserInfo(param);
						
						sbLog.append("welcomepoint : end" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
						
					} catch (Exception e) {
						logger.error("welcome point insert error , user_ci : {} , ctn :{} , cust_id :{} , ga_id :{}"
								,parameters.get("user_ci"),parameters.get("ctn") ,parameters.get("cust_id"),parameters.get("ga_id") ,e);
					}
					
				} else {
					throw new NotFoundException("404", "사용자 정보 없음!");
				}
			} 
			
			if((!StringUtils.isEmpty(parameters.get("cust_id")) && !StringUtils.equals(uiv.getCustId(), parameters.get("cust_id"))) ||
					(!StringUtils.isEmpty(parameters.get("ctn")) && !StringUtils.equals(uiv.getCtn(), parameters.get("ctn"))) ||
					(!StringUtils.isEmpty(parameters.get("ga_id")) && !StringUtils.equals(uiv.getGaId(), parameters.get("ga_id")))) {

				uiv.setCustId(parameters.get("cust_id"));
				uiv.setCtn(parameters.get("ctn"));
				uiv.setGaId(parameters.get("ga_id"));

				if(this.userInfoMapper.updateUserInfo(uiv) > 0){
					uiv = this.userInfoMapper.getUserInfo(parameters);
					uiv.setUpdate_gbn("U");
					this.userInfoMapper.insertUserInfoLog(uiv);
				}
			}
			
			
			this.logger.debug("USER_IDX = [" + uiv.getUserIdx() + "]/USER_CI = [" + uiv.getUserCi() +"]");
			
		} catch(Exception ex){
			ex.printStackTrace();
			this.logger.error("사용자 정보 처리 중 오류 발생!" + ex.getLocalizedMessage());
			throw ex;
		}
		sbLog.append("CLiPPointService : UserCheck End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

		
		// 2. 미적립 포인트 조회 및 적립처리

//		try{
//			int succCnt = 0;
//			List<PointHistVo> phvList = this.pointHistMapper.getPointHistListForChange(uiv);
//			
//			if(phvList != null && phvList.size() > 0){
//				succCnt = phvList.size();
//				for(int i=0; i<phvList.size(); i++){
//					try{
//						this.clipPointMapperRepository.changePoint(phvList.get(i).getPointHistIdx(), uiv);
//					} catch(CommonException ex){
//						succCnt--;
//					}
//				}
//			}
//			
//			this.logger.debug(succCnt + "/" + phvList.size() + " 건 적립 처리 완료!");
//		} catch(Exception ex){
//			ex.printStackTrace();
//			this.logger.error("포인트 적립 처리 중 오류 발생!" + ex.getLocalizedMessage());
//			throw ex;
//		}
		
		// 3. 최종 포인트 조회
		PointInfoVo piv = null;
		try{
			piv = this.pointInfoMapper.getPointInfo(uiv.getUserCi());
		} catch(Exception ex){
			ex.printStackTrace();
			this.logger.error("포인트 조회 처리 중 오류 발생!" + ex.getLocalizedMessage());
			throw ex;
		}
		
		if(piv == null){
			piv = new PointInfoVo();
			piv.setUserCi(uiv.getUserCi());
			piv.setBalance(0);
		}
		sbLog.append("CLiPPointService : getPointInfo End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

		piv.setLog(sbLog);
		
		return piv;
	}
	
	// 포인트 조회 시 가입하지 않는 함수
	public PointInfoVo getPointInfo2(Map<String, String> parameters) throws java.lang.Exception {
		StringBuffer sbLog = new StringBuffer();
		sbLog.append("CLiPPointService : getPointInfo2 Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

		// 1. 사용자 정보 확인
		sbLog.append("CLiPPointService : UserCheck Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		UserInfoVo uiv = null;
		try{
			Map<String, String> param = new HashMap<String, String>();
			param.put("user_ci", parameters.get("user_ci"));
			
			uiv = this.userInfoMapper.getUserInfo(param);
				
			// 사용자 정보가 없는 경우 익셉션 발생, 사용자 정보가 있고 값이 하나라도 다르다면 else 진입
			if(uiv == null){
				throw new NotFoundException("404", "사용자 정보 없음!");
			} 
			
			if((!StringUtils.isEmpty(parameters.get("cust_id")) && !StringUtils.equals(uiv.getCustId(), parameters.get("cust_id"))) ||
					(!StringUtils.isEmpty(parameters.get("ctn")) && !StringUtils.equals(uiv.getCtn(), parameters.get("ctn"))) ||
					(!StringUtils.isEmpty(parameters.get("ga_id")) && !StringUtils.equals(uiv.getGaId(), parameters.get("ga_id")))) {

				uiv.setCustId(parameters.get("cust_id"));
				uiv.setCtn(parameters.get("ctn"));
				uiv.setGaId(parameters.get("ga_id"));

				if(this.userInfoMapper.updateUserInfo(uiv) > 0){
					uiv = this.userInfoMapper.getUserInfo(parameters);
					uiv.setUpdate_gbn("U");
					this.userInfoMapper.insertUserInfoLog(uiv);
				}
			}
			
			
			this.logger.debug("USER_IDX = [" + uiv.getUserIdx() + "]/USER_CI = [" + uiv.getUserCi() +"]");
			
		} catch(Exception ex){
			ex.printStackTrace();
			this.logger.error("사용자 정보 처리 중 오류 발생!" + ex.getLocalizedMessage());
			throw ex;
		}
		sbLog.append("CLiPPointService : UserCheck End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

		
		// 2. 미적립 포인트 조회 및 적립처리
		
		// 3. 최종 포인트 조회
		PointInfoVo piv = null;
		try{
			piv = this.pointInfoMapper.getPointInfo(uiv.getUserCi());
		} catch(Exception ex){
			ex.printStackTrace();
			this.logger.error("포인트 조회 처리 중 오류 발생!" + ex.getLocalizedMessage());
			throw ex;
		}
		
		if(piv == null){
			piv = new PointInfoVo();
			piv.setUserCi(uiv.getUserCi());
			piv.setBalance(0);
		}
		sbLog.append("CLiPPointService : getPointInfo2 End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

		piv.setLog(sbLog);
		
		return piv;
	}

	public PointInfoVo getPointRefresh(Map<String, String> parameters) throws java.lang.Exception {
		StringBuffer sbLog = new StringBuffer();
		sbLog.append("CLiPPointService : getPointRefresh Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

		int joinLimitFailCnt = 0;
		
		// 1. 사용자 정보 확인
		sbLog.append("CLiPPointService : UserCheck Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		UserInfoVo uiv = null;
		try{
			Map<String, String> param = new HashMap<String, String>();
			param.put("user_ci", parameters.get("user_ci"));
			
			uiv = this.userInfoMapper.getUserInfo(param);
				
			// 사용자 정보가 없는 경우, 사용자 정보 등록 처리
			if(uiv == null){
				uiv = new UserInfoVo();
				uiv.setUserCi(parameters.get("user_ci"));
				uiv.setCustId(parameters.get("cust_id"));
				uiv.setCtn(parameters.get("ctn"));
				uiv.setGaId(parameters.get("ga_id"));
				
				if(this.userInfoMapper.insertUserInfo(uiv) > 0){
					uiv = this.userInfoMapper.getUserInfo(parameters);
					uiv.setUpdate_gbn("I");
					this.userInfoMapper.insertUserInfoLog(uiv);
				}
				
				//2017.07  최초 사용자 포인트 적립 , plusPoint param 생성				
//				if ( "Y".equals(welcomeYn) && !"0".equals(intsertPoint) && !"".equals(pointRequester) ) {				
//					
//					try {		
//						
//						String transaction_id = String.valueOf(System.currentTimeMillis());
//						
//						parameters.put("reg_source", pointRequester);
//						parameters.put("point_value", intsertPoint);
//						parameters.put("description", description);
//						parameters.put("transaction_id",transaction_id);
//						
//												
//						ACLInfoVo aiv = this.aclInfoMapper.getACLInfo(parameters.get("reg_source"));
//						parameters.put("acl_limit_yn", aiv.acl_limit_yn);
//						parameters.put("acl_limit_cnt", aiv.acl_limit_cnt);
//						
//						PointHistVo phv = this.plusPoint(parameters);
//						
//						if( phv == null ) {
//							logger.error("welcome point insert fail, result null , user_ci : {} , ctn :{} , cust_id :{} , ga_id :{}"
//									,parameters.get("user_ci"),parameters.get("ctn") ,parameters.get("cust_id"),parameters.get("ga_id") );
//						}
//						
//					} catch (Exception e) {
//						logger.error("welcome point insert error , user_ci : {} , ctn :{} , cust_id :{} , ga_id :{}"
//								,parameters.get("user_ci"),parameters.get("ctn") ,parameters.get("cust_id"),parameters.get("ga_id") ,e);
//					}	
//					
//				}
				
				
				
			} else {
				if((!StringUtils.isEmpty(parameters.get("cust_id")) && !StringUtils.equals(uiv.getCustId(), parameters.get("cust_id"))) ||
						(!StringUtils.isEmpty(parameters.get("ctn")) && !StringUtils.equals(uiv.getCtn(), parameters.get("ctn"))) ||
						(!StringUtils.isEmpty(parameters.get("ga_id")) && !StringUtils.equals(uiv.getGaId(), parameters.get("ga_id")))) {

					uiv.setCustId(parameters.get("cust_id"));
					uiv.setCtn(parameters.get("ctn"));
					uiv.setGaId(parameters.get("ga_id"));

					if(this.userInfoMapper.updateUserInfo(uiv) > 0){
						uiv = this.userInfoMapper.getUserInfo(parameters);
						uiv.setUpdate_gbn("U");
						this.userInfoMapper.insertUserInfoLog(uiv);
					}
				}
			}
			
			this.logger.debug("USER_IDX = [" + uiv.getUserIdx() + "]/USER_CI = [" + uiv.getUserCi() +"]");
			
		} catch(Exception ex){
			ex.printStackTrace();
			this.logger.error("사용자 정보 처리 중 오류 발생!" + ex.getLocalizedMessage());
			throw ex;
		}
		sbLog.append("CLiPPointService : UserCheck End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

		// 2. 미적립 포인트 조회 및 적립처리
		sbLog.append("CLiPPointService : Point Change Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		try{
			int succCnt = 0;
			int totalCnt = 0;
			
			List<PointHistVo> phvList;
			UserInfoVo uiv2;

			sbLog.append("CLiPPointService : Point Change Start by USER_CI!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			// 사용자 CI 로 적립된 내역 처리
			if(StringUtils.isNotEmpty(uiv.getUserCi())){
				uiv2 = new UserInfoVo();
				uiv2.setUserCi(uiv.getUserCi());
				sbLog.append("select by USER_CI Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
				phvList = this.pointHistMapper.getPointHistListForChange(uiv2);
				sbLog.append("select by USER_CI End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			
				if(phvList != null && phvList.size() > 0){
					succCnt += phvList.size();
					totalCnt += phvList.size();
					for(int i=0; i<phvList.size(); i++){
						try{
							joinLimitFailCnt = joinLimitFailCnt + this.clipPointMapperRepository.changePointRefresh(phvList.get(i).getPointHistIdx(), uiv);
						} catch(CommonException ex){
							succCnt--;
						}
					}
				}
			}
			sbLog.append("CLiPPointService : Point Change End by USER_CI!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

			sbLog.append("CLiPPointService : Point Change Start by CTN!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			// CTN 로 적립된 내역 처리
			if(StringUtils.isNotEmpty(uiv.getCtn())){
				uiv2 = new UserInfoVo();
				uiv2.setCtn(uiv.getCtn());
				sbLog.append("select by CTN Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
				phvList = this.pointHistMapper.getPointHistListForChange(uiv2);
				sbLog.append("select by CTN End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		
				if(phvList != null && phvList.size() > 0){
					succCnt += phvList.size();
					totalCnt += phvList.size();
					for(int i=0; i<phvList.size(); i++){
						try{
							joinLimitFailCnt = joinLimitFailCnt + this.clipPointMapperRepository.changePointRefresh(phvList.get(i).getPointHistIdx(), uiv);
						} catch(CommonException ex){
							succCnt--;
						}
					}
				}
			}
			sbLog.append("CLiPPointService : Point Change End by CTN!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

			sbLog.append("CLiPPointService : Point Change Start by CUST_ID!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			// cust_id 로 적립된 내역 처리
			if(StringUtils.isNotEmpty(uiv.getCustId())){
				uiv2 = new UserInfoVo();
				uiv2.setCustId(uiv.getCustId());
				sbLog.append("select by CUST_ID Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
				phvList = this.pointHistMapper.getPointHistListForChange(uiv2);
				sbLog.append("select by CUST_ID End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
								
				if(phvList != null && phvList.size() > 0){
					succCnt += phvList.size();
					totalCnt += phvList.size();
					for(int i=0; i<phvList.size(); i++){
						try{
							joinLimitFailCnt = joinLimitFailCnt + this.clipPointMapperRepository.changePointRefresh(phvList.get(i).getPointHistIdx(), uiv);
						} catch(CommonException ex){
							succCnt--;
						}
					}
				}
			}
			sbLog.append("CLiPPointService : Point Change End by CUST_ID!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

			sbLog.append("CLiPPointService : Point Change Start by GA_ID!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			// ga_id 로 적립된 내역 처리
			if(StringUtils.isNotEmpty(uiv.getGaId())){
				uiv2 = new UserInfoVo();
				uiv2.setGaId(uiv.getGaId());
				sbLog.append("select by GA_ID Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
				phvList = this.pointHistMapper.getPointHistListForChange(uiv2);
				sbLog.append("select by GA_ID End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		
				if(phvList != null && phvList.size() > 0){
					succCnt += phvList.size();
					totalCnt += phvList.size();
					for(int i=0; i<phvList.size(); i++){
						try{
							joinLimitFailCnt = joinLimitFailCnt + this.clipPointMapperRepository.changePointRefresh(phvList.get(i).getPointHistIdx(), uiv);
						} catch(CommonException ex){
							succCnt--;
						}
					}
				}
			}
			sbLog.append("CLiPPointService : Point Change End by GA_ID!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			
			this.logger.debug(succCnt + "/" + totalCnt + " 건 적립 처리 완료! (완료건중 "+joinLimitFailCnt+"건은 적립횟수 초과로 적립 보류(플래그값 F))");
		} catch(Exception ex){
			ex.printStackTrace();
			this.logger.error("포인트 적립 처리 중 오류 발생!" + ex.getLocalizedMessage());
			throw ex;
		}
		sbLog.append("CLiPPointService : Point Change End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		
		// 3. 최종 포인트 조회
		PointInfoVo piv = null;
		try{
			piv = this.pointInfoMapper.getPointInfo(uiv.getUserCi());
		} catch(Exception ex){
			ex.printStackTrace();
			this.logger.error("포인트 조회 처리 중 오류 발생!" + ex.getLocalizedMessage());
			throw ex;
		}
		
		if(piv == null){
			piv = new PointInfoVo();
			piv.setUserCi(uiv.getUserCi());
			piv.setBalance(0);
		}
		sbLog.append("CLiPPointService : getPointRefresh End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

		piv.setLog(sbLog);
		
		if( joinLimitFailCnt > 0){
			piv.setJoinLimitFailCnt(joinLimitFailCnt);
			piv.setDescription("적립횟수가 초과되어 적립되지 못한 기록이 있습니다.");
		}
		return piv;
	}
	
	@SuppressWarnings("unchecked")
	public PointHistVo plusPoint(Map<String, String> parameters) throws java.lang.Exception {
		
		int joinLimitFailCnt = 0; 
		
		UserInfoVo uiv = null;
		
		try{
			uiv = this.userInfoMapper.getUserInfo(parameters);
			
			
			if(null == uiv && !"".equals(parameters.get("cust_id")) && null != parameters.get("cust_id")){
				String cust_id = parameters.get("cust_id");
				String ctn ="";
				String cert_type ="2";
				Calendar calendar = Calendar.getInstance();
		        SimpleDateFormat format = new SimpleDateFormat();
		        calendar.add(Calendar.MINUTE, +30);
		        format.applyPattern("yyyyMMddHHmmss");
		        ctn = format.format(calendar.getTime());
		        ctn = AES256CipherClip.AES_Encode(ctn);
			
				String user_ci = "";
				String result_msg = "";
				String strHtmlSource ="";
				
				JSONObject jObject = new JSONObject();
				
				jObject.put("cust_id", cust_id);
				jObject.put("service_id", "clippoint");
				jObject.put("cert_type", cert_type);
				jObject.put("cert_key", ctn);
				
				String params = jObject.toJSONString();
				
				this.logger.error("===========================================================" );
				this.logger.error("GetUserCi API CALL param : "+params );	
				if("N".equals(this.env.getProperty("HTTPS_YN"))){
					strHtmlSource = httpNetwork.strGetUserCi(params);
				}else{
					strHtmlSource = httpNetworkHTTPS.strGetUserCi(params);
				}
				this.logger.error("strHtmlSource"+strHtmlSource );
				this.logger.error("===========================================================" );
				
				JSONParser jsonParser = new JSONParser();

		    	JSONObject jsonObject = (JSONObject) jsonParser.parse(strHtmlSource);
		    	
		    	result_msg = jsonObject.get("msg").toString();
		    	
		    	if(result_msg.equals("SUCCESS")){
		    		user_ci = jsonObject.get("ci").toString();
		    		ctn = jsonObject.get("phone").toString();
		    		
		    		// 사용자 정보를 정상적으로 조회한 경우, 사용자 정보 등록 처리
						uiv = new UserInfoVo();
						uiv.setUserCi(user_ci);
						uiv.setCustId(cust_id);
						uiv.setCtn(ctn);
						
						if(this.userInfoMapper.insertUserInfo(uiv) > 0){
							uiv = this.userInfoMapper.getUserInfo(parameters);
						}
		    	}
			}
			
			
		} catch(Exception ex){
			//TODO 제거
			ex.printStackTrace();
			uiv = null;
			
		}
		
		PointHistVo phv = new PointHistVo();
		if(parameters.containsKey("user_ci")){
			phv.setUserCi(parameters.get("user_ci"));
		}
		if(parameters.containsKey("cust_id")){
			phv.setCustId(parameters.get("cust_id"));
		}
		if(parameters.containsKey("ctn")){
			phv.setCtn(parameters.get("ctn"));
		}
		if(parameters.containsKey("ga_id")){
			phv.setGaId(parameters.get("ga_id"));
		}
		
		phv.setTransactionId(parameters.get("transaction_id"));
		
		phv.setPointType("I");
		
		phv.setPointValue(Long.valueOf(parameters.get("point_value")));
		
		phv.setRegSource(parameters.get("reg_source"));

		phv.setDescription(parameters.get("description"));
		
		phv.setAcl_limit_yn(parameters.get("acl_limit_yn"));
		phv.setAcl_limit_cnt(Integer.parseInt(parameters.get("acl_limit_cnt")));
		
		
		if("clippoint".equals(parameters.get("reg_source"))){
			//point_roulette_join, point_hist_tbl을 이용한다.
			// 오늘 적립 요청이 처음으로 들어왔다면  
			int duplicateChk2 = this.pointHistMapper.getDuplicateChk2(parameters);
			
			// 오늘 첫 적립건이라면 이번건을 적립하고 룰렛을 완료 처리한다. AD쪽의 DB에서 직접 완료시킨다. 
			if( 0 == duplicateChk2){
				
				int ret = 0;
				try {
					this.pointHistMapper.insertPointHist(phv);
					ret = 1;
				} catch (Exception e) {
					ret = 0;
				}
				
				if(ret > 0){
					
					phv = this.pointHistMapper.getPointHist(parameters);
					
					if(uiv != null && phv != null){
						try{
							joinLimitFailCnt = joinLimitFailCnt + this.clipPointMapperRepository.changePoint(phv.getPointHistIdx(), uiv);
						} catch(CommonException ex){
							this.logger.error("포인트 입력 처리 중 오류 발생!" + ex.toString());
							// do noting! - 소스코드 품질검사로 인해 로그를 남기는 로직을 추가함
						}	
					}		
					
					if( joinLimitFailCnt > 0){
						phv.setJoinLimitFailCnt(joinLimitFailCnt);
						phv.setDescription("적립횟수가 초과되어 적립되지 못한 기록이 있습니다.");
					}
					
					@SuppressWarnings("unused")
					int resultCnt = this.pointHistMapper.updatePointRouletteJoin(parameters);
					
					return phv;
				} else {
					return null;
				}
				
			}else{
				throw new CommonException("600", "당일 이벤트 중복참여");
			}
			
			
		}else{
			int rst = 0;
			try {
				this.pointHistMapper.insertPointHist(phv);
				rst = 1;
			} catch (Exception e) {
				//TODO 제거				
//				e.printStackTrace();
				this.logger.error("PLUS_POINT_ERROR :: POINT_HIST_TBL INSERT FAIL!");
				rst = 0;
			}
			
			if(rst > 0){
				
				try {
					phv = this.pointHistMapper.getPointHist(parameters);
				}catch(Exception e) {
					this.logger.error("PLUS_POINT_ERROR :: POINT_HIST_TBL SELECT FAIL!");
					rst = 0;
				}
				
				if(rst > 0) {
					if(uiv != null && phv != null){
						try{
							joinLimitFailCnt = joinLimitFailCnt + this.clipPointMapperRepository.changePoint(phv.getPointHistIdx(), uiv);
						} catch(CommonException ex){
							this.logger.error("포인트 입력 처리 중 오류 발생!" + ex.toString());
							// do noting! - 소스코드 품질검사로 인해 로그를 남기는 로직을 추가함
						}
					}		
					
					if( joinLimitFailCnt > 0){
						phv.setJoinLimitFailCnt(joinLimitFailCnt);
						phv.setDescription("적립횟수가 초과되어 적립되지 못한 기록이 있습니다.");
					}
					
					return phv;
				}
			}
			
			if(rst == 0) {
				int checkTRID = 0;
				String transactionId = phv.getTransactionId();
				@SuppressWarnings("rawtypes")
				Map	map = new HashMap<String, Object>();
				map.put("transaction_id", transactionId);
				checkTRID = this.pointHistMapper.getCheckPointHist(map);
				if(checkTRID != 0) {
					this.logger.error("PLUS_POINT_ERROR :: transaction_id 중복! transaction_id : " + transactionId);
					
					phv = this.pointHistMapper.getPointHist(parameters);
					return phv;
				}else {
					this.logger.error("PLUS_POINT_ERROR :: transaction_id 중복아님!");
				}
			}
			return null;
		}
	}
	
	public PointInfoVo minusPoint(Map<String, String> parameters) throws java.lang.Exception {
		UserInfoVo uiv = null;
		try{
			uiv = this.userInfoMapper.getUserInfo(parameters);
		} catch(Exception ex){
			uiv = null;
		}
		
		if(uiv == null){
			throw new CommonException("500", "사용자 정보 없음");
		}

		PointInfoVo piv = this.pointInfoMapper.getPointInfo(parameters.get("user_ci"));
		
		if(piv == null){
			throw new CommonException("500", "잔여 포인트 정보 없음");
		}
		
		if(piv.getBalance() < Long.valueOf(parameters.get("point_value"))){
			throw new CommonException("500", "잔여 포인트 부족");
		}
		
		PointHistVo phv = new PointHistVo();
		if(parameters.containsKey("user_ci")){
			phv.setUserCi(parameters.get("user_ci"));
		}

		phv.setTransactionId(parameters.get("transaction_id"));
		
		phv.setPointType("O");
		
		phv.setPointValue(Long.valueOf(parameters.get("point_value")));
		
		phv.setRegSource(parameters.get("reg_source"));

		phv.setDescription(parameters.get("description"));

		int rst = 0;
		try {
			this.pointHistMapper.insertPointHist(phv);
			rst = 1;
		} catch (Exception e) {
			e.printStackTrace();
			rst = 0;
		}
		
		if(rst > 0){
			phv = this.pointHistMapper.getPointHist(parameters);
			
			if(uiv != null && phv != null){
				try{
					this.clipPointMapperRepository.changePoint(phv.getPointHistIdx(), uiv);
				} catch(CommonException ex){
					throw ex;
				}	
			}		
			
			return this.pointInfoMapper.getPointInfo(uiv.getUserCi());
		} else {
			return null;
		}
	}
	
	public UserInfoVo exitUser(Map<String, String> parameters) throws java.lang.Exception {
		return this.clipPointMapperRepository.exitUser(parameters);
	}
	
	public boolean checkIP(String requesterCode, String remoteIp) throws java.lang.Exception {

		ACLInfoVo aiv = this.aclInfoMapper.getACLInfo(requesterCode);
		
		String[] ipList = aiv.getIpAddr().split("[,]");
		
		for(int i=0; i<ipList.length; i++){
			String[] temp = ipList[i].split("[-]");
			
			if(temp.length == 2){	// 대역 IP
				long tempIpFront = ConvertUtil.convertIP2Long(temp[0].trim());
				long tempIpBack = ConvertUtil.convertIP2Long(temp[1].trim());
				long tempRemoteIp = ConvertUtil.convertIP2Long(remoteIp.trim());
				
				this.logger.debug("tempIpFront = " + tempIpFront);
				this.logger.debug("tempIpBack = " + tempIpBack);
				this.logger.debug("tempRemoteIp = " + tempRemoteIp);
				
				if(tempIpFront <= tempIpBack){
					if(tempIpFront <= tempRemoteIp && tempIpBack >= tempRemoteIp){
						return true;
					}
				} else {
					if(tempIpFront >= tempRemoteIp && tempIpBack <= tempRemoteIp){
						return true;
					}
				}
			} else {				// 단일 IP
				if(StringUtils.equals(temp[0].trim(), remoteIp)){
					return true;
				}
			}
		}
		
		return false;
	}
	
	public int getPlusCount(Map<String, String> parameters) throws java.lang.Exception {
		return this.pointInfoMapper.getPlusCount(parameters);
	}
	
	/**
	 * 현재 Clip 포인트 단순 조회
	 * @param userCi
	 * @return
	 * @throws java.lang.Exception
	 */
	public PointInfoVo getCurrPoint(String userCi) throws java.lang.Exception {
		return this.pointInfoMapper.getPointInfo(userCi);
	}
	
	/**
	 * Clip 포인트 적립 취소
	 * @param parameters
	 * @return
	 * @throws java.lang.Exception
	 */
	public PointHistVo plusPointCancel(Map<String, String> parameters) throws java.lang.Exception {
		UserInfoVo uiv = null;
		try{
			uiv = this.userInfoMapper.getUserInfo(parameters);
		} catch(Exception ex){
			throw new CommonException("500", "사용자 정보 없음");
		}
		
		//이전 거래 정보 존재 확인
		parameters.put("point_type", "I");
		PointHistVo orgPhv = pointHistMapper.selectPointHistForCancel(parameters);
		if(orgPhv == null)
			throw new CommonException("500", "취소 대상 정보 없음");
		else {
			
			if(orgPhv.getTransactionId().equals(parameters.get("transaction_id")))
				throw new CommonException("500", "취소 처리가 완료된 거래");
			
			if(parameters.get("reference_id").equals(orgPhv.getTransactionId())) {
				parameters.put("point_hist_idx", ""+orgPhv.getPointHistIdx());
				int chkVal = pointHistMapper.selectPointCancelInfoChk(parameters);
				
				parameters.remove("point_hist_idx");
				if(chkVal > 0)
					throw new CommonException("500", "취소 처리가 완료된 거래");
			} else
				throw new CommonException("500", "취소 거래 아이디가 일치 하지 않음");
		}

		//금액으로 차감 가능 여부 확인
		PointInfoVo piv = this.pointInfoMapper.getPointInfo(parameters.get("user_ci"));
		if(piv == null){
			throw new CommonException("500", "잔여 포인트 정보 없음");
		}	
		if(piv.getBalance() < Long.valueOf(parameters.get("point_value"))){
			throw new CommonException("500", "잔여 포인트 부족");
		}
		
		//적립 취소로 인한 차감 시작
		PointHistVo phv = new PointHistVo();
		phv.setUserCi(parameters.get("user_ci"));
		phv.setTransactionId(parameters.get("transaction_id"));
		phv.setPointType("O");
		phv.setPointValue(Long.valueOf(parameters.get("point_value")));
		phv.setRegSource(parameters.get("reg_source"));
		phv.setDescription(parameters.get("description"));
		//phv.setAcl_limit_yn(parameters.get("acl_limit_yn"));
		//phv.setAcl_limit_cnt(Integer.parseInt(parameters.get("acl_limit_cnt")));

		int rst = 0;
		try {
			this.pointHistMapper.insertPointHist(phv);
			rst = 1;
		} catch (Exception e) {
			e.printStackTrace();
			rst = 0;
		}
		
		if(rst > 0){
			parameters.put("point_type", "O"); //입출이 바뀜
			phv = this.pointHistMapper.getPointHist(parameters);
			
			if(phv != null){
				try{
					this.clipPointMapperRepository.changePoint(phv.getPointHistIdx(), uiv);
				} catch(CommonException ex){
					this.logger.error("포인트 취소 입력 처리 중 오류 발생!" + ex.toString());
					// do noting! - 소스코드 품질검사로 인해 로그를 남기는 로직을 추가함
				}	
			}
			
			parameters.put("point_hist_idx", ""+phv.getPointHistIdx());
			parameters.put("ref_point_hist_idx", ""+orgPhv.getPointHistIdx());
			pointHistMapper.insertPointCancelInfo(parameters);

			return phv;
		} else {
			return null;
		}
		
	}
	
	public PointInfoVo minusPointCancel(Map<String, String> parameters) throws java.lang.Exception {
		UserInfoVo uiv = null;
		try{
			uiv = this.userInfoMapper.getUserInfo(parameters);
		} catch(Exception ex){
			throw new CommonException("500", "사용자 정보 없음");
		}

		//이전 거래 정보 존재 확인
		parameters.put("point_type", "O");
		PointHistVo orgPhv = pointHistMapper.selectPointHistForCancel(parameters);
		if(orgPhv == null)
			throw new CommonException("500", "취소 대상 정보 없음");
		else {
			
			if(orgPhv.getTransactionId().equals(parameters.get("transaction_id")))
				throw new CommonException("500", "취소 처리가 완료된 거래");
			
			if(parameters.get("reference_id").equals(orgPhv.getTransactionId())) {
				parameters.put("point_hist_idx", ""+orgPhv.getPointHistIdx());
				int chkVal = pointHistMapper.selectPointCancelInfoChk(parameters);
				parameters.remove("point_hist_idx");
				if(chkVal > 0)
					throw new CommonException("500", "취소 처리가 완료된 거래");
			} else
				throw new CommonException("500", "취소 거래 아이디가 일치 하지 않음");
		}

		//차감 취소로 인한 적립 시작
		PointHistVo phv = new PointHistVo();
		phv.setUserCi(parameters.get("user_ci"));
		phv.setTransactionId(parameters.get("transaction_id"));
		phv.setPointType("I");
		phv.setPointValue(Long.valueOf(parameters.get("point_value")));
		phv.setRegSource(parameters.get("reg_source"));
		phv.setDescription(parameters.get("description"));

		int rst = 0;
		try {
			this.pointHistMapper.insertPointHist(phv);
			rst = 1;
		} catch (Exception e) {
			e.printStackTrace();
			rst = 0;
		}
		
		if(rst > 0){
			parameters.put("point_type", "I"); //입출이 바뀜
			phv = this.pointHistMapper.getPointHist(parameters);
			
			if(uiv != null && phv != null){
				try{
					this.clipPointMapperRepository.changePoint(phv.getPointHistIdx(), uiv);
				} catch(CommonException ex){
					throw ex;
				}	
			}		
			
			parameters.put("point_hist_idx", ""+phv.getPointHistIdx());
			parameters.put("ref_point_hist_idx", ""+orgPhv.getPointHistIdx());
			pointHistMapper.insertPointCancelInfo(parameters);

			return this.pointInfoMapper.getPointInfo(uiv.getUserCi());
		} else {
			return null;
		}
	}
	
	public String getUserCiByUserToken(String userToken) throws Exception {
		return this.userInfoMapper.selectUserCiByUserToken(userToken);
	}

	public String getCustIdByUserCi(String userCi) throws Exception {

		Map<String, String> param = new HashMap<String, String>();
		param.put("user_ci", userCi);
		
		UserInfoVo uiv = null;
		try{
			uiv = this.userInfoMapper.getUserInfo(param);
		} catch(Exception ex){
			
			ex.printStackTrace();
			return null;
		}
		
		if(uiv == null || StringUtils.isEmpty(uiv.getCustId()))
			return null;
		
		return uiv.getCustId();
	}
	
	/**
	 * 사용자 포인트 조회
	 * cust_id 로 요청시,클립서버와 연동하여 사용자 정보 조회후, 포인트 조회 
	 *
	 */
	public PointInfoVo getPointForOtherService(Map<String, String> parameters) {
		StringBuffer sbLog = new StringBuffer();

		sbLog.append("CLiPPointService : getPointForOtherService start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		
		PointInfoVo piv = null;
		UserInfoVo uiv = null;
		
		if( parameters == null ) {
				return null;
		}
		
		try{
			uiv = this.userInfoMapper.getUserInfo(parameters);
			
			if( uiv == null &&  parameters.containsKey("cust_id") && !StringUtils.isEmpty(parameters.get("cust_id")) ) {				
				uiv = this.getUserCiByCustId( (String) parameters.get("cust_id"));
			}
			
		} catch(Exception ex){
			ex.printStackTrace();
			this.logger.error("getPointForOtherService 사용자 정보 조회 중 오류 발생!" + ex.getLocalizedMessage());
			return null;
		}
		
		try{
			piv = this.pointInfoMapper.getPointInfo(uiv.getUserCi());
		} catch(Exception ex){
			ex.printStackTrace();
			this.logger.error("getPointForOtherService 포인트 조회 처리 중 오류 발생!" + ex.getLocalizedMessage());
		}
	
		if(piv == null){
			piv = new PointInfoVo();
 			piv.setBalance(-1);
		}
		sbLog.append("CLiPPointService : getPointForOtherService End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

		piv.setLog(sbLog);
		
		return piv;
		
	}
	
	/**
	 * cust_id 로 클립서버와 연동하여 user_ci 조회 
	 *  @param cust_id 사용자 cust_id
	 *  @return UserInfoVo vo 안에 user_ci , ctn
	 */
	@SuppressWarnings("unchecked")
	public UserInfoVo getUserCiByCustId(String cust_id) throws Exception {
		
			UserInfoVo uiv = null;
			String ctn ="";
			String cert_type ="2";
			Calendar calendar = Calendar.getInstance();
	        SimpleDateFormat format = new SimpleDateFormat();
	        calendar.add(Calendar.MINUTE, +30);
	        format.applyPattern("yyyyMMddHHmmss");
	        ctn = format.format(calendar.getTime());
	        ctn = AES256CipherClip.AES_Encode(ctn);
		
			String user_ci = "";
			String result_msg = "";
			String strHtmlSource ="";
			
			JSONObject jObject = new JSONObject();
			JSONObject jsonResultObject =null;
			jObject.put("cust_id", cust_id);
			jObject.put("service_id", "clippoint");
			jObject.put("cert_type", cert_type);
			jObject.put("cert_key", ctn);
			
			String params = jObject.toJSONString();
			try {
				this.logger.info("===========================================================" );
				this.logger.error("GetUserCi API CALL param : "+params );	
				if("N".equals(this.env.getProperty("HTTPS_YN"))){
					strHtmlSource = httpNetwork.strGetUserCi(params);
				}else{
					strHtmlSource = httpNetworkHTTPS.strGetUserCi(params);
				}
				this.logger.error("strHtmlSource"+strHtmlSource );
				this.logger.error("===========================================================" );
				
				JSONParser jsonParser = new JSONParser();
		
		    	 jsonResultObject = (JSONObject) jsonParser.parse(strHtmlSource);
		    	
		    	result_msg = jsonResultObject.get("msg").toString();
			} catch (Exception e) {
				this.logger.error("GetUserCi API CALL error : ",e );
			}
		  	
	    	if(jsonResultObject !=null && result_msg !=null && result_msg.equals("SUCCESS") &&  jsonResultObject.get("ci") !=null &&  jsonResultObject.get("phone") !=null ){
	    		user_ci = jsonResultObject.get("ci").toString();
	    		ctn = jsonResultObject.get("phone").toString();		
	    		// 사용자 정보를 정상적으로 조회한 경우, 사용자 정보 등록 처리
	    			uiv = new UserInfoVo();
					uiv.setUserCi(user_ci);
					uiv.setCustId(cust_id);
					uiv.setCtn(ctn);
					
					this.userInfoMapper.insertUserInfo(uiv);
	    	}
	    	
	    	return uiv;
		}
	
}
