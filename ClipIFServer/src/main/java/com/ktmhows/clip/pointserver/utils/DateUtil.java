/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.SimpleTimeZone;

public class DateUtil {

	/**
	 * 현재날짜 가져오기
	 * 
	 * @param format
	 * @return
	 */
	public static String getDate(String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format,
				java.util.Locale.KOREA);
		return sdf.format(new Date());
	}

	/**
	 * 현재시간 가져오기
	 * 
	 * @param String
	 *            formatter
	 * @return String
	 */
	public static String getTime(String formatter) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(formatter,Locale.KOREA);
		return sdf.format(date.getTime());
	}

	/**
	 * 현재날짜 가져오기
	 * @param format(패턴)
	 * @param value(일)
	 * @return String
	 */
	public static String getDate(String format, int value) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format,
				java.util.Locale.KOREA);
		cal.add(Calendar.DATE, value);
		return sdf.format(cal.getTime());
	}

	/**
	 * 유효기간 종료일자
	 * 
	 * @param limitStartDate
	 * @param limitDate
	 * @return String
	 */
	public static String getLimitEndDate(String limitStartDate, String limitDate) {

		String limitEndDate = null;

		if (limitDate.length() == 8) {
			limitEndDate = limitDate;
		} else {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd",Locale.KOREA);
			String tmpYear = limitStartDate.substring(0, 4);
			String tmpMonth = limitStartDate.substring(4, 6);
			String tmpDay = limitStartDate.substring(6, 8);
			cal.set(Integer.parseInt(tmpYear), Integer.parseInt(tmpMonth) - 1,
					Integer.parseInt(tmpDay));
			cal.add(Calendar.DATE, Integer.parseInt(limitDate));
			limitEndDate = df.format(cal.getTime());
		}

		return limitEndDate;
	}

	/**
	 * 데이트 포맷 변환
	 * String DATE(예:20001231) to yyyy/MM/dd(예:2000/12/31)
	 * </pre>
	 * 
	 * @param String	dateData
	 * @return String
	 */
	public static String convertFormat(String dateData) {
		if (dateData == null){
			return "";
		}
		else
			return convertFormat(dateData, "yyyy/MM/dd");
	}

	/**
	 * <pre>
	 * 데이트 포맷 변환
	 * String DATE to 입력받은 format형식
	 * </pre>
	 * 
	 * @param String
	 *            dateData
	 * @return String
	 */
	public static String convertFormat(String dateData, String format) {
		if (dateData == null){
			return "";
		}
		else
			return convertToString(convertToTimestamp(dateData), format);
	}

	/**
	 * TimeStamp to String Convert
	 * 
	 * @param Timestamp
	 *            dateData
	 * @param String
	 *            pattern
	 * @return String
	 */
	public static String convertToString(Timestamp dateData, String pattern) {
		return convertToString(dateData, pattern, Locale.KOREA);
	}

	/**
	 * TimeStamp to String Convert
	 * 
	 * @param Timestamp
	 *            dateData
	 * @param String
	 *            pattern
	 * @param Locale
	 *            locale
	 * @return String
	 */
	public static String convertToString(Timestamp dateData, String pattern,
			Locale locale) {
		try {
			if (dateData == null) {
				return "";
			} else {
				SimpleDateFormat formatter = new SimpleDateFormat(pattern,
						locale);
				return formatter.format(dateData);
			}
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * <pre>
	 * String to TimeStamp Convert
	 * </pre>
	 * 
	 * @param dateData
	 * @return
	 */
	public static Timestamp convertToTimestamp(String dateData) {
		try {
			if (dateData == null)
				return null;
			if (dateData.trim().equals(""))
				return null;

			int dateObjLength = dateData.length();
			String yearString = "2002";
			String monthString = "01";
			String dayString = "01";

			if (dateObjLength >= 4){
				yearString = dateData.substring(0, 4);
			}
			if (dateObjLength >= 6){
				monthString = dateData.substring(4, 6);
			}
			if (dateObjLength >= 8){
				dayString = dateData.substring(6, 8);
			}

			int year = Integer.parseInt(yearString);
			int month = Integer.parseInt(monthString) - 1;
			int day = Integer.parseInt(dayString);

			Calendar cal = new GregorianCalendar();
			cal.set(year, month, day);
			return new Timestamp(cal.getTime().getTime());
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 현재 시간을 포맺에 맞추어 변경
	 * 
	 * @param format
	 * @return
	 */
	public static String getCurrnetTimeFormattedStr(String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format,Locale.KOREA);

		return dateFormat.format(new Date());
	}

	/**
	 * yyyyMMdd Format으로 입력된 날짜에 addDay 만큼 더한 날짜(yyyyMMdd)를 Return한다.
	 * <pre>
	 * [사용 예제]
	 * addDays("20040225", 1)	===> 20040226
	 * </pre>
	 * 
	 * @param date
	 * @param addDay
	 * @return String
	 * @throws ChainedException
	 */
	public static String addDays(String date, int addDay) {
		return addDays(date, addDay, "yyyyMMdd");
	}

	/**
	 * 
	 * 입력한 날짜를 입력한 Format으로 해석하여 addDay 만큼 더한 날짜를 Return한다.
	 * 
	 * addDays("2004-02-25", 1, "yyyy-MM-dd") ===> 2004-02-26
	 * 
	 * @param date
	 * @param addDay
	 *            더할 일수
	 * @param format
	 * @return String
	 * @throws ChainedException
	 */
	public static String addDays(String date, int addDay, String format) {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				format, java.util.Locale.KOREA);
		java.util.Date formattedDate = ConvertUtil
				.formatValidDate(date, format);
		formattedDate.setTime(formattedDate.getTime()
				+ ((long) addDay * 1000 * 60 * 60 * 24));
		return formatter.format(formattedDate);
	}

	/**
	 * 
	 * yyyyMMdd Format으로 입력된 날짜에 addMonth 만큼 더한 날짜를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * addMonths("20040225", 1)	===> 20040325
	 * 
	 * </pre>
	 * 
	 * @param date
	 * @param addMonth
	 *            더할 월수
	 * @return String
	 * @throws ChainedException
	 */
	public static String addMonths(String date, int addMonth) {
		return addMonths(date, addMonth, "yyyyMMdd");
	}

	/**
	 * 
	 * 입력한 날짜를 입력한 Format으로 해석하여 addMonth 만큼 더한 날짜를 Return한다.
	 * 
	 * addMonths("2004-02-25", 1, "yyyy-MM-dd") ===> 2004-03-25
	 * 
	 * @param date
	 * @param addMonth
	 *            더할 월수
	 * @param format
	 * @return String
	 * @throws ChainedException
	 */
	public static String addMonths(String date, int addMonth, String format) {

		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				format, java.util.Locale.KOREA);
		java.util.Date formattedDate = ConvertUtil
				.formatValidDate(date, format);

		java.text.SimpleDateFormat yearFormat = new java.text.SimpleDateFormat(
				"yyyy", java.util.Locale.KOREA);
		java.text.SimpleDateFormat monthFormat = new java.text.SimpleDateFormat(
				"MM", java.util.Locale.KOREA);
		java.text.SimpleDateFormat dayFormat = new java.text.SimpleDateFormat(
				"dd", java.util.Locale.KOREA);

		int year = Integer.parseInt(yearFormat.format(formattedDate));
		int month = Integer.parseInt(monthFormat.format(formattedDate));
		int day = Integer.parseInt(dayFormat.format(formattedDate));

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day);

		cal.add(Calendar.MONTH, addMonth);

		year = cal.get(Calendar.YEAR);
		month = cal.get(Calendar.MONTH) + 1;
		day = cal.get(Calendar.DAY_OF_MONTH);

		java.text.DecimalFormat fourDf = new java.text.DecimalFormat("0000");
		java.text.DecimalFormat twoDf = new java.text.DecimalFormat("00");
		String tempDate = fourDf.format(year)
				+ twoDf.format(month)
				+ twoDf.format(day);
		java.util.Date targetDate = null;

		targetDate = ConvertUtil.formatValidDate(tempDate, "yyyyMMdd");

		return formatter.format(targetDate);

	}

	/**
	 * 
	 * yyyyMMdd Format으로 입력된 날짜에 addYear 만큼 더한 날짜를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * addYears("20040225", 1)	===> 20050225
	 * 
	 * </pre>
	 * 
	 * @param date
	 * @param addYear
	 *            더할 년수
	 * @return String
	 * @throws ChainedException
	 */
	public static String addYears(String date, int addYear) {
		return addYears(date, addYear, "yyyyMMdd");
	}

	/**
	 * 
	 * 입력한 날짜를 입력한 Format으로 해석하여 addYear 만큼 더한 날짜를 Return한다.
	 * 
	 * addYears("2004-02-25", 1, "yyyy-MM-dd") ===> 2005-02-25
	 * 
	 * @param date
	 * @param addYear
	 *            더할 년수
	 * @param format
	 * @return String
	 * @throws ChainedException
	 */
	public static String addYears(String date, int addYear, String format) {

		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				format, java.util.Locale.KOREA);
		java.util.Date formattedDate = ConvertUtil
				.formatValidDate(date, format);

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(formattedDate);
		calendar.add(Calendar.YEAR, addYear);
		formattedDate = calendar.getTime();

		return formatter.format(formattedDate);

	}

	/**
	 * 
	 * 현재 시각을 HHmmss Format 의 String 으로 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * CDateUtil.getCurrentTimeString()  ===> 131234
	 * 
	 * </pre>
	 * 
	 * @return java.lang.String
	 */
	public static String getCurrentTimeString() {
		return getCurrentDateString("HHmmss");
	}

	/**
	 * 
	 * 현재 날짜와 시각을 yyyy-MM-dd hh:mm:ss.fffffffff Format의 Timestamp 로 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * CDateUtil.getCurrentTimeStamp()	===> 2004-02-24 13:22:11.734
	 * 
	 * </pre>
	 * 
	 * @return Timestamp
	 */
	public static Timestamp getCurrentTimeStamp() {
		return new Timestamp(new GregorianCalendar().getTime().getTime());
	}

	/**
	 * 
	 * 현재 날짜를 yyyyMMdd Format 의 String 으로 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * CDateUtil.getCurrentDateString()  ===> 20040224
	 * 
	 * </pre>
	 * 
	 * @return java.lang.String
	 */
	public static String getCurrentDateString() {
		return getCurrentDateString("yyyyMMdd");
	}

	/**
	 * 
	 * 현재 날짜를 주어진 Format 의 String 으로 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * CDateUtil.getCurrentDateString("yyyy/MM/dd") ===> 2004/02/24
	 * CDateUtil.getCurrentDateString("HH:mm:ss"));	===> 13:40:05
	 * CDateUtil.getCurrentDateString("hh:mm:ss"));	===> 01:40:05
	 * 
	 * format :  h        hour in am/pm (1~12)   
	 * format :  H        hour in day (0~23)
	 * 
	 * </pre>
	 * 
	 * @param format
	 * @return java.lang.String
	 */
	public static String getCurrentDateString(String format) {
		return ConvertUtil.formatTimestamp(getCurrentTimeStamp(), format,
				java.util.Locale.KOREA);
	}

	/**
	 * 
	 * yyyyMMdd Format으로 입력된 날짜의 요일을 조회하여 java.util.Calendar의 요일을 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * String date = "20040524";
	 * int dayOfWeek = whichDay(date);
	 * if (dayOfWeek == java.util.Calendar.MONDAY)   LLog.debug.println(" 월요일: " + dayOfWeek);
	 * if (dayOfWeek == java.util.Calendar.TUESDAY)  LLog.debug.println(" 화요일: " + dayOfWeek);
	 *  ===> 월요일: 2
	 * 
	 * </pre>
	 * 
	 * @param date
	 * @return int 입력한 날짜의 요일 1: 일요일 (java.util.Calendar.SUNDAY ) 2: 월요일
	 *         (java.util.Calendar.MONDAY ) 3: 화요일 (java.util.Calendar.TUESDAY)
	 *         4: 수요일 (java.util.Calendar.WENDESDAY) 5: 목요일
	 *         (java.util.Calendar.THURSDAY) 6: 금요일 (java.util.Calendar.FRIDAY)
	 *         7: 토요일 (java.util.Calendar.SATURDAY)
	 * @throws ChainedException
	 */
	public static int getWhichDay(String date) {
		return getWhichDay(date, "yyyyMMdd");
	}

	public static String getWhichDayNm(String date) {
		int dayOfWeek = getWhichDay(date, "yyyyMMdd");
		if (dayOfWeek == java.util.Calendar.MONDAY) {
			return "월";
		} else if (dayOfWeek == java.util.Calendar.TUESDAY) {
			return "화";
		} else if (dayOfWeek == java.util.Calendar.WEDNESDAY) {
			return "수";
		} else if (dayOfWeek == java.util.Calendar.THURSDAY) {
			return "목";
		} else if (dayOfWeek == java.util.Calendar.FRIDAY) {
			return "금";
		} else if (dayOfWeek == java.util.Calendar.SATURDAY) {
			return "토";
		} else if (dayOfWeek == java.util.Calendar.SUNDAY) {
			return "일";
		} else {
			return "";
		}
	}

	/**
	 * 
	 * 입력된 날짜와 입력된 Format을 에 따른 요일을 조회하여 java.util.Calendar의 요일을 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * String date = "2004-05-24";
	 * int dayOfWeek = whichDay(date,"yyyy-MM-dd");
	 * if (dayOfWeek == java.util.Calendar.MONDAY)  LLog.debug.println(" 월요일: " + dayOfWeek);
	 * if (dayOfWeek == java.util.Calendar.TUESDAY)  LLog.debug.println(" 화요일: " + dayOfWeek); 
	 *  ===> 월요일: 2
	 * 
	 * </pre>
	 * 
	 * @param date
	 * @param format
	 * @return int 입력한 날짜의 요일 1: 일요일 (java.util.Calendar.SUNDAY ) 2: 월요일
	 *         (java.util.Calendar.MONDAY ) 3: 화요일 (java.util.Calendar.TUESDAY)
	 *         4: 수요일 (java.util.Calendar.WENDESDAY) 5: 목요일
	 *         (java.util.Calendar.THURSDAY) 6: 금요일 (java.util.Calendar.FRIDAY)
	 *         7: 토요일 (java.util.Calendar.SATURDAY)
	 * @throws ChainedException
	 */
	public static int getWhichDay(String date, String format) {

		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				format, java.util.Locale.KOREA);
		java.util.Date formatteddate = ConvertUtil
				.formatValidDate(date, format);
		java.util.Calendar calendar = formatter.getCalendar();
		calendar.setTime(formatteddate);
		return calendar.get(java.util.Calendar.DAY_OF_WEEK);

	}

	/**
	 * 
	 * yyyyMMdd Format으로 입력된 날짜가 유효한 날짜인지 확인한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * CDateUtil.isValidDate("20050225")	===> true
	 * 
	 * </pre>
	 * 
	 * @param date
	 * @return boolean
	 * @throws ChainedException
	 */
	public static boolean isValidDate(String date) {
		return DateUtil.isValidDate(date, "yyyyMMdd");
	}

	/**
	 * 
	 * 입력된 날짜와 입력된 Format 으로 해석하여 입력된 날짜가 유효한 날짜인지 확인한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * CDateUtil.isValidDate("2004-02-99","yyyy-MM-dd")	===> false
	 * 
	 * </pre>
	 * 
	 * @param date
	 * @return boolean
	 * @throws ChainedException
	 */
	public static boolean isValidDate(String date, String format) {

		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				format, java.util.Locale.KOREA);
		java.util.Date formattedDate = null;

		try {

			formattedDate = formatter.parse(date);

		} catch (java.text.ParseException e) {
			return false;
		}

		if (!formatter.format(formattedDate).equals(date)){
			return false;
		}
		return true;

	}

	/**
	 * 
	 * yyyyMMdd Format으로 입력된 from 날짜와 to 날짜 사이의 일수를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * daysBetween("20040225", "20040301")	===> 5
	 * 
	 * </pre>
	 * 
	 * @param from
	 * @param to
	 * @return int
	 * @throws ChainedException
	 */
	public static int getDaysBetween(String from, String to) {
		return getDaysBetween(from, to, "yyyyMMdd");
	}

	/**
	 * 
	 * 입력된 from 날짜와 to 날짜를 입력된 Format으로 해석하여 날짜 사이의 일수를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * daysBetween("2004-02-25", "2004-03-01", "yyyy-MM-dd")	===> 5
	 * 
	 * </pre>
	 * 
	 * @param from
	 * @param to
	 * @param format
	 * @return int
	 * @throws ChainedException
	 */
	public static int getDaysBetween(String from, String to, String format) {

		java.util.Date d1 = ConvertUtil.formatValidDate(from, format);
		java.util.Date d2 = ConvertUtil.formatValidDate(to, format);
		long duration = d2.getTime() - d1.getTime();
		return (int) (duration / (1000 * 60 * 60 * 24));

	}

	/**
	 * 
	 * yyyyMMdd Format으로 입력된 from 날짜와 to 날짜 사이의 월수를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * monthsBetween("20040225", "20041001")	===> 8
	 * 
	 * </pre>
	 * 
	 * @param from
	 * @param to
	 * @return int
	 * @throws ChainedException
	 */
	public static int getMonthsBetween(String from, String to) {
		return getMonthsBetween(from, to, "yyyyMMdd");
	}

	/**
	 * 
	 * 입력된 from 날짜와 to 날짜를 입력된 Format으로 해석하여 날짜 사이의 월수를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * monthsBetween("2004-02-25", "2004-10-01", "yyyy-MM-dd")	===> 8
	 * 
	 * </pre>
	 * 
	 * @param from
	 * @param to
	 * @param format
	 * @return int
	 * @throws ChainedException
	 */
	public static int getMonthsBetween(String from, String to, String format) {
		// java.text.SimpleDateFormat formatter =
		// new java.text.SimpleDateFormat (format, java.util.Locale.KOREA);

		java.util.Date fromDate = ConvertUtil.formatValidDate(from, format);
		java.util.Date toDate = ConvertUtil.formatValidDate(to, format);
		// if two date are same, return 0.
		if (fromDate.compareTo(toDate) == 0){
			return 0;
		}
		java.text.SimpleDateFormat yearFormat = new java.text.SimpleDateFormat(
				"yyyy", java.util.Locale.KOREA);
		java.text.SimpleDateFormat monthFormat = new java.text.SimpleDateFormat(
				"MM", java.util.Locale.KOREA);
		java.text.SimpleDateFormat dayFormat = new java.text.SimpleDateFormat(
				"dd", java.util.Locale.KOREA);
		int fromYear = Integer.parseInt(yearFormat.format(fromDate));
		int toYear = Integer.parseInt(yearFormat.format(toDate));
		int fromMonth = Integer.parseInt(monthFormat.format(fromDate));
		int toMonth = Integer.parseInt(monthFormat.format(toDate));
		int fromDay = Integer.parseInt(dayFormat.format(fromDate));
		int toDay = Integer.parseInt(dayFormat.format(toDate));
		int result = 0;
		result += ((toYear - fromYear) * 12);
		result += (toMonth - fromMonth);
		// if (((toDay - fromDay) < 0) ) result += fromDate.compareTo(toDate);
		// ceil과 floor의 효과
		if (((toDay - fromDay) > 0)){
			result += toDate.compareTo(fromDate);
		}
		return result;

	}

	/**
	 * 
	 * yyyyMMdd Format으로 입력된 from 날짜와 to 날짜 사이의 년수를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * yearsBetween("20040225", "20071001")	===> 3
	 * 
	 * </pre>
	 * 
	 * @param from
	 * @param to
	 * @return int
	 * @throws ChainedException
	 */
	public static int getYearsBetween(String from, String to) {
		return getYearsBetween(from, to, "yyyyMMdd");
	}

	/**
	 * 
	 * 입력된 from 날짜와 to 날짜를 입력된 Format으로 해석하여 날짜 사이의 년수를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * yearsBetween("2004-02-25", "2007-10-01", "yyyy-MM-dd")	===> 3
	 * 
	 * </pre>
	 * 
	 * @param from
	 * @param to
	 * @param format
	 * @return int
	 * @throws ChainedException
	 */
	public static int getYearsBetween(String from, String to, String format) {
		return (int) (getDaysBetween(from, to, format) / 365);
	}

	/**
	 * 
	 * yyyyMMdd Format으로 입력된 날짜로부터 입력한 년수 만큼 이전 날짜를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * subYears("20040225", 2)	===> 20020225
	 * 
	 * </pre>
	 * 
	 * @param date
	 * @param year
	 * @return String
	 * @throws ChainedException
	 */
	public static String subtractYears(String date, int year) {
		return subtractYears(date, year, "yyyyMMdd");
	}

	/**
	 * 
	 * 입력된 날짜를 입력된 Format 으로 해석하여 입력한 년수 만큼 이전 날짜를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * subYears("2004-02-25", 2, "yyyy-MM-dd")	===> 2002-02-25
	 * 
	 * </pre>
	 * 
	 * @param date
	 * @param year
	 * @param format
	 * @return String
	 * @throws ChainedException
	 */
	public static String subtractYears(String date, int year, String format) {

		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				format, java.util.Locale.KOREA);
		java.util.Date formattedDate = ConvertUtil
				.formatValidDate(date, format);

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(formattedDate);

		calendar.add(Calendar.YEAR, year * (-1));

		formattedDate = calendar.getTime();
		return formatter.format(formattedDate);

	}

	/**
	 * 
	 * yyyyMMdd Format으로 입력된 날짜에서 해당 년월의 마지막 날짜를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * lastDayOfMonth("20040202")	===> 20040229
	 * 
	 * </pre>
	 * 
	 * @param date
	 * @return String
	 * @throws ChainedException
	 */
	public static String getLastDayOfMonth(String date) {
		return getLastDayOfMonth(date, "yyyyMMdd");
	}

	/**
	 * 
	 * 입력된 날짜을 입력된 Format 으로 해석하여 그 날짜에서 해당 년월의 마지막 날짜를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * lastDayOfMonth("20040202", "yyyyMMdd")	===> 20040429
	 * 
	 * </pre>
	 * 
	 * @param date
	 * @param format
	 * @return String
	 * @throws ChainedException
	 */
	public static String getLastDayOfMonth(String date, String format) {

		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				format, java.util.Locale.KOREA);
		java.util.Date formattedDate = ConvertUtil
				.formatValidDate(date, format);
		java.text.SimpleDateFormat yearFormat = new java.text.SimpleDateFormat(
				"yyyy", java.util.Locale.KOREA);
		java.text.SimpleDateFormat monthFormat = new java.text.SimpleDateFormat(
				"MM", java.util.Locale.KOREA);
		int year = Integer.parseInt(yearFormat.format(formattedDate));
		int month = Integer.parseInt(monthFormat.format(formattedDate));
		int day = getLastDay(year, month);
		java.text.DecimalFormat fourDf = new java.text.DecimalFormat("0000");
		java.text.DecimalFormat twoDf = new java.text.DecimalFormat("00");
		String tempDate = fourDf.format(year)
				+ twoDf.format(month)
				+ twoDf.format(day);
		java.util.Date targetDate = ConvertUtil.formatValidDate(tempDate,
				"yyyyMMdd");

		return formatter.format(targetDate);

	}

	/**
	 * 
	 * 입력된 년도와 입력된 월의 마지막 일를 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * lastDay(2004,02)	===> 29
	 * 
	 * </pre>
	 * 
	 * @param year
	 * @param month
	 * @return int
	 * @throws ParseException
	 */
	public static int getLastDay(int year, int month) {
		int day = 0;
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = 31;
			break;
		case 2:
			if ((year % 4) == 0) {
				if ((year % 100) == 0 && (year % 400) != 0) {
					day = 28;
				} else {
					day = 29;
				}
			} else {
				day = 28;
			}
			break;
		default:
			day = 30;
		}
		return day;
	}

	/**
	 * 
	 * 입력된 시간을 입력된 Format 으로 해석하여 수행시간을 milliseconds 로 Return한다.
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * getElapsedTime("111111","121111","HHmmss")	===> 3600000
	 * 
	 * </pre>
	 * 
	 * @param start
	 * @param end
	 * @param format
	 *            : default format = HHmmss
	 * @return long : milliseconds
	 * @throws ChainedException
	 */
	public static long getElapsedTime(String start, String end, String tempformat) {
		
		String format = tempformat;

		if (format == null || format.length() == 0) {
			format = "HHmmssSSS";
		}

		Date startTime = ConvertUtil.formatValidDate(start, format);
		Date endTime = ConvertUtil.formatValidDate(end, format);

		return endTime.getTime() - startTime.getTime();

	}

	/**
	 * 
	 * 현재 시간을 Long Type 으로 Return한다. (워크플로우 작업이력저장 용도)
	 * 
	 * <pre>
	 * 
	 * [사용 예제]
	 * 
	 * DateUtil.getCurrentTime()
	 * 
	 * </pre>
	 * 
	 * @return Long
	 * @throws LException
	 */
	public static Long getCurrentTime() {

		java.util.Date date = new java.util.Date();

		long ltime1 = date.getTime(); // 현재시간

		Calendar cal = Calendar.getInstance();
		cal.set(1970, 0, 1, 9, 0, 0);// 파일넷 기준시간

		long ltime2 = cal.getTimeInMillis();

		long curTime = ltime1 - ltime2;

		return Long.valueOf(curTime / 1000);

	}
	
	/**
	 * 시작일, 종료일 사이의 날짜 계산 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static long calculateDays(String startDate, String endDate) {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd",Locale.KOREA);
		long oneDay = 1000*60*60*24; 
		
		try {
			Date sDate = df.parse(startDate);
			Date eDate = df.parse(endDate);

			return ((eDate.getTime() - sDate.getTime())/oneDay)  + 1;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	/**
     * 입력 받은 날로 부터 한달 전 날짜 구하기
     * @param type
     * @return
     */
    public static String getMonthAgoDate(String dt){
    	Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280, "KST"));
    	DateFormat df = new SimpleDateFormat("yyyyMMdd",Locale.KOREA);
    	Date d = df.parse(dt, new ParsePosition(0));
    	
    	cal.setTime(d);
    	cal.add(Calendar.MONTH, -1);
    	
    	return df.format(cal.getTime());
    }
    
	/**
     * 입력 받은 날로 부터 한달 후 날짜 구하기
     * @param type
     * @return
     */
    public static String getMonthAfterDate(String dt){
    	Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280, "KST"));
    	DateFormat df = new SimpleDateFormat("yyyyMMdd",Locale.KOREA);
    	Date d = df.parse(dt, new ParsePosition(0));
    	
    	cal.setTime(d);
    	cal.add(Calendar.MONTH, 1);
    	
    	return df.format(cal.getTime());
    }

	public static String convertToString(long currentTimeMillis, String pattern) {
		// TODO Auto-generated method stub
		return null;
	}

}
