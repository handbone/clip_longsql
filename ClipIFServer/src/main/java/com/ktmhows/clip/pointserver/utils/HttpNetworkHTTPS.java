/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package com.ktmhows.clip.pointserver.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
//import org.apache.log4j.Logger;
//import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

import com.ktmhows.clip.pointserver.vo.PointInfoVo;
import com.ktmhows.clip.pointserver.vo.UserInfoVo;


@Component
@PropertySource("classpath:config.properties")
public class HttpNetworkHTTPS {

	@Autowired
	private Environment env;
	
	private String API_PUSH_SEND_DOMAIN_HTTPS;
	
	private String API_PUSH_SEND_PLUS_MESSAGE;
	
	private String API_PUSH_SEND_MINUS_MESSAGE;
	
	private String API_PUSH_SEND_TITLE;
	
	private long API_PUSH_LIMIT_POINT;
	
	private String API_PUSH_RANDING_URL;
	
	private String API_PUSH_SEND_EXCLUDE;
	
	private String API_PUSH_SEND_COMMON_MESSAGE;
	
	private String API_PUSH_IMG_URL;
	
	@SuppressWarnings("unused")
	@Autowired
	private ClipRequesterCode clipRequesterCode;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PostConstruct
	public void init(){
		API_PUSH_SEND_DOMAIN_HTTPS = Objects.toString(this.env.getProperty("API_PUSH_SEND_DOMAIN_HTTPS"),"");
		API_PUSH_SEND_PLUS_MESSAGE = Objects.toString(this.env.getProperty("API_PUSH_SEND_PLUS_MESSAGE"),"");
		API_PUSH_SEND_MINUS_MESSAGE = Objects.toString(this.env.getProperty("API_PUSH_SEND_MINUS_MESSAGE"),"");		
		API_PUSH_SEND_TITLE = Objects.toString(this.env.getProperty("API_PUSH_SEND_TITLE"),"");
		API_PUSH_RANDING_URL = this.env.getProperty("API_PUSH_RANDING_URL");
		API_PUSH_SEND_EXCLUDE = this.env.getProperty("API_PUSH_SEND_EXCLUDE");
		API_PUSH_SEND_COMMON_MESSAGE =  Objects.toString(this.env.getProperty("API_PUSH_SEND_COMMON_MESSAGE"),"클립포인트");
		API_PUSH_IMG_URL = Objects.toString(this.env.getProperty("API_PUSH_IMG_URL"), "");
		
		String envLimitPoint = Objects.toString( this.env.getProperty("API_PUSH_LIMIT_POINT"),"0");
		
		if ( envLimitPoint.matches("^[0-9]+$") ) {
			API_PUSH_LIMIT_POINT = Integer.parseInt(envLimitPoint);
		} else {
			API_PUSH_LIMIT_POINT = 0;
		}
		
		if("Y".equals(this.env.getProperty("HTTPS_YN"))) {
			logger.info("API_PUSH_HTTPS");
			logger.info("API_PUSH_SEND_DOMAIN : {}" ,API_PUSH_SEND_DOMAIN_HTTPS);
			logger.info("API_PUSH_SEND_TITLE : {} " , API_PUSH_SEND_TITLE);
			logger.info("API_PUSH_SEND_PLUS_MESSAGE : {} " ,API_PUSH_SEND_PLUS_MESSAGE);
			logger.info("API_PUSH_SEND_MINUS_MESSAGE : {} " ,API_PUSH_SEND_MINUS_MESSAGE);
			logger.info("API_PUSH_LIMIT_POINT: {} " ,API_PUSH_LIMIT_POINT);
			logger.info("API_PUSH_SEND_EXCLUDE: {} " ,API_PUSH_SEND_EXCLUDE);
			
		}
		
		
	}
	
	public String strGetUserCi(String params) throws java.lang.Exception {
		
		BufferedReader oBufReader = null;
		String strBuffer = "";
		String strRslt = "";
		
		
		URL oOpenURL = new URL(this.env.getProperty("CLIP_DOMAIN"));
		
		//HttpURLConnection httpConn = null;
		//httpConn = (HttpURLConnection) oOpenURL.openConnection();
		
		HttpsURLConnection httpConn = (HttpsURLConnection) oOpenURL.openConnection();
		HttpsURLConnection.setDefaultHostnameVerifier( new HostnameVerifier(){
		    public boolean verify(String string,SSLSession ssls) {
		     return true;
		    }
		});
		
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(5000);
		httpConn.setReadTimeout(5000);
		
		httpConn.setDoInput(true);
		httpConn.setRequestProperty("Accept", "application/json");
		httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		
		httpConn.setRequestProperty("Content-Length", String.valueOf(params.length()));
		OutputStream os = httpConn.getOutputStream();
		os.write(params.getBytes());
		os.flush();
		os.close();
		
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),"utf-8"));

		StringBuffer sb = new StringBuffer(strRslt);
		
		int i = 1;
		while ((strBuffer = oBufReader.readLine()) != null && (i < 2)) {
			if (strBuffer.length() > 1) {
				sb.append(strBuffer);
			}
		}

		oBufReader.close();
			
		
		return sb.toString();
	}
	
	/**
	 *@param phv 포인트 적립(차감) 요청 정보 
	 *@param uiv 사용자 정보
	 *@return 결과 메시지 
	 */
	@SuppressWarnings("unchecked")
	public String sendPush(PointInfoVo piv,UserInfoVo uiv) throws java.lang.Exception {
		
		if(API_PUSH_LIMIT_POINT < 1) {
			return "PUSH_SEND_NO_SETTING";
		} else if (piv.getPointValue() <  API_PUSH_LIMIT_POINT) {
			return "PUSH_SEND_NO_LIMIT";
		}
		
		if( (API_PUSH_SEND_EXCLUDE.indexOf(piv.getRegSource()) > -1) ){
			logger.debug("API_PUSH_SEND_EXCLUDE_TYPE :{}",  piv.getRegSource());
			
			return  "API_PUSH_SEND_EXCLUDE_TYPE";
		}else {
			logger.debug("API_PUSH_SEND_INCLUDE_TYPE :{}",  piv.getRegSource());
		}
		
		String pushType ="";
		String pushId ="";
		String expireDate="";
		String result="";
		
		if ( uiv.getCtn() != null && !"".equals(uiv.getCtn())) {
			pushType = "ctn";
			pushId = uiv.getCtn();
		} else if (  uiv.getCustId() != null && !"".equals(uiv.getCustId()) ) {
			pushType = "cst";
			pushId = uiv.getCustId();
		} else {
			return "PUSH_SEND_NO_DATA_CUST_ID_AND_CTN";
		}
		
		//현재 시간 +30 
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, 30);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmss");
	    expireDate = format1.format(cal.getTime());
		
	    String content = API_PUSH_SEND_COMMON_MESSAGE +" "+ String.valueOf(piv.getPointValue());
	    if ( StringUtils.equals(piv.getPointType(), "I") ) {
    			content = content+ API_PUSH_SEND_PLUS_MESSAGE;
	    } else {
	    		content = content+ API_PUSH_SEND_MINUS_MESSAGE;
	    }
	    
	    //2018.01.04 jyi 카테고리 문구 추가
//		String requester_code = piv.getRegSource();
//		content = content + clipRequesterCode.getName(requester_code);
	    
		String type = pushType;
		String id =pushId;
		String title =API_PUSH_SEND_TITLE;
		String agreeCheckYn	 ="N";
		String sendServiceType	 ="01";
		String linkId	 ="clippoint";
		String sendexpireDate =AES256CipherClip.AES_Encode(expireDate);
		String imgUrl =API_PUSH_IMG_URL;
		String randingUrl =API_PUSH_RANDING_URL;
		String signdata = agreeCheckYn + sendServiceType + linkId + type +AES256CipherClip.AES_Decode(id) + imgUrl + randingUrl + expireDate + title +content;
		signdata = signdata +"push" + "external" + "getPushInfo";
		String encSigndata = HmacCrypto.getHmacVal(signdata);
		 
		JSONObject jObject = new JSONObject();
		
		jObject.put("type", type);
		jObject.put("id", id);
		jObject.put("title", title);
		jObject.put("content", content);
		jObject.put("agreeCheckYn", agreeCheckYn);
		jObject.put("sendServiceType", sendServiceType);
		jObject.put("linkId", linkId);
		jObject.put("expireDate", sendexpireDate);
		jObject.put("sign", encSigndata);
		jObject.put("randingUrl", randingUrl);
		jObject.put("imgUrl", imgUrl);
		
		String params = jObject.toJSONString();
		
		
		BufferedReader oBufReader = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = API_PUSH_SEND_DOMAIN_HTTPS;
		
		logger.debug("::::::::: "+strEncodeUrl+"?"+params);
		
		URL oOpenURL = new URL(strEncodeUrl);
		
		//HttpURLConnection httpConn = null;
		//httpConn = (HttpURLConnection) oOpenURL.openConnection();
		
		HttpURLConnection httpConn = (HttpURLConnection) oOpenURL.openConnection();
		HttpsURLConnection.setDefaultHostnameVerifier( new HostnameVerifier(){
		    public boolean verify(String string,SSLSession ssls) {
		     return true;
		    }
		});
		
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(5000);
		httpConn.setReadTimeout(10000);
		
		httpConn.setDoInput(true);
		httpConn.setRequestProperty("Accept", "application/json");
		httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		
		httpConn.setRequestProperty("Content-Length", String.valueOf(params.length()));
		OutputStream os = httpConn.getOutputStream();
		os.write(params.getBytes("utf-8"));
		os.flush();
		os.close();
		
		int responseCode = httpConn.getResponseCode();
		
		if( HttpURLConnection.HTTP_OK == responseCode) {
			oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),"UTF-8"));
		} else {
			logger.info("responseCode etc >>>> : " +responseCode);
			oBufReader = new BufferedReader(new InputStreamReader(httpConn.getErrorStream(),"UTF-8"));
			result = "PUSH_SEND_HTTP_RESULT_FAIL ERRODE : " + String.valueOf(responseCode);
		}
	

		StringBuffer sb = new StringBuffer(strRslt);
		
		int i = 1;
		while ((strBuffer = oBufReader.readLine()) != null && (i < 2)) {
			if (strBuffer.length() > 1) {
				sb.append(strBuffer);
			}
		}

		oBufReader.close();
	
		if( HttpURLConnection.HTTP_OK == responseCode ) {
			result = "PUSH_SEND_HTTP_RESULT : " + sb.toString();
		}
		
		logger.debug("pushtype : {} pushid :{} pushrsesult {}", type,id,result);
		
		return result;
	}
	
	
}