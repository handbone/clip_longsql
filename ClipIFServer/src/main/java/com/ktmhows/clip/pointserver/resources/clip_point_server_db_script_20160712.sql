
CREATE SEQUENCE "POINT_HIST_TBL_point_hist_idx_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_HIST_TBL_point_hist_idx_seq"
  OWNER TO point;

CREATE SEQUENCE "POINT_INFO_TBL_point_hist_idx_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_INFO_TBL_point_hist_idx_seq"
  OWNER TO point;

CREATE SEQUENCE "POINT_INFO_TBL_point_info_idx_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_INFO_TBL_point_info_idx_seq"
  OWNER TO point;

CREATE SEQUENCE "USER_INFO_TBL_user_idx_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 19
  CACHE 1;
ALTER TABLE "USER_INFO_TBL_user_idx_seq"
  OWNER TO point;

CREATE TABLE "POINT_HIST_TBL"
(
  point_hist_idx serial NOT NULL,
  user_ci character varying(256),
  cust_id character varying(256),
  ctn character varying(256),
  ga_id character varying(256),
  transaction_id character varying(256) NOT NULL,
  point_value bigint,
  description character varying(1024),
  status character(1),
  reg_source character varying(30) NOT NULL,
  reg_date character varying(8),
  reg_time character varying(6),
  chg_date character varying(8),
  chg_time character varying(6),
  point_type character(1),
  CONSTRAINT "PK_POINT_HIST_TBL" PRIMARY KEY (point_hist_idx),
  CONSTRAINT "POINT_HIST_TBL_transaction_id_key" UNIQUE (transaction_id, reg_source)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "POINT_HIST_TBL"
  OWNER TO point;

CREATE INDEX "IDX_POINT_HIST_TBL_01"
  ON "POINT_HIST_TBL"
  USING btree
  (user_ci, cust_id, ctn, ga_id, status);

CREATE TABLE "POINT_INFO_TBL"
(
  user_ci character varying(256) NOT NULL,
  point_hist_idx serial NOT NULL,
  point_type character(1),
  point_value bigint,
  balance bigint,
  description character varying(1024),
  reg_source character varying(30) NOT NULL,
  reg_date character varying(8),
  reg_time character varying(6),
  point_info_idx serial NOT NULL,
  CONSTRAINT "PK_POINT_INFO_TBL" PRIMARY KEY (user_ci, point_hist_idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "POINT_INFO_TBL"
  OWNER TO point;

CREATE INDEX "IDX_POINT_INFO_TBL_01"
  ON "POINT_INFO_TBL"
  USING btree
  (user_ci, reg_date, reg_time);

CREATE TABLE "USER_INFO_TBL"
(
  user_idx serial NOT NULL,
  user_ci character varying(256) NOT NULL,
  cust_id character varying(256),
  ctn character varying(256),
  ga_id character varying(256),
  status character(1),
  reg_date character varying(8),
  reg_time character varying(6),
  exit_ci character varying(256),
  exit_date character varying(8),
  exit_time character varying(6),
  CONSTRAINT "PK_USER_INFO_TBL" PRIMARY KEY (user_idx),
  CONSTRAINT "UNQ_USER_INFO_TBL_01" UNIQUE (user_ci)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "USER_INFO_TBL"
  OWNER TO point;

CREATE INDEX "IDX_USER_INFO_TBL_01"
  ON "USER_INFO_TBL"
  USING btree
  (user_ci, cust_id, ctn, ga_id, status);

CREATE TABLE "BZSCREEN_POST_TBL"
(
  transaction_id character varying(256) NOT NULL,
  user_id character varying(256) NOT NULL,
  campaign_id bigint,
  campaign_name character varying(256),
  event_at bigint,
  is_media integer,
  extra character varying(512),
  action_type character(1),
  point bigint,
  base_point bigint,
  data character varying(256),
  reg_date character varying(8),
  reg_time character varying(6),
  CONSTRAINT "PK_BZSCREEN_POST_TBL" PRIMARY KEY (transaction_id, user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "BZSCREEN_POST_TBL"
  OWNER TO point;

CREATE TABLE "BZAD_POST_TBL"
(
  app_key character varying(256) NOT NULL,
  transaction_id character varying(256) NOT NULL,
  user_id character varying(256) NOT NULL,
  campaign_id bigint,
  title character varying(256),
  event_at bigint,
  is_media integer,
  extra character varying(512),
  action_type character(1),
  point bigint,  
  reg_date character varying(8),
  reg_time character varying(6),
  CONSTRAINT "PK_BZAD_POST_TBL" PRIMARY KEY (app_key, transaction_id, user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "BZAD_POST_TBL"
  OWNER TO point;

CREATE TABLE "ACL_INFO_TBL"
(
  requester_code character varying(256) NOT NULL,
  requester_name character varying(256) NOT NULL,
  ip_addr character varying(2000),
  use_status character(1),
  description character varying(2000),
  CONSTRAINT "ACL_INFO_TBL_pkey" PRIMARY KEY (requester_code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "ACL_INFO_TBL"
  OWNER TO point;

INSERT INTO "ACL_INFO_TBL"(requester_code, requester_name, ip_addr, use_status, description) VALUES ('clip', 'clip', '61.78.33.147,61.78.33.148,172.31.50.18', 'Y', 'clip');
INSERT INTO "ACL_INFO_TBL"(requester_code, requester_name, ip_addr, use_status, description) VALUES ('clippoint', 'clippoint', '172.31.50.31,172.31.50.32,222.107.254.178', 'Y', 'clippoint');
INSERT INTO "ACL_INFO_TBL"(requester_code, requester_name, ip_addr, use_status, description) VALUES ('giftishow', 'giftishow', '125.131.141.0-125.131.141.128,61.74.201.224-255,220.117.242.128-220.117.242.191', 'Y', 'giftishow');
