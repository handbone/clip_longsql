/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.resources.mappers;

import java.util.List;
import java.util.Map;

import com.ktmhows.clip.pointserver.vo.PointHistVo;
import com.ktmhows.clip.pointserver.vo.UserInfoVo;

public interface PointHistMapper {
	PointHistVo getPointHist(Map<String, String> parameters) throws java.lang.Exception;
	List<PointHistVo> getPointHistListForChange(UserInfoVo uiv) throws java.lang.Exception;
	
	int insertPointHist(PointHistVo phv) throws java.lang.Exception;
	int insertPointHistCount(PointHistVo phv) throws java.lang.Exception;
	
	int changePointHist(int pointHistIdx) throws java.lang.Exception;
	
	int changePointHistCount(int pointHistIdx) throws java.lang.Exception;
	int changePointHistFail(int pointHistIdx) throws java.lang.Exception;
	
	int getPointHistCount(Map<String, String> parameters) throws java.lang.Exception;
	int changePointHistCountFail(int pointHistIdx) throws java.lang.Exception;
	int getDuplicateChk2(Map<String, String> parameters) throws java.lang.Exception;
	int updatePointRouletteJoin(Map<String, String> parameters) throws java.lang.Exception;
	
	//취소용 추가
	PointHistVo selectPointHistForCancel(Map<String, String> parameters) throws java.lang.Exception;
	int selectPointCancelInfoChk(Map<String, String> parameters) throws java.lang.Exception;
	int insertPointCancelInfo(Map<String, String> parameters) throws java.lang.Exception;
	
	/**
	 * point_hist_tbl 입력실패 시 transaction_id 중복 체크
	 * @param map
	 * @return
	 * @throws java.lang.Exception
	 */
	int getCheckPointHist(@SuppressWarnings("rawtypes") Map map) throws java.lang.Exception;
}
