/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.controllers;

import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ktmhows.clip.pointserver.config.exceptions.CommonException;
import com.ktmhows.clip.pointserver.config.exceptions.NotFoundException;
import com.ktmhows.clip.pointserver.config.exceptions.UnAuthorizedException;
import com.ktmhows.clip.pointserver.services.BZPostBackService;
import com.ktmhows.clip.pointserver.services.CLiPPointService;
import com.ktmhows.clip.pointserver.utils.DateUtil;
import com.ktmhows.clip.pointserver.vo.BuzzAdNewVo;
import com.ktmhows.clip.pointserver.vo.BuzzVillVo;
import com.ktmhows.clip.pointserver.vo.PointHistVo;

@Controller
@SuppressWarnings("unchecked")
public class BZPostBackController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final String BUZ_UNIT_ID = "219856490694845";
	
	@Autowired
	private BZPostBackService bzPostBackService;

	@Autowired
	private CLiPPointService clipPointService;

	// insertPointHist 를 쓰는 모든곳에 횟수 제한을 걸다보니 BZS까지 횟수제한 로직을 걸었다.
	// 실제 사용하지는 않기때문에 주석처리한다.
	//@Autowired
	//private ACLInfoMapper aclInfoMapper;
	
	
	@RequestMapping(value="postBackForBZScreen.do")
	@ResponseBody
	public String postBackForBZScreen(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		// 등록 매체를 구분하기 위한 값
		parameters.put("reg_source", "BZS");

		if(!this.clipPointService.checkIP(parameters.get("reg_source"), request.getRemoteAddr())){
			throw new UnAuthorizedException("401", "잘못된 IP 주소.");
		}
		
		// insertPointHist 를 쓰는 모든곳에 횟수 제한을 걸다보니 BZS까지 횟수제한 로직을 걸었다.
		// 실제 사용하지는 않기때문에 주석처리한다.
		//ACLInfoVo aiv = this.aclInfoMapper.getACLInfo(parameters.get("reg_source"));
		//parameters.put("acl_limit_yn", aiv.acl_limit_yn);
		//parameters.put("acl_limit_cnt", aiv.acl_limit_cnt);
/*		
		PointHistVo phv = this.bzPostBackService.insertBuzzScreenPoint(parameters);

		if(phv == null){
			throw new CommonException("501", "입력 실패!");
		}
		
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("cust_id", phv.getCustId());
        jsonObj.put("transaction_id", phv.getTransactionId());
        jsonObj.put("datetime", phv.getRegDate() + phv.getRegTime());
        jsonObj.put("point_value", phv.getPointValue());
        jsonObj.put("description", phv.getDescription());

		log.append(phv.getLog());
		
		request.setAttribute("logSb", log);
		return jsonObj.toString();
*/
		
		int checkBZS = 0;
		PointHistVo phv =null;
		 
		try {
			phv = this.bzPostBackService.insertBuzzScreenPoint(parameters);
		}catch (NotFoundException e) {
			throw new NotFoundException("404", "클립서버 사용자정보 없음!");
		}catch (Exception e) {
			logger.error("BZS error transactionId :"+ Objects.toString(parameters.get("transaction_id"),"")  ,e);
			
			log.append("BZS error transactionId : "+ Objects.toString(parameters.get("transaction_id"),"") + "\r\n");
			phv = null;
		} 
		
	
		if(phv == null) {
			log.append("BZS 중복 확인 시작  transactionId : " + Objects.toString(parameters.get("transaction_id"),"") +" time="+DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			checkBZS = this.bzPostBackService.getBZScreenPostCheck(parameters);
			
			if( checkBZS == 0 ) {
				log.append("BZS 입력 실패 중복아님  transactionId : " + Objects.toString(parameters.get("transaction_id"),"") + " time="+DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
				throw new CommonException("501", "입력 실패!");
			} else {
				log.append("BZS 중복 요청 성공 transactionId : " + Objects.toString(parameters.get("transaction_id"),"")+ " time="+DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			}
		}
		
        JSONObject jsonObj = new JSONObject();
        if( phv != null ) {
        	  
            jsonObj.put("cust_id", Objects.toString(phv.getCustId(),"") );
            jsonObj.put("transaction_id", Objects.toString(phv.getTransactionId(),"") );
            jsonObj.put("datetime", Objects.toString(phv.getRegDate() + phv.getRegTime(),"") );
            jsonObj.put("point_value", Objects.toString(phv.getPointValue(),"") );
            jsonObj.put("description", Objects.toString(phv.getDescription(),"") );

    		log.append(phv.getLog());
        	
        }		
		
        request.setAttribute("logSb", log);
		return jsonObj.toString();
		
	}

	@RequestMapping(value="postBackForBZAD.do")
	@ResponseBody
	public String postBackForBZAD(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");
		
		
		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		
		//if(BUZ_UNIT_ID.equals(buzzAdNewVo.getUnit_id())){
		if(BUZ_UNIT_ID.equals(parameters.get("unit_id") ) ){
			
			BuzzAdNewVo buzzAdNewVo = new BuzzAdNewVo(parameters);
			BuzzAdNewVo result = this.bzPostBackService.insertBuzzADPoint(buzzAdNewVo);
			
			JSONObject jsonObj = new JSONObject();
	        jsonObj.put("custom", result.getCust_id());
	        jsonObj.put("transaction_id", result.getTransaction_id());
	        jsonObj.put("result", "success");
	        jsonObj.put("resultMsg", result.getResultMsg());
	        
			request.setAttribute("logSb", log);
			return jsonObj.toString();
		}
		
		// 등록 매체를 구분하기 위한 값
		parameters.put("reg_source", "BZA");

		if(!this.clipPointService.checkIP(parameters.get("reg_source"), request.getRemoteAddr())){
			throw new UnAuthorizedException("401", "잘못된 IP 주소.");
		}
		
		// insertPointHist 를 쓰는 모든곳에 횟수 제한을 걸다보니 BZS까지 횟수제한 로직을 걸었다.
		// 실제 사용하지는 않기때문에 주석처리한다.
		//ACLInfoVo aiv = this.aclInfoMapper.getACLInfo(parameters.get("reg_source"));
		//parameters.put("acl_limit_yn", aiv.acl_limit_yn);
		//parameters.put("acl_limit_cnt", aiv.acl_limit_cnt);
		
		PointHistVo phv = this.bzPostBackService.insertBuzzADPoint(parameters);

		if(phv == null){
			throw new CommonException("502", "입력 실패!");
		}
		
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("cust_id", phv.getCustId());
        jsonObj.put("transaction_id", phv.getTransactionId());
        jsonObj.put("datetime", phv.getRegDate() + phv.getRegTime());
        jsonObj.put("point_value", phv.getPointValue());
        jsonObj.put("description", phv.getDescription());

		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}
	
	
	
	
	
	
	// insertPointHist 를 쓰는 모든곳에 횟수 제한을 걸다보니 BZS까지 횟수제한 로직을 걸었다.
	// 실제 사용하지는 않기때문에 주석처리한다.
	/*@RequestMapping(value="aclLimitTest.do")
	@ResponseBody
	public String aclLimitTest(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		// 등록 매체를 구분하기 위한 값
		parameters.put("reg_source", "aclLimitTest");

		if(!this.clipPointService.checkIP(parameters.get("reg_source"), request.getRemoteAddr())){
			throw new UnAuthorizedException("401", "잘못된 IP 주소.");
		}
		
		ACLInfoVo aiv = this.aclInfoMapper.getACLInfo(parameters.get("reg_source"));
		parameters.put("acl_limit_yn", aiv.acl_limit_yn);
		parameters.put("acl_limit_cnt", aiv.acl_limit_cnt);
		
		PointHistVo phv = this.bzPostBackService.insertBuzzScreenPoint(parameters);

		if(phv == null){
			throw new CommonException("501", "입력 실패!");
		}
		
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("cust_id", phv.getCustId());
        jsonObj.put("transaction_id", phv.getTransactionId());
        jsonObj.put("datetime", phv.getRegDate() + phv.getRegTime());
        jsonObj.put("point_value", phv.getPointValue());
        jsonObj.put("description", phv.getDescription());

		log.append(phv.getLog());
		
		request.setAttribute("logSb", log);
		return jsonObj.toString();user_id
	}*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
