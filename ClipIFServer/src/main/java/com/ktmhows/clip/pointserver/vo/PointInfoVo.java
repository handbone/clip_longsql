/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.vo;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


public class PointInfoVo {
	private String userCi;
	private int pointHistIdx;
	private String pointType;
	private long pointValue;
	private long balance;
	private String description;
	private String regSource;
	private String regDate;
	private String regTime;
	private String status;
	private StringBuffer log;
	private int joinLimitFailCnt;
	
	private int pageNum;
	private int pageSize;
	
	private String startDate;
	private String endDate;
	
	
	public int getJoinLimitFailCnt() {
		return joinLimitFailCnt;
	}
	public void setJoinLimitFailCnt(int joinLimitFailCnt) {
		this.joinLimitFailCnt = joinLimitFailCnt;
	}
	public String getUserCi() {
		return userCi;
	}
	public void setUserCi(String userCi) {
		this.userCi = userCi;
	}
	public int getPointHistIdx() {
		return pointHistIdx;
	}
	public void setPointHistIdx(int pointHistIdx) {
		this.pointHistIdx = pointHistIdx;
	}
	public String getPointType() {
		return pointType;
	}
	public void setPointType(String pointType) {
		this.pointType = pointType;
	}
	public long getPointValue() {
		return pointValue;
	}
	public void setPointValue(long pointValue) {
		this.pointValue = pointValue;
	}
	public long getBalance() {
		return balance;
	}
	public void setBalance(long balance) {
		this.balance = balance;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRegSource() {
		return regSource;
	}
	public void setRegSource(String regSource) {
		this.regSource = regSource;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public StringBuffer getLog() {
		return log;
	}
	public void setLog(StringBuffer log) {
		this.log = log;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void getUser_(int pageSize) {
		this.pageSize = pageSize;
	}
	
	@Override
	public String toString() {
		
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public void setUser_ci(String user_ci) {
		this.userCi = user_ci;
	}
	public void setStart_date(String start_date) {
		this.startDate = start_date;
	}

	public void setEnd_date(String end_date) {
		this.endDate = end_date;
	}
	public void setPage_num(String page_num) {
		this.pageNum =  Integer.parseInt(page_num);
	}
	public void setPage_size(String page_size) {
		this.pageSize = Integer.parseInt(page_size);
	}

	public void setPoint_type(String point_type) {
		this.pointType = point_type;
	}
	
	public void paingSetting() {
		if(this.pageNum > 0 ) {
			this.pageNum = this.pageNum * this.pageSize;
		}
		this.pageSize = this.pageSize+1;
	}
}
