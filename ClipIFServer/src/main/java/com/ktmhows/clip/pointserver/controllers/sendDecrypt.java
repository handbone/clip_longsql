package com.ktmhows.clip.pointserver.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ktmhows.clip.pointserver.utils.AesEcbCryptoUtil;
import com.ktmhows.clip.pointserver.utils.DateUtil;


@Controller
public class sendDecrypt {
	
	@Autowired
	private SqlSession postgreSession;
 
	@RequestMapping(value = "/sendDecrypt.do") 
	@ResponseBody
	public String sendDecrypt(HttpServletRequest request, @RequestParam String ctn) throws java.lang.Exception {
	
		return AesEcbCryptoUtil.decryptECB(ctn);
	}

	@RequestMapping(value = "/sendIncrypt.do") 
	@ResponseBody
	public String sendIncrypt(HttpServletRequest request, @RequestParam String ctn) throws java.lang.Exception {
	
		String exitCi = AesEcbCryptoUtil.encryptECB(ctn + DateUtil.getDate("yyyyMMddHHmmss"));
		
		return exitCi;
	}

	
	
}
