/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.vo;

public class ACLInfoVo {
	public String requesterCode;
	public String requesterName;
	public String ipAddr;
	public String useStatus;
	public String description;
	public String acl_limit_yn;
	public String acl_limit_cnt;
		
		
	public String getAcl_limit_yn() {
		return acl_limit_yn;
	}
	public void setAcl_limit_yn(String acl_limit_yn) {
		this.acl_limit_yn = acl_limit_yn;
	}
	public String getAcl_limit_cnt() {
		return acl_limit_cnt;
	}
	public void setAcl_limit_cnt(String acl_limit_cnt) {
		this.acl_limit_cnt = acl_limit_cnt;
	}
	public String getRequesterCode() {
		return requesterCode;
	}
	public void setRequesterCode(String requesterCode) {
		this.requesterCode = requesterCode;
	}
	public String getRequesterName() {
		return requesterName;
	}
	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}
	public String getIpAddr() {
		return ipAddr;
	}
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}
	public String getUseStatus() {
		return useStatus;
	}
	public void setUseStatus(String useStatus) {
		this.useStatus = useStatus;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
