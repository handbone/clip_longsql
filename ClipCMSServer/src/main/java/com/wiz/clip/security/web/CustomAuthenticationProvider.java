/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.security.web;

import java.util.Collection;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

import com.wiz.clip.security.service.UserService;
import com.wiz.clip.security.bean.CmsUserInfo;

public class CustomAuthenticationProvider implements AuthenticationProvider{
	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
	
	@Resource(name="userService") 
    UserService userService;

    //@Autowired
    //private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName(); //폼에서 입력받은 아이디
        String password = (String) authentication.getCredentials(); //폼에서 입력받은 비번
        
        CmsUserInfo user;
        Collection<? extends GrantedAuthority> authorities;

        try {
            user = userService.loadUserByUsername(username);

            //스프링 시큐리티 암호화 사용안함, 기존 sha256으로 유지
            /*****************************************************************************************************************
            String bcryptPassword = passwordEncoder.encode(password);

            logger.info("username : " + username + " / password : " + password + " / hash password : " + bcryptPassword);
            logger.info("username : " + user.getUsername() + " / password : " + user.getPassword());

            //생성시마다 변경되는 암호화 방식 matches로 비교해야함
            if ( !passwordEncoder.matches(password, user.getPassword()) ) {
                throw new BadCredentialsException( "암호가 일치하지 않습니다." );
            }*****************************************************************************************************************/
            
            //패스워드 sha256 체크 
            if(!password.equals(user.getPassword())){
            	userService.passFail(username);
            	throw new BadCredentialsException( "비밀번호가 일치하지 않습니다." );
            }
            authorities = user.getAuthorities();
            
        	
            
        } catch(UsernameNotFoundException e) {
            logger.info(e.toString());
            throw new UsernameNotFoundException(e.getMessage());
        } catch(BadCredentialsException e) {
            logger.info(e.toString());
            throw new BadCredentialsException(e.getMessage());
        } 

        return new UsernamePasswordAuthenticationToken(user, password, authorities);
    }

    @Override
    public boolean supports(Class<?> arg0) {
        return true;
    }
}
