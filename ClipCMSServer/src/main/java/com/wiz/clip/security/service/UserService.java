/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.security.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import com.wiz.clip.security.bean.CmsUserInfo;
import com.wiz.clip.security.bean.Role;
import com.wiz.clip.security.dao.UserDAO;

@Service("userService")
public class UserService implements UserDetailsService{
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	@Resource(name="userDao")
	private UserDAO userDao;
	
    @Override
    public CmsUserInfo loadUserByUsername(final String username) throws UsernameNotFoundException {

        logger.debug("username : " + username);

        HashMap<String,String> hMap = new HashMap<String,String>();
        hMap.put("username", username);
        
        CmsUserInfo user = new CmsUserInfo();
        user = userDao.getUserInfo(hMap);
         
       // 만약 데이터가 없을 경우 익셉션 
        if(user==null){
        	throw new UsernameNotFoundException( "일치하는 아이디가 없습니다." );
        }
        // test 값을 암호화함.
        //String password = "2ea3bb9697ca2ce2e527ad09636073ed5ac1bae5117de38cecb96d3fe9fc1ca3";

        user.setUsername(username);
        user.setPassword(user.getPassword());
        
        /*********************************
         * 3개월 체크 (개인정보, 비번변경)
         *********************************/
        /*SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA);
		Date currentTime = new Date ( );
        //1)3개월 비밀번호 변경 대상자 체크
		Date passdate = null;
		try {
			passdate=mSimpleDateFormat.parse(user.getPassdate());
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Calendar cngday = Calendar.getInstance(); 
		cngday.setTime(passdate);
		cngday.add(Calendar.MONTH,3);
		
		if(currentTime.compareTo(cngday.getTime()) > 0 ){
			user.setState("2"); //3개월 변경 대상자의 경우 state값을 2로 셋팅
		}
		
		//2)3개월  개인정보 취급동의자 체크 [시큐리티 ROLE_GUEST로 취급동의 화면만 허용토록...]
		boolean privateCk = true;
		if(!user.getState().equals("2")){ //비번 3개월 변경 대상자의 경우 개인정보 취급 비번변경후 체크토록... 
			
		        if(user.getPrivatedate().equals("F")){
		        	privateCk = false;
		        }else{
		        	try {
		    			passdate=mSimpleDateFormat.parse(user.getPrivatedate());
		    		
		    		} catch (ParseException e) {
		    			// TODO Auto-generated catch block
		    			e.printStackTrace();
		    		}
		    		
		    		Calendar certday = Calendar.getInstance();
		    		certday.setTime(passdate);
		    		certday.add(Calendar.MONTH,3);
		    		if(currentTime.compareTo(certday.getTime()) > 0 ){
		    			privateCk = false;
		    		}
		        	
		        }
		}*/
       
        /************3개월 체크로직 END***************/
        
        
        /*********************************
         * Spring Security ROLE 부여
         *********************************/
        //ROLE_USER, ROLE_ADMIN
        Role role = new Role();
        if(user.getUserauth().equals("0") ) {
        	role.setName("ROLE_ADMIN");
        }else{
        	if(user.getUserauth().equals("1")){  
        		role.setName("ROLE_USER");
	        }else if(user.getUserauth().equals("2")){  
        		role.setName("ROLE_USER_2");
	        }else{
	        	role.setName("ROLE_GUEST");
	        }
        }
        /************ROLE 부여 END***************/
       

        List<Role> roles = new ArrayList<Role>();
        roles.add(role);
        user.setAuthorities(roles);     
        
        return user;
    }



	public Map<String,String> loadUserByUsercode(String code, String oldpassword, String newpassword) {
		
		Map<String,String> hMap = userDao.getUserPwChange(code);
		String userid =  hMap.get("USERID");
		//기존비번과 다르면
		if(!hMap.get("PASSWORD").equals(oldpassword)){
			hMap.clear();
			hMap.put("userid",userid);
			hMap.put("result", "OLD_DIFF");	//실패 플래그
		}else{
		//기존비번고 같으면
			if(hMap.get("PASSWORD").equals(newpassword)){
				hMap.clear();
				hMap.put("result", "NEW_SAME"); //성공 플래그
			}else{
				hMap.put("pwd", newpassword);
				hMap.put("code", code);
				
				//비번 변경
				userDao.pwChange(hMap);
				
				hMap.clear();
				hMap.put("result", "SUCESS"); //성공 플래그
			}
			
			
		}
		
		
		
		return hMap;
	}

	@SuppressWarnings("deprecation")
	public void privateCertOk(String agentUserCode, HttpServletRequest request) {
		//승인시 PRIVATEDATE 현재날짜로 업데이트
		userDao.privateCertOk(agentUserCode);
		
		//시큐리티 ROLE변경  (update the current Authentication)
		CmsUserInfo user = (CmsUserInfo)SecurityContextHolder.getContext().getAuthentication().getPrincipal();   
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(auth.getAuthorities());
        //현재 시스템에서 ROLE은 한개만 부여토록..클리어
        authorities.clear();
        String role="";
        
        //ROLE_USER, ROLE_ADMIN, ROLE_SUPER_USER
        if(user.getUserauth().equals("0") ) {
        	role = "ROLE_ADMIN";
        }else{
        	if(user.getUserauth().equals("1")){  //KT 엠하우스 
        		role = "ROLE_USER";
	        }else if(user.getUserauth().equals("2")){  //KT 엠하우스 
        		role = "ROLE_USER_2";
	        }else{
	        	role = "ROLE_GUEST";
	        }
        }
        
        authorities.add(new GrantedAuthorityImpl(role));
        Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(),auth.getCredentials(),authorities);
        SecurityContextHolder.getContext().setAuthentication(newAuth);
        
        //좌측 메뉴 오픈위해 세션값 업데이트
        HttpSession session = (HttpSession)request.getSession(false);
        session.setAttribute("RoleName", role);
		
    
     
	}


	//비밀번호 실패 카운트업
	public void passFail(String username) {
		int failCnt = userDao.selectFailCnt(username); //현재까지 실패건수 조회
		
		if(failCnt > 2){ //실패3번 이면 블럭
			userDao.blockLogin(username);
		}else{//실패 5번 미만이면 카운트업
			Map<String,String> map = new HashMap<String,String> ();
			map.put("failCnt",(failCnt+1)+"");
			map.put("username",username);
			userDao.passFailCntUp(map);
		}
		
		
	}


	//로그인 성공시 실패카운트 0으로 초기화
	public void loginFailCntReset(String user_id) {
		userDao.loginFailCntReset(user_id);
		
	}
}
