/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.security.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

public class SigninFailureHandler extends SimpleUrlAuthenticationFailureHandler{
	public static String DEFAULT_TARGET_PARAMETER = "spring-security-redirect-login-failure";
    private String targetUrlParameter = DEFAULT_TARGET_PARAMETER;
    public String getTargetUrlParameter() {
         return targetUrlParameter;
    }

    public void setTargetUrlParameter(String targetUrlParameter) {
         this.targetUrlParameter = targetUrlParameter;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
    	
         String accept = request.getHeader("accept");


         if( StringUtils.indexOf(accept, "html") > -1 ) {

              String redirectUrl = request.getParameter(this.targetUrlParameter);
              if (redirectUrl != null) {
                   super.logger.debug("Found redirect URL: " + redirectUrl);
                   getRedirectStrategy().sendRedirect(request, response, redirectUrl);
              } else {
                   super.onAuthenticationFailure(request, response, exception);
              }

         }
       //※)IE8에서 fail 
 	   /* String ua = request.getHeader("User-Agent").toLowerCase();
 		if (ua.matches(".*msie 8.0.*")) {
 			request.getRequestDispatcher("/signin.do?error=ERROR").forward(request, response);
 		}*/
        
    }
}
