/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.security.bean;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;

public class Role implements GrantedAuthority  {
	 private static final long serialVersionUID = 1L;

	    private String name; //ROLE 명
	    private List<Privilege> privileges; //ROLE 권한

	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }

	    @Override
	    public String getAuthority() {
	        return this.name;
	    }

	    public List<Privilege> getPrivileges() {
	        return privileges;
	    }
	    public void setPrivileges(List<Privilege> privileges) {
	        this.privileges = privileges;
	    }
}
