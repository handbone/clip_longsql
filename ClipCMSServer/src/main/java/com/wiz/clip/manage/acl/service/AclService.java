/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.acl.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.acl.bean.AclInfoTbl;

public interface AclService {

	List<AclInfoTbl> getAclList(ModelMap map);

	List<AclInfoTbl> getAclListNoPaging(ModelMap map);
	
	int getAclListCnt(ModelMap map);

	int aclInsert(HttpServletRequest request, AclInfoTbl actInfoTbl);

	AclInfoTbl getAcl(String requester_code);

	int aclUpdate(HttpServletRequest request, AclInfoTbl aclInfoTbl);
	
	int aclDelete(String requester_code);

	int requesterNameChk(String requester_name);

	int requesterCodeChk(String requester_code);

}
