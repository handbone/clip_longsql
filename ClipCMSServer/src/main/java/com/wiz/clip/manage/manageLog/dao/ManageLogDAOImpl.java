/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.manageLog.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wiz.clip.manage.manageLog.bean.PointHistTbl;
import com.wiz.clip.manage.manageLog.bean.PointInfoTbl;
import com.wiz.clip.manage.manageLog.bean.SearchBean;



@Repository
public class ManageLogDAOImpl implements ManageLogDAO  {

	@Autowired
	private SqlSession postgreSession;

	Logger logger = Logger.getLogger(ManageLogDAOImpl.class.getName());

	@Override
	public int getPointInfoTblCnt(SearchBean searchBean) {
		return postgreSession.selectOne("MANAGELOG.getPointInfoTblCnt", searchBean);
	}

	@Override
	public int getPointHistTblCnt(SearchBean searchBean) {
		return postgreSession.selectOne("MANAGELOG.getPointHistTblCnt", searchBean);
	}

	@Override
	public List<PointInfoTbl> getPointInfoTbl(SearchBean searchBean) {
		return postgreSession.selectList("MANAGELOG.getPointInfoTbl", searchBean);
	}

	@Override
	public List<PointHistTbl> getPointHistTbl(SearchBean searchBean) {
		return postgreSession.selectList("MANAGELOG.getPointHistTbl", searchBean);
	}

	@Override
	public List<PointInfoTbl> getPointInfoTblAll(SearchBean searchBean) {
		return postgreSession.selectList("MANAGELOG.getPointInfoTblAll", searchBean);
	}

	@Override
	public int getPointInfoTblCntAll(SearchBean searchBean) {
		return postgreSession.selectOne("MANAGELOG.getPointInfoTblCntAll", searchBean);
	}

	@Override
	public PointHistTbl getPointHist(SearchBean searchBean) {
		try{
			return postgreSession.selectOne("MANAGELOG.getPointHist", searchBean);
		} catch (Exception e ){
			return postgreSession.selectOne("MANAGELOG.getPointHistLocal", searchBean);
		}
		
	}


	
	
}
