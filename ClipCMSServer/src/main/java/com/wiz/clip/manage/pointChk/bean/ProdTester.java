package com.wiz.clip.manage.pointChk.bean;

public class ProdTester {
	
	private String reg_date;
	private String user_ci;
	private String ctn;
	private String active;
	
	public ProdTester() {
		// TODO Auto-generated constructor stub
	}
	
	public ProdTester(String user_ci, String active) {
		this.user_ci = user_ci;
		this.active = active;
	}
	
	
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
	

}
