/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.userInfo.dao;

import java.util.List;

import com.wiz.clip.manage.userInfo.bean.SearchBean;
import com.wiz.clip.manage.userInfo.bean.UserInfoTbl;
import com.wiz.clip.manage.userInfo.bean.UserInfoTblLog;




public interface UserInfoDAO  {

	int getUserInfoCnt(SearchBean searchBean);
	
	List<UserInfoTbl> getUserInfoList(SearchBean searchBean);

	UserInfoTbl getUserInfo(SearchBean searchBean);

	int getUserLogCnt(SearchBean searchBean);

	List<UserInfoTblLog> getUserLogList(SearchBean searchBean);


}
