/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.stat.web;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.manage.acl.bean.AclInfoTbl;
import com.wiz.clip.manage.stat.bean.SearchBean;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint2;
import com.wiz.clip.manage.stat.bean.TblStatMediaPoint;
import com.wiz.clip.manage.stat.bean.TblStatMonthlyPoint;
import com.wiz.clip.manage.stat.service.StatService;

@Controller
public class StatLogController  {
	
	Logger logger = Logger.getLogger(StatLogController.class.getName());

	@Autowired
	private StatService statService;

	@Value("#{config['PAGE_SIZE']}") int PAGE_SIZE;
	
	/*
	 * 통계관리
	 */
	@RequestMapping(value = "/manage/stat/statLog.do")
	public ModelAndView statLog(
			@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) {

		/*logger.debug("status :" + searchBean.getRoulette_id());
		logger.debug("dateType :" + searchBean.getDateType());
		logger.debug("startdate :" + searchBean.getStartdate());
		logger.debug("enddate :" + searchBean.getEnddate());*/

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		
		List<AclInfoTbl> requesterNameList = statService.getRequesterNameList(map);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("requesterNameList", requesterNameList);
		mav.addObject("searchBean", searchBean);
		
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
				
		if("media".equals(searchBean.getDateType())){
			List<TblStatMediaPoint> medialist = statService.statLogMedia(searchBean);
			int totalCnt = statService.getStatLogMediaCnt(searchBean);
			paging.makePagingHtml(totalCnt);
			mav.addObject("medialist", medialist);
			mav.addObject("totalCnt", totalCnt);
			mav.addObject("divString", "statLogMedia");
		}else if("day".equals(searchBean.getDateType())){
			List<TblStatDailyPoint> daylist = statService.statLogDaily(searchBean);
			int totalCnt = statService.getStatLogDailyCnt(searchBean);
			paging.makePagingHtml(totalCnt);
			mav.addObject("daylist", daylist);
			mav.addObject("totalCnt", totalCnt);
			mav.addObject("divString", "statLogDay");
		}else if("month".equals(searchBean.getDateType())){
			List<TblStatMonthlyPoint> monthlist = statService.statLogMonthly(searchBean);
			int totalCnt = statService.getStatLogMonthlyCnt(searchBean);
			paging.makePagingHtml(totalCnt);
			mav.addObject("monthlist", monthlist);
			mav.addObject("totalCnt", totalCnt);
			mav.addObject("divString", "statLogMonth");
		}else{
			paging.makePagingHtml(0);
		}
		
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "통계관리");
		mav.addObject("display", "../clip/manage/stat/statLog.jsp");
		
		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "통계관리");
			//mav.addObject("divString", "statLogDay");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	/*
	 * 통계확장
	 */
	@RequestMapping(value = "/manage/stat/statLog2.do")
	public ModelAndView statLog2(
			@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) {

		/*logger.debug("status :" + searchBean.getRoulette_id());
		logger.debug("dateType :" + searchBean.getDateType());
		logger.debug("startdate :" + searchBean.getStartdate());
		logger.debug("enddate :" + searchBean.getEnddate());*/

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		
		List<AclInfoTbl> requesterNameList = statService.getRequesterNameList(map);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("requesterNameList", requesterNameList);
		mav.addObject("searchBean", searchBean);
		
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
				
			List<TblStatDailyPoint2> daylist = statService.statLogDaily2(searchBean);
			int totalCnt = statService.getStatLogDailyCnt2(searchBean);
			paging.makePagingHtml(totalCnt);
			mav.addObject("daylist", daylist);
			mav.addObject("totalCnt", totalCnt);
			mav.addObject("divString", "statLogDay");
		
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "통계확장");
		mav.addObject("display", "../clip/manage/stat/statLog2.jsp");
		
		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "통계확장");
			mav.addObject("divString", "statLogDay2");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}

	

}
