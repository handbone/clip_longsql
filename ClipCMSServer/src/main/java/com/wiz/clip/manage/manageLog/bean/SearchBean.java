/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.manageLog.bean;

import com.wiz.clip.common.page.PageBean;

public class SearchBean extends PageBean {
	private String status;
	private String keywd;
	private String requester_code;
	private String statusUseDiv;
	
	private String startdate;
	private String enddate;
	
	private String cust_id;
	private String ga_id;
	private String ctn;
	
	private String excelDownload;
	
	private String point_hist_idx;
	
	
	public String getPoint_hist_idx() {
		return point_hist_idx;
	}
	public void setPoint_hist_idx(String point_hist_idx) {
		this.point_hist_idx = point_hist_idx;
	}
	public String getStatusUseDiv() {
		return statusUseDiv;
	}
	public void setStatusUseDiv(String statusUseDiv) {
		this.statusUseDiv = statusUseDiv;
	}
	public String getRequester_code() {
		return requester_code;
	}
	public void setRequester_code(String requester_code) {
		this.requester_code = requester_code;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getGa_id() {
		return ga_id;
	}
	public void setGa_id(String ga_id) {
		this.ga_id = ga_id;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getKeywd() {
		return keywd;
	}
	public void setKeywd(String keywd) {
		this.keywd = keywd;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getExcelDownload() {
		return excelDownload;
	}
	public void setExcelDownload(String excelDownload) {
		this.excelDownload = excelDownload;
	}
	
	
	
}
