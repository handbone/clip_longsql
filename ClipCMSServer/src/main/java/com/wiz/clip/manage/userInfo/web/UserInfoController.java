/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.userInfo.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.manage.manageLog.bean.PointHistTbl;
import com.wiz.clip.manage.manageLog.bean.PointInfoTbl;
import com.wiz.clip.manage.manageLog.service.ManageLogService;
import com.wiz.clip.manage.pointChk.bean.TblStatTopUser;
import com.wiz.clip.manage.userInfo.bean.SearchBean;
import com.wiz.clip.manage.userInfo.bean.UserInfoTbl;
import com.wiz.clip.manage.userInfo.bean.UserInfoTblLog;
import com.wiz.clip.manage.userInfo.service.UserInfoService;

@Controller
public class UserInfoController  {
	
	Logger logger = Logger.getLogger(UserInfoController.class.getName());

	@Value("#{config['PAGE_SIZE']}") int PAGE_SIZE;
	
	@Autowired
	private UserInfoService userInfoService;
	
	@Autowired
	private ManageLogService manageLogService;

	/*
	 * 사용자 조회
	 */
	@RequestMapping(value = "/manage/userInfo/userInfo.do")
	public ModelAndView userInfo(
			@RequestParam(value = "pg", required = false) String page,
			@RequestParam(value = "search_yn", required = false) String search_yn,
			SearchBean searchBean, ModelMap map) throws java.lang.Exception {

		
		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		
		
		List<UserInfoTbl> list = new ArrayList<UserInfoTbl>();
		if("Y".equals(search_yn)){
			list = userInfoService.getUserInfoList(searchBean);
		}
		
		if( "ctn".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		}
		if( "ga_id".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		}
		
		int totalCnt = 0;
		if("Y".equals(search_yn)){
			totalCnt = userInfoService.getUserInfoCnt(searchBean);
		}
		if( "ctn".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		}
		if( "ga_id".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		}
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "사용자 조회");
		mav.addObject("display", "../clip/manage/userInfo/userInfo.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "사용자조회");
			mav.addObject("divString", "userInfo");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	/*
	 * 사용자 변경기록
	 */
	@RequestMapping(value = "/manage/userInfo/userLog.do")
	public ModelAndView userLog(
			@RequestParam(value = "pg", required = false) String page,
			@RequestParam(value = "search_yn", required = false) String search_yn,
			SearchBean searchBean, ModelMap map) throws java.lang.Exception {

		
		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		
		
		List<UserInfoTblLog> list = new ArrayList<UserInfoTblLog>();
		if("Y".equals(search_yn)){
			list = userInfoService.getUserLogList(searchBean);
		}
		
		if( "ctn".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		}
		if( "ga_id".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		}
		
		int totalCnt = 0;
		if("Y".equals(search_yn)){
			totalCnt = userInfoService.getUserLogCnt(searchBean);
		}
		if( "ctn".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		}
		if( "ga_id".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		}
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "사용자 변경기록");
		mav.addObject("display", "../clip/manage/userInfo/userLog.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "사용자조회");
			mav.addObject("divString", "userLog");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	/*
	 * 사용자 조회
	 */
	@RequestMapping(value = "/manage/userInfo/userHistory.do")
	public ModelAndView userHistory(
			@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) throws java.lang.Exception {

		ModelAndView mav = new ModelAndView();
		
		// 입력된 키워드 기반으로 사용자 정보를 가져온다
		UserInfoTbl userInfo = userInfoService.getUserInfo(searchBean);
		searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		mav.addObject("userInfo", userInfo);
		
		
		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		
		// user_ci 기반으로 "POINT_INFO_TBL"를 조회한다.
		com.wiz.clip.manage.manageLog.bean.SearchBean searchBean2 = new com.wiz.clip.manage.manageLog.bean.SearchBean();
		if( null != userInfo){
			searchBean2.setStatus("user_ci");
			searchBean2.setKeywd(userInfo.getUser_ci());
			searchBean2.setStartdate(searchBean.getStartdate());
			searchBean2.setEnddate(searchBean.getEnddate());
			searchBean2.setStartNum(startNum);
			searchBean2.setEndNum(endNum);
			searchBean2.setExcelDownload(searchBean.getExcelDownload());
		}
		List<PointInfoTbl> infolist = manageLogService.getPointInfoTbl(searchBean2);
		int totalCnt = manageLogService.getPointInfoTblCnt(searchBean2);
		paging.makePagingHtml(totalCnt);
		mav.addObject("infolist", infolist);
		mav.addObject("totalCnt", totalCnt);
		
		// 상세 버튼으로 HISTORY 테이블을 본다.
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		mav.addObject("searchBean", searchBean);
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "사용자 기록조회");
		mav.addObject("display", "../clip/manage/userInfo/userHistory.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "사용자 기록조회");
			mav.addObject("divString", "userHistory");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	
	
	/*
	 * 사용자 기록보기 > 상세보기
	 */
	@RequestMapping(value = "/manage/userInfo/userHistoryDetail.do", method = { RequestMethod.POST })
	@ResponseBody
	public  PointHistTbl userHistoryDetail(SearchBean searchBean, 
			HttpServletRequest request, HttpServletResponse response
			) throws java.lang.Exception {

		com.wiz.clip.manage.manageLog.bean.SearchBean searchBean2 = new com.wiz.clip.manage.manageLog.bean.SearchBean();
		searchBean2.setPoint_hist_idx(searchBean.getPoint_hist_idx());
		PointHistTbl pointHistTbl = manageLogService.getPointHist(searchBean2);		
				
		return pointHistTbl;
        
		
		
	}
		

}
