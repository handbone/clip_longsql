/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.manageLog.bean;

public class PointHistTbl  {
	
	private String requester_code;
	
	private String point_hist_idx;
	private String user_ci;
	private String cust_id;
	private String ctn;
	private String ga_id;
	private String transaction_id;
	private String point_value;
	private String description;
	private String status;
	private String reg_source;
	private String reg_date;
	private String reg_time;
	private String chg_date;
	private String chg_time;
	private String point_type;
	
	
	
	public String getRequester_code() {
		return requester_code;
	}
	public void setRequester_code(String requester_code) {
		this.requester_code = requester_code;
	}
	public String getPoint_hist_idx() {
		return point_hist_idx;
	}
	public void setPoint_hist_idx(String point_hist_idx) {
		this.point_hist_idx = point_hist_idx;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getGa_id() {
		return ga_id;
	}
	public void setGa_id(String ga_id) {
		this.ga_id = ga_id;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getPoint_value() {
		return point_value;
	}
	public void setPoint_value(String point_value) {
		this.point_value = point_value;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReg_source() {
		return reg_source;
	}
	public void setReg_source(String reg_source) {
		this.reg_source = reg_source;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getReg_time() {
		return reg_time;
	}
	public void setReg_time(String reg_time) {
		this.reg_time = reg_time;
	}
	public String getChg_date() {
		return chg_date;
	}
	public void setChg_date(String chg_date) {
		this.chg_date = chg_date;
	}
	public String getChg_time() {
		return chg_time;
	}
	public void setChg_time(String chg_time) {
		this.chg_time = chg_time;
	}
	public String getPoint_type() {
		return point_type;
	}
	public void setPoint_type(String point_type) {
		this.point_type = point_type;
	} 
	
	
	
}
