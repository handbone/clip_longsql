/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.acl.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.acl.bean.AclInfoTbl;
import com.wiz.clip.manage.acl.dao.AclDAO;

@Service
@Transactional
public class AclServiceImpl implements AclService {
	
	@Autowired
	private AclDAO aclDao;

	Logger logger = Logger.getLogger(AclServiceImpl.class.getName());
	
	@Override
	public List<AclInfoTbl> getAclList(ModelMap map) {
		return aclDao.getAclList(map);
	}

	@Override
	public List<AclInfoTbl> getAclListNoPaging(ModelMap map) {
		return aclDao.getAclListNoPaging(map);
	}
	
	
	@Override
	public int getAclListCnt(ModelMap map) {
		return aclDao.getAclListCnt(map);
	}

	@Override
	public int aclInsert(HttpServletRequest request, AclInfoTbl aclInfoTbl) {
		return aclDao.aclInsert(aclInfoTbl);	
	}

	@Override
	public AclInfoTbl getAcl(String requester_code) {
		return aclDao.getAcl(requester_code);	
	}

	@Override
	public int aclUpdate(HttpServletRequest request, AclInfoTbl aclInfoTbl) {
		return aclDao.aclUpdate(aclInfoTbl);	
	}

	@Override
	public int aclDelete(String requester_code) {
		return aclDao.aclDelete(requester_code);	
	}

	@Override
	public int requesterNameChk(String requester_name) {
		return aclDao.requesterNameChk(requester_name);
	}
	
	@Override
	public int requesterCodeChk(String requester_code) {
		return aclDao.requesterCodeChk(requester_code);
	}	

}