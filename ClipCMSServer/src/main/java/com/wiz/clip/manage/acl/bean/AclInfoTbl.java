/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.acl.bean;

public class AclInfoTbl  {
	
	private String requester_code;
	private String requester_name;
	private String ip_addr;
	private String use_status;
	private String description;
	private String regdate;
	private String acl_limit_yn;
	private String acl_limit_cnt;
	private String datepicker1;
	private String datepicker2;
	
	public String getAcl_limit_yn() {
		return acl_limit_yn;
	}
	public void setAcl_limit_yn(String acl_limit_yn) {
		this.acl_limit_yn = acl_limit_yn;
	}
	public String getAcl_limit_cnt() {
		return acl_limit_cnt;
	}
	public void setAcl_limit_cnt(String acl_limit_cnt) {
		this.acl_limit_cnt = acl_limit_cnt;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getRequester_code() {
		return requester_code;
	}
	public void setRequester_code(String requester_code) {
		this.requester_code = requester_code;
	}
	public String getRequester_name() {
		return requester_name;
	}
	public void setRequester_name(String requester_name) {
		this.requester_name = requester_name;
	}
	public String getIp_addr() {
		return ip_addr;
	}
	public void setIp_addr(String ip_addr) {
		this.ip_addr = ip_addr;
	}
	public String getUse_status() {
		return use_status;
	}
	public void setUse_status(String use_status) {
		this.use_status = use_status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDatepicker2() {
		return datepicker2;
	}
	public void setDatepicker2(String datepicker2) {
		this.datepicker2 = datepicker2;
	}
	public String getDatepicker1() {
		return datepicker1;
	}
	public void setDatepicker1(String datepicker1) {
		this.datepicker1 = datepicker1;
	}

	
	
	
}
