/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.userInfo.bean;

import com.wiz.clip.common.page.PageBean;

public class SearchBean extends PageBean {
	private String status;
	private String keywd;
	private String startdate;
	private String enddate;
	
	private String excelDownload;
	
	private String point_hist_idx;
	
	
	public String getPoint_hist_idx() {
		return point_hist_idx;
	}
	public void setPoint_hist_idx(String point_hist_idx) {
		this.point_hist_idx = point_hist_idx;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getKeywd() {
		return keywd;
	}
	public void setKeywd(String keywd) {
		this.keywd = keywd;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getExcelDownload() {
		return excelDownload;
	}
	public void setExcelDownload(String excelDownload) {
		this.excelDownload = excelDownload;
	}
	
	
	
}
