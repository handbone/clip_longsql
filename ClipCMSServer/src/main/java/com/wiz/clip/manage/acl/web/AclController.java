/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.acl.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.manage.acl.bean.AclInfoTbl;
import com.wiz.clip.manage.acl.service.AclService;

@Controller
public class AclController  {
	Logger logger = Logger.getLogger(AclController.class.getName());

	@Autowired
	private AclService aclService;

	@Value("#{config['PAGE_SIZE']}") int PAGE_SIZE;
	
	/*
	 * 리스트
	 */
	@RequestMapping(value = "/manage/acl/aclList.do")
	public ModelAndView aclList(
									@RequestParam(value = "pg", required = false) String page
									, @RequestParam(required = false) String keywd1
									, @RequestParam(required = false) String keywd2
									, @RequestParam(required = false) String use_status
									, ModelMap map) {

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("startNum", startNum);
		map.put("endNum", endNum);

		// 검색어 관련
		if (keywd1 != null && !keywd1.equals("")) {
			map.put("keywd1", keywd1);
		}else{
			map.put("keywd1", "");
		}
		if (keywd2 != null && !keywd2.equals("")) {
			map.put("keywd2", keywd2);
		} else {
			map.put("keywd2", "");
		}
		map.put("use_status", use_status);
		
		List<AclInfoTbl> list = aclService.getAclList(map);

		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		// 토탈 카운트
		int aclListCng = aclService.getAclListCnt(map);
		paging.makePagingHtml(aclListCng);

		map.clear();
		map.put("pg", pg);
		map.put("list", list);
		map.put("keywd1", keywd1);
		map.put("keywd2", keywd2);
		map.put("use_status", use_status);
		map.put("aclListCng", aclListCng);
		map.put("boardPaging", paging);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "매체관리");
		mav.addObject("display", "../clip/manage/acl/aclList.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 입력폼
	 */
	@RequestMapping(value = "/manage/acl/aclInsertForm.do")
	public ModelAndView aclInsertForm(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("aclInsertForm Page start!!!");
		ModelAndView mav = new ModelAndView();

		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "매체관리");
		mav.addObject("display", "../clip/manage/acl/aclInsertForm.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 입력처리
	 */
	@RequestMapping(value = "/manage/acl/aclInsert.do")
	public ModelAndView aclInsert(HttpServletRequest request, HttpServletResponse response, ModelMap map,
			AclInfoTbl actInfoTbl) {

		logger.debug("aclInsert Page start!!!");
		ModelAndView mav = new ModelAndView();

		aclService.aclInsert(request, actInfoTbl);

		map.addAttribute("msg", "작성하신 내용이 정상적으로 등록되었습니다.");
		map.addAttribute("url", "/manage/acl/aclList.do");
		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 상세보기
	 */
	@RequestMapping(value = "/manage/acl/aclView.do")
	public ModelAndView aclView(@RequestParam String requester_code, @RequestParam(required = false) String page,
			ModelMap map) {
		logger.debug("aclView Page start!!!");
		
		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		AclInfoTbl aclInfoTbl = aclService.getAcl(requester_code);

		map.put("pg", pg);
		map.put("aclInfoTbl", aclInfoTbl);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "매체관리");
		mav.addObject("display", "../clip/manage/acl/aclView.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 수정폼
	 */
	@RequestMapping(value = "/manage/acl/aclUpdateForm.do")
	public ModelAndView aclUpdateForm(@RequestParam String requester_code, @RequestParam(required = false) String page,
			ModelMap map) {
		logger.debug("aclUpdateForm Page start!!!");
		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		AclInfoTbl aclInfoTbl = aclService.getAcl(requester_code);

		map.put("pg", pg);
		map.put("aclInfoTbl", aclInfoTbl);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "매체관리");
		mav.addObject("display", "../clip/manage/acl/aclUpdateForm.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 수정처리
	 */
	@RequestMapping(value = "/manage/acl/aclUpdate.do")
	public ModelAndView aclUpdate(
			@RequestParam(required = false) String requester_code,
			@RequestParam(required = false) String page,
			HttpServletRequest request, HttpServletResponse response, ModelMap map,
			AclInfoTbl actInfoTbl
			) throws java.lang.Exception {

		logger.debug("aclUpdate Page start!!!");
		ModelAndView mav = new ModelAndView();

		String pg = page;

		if (pg == null || pg == ""){
			pg = "1";
		}

		aclService.aclUpdate(request, actInfoTbl);

		map.addAttribute("msg", "작성하신 내용이 정상적으로 수정되었습니다.");
		map.addAttribute("url", "/manage/acl/aclView.do?requester_code=" + requester_code + "&pg=" + pg);

		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}
	
	/*
	 * 삭제처리
	 */
	@RequestMapping(value = "/manage/acl/aclDelete.do")
	public ModelAndView aclDelete(@RequestParam String requester_code,
			@RequestParam(value = "pg", required = false) String page, ModelMap map) {
		logger.debug("aclDelete Page start!!!");

		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		aclService.aclDelete(requester_code);

		map.put("pg", pg);

		ModelAndView mav = new ModelAndView();
		map.addAttribute("msg", "정상적으로 삭제 되었습니다.");
		map.addAttribute("url", "/manage/acl/aclList.do");
		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	
	
	
	/*
	 * 매체명 중복 조회
	 */
	@RequestMapping(value = "/manage/acl/requesterNameChk.do", method = { RequestMethod.POST })
	@ResponseBody
	public String requesterNameChk(
			@RequestParam (required=true) String requester_name,
			HttpServletRequest request, HttpServletResponse response
	) throws java.lang.Exception {
		logger.debug("requesterNameChk Page start!!!");

		// 비교
		int result = aclService.requesterNameChk(requester_name);
		
		Map<String,String> map = new HashMap<String,String>();
		if (result >= 1) {
			map.put("result", "fail");
			return JSONValue.toJSONString(map);
		} else {
			map.put("result", "success");
			return JSONValue.toJSONString(map);
		}
		
	}
	
	
	
	/*
	 * 매체코드 중복 조회
	 */
	@RequestMapping(value = "/manage/acl/requesterCodeChk.do", method = { RequestMethod.POST })
	@ResponseBody
	public String requesterCodeChk(
			@RequestParam (required=true) String requester_code,
			HttpServletRequest request, HttpServletResponse response
	) throws java.lang.Exception {
		logger.debug("requesterCodeChk Page start!!!");

		// 비교
		int result = aclService.requesterCodeChk(requester_code);
		
		Map<String,String> map = new HashMap<String,String>();
		if (result >= 1) {
			map.put("result", "fail");
			return JSONValue.toJSONString(map);
		} else {
			map.put("result", "success");
			return JSONValue.toJSONString(map);
		}
		
	}
	
	
}
