/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.userInfo.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.manage.userInfo.bean.SearchBean;
import com.wiz.clip.manage.userInfo.bean.UserInfoTbl;
import com.wiz.clip.manage.userInfo.bean.UserInfoTblLog;
import com.wiz.clip.manage.userInfo.dao.UserInfoDAO;

@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService {
	
	@Autowired
	private UserInfoDAO userInfoDao;

	Logger logger = Logger.getLogger(UserInfoServiceImpl.class.getName());
	
	public int getUserInfoCnt(SearchBean searchBean) throws java.lang.Exception {
		if( "ctn".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		if( "ga_id".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		return userInfoDao.getUserInfoCnt(searchBean);
	}

	@Override
	public List<UserInfoTbl> getUserInfoList(SearchBean searchBean) throws java.lang.Exception  {
		if( "ctn".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		if( "ga_id".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		return userInfoDao.getUserInfoList(searchBean);
	}

	public int getUserLogCnt(SearchBean searchBean) throws java.lang.Exception {
		if( "ctn".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		if( "ga_id".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		return userInfoDao.getUserLogCnt(searchBean);
	}

	@Override
	public List<UserInfoTblLog> getUserLogList(SearchBean searchBean) throws java.lang.Exception  {
		if( "ctn".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		if( "ga_id".equals(searchBean.getStatus()) && null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		return userInfoDao.getUserLogList(searchBean);
	}
	@Override
	public UserInfoTbl getUserInfo(SearchBean searchBean) throws Exception {
		if( null != searchBean.getKeywd() && !"".equals(searchBean.getKeywd())){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		return userInfoDao.getUserInfo(searchBean);
		
	}
	
	

}
