/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.pointChk.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.common.sendAPI.SendUpdatePoint;
import com.wiz.clip.manage.acl.bean.AclInfoTbl;
import com.wiz.clip.manage.acl.service.AclService;
import com.wiz.clip.manage.pointChk.bean.CmsPointModify;
import com.wiz.clip.manage.pointChk.bean.ProdTester;
import com.wiz.clip.manage.pointChk.bean.SearchBean;
import com.wiz.clip.manage.pointChk.bean.TblStatAnomalyDetection;
import com.wiz.clip.manage.pointChk.bean.TblStatReceiverInfo;
import com.wiz.clip.manage.pointChk.bean.TblStatTopUser;
import com.wiz.clip.manage.pointChk.service.PointChkService;


@Controller
public class PointChkController  {
	
	Logger logger = Logger.getLogger(PointChkController.class.getName());

	@Autowired
	private AclService aclService;
	
	@Autowired
	private PointChkService pointChkService;

	@Autowired
	private SendUpdatePoint sendUpdatePoint;
	
	@Value("#{config['PAGE_SIZE']}") int PAGE_SIZE;
	
	@Value("#{config['WINDOW_OPEN_YN']}") String WINDOW_OPEN_YN;
	
	
	/*
	 * 암호화 테스트 
	 */
	@RequestMapping(value ="/crypto.do")
	@ResponseBody
	public ResponseEntity<String> crypto(@RequestParam(required = true) String stringData,HttpServletRequest request, ModelMap model)  throws java.lang.Exception {
		logger.debug("crypto Page start!!!");
		 
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		return new ResponseEntity<String>(stringData+" : "+new String(AES256CipherClip.AES_Encode(stringData.trim()).getBytes()),responseHeaders, HttpStatus.CREATED);
	}
	/*
	 * 암호화 테스트 
	 */
	@RequestMapping(value ="/decrypto.do") 
	@ResponseBody
	public String decrypto(@RequestParam(required = true) String stringData,HttpServletRequest request, ModelMap model)  throws java.lang.Exception {
		
		logger.debug("decrypto Page start!!!");
		
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		ModelMap map = new ModelMap();
		map.put("result", "success");
		map.put("ctn", new String(AES256CipherClip.AES_Decode(stringData)));
		return JSONValue.toJSONString(map);
	}
	
	/*
	 * TOP 30 (합계)
	 */
	@RequestMapping(value = "/manage/pointChk/pointChk2.do")
	public ModelAndView pointChk2(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) throws java.lang.Exception {

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));


		
		List<TblStatTopUser> list = pointChkService.getPointInfoList(searchBean);

		List<AclInfoTbl> aclNamelist = aclService.getAclListNoPaging(map);
		
		int totalCnt = pointChkService.getPointInfoListCnt(searchBean);
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("aclNamelist", aclNamelist);
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "TOP 30(합계)");
		mav.addObject("display", "../clip/manage/pointChk/pointChk2.jsp");

		mav.addObject("WINDOW_OPEN_YN", WINDOW_OPEN_YN);
		
		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "TOP30(합계)");
			mav.addObject("divString", "pointChk2");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	
	/*
	 * TOP 30 (매체별)
	 */
	@RequestMapping(value = "/manage/pointChk/pointChk.do")
	public ModelAndView pointChk(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) throws java.lang.Exception {

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));


		
		List<TblStatTopUser> list = pointChkService.getPointInfoList(searchBean);

		List<AclInfoTbl> aclNamelist = aclService.getAclListNoPaging(map);
		
		int totalCnt = pointChkService.getPointInfoListCnt(searchBean);
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("aclNamelist", aclNamelist);
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "TOP 30(매체별)");
		mav.addObject("display", "../clip/manage/pointChk/pointChk.jsp");

		mav.addObject("WINDOW_OPEN_YN", WINDOW_OPEN_YN);
		
		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "TOP30(매체별)");
			mav.addObject("divString", "pointChk");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}

	/*
	 * 이상탐지 리스트
	 */
	@RequestMapping(value = "/manage/pointChk/anomalyList.do")
	public ModelAndView anomalyList(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) throws java.lang.Exception {

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));


		
		List<TblStatAnomalyDetection> list = pointChkService.getTblStatAnomalyDetection(searchBean);

		List<AclInfoTbl> aclNamelist = aclService.getAclListNoPaging(map);
		
		int totalCnt = pointChkService.getTblStatAnomalyDetectionCnt(searchBean);
		
		TblStatAnomalyDetection tblStatAnomalyDetectionTop = pointChkService.tblStatAnomalyDetectionTop(1);
		
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("tblStatAnomalyDetectionTop", tblStatAnomalyDetectionTop);
		mav.addObject("aclNamelist", aclNamelist);
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "이상탐지(매체별)");
		mav.addObject("display", "../clip/manage/pointChk/anomalyList.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "이상탐지(매체별)");
			mav.addObject("divString", "anomalyList");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	/*
	 * 이상탐지 리스트 (합계)
	 */
	@RequestMapping(value = "/manage/pointChk/anomalyList2.do")
	public ModelAndView anomalyList2(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) throws java.lang.Exception {
		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

 
		List<TblStatTopUser> list = pointChkService.anomalyList2(searchBean);

		List<AclInfoTbl> aclNamelist = aclService.getAclListNoPaging(map);
		
		int totalCnt = pointChkService.anomalyList2Cnt(searchBean);
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		map.put("totalCnt", totalCnt);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("aclNamelist", aclNamelist);
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "이상탐지(합계)");
		mav.addObject("display", "../clip/manage/pointChk/anomalyList2.jsp");

		mav.addObject("WINDOW_OPEN_YN", WINDOW_OPEN_YN);
		
		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "이상탐지합계");
			mav.addObject("divString", "anomalyList2");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	
	/*
	 * 이상탐지 설정 페이지
	 */
	@RequestMapping(value = "/manage/pointChk/anomalyConfigForm.do")
	public ModelAndView anomalyConfigForm(SearchBean searchBean, ModelMap map) throws java.lang.Exception {

		// 탐지 기준값 가져오기
		int limitPoint = Integer.parseInt(pointChkService.getCmsCommonCode("limitPoint"));
		
		// SMS 통지 수신자 리스트 가져오기
		List<TblStatReceiverInfo> list = pointChkService.getTblStatReceiverInfo();
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("limitPoint", limitPoint);
		mav.addObject("list", list);
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "이상탐지(매체별)");
		mav.addObject("display", "../clip/manage/pointChk/anomalyConfigForm.jsp");
		mav.setViewName("/main/index");
		
		return mav;
	}
	
	/*
	 * 이상탐지 설정 페이지 > 수신자 사용안함으로 변환
	 */
	@RequestMapping(value = "/manage/pointChk/anomalyConfigDelete.do", method = { RequestMethod.POST })
	@ResponseBody
	public String anomalyConfigDelete(
			SearchBean searchBean, 
			HttpServletRequest request, HttpServletResponse response
	) throws java.lang.Exception {
		logger.debug("anomalyConfigDelete Page start!!!");

		int result = pointChkService.anomalyConfigDelete(searchBean);
		
		ModelMap map = new ModelMap();
		if (result >= 1) {
			map.put("result", "success");
			return JSONValue.toJSONString(map);
		} else {
			map.put("result", "fail");
			return JSONValue.toJSONString(map);
		}
		
	}
	
		
	/*
	 * 이상탐지 설정 페이지 > 수신자 추가
	 */
	@RequestMapping(value = "/manage/pointChk/insertTblStatReceiverInfo.do", method = { RequestMethod.POST })
	@ResponseBody
	public String insertTblStatReceiverInfo(
			SearchBean searchBean,
			TblStatReceiverInfo tblStatReceiverInfo,
			HttpServletRequest request, HttpServletResponse response
	) throws java.lang.Exception {
		logger.debug("insertTblStatReceiverInfo Page start!!!");

		int result = pointChkService.insertTblStatReceiverInfo(tblStatReceiverInfo);
		
		ModelMap map = new ModelMap();
		if (result >= 1) {
			map.put("result", "success");
			return JSONValue.toJSONString(map);
		} else {
			map.put("result", "fail");
			return JSONValue.toJSONString(map);
		}
		
	}

	/*
	 * 탐지 기준 변경
	 */
	@RequestMapping(value = "/manage/pointChk/goLimitPointChange.do", method = { RequestMethod.POST })
	@ResponseBody
	public String goLimitPointChange(
			SearchBean searchBean,
			HttpServletRequest request, HttpServletResponse response
	) throws java.lang.Exception {
		logger.debug("goLimitPointChange Page start!!!");

		int result = pointChkService.goLimitPointChange(searchBean);
		
		ModelMap map = new ModelMap();
		if (result >= 1) {
			map.put("result", "success");
			return JSONValue.toJSONString(map);
		} else {
			map.put("result", "fail"); 
			return JSONValue.toJSONString(map);
		}
		
	}
		
	/*
	 * TOP 30 리스트 > 상세보기
	 */
	@RequestMapping(value = "/manage/pointChk/viewDetail.do", method = { RequestMethod.POST })
	@ResponseBody
	public  List<TblStatTopUser> viewDetail(SearchBean searchBean, 
			HttpServletRequest request, HttpServletResponse response
			) throws java.lang.Exception {

		//NOT USE PAGING
		searchBean.setStartNum(0);
		searchBean.setEndNum(1000000);
				
		List<TblStatTopUser> list = pointChkService.viewDetail(searchBean);
		
		return list;
        
		
		
	}
	
	/*
	 * TOP 30 리스트 > 상세보기 팝업
	 */
	@RequestMapping(value = "/manage/pointChk/pointChkPopup.do")
	public ModelAndView pointChkPopup(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) throws java.lang.Exception {
		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		List<TblStatTopUser> list = pointChkService.viewDetail(searchBean);
		
		int totalCnt = pointChkService.viewDetailCnt(searchBean);
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		map.put("totalCnt", totalCnt);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("display", "../clip/manage/pointChk/pointChkPopup.jsp");

		mav.setViewName("/main/popup");
		return mav;
		
		
	}
	
	/*
	 * TOP 30 리스트 > 상세보기 팝업
	 */
	@RequestMapping(value = "/manage/pointChk/pointChk2Popup.do")
	public ModelAndView pointChk2Popup(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) throws java.lang.Exception {
		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		List<TblStatTopUser> list = pointChkService.viewDetail(searchBean);
		
		int totalCnt = pointChkService.viewDetailCnt(searchBean);
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		map.put("totalCnt", totalCnt);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("display", "../clip/manage/pointChk/pointChk2Popup.jsp");

		mav.setViewName("/main/popup");
		return mav;
		
		
	}
	
	/*
	 * 이상탐지(합계) > 상세보기
	 */
	@RequestMapping(value = "/manage/pointChk/viewDetailAnomaly.do", method = { RequestMethod.POST })
	@ResponseBody
	public  List<TblStatTopUser> viewDetailAnomaly(SearchBean searchBean, 
			HttpServletRequest request, HttpServletResponse response
			) throws java.lang.Exception {

		//NOT USE PAGING
		searchBean.setStartNum(0);
		searchBean.setEndNum(1000000);
		
		List<TblStatTopUser> list = pointChkService.viewDetailAnomaly(searchBean);
		
		return list;
   }
	
	/*
	 *  이상탐지(합계) > 상세보기 팝업
	 */
	@RequestMapping(value = "/manage/pointChk/viewDetailAnomalyPopup.do")
	public ModelAndView viewDetailAnomalyPopup(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) throws java.lang.Exception {
		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

 
		List<TblStatTopUser> list = pointChkService.viewDetailAnomaly(searchBean);

		int totalCnt = pointChkService.viewDetailAnomalyCnt(searchBean);
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		map.put("totalCnt", totalCnt);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("display", "../clip/manage/pointChk/anomalyList2Popup.jsp");

		mav.setViewName("/main/popup");
		return mav;
	}
	
	
	
	
	
		/*
		 * 포인트조정 이력조회
		 */
		@RequestMapping(value = "/manage/pointChk/pointUpdateHistory.do")
		public ModelAndView pointUpdateHistory(@RequestParam(value = "pg", required = false) String page,
				SearchBean searchBean, ModelMap map) throws java.lang.Exception {
			String pg = page;
			// 페이징 관련
			if (pg == null || pg == ""){
				pg = "1";
			}
			int endNum = Integer.parseInt(pg) * PAGE_SIZE;
			int startNum = endNum - ( PAGE_SIZE - 1 ) ;
			map.put("endNum", endNum);
			map.put("startNum", startNum);
			searchBean.setStartNum(startNum);
			searchBean.setEndNum(endNum);
			// 페이징
			BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
			paging.setCurrentPage(Integer.parseInt(pg));

			List<CmsPointModify> list = pointChkService.CmsPointModifyList(searchBean);

			int totalCnt = pointChkService.CmsPointModifyListCnt(searchBean);
			paging.makePagingHtml(totalCnt);
			map.clear();
			map.put("pg", pg);
			map.put("boardPaging", paging);
			map.put("totalCnt", totalCnt);
			
			ModelAndView mav = new ModelAndView();
			mav.addObject("searchBean", searchBean);
			mav.addObject("list", list);
			mav.addObject("menu1", "포인트관리");
			mav.addObject("menu2", "포인트 조정");
			mav.addObject("display", "../clip/manage/pointChk/pointUpdateHistory.jsp");

			if("Y".equals(searchBean.getExcelDownload())){
				mav.addObject("pageNm", "포인트 조정");
				mav.addObject("divString", "pointUpdateHistory");
				mav.setViewName("excelView");
			}else{
				mav.setViewName("/main/index");
			}
			
			return mav;
		}
	
	
	
		
		/*
		 * 포인트조정 폼
		 */
		@RequestMapping(value = "/manage/pointChk/pointUpdateForm.do")
		public ModelAndView pointUpdateForm(
				SearchBean searchBean, ModelMap map) throws java.lang.Exception {
			
			ModelAndView mav = new ModelAndView();
			mav.addObject("searchBean", searchBean);
			mav.addObject("menu1", "포인트관리");
			mav.addObject("menu2", "포인트 조정");
			mav.addObject("display", "../clip/manage/pointChk/pointUpdateForm.jsp");
			mav.setViewName("/main/index");
			
			return mav;
		}

	
		
		/*
		 * 입력 된 ctn으로 user_ci를 체크함 / point_hist_tbl 기준으로 체크함
		 */
		@RequestMapping(value ="/manage/pointChk/ctnChk.do") 
		@ResponseBody
		public String ctnChk(SearchBean searchBean
				,HttpServletRequest request, ModelMap model)  throws java.lang.Exception {
			
			logger.debug("ctnChk Page start!!!");
			
			HttpHeaders responseHeaders = new HttpHeaders(); 
			responseHeaders.add("Content-Type", "text/html; charset=utf-8");

			List<SearchBean> resultUserCiList = (List<SearchBean>)pointChkService.ctnChk(searchBean);
			
			ModelMap map = new ModelMap();
			if( null == resultUserCiList || resultUserCiList.size() == 0 ){
				map.put("result", "fail");
			}else if(resultUserCiList.size() == 1 ){
				map.put("result", "success");
			}else{
				map.put("result", "One More");
			}
			return JSONValue.toJSONString(map);
		}
	
		/*
		 * 팝업 입력 된 ctn으로 user_ci를 체크함 / point_hist_tbl 기준으로 체크함
		 */
		@RequestMapping(value = "/manage/pointChk/ctnChkPopup.do")
		public ModelAndView ctnChkPopup(@RequestParam(value = "pg", required = false) String page,
				SearchBean searchBean, ModelMap map) throws java.lang.Exception {

			List<SearchBean> resultUserCiList = (List<SearchBean>)pointChkService.ctnChk(searchBean);
			
			ModelAndView mav = new ModelAndView();
			mav.addObject("searchBean", searchBean);
			mav.addObject("list", resultUserCiList);
			mav.addObject("display", "../clip/manage/pointChk/ctnChkPopup.jsp");

			mav.setViewName("/main/popup");
			return mav;
			
			
		}
		
		/*
		 * 입력 된 ctn으로 user_ci를 체크함 / USER_INFO_TBL 에서 검색함
		 */
		@RequestMapping(value ="/manage/pointChk/ctnChk2.do") 
		@ResponseBody
		public String ctnChk2(SearchBean searchBean
				,HttpServletRequest request, ModelMap model)  throws java.lang.Exception {
			
			logger.debug("ctnChk Page start!!!");
			
			HttpHeaders responseHeaders = new HttpHeaders(); 
			responseHeaders.add("Content-Type", "text/html; charset=utf-8");

			List<SearchBean> resultUserCiList = (List<SearchBean>)pointChkService.ctnChk2(searchBean);
			
			ModelMap map = new ModelMap();
			if( null == resultUserCiList || resultUserCiList.size() == 0 ){
				map.put("result", "fail");
			}else if(resultUserCiList.size() == 1 ){
				map.put("result", "success");
			}else{
				map.put("result", "One More");
			}
			return JSONValue.toJSONString(map);
		}
	
		/*
		 * 팝업 입력 된 ctn으로 user_ci를 체크함 / USER_INFO_TBL 에서 검색함
		 */
		@RequestMapping(value = "/manage/pointChk/ctnChk2Popup.do")
		public ModelAndView ctnChk2Popup(SearchBean searchBean
				,HttpServletRequest request, ModelMap model)  throws java.lang.Exception {

			List<SearchBean> resultUserCiList = (List<SearchBean>)pointChkService.ctnChk2(searchBean);
			
			ModelAndView mav = new ModelAndView();
			mav.addObject("searchBean", searchBean);
			mav.addObject("list", resultUserCiList);
			mav.addObject("display", "../clip/manage/pointChk/ctnChkPopup.jsp");

			mav.setViewName("/main/popup");
			return mav;
			
			
		}
		
		
		/*
		 * 포인트조정
		 */
		@RequestMapping(value ="/manage/pointChk/pointUpdate.do") 
		@ResponseBody
		public String pointUpdate(CmsPointModify cmsPointModify
				, SearchBean searchBean
				, HttpServletRequest request, ModelMap model
			)  throws java.lang.Exception {
			
			logger.debug("pointUpdate Page start!!!");
			
			HttpHeaders responseHeaders = new HttpHeaders(); 
			responseHeaders.add("Content-Type", "text/html; charset=utf-8");

			String resultJson = sendUpdatePoint.sendUpdatePoint(searchBean, cmsPointModify);
			
			// 탈퇴한 사용자일때는 사용자를 찾을 수 없는 404에러가 발생한다.
			// POINT_INFO_TBL에 등록되어있는 USER_CI가  USER_INFO_TBL에 없다면 사용자를 찾을 수 없는 404에러가 발생한다.
			String balanceJson = sendUpdatePoint.sendGetPoint(searchBean, cmsPointModify.getUser_ci());
			
			pointChkService.insertCmsPointModify(request, searchBean, cmsPointModify, resultJson, balanceJson);
			
			ModelMap map = new ModelMap();
			map.put("result", resultJson);
			return JSONValue.toJSONString(map);
		}
		
		/*
		 * 상용 테스터 등록
		 */
		@RequestMapping(value = "/manage/prod_test.do")
		public ModelAndView prod_test(HttpServletRequest request, @RequestParam(value = "pg", required = false) String page, ModelMap map) throws java.lang.Exception {

			String pg = page;
			// 페이징 관련
			if (pg == null || pg == ""){
				pg = "1";
			}
			int endNum = Integer.parseInt(pg) * PAGE_SIZE;
			int startNum = endNum - ( PAGE_SIZE - 1 ) ;
			map.put("endNum", endNum);
			map.put("startNum", startNum);

			// 페이징
			BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
			paging.setCurrentPage(Integer.parseInt(pg));

			List<ProdTester> list = pointChkService.getProdTestList();
			
			HttpSession session = request.getSession();
			session.setAttribute("testList", list);
			
			ModelAndView mav = new ModelAndView();
			mav.addObject("list", list);
			mav.addObject("menu1", "포인트관리");
			mav.addObject("menu2", "상용 테스터 등록");
			mav.addObject("display", "../clip/manage/pointChk/prod_test.jsp");
			mav.setViewName("/main/index");
			return mav;
			
		}
		
		@RequestMapping(value ="/manage/prod_add_tester.do") 
		@ResponseBody
		public String prod_tester_add(ProdTester protester, HttpServletRequest request, ModelMap model)  throws java.lang.Exception {
			
			ModelMap map = new ModelMap();
			
			String active = protester.getActive();
			String ctn = protester.getCtn();
			String user_ci = protester.getUser_ci();
			
			if (active == null || ctn == null || user_ci == null || active.isEmpty() || ctn.isEmpty() || user_ci.isEmpty()) {
				map.put("result", "fail");	
				return JSONValue.toJSONString(map);
			}else {
				
				int result = pointChkService.addTester(protester);
				System.out.println("result : "+result);
				map.put("result", "success");	
				return JSONValue.toJSONString(map);
				
			}

		}
		
		@SuppressWarnings("unchecked")
		@RequestMapping(value ="/manage/prod_del_tester.do") 
		@ResponseBody
		public String prod_tester_del(@RequestParam(value = "key", required = true) String key, HttpServletRequest request, ModelMap model)  throws java.lang.Exception {
			
			ModelMap map = new ModelMap();
			
			HttpSession session = request.getSession();
			List<ProdTester> list = (List<ProdTester>) session.getAttribute("testList");
			
			if (list == null) {
				map.put("result", "fail");	
				return JSONValue.toJSONString(map);
			}
			
			int result = pointChkService.removeTesterList(list.get(Integer.parseInt(key)-1).getUser_ci());
			System.out.println("result : "+result);
			map.put("result", "success");
			map.put("id", key);
			return JSONValue.toJSONString(map);
			
		}
		
		@SuppressWarnings("unchecked")
		@RequestMapping(value ="/manage/prod_active_tester.do") 
		@ResponseBody
		public String prod_tester_active(@RequestParam(value = "key", required = true) String key,  @RequestParam(value = "state", required = true) String state, HttpServletRequest request, ModelMap model)  throws java.lang.Exception {			

			ModelMap map = new ModelMap();
			
			HttpSession session = request.getSession();
			List<ProdTester> list = (List<ProdTester>) session.getAttribute("testList");
			
			if (list == null) {
				map.put("result", "fail");	
				return JSONValue.toJSONString(map);
			}else {
				
				if (state.equals("1")) {
					state = "0";
					map.put("state", "0");
					map.put("msg", "비활성화");
				}else if (state.equals("0")) {
					state = "1";
					map.put("state", "1");
					map.put("msg", "활성화");
				}
				
				int result = pointChkService.setTesterState(new ProdTester(list.get(Integer.parseInt(key)-1).getUser_ci(), state));
				System.out.println("result : "+result);
				map.put("result", "success");
				return JSONValue.toJSONString(map);
				
			}

		}
		
}