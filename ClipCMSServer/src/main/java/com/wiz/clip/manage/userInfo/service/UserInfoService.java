/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.userInfo.service;

import java.util.List;

import com.wiz.clip.manage.userInfo.bean.SearchBean;
import com.wiz.clip.manage.userInfo.bean.UserInfoTbl;
import com.wiz.clip.manage.userInfo.bean.UserInfoTblLog;

public interface UserInfoService {

	int getUserInfoCnt(SearchBean searchBean)  throws java.lang.Exception ;
	
	List<UserInfoTbl> getUserInfoList(SearchBean searchBean)  throws java.lang.Exception ;

	UserInfoTbl getUserInfo(SearchBean searchBean)  throws java.lang.Exception ;

	List<UserInfoTblLog> getUserLogList(SearchBean searchBean) throws java.lang.Exception ;

	int getUserLogCnt(SearchBean searchBean) throws java.lang.Exception ;
}
