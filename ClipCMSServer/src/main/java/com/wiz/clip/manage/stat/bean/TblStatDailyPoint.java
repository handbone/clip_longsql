/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.stat.bean;

public class TblStatDailyPoint  {
	
	private String year;
	private String month;
	private String day;
	private String d_day;
	private String total_balance;
	private String input_point;
	private String output_point;
	private String remove_point;
	private String pre_point;
	private String input_count;
	private String output_count;
	private String remove_count;
	private String pre_count;
	private String active_user;
	
private String sdate;
	
	public String getSdate() {
		return sdate;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getD_day() {
		return d_day;
	}
	public void setD_day(String d_day) {
		this.d_day = d_day;
	}
	public String getTotal_balance() {
		return total_balance;
	}
	public void setTotal_balance(String total_balance) {
		this.total_balance = total_balance;
	}
	public String getInput_point() {
		return input_point;
	}
	public void setInput_point(String input_point) {
		this.input_point = input_point;
	}
	public String getOutput_point() {
		return output_point;
	}
	public void setOutput_point(String output_point) {
		this.output_point = output_point;
	}
	public String getRemove_point() {
		return remove_point;
	}
	public void setRemove_point(String remove_point) {
		this.remove_point = remove_point;
	}
	public String getPre_point() {
		return pre_point;
	}
	public void setPre_point(String pre_point) {
		this.pre_point = pre_point;
	}
	public String getInput_count() {
		return input_count;
	}
	public void setInput_count(String input_count) {
		this.input_count = input_count;
	}
	public String getOutput_count() {
		return output_count;
	}
	public void setOutput_count(String output_count) {
		this.output_count = output_count;
	}
	public String getRemove_count() {
		return remove_count;
	}
	public void setRemove_count(String remove_count) {
		this.remove_count = remove_count;
	}
	public String getPre_count() {
		return pre_count;
	}
	public void setPre_count(String pre_count) {
		this.pre_count = pre_count;
	}
	public String getActive_user() {
		return active_user;
	}
	public void setActive_user(String active_user) {
		this.active_user = active_user;
	} 
	
	
	
	
	
}
