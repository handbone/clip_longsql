/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.stat.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.acl.bean.AclInfoTbl;
import com.wiz.clip.manage.stat.bean.SearchBean;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint2;
import com.wiz.clip.manage.stat.bean.TblStatMediaPoint;
import com.wiz.clip.manage.stat.bean.TblStatMonthlyPoint;
import com.wiz.clip.manage.stat.dao.StatDAO;

@Service
@Transactional
public class StatServiceImpl implements StatService {
	
	@Autowired
	private StatDAO statDao;

	Logger logger = Logger.getLogger(StatServiceImpl.class.getName());

	@Override
	public List<AclInfoTbl> getRequesterNameList(ModelMap map) {
		return statDao.getRequesterNameList(map);
	}

	@Override
	public int getStatLogMediaCnt(SearchBean searchBean) {
		return statDao.getStatLogMediaCnt(searchBean);
	}

	@Override
	public int getStatLogDailyCnt(SearchBean searchBean) {
		return statDao.getStatLogDailyCnt(searchBean);
	}

	@Override
	public int getStatLogMonthlyCnt(SearchBean searchBean) {
		return statDao.getStatLogMonthlyCnt(searchBean);
	}

	@Override
	public List<TblStatMediaPoint> statLogMedia(SearchBean searchBean) {
		return statDao.statLogMedia(searchBean);
	}

	@Override
	public List<TblStatDailyPoint> statLogDaily(SearchBean searchBean) {
		return statDao.statLogDaily(searchBean);
	}

	@Override
	public List<TblStatMonthlyPoint> statLogMonthly(SearchBean searchBean) {
		return statDao.statLogMonthly(searchBean);
	}

	@Override
	public List<TblStatDailyPoint2> statLogDaily2(SearchBean searchBean) {
		return statDao.statLogDaily2(searchBean);
	}

	@Override
	public int getStatLogDailyCnt2(SearchBean searchBean) {
		return statDao.getStatLogDailyCnt2(searchBean);
	}
	
	

}
