/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.schedule.service;

public interface ScheduleService {

	void makeDailyStat();

	void makeMediaStat();

	void makeDailyStat(String preDate);
	
	void makeMediaStat(String curDate);

	void deleteTblStatDailyPoint(String curDate);

	void deleteTblStatMediaPoint(String curDate);

	void makePointRouletteJoinSum();
	
	void makePointRouletteJoinSum(String curDate);

	void falsePointChk();

	void makeMonthlyStat();

	void deleteTblStatMonthlyPoint(String curDate);

	void makeMonthlyStat(String curDate, String nextDate);

	void makePointRouletteConnLogSum();
	
	int makePointRouletteConnLogSum(String curDate);

	String makeTop30(String curDate, int limitCount, String POINT_MODIFY_ACL_REQUEST_CODE);
	
	void makeTop30(String POINT_MODIFY_ACL_REQUEST_CODE);

	String makeTop30_2(String curDate, int limitCount, String POINT_MODIFY_ACL_REQUEST_CODE);
	
	void makeTop30_2(String POINT_MODIFY_ACL_REQUEST_CODE);
	
	String makeAnomaly(String curDate, String curTime, int limitPoint, String POINT_MODIFY_ACL_REQUEST_CODE) throws java.lang.Exception;

	void makeAnomaly(String POINT_MODIFY_ACL_REQUEST_CODE);
	
	String makeAnomaly2(String curDate, String curTime, int limitPoint, String POINT_MODIFY_ACL_REQUEST_CODE);

	void makeAnomaly2(String POINT_MODIFY_ACL_REQUEST_CODE);

	void makeDailyStat2(String POINT_MODIFY_ACL_REQUEST_CODE);

	void deleteTblStatDailyPoint2(String preDate, String POINT_MODIFY_ACL_REQUEST_CODE);

	void makeDailyStat2(String preDate, String POINT_MODIFY_ACL_REQUEST_CODE);

	void point_roulette_join_log();

	String point_roulette_join_log(String preDate);

	void statDayModify(String preDate);

	void updateAclStatus() throws java.lang.Exception;

	void makeDailyStat_edit() throws java.lang.Exception;

	void makeDailyStat2_edit(String pOINT_MODIFY_ACL_REQUEST_CODE) throws java.lang.Exception;

	

}
