/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.schedule.service;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.wiz.clip.common.sendAPI.HttpNetwork;
import com.wiz.clip.log.service.PvLogService;
import com.wiz.clip.manage.pointChk.bean.TblStatAnomalyDetection;
import com.wiz.clip.manage.pointChk.bean.TblStatReceiverInfo;
import com.wiz.clip.manage.pointChk.service.PointChkService;
import com.wiz.clip.manage.stat.bean.SearchBean;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint;
import com.wiz.clip.manage.stat.service.StatService;
import com.wiz.clip.roulette.channel.bean.PointRoulette;
import com.wiz.clip.roulette.viewLog.bean.PointRouletteJoin;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;
import com.wiz.clip.schedule.bean.PointHistVo;
import com.wiz.clip.schedule.bean.PointInfoVo;
import com.wiz.clip.schedule.dao.ScheduleServiceDAO;
import com.wiz.clip.schedule.sendSMS.sendSMS;

@Service("scheduleService")
public class ScheduleServiceImpl implements ScheduleService {
	
	Logger logger = Logger.getLogger(ScheduleServiceImpl.class.getName());
	
	@Autowired
	private StatService statService;
	
	@Autowired
	private PointChkService pointChkService;
	
	@Autowired
	private PvLogService pvLogService;

	@Autowired
	private sendSMS sendSMS;
	
	@SuppressWarnings("unused")
	@Autowired
	private HttpNetwork httpNetwork;
	
	@Resource(name="scheduleServiceDao")
	private ScheduleServiceDAO scheduleServiceDao;
	
	@Value("#{config['MSG_CONTENT_YN']}") String MSG_CONTENT_YN;
	
	@Override
	public void makeDailyStat() {

		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        format.applyPattern("yyyyMMdd");
	    String preDate = format.format(calendar.getTime());  
	    
	   /* calendar.add(Calendar.DAY_OF_MONTH, +1);
        String nextDate = format.format(calendar.getTime());*/
	    
       scheduleServiceDao.makeDailyStat(preDate);
        
       statDayModify(preDate);
        
	}

	@Override
	public void makeDailyStat_edit() throws Exception {
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, -8);
        format.applyPattern("yyyyMMdd");
	    String preDate = format.format(calendar.getTime());  
	    
	    String lastDate = "20180313";
	    Date last = format.parse(lastDate);
	    Date pre = calendar.getTime();
	    
	    int compare = pre.compareTo(last);
	    if(compare > 0) {
	    		logger.debug("makDailyStat_edit :: END");
	    		return;
	    }
//	    calendar.add(Calendar.DAY_OF_MONTH, +1);
//      String nextDate = format.format(calendar.getTime());
	    
       scheduleServiceDao.makeDailyStat(preDate);
        
       statDayModify(preDate);
	}

	@Override
	public void makeMediaStat() {
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        format.applyPattern("yyyyMMdd");
        
		scheduleServiceDao.makeMediaStat(format.format(calendar.getTime()));
		
	}

	
	
	@Override
	public void makeDailyStat(String preDate) {
		scheduleServiceDao.makeDailyStat(preDate);
		statDayModify(preDate);
	}
	
	@Override
	public void statDayModify(String preDate) {

		SearchBean searchBean = new SearchBean();
		
		Calendar calendar = Calendar.getInstance();
	    SimpleDateFormat format = new SimpleDateFormat();
	    calendar.set ( Integer.parseInt(preDate.substring(0,4)), Integer.parseInt(preDate.substring(4,6)) -1 , Integer.parseInt(preDate.substring(6,8)) );
	    calendar.add(Calendar.DAY_OF_MONTH, -1);
	    format.applyPattern("yyyyMMdd");
	    
		searchBean.setStartdate(format.format(calendar.getTime()));
		searchBean.setEnddate(preDate);
		
		searchBean.setStartNum(0);
		searchBean.setEndNum(10);
		
		// 이틀전과 어제의 데이터를 가져옴
		List<TblStatDailyPoint> daylist = statService.statLogDaily(searchBean);
		
		// 차액을 비교함
		long compare = 0;
		if(daylist.size() == 2){
			compare = Long.parseLong(daylist.get(0).getTotal_balance()) 
			+ Long.parseLong(daylist.get(1).getInput_point()) 
			- Long.parseLong(daylist.get(1).getOutput_point())
			- Long.parseLong(daylist.get(1).getTotal_balance())
			- Long.parseLong(daylist.get(1).getRemove_point());
		}
		
		// 포인트 조정
		if(compare != 0){
			/*
			 * 특정 user_ci를 이용해서, 어제날짜로, balance가 항상 0인 입력기록을 만들어준다. 
			 * 
			 * pluspoint API를 이용하면 어제 날짜로 입력 할 수 없다.
			 * 또한 balance 값을 0으로 만들 수 없다. 
			 * 그래서 pluspoint API를 이용하지 못하고 직접 DB에 입력한다.
			 * try {
				httpNetwork.sendPlusPoint("testUserCi20170322", "pointUpdate", String.valueOf(compare)
						, preDate + "balance Modify" );
			} catch (Exception e) {
				e.printStackTrace();
			}*/
			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put("user_ci","accountBalanceUserCi");
			String tr_id = String.valueOf(System.currentTimeMillis());
			StringBuffer sb2 = new StringBuffer(tr_id);
			if (tr_id.length() < 16) {
				Random rand = new Random(System.currentTimeMillis()); 
				sb2.append(Math.abs(rand.nextInt(999)));
			}
			if (tr_id.length() < 16) {
				while (sb2.length() < 16) {
					sb2.append("0");
				}
			}
			tr_id = sb2.toString();
			parameters.put("transaction_id",tr_id);
			compare = compare * (-1);
			parameters.put("point_value",String.valueOf(compare));
			parameters.put("reg_source","accountUpdate");
			parameters.put("description",preDate + " Modify");
			parameters.put("acl_limit_yn","N");
			parameters.put("acl_limit_cnt","0");
			parameters.put("status","D");
			parameters.put("reg_date",preDate);
			parameters.put("reg_time","000001");
			parameters.put("cng_date",preDate);
			parameters.put("cng_time","000001");
			
			PointHistVo phv = new PointHistVo();
			phv.setUserCi(parameters.get("user_ci"));
			phv.setTransactionId(parameters.get("transaction_id"));
			phv.setPointType("I");
			phv.setPointValue(Long.valueOf(parameters.get("point_value")));
			phv.setRegSource(parameters.get("reg_source"));
			phv.setDescription(parameters.get("description"));
			phv.setAcl_limit_yn(parameters.get("acl_limit_yn"));
			phv.setAcl_limit_cnt(Integer.parseInt(parameters.get("acl_limit_cnt")));
			phv.setStatus(parameters.get("status"));
			phv.setRegDate(parameters.get("reg_date"));
			phv.setRegTime(parameters.get("reg_time"));
			phv.setCngDate(parameters.get("cng_date"));
			phv.setCngTime(parameters.get("cng_time"));
			
			if(scheduleServiceDao.insertPointHist(phv) > 0){
				phv = scheduleServiceDao.getPointHistByTrId(tr_id);
				
				PointInfoVo piv = new PointInfoVo();
				piv.setUserCi(phv.getUserCi());
				piv.setPointHistIdx(phv.getPointHistIdx());
				piv.setPointType(phv.getPointType());
				piv.setPointValue(phv.getPointValue());
				piv.setBalance(0);
				piv.setDescription(phv.getDescription());
				piv.setRegSource(phv.getRegSource());
				piv.setRegDate(parameters.get("reg_date"));
				piv.setRegTime(parameters.get("reg_time"));
				
				if(scheduleServiceDao.insertPointInfo(piv) > 0){
					// 오늘 날짜의 적립포인트값을 조정함
					Map<String, String> parameters2 = new HashMap<String, String>();
					parameters2.put("regDate",preDate);
					parameters2.put("compare",String.valueOf(compare));
					scheduleServiceDao.updateTblStatDailyPoint(parameters2);
				}
			}
			
		}
	}
	
	@Override
	public void makeMediaStat(String curDate) {
		scheduleServiceDao.makeMediaStat(curDate);
		
		
	}

	@Override
	public void deleteTblStatDailyPoint(String curDate) {
		scheduleServiceDao.deleteTblStatDailyPoint(curDate);
		
	}

	@Override
	public void deleteTblStatMediaPoint(String curDate) {
		scheduleServiceDao.deleteTblStatMediaPoint(curDate);
		
	}

	@Override
	public void makePointRouletteJoinSum() {
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, +1);
        format.applyPattern("yyyyMMdd");
	        
        //System.out.println(format.format(calendar.getTime()));
        
        // 내일 날짜가 기간안에 존재하는 룰렛을 모두 가져온다.
        List<PointRoulette> rouletteIdArray = (List<PointRoulette>)scheduleServiceDao.chkPointRoulette(format.format(calendar.getTime()));

        // 하나하나 체크하면서 만들어준다.
        for(int i = 0 ; i < rouletteIdArray.size() ; i++){
        	
        	//PointRouletteJoinSum 에 내일 합계표가 없으면 만든다.
        	PointRoulette pointRoulette = new PointRoulette();
    		pointRoulette.setRoulette_id(rouletteIdArray.get(i).getRoulette_id());
    		pointRoulette.setCurDate(format.format(calendar.getTime()));
    		
    		// 내일 합계표 카운트
    		int joinSumCnt = scheduleServiceDao.selectPointRouletteJoinSumByDate(pointRoulette);
    		if( 0 == joinSumCnt){
    			scheduleServiceDao.insertPointRouletteJoinSum(pointRoulette);
    		}
    	}
        
        
	}

	@Override
	public void makePointRouletteJoinSum(String curDate) {

		// 내일 날짜가 기간안에 존재하는 룰렛을 모두 가져온다.
        List<PointRoulette> rouletteIdArray = (List<PointRoulette>)scheduleServiceDao.chkPointRoulette(curDate);

        // 하나하나 체크하면서 만들어준다.
        for(int i = 0 ; i < rouletteIdArray.size() ; i++){
        	
        	//PointRouletteJoinSum 에 내일 합계표가 없으면 만든다.
        	PointRoulette pointRoulette = new PointRoulette();
    		pointRoulette.setRoulette_id(rouletteIdArray.get(i).getRoulette_id());
    		pointRoulette.setCurDate(curDate);
    		
    		// 내일 합계표 카운트
    		int joinSumCnt = scheduleServiceDao.selectPointRouletteJoinSumByDate(pointRoulette);
    		
    		if( 0 == joinSumCnt){
    			scheduleServiceDao.insertPointRouletteJoinSum(pointRoulette);
    		}
    	}
        
        
	}

	@Override
	public void falsePointChk() {
		//Calendar calendar = Calendar.getInstance();
        //SimpleDateFormat format = new SimpleDateFormat();
        //format.applyPattern("yyyyMMdd");
	        
        //System.out.println(format.format(calendar.getTime()));
        //List<PointRoulette> pointRoulette = new ArrayList<PointRoulette>();
        
        //pointRoulette = scheduleServiceDao.falsePointChk();
		
        //for( PointRoulette selected : pointRoulette){
        	//System.out.println("getRoulette_id:"+selected.getRoulette_id());
        	//System.out.println("getTotal:"+selected.getTotal());
        	//System.out.println("getCurrTotal:"+selected.getCurrTotal());
        //}

	}

	@Override
	public void makeMonthlyStat() {
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.MONTH, -1);
        format.applyPattern("yyyyMM");
	        
        String curDate = format.format(calendar.getTime());
        
        calendar.add(Calendar.MONTH, +1);
        String nextDate = format.format(calendar.getTime());
        
        scheduleServiceDao.makeMonthlyStat(curDate, nextDate);
		
	}
	
	@Override
	public void makeMonthlyStat(String curDate, String nextDate) {
		scheduleServiceDao.makeMonthlyStat(curDate, nextDate);
		
	}
	

	@Override
	public void deleteTblStatMonthlyPoint(String curDate) {
		scheduleServiceDao.deleteTblStatMonthlyPoint(curDate);
		
	}

	
	@Override
	public void makePointRouletteConnLogSum() {
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyyMMdd");
	    
        String curDate = format.format(calendar.getTime());
        
		// 어제 통계를 가져온다
		List<ViewLogBean> pointRouletteConnLogSumArray = (List<ViewLogBean>)scheduleServiceDao.selectPointRouletteConnLogSum(curDate);

        // 하나하나 체크하면서 입력해준다.
        for(int i = 0 ; i < pointRouletteConnLogSumArray.size() ; i++){
        	
        	//PointRouletteConnLogSum 에 입력
        	ViewLogBean viewLogBean = new ViewLogBean();
        	viewLogBean.setRoulette_id(pointRouletteConnLogSumArray.get(i).getRoulette_id());
        	viewLogBean.setSdate(pointRouletteConnLogSumArray.get(i).getSdate());
        	viewLogBean.setPageView(pointRouletteConnLogSumArray.get(i).getPageView());
        	
   			scheduleServiceDao.insertPointRouletteConnLogSum(viewLogBean);
   			
   		}
        
        
	}

	@Override
	public int makePointRouletteConnLogSum(String curDate) {

		int resultCnt = 0;
		
		// 수동에서는 일단 해당 날짜(1일전)의 통계를 삭제부터 한다.
		scheduleServiceDao.deletePointRouletteConnLogSum(curDate);
		
		// 어제 통계를 가져온다
		List<ViewLogBean> pointRouletteConnLogSumArray = (List<ViewLogBean>)scheduleServiceDao.selectPointRouletteConnLogSum(curDate);

        // 하나하나 체크하면서 입력해준다.
        for(int i = 0 ; i < pointRouletteConnLogSumArray.size() ; i++){
        	
        	//PointRouletteConnLogSum 에 입력
        	ViewLogBean viewLogBean = new ViewLogBean();
        	viewLogBean.setRoulette_id(pointRouletteConnLogSumArray.get(i).getRoulette_id());
        	viewLogBean.setSdate(pointRouletteConnLogSumArray.get(i).getSdate());
        	viewLogBean.setPageView(pointRouletteConnLogSumArray.get(i).getPageView());
        	
   			scheduleServiceDao.insertPointRouletteConnLogSum(viewLogBean);
   			
   			resultCnt = resultCnt + 1;
   			
   		}
        
        return resultCnt;
        
	}

	@Override
	public void makeTop30(String POINT_MODIFY_ACL_REQUEST_CODE) {
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        format.applyPattern("yyyyMMdd");
	        
		scheduleServiceDao.makeTop30(format.format(calendar.getTime()) , 30, POINT_MODIFY_ACL_REQUEST_CODE);
	}
	
	@Override
	public String makeTop30(String curDate, int limitCount, String POINT_MODIFY_ACL_REQUEST_CODE) {
		
        // 수동에서는 일단 해당 날짜의 통계를 삭제부터 한다.
     	scheduleServiceDao.deleteTbl_stat_top_user(curDate);
     		
		return scheduleServiceDao.makeTop30(curDate , limitCount, POINT_MODIFY_ACL_REQUEST_CODE);
	}

	@Override
	public void makeTop30_2(String POINT_MODIFY_ACL_REQUEST_CODE) {
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        format.applyPattern("yyyyMMdd");
	        
		scheduleServiceDao.makeTop30_2(format.format(calendar.getTime()) , 30, POINT_MODIFY_ACL_REQUEST_CODE);
	}
	
	@Override
	public String makeTop30_2(String curDate, int limitCount, String POINT_MODIFY_ACL_REQUEST_CODE) {
		
        // 수동에서는 일단 해당 날짜의 통계를 삭제부터 한다.
     	scheduleServiceDao.deleteTbl_stat_top_user_2(curDate);
     		
		return scheduleServiceDao.makeTop30_2(curDate , limitCount, POINT_MODIFY_ACL_REQUEST_CODE);
	}
	
	@Override
	public String makeAnomaly(String curDate, String curTime, int limitPoint, String POINT_MODIFY_ACL_REQUEST_CODE) throws java.lang.Exception {
		
		// 수동에서는 일단 해당 날짜 및 시간의 통계를 삭제부터 한다.
     	scheduleServiceDao.deleteTbl_stat_anomaly_detection(curDate, curTime, limitPoint);
     	
     	// 시작
     	String result = scheduleServiceDao.makeAnomaly(curDate , curTime, limitPoint, POINT_MODIFY_ACL_REQUEST_CODE);
     	
     	ModelMap model = new ModelMap();
     	
     	model.put("curDate", curDate);
		model.put("curTime", curTime);
		
		List<TblStatAnomalyDetection> tblStatAnomalyDetectionTopSMS = pointChkService.tblStatAnomalyDetectionTopSMS(model);
		
//		기준초과자 데이터가 몇개인지 확인	
		if(tblStatAnomalyDetectionTopSMS.size() > 0) {
//			System.out.println("============================== 값이 존재함 SMS 발송합니다. ==============================");
			for(int i=0 ; i<tblStatAnomalyDetectionTopSMS.size() ; i++) {
				model.clear();
				
				if(Integer.parseInt(tblStatAnomalyDetectionTopSMS.get(i).getLimit_input_count().toString()) > 0) {
					// msg_content 를 제외한 나머지 변수 값들이 자주 변동이 될 경우 프로퍼티로 빼서 관리할 것
					String username = "자동발송";
					String msg_content = "[" + tblStatAnomalyDetectionTopSMS.get(i).getMedia().toString() + "] " + curDate + ":" + curTime + "시 " 
									+ tblStatAnomalyDetectionTopSMS.get(i).getLimit_input_count().toString() + "명 " 
									+ limitPoint + "포인트 초과 적립자 발생";
					String call_ctn = "01030943326";
					String success_yn = "Y";
					
					model.put("username", username);
					model.put("msg_content", msg_content);
					model.put("call_ctn", call_ctn);
	
					List<TblStatReceiverInfo> list = pointChkService.getTblStatReceiverInfo();
					
					for(int j=0 ; j<list.size() ; j++) {
						model.put("rcv_ctn", list.get(j).getReceiver_tel_no().replaceAll("-", ""));
						String response = sendSMS.sendSMS(model);
						
						if(response.indexOf("Success") > -1) {
							success_yn = "Y";
						} else {
							success_yn = "N";
						}
						
						String send_userid = "자동발송";
						
						// request 값으로 ip 주소 구하는 법
	//					String ipaddr = request.getHeader("X-FORWARDED-FOR");
	//					if (ipaddr == null) {
	//						ipaddr = request.getRemoteAddr();
	//					}
						
						String ipaddr = null;
						try {
	
							boolean isLoopBack = true;
							Enumeration<NetworkInterface> en;		
							en = NetworkInterface.getNetworkInterfaces();
	
				 			while(en.hasMoreElements()) {
								NetworkInterface ni = en.nextElement();
								if (ni.isLoopback())
									continue;
	
								Enumeration<InetAddress> inetAddresses = ni.getInetAddresses();
								while(inetAddresses.hasMoreElements()) { 
									InetAddress ia = inetAddresses.nextElement();
									if (ia.getHostAddress() != null && ia.getHostAddress().indexOf(".") != -1) {
										ipaddr = ia.getHostAddress();
	//									System.out.println(ipaddr);
										isLoopBack = false;
										break;
									}
								}
								if (!isLoopBack)
									break;
							}
	//						System.out.println(" IP  =   " + ipaddr);
						} catch (SocketException e) {
							e.printStackTrace();
						}
						
						model.put("send_userid", send_userid);
						model.put("response", response);
						model.put("ipaddr", ipaddr);
						model.put("success_yn", success_yn);
						
						pvLogService.insertSMSLog(model);
						
					}
				}
				
				
				if(Integer.parseInt(tblStatAnomalyDetectionTopSMS.get(i).getLimit_output_count().toString()) > 0) {
					// msg_content 를 제외한 나머지 변수 값들이 자주 변동이 될 경우 프로퍼티로 빼서 관리할 것
					String username = "자동발송";
					String msg_content = "[" + tblStatAnomalyDetectionTopSMS.get(i).getMedia().toString() + "] " + curDate + ":" + curTime + "시 " 
									+ tblStatAnomalyDetectionTopSMS.get(i).getLimit_output_count().toString() + "명 " 
									+ limitPoint + "포인트 초과 사용자 발생";
					String call_ctn = "01030943326";
					String success_yn = "Y";
					
					model.put("username", username);
					model.put("msg_content", msg_content);
					model.put("call_ctn", call_ctn);
	
					List<TblStatReceiverInfo> list = pointChkService.getTblStatReceiverInfo();
					
					for(int j=0 ; j<list.size() ; j++) {
						model.put("rcv_ctn", list.get(j).getReceiver_tel_no().replaceAll("-", ""));
						String response = sendSMS.sendSMS(model);
						
						if(response.indexOf("Success") > -1) {
							success_yn = "Y";
						} else {
							success_yn = "N";
						}
						
						String send_userid = "자동발송";
						
						// request 값으로 ip 주소 구하는 법
	//					String ipaddr = request.getHeader("X-FORWARDED-FOR");
	//					if (ipaddr == null) {
	//						ipaddr = request.getRemoteAddr();
	//					}
						
						String ipaddr = null;
						try {
	
							boolean isLoopBack = true;
							Enumeration<NetworkInterface> en;		
							en = NetworkInterface.getNetworkInterfaces();
	
				 			while(en.hasMoreElements()) {
								NetworkInterface ni = en.nextElement();
								if (ni.isLoopback())
									continue;
	
								Enumeration<InetAddress> inetAddresses = ni.getInetAddresses();
								while(inetAddresses.hasMoreElements()) { 
									InetAddress ia = inetAddresses.nextElement();
									if (ia.getHostAddress() != null && ia.getHostAddress().indexOf(".") != -1) {
										ipaddr = ia.getHostAddress();
	//									System.out.println(ipaddr);
										isLoopBack = false;
										break;
									}
								}
								if (!isLoopBack)
									break;
							}
	//						System.out.println(" IP  =   " + ipaddr);
						} catch (SocketException e) {
							e.printStackTrace();
						}
						
						model.put("send_userid", send_userid);
						model.put("response", response);
						model.put("ipaddr", ipaddr);
						model.put("success_yn", success_yn);
						
						pvLogService.insertSMSLog(model);
						
					}
				}
				
			}
		}
		
		
     	
     	return result;
	}
	
	@Override
	public void makeAnomaly(String POINT_MODIFY_ACL_REQUEST_CODE) {
		
//		HttpServletRequest request 
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyyMMdd");
		//System.out.println(format.format(calendar.getTime()));
		String curDate = format.format(calendar.getTime());
		
		format.applyPattern("HH");
		calendar.add(Calendar.HOUR_OF_DAY, -1);
		String curTime = format.format(calendar.getTime());
		
		int limitPoint = Integer.parseInt(scheduleServiceDao.getCmsCommonCode("limitPoint"));
		
     	try {
			makeAnomaly(curDate, curTime, limitPoint, POINT_MODIFY_ACL_REQUEST_CODE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//			System.out.println(curDate + ":" + curTime + "시에 스케줄러 오류 발생");
		}
	}
	
	
	@Override
	public String makeAnomaly2(String curDate, String curTime, int limitPoint, String POINT_MODIFY_ACL_REQUEST_CODE) {
		
		// 수동에서는 일단 해당 날짜 및 시간의 통계를 삭제부터 한다.
     	scheduleServiceDao.deleteTbl_stat_anomaly_detection2(curDate, curTime, limitPoint);
     	return scheduleServiceDao.makeAnomaly2(curDate , curTime, limitPoint, POINT_MODIFY_ACL_REQUEST_CODE);
	}
	
	@Override
	public void makeAnomaly2(String POINT_MODIFY_ACL_REQUEST_CODE) {
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyyMMdd");
		//System.out.println(format.format(calendar.getTime()));
		String curDate = format.format(calendar.getTime());
		
		format.applyPattern("HH");
		//System.out.println(format.format(calendar.getTime()));
		calendar.add(Calendar.HOUR_OF_DAY, -1);
		String curTime = format.format(calendar.getTime());
		
		int limitPoint = Integer.parseInt(scheduleServiceDao.getCmsCommonCode("limitPoint"));
		
     	scheduleServiceDao.makeAnomaly2(curDate , curTime, limitPoint, POINT_MODIFY_ACL_REQUEST_CODE);
	}

	@Override
	public void makeDailyStat2(String POINT_MODIFY_ACL_REQUEST_CODE) {
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        format.applyPattern("yyyyMMdd");
	    String preDate = format.format(calendar.getTime());  
	    
	   /* calendar.add(Calendar.DAY_OF_MONTH, +1);
        String nextDate = format.format(calendar.getTime());*/
	    
       scheduleServiceDao.makeDailyStat2(preDate,POINT_MODIFY_ACL_REQUEST_CODE);
    }

	@Override
	public void makeDailyStat2_edit(String POINT_MODIFY_ACL_REQUEST_CODE) throws Exception {
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, -56);
        format.applyPattern("yyyyMMdd");
	    String preDate = format.format(calendar.getTime());  
	    
	    String lastDate = "20180315";
	    Date last = format.parse(lastDate);
	    Date pre = calendar.getTime();
	    
	    int compare = pre.compareTo(last);
	    if(compare > 0) {
	    		logger.debug("makDailyStat2_edit :: END");
	    		return;
	    }
	    
	   /* calendar.add(Calendar.DAY_OF_MONTH, +1);
        String nextDate = format.format(calendar.getTime());*/
	    
       scheduleServiceDao.makeDailyStat2(preDate,POINT_MODIFY_ACL_REQUEST_CODE);
    }
	
	@Override
	public void deleteTblStatDailyPoint2(String preDate, String POINT_MODIFY_ACL_REQUEST_CODE) {
		scheduleServiceDao.deleteTblStatDailyPoint2(preDate, POINT_MODIFY_ACL_REQUEST_CODE);
	}

	@Override
	public void makeDailyStat2(String preDate, String POINT_MODIFY_ACL_REQUEST_CODE) {
		scheduleServiceDao.makeDailyStat2(preDate, POINT_MODIFY_ACL_REQUEST_CODE);
	}

	@Override
	public void point_roulette_join_log() {
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        format.applyPattern("yyyyMMdd");
	    
        String preDate = format.format(calendar.getTime());
		
		// point_roulette_join 테이블에서 3일전 데이터를 삭제한다. ( 6일이라면 3,4,5,6일만 남겨두고 이전 삭제  ) 
		scheduleServiceDao.point_roulette_join_delete_pre_3days(preDate);
		
		// point_roulette_join_log 에 하루 전 데이터를 삭제
		scheduleServiceDao.point_roulette_join_log_delete_pre_1day(preDate);
		
		// point_roulette_join 테이블에서 하루 전 데이터를 선택
		List<PointRouletteJoin> list = (List<PointRouletteJoin>)scheduleServiceDao.point_roulette_join_select_pre_1day(preDate);
		
		// point_roulette_join_log 에 하루 전 데이터를 복사
		for(int i = 0; i < list.size() ; i++ ){
			scheduleServiceDao.insert_point_roulette_join_log(list.get(i));
		}
	}

	@Override
	@Transactional
	public String point_roulette_join_log(String preDate) {
		// point_roulette_join 테이블에서 3일전 데이터를 삭제한다. ( 6일이라면 3,4,5,6일만 남겨두고 이전 삭제  ) 
		scheduleServiceDao.point_roulette_join_delete_pre_3days(preDate);
		
		// point_roulette_join_log 에 하루 전 데이터를 삭제
		scheduleServiceDao.point_roulette_join_log_delete_pre_1day(preDate);
		
		// point_roulette_join 테이블에서 하루 전 데이터를 선택
		List<PointRouletteJoin> list = (List<PointRouletteJoin>)scheduleServiceDao.point_roulette_join_select_pre_1day(preDate);
		
		// point_roulette_join_log 에 하루 전 데이터를 복사
		for(int i = 0; i < list.size() ; i++ ){
			scheduleServiceDao.insert_point_roulette_join_log(list.get(i));
		}
		
		return String.valueOf(list.size());
	}

	@Override
	public void updateAclStatus() throws Exception {
		scheduleServiceDao.updateAclStatus();
	}
	
	
}