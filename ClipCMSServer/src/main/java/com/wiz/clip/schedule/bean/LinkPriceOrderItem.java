package com.wiz.clip.schedule.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkPriceOrderItem {
	
	private int idx;
	private int main_idx;
	private String day;
	private String time;
	
	private String user_token;

	private String trlog_id;	//	주문 고유번호
	private String yyyymmdd;	//	주문 일자
	private String hhmiss;		//	주문 시간
	private String m_id;		//	머천트 아이디
	private String o_cd;		//	주문 번호
	private String p_cd;		//	상품 코드
	private String p_nm;		//	상품명
	private String c_cd;		//	카테고리 코드
	private String it_cnt;		//	주문수량
	private String sales;		//	판매 합계금액
	private String commission;	//	어필 커미션
	private String user_id;		//	매체 사용자 정보 (click 주소를 통해 u_id 로 전달된 값)	<- user_token
	private String membership_id;	//	머천트의 주문자 정보
	private String remote_addr;	//	사용자 PC 주소
	private String status;		// 100 (일반) / 300(취소신청) / 310(취소완료) / 200(정산대기)/ 210(정산완료 MER) / 220(정산완료 AFF)
								// 정산 처리 도중에 P : 처리중, C : 처리 완료 로 변경됨
	private String trans_comment;	//	실적 취소 사유(값이 있을 경우에만)
	
	private String resultMsg;
	
	public String getTrlog_id() {
		return trlog_id;
	}
	public void setTrlog_id(String trlog_id) {
		this.trlog_id = trlog_id;
	}
	public String getYyyymmdd() {
		return yyyymmdd;
	}
	public void setYyyymmdd(String yyyymmdd) {
		this.yyyymmdd = yyyymmdd;
		this.day = yyyymmdd;
	}
	public String getHhmiss() {
		return hhmiss;
	}
	public void setHhmiss(String hhmiss) {
		this.hhmiss = hhmiss;
		this.time = hhmiss;
	}
	public String getM_id() {
		return m_id;
	}
	public void setM_id(String m_id) {
		this.m_id = m_id;
	}
	public String getO_cd() {
		return o_cd;
	}
	public void setO_cd(String o_cd) {
		this.o_cd = o_cd;
	}
	public String getP_cd() {
		return p_cd;
	}
	public void setP_cd(String p_cd) {
		this.p_cd = p_cd;
	}
	public String getP_nm() {
		return p_nm;
	}
	public void setP_nm(String p_nm) {
		this.p_nm = p_nm;
	}
	public String getC_cd() {
		return c_cd;
	}
	public void setC_cd(String c_cd) {
		this.c_cd = c_cd;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getMembership_id() {
		return membership_id;
	}
	public void setMembership_id(String membership_id) {
		this.membership_id = membership_id;
	}
	public String getRemote_addr() {
		return remote_addr;
	}
	public void setRemote_addr(String remote_addr) {
		this.remote_addr = remote_addr;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTrans_comment() {
		return trans_comment;
	}
	public void setTrans_comment(String trans_comment) {
		this.trans_comment = trans_comment;
	}
	public String getUser_token() {
		return user_token;
	}
	public void setUser_token(String user_token) {
		this.user_token = user_token;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public int getMain_idx() {
		return main_idx;
	}
	public void setMain_idx(int main_idx) {
		this.main_idx = main_idx;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getIt_cnt() {
		return it_cnt;
	}
	public void setIt_cnt(String it_cnt) {
		this.it_cnt = it_cnt;
	}
	public String getSales() {
		return sales;
	}
	public void setSales(String sales) {
		this.sales = sales;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	
}
