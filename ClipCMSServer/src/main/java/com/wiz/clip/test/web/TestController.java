/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.test.web;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.clip3_0.service.Clip_3_0_Service;
import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.common.crypto.HmacCrypto;
import com.wiz.clip.test.dao.PushDAO;

@Controller
public class TestController  {
	
	Logger logger = Logger.getLogger(TestController.class.getName());

	@Value("#{config['PAGE_SIZE']}") int PAGE_SIZE;
	
	@Value("#{config['API_DOMAIN_SEND_PUSH']}")
	String API_DOMAIN_SEND_PUSH;
	
	@Value("#{config['API_DOMAIN_SEND_MESSAGE']}")
	String API_DOMAIN_SEND_MESSAGE;
	
	@Value("#{config['API_DOMAIN_SEND_TITLE']}")
	String API_DOMAIN_SEND_TITLE;

	@Value("#{config['API_DOMAIN_PUSH_PROD']}")
	String API_DOMAIN_PUSH_PROD;
	
	@Autowired
	public Clip_3_0_Service clip_3_0_service;
	/***************************************************************************************************
	 * 
	 * Page Controller
	 * 
	 */
	
	
	@RequestMapping(value = "/test/main.do", method = RequestMethod.GET)
	public ModelAndView main() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/test/index");
		mav.addObject("menu1", "테스트용");
		mav.addObject("menu2", "Push 테스트");
		mav.addObject("display", "../test/send_push.jsp");
		return mav;
	}
	
	@RequestMapping(value = "/test/send_push.do", method = RequestMethod.GET)
	public ModelAndView cardpoint_change_search() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/test/index");
		mav.addObject("menu1", "테스트용");
		mav.addObject("menu2", "Push 테스트");
		mav.addObject("display", "../test/send_push.jsp");
		return mav;
	}
	
	/***************************************************************************************************
	 * 
	 * Ajax Controller
	 * 
	 */
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/test/sendpush.do", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView TestSendPush(PushDAO obj , HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		
		logger.debug("################# getTYPE : "+obj.getTYPE());
		logger.debug("################# getID : "+obj.getID());
		logger.debug("################# getTITLE : "+obj.getTITLE());
		logger.debug("################# getCONTENT : "+obj.getCONTENT());
		logger.debug("################# getRANDINGURL : "+obj.getRANDINGURL());
		logger.debug("################# getAGREECHECKYN : "+obj.getAGREECHECKYN());
		logger.debug("################# getSENDSERVICETYPE : "+obj.getSENDSERVICETYPE());
		logger.debug("################# getLINKID : "+obj.getLINKID());
		logger.debug("################# getEXPIREDATE : "+obj.getEXPIREDATE());
		logger.debug("################# getIMGURL : "+obj.getIMGURL());
		
		
		@SuppressWarnings("unused")
		String result="";
		try{
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, 30);
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmss");
			String expireDate = format1.format(cal.getTime());
			System.out.println(expireDate);
			
			String date_type = obj.getTYPE();
			String data_id = obj.getID();
			String title = obj.getTITLE();
			String content = obj.getCONTENT();
			String agreeCheckYn	 = obj.getAGREECHECKYN();
			String sendServiceType	 = obj.getSENDSERVICETYPE();
			String linkId	 = obj.getLINKID();
			String sendexpireDate =AES256CipherClip.AES_Encode(expireDate);
			String imgUrl = obj.getIMGURL();
			String randingUrl = obj.getRANDINGURL();
			String signdata = agreeCheckYn + sendServiceType + linkId + date_type +AES256CipherClip.AES_Decode(data_id) + imgUrl + randingUrl + expireDate + title +content;
			signdata = signdata +"push" + "external" + "getPushInfo";
			String encSigndata = HmacCrypto.getHmacVal(signdata);
			
			JSONObject jObject = new JSONObject();
			jObject.put("type", date_type);
			jObject.put("id", data_id);
			jObject.put("title", title);
			jObject.put("content", content);
			jObject.put("randingUrl", randingUrl);
			jObject.put("imgUrl", imgUrl);
			jObject.put("agreeCheckYn", agreeCheckYn);
			jObject.put("sendServiceType", sendServiceType);
			jObject.put("linkId", linkId);
			jObject.put("expireDate", sendexpireDate);
			jObject.put("sign", encSigndata);
			
			logger.debug("################# sign : "+encSigndata);

			String params = jObject.toJSONString();
	
			
			if ("N".equals(API_DOMAIN_PUSH_PROD))   {
				result =	sendPushTest(params);
			} else {
				result =	sendPushHttps(params);
			}
			
			result =	sendPushTest(params);
		} catch (Exception e){
			logger.error("",e);
		}
		
//		ModelAndView mav = new ModelAndView("jsonView");
//		mav.addObject("result", result);
//		return mav;
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/test/index");
		mav.addObject("menu1", "테스트용");
		mav.addObject("menu2", "Push 테스트");
		mav.addObject("display", "../test/send_push.jsp");
		return mav;
	}
	
	public String sendPushTest(String params) throws java.lang.Exception {
		
		BufferedReader oBufReader = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = API_DOMAIN_SEND_PUSH;
		
		logger.debug("::::::::: "+strEncodeUrl+"?"+params);
		
		URL oOpenURL = new URL(strEncodeUrl);
		
		HttpURLConnection httpConn = (HttpURLConnection) oOpenURL.openConnection();

		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(5000);
		httpConn.setReadTimeout(10000);
		
		httpConn.setDoInput(true);
		httpConn.setRequestProperty("Accept", "application/json");
		httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		
		httpConn.setRequestProperty("Content-Length", String.valueOf(params.length()));
		OutputStream os = httpConn.getOutputStream();
		os.write(params.getBytes());
		os.flush();
		os.close();
		
		int responseCode = httpConn.getResponseCode();
		
		if( HttpURLConnection.HTTP_OK == responseCode) {
			logger.info("responseCode >>>> : " +responseCode);
			oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),"UTF-8"));
		} else {
			logger.info("responseCode etc >>>> : " +responseCode);
			oBufReader = new BufferedReader(new InputStreamReader(httpConn.getErrorStream(),"UTF-8"));
		}
	

		StringBuffer sb = new StringBuffer(strRslt);
		
		int i = 1;
		while ((strBuffer = oBufReader.readLine()) != null && (i < 2)) {
			if (strBuffer.length() > 1) {
				sb.append(strBuffer);
			}
		}
		logger.info("result :" + sb.toString());
		oBufReader.close();
		
		return sb.toString();
	}
	
	
	public String sendPushHttps(String params) throws java.lang.Exception {
		
		BufferedReader oBufReader = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = API_DOMAIN_SEND_PUSH;
		
		logger.debug("::::::::: "+strEncodeUrl+"?"+params);
		
		URL oOpenURL = new URL(strEncodeUrl);
		
		HttpsURLConnection httpConn = (HttpsURLConnection) oOpenURL.openConnection();
		HttpsURLConnection.setDefaultHostnameVerifier( new HostnameVerifier(){
		    public boolean verify(String string,SSLSession ssls) {
		     return true;
		    }
		});
		
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(5000);
		httpConn.setReadTimeout(10000);
		
		httpConn.setDoInput(true);
		httpConn.setRequestProperty("Accept", "application/json");
		httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		
		httpConn.setRequestProperty("Content-Length", String.valueOf(params.length()));
		OutputStream os = httpConn.getOutputStream();
		os.write(params.getBytes());
		os.flush();
		os.close();
		
		int responseCode = httpConn.getResponseCode();
		
		if( HttpURLConnection.HTTP_OK == responseCode) {
			logger.info("responseCode >>>> : " +responseCode);
			oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),"UTF-8"));
		} else {
			logger.info("responseCode etc >>>> : " +responseCode);
			oBufReader = new BufferedReader(new InputStreamReader(httpConn.getErrorStream(),"UTF-8"));
		}
	

		StringBuffer sb = new StringBuffer(strRslt);
		
		int i = 1;
		while ((strBuffer = oBufReader.readLine()) != null && (i < 2)) {
			if (strBuffer.length() > 1) {
				sb.append(strBuffer);
			}
		}
		logger.info("result :" + sb.toString());
		oBufReader.close();
		
		return sb.toString();
	}
	
}