/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.user.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.security.bean.CmsUserInfo;
import com.wiz.clip.user.bean.CmsLoginLog;
import com.wiz.clip.user.bean.CmsUseLog;
import com.wiz.clip.user.bean.SearchBean;
import com.wiz.clip.user.service.UserService;

@Controller
public class UserController  {
	Logger logger = Logger.getLogger(UserController.class.getName());
	
	@Value("#{config['PAGE_SIZE']}") int PAGE_SIZE;
	
	@Autowired
	private UserService userService;

	
	/*
	 * 리스트
	 */
	@RequestMapping(value = "/user/userList.do")
	public ModelAndView userList(@RequestParam(value = "pg", required = false) String page,
			@RequestParam(required = false) String status, @RequestParam(required = false) String keywd, ModelMap map) {

		logger.debug("status :" + status);
		logger.debug("keywd :" + keywd);

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("startNum", startNum);
		map.put("endNum", endNum);

		// 검색어 관련
		if (status != null && !status.equals("") && keywd != null && !keywd.equals("")) {
			map.put("status", status);
			map.put("keywd", keywd);
		} else {
			map.put("status", "");
			map.put("keywd", "");
		}

		List<CmsUserInfo> list = userService.getUserList(map);

		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		// 토탈 카운트
		paging.makePagingHtml(userService.getUserListCnt(map));

		map.clear();
		map.put("pg", pg);
		map.put("list", list);
		map.put("keywd", keywd);
		map.put("status", status);
		map.put("boardPaging", paging);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "사용자 관리");
		mav.addObject("menu2", "ID 관리");
		mav.addObject("display", "../clip/user/userList.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 입력폼
	 */
	@RequestMapping(value = "/user/userInsertForm.do")
	public ModelAndView userInsertForm(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("userInsertForm Page start!!!");
		ModelAndView mav = new ModelAndView();

		mav.addObject("menu1", "사용자 관리");
		mav.addObject("menu2", "ID 관리");
		mav.addObject("display", "../clip/user/userInsertForm.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	
	/*
	 * 같은 ID가 있는지 체크한다.
	 */
	@RequestMapping(value = "/user/userIdCnt.do", method = { RequestMethod.POST })
	@ResponseBody
	public String userIdCnt( @RequestParam String userid ,
			HttpServletRequest request, HttpServletResponse response) {
		logger.debug("rouletteComplete Page start!!!");

		// 아이디가 있는지 체크한다.
		int result = userService.userIdCnt(userid);

		Map<String,String> map = new HashMap<String,String>();
		
		if (result >= 1) {
			map.put("result", "false");
			return JSONValue.toJSONString(map);
		} else {
			map.put("result", "success");
			return JSONValue.toJSONString(map);
		}

	}
	
	/*
	 * 입력처리
	 */
	@RequestMapping(value = "/user/userInsert.do")
	public ModelAndView userInsert(HttpServletRequest request, HttpServletResponse response, ModelMap map,
			CmsUserInfo cmsUserInfo) {

		logger.debug("userInsert Page start!!!");
		ModelAndView mav = new ModelAndView();

		userService.userInsert(cmsUserInfo, request);

		map.addAttribute("msg", "작성하신 내용이 정상적으로 등록되었습니다.");
		map.addAttribute("url", "/user/userList.do");
		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 상세보기
	 */
	@RequestMapping(value = "/user/userView.do")
	public ModelAndView userView(@RequestParam String userid, @RequestParam(required = false) String page,
			ModelMap map) {
		logger.debug("userView Page start!!!");
		
		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		CmsUserInfo cmsUserInfo = userService.getUser(userid);

		map.put("pg", pg);
		map.put("cmsUserInfo", cmsUserInfo);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "사용자 관리");
		mav.addObject("menu2", "ID 관리");
		mav.addObject("display", "../clip/user/userView.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 수정폼
	 */
	@RequestMapping(value = "/user/userUpdateForm.do")
	public ModelAndView userUpdateForm(@RequestParam String userid, @RequestParam(required = false) String page,
			ModelMap map) {
		logger.debug("userUpdateForm Page start!!!");
		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		CmsUserInfo cmsUserInfo = userService.getUser(userid);

		map.put("pg", pg);
		map.put("cmsUserInfo", cmsUserInfo);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "사용자 관리");
		mav.addObject("menu2", "ID 관리");
		mav.addObject("display", "../clip/user/userUpdateForm.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 수정처리
	 */
	@RequestMapping(value = "/user/userUpdate.do")
	public ModelAndView channelUpdate(
			@RequestParam(required = false) String userid,
			@RequestParam(required = false) String page,
			HttpServletRequest request, HttpServletResponse response, ModelMap map,
			CmsUserInfo cmsUserInfo
			) throws java.lang.Exception {

		logger.debug("userUpdate Page start!!!");
		ModelAndView mav = new ModelAndView();

		String pg = page;

		if (pg == null || pg == ""){
			pg = "1";
		}

		userService.userUpdate(cmsUserInfo, request);

		map.addAttribute("msg", "작성하신 내용이 정상적으로 수정되었습니다.");
		map.addAttribute("url", "/user/userView.do?userid=" + userid + "&pg=" + pg);

		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}
	
	/*
	 * 삭제처리
	 */
	@RequestMapping(value = "/user/userDelete.do")
	public ModelAndView userDelete(@RequestParam String userid,
			@RequestParam(value = "pg", required = false) String page, ModelMap map) {
		logger.debug("userDelete Page start!!!");

		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		userService.userDelete(userid);

		map.put("pg", pg);

		ModelAndView mav = new ModelAndView();
		map.addAttribute("msg", "정상적으로 삭제 되었습니다.");
		map.addAttribute("url", "/user/userList.do");
		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	
	
	
	
		
	
		/*
		 * 로그인/아웃 내역확인
		 */
		@RequestMapping(value = "/user/viewLogLoginOut.do")
		public ModelAndView viewLogLoginOut(@RequestParam(value = "pg", required = false) String page,
				SearchBean searchBean, ModelMap map) {

			String pg = page;
			// 페이징 관련
			if (pg == null || pg == ""){
				pg = "1";
			}
			int endNum = Integer.parseInt(pg) * PAGE_SIZE;
			int startNum = endNum - ( PAGE_SIZE - 1 ) ;
			map.put("endNum", endNum);
			map.put("startNum", startNum);
			searchBean.setStartNum(startNum);
			searchBean.setEndNum(endNum);
			// 페이징
			BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
			paging.setCurrentPage(Integer.parseInt(pg));
			
			List<CmsLoginLog> list = userService.viewLogLoginOut(searchBean);
			
			int totalCnt = userService.viewLogLoginOutCnt(searchBean);
			paging.makePagingHtml(totalCnt);
			map.clear();
			map.put("pg", pg);
			map.put("boardPaging", paging);
			
			ModelAndView mav = new ModelAndView();
			mav.addObject("searchBean", searchBean);
			mav.addObject("list", list);
			mav.addObject("menu1", "사용자 관리");
			mav.addObject("menu2", "로그인/아웃 내역확인");
			mav.addObject("display", "../clip/user/viewLogLoginOut.jsp");
			
			if("Y".equals(searchBean.getExcelDownload())){
				mav.addObject("pageNm", "로그인아웃내역확인");
				mav.addObject("divString", "viewLogLoginOut");
				mav.setViewName("excelView");
			}else{
				mav.setViewName("/main/index");
			}
			
			return mav;
		}
		
		/*
		 * 메뉴 접속로그
		 */
		@RequestMapping(value = "/user/viewLogMenu.do")
		public ModelAndView viewLogMenu(@RequestParam(value = "pg", required = false) String page,
				SearchBean searchBean, ModelMap map) {

			String pg = page;
			// 페이징 관련
			if (pg == null || pg == ""){
				pg = "1";
			}
			int endNum = Integer.parseInt(pg) * PAGE_SIZE;
			int startNum = endNum - ( PAGE_SIZE - 1 ) ;
			map.put("endNum", endNum);
			map.put("startNum", startNum);
			searchBean.setStartNum(startNum);
			searchBean.setEndNum(endNum);
			// 페이징
			BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
			paging.setCurrentPage(Integer.parseInt(pg));
			
			List<CmsUseLog> list = userService.viewLogMenu(searchBean);

			int totalCnt = userService.viewLogMenuCnt(searchBean);
			paging.makePagingHtml(totalCnt);
			map.clear();
			map.put("pg", pg);
			map.put("boardPaging", paging);

			ModelAndView mav = new ModelAndView();
			mav.addObject("searchBean", searchBean);
			mav.addObject("list", list);
			mav.addObject("menu1", "사용자 관리");
			mav.addObject("menu2", "메뉴 접속로그");
			mav.addObject("display", "../clip/user/viewLogMenu.jsp");

			if("Y".equals(searchBean.getExcelDownload())){
				mav.addObject("pageNm", "메뉴접속로그");
				mav.addObject("divString", "viewLogMenu");
				mav.setViewName("excelView");
			}else{
				mav.setViewName("/main/index");
			}
			
			return mav;
		}
	
	
	
	
	
	
	
	
	
	
	
}
