/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.user.service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.wiz.clip.security.bean.CmsUserInfo;
import com.wiz.clip.user.bean.CmsLoginLog;
import com.wiz.clip.user.bean.CmsUseLog;
import com.wiz.clip.user.bean.SearchBean;
import com.wiz.clip.user.dao.UserDAO;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Value("#{config['LOCAL_IP_6']}") String LOCAL_IP_6;
	
	@Autowired
	private UserDAO userDao;

	Logger logger = Logger.getLogger(UserServiceImpl.class.getName());

	@Override
	public List<CmsUserInfo> getUserList(ModelMap map) {
		return userDao.getUserList(map);
	}

	@Override
	public int getUserListCnt(ModelMap map) {
		return userDao.getUserListCnt(map);
	}

	@Override
	public int userInsert(CmsUserInfo cmsUserInfo, HttpServletRequest request) {
		
		String USER_IP = request.getRemoteAddr();
 		
		if (USER_IP.equalsIgnoreCase(LOCAL_IP_6)) {
		    InetAddress inetAddress;
			try {
				inetAddress = InetAddress.getLocalHost();
				USER_IP = inetAddress.getHostAddress();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		cmsUserInfo.setIpaddr(USER_IP);
		
		return userDao.userInsert(cmsUserInfo);
	}

	@Override
	public CmsUserInfo getUser(String userid) {
		return userDao.getUser(userid);
	}

	@Override
	public int userUpdate(CmsUserInfo cmsUserInfo, HttpServletRequest request) {
		
		String USER_IP = request.getRemoteAddr();
 		
		if (USER_IP.equalsIgnoreCase(LOCAL_IP_6)) {
		    InetAddress inetAddress;
			try {
				inetAddress = InetAddress.getLocalHost();
				USER_IP = inetAddress.getHostAddress();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		cmsUserInfo.setModiipaddr(USER_IP);
		
		return userDao.userUpdate(cmsUserInfo);
	}

	@Override
	public int userDelete(String userid) {
		return userDao.userDelete(userid);
	}

	@Override
	public int userIdCnt(String userid) {
		return userDao.userIdCnt(userid);
	}

	@Override
	public List<CmsLoginLog> viewLogLoginOut(SearchBean searchBean) {
		return userDao.viewLogLoginOut(searchBean);
	}

	@Override
	public List<CmsUseLog> viewLogMenu(SearchBean searchBean) {
		return userDao.viewLogMenu(searchBean);
	}

	@Override
	public int viewLogLoginOutCnt(SearchBean searchBean) {
		return userDao.viewLogLoginOutCnt(searchBean);
	}

	@Override
	public int viewLogMenuCnt(SearchBean searchBean) {
		return userDao.viewLogMenuCnt(searchBean);
	}
	
	

	

}
