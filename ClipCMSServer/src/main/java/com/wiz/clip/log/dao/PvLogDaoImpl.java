/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.log.dao;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

@Repository
public class PvLogDaoImpl implements PvLogDao {
	@Autowired
	private SqlSession postgreSession;
	
	@Override
	public void insertPvLog(Map<String, String> map) {
		postgreSession.insert("PvLOG.insertPvLog",map);
	}

	@Override
	public void insertSMSLog(ModelMap model) {
		postgreSession.insert("PvLOG.insertSMSLog",model);
	}		
}
