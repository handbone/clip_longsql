package com.wiz.clip.common.sendAPI;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class sendGetPlusCount {
	
	Logger logger = Logger.getLogger(sendGetPlusCount.class.getName());
	
	@Autowired
	private SqlSession postgreSession;
	
	@Value("#{config['API_DOMAIN_GET_PLUS_COUNT']}")
	String API_DOMAIN_GET_PLUS_COUNT;

	
	@RequestMapping(value = "/sendGetPlusCount.do")
	@ResponseBody
	public String sendGetPlusCount(@RequestParam String cust_id, @RequestParam String ctn, @RequestParam String requester_code)  throws java.lang.Exception {
	
		StringBuffer sb = new StringBuffer();
		
		sb.append("cust_id=");
		sb.append(java.net.URLEncoder.encode(cust_id,"UTF-8"));
		
		sb.append("&ctn=");
		sb.append(java.net.URLEncoder.encode(ctn,"UTF-8"));
		
		sb.append("&requester_code=");
		sb.append(java.net.URLEncoder.encode(requester_code,"UTF-8"));
		
		logger.debug("sb ::::"+sb.toString());
		
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = API_DOMAIN_GET_PLUS_COUNT;

		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(10000);
		httpConn.setReadTimeout(10000);
		httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));
		//httpConn.setRequestProperty("Content-Type", "application/soap+xml; charset=utf-8");
		OutputStream os = httpConn.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();
		os.close();
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
		StringBuffer sb3 = new StringBuffer(strRslt);
			
		while ((strBuffer = oBufReader.readLine()) != null) {
			if (strBuffer.length() > 1) {
				sb3.append(strBuffer);
			}
		}
		oBufReader.close();
			
		return sb3.toString();
	}

}
