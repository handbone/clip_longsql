/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package com.wiz.clip.common.sendAPI;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Random;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import com.wiz.clip.common.crypto.HmacCrypto;

@Component
public class HttpNetwork {
	
	Logger logger = Logger.getLogger(this.getClass());

	@Value("#{config['API_DOMAIN_SEND_PLUS_POINT']}")
	String API_DOMAIN_SEND_PLUS_POINT;

	public String sendPlusPoint(
			String user_ci,
			String requester_code,
			String point_value,
			String description)  
			throws java.lang.Exception {
	
		String userCi = user_ci;
			
		String smsDomain = API_DOMAIN_SEND_PLUS_POINT;
				
		String tr_id = String.valueOf(System.currentTimeMillis());
		if("".equals(tr_id) || null == tr_id){
			throw new java.lang.Exception();
		}
		StringBuffer sb2 = new StringBuffer(tr_id);
		if (tr_id.length() < 16) {
			Random rand = new Random(System.currentTimeMillis()); 
			sb2.append(Math.abs(rand.nextInt(999)));
		}
		if (tr_id.length() < 16) {
			while (sb2.length() < 16) {
				sb2.append("0");
			}
		}
		tr_id = sb2.toString();
		
		StringBuffer sb = new StringBuffer();
		
		if(null != userCi){
			sb.append("&user_ci=");
			sb.append(java.net.URLEncoder.encode(userCi,"UTF-8"));
		}
		
		sb.append("&transaction_id=");
		sb.append(tr_id);
		sb.append("&requester_code=");
		sb.append(requester_code);
		sb.append("&point_value=");
		sb.append(point_value);
		sb.append("&description=");
		sb.append(java.net.URLEncoder.encode(description,"UTF-8"));
				
		logger.debug("sb ::::"+sb.toString());
		
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = smsDomain;

		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(10000);
		httpConn.setReadTimeout(10000);
		httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));

		OutputStream os = httpConn.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();
		os.close(); 
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
		StringBuffer sb3 = new StringBuffer(strRslt);
			
		while ((strBuffer = oBufReader.readLine()) != null) {
			if (strBuffer.length() > 1) {
				sb3.append(strBuffer);
			}
		}
		oBufReader.close();
			
		return sb3.toString();
	}
	
	
//
//	public static void main(String[] args){
//		
//		
//	
//	String agreeCheckYn = "y";
//	String sendServiceType = "01";
//	String linkId = "clippoint";
//	String type = "ctn";
//	String id = "01073215434";
//	String imgUrl = null;
//	String randingUrl = "KTolleh00114://";
//	String expireDate = "20170831195000";
//	String title = "푸시 테스트";
//	String content = "외부 PUSH Test";
//	
//	String signdata = agreeCheckYn + sendServiceType + linkId + type + id + imgUrl + randingUrl + expireDate + title +content;
//	System.out.println(signdata);
//	signdata = signdata +"push" + "external" + "getPushInfo";
//	String encSigndata = HmacCrypto.getHmacVal(signdata);
//	
//	System.out.println(encSigndata);
//	String tt= "387cbe4956ccbb84c3fd86c8ffb3782c6a1d65a8ecfbdaa6d3e4f8c2f4809123";
//	}


}
