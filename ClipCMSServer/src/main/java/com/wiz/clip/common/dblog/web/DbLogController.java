/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.common.dblog.web;

import java.util.Map;
import java.util.WeakHashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.wiz.clip.common.dblog.service.DbLogService;

@Controller
public class DbLogController {
	
	@Resource(name="dbLogService")
	private DbLogService dbLogService;
	
	//통계 엑셀 출력 로그 남기기  ajax
	@RequestMapping(value = "/dblog/insertReportLogG.do")
	public ResponseEntity<String> cancelCheckG(HttpServletRequest request, 
			HttpServletResponse response,
			@RequestParam(value="agent_code") String agentcode,
			@RequestParam(value="remark") String remark,
			ModelMap map) {
		
		HttpSession session = (HttpSession)request.getSession(false);

		//현행화 데이터 조회[고객정보조회] 로그 남기기
		Map<String, String> wMap = new WeakHashMap<String, String>();
		wMap.put("kind", "DAY_STATISTICS");           //조회메뉴 : ALL_COM_DATA 현행화데이터
													  //DAY_STATISTICS 비즈링 기업별설정/해지내역
		
		wMap.put("status", "R");                      // S:조회, U:수정 , R:출력
		wMap.put("agentcode", agentcode);             //조회대상 영업대행사 코드
		wMap.put("cocode", "-");                      //조회대상 기업코드
		wMap.put("userid", (String)session.getAttribute("AgentUserId")); //사용자 ID
		wMap.put("userip", (String)session.getAttribute("AgentUserIp")); //사용자 IP
		wMap.put("remark", remark);                                         //비고
		
		dbLogService.insertAgentUseLog(wMap);
		
		
		map.clear();
		map.put("result", "pass");
		
		
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		return new ResponseEntity<String>(JSONArray.fromObject(map).toString(),responseHeaders, HttpStatus.CREATED);
	}

}
