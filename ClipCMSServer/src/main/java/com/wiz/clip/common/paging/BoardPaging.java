/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.common.paging;


public class BoardPaging {
	
	
	int currentPage; //현재페이지
	int pageBlock; //이전 1 2 3 다음
	int pageSize;//1페이지당 뿌려질 목록수
	StringBuffer pagingHTML;
	StringBuffer pagingHTMLDetail;
	
	public BoardPaging(int pageBlock, int pageSize) {
		this.pageBlock = pageBlock;
		this.pageSize = pageSize;
	}
	
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void makePagingHtml(int A){
		pagingHTML = new StringBuffer();
		int totalA = A; //DB 총글수 
		int totalP = (totalA+(pageSize-1))/pageSize;		
		
		int startPage = (currentPage-1)/pageBlock*pageBlock+1; 
		int endPage = startPage+pageBlock-1;
		
		if(endPage > totalP) {
			endPage = totalP;
		}
		
		//-------------------------------------
		if(startPage > pageBlock){
			pagingHTML.append("<span id='pageSpan' class='page_btn1' onclick='boardPaging("+(startPage-1)+")'><</span>");
		}
		
		for(int i=startPage; i<=endPage; i++){
			if(i==currentPage){
				pagingHTML.append("<span id='currentPageSpan' class='page_btn2' onclick='boardPaging("+i+")'>"+i+"</span>");
			}else{
				pagingHTML.append("<span id='pageSpan' class='page_btn1' onclick='boardPaging("+i+")'>"+i+"</span>");
			}
		}
		
		if(endPage<totalP){
			pagingHTML.append("<span id='pageSpan' class='page_btn1' onclick='boardPaging("+(endPage+1)+")'>></span>");
		}
	}
	
	
	public void makePagingHtmlDetail(int A){
		pagingHTMLDetail = new StringBuffer();
		int totalA = A; //DB 총글수 
		int totalP = (totalA+(pageSize-1))/pageSize;		
		
		int startPage = (currentPage-1)/pageBlock*pageBlock+1; 
		int endPage = startPage+pageBlock-1;
		
		if(endPage > totalP) {
			endPage = totalP;
		}
		
		//-------------------------------------
		if(startPage > pageBlock){
			pagingHTMLDetail.append("<span id='pageSpan' class='page_btn1' onclick='boardPaging("+(startPage-1)+")'><</span>");
		}
		
		for(int i=startPage; i<=endPage; i++){
			if(i==currentPage){
				pagingHTMLDetail.append("<span id='currentPageSpan' class='page_btn2' onclick='boardPaging("+i+")'>"+i+"</span>");
			}else{
				pagingHTMLDetail.append("<span id='pageSpan' class='page_btn1' onclick='boardPaging("+i+")'>"+i+"</span>");
			}
		}
		
		if(endPage<totalP){
			pagingHTMLDetail.append("<span id='pageSpan' class='page_btn1' onclick='boardPaging("+(endPage+1)+")'>></span>");
		}
	}
	
	public String getPagingHTML(){	
		return pagingHTML.toString();
	}
	
	public String getPagingHTMLDetail(){	
		return pagingHTMLDetail.toString();
	}

}










