/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.common.dblog.dao;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("dbLogDao")
public class DbLogDAO {
	@Autowired
	private SqlSession postgreSession;
	
	public void insertAgentUserLog(Map<String, String> map) {
		postgreSession.insert("DBLOG.insertAgentUserLog",map);	
	}
	

	public void saveLoginLog(Map<String, String> map) {
		postgreSession.insert("DBLOG.saveLoginLog",map);	
	}

	public void saveLogoutLog(Map<String, String> map) {
		postgreSession.update("DBLOG.saveLogoutLog",map);	
		
	}
	public void insertAgentUseLog(Map<String, String> wMap) {
		postgreSession.insert("DBLOG.insertAgentUseLog",wMap);
		
	}
	public void insertBizUserSearchLog(Map<String, String> hMap) {
		postgreSession.insert("DBLOG.insertBizUserSearchLog", hMap);
		
	}		
}
