/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.common.download;

import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class DownloadController {
	Logger logger = Logger.getLogger(DownloadController.class.getName());
	
	@Value("#{config['storage.notice']}") String noticePath;
	@Value("#{config['storage.sound']}") String soundPath;
	@Value("#{config['storage.sound_local']}") String soundPath_local;
	
	@Value("#{config['logfilepath']}") String logfilepath;
	//한글파일 문제로 다운로드 유틸 사용
	@RequestMapping("/fileDownload.do")
	public ModelAndView download(HttpServletRequest request,
			@RequestParam String fileName,
			@RequestParam String gubun) throws UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		
		logger.debug("fileDownload Page start!!!");
		logger.debug("[fileName] :"+fileName);
		
		
		
		File downloadFile = null;
		if(gubun.equalsIgnoreCase("notice")){
			 downloadFile = new File(noticePath + fileName); //공지파일
		}else if(gubun.equalsIgnoreCase("sound")){
			 downloadFile = new File(soundPath + fileName); //사운드파일
		}else if(gubun.equalsIgnoreCase("sound_local")){
			 downloadFile = new File(soundPath_local + fileName); //사운드파일 로컬(구 비즈링 로컬)	 
		}else if(gubun.equalsIgnoreCase("logfile")){
			 downloadFile = new File(logfilepath + fileName); //사운드파일
		}
		
		
		return new ModelAndView("downloadView", "downloadFile", downloadFile);
	}
	
}