package com.wiz.clip.common.sendAPI;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.json.simple.JSONValue;
import org.json.simple.parser.*;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.manage.manageLog.bean.PointHistTbl;
import com.wiz.clip.manage.pointChk.bean.CmsPointModify;
import com.wiz.clip.manage.pointChk.bean.SearchBean;
import com.wiz.clip.manage.pointChk.service.PointChkService;

import sun.security.pkcs.ParsingException;

/*  
  
 로직의 문제로 인해 중복적립 사용자가 600명 이상 발행하여
 포인트를 환수하는 프로그램
 
 
-- Table: "UPDATE_POINT_TEMP"

-- DROP TABLE "UPDATE_POINT_TEMP";

CREATE TABLE "UPDATE_POINT_TEMP"
(
  point_hist_idx integer NOT NULL,
  user_ci character varying(256),
  point_value bigint,
  description character varying(1024),
  prebalance character varying(256),
  resultyn character varying(2),
  minuspoint character varying(256),
  resultpoint character varying(256),
  CONSTRAINT "pk_UPDATE_POINT_TEMP" PRIMARY KEY (point_hist_idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "UPDATE_POINT_TEMP"
  OWNER TO point;
  
  
  
 * 테스트 자료 입력 -  , cust_id     , transaction_id  삭제하라.
  INSERT INTO "UPDATE_POINT_TEMP"(         point_hist_idx         , user_ci         , cust_id     , transaction_id         , point_value         )
	VALUES ( 	5 	, 'user_ci005' 	, 'cust_id005' , 'transaction_id0005', '1005'	);

5;"NmwrWysjrN/c0tQQp1EVg/iVjx4Rqt21X4XWE0gu54X9m2Z79lO4oYG6D9wZoBZmsk5OYPIbh/qJVKcVWjs9TA==";"cust_id005";"";"";"transaction_id0005";1005;"java.io.FileNotFoundException";"";"";"";"";"";"";"";"";;"";"";"";""
4;"NmwrWysjrN/c0tQQp1EVg/iVjx4Rqt21X4XWE0gu54X9m2Z79lO4oYG6D9wZoBZmsk5OYPIbh/qJVKcVWjs9TA==";"cust_id004";"";"";"transaction_id0004";1004;"java.io.FileNotFoundException";"";"";"";"";"";"";"";"";;"";"";"";""
3;"NmwrWysjrN/c0tQQp1EVg/iVjx4Rqt21X4XWE0gu54X9m2Z79lO4oYG6D9wZoBZmsk5OYPIbh/qJVKcVWjs9TA==";"cust_id003";"";"";"transaction_id0003";1003;"java.io.FileNotFoundException";"";"";"";"";"";"";"";"";;"";"";"";""
2;"NmwrWysjrN/c0tQQp1EVg/iVjx4Rqt21X4XWE0gu54X9m2Z79lO4oYG6D9wZoBZmsk5OYPIbh/qJVKcVWjs9TA==";"cust_id002";"";"";"transaction_id0002";1002;"java.io.FileNotFoundException";"";"";"";"";"";"";"";"";;"";"";"";""
1;"+unw9KYqdFA/J0wcwPRMaAYdADuPlkxO9tw2J/KECwp2NJvehLRnPcCcFhBaoxoEkUO/31l8tZ+V81PuxUaurg==";"cust_id001";"";"";"transaction_id0001";1000;"{"balance":38,"description":"�뿬湲곗뿉 �쟻�떦�븳 �꽕紐낆씠 �뱾�뼱媛��빞 �븳�떎.","point_value":1,"datetime":"20170512184812","user_ci":"+unw9KYqdFA\/J0wcwPRMaAYdADuPlkxO9tw2J\/KECwp2NJvehLRnPcCcFhBaoxoEkUO\/31l8tZ+V81PuxUaurg==","transaction_id":"1494582491865475"}";"";"";"";"";"";"";"";"";;"39";"Y";"39";"38"



실제 자료 DB에 입력 후

전액 환수가 안되는 인원수
SELECT COUNT(point_hist_idx)
  FROM "UPDATE_POINT_TEMP"
WHERE point_value > TO_NUMBER(prebalance,'99999999')

전액 환수가 안되는 금액
SELECT SUM(point_value - TO_NUMBER(prebalance,'99999999') ) 
FROM "UPDATE_POINT_TEMP"
WHERE point_hist_idx IN (
	SELECT point_hist_idx
	  FROM "UPDATE_POINT_TEMP"
	WHERE point_value > TO_NUMBER(prebalance,'99999999')
)



*/

/*
 * Y - 정상적으로 마이너스 처리
 * N - 잔액이 0이어서 아무것도 못함
 * L - 잔액이 부족해서 잔액 만큼만 마이너스 처리함
 * K - 에러
 * R - 에러

실행 URL 
http://localhost:8080/clipcms/updatePointByList.do?startNum=1&endNum=1
결과 
{"TOTAL_COUNT":30,"CANT_MINUS_COUNT":24,"COMPLETE_COUNT":6,"EXCEPTION_COUNT":0,"EXCEPTION_COUNT_2":0,"result":"result"}
상세결과는 DB통해 직접 확인
*/


@Controller
public class SendUpdatePointByList {
	
	Logger logger = Logger.getLogger(SendUpdatePointByList.class.getName());

	
	@Value("#{config['API_DOMAIN']}") String API_DOMAIN;
	@Value("#{config['POINT_MODIFY_ACL_REQUEST_CODE']}") String POINT_MODIFY_ACL_REQUEST_CODE;
	
	@Autowired
	private SqlSession postgreSession;

	@Autowired
	private PointChkService pointChkService;
	
	/*
	 * 포인트조정
	 */
	@RequestMapping(value ="/updatePointByList.do")
	@ResponseBody
	public String sendUpdatePoint(
			@RequestParam(value = "startNum", required = true) int startNum,
			@RequestParam(value = "endNum", required = true) int endNum,
			SearchBean searchBean, CmsPointModify cmsPointModify)  throws java.lang.Exception {
	
		// TODO 못깐 포인트 SUM 해볼까
		int CANT_MINUS_COUNT = 0; // 포인트 부족, 가지고 있는 포인트만 조정함
		int COMPLETE_COUNT = 0; // 정상완료
		int EXCEPTION_COUNT = 0; // 에러
		int EXCEPTION_COUNT_2 = 0; // 에러 
		
		ModelMap map = new ModelMap();
		
		//조정할 사용자가 너무 많으면 나눠서 실행한다.
		//DESC 정렬이다. idx값이 높은 순서로 가져온다.  
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		
		List<PointHistTbl> histlist = postgreSession.selectList("MANAGELOG.getUpdatePointByList", searchBean);	
		
		for(int i = 0 ;i < histlist.size() ; i++ ){
			PointHistTbl pointHistTbl = histlist.get(i);
			String balanceJson = "";
			
			String balance = "";
			
			try{
				balanceJson = sendGetPoint(pointHistTbl.getUser_ci());
				// test
				// throw new FileNotFoundException();
				
				// 현재 잔액 가져와서 
				
				String jsonText = balanceJson;
				  JSONParser parser = new JSONParser();
				  Map json = (Map)parser.parse(jsonText, containerFactory);
				  Iterator iter = json.entrySet().iterator();
				  while(iter.hasNext()){
				        Map.Entry entry = (Map.Entry)iter.next();
				        if("balance".equals(entry.getKey())){
				         balance = entry.getValue().toString();
				        }
				  }
				  
				  //일단 DB에 입력해놓고 
				  searchBean.setNo(pointHistTbl.getUser_ci());
				  searchBean.setKeywd(balance);
				  postgreSession.update("MANAGELOG.updatePreBalance", searchBean);
				  
				  searchBean.setKeywd("R"); 
				  postgreSession.update("MANAGELOG.updateResultYN", searchBean);
				  
				  
			}catch (Exception e) {
				
				EXCEPTION_COUNT++;
				
				// 에러 났을 경우 DESC에 입력해둠 
				// 잘못됐다는 필드도 업데이트 해야함
				
				searchBean.setNo(pointHistTbl.getUser_ci());
				if(e.toString().length() > 1022){
					searchBean.setKeywd(e.toString().substring(0,1022));
				}else{
					searchBean.setKeywd(e.toString());
				}
				postgreSession.update("MANAGELOG.updateDesc", searchBean);	
			}
			
			// 에러 없으면 포인트 조정을 시작해 볼까
			if(!"".equals(balanceJson)){
				
				  if( Integer.parseInt(balance) >= Integer.parseInt(pointHistTbl.getPoint_value())){
					  COMPLETE_COUNT++;
					  
					  // TODO pointHistTbl.getPoint_value() 는 실제 빼야하는 잘못 적립 된 포인트다. 
					  
					  try{
						  // 마이너스 실행
						  String sendMinusPointResultString = sendMinusPoint(pointHistTbl.getUser_ci(), pointHistTbl.getPoint_value());  
						  
						  // test
						  //map.put("sendMinusPointResultString", sendMinusPointResultString);
						  
						  // 마이너스 기록
						  searchBean.setKeywd(pointHistTbl.getPoint_value());
						  postgreSession.update("MANAGELOG.updateMinusPoint", searchBean);
						  // 잘됐다는 필드 업데이트 
						  searchBean.setKeywd("Y");
						  postgreSession.update("MANAGELOG.updateResultYN", searchBean);
						  //리턴값 DESC에 기록
						  searchBean.setKeywd(sendMinusPointResultString);
						  postgreSession.update("MANAGELOG.updateDesc", searchBean);
					  
						  
						  // 종료 되었다면 다시 최종 값을 가져와서 DB에 저장한다.
						  	try{
								balanceJson = sendGetPoint(pointHistTbl.getUser_ci());
								String jsonText = balanceJson;
								JSONParser parser2 = new JSONParser();
								  Map json2 = (Map)parser2.parse(jsonText, containerFactory);
								  Iterator iter2 = json2.entrySet().iterator();
								  while(iter2.hasNext()){
								        Map.Entry entry = (Map.Entry)iter2.next();
								        if("balance".equals(entry.getKey())){
								         balance = entry.getValue().toString();
								        }
								  }
								// 최종 포인트 기록
								searchBean.setKeywd(balance);
								postgreSession.update("MANAGELOG.updateResultPoint", searchBean);
								  
								
							}catch (Exception e) {
								// 특수한 경우, 이때는 수동으로 다시 최종값만 조회해오면 된다.
								searchBean.setKeywd("K"); 
								postgreSession.update("MANAGELOG.updateResultYN", searchBean);
							}
						  
						  
					  }catch (Exception e) {
						  
						  EXCEPTION_COUNT_2++;
						  
						  // 무언가 에러로 인해 다시 실행해야 하는 경우 
						  searchBean.setKeywd("R");
						  postgreSession.update("MANAGELOG.updateResultYN", searchBean);
						  
						  if(e.toString().length() > 1022){
								searchBean.setKeywd(e.toString().substring(0,1022));
							}else{
								searchBean.setKeywd(e.toString());
							}
						  postgreSession.update("MANAGELOG.updateDesc", searchBean);
					  }
					  
					  
					  
					  
				  }else{
					  CANT_MINUS_COUNT++;
					  
					  if( 0 != Integer.parseInt(balance)){
						  try{
							  // 현재 잔액이 부정적립된 액수보다 적을 경우에는, 현재 잔액만 모두 마이너스로 처리한다.
							  // balance 부분만 다르다
							  // 마이너스 실행
							  String sendMinusPointResultString = sendMinusPoint(pointHistTbl.getUser_ci(), balance);  
							  
							  // test
							  //map.put("sendMinusPointResultString", sendMinusPointResultString);
							  
							  // 마이너스 기록
							  searchBean.setKeywd(balance);
							  postgreSession.update("MANAGELOG.updateMinusPoint", searchBean);
							  // 잘됐다는 필드 업데이트 
							  searchBean.setKeywd("L");
							  postgreSession.update("MANAGELOG.updateResultYN", searchBean);
							  //리턴값 DESC에 기록
							  searchBean.setKeywd(sendMinusPointResultString);
							  postgreSession.update("MANAGELOG.updateDesc", searchBean);
						  
							  
							  // 종료 되었다면 다시 최종 값을 가져와서 DB에 저장한다.
							  	try{
									balanceJson = sendGetPoint(pointHistTbl.getUser_ci());
									String jsonText = balanceJson;
									JSONParser parser2 = new JSONParser();
									  Map json2 = (Map)parser2.parse(jsonText, containerFactory);
									  Iterator iter2 = json2.entrySet().iterator();
									  while(iter2.hasNext()){
									        Map.Entry entry = (Map.Entry)iter2.next();
									        if("balance".equals(entry.getKey())){
									         balance = entry.getValue().toString();
									        }
									  }
									// 최종 포인트 기록
									searchBean.setKeywd(balance);
									postgreSession.update("MANAGELOG.updateResultPoint", searchBean);
									  
									
								}catch (Exception e) {
									// 특수한 경우, 이때는 수동으로 다시 최종값만 조회해오면 된다.
									searchBean.setKeywd("K"); 
									postgreSession.update("MANAGELOG.updateResultYN", searchBean);
								}
							  
							  
						  }catch (Exception e) {
							  
							  EXCEPTION_COUNT_2++;
							  
							  // 무언가 에러로 인해 다시 실행해야 하는 경우 
							  searchBean.setKeywd("R");
							  postgreSession.update("MANAGELOG.updateResultYN", searchBean);
							  
							  if(e.toString().length() > 1022){
									searchBean.setKeywd(e.toString().substring(0,1022));
								}else{
									searchBean.setKeywd(e.toString());
								}
							  postgreSession.update("MANAGELOG.updateDesc", searchBean);
						  }
						  
						  
							
					  }else{
						  // 현재 잔액이 0이어서 아무것도 할 수 없는 경우
						  searchBean.setKeywd("N");
						  postgreSession.update("MANAGELOG.updateResultYN", searchBean);
						  searchBean.setKeywd("Point is 0!!");
						  postgreSession.update("MANAGELOG.updateDesc", searchBean);
						  
					  }
				  }
				  
				  
				  
			} 
		}
		
		
		map.put("TOTAL_COUNT", histlist.size());
		map.put("CANT_MINUS_COUNT", CANT_MINUS_COUNT);
		map.put("COMPLETE_COUNT", COMPLETE_COUNT);
		map.put("EXCEPTION_COUNT", EXCEPTION_COUNT);
		map.put("EXCEPTION_COUNT_2", EXCEPTION_COUNT_2);
		
	
		map.put("result", "result");
		return JSONValue.toJSONString(map);
		
	}
	
	
	public String sendMinusPoint(String user_ci, String point_value)  throws java.lang.Exception {
		
		
	// TODO test
	//user_ci = "+unw9KYqdFA/J0wcwPRMaAYdADuPlkxO9tw2J/KECwp2NJvehLRnPcCcFhBaoxoEkUO/31l8tZ+V81PuxUaurg==";
	//point_value = "1";	
	
	String tr_id = String.valueOf(System.currentTimeMillis());
	if("".equals(tr_id) || null == tr_id){
		throw new java.lang.Exception();
	}
	StringBuffer sb2 = new StringBuffer(tr_id);
	if (tr_id.length() < 16) {
		Random rand = new Random(System.currentTimeMillis()); 
		sb2.append(Math.abs(rand.nextInt(999)));
	}
	if (tr_id.length() < 16) {
		while (sb2.length() < 16) {
			sb2.append("0");
		}
	}
	
	tr_id = sb2.toString();
	
	StringBuffer sb = new StringBuffer();
	
	sb.append("user_ci=");
	sb.append(java.net.URLEncoder.encode(user_ci,"UTF-8"));
	sb.append("&transaction_id=");
	sb.append(tr_id);
	sb.append("&requester_code=");
	sb.append("pointUpdate");
	sb.append("&point_value=");
	sb.append(point_value);
	sb.append("&description=");
	
	// test
	sb.append(java.net.URLEncoder.encode("룰렛 오적립 포인트 조정","UTF-8"));
			
	BufferedReader oBufReader = null;
	HttpURLConnection httpConn = null;
	String strBuffer = "";
	String strRslt = "";
	
	String domain = API_DOMAIN;
	String path = "";
	path = "minusPoint.do";
	
	String strEncodeUrl = domain + path;

	URL oOpenURL = new URL(strEncodeUrl);
	httpConn = (HttpURLConnection) oOpenURL.openConnection();
	httpConn.setDoOutput(true);
	httpConn.setRequestMethod("POST");
	httpConn.setConnectTimeout(30000);
	httpConn.setReadTimeout(30000);
	httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));
	OutputStream os = httpConn.getOutputStream();
	os.write(sb.toString().getBytes());
	os.flush();
	os.close();
	oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
	StringBuffer sb3 = new StringBuffer(strRslt);
		
	while ((strBuffer = oBufReader.readLine()) != null) {
		if (strBuffer.length() > 1) {
			sb3.append(strBuffer);
		}
	}
	oBufReader.close();
	
	return sb3.toString();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String sendGetPoint(String user_ci)  throws java.lang.Exception {
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("user_ci=");
		sb.append(java.net.URLEncoder.encode(user_ci,"UTF-8"));
				
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String domain = API_DOMAIN;
		String path = "getPoint.do";
		String strEncodeUrl = domain + path;

		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(30000);
		httpConn.setReadTimeout(30000);
		httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));
		OutputStream os = httpConn.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();
		os.close();
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
		StringBuffer sb3 = new StringBuffer(strRslt);
			
		while ((strBuffer = oBufReader.readLine()) != null) {
			if (strBuffer.length() > 1) {
				sb3.append(strBuffer);
			}
		}
		oBufReader.close();
			
		return sb3.toString();
	}
	
	ContainerFactory containerFactory = new ContainerFactory(){

		@Override
		public Map createObjectContainer() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List creatArrayContainer() {
			// TODO Auto-generated method stub
			return null;
		}};
}
