/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.common.dblog.service;

import java.util.Map;

public interface DbLogService {
	void insertAgentUserLog(String id, String kind,
			String status, String agentcode, String suid, String suip);
	
	public void saveLoginLog(Map<String, String> map);
	
	public void saveLogoutLog(Map<String, String> map);
	
	//고객정보조회 수정 로그 [고객조회,현행화데이터,통계출력]
	void insertAgentUseLog(Map<String, String> wMap);
	
	//비즈링 고객정보조회 수정 로그 [비즈링고객정보 조회 메뉴 관련 수정,조회등]
	void insertBizUserSearchLog(Map<String, String> hMap);
	
}
