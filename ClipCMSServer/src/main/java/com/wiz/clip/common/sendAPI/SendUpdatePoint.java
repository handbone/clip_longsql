package com.wiz.clip.common.sendAPI;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Random;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.manage.pointChk.bean.CmsPointModify;
import com.wiz.clip.manage.pointChk.bean.SearchBean;
import com.wiz.clip.manage.pointChk.service.PointChkService;

@Service
public class SendUpdatePoint {
	
	Logger logger = Logger.getLogger(SendUpdatePoint.class.getName());

	
	@Value("#{config['API_DOMAIN']}") String API_DOMAIN;
	@Value("#{config['POINT_MODIFY_ACL_REQUEST_CODE']}") String POINT_MODIFY_ACL_REQUEST_CODE;
	
	@Autowired
	private SqlSession postgreSession;

	@Autowired
	private PointChkService pointChkService;
	
	public String sendUpdatePoint(SearchBean searchBean, CmsPointModify cmsPointModify)  throws java.lang.Exception {
	
		//user_ci를 직접 받아라
		cmsPointModify.setUser_ci(searchBean.getUser_ci());
		
		String tr_id = String.valueOf(System.currentTimeMillis());
		if("".equals(tr_id) || null == tr_id){
			throw new java.lang.Exception();
		}
		StringBuffer sb2 = new StringBuffer(tr_id);
		if (tr_id.length() < 16) {
			Random rand = new Random(System.currentTimeMillis()); 
			sb2.append(Math.abs(rand.nextInt(999)));
		}
		if (tr_id.length() < 16) {
			while (sb2.length() < 16) {
				sb2.append("0");
			}
		}
		tr_id = sb2.toString();
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("user_ci=");
		sb.append(java.net.URLEncoder.encode(cmsPointModify.getUser_ci(),"UTF-8"));
		sb.append("&transaction_id=");
		sb.append(tr_id);
		sb.append("&requester_code=");
		sb.append(POINT_MODIFY_ACL_REQUEST_CODE);
		sb.append("&point_value=");
		sb.append(cmsPointModify.getPoint_value());
		sb.append("&description=");
		sb.append(java.net.URLEncoder.encode(cmsPointModify.getDescription(),"UTF-8"));
				
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String domain = API_DOMAIN;
		String path = "";
		if("I".equals(cmsPointModify.getPoint_type())){
			path = "plusPoint.do";
		}else if("O".equals(cmsPointModify.getPoint_type())){
			path = "minusPoint.do";
		}
		String strEncodeUrl = domain + path;

		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(10000);
		httpConn.setReadTimeout(10000);
		httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));
		OutputStream os = httpConn.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();
		os.close();
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
		StringBuffer sb3 = new StringBuffer(strRslt);
			
		while ((strBuffer = oBufReader.readLine()) != null) {
			if (strBuffer.length() > 1) {
				sb3.append(strBuffer);
			}
		}
		oBufReader.close();
			
		return sb3.toString();
	}
	
	public String sendGetPoint(SearchBean searchBean, String user_ci)  throws java.lang.Exception {
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("user_ci=");
		sb.append(java.net.URLEncoder.encode(user_ci,"UTF-8"));
				
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String domain = API_DOMAIN;
		String path = "getPoint.do";
		String strEncodeUrl = domain + path;

		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(10000);
		httpConn.setReadTimeout(10000);
		httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));
		OutputStream os = httpConn.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();
		os.close();
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
		StringBuffer sb3 = new StringBuffer(strRslt);
			
		while ((strBuffer = oBufReader.readLine()) != null) {
			if (strBuffer.length() > 1) {
				sb3.append(strBuffer);
			}
		}
		oBufReader.close();
			
		return sb3.toString();
	}
	
}
