/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.roulette.channel.service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.wiz.clip.roulette.channel.bean.PointRoulette;
import com.wiz.clip.roulette.channel.dao.ChannelDAO;
import com.wiz.clip.security.bean.CmsUserInfo;

@Service
@Transactional
public class ChannelServiceImpl implements ChannelService {
	
	@Value("#{config['LOCAL_IP_6']}") String LOCAL_IP_6;
	
	@Autowired
	private ChannelDAO channelDao;

	Logger logger = Logger.getLogger(ChannelServiceImpl.class.getName());
	
	@Override
	public List<PointRoulette> getRouletteList(ModelMap map) {
		return channelDao.getRouletteList(map);
	}

	@Override
	public int getRouletteListCnt(ModelMap map) {
		return channelDao.getRouletteListCnt(map);
	}

	@Override
	public int channelInsert(HttpServletRequest request, PointRoulette pointRoulette) {
		
		GregorianCalendar today = new GregorianCalendar();
		String year = String.valueOf(today.get(today.YEAR)).substring(2,4);
        String randomInt = String.valueOf(Math.abs((int) (Math.random() * 1000000000) + 1000000000));
        
        String roulette_id = year + "0" + pointRoulette.getRoutype() + randomInt;
		pointRoulette.setRoulette_id(roulette_id);

		int totmax = /*Integer.parseInt(pointRoulette.getMax01())+ */
				Integer.parseInt(pointRoulette.getMax02())
				+ Integer.parseInt(pointRoulette.getMax03())
				+ Integer.parseInt(pointRoulette.getMax04())
				+ Integer.parseInt(pointRoulette.getMax05())
				+ Integer.parseInt(pointRoulette.getMax06());
		pointRoulette.setTotmax(String.valueOf(totmax));
		
		CmsUserInfo user = (CmsUserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		pointRoulette.setUserid(user.getUserid());
		
		String USER_IP = request.getRemoteAddr();
		   
		if (USER_IP.equalsIgnoreCase(LOCAL_IP_6)) {
		    InetAddress inetAddress;
			try {
				inetAddress = InetAddress.getLocalHost();
				String ipAddress = inetAddress.getHostAddress();
			    USER_IP = ipAddress;
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		}
		pointRoulette.setIpaddr(USER_IP);
		
		  
		/*logger.debug("totmax"+pointRoulette.getTotmax());
		logger.debug("timegbn"+pointRoulette.getTimegbn());
		logger.debug("startdate"+pointRoulette.getStartdate());
		logger.debug("enddate"+pointRoulette.getEnddate());
		logger.debug("userid"+pointRoulette.getUserid());
		logger.debug("ipaddr"+pointRoulette.getIpaddr());*/
		
		return channelDao.channelInsert(pointRoulette);	
	}

	@Override
	public PointRoulette getChannel(String roulette_id) {
		return channelDao.getChannel(roulette_id);	
	}

	@Override
	public int channelUpdate(HttpServletRequest request, PointRoulette pointRoulette) {
		
		int totmax = /*Integer.parseInt(pointRoulette.getMax01())+ */
				Integer.parseInt(pointRoulette.getMax02())
				+ Integer.parseInt(pointRoulette.getMax03())
				+ Integer.parseInt(pointRoulette.getMax04())
				+ Integer.parseInt(pointRoulette.getMax05())
				+ Integer.parseInt(pointRoulette.getMax06());
		pointRoulette.setTotmax(String.valueOf(totmax));
		
		CmsUserInfo user = (CmsUserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		pointRoulette.setUserid(user.getUserid());

		String USER_IP = request.getRemoteAddr();
		   
		if (USER_IP.equalsIgnoreCase(LOCAL_IP_6)) {
		    InetAddress inetAddress;
			try {
				inetAddress = InetAddress.getLocalHost();
				String ipAddress = inetAddress.getHostAddress();
			    USER_IP = ipAddress;
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		}
		pointRoulette.setIpaddr(USER_IP);
		
		
		return channelDao.channelUpdate(pointRoulette);	
	}

	@Override
	public int channelDelete(String roulette_id) {
		return channelDao.channelDelete(roulette_id);	
	}

	

}
