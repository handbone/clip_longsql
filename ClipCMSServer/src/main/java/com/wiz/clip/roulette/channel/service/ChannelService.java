/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.roulette.channel.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;

import com.wiz.clip.roulette.channel.bean.PointRoulette;

public interface ChannelService {

	List<PointRoulette> getRouletteList(ModelMap map);

	int getRouletteListCnt(ModelMap map);

	int channelInsert(HttpServletRequest request, PointRoulette pointRoulette);

	PointRoulette getChannel(String roulette_id);

	int channelUpdate(HttpServletRequest request, PointRoulette pointRoulette);

	int channelDelete(String roulette_id);

}
