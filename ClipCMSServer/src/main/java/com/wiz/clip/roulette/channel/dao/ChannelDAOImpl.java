/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.roulette.channel.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.wiz.clip.roulette.channel.bean.PointRoulette;



@Repository
public class ChannelDAOImpl implements ChannelDAO  {

	@Autowired
	private SqlSession postgreSession;

	Logger logger = Logger.getLogger(ChannelDAOImpl.class.getName());
	
	@Override
	public List<PointRoulette> getRouletteList(ModelMap map) {
		return postgreSession.selectList("CHANNEL.getRouletteList", map);
	}

	@Override
	public int getRouletteListCnt(ModelMap map) {
		return postgreSession.selectOne("CHANNEL.getRouletteListCnt",map);
	}

	@Override
	public int channelInsert(PointRoulette pointRoulette) {
		return postgreSession.insert("CHANNEL.channelInsert",pointRoulette);
	}

	@Override
	public PointRoulette getChannel(String roulette_id) {
		return postgreSession.selectOne("CHANNEL.getChannel",roulette_id);
	}

	@Override
	public int channelUpdate(PointRoulette pointRoulette) {
		return postgreSession.update("CHANNEL.channelUpdate",pointRoulette);
	}

	@Override
	public int channelDelete(String roulette_id) {
		return postgreSession.delete("CHANNEL.channelDelete",roulette_id);
	}
	
	

}
