/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.roulette.channel.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.roulette.channel.bean.PointRoulette;
import com.wiz.clip.roulette.channel.service.ChannelService;

@Controller
public class ChannelController  {
	Logger logger = Logger.getLogger(ChannelController.class.getName());

	@Autowired
	private ChannelService channelService;

	@Value("#{config['PAGE_SIZE']}") int PAGE_SIZE;
	
	/*
	 * 리스트
	 */
	@RequestMapping(value = "/roulette/channel/channelList.do")
	public ModelAndView channelList(@RequestParam(value = "pg", required = false) String page,
			@RequestParam(required = false) String status, @RequestParam(required = false) String keywd, ModelMap map) {

		logger.debug("status :" + status);
		logger.debug("keywd :" + keywd);

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("startNum", startNum);
		map.put("endNum", endNum);

		// 검색어 관련
		if (status != null && !status.equals("") && keywd != null && !keywd.equals("")) {
			map.put("status", status);
			map.put("keywd", keywd);
		} else {
			map.put("status", "");
			map.put("keywd", "");
		}

		List<PointRoulette> list = channelService.getRouletteList(map);

		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		// 토탈 카운트
		paging.makePagingHtml(channelService.getRouletteListCnt(map));

		map.clear();
		map.put("pg", pg);
		map.put("list", list);
		map.put("keywd", keywd);
		map.put("status", status);
		map.put("boardPaging", paging);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "채널관리");
		mav.addObject("display", "../clip/roulette/channel/channelList.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 입력폼
	 */
	@RequestMapping(value = "/roulette/channel/channelInsertForm.do")
	public ModelAndView channelInsertForm(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("channelInsertForm Page start!!!");
		ModelAndView mav = new ModelAndView();

		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "채널관리");
		mav.addObject("display", "../clip/roulette/channel/channelInsertForm.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 입력처리
	 */
	@RequestMapping(value = "/roulette/channel/channelInsert.do")
	public ModelAndView channelInsert(HttpServletRequest request, HttpServletResponse response, ModelMap map,
			PointRoulette pointRoulette) {

		logger.debug("channelInsert Page start!!!");
		ModelAndView mav = new ModelAndView();

		channelService.channelInsert(request, pointRoulette);

		map.addAttribute("msg", "작성하신 내용이 정상적으로 등록되었습니다.");
		map.addAttribute("url", "/roulette/channel/channelList.do");
		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 상세보기
	 */
	@RequestMapping(value = "/roulette/channel/channelView.do")
	public ModelAndView channelView(@RequestParam String roulette_id, @RequestParam(required = false) String page,
			ModelMap map) {
		logger.debug("channelView Page start!!!");
		
		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		PointRoulette pointRoulette = channelService.getChannel(roulette_id);

		map.put("pg", pg);
		map.put("pointRoulette", pointRoulette);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "채널관리");
		mav.addObject("display", "../clip/roulette/channel/channelView.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 수정폼
	 */
	@RequestMapping(value = "/roulette/channel/channelUpdateForm.do")
	public ModelAndView channelUpdateForm(@RequestParam String roulette_id, @RequestParam(required = false) String page,
			ModelMap map) {
		logger.debug("channelUpdateForm Page start!!!");
		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		PointRoulette pointRoulette = channelService.getChannel(roulette_id);

		map.put("pg", pg);
		map.put("pointRoulette", pointRoulette);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "채널관리");
		mav.addObject("display", "../clip/roulette/channel/channelUpdateForm.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 수정처리
	 */
	@RequestMapping(value = "/roulette/channel/channelUpdate.do")
	public ModelAndView channelUpdate(
			@RequestParam(required = false) String roulette_id,
			@RequestParam(required = false) String page,
			HttpServletRequest request, HttpServletResponse response, ModelMap map,
			PointRoulette pointRoulette
			) throws java.lang.Exception {

		logger.debug("channelUpdate Page start!!!");
		ModelAndView mav = new ModelAndView();

		String pg = page;

		if (pg == null || pg == ""){
			pg = "1";
		}

		channelService.channelUpdate(request, pointRoulette);

		map.addAttribute("msg", "작성하신 내용이 정상적으로 수정되었습니다.");
		map.addAttribute("url", "/roulette/channel/channelView.do?roulette_id=" + roulette_id + "&pg=" + pg);

		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}
	
	/*
	 * 삭제처리
	 */
	@RequestMapping(value = "/roulette/channel/channelDelete.do")
	public ModelAndView channelDelete(@RequestParam String roulette_id,
			@RequestParam(value = "pg", required = false) String page, ModelMap map) {
		logger.debug("channelDelete Page start!!!");

		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		channelService.channelDelete(roulette_id);

		map.put("pg", pg);

		ModelAndView mav = new ModelAndView();
		map.addAttribute("msg", "정상적으로 삭제 되었습니다.");
		map.addAttribute("url", "/roulette/channel/channelList.do");
		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

}
