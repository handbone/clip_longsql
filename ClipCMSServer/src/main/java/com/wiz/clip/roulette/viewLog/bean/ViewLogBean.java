/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.roulette.viewLog.bean;

public class ViewLogBean  {
	//JoinByChannel
	private String sdate;
	private String joinSum;
	private String savePointSum;
	private String channelname;
	private String joinSumPlusZero;
	
	//DetailByChannel
	//private String channelname;
	private String roulette_id;
	//private String sdate;
	private String idx;
	private String area01;
	private String area02;
	private String area03;
	private String area04;
	private String area05;
	private String area06;
	private String sumPoint01;
	private String sumPoint02;
	private String sumPoint03;
	private String sumPoint04;
	private String sumPoint05;
	private String sumPoint06;
	
	//pageView
	private String pageView;
	//private String channelname;
	//private String sdate;
	
	
	//connectAPI
	private String cust_id;
	private String ga_id;
	//private String ctn;
	private String user_ci;
	private String apiurl;
	private String tr_id;
	private String tr_response;
	private String ipaddr;
	private String regdate;
	
	//joinEvent
	//private String channelname;
	//private String cust_id;
	//private String ga_id;
	private String ctn;
	//private String sdate;
	//private String ipaddr;
	//private String regdate;
	
	//timeGbn
	//private String channelname;
	private String settime;
	private String setnum;
	//private String ipaddr;
	//private String regdate;
	//private String sdate;
	
	//dailyJoinSum = //DetailByChannel
	
	
	
	
	
	
	
	
	
	public String getSdate() {
		return sdate;
	}
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getGa_id() {
		return ga_id;
	}
	public void setGa_id(String ga_id) {
		this.ga_id = ga_id;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getApiurl() {
		return apiurl;
	}
	public void setApiurl(String apiurl) {
		this.apiurl = apiurl;
	}
	public String getTr_id() {
		return tr_id;
	}
	public void setTr_id(String tr_id) {
		this.tr_id = tr_id;
	}
	public String getTr_response() {
		return tr_response;
	}
	public void setTr_response(String tr_response) {
		this.tr_response = tr_response;
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getSettime() {
		return settime;
	}
	public void setSettime(String settime) {
		this.settime = settime;
	}
	public String getSetnum() {
		return setnum;
	}
	public void setSetnum(String setnum) {
		this.setnum = setnum;
	}
	public String getPageView() {
		return pageView;
	}
	public void setPageView(String pageView) {
		this.pageView = pageView;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	public String getJoinSum() {
		return joinSum;
	}
	public void setJoinSum(String joinSum) {
		this.joinSum = joinSum;
	}
	public String getSavePointSum() {
		return savePointSum;
	}
	public void setSavePointSum(String savePointSum) {
		this.savePointSum = savePointSum;
	}
	public String getChannelname() {
		return channelname;
	}
	public void setChannelname(String channelname) {
		this.channelname = channelname;
	}
	public String getJoinSumPlusZero() {
		return joinSumPlusZero;
	}
	public void setJoinSumPlusZero(String joinSumPlusZero) {
		this.joinSumPlusZero = joinSumPlusZero;
	}
	public String getRoulette_id() {
		return roulette_id;
	}
	public void setRoulette_id(String roulette_id) {
		this.roulette_id = roulette_id;
	}
	public String getArea01() {
		return area01;
	}
	public void setArea01(String area01) {
		this.area01 = area01;
	}
	public String getArea02() {
		return area02;
	}
	public void setArea02(String area02) {
		this.area02 = area02;
	}
	public String getArea03() {
		return area03;
	}
	public void setArea03(String area03) {
		this.area03 = area03;
	}
	public String getArea04() {
		return area04;
	}
	public void setArea04(String area04) {
		this.area04 = area04;
	}
	public String getArea05() {
		return area05;
	}
	public void setArea05(String area05) {
		this.area05 = area05;
	}
	public String getArea06() {
		return area06;
	}
	public void setArea06(String area06) {
		this.area06 = area06;
	}
	public String getSumPoint01() {
		return sumPoint01;
	}
	public void setSumPoint01(String sumPoint01) {
		this.sumPoint01 = sumPoint01;
	}
	public String getSumPoint02() {
		return sumPoint02;
	}
	public void setSumPoint02(String sumPoint02) {
		this.sumPoint02 = sumPoint02;
	}
	public String getSumPoint03() {
		return sumPoint03;
	}
	public void setSumPoint03(String sumPoint03) {
		this.sumPoint03 = sumPoint03;
	}
	public String getSumPoint04() {
		return sumPoint04;
	}
	public void setSumPoint04(String sumPoint04) {
		this.sumPoint04 = sumPoint04;
	}
	public String getSumPoint05() {
		return sumPoint05;
	}
	public void setSumPoint05(String sumPoint05) {
		this.sumPoint05 = sumPoint05;
	}
	public String getSumPoint06() {
		return sumPoint06;
	}
	public void setSumPoint06(String sumPoint06) {
		this.sumPoint06 = sumPoint06;
	}
	
	
	
	
	
	
}
