/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.mypoint.banner.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.wiz.clip.mypoint.banner.bean.BannerBean;
import com.wiz.clip.roulette.channel.dao.ChannelDAOImpl;

@Repository
public class BannerDAOImpl implements BannerDAO{

	@Autowired
	private SqlSession postgreSession;

	Logger logger = Logger.getLogger(BannerDAOImpl.class.getName());
	
	@Override
	public List<BannerBean> getBannerList(ModelMap map){
		return postgreSession.selectList("BANNER.getBannerList",map);
	}
	
	@Override
	public int getBannerListCnt(ModelMap map){
		return postgreSession.selectOne("BANNER.getBannerListCnt",map);
	}
	
	@Override
	public int addBanner(BannerBean bannerBean){
		return postgreSession.insert("BANNER.addBanner",bannerBean);
	}
	
	@Override
	public BannerBean bannerView(int idx){
		return postgreSession.selectOne("BANNER.bannerView",idx);
	}
	
	@Override
	public int bannerUpdate(BannerBean bannerBean){
		return postgreSession.update("BANNER.bannerUpdate",bannerBean);
	}
	
	@Override
	public int bannerDelete(int idx){
		return postgreSession.delete("BANNER.bannerDelete",idx);
	}
}
