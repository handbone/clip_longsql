/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.mypoint.banner.bean;

public class BannerBean {
	
	private int idx;
	private String bannername;
	private String bannerimage;
	private String startdate;
	private String enddate;
	private String status;
	private String linktype;
	private String linkurl;
	private String regdate;
	private String userid;
	private String ipaddr;
	private String modiipaddr;
	private String modidate;
	private String androidgbn;
	private String iosgbn;
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getBannername() {
		return bannername;
	}
	public void setBannername(String bannername) {
		this.bannername = bannername;
	}
	public String getBannerimage() {
		return bannerimage;
	}
	public void setBannerimage(String bannerimage) {
		this.bannerimage = bannerimage;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLinktype() {
		return linktype;
	}
	public void setLinktype(String linktype) {
		this.linktype = linktype;
	}
	public String getLinkurl() {
		return linkurl;
	}
	public void setLinkurl(String linkurl) {
		this.linkurl = linkurl;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	public String getModiipaddr() {
		return modiipaddr;
	}
	public void setModiipaddr(String modiipaddr) {
		this.modiipaddr = modiipaddr;
	}
	public String getModidate() {
		return modidate;
	}
	public void setModidate(String modidate) {
		this.modidate = modidate;
	}
	public String getAndroidgbn() {
		return androidgbn;
	}
	public void setAndroidgbn(String androidgbn) {
		this.androidgbn = androidgbn;
	}
	public String getIosgbn() {
		return iosgbn;
	}
	public void setIosgbn(String iosgbn) {
		this.iosgbn = iosgbn;
	}
	
	

}
