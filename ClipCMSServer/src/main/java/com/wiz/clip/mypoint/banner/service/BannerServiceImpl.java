/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.mypoint.banner.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.wiz.clip.mypoint.banner.bean.BannerBean;
import com.wiz.clip.mypoint.banner.dao.BannerDAO;
import com.wiz.clip.roulette.channel.service.ChannelServiceImpl;
import com.wiz.clip.security.bean.CmsUserInfo;

@Service
@Transactional
public class BannerServiceImpl implements BannerService{
	
	@Autowired
	private BannerDAO bannerDao;
	
	Logger logger = Logger.getLogger(BannerServiceImpl.class.getName());
	
	@Override
	public List<BannerBean> getBannerList(ModelMap map){
		return bannerDao.getBannerList(map);
	}

	@Override
	public int getBannerListCnt(ModelMap map){
		return bannerDao.getBannerListCnt(map);
	}
	
	@Override
	public int addBanner(BannerBean bannerBean){
		
		CmsUserInfo user = (CmsUserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		bannerBean.setUserid(user.getUserid());
		bannerBean.setIpaddr(user.getIpaddr());
		
		return bannerDao.addBanner(bannerBean);
		
	}
	
	@Override
	public BannerBean bannerView(int idx){
		return bannerDao.bannerView(idx);
	}
	
	@Override
	public int bannerUpdate(BannerBean bannerBean){
		
		CmsUserInfo user = (CmsUserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		bannerBean.setUserid(user.getUserid());
		bannerBean.setModiipaddr(user.getIpaddr());
		
		return bannerDao.bannerUpdate(bannerBean);
	}
	
	@Override 
	public int bannerDelete(int idx){
		return bannerDao.bannerDelete(idx);
	}
}
