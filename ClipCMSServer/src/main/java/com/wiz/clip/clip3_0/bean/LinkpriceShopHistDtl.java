package com.wiz.clip.clip3_0.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class LinkpriceShopHistDtl {
	
	 private String idx;
	 private String main_idx;
	 private String trlog_id;
	 private String user_token;
	 private String day;
	 private String time;
	 private String m_id;
	 private String o_cd;
	 private String p_cd;
	 private String p_nm;
	 private String it_cnt ;
	 private String sales;
	 private String commission;
	 private String status;
	 private String regdate;
	 
	 // 쇼핑몰 이름
	 private String m_name;
	 
	public String getM_name() {
		return m_name;
	}
	public void setM_name(String m_name) {
		this.m_name = m_name;
	}
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getMain_idx() {
		return main_idx;
	}
	public void setMain_idx(String main_idx) {
		this.main_idx = main_idx;
	}
	public String getTrlog_id() {
		return trlog_id;
	}
	public void setTrlog_id(String trlog_id) {
		this.trlog_id = trlog_id;
	}
	public String getUser_token() {
		return user_token;
	}
	public void setUser_token(String user_token) {
		this.user_token = user_token;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getM_id() {
		return m_id;
	}
	public void setM_id(String m_id) {
		this.m_id = m_id;
	}
	public String getO_cd() {
		return o_cd;
	}
	public void setO_cd(String o_cd) {
		this.o_cd = o_cd;
	}
	public String getP_cd() {
		return p_cd;
	}
	public void setP_cd(String p_cd) {
		this.p_cd = p_cd;
	}
	public String getP_nm() {
		return p_nm;
	}
	public void setP_nm(String p_nm) {
		this.p_nm = p_nm;
	}
	public String getIt_cnt() {
		return it_cnt;
	}
	public void setIt_cnt(String it_cnt) {
		this.it_cnt = it_cnt;
	}
	public String getSales() {
		return sales;
	}
	public void setSales(String sales) {
		this.sales = sales;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	 
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	} 
	  
}
