package com.wiz.clip.clip3_0.bean;

public class ScreenLockDAO {
	
	private String day_count;
	private String content;
	private String link_url;
	private String expose_yn;
	private String regdate;
	
	
	public String getDay_count() {
		return day_count;
	}
	public void setDay_count(String day_count) {
		this.day_count = day_count;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getLink_url() {
		return link_url;
	}
	public void setLink_url(String link_url) {
		this.link_url = link_url;
	}
	public String getExpose_yn() {
		return expose_yn;
	}
	public void setExpose_yn(String expose_yn) {
		this.expose_yn = expose_yn;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	

}
