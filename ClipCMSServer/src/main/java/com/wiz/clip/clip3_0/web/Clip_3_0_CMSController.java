/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.clip3_0.web;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.common.util.HttpClientUtil;
import com.wiz.clip.clip3_0.bean.CancelPointSwapInfo;
import com.wiz.clip.clip3_0.bean.CardPointSwapList;
import com.wiz.clip.clip3_0.bean.LinkPriceItemInfo;
import com.wiz.clip.clip3_0.bean.LinkPriceReportItem;
import com.wiz.clip.clip3_0.bean.LinkPriceReportMessege;
import com.wiz.clip.clip3_0.bean.LinkpriceShopHistDtl;
import com.wiz.clip.clip3_0.bean.MainBannerDO;
import com.wiz.clip.clip3_0.bean.PointCouponHist;
import com.wiz.clip.clip3_0.bean.PointbombInfo;
import com.wiz.clip.clip3_0.bean.ScreenLockDAO;
import com.wiz.clip.clip3_0.bean.ScreenLockRandomPointMDAO;
import com.wiz.clip.clip3_0.bean.UserTokenInfo;
import com.wiz.clip.clip3_0.service.Clip_3_0_Service;
import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.manage.pointChk.bean.SearchBean;


@Controller
public class Clip_3_0_CMSController  {
	
	Logger logger = Logger.getLogger(Clip_3_0_CMSController.class.getName());

	private int PAGE_SIZE = 20;
	
	@Autowired
	public Clip_3_0_Service clip_3_0_service;
	
	@Value("#{config['API_DOMAIN']}")
	String API_DOMAIN;
	
	@Value("#{config['AD_DOMAIN']}")
	String AD_DOMAIN;
	
	/***************************************************************************************************
	 * 
	 * Page Controller
	 * 
	 */
	
	/**
	 * 카드포인트 전환 조회
	 * @return ModelAndView 
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/clip3_0/KTCPMW01.do", method = RequestMethod.POST)
	public ModelAndView cardpoint_change_search( HttpServletRequest req, HttpServletResponse res,
												@RequestParam(value = "pg", required = false) String page,
												@RequestParam(value = "status", required = false) String status,
												@RequestParam(value = "keywd", required = false) String keywd,
												@RequestParam(value = "datepicker1", required = false) String datepicker1,
												@RequestParam(value = "datepicker2", required = false) String datepicker2,
												@RequestParam(value = "card_com", required = false) String card_com,
												@RequestParam(value = "use_status", required = false) String use_status,
												@RequestParam(value = "value", required = false) String value,
												ModelMap map) throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/main/index");
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "카드포인트 전환 조회");
		mav.addObject("display", "../clip/point_3_0/search/cardpoint_change_search.jsp");
		
		HttpSession session = req.getSession();
		
		boolean isfirst = false;
		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
			isfirst = true;
		}else{
			status = (String) session.getAttribute("status");
			keywd = (String) session.getAttribute("keywd");
			datepicker1 = (String) session.getAttribute("datepicker1");
			datepicker2 = (String) session.getAttribute("datepicker2");
			card_com = (String) session.getAttribute("card_com");
			use_status = (String) session.getAttribute("use_status");
			value = (String) session.getAttribute("value");
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("startNum", startNum);
		map.put("endNum", endNum);
		
		if(datepicker1 != null && !datepicker1.isEmpty()) {
			datepicker1 = datepicker1 + "000000";
		}
		if(datepicker2 != null && !datepicker2.isEmpty()) {
			datepicker2 = datepicker2 + "235959";
		}
		
		if (isfirst == false && Integer.parseInt(pg) >= 1) {
			
			if (status != null && !status.isEmpty()) {
				map.put("status", status);
			}else{
				map.put("status", "");
				status = "";
			}
			if (keywd != null && !keywd.isEmpty()) {
				map.put("keywd", keywd);
			}else {
				keywd = "";
			}
			if (datepicker1 != null && !datepicker1.isEmpty()) {
				map.put("datepicker1", datepicker1);
			}
			if (datepicker2 != null && !datepicker2.isEmpty()) {
				map.put("datepicker2", datepicker2);
			}
			if (card_com != null && !card_com.isEmpty() && !card_com.equals("0")) {
				map.put("card_com", card_com);
			}
			if (use_status != null && !use_status.isEmpty() && !use_status.equals("0")) {
				map.put("use_status", use_status);
			}
			if (value != null && !value.isEmpty() && !value.equals("0")) {
				map.put("value", value);
			}
			
		}else {
			
			
			if (status != null && !status.isEmpty()) {
				map.put("status", status);
			}else{
				map.put("status", "");
				status = "";
			}
			if (keywd != null && !keywd.isEmpty()) {
				map.put("keywd", keywd);
			}else {
				keywd = "";
			}
			if (datepicker1 != null && !datepicker1.isEmpty()) {
				map.put("datepicker1", datepicker1);
			}
			if (datepicker2 != null && !datepicker2.isEmpty()) {
				map.put("datepicker2", datepicker2);
			}
			if (card_com != null && !card_com.isEmpty() && !card_com.equals("0")) {
				map.put("card_com", card_com);
			}
			if (use_status != null && !use_status.isEmpty() && !use_status.equals("0")) {
				map.put("use_status", use_status);
			}
			if (value != null && !value.isEmpty() && !value.equals("0")) {
				map.put("value", value);
			}
			
			
		}
		
		System.out.println("################## startNum : "+startNum);
		System.out.println("################## endNum : "+endNum);
		
		List<UserTokenInfo> userinfo = null;
		List<CardPointSwapList> list = new ArrayList<CardPointSwapList>();
		
		if(keywd != null && !keywd.isEmpty()){
			userinfo = clip_3_0_service.selectUserCi(map);
		}
		
		String userCi = "";
		
		if(userinfo != null){
			for(UserTokenInfo user : userinfo){
				if (user.getUserCi() != null && !user.getUserCi().isEmpty()) {
//					model.put("user_ci", user.getUserCi());
					map.put("user_ci", user.getUserCi());
					userCi = user.getUserCi();
				}
//				totalcount = clip_3_0_service.selectCouponListCount(model);
//				sum += Integer.parseInt(totalcount);
//				totalvalue = clip_3_0_service.selectCoupontotalPoint(model);
//				if(totalvalue != null && !totalvalue.isEmpty())
//					sumvalue += Integer.parseInt(totalvalue);
				list.addAll((List<CardPointSwapList>)clip_3_0_service.getCardpointChangeList(map));
			}
		}
		else if(keywd.equals("")&&status.equals("")) {
			list = (List<CardPointSwapList>) clip_3_0_service.getCardpointChangeList(map);
		}
		
//		List<CardPointSwapList> list = (List<CardPointSwapList>) clip_3_0_service.getCardpointChangeList(map);
		try {
		for(CardPointSwapList point : list){
			logger.debug(point.toString());
			String index = point.getRes_code();
			String card_kind = point.getReg_source();
			if(index == null||index.isEmpty()){
				continue;
			}
			int i = Integer.parseInt(index);
			if(card_kind.equals("shinhan")){
				if(i >= 0 && i <= 0){
					point.setRes_message(shinhanResCodeMessage[i][1]);
				}else if(i >= 31 && i <= 33){
					point.setRes_message(shinhanResCodeMessage[i-30][1]);
				}else if(i >= 36 && i <= 36){
					point.setRes_message(shinhanResCodeMessage[i-32][1]);
				}else if(i >= 94 && i <= 96){
					point.setRes_message(shinhanResCodeMessage[i-89][1]);
				}else if(i >= 332 && i <= 332){
					point.setRes_message(shinhanResCodeMessage[i-324][1]);
				}else if(i >= 402 && i <= 402){
					point.setRes_message(shinhanResCodeMessage[i-393][1]);
				}else if(i >= 1001 && i <= 1003){
					point.setRes_message(shinhanResCodeMessage[i-991][1]);
				}else if(i >= 1011 && i <= 1021){
					point.setRes_message(shinhanResCodeMessage[i-998][1]);
				}else if(i >= 1029 && i <= 1029){
					point.setRes_message(shinhanResCodeMessage[i-1005][1]);
				}else if(i >= 1201 && i <= 1204){
					point.setRes_message(shinhanResCodeMessage[i-1176][1]);
				}else if(i >= 1301 && i <= 1303){
					point.setRes_message(shinhanResCodeMessage[i-1272][1]);
				}else if(i >= 1400 && i <= 1418){
					point.setRes_message(shinhanResCodeMessage[i-1368][1]);
				}else if(i >= 1501 && i <= 1510){
					point.setRes_message(shinhanResCodeMessage[i-1450][1]);
				}else if(i >= 2001 && i <= 2017){
					point.setRes_message(shinhanResCodeMessage[i-1940][1]);
				}else if(i >= 8888 && i <= 8888){
					point.setRes_message(shinhanResCodeMessage[i-8810][1]);
				}else if(i >= 9998 && i <= 9999){
					point.setRes_message(shinhanResCodeMessage[i-9919][1]);
				}else if(i == 38) {
					point.setRes_message(shinhanResCodeMessage[81][1]);
				}
				else{
					point.setRes_message("알수없는 응답코드");
				}
			}else if(card_kind.equals("hanamembers")){
				if(i >= 0 && i <= 108){
					point.setRes_message(hanaResCodeMessage[i][1]);
				}else if(i >= 1001 && i <= 1013){
					point.setRes_message(hanaResCodeMessage[i-892][1]);
				}else if(i >= 1101 && i <= 1119){
					point.setRes_message(hanaResCodeMessage[i-978][1]);
				}else if(i >= 1901 && i <= 2006){
					point.setRes_message(hanaResCodeMessage[i-1769][1]);
				}else if(i >= 2116 && i <= 2124){
					point.setRes_message(hanaResCodeMessage[i-1978][1]);
				}else if(i >= 3001 && i <= 3006){
					point.setRes_message(hanaResCodeMessage[i-2854][1]);
				}else if(i >= 3102 && i <= 3108){
					point.setRes_message(hanaResCodeMessage[i-2949][1]);
				}else if(i >= 3500 && i <= 3500){
					point.setRes_message(hanaResCodeMessage[i-3340][1]);
				}else if(i >= 4001 && i <= 4003){
					point.setRes_message(hanaResCodeMessage[i-3840][1]);
				}else if(i >= 5001 && i <= 5065){
					point.setRes_message(hanaResCodeMessage[i-4837][1]);
				}else if(i >= 6001 && i <= 6027){
					point.setRes_message(hanaResCodeMessage[i-5772][1]);
				}else{
					point.setRes_message("알수없는 응답코드");
				}
			}else if(card_kind.equals("bccard")) {
				if(i == 0) {
					point.setRes_message(bcResCodeMessage[i][1]);
				}else if(i == 17) {
					point.setRes_message(bcResCodeMessage[i - 16][1]);
				}else if(i == 19) {
					point.setRes_message(bcResCodeMessage[i - 16][1]);
				}else if(i >= 1100 && i <= 1101) {
					point.setRes_message(bcResCodeMessage[i - 1097][1]);
				}else if(i == 1103) {
					point.setRes_message(bcResCodeMessage[i - 1098][1]);
				}else if(i == 1105) {
					point.setRes_message(bcResCodeMessage[i - 1099][1]);
				}else if(i >= 1108 && i <= 1109) {
					point.setRes_message(bcResCodeMessage[i - 1101][1]);
				}else if(i >= 1201 && i <= 1202) {
					point.setRes_message(bcResCodeMessage[i - 1192][1]);
				}else if(i == 1204) {
					point.setRes_message(bcResCodeMessage[i - 1193][1]);
				}else if(i >= 1209 && i <= 1210) {
					point.setRes_message(bcResCodeMessage[i - 1197][1]);
				}else if(i >= 1213 && i <= 1215) {
					point.setRes_message(bcResCodeMessage[i - 1198][1]);
				}else if(i == 1302) {
					point.setRes_message(bcResCodeMessage[i - 1184][1]);
				}else if(i == 1801) {
					point.setRes_message(bcResCodeMessage[i - 1782][1]);
				}else if(i == 1805) {
					point.setRes_message(bcResCodeMessage[i - 1785][1]);
				}else if(i == 16) {
					point.setRes_message(bcResCodeMessage[i + 5][1]);
				}
				else {
					point.setRes_message("알수없는 응답코드");
				}
			} else if(card_kind.equals("kbpointree")) {
				if(i == 1) {
					point.setRes_message(kbResCodeMessage[i - 1][1]);
				}else if(i == 17) {
					point.setRes_message(kbResCodeMessage[i - 16][1]);
				}else if(i == 90) {
					point.setRes_message(kbResCodeMessage[i - 88][1]);
				}else if(i >= 93 && i <= 99) {
					point.setRes_message(kbResCodeMessage[i - 90][1]);
				}else if(i == 0) {
					point.setRes_message(kbResCodeMessage[i + 10][1]);
				}else if(i == 91) {
					point.setRes_message(kbResCodeMessage[i - 80][1]);
				}
				else {
					point.setRes_message("알수없는 응답코드");
				}
			}
			
			else{
				point.setRes_message(index);
			}
			
		}
		}catch(Exception e) {
			logger.error(e.getMessage(), e);
			logger.debug(e.getMessage());
		}
		String totalvalue = clip_3_0_service.getCardpointChangeListTotalValue(map);
//		String cancelvalue = clip_3_0_service.selectCardpointChangeCancelvalue(map);
//		Integer iTotalvalue = 0;
//		if(totalvalue == null){
//			totalvalue = "0";
//		}else if(cancelvalue != null){
//			iTotalvalue = Integer.parseInt(totalvalue) - Integer.parseInt(cancelvalue);
//			totalvalue = iTotalvalue.toString();
//		}
		String totalcount = clip_3_0_service.getCardpointChangeListTotalCount(map);
		String totalpoint = clip_3_0_service.selectCardpointTotalPoint(map);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		paging.makePagingHtml(Integer.parseInt(totalcount));
		
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		/**
		 * 
		 */
		
		if(datepicker1 != null && !datepicker1.isEmpty()) {
			datepicker1 = datepicker1.substring(0, 8);
		}
		if(datepicker2 != null && !datepicker2.isEmpty()) {
			datepicker2 = datepicker2.substring(0, 8);
		}
		
		session.setAttribute("totalcount", totalcount);
		if (status != null && !status.isEmpty()) {
			session.setAttribute("status", status);
			mav.addObject("status", status);
		}else{
			session.setAttribute("status", status);
			mav.addObject("status", status);
		}
		if (keywd != null && !keywd.isEmpty()) {
			session.setAttribute("keywd", keywd);
			mav.addObject("keywd", keywd);
		}else{
			session.setAttribute("keywd", keywd);
			mav.addObject("keywd", keywd);
		}
		if (datepicker1 != null && !datepicker1.isEmpty()) {
			session.setAttribute("datepicker1", datepicker1);
			mav.addObject("datepicker1", datepicker1);
		}else{
			session.setAttribute("datepicker1", datepicker1);
			mav.addObject("datepicker1", datepicker1);
		}
		if (datepicker2 != null && !datepicker2.isEmpty()) {
			session.setAttribute("datepicker2", datepicker2);
			mav.addObject("datepicker2", datepicker2);
		}else{
			session.setAttribute("datepicker2", datepicker2);
			mav.addObject("datepicker2", datepicker2);
		}
		if (card_com != null && !card_com.isEmpty() && !card_com.equals("0")) {
			session.setAttribute("card_com", card_com);
			mav.addObject("card_com", card_com);
		}else{
			session.setAttribute("card_com", card_com);
			mav.addObject("card_com", "0");
		}
		if (use_status != null && !use_status.isEmpty() && !use_status.equals("0")) {
			session.setAttribute("use_status", use_status);
			mav.addObject("use_status", use_status);
		}else{
			session.setAttribute("use_status", use_status);
			mav.addObject("use_status", "0");
		}
		if (value != null && !value.isEmpty()) {
			session.setAttribute("value", value);
			mav.addObject("value", value);
		}else{
			session.setAttribute("value", value);
			mav.addObject("value", value);
		}
		if(userCi != null && !userCi.isEmpty()) {
			session.setAttribute("user_ci", userCi);
			mav.addObject("user_ci", userCi);
		}
		
		mav.addObject("totalpoint", totalpoint);
		mav.addObject("totalvalue", totalvalue);
		mav.addObject("totalcount", totalcount);		
		mav.addObject("list", list);
		mav.addObject("userCi", userCi);
		return mav;
		
	}

	/**
	 * 쇼핑적립 적립예정 내역 조회
	 * @author jyi
	 * @return ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW02.do", method = RequestMethod.POST)
	public ModelAndView shoppingsaving_pre_saving_list_search(HttpServletRequest request, ModelMap model, SearchBean searchBean
			, @RequestParam(value = "pg", required = false) String page
			, @RequestParam(value = "requester_code", required = false) String requester_code) throws java.lang.Exception {
		ModelAndView mav = new ModelAndView();
		HttpSession session = request.getSession();

		mav.setViewName("/main/index");
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "쇼핑적립 적립예정 내역 조회");
		mav.addObject("display", "../clip/point_3_0/search/shoppingsaving_pre_saving_list_search.jsp");
		
		System.out.println(searchBean.toString());
		
		boolean isfirst = false;
		String pg = page;
		String status = "";
		String keywd = "";
		String point_type = "";
		
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
			isfirst = true;
		}else{
			status = (String) session.getAttribute("status");
			keywd = (String) session.getAttribute("keywd");
			point_type = (String) session.getAttribute("point_type");
			requester_code = (String) session.getAttribute("requester_code");
		}
		
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		model.put("startNum", startNum);
		model.put("endNum", endNum);
		
		if (isfirst == false && Integer.parseInt(pg) >= 1) {
			
			if (status != null && !status.isEmpty()) {
				model.put("status", status);
				searchBean.setStatus(status);
			}else{
				model.put("status", "");
				searchBean.setStatus("");
			}
			if (keywd != null && !keywd.isEmpty()) {
				model.put("keywd", keywd);
				searchBean.setKeywd(keywd);
			}
			if (point_type != null && !point_type.isEmpty()) {
				model.put("point_type", point_type);
			}else{
				point_type = "N";
				model.put("point_type", point_type);
			}
			if (requester_code != null && !requester_code.isEmpty()) {
				model.put("requester_code", requester_code);
			}
			
		}else {
			status = searchBean.getStatus();
			keywd = searchBean.getKeywd();
			point_type = searchBean.getPoint_type();
			
			if (status != null && !status.isEmpty()) {
				model.put("status", status);
			}else{
				model.put("status", "");
				searchBean.setStatus("");
			}
			if (keywd != null && !keywd.isEmpty()) {
				model.put("keywd", keywd);
			}
			if (point_type != null && !point_type.isEmpty()) {
				model.put("point_type", point_type);
			}else{
				point_type = "N";
				model.put("point_type", point_type);
			}
			if (requester_code != null && !requester_code.isEmpty()) {
				model.put("requester_code", requester_code);
			}
			
		}
		if(checkParameter(model)) {
			model.addAttribute("reqCode", "PE");
			return mav;
		}
		
		List<UserTokenInfo> userinfo = null;
		LinkPriceReportMessege msg = new LinkPriceReportMessege();
		String userToken = "";
		int sum = 0;
		int totalcount = 0;
		
		if(keywd != null){
			userinfo = clip_3_0_service.selectUserToken(searchBean);
		}
	
//		if(userinfo != null){
//			for(@SuppressWarnings("unused") UserTokenInfo user : userinfo){
//				System.out.println("######USER_INFO_TBL DATA -> " + userinfo.toString());
//			}
//		}
		
		if(userinfo != null){
			for(UserTokenInfo info : userinfo){
				msg.setuId(info.getUserToken());
				userToken = info.getUserToken();
			
				if(point_type.equals("I")){
					msg = clip_3_0_service.getNextSavingHistory(msg);
					logger.debug("##########msg :: " + msg.getResultCode() + " " + msg.getResultMsg());
				} else if(point_type.equals("O")){
					msg = clip_3_0_service.getNext2SavingHistory(msg);
					logger.debug("##########msg :: " + msg.getResultCode() + " " + msg.getResultMsg());
				} else {
					msg = clip_3_0_service.getNowSavingHistory(msg);
					logger.debug("##########msg :: " + msg.getResultCode() + " " + msg.getResultMsg());
				}
				
				if( msg.getOrder_list() != null) {
//					for(LinkPriceReportItem item : msg.getOrder_list()){
//						String itemCount = clip_3_0_service.selectItemCount(item);
//						item.setItemCount(itemCount);
//						sum += Integer.parseInt(item.getCommission());
//						logger.debug("item :: " + item.getOrder_code() + "itemcount :: " + item.getItemCount());
//					}
				}
			}
		}
		
		if( msg.getOrder_list() != null &&  msg.getOrder_list().size() > 0 ) {
			totalcount = msg.getOrder_list().size();
		} else {
			logger.debug("적립 예정 포인트 내역이 없습니다.");
		}
		//totalcount = msg.getOrder_list().size();
		
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", msg.getOrder_list());
		mav.addObject("totalcount", totalcount);
		mav.addObject("order_sum", sum);
		
		if(searchBean.getStatus().equals("ctn")){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		}
		
		// 쇼핑몰 리스트 조회 selectbox 그리기용
		List<LinkPriceItemInfo> shoppinMallList= null;
		
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		paging.makePagingHtml(totalcount);
		
		model.put("pg", pg);
		model.put("boardPaging", paging);
		
		// session setting
		try{
			session.setAttribute("totalcount", msg.getOrder_list().size());
		}catch(NullPointerException e){
			session.setAttribute("totalcount", 0);
		}
		if (searchBean.getStatus() != null && !searchBean.getStatus().isEmpty()) {
			session.setAttribute("status", searchBean.getStatus());
		}
		if (searchBean.getKeywd() != null && !searchBean.getKeywd().isEmpty()) {
			session.setAttribute("keywd", searchBean.getKeywd());
		}
//				if (msg != null && !msg.getOrder_list().isEmpty()){
//					session.setAttribute("preSavingList", msg);
//				}
		if (userToken != null && !userToken.isEmpty()){
			session.setAttribute("userToken", userToken);
		}
		if (searchBean.getPoint_type() != null && !searchBean.getPoint_type().isEmpty()){
			session.setAttribute("point_type", searchBean.getPoint_type());
		}
		// session setting
		
		try {
			shoppinMallList = clip_3_0_service.getShoppinMallList();
			
			logger.info("shoppinMallList : " + shoppinMallList.toString());
		} catch (Exception e) {
			logger.error("쇼핑몰 적립 매장 조회 에러", e); 
		}
		
		mav.addObject("shoppinMallList", shoppinMallList);
		
		return mav;
	}

	/**
	 * 쇼핑적립 적립 내역 조회
	 * @author jyi
	 * @param LinkPriceItemInfo
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW03.do", method = RequestMethod.GET)
	public ModelAndView shoppingsaving_saving_list_search(HttpServletRequest request, LinkPriceItemInfo linkPriceItemInfo, ModelMap model, SearchBean searchBean
			, @RequestParam(value = "pg", required = false) String page
			, @RequestParam(value = "searchYear", required = false) String searchYear
			, @RequestParam(value = "searchDate", required = false) String searchDate
			, @RequestParam(value = "requester_code", required = false) String requester_code) throws Exception {
		ModelAndView mav = new ModelAndView();
		HttpSession session = request.getSession();
		
		boolean isfirst = false;
		String pg = page;
		String status = "";
		String keywd = "";
		String searchDay = "";
		String endDay = "";
		
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
			isfirst = true;
		}else{
			status = (String) session.getAttribute("status");
			keywd = (String) session.getAttribute("keywd");
			searchYear = (String) session.getAttribute("searchYear");
			searchDate = (String) session.getAttribute("searchDate");
			requester_code = (String) session.getAttribute("requester_code");
		}
		
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		model.put("startNum", startNum);
		model.put("endNum", endNum);
		
		if(searchYear != null && searchDate != null) {
			if(Integer.parseInt(searchDate) < 10) {
				searchDay = searchYear + "0" + searchDate + "01000000";
				endDay = searchYear + "0" + searchDate + "31235959";
			}else {
				searchDay = searchYear + searchDate + "01000000";
				endDay = searchYear + searchDate + "31235959";
			}
		}
		
		if (isfirst == false && Integer.parseInt(pg) >= 1) {
			
			if (status != null && !status.isEmpty()) {
				model.put("status", status);
				searchBean.setStatus(status);
			}else{
				model.put("status", "");
				searchBean.setStatus("");
				status = "";
			}
			if (keywd != null && !keywd.isEmpty()) {
				model.put("keywd", keywd);
				searchBean.setKeywd(keywd);
			}else {
				keywd = "";
			}
			if (searchDay != null && !searchDay.isEmpty()) {
				model.put("searchDay", searchDay);
			}
			if (endDay != null && !endDay.isEmpty()) {
				model.put("endDay", endDay);
			}
			if (requester_code != null && !requester_code.isEmpty()) {
				model.put("requester_code", requester_code);
			}
			
		}else {
			status = searchBean.getStatus();
			keywd = searchBean.getKeywd();
			
			if (status != null && !status.isEmpty()) {
				model.put("status", status);
			}else{
				model.put("status", "");
				searchBean.setStatus("");
				status = "";
			}
			if (keywd != null && !keywd.isEmpty()) {
				model.put("keywd", keywd);
			}else {
				keywd = "";
			}
			if (searchDay != null && !searchDay.isEmpty()) {
				model.put("searchDay", searchDay);
			}
			if (endDay != null && !endDay.isEmpty()) {
				model.put("endDay", endDay);
			}
			if (requester_code != null && !requester_code.isEmpty()) {
				model.put("requester_code", requester_code);
			}
			
		}
		
		String totalcount = "";
		int sum = 0;
		@SuppressWarnings("unused")
		List<HashMap<String, String>> total = null;
		String strTotalvalue = "";
		int totalvalue = 0;
		List<LinkpriceShopHistDtl> list = new ArrayList<LinkpriceShopHistDtl>();
		
		List<UserTokenInfo> userinfo = null;
		if(searchBean.getKeywd() != null && !searchBean.getKeywd().isEmpty()){
			userinfo = clip_3_0_service.selectUserToken(searchBean);
		}
		
		if(userinfo != null){
//			System.out.println("######USER_INFO_TBL DATA -> " + userinfo.toString());
			
			for(UserTokenInfo user : userinfo){
				if (user.getUserToken() != null && !user.getUserToken().isEmpty()) {
					model.put("user_token", user.getUserToken());
				}
				totalcount = clip_3_0_service.selectShopHistCount(model);
				sum += Integer.parseInt(totalcount);
				strTotalvalue = clip_3_0_service.selectShopHistTotalvalue(model);
				if(strTotalvalue != null && !strTotalvalue.isEmpty())
					totalvalue += Integer.parseInt(strTotalvalue);
				list.addAll(clip_3_0_service.selectShopHistList(model));
			}
		}
		else if(keywd.equals("") && status.equals("")) {
			totalcount = clip_3_0_service.selectShopHistCount(model);
			sum += Integer.parseInt(totalcount);
			strTotalvalue = clip_3_0_service.selectShopHistTotalvalue(model);
			if(strTotalvalue != null && !strTotalvalue.isEmpty())
				totalvalue += Integer.parseInt(strTotalvalue);
			list = clip_3_0_service.selectShopHistList(model);
		}
		
		for(LinkpriceShopHistDtl hist : list){
			logger.info("#########적립내역 리스트 주문번호 :: " + hist.getO_cd());
		}
		
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		paging.makePagingHtml(sum);
		
		model.put("pg", pg);
		model.put("boardPaging", paging);
		
		// session setting
		session.setAttribute("totalcount", sum);
		mav.addObject("totalcount", sum);
		if (status != null && !status.isEmpty()) {
			session.setAttribute("status", status);
			mav.addObject("status", status);
		}else{
			session.setAttribute("status", status);
			mav.addObject("status", status);
		}
		if (keywd != null && !keywd.isEmpty()) {
			session.setAttribute("keywd", keywd);
			mav.addObject("keywd", keywd);
		}else{
			session.setAttribute("keywd", keywd);
			mav.addObject("keywd", keywd);
		}
		if (searchYear != null && !searchYear.isEmpty()) {
			session.setAttribute("searchYear", searchYear);
			mav.addObject("searchYear", searchYear);
		}else{
			session.setAttribute("searchYear", searchYear);
			mav.addObject("searchYear", searchYear);
		}
		if (searchDate != null && !searchDate.isEmpty()) {
			session.setAttribute("searchDate", searchDate);
			mav.addObject("searchDate", searchDate);
		}else{
			session.setAttribute("searchDate", searchDate);
			mav.addObject("searchDate", searchDate);
		}
		if (requester_code != null && !requester_code.isEmpty()) {
			session.setAttribute("requester_code", requester_code);
			mav.addObject("requester_code", requester_code);
		}else{
			session.setAttribute("requester_code", requester_code);
			mav.addObject("requester_code", requester_code);
		}
		if (model.get("user_ci") != null) {
			session.setAttribute("user_ci", model.get("user_ci"));
		}else{
			session.setAttribute("user_ci", "");
		}
		// session setting
		
		mav.addObject("list", list);
		mav.addObject("totalvalue", totalvalue);
		mav.setViewName("/main/index");
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "쇼핑적립 적립 내역 조회");
		mav.addObject("display", "../clip/point_3_0/search/shoppingsaving_saving_list_search.jsp");
		
		// 쇼핑몰 리스트 조회 selectbox 그리기용
		List<LinkPriceItemInfo> shoppinMallList= null;
		
		try {
			shoppinMallList = clip_3_0_service.getShoppinMallList();
			
			logger.info("shoppinMallList : " + shoppinMallList.toString());
		} catch (Exception e) {
			logger.error("쇼핑몰 적립 매장 조회 에러", e); 
		}
		
		mav.addObject("shoppinMallList", shoppinMallList);
		
//		//검색,  또는 엑셀 다운로드 클릭시 
//		if ( linkPriceItemInfo !=null ) {
//			try{
//				if("Y".equals(linkPriceItemInfo.getExcelDownload())){
//					mav.addObject("pageNm", "통계관리");
//					//mav.addObject("divString", "statLogDay");
//					mav.setViewName("excelView");
//				}else{
//					mav.setViewName("/main/index");
//				}
//				
//				
//				
//				
//			} catch(Exception e) {
//				logger.error("검색, 또는 엑셀 다운로드 에러 ",e);
//				
//			}
//		}
	
		return mav;
	}


	/**
	 * 포인트 쿠폰 내역 조회
	 * @author jyi
	 * @param SearchBean
	 * @param request
	 * @param ModelMap
	 * @param page
	 * @param couponNumber
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW04.do", method = RequestMethod.GET)
	public ModelAndView point_coupon_list_search(SearchBean searchBean, HttpServletRequest request, ModelMap model
			, @RequestParam(value = "pg", required = false) String page
			, @RequestParam(value = "couponNumber", required = false) String couponNumber) throws Exception {
		ModelAndView mav = new ModelAndView();
		HttpSession session = request.getSession();
		
		boolean isfirst = false;
		String pg = page;
		String status = "";
		String keywd = "";
		String datepicker1 = "";
		String datepicker2 = "";
		
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
			isfirst = true;
		}else{
			status = (String) session.getAttribute("status");
			keywd = (String) session.getAttribute("keywd");
			datepicker1 = (String) session.getAttribute("datepicker1");
			datepicker2 = (String) session.getAttribute("datepicker2");
			couponNumber = (String) session.getAttribute("couponNumber");
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		model.put("startNum", startNum);
		model.put("endNum", endNum);
		
//		if(datepicker1 != null && !datepicker1.isEmpty()) {
//			datepicker1 = datepicker1 + "000000";
//		}
//		if(datepicker2 != null && !datepicker2.isEmpty()) {
//			datepicker2 = datepicker2 + "235959";
//		}
		
		if (isfirst == false && Integer.parseInt(pg) >= 1) {
			
			if (status != null && !status.isEmpty()) {
				model.put("status", status);
			}else{
				model.put("status", "");
				status = "";
			}
			if (keywd != null && !keywd.isEmpty()) {
				model.put("keywd", keywd);
			}else {
				keywd = "";
			}
			if (datepicker1 != null && !datepicker1.isEmpty()) {
				model.put("datepicker1", datepicker1);
			}
			if (datepicker2 != null && !datepicker2.isEmpty()) {
				model.put("datepicker2", datepicker2);
			}
			if (couponNumber != null && !couponNumber.isEmpty() && !couponNumber.equals("0")) {
				model.put("couponNumber", couponNumber);
			}
			
		}else {
			status = searchBean.getStatus();
			keywd = searchBean.getKeywd();
			datepicker1 = searchBean.getDatepicker1();
			datepicker2 = searchBean.getDatepicker2();
			
			if (status != null && !status.isEmpty()) {
				model.put("status", status);
			}else{
				model.put("status", "");
				status = "";
			}
			if (keywd != null && !keywd.isEmpty()) {
				model.put("keywd", keywd);
			}else {
				keywd = "";
			}
			if (datepicker1 != null && !datepicker1.isEmpty()) {
				model.put("datepicker1", datepicker1);
			}
			if (datepicker2 != null && !datepicker2.isEmpty()) {
				model.put("datepicker2", datepicker2);
			}
			if (couponNumber != null && !couponNumber.isEmpty() && !couponNumber.equals("0")) {
				model.put("couponNumber", couponNumber);
			}
			
		}
		
		String totalcount = "";
		int sum = 0;
		@SuppressWarnings("unused")
		List<HashMap<String, String>> total = null;
		String totalvalue = "";
		int sumvalue=0;
		List<PointCouponHist> list = new ArrayList<PointCouponHist>();
		
		List<UserTokenInfo> userinfo = null;
		if(keywd != null && !keywd.isEmpty()){
			userinfo = clip_3_0_service.selectUserCi(model);
		}
		
		if(userinfo != null){
			for(UserTokenInfo user : userinfo){
				if (user.getUserCi() != null && !user.getUserCi().isEmpty()) {
					model.put("user_ci", user.getUserCi());
				}
				totalcount = clip_3_0_service.selectCouponListCount(model);
				sum += Integer.parseInt(totalcount);
				totalvalue = clip_3_0_service.selectCoupontotalPoint(model);
				if(totalvalue != null && !totalvalue.isEmpty())
					sumvalue += Integer.parseInt(totalvalue);
				list.addAll(clip_3_0_service.selectPointCouponHistList(model));
			}
		}
		else if(keywd.equals("") && status.equals("")) {
			totalcount = clip_3_0_service.selectCouponListCount(model);
			sum += Integer.parseInt(totalcount);
			totalvalue = clip_3_0_service.selectCoupontotalPoint(model);
			if(totalvalue != null && !totalvalue.isEmpty())
				sumvalue += Integer.parseInt(totalvalue);
			list = clip_3_0_service.selectPointCouponHistList(model);
		}
		
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		paging.makePagingHtml(sum);
		
		model.put("pg", pg);
		model.put("boardPaging", paging);
		
		if(datepicker1 != null && !datepicker1.isEmpty()) {
			datepicker1 = datepicker1.substring(0, 8);
		}
		if(datepicker2 != null && !datepicker2.isEmpty()) {
			datepicker2 = datepicker2.substring(0, 8);
		}
		
		// session setting
		session.setAttribute("totalcount", sum);
		mav.addObject("totalcount", sum);
		if (status != null && !status.isEmpty()) {
			session.setAttribute("status", status);
			mav.addObject("status", status);
		}else{
			session.setAttribute("status", status);
			mav.addObject("status", status);
		}
		if (keywd != null && !keywd.isEmpty()) {
			session.setAttribute("keywd", keywd);
			mav.addObject("keywd", keywd);
		}else{
			session.setAttribute("keywd", keywd);
			mav.addObject("keywd", keywd);
		}
		if (datepicker1 != null && !datepicker1.isEmpty()) {
			session.setAttribute("datepicker1", datepicker1);
			mav.addObject("datepicker1", datepicker1);
		}else{
			session.setAttribute("datepicker1", datepicker1);
			mav.addObject("datepicker1", datepicker1);
		}
		if (datepicker2 != null && !datepicker2.isEmpty()) {
			session.setAttribute("datepicker2", datepicker2);
			mav.addObject("datepicker2", datepicker2);
		}else{
			session.setAttribute("datepicker2", datepicker2);
			mav.addObject("datepicker2", datepicker2);
		}
		if (couponNumber != null && !couponNumber.isEmpty() && !couponNumber.equals("0")) {
			session.setAttribute("couponNumber", couponNumber);
			mav.addObject("couponNumber", couponNumber);
		}else{
			session.setAttribute("couponNumber", couponNumber);
			mav.addObject("couponNumber", couponNumber);
		}
		if (model.get("user_ci") != null) {
			session.setAttribute("user_ci", model.get("user_ci"));
		}else{
			session.setAttribute("user_ci", "");
		}
		// session setting
		
		mav.addObject("list", list);
		mav.addObject("totalvalue", sumvalue);
		mav.setViewName("/main/index");
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "포인트 쿠폰 내역 조회");
		mav.addObject("display", "../clip/point_3_0/search/point_coupon_list_search.jsp");
		return mav;
	}

	/**
	 * 매장 바로 쓰기 내역 조회
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW05.do", method = RequestMethod.GET)
	public ModelAndView store_direct_buying_list_search() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/main/index");
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "매장 바로 쓰기 내역 조회");
		mav.addObject("display", "../clip/point_3_0/search/store_direct_buying_list_search.jsp");
		return mav;
	}

	/**
	 * 포인트 폭탄 내역 조회
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW06.do", method = RequestMethod.GET)
	public ModelAndView point_bomb_list_search(HttpServletRequest request, ModelMap model, SearchBean searchBean
			, @RequestParam(value = "pg", required = false) String page
			, @RequestParam(value = "keywd", required = false) String keywd
			, @RequestParam(value = "status", required = false) String status
//			, @RequestParam(value = "datepicker1", required = false) String datepicker1
//			, @RequestParam(value = "datepicker2", required = false) String datepicker2
			, @RequestParam(value = "day_count", required = false) String day_count) throws Exception {
		ModelAndView mav = new ModelAndView();
		HttpSession session = request.getSession();
		
		boolean isfirst = false;
		String pg = page;
		
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
			isfirst = true;
		}else{
			status = (String) session.getAttribute("status");
			keywd = (String) session.getAttribute("keywd");
//			datepicker1 = (String) session.getAttribute("datepicker1");
//			datepicker2 = (String) session.getAttribute("datepicker2");
			day_count = (String) session.getAttribute("day_count");
		}
		
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		model.put("startNum", startNum);
		model.put("endNum", endNum);
		
//		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy.MM.dd");
//		SimpleDateFormat dt2 = new SimpleDateFormat("yyyyMMdd");
		
		if (isfirst == false && Integer.parseInt(pg) >= 1) {
			
			if (status != null && !status.isEmpty()) {
				model.put("status", status);
			}else{
				model.put("status", "");
				searchBean.setStatus("");
				status = "";
			}
			if (keywd != null && !keywd.isEmpty()) {
				model.put("keywd", keywd);
			}else {
				keywd = "";
			}
//			if (datepicker1 != null && !datepicker1.isEmpty()) {
//				model.put("datepicker1", datepicker1);
//			}
//			if (datepicker2 != null && !datepicker2.isEmpty()) {
//				model.put("datepicker2", datepicker2);
//			}
			if (day_count != null && !day_count.isEmpty()) {
				model.put("day_count", day_count);
			}
			
		}else {
			
			if (status != null && !status.isEmpty()) {
				model.put("status", status);
			}else{
				model.put("status", "");
				status = "";
			}
			if (keywd != null && !keywd.isEmpty()) {
				model.put("keywd", keywd);
			}else {
				keywd = "";
			}
//			if (datepicker1 != null && !datepicker1.isEmpty()) {
//				Date date1 = dt1.parse(datepicker1);
//				datepicker1 = dt2.format(date1);
//				model.put("datepicker1", datepicker1);
//			}
//			if (datepicker2 != null && !datepicker2.isEmpty()) {
//				Date date2 = dt1.parse(datepicker2);
//				datepicker2 = dt2.format(date2);
//				model.put("datepicker2", datepicker2);
//			}
			if (day_count != null && !day_count.isEmpty()) {
				model.put("day_count", day_count);
			}
			
		}
		
		List<UserTokenInfo> userinfo = null;
		if(keywd != null && !keywd.isEmpty()){
			userinfo = clip_3_0_service.selectUserCi(model);
		}
		
		String totalcount = "";
		int sum = 0;
//		List<HashMap<String, String>> total = null;
//		String totalvalue = "";
		int sumvalue=0;
		List<PointbombInfo> list = new ArrayList<PointbombInfo>();
		
		if(userinfo != null){
			for(UserTokenInfo user : userinfo){
				if (user.getCustId() != null && !user.getCustId().isEmpty() && !user.getCustId().equals("'")) {
					model.put("cust_id", user.getCustId());
				
					totalcount = clip_3_0_service.selectPointbombTotalcount(model);
					sum += Integer.parseInt(totalcount);
					list.addAll(clip_3_0_service.selectPointbombList(model));
				}
				
			}
		}
		else if(keywd.equals("") && status.equals("")) {
			totalcount = clip_3_0_service.selectPointbombTotalcount(model);
			sum += Integer.parseInt(totalcount);
			list = clip_3_0_service.selectPointbombList(model);
		}
		
		if(list != null && !list.isEmpty()){
			for(PointbombInfo info : list){
				info.setCtn(AES256CipherClip.AES_Decode(info.getCtn()));
				info.setUser_ci(new String(Base64.encodeBase64(info.getUser_ci().getBytes())));
				sumvalue += Integer.parseInt(info.getTotal_reward_point());
			}
		}
		mav.addObject("totalvalue", sumvalue);
		mav.addObject("totalcount", sum);
		mav.addObject("list", list);
		
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		paging.makePagingHtml(sum);
		
		model.put("pg", pg);
		model.put("boardPaging", paging);
		
		// session setting
		session.setAttribute("totalcount", sum);
		mav.addObject("totalcount", sum);
		if (status != null && !status.isEmpty()) {
			session.setAttribute("status", status);
			mav.addObject("status", status);
		}else{
			session.setAttribute("status", status);
			mav.addObject("status", status);
		}
		if (keywd != null && !keywd.isEmpty()) {
			session.setAttribute("keywd", keywd);
			mav.addObject("keywd", keywd);
		}else{
			session.setAttribute("keywd", keywd);
			mav.addObject("keywd", keywd);
		}
//		if (datepicker1 != null && !datepicker1.isEmpty()) {
//			session.setAttribute("datepicker1", datepicker1);
//			mav.addObject("datepicker1", datepicker1);
//		}else{
//			session.setAttribute("datepicker1", datepicker1);
//			mav.addObject("datepicker1", datepicker1);
//		}
//		if (datepicker2 != null && !datepicker2.isEmpty()) {
//			session.setAttribute("datepicker2", datepicker2);
//			mav.addObject("datepicker2", datepicker2);
//		}else{
//			session.setAttribute("datepicker2", datepicker2);
//			mav.addObject("datepicker2", datepicker2);
//		}
		if (day_count != null && !day_count.isEmpty()) {
			session.setAttribute("day_count", day_count);
			mav.addObject("day_count", day_count);
		}else{
			session.setAttribute("day_count", day_count);
			mav.addObject("day_count", day_count);
		}
		if (userinfo != null && !userinfo.isEmpty()){
			session.setAttribute("userinfo", userinfo);
			mav.addObject("userinfo", userinfo);
		}else{
			session.setAttribute("userinfo", userinfo);
			mav.addObject("userinfo", userinfo);
		}
		// session setting
		
		mav.setViewName("/main/index");
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "포인트 폭탄 내역 조회");
		mav.addObject("display", "../clip/point_3_0/search/point_bomb_list_search.jsp");
		return mav;
	}
	
	/**
	 * 포인트 폭탄 내역 상세 조회(user_ci)
	 * @author jyi
	 * @param request
	 * @param model
	 * @param mode
	 * @param user_ci
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/pointbomb_detail.do", method = RequestMethod.GET)
	public ModelAndView pointbomb_search_detail(HttpServletRequest request, ModelMap model
			, @RequestParam(value = "user_ci", required = false) String user_ci) throws Exception {
		ModelAndView mav = new ModelAndView();
		List<PointbombInfo> list = new ArrayList<PointbombInfo>();
		
		user_ci = new String(Base64.decodeBase64(user_ci.getBytes()));
		model.put("keywd", user_ci);
		model.put("status", "user_ci");
		
		List<UserTokenInfo> userinfo = null;
		userinfo = clip_3_0_service.selectUserCi(model);
		
		for(UserTokenInfo info : userinfo) {
			model.put("cust_id", info.getCustId());
			
			list.addAll(clip_3_0_service.selectPointbombList(model));
			
			if(list != null && !list.isEmpty()){
				for(PointbombInfo bombinfo : list){
					info.setCtn(AES256CipherClip.AES_Decode(bombinfo.getCtn()));
				}
			}
		}
		mav.addObject("list", list);
		
		mav.setViewName("/main/index");
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "포인트 폭탄 내역 조회");
		mav.addObject("display", "../clip/point_3_0/search/point_bomb_detail.jsp");
		
		return mav;
	}

	/**
	 * 잠금화면 일차 관리
	 * point_bomb_day_info
	 * @return ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW07.do", method = RequestMethod.GET)
	public ModelAndView lock_screen_daily_management() throws Exception {
		List<ScreenLockDAO> screenlockvo =  clip_3_0_service.getScreenLockList();
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/main/index");
		mav.addObject("screenlockvo", screenlockvo);
		mav.addObject("menu1", "형상관리");
		mav.addObject("menu2", "잠금화면 일차 관리");
		mav.addObject("display", "../clip/point_3_0/management/lock_screen_daily_management.jsp");
		return mav;
	}
	
	/**
	 * 잠금화면 일차 관리
	 * point_bomb_day_info
	 * @return ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW12.do", method = RequestMethod.GET)
	public ModelAndView lock_screen_daily_management_modify(@RequestParam(value = "index") String index) throws Exception {
		ScreenLockDAO screenlockvo =  clip_3_0_service.getScreenLockListDetail(index);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/main/index");
		mav.addObject("screenlockvo", screenlockvo);
		mav.addObject("menu1", "형상관리");
		mav.addObject("menu2", "잠금화면 일차 관리");
		mav.addObject("display", "../clip/point_3_0/management/lock_screen_daily_management_modify.jsp");
		return mav;
	}

	/**
	 * 잠금화면 랜덤포인트 관리
	 * @return ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW08.do", method = RequestMethod.GET)
	public ModelAndView lock_screen_randompoint_management() throws Exception {
		
		ScreenLockRandomPointMDAO screenlockrandompointm =  clip_3_0_service.getScreenLockRandomPointManagment();
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/main/index");
		mav.addObject("screenlockrandompointm", screenlockrandompointm);
		mav.addObject("menu1", "형상관리");
		mav.addObject("menu2", "잠금화면 랜덤포인트 관리");
		mav.addObject("display", "../clip/point_3_0/management/lock_screen_randompoint_management.jsp");
		return mav;
	}

	/**
	 * 쇼핑적립 관리
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW09.do", method = RequestMethod.GET)
	public ModelAndView shopping_saving_management(HttpServletRequest request, ModelMap model) throws Exception {
		ModelAndView mav = new ModelAndView();
		
		List<LinkPriceItemInfo> list = clip_3_0_service.selectLinkPriceItemInfoList();
		for(LinkPriceItemInfo info : list){
			if(info.getReward_kt() == null)
				info.setReward_kt(info.getReward());
		}
		mav.addObject("list", list);
		
		mav.setViewName("/main/index");
		mav.addObject("menu1", "형상관리");
		mav.addObject("menu2", "쇼핑적립 관리");
		mav.addObject("display", "../clip/point_3_0/management/shopping_saving_management.jsp");
		return mav;
	}
	
	/**
	 * 쇼핑적립 관리 상세, 신규, 수정, 순위
	 * @author jyi
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/clip3_0/shopping_saving_new.do", method = RequestMethod.GET)
	public ModelAndView shopping_saving_management_new(HttpServletRequest request, ModelMap model
			, @RequestParam(value = "mode", required = false) String mode
			, @RequestParam(value = "mall_id", required = false) String mall_id) throws Exception {
		ModelAndView mav = new ModelAndView();
		
		if(!mode.equals("3")){
			model.put("mall_id", mall_id);
			LinkPriceItemInfo info = clip_3_0_service.selectLinkPriceItemInfoOne(model);
			mav.addObject("data", info);
		}else{
			List<LinkPriceItemInfo> list = clip_3_0_service.selectLinkPriceItemInfoList();
			mav.addObject("list", list);
		}
		
		mav.setViewName("/main/index");
		mav.addObject("menu1", "형상관리");
		mav.addObject("menu2", "쇼핑적립 관리");
		/** mode :: 0(신규등록), 1(상세보기), 2(수정), 3(노출 순위 설정) */
		if(mode.equals("1")){
			mav.addObject("display", "../clip/point_3_0/management/shopping_saving_management_detail.jsp");
		}else if(mode.equals("2")){
			mav.addObject("display", "../clip/point_3_0/management/shopping_saving_management_modify.jsp");
		}else if(mode.equals("3")){
			mav.addObject("display", "../clip/point_3_0/management/shopping_saving_management_rank.jsp");
		}else{
			mav.addObject("display", "../clip/point_3_0/management/shopping_saving_management_new.jsp");
		}
		
		return mav;
	}

	/**
	 * 메인 배너 관리
	 * @author jyi
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW10.do", method = RequestMethod.GET)
	public ModelAndView main_banner_management(HttpServletRequest request, ModelMap model
			, @RequestParam(value = "pg", required = false) String page
			, @RequestParam(value = "mb_name", required = false) String mb_name
			, @RequestParam(value = "datepicker1", required = false) String datepicker1
			, @RequestParam(value = "datepicker2", required = false) String datepicker2
			, @RequestParam(value = "service_status", required = false) String service_status) throws Exception {
		ModelAndView mav = new ModelAndView();
		HttpSession session = request.getSession();
		
		boolean isfirst = false;
		String pg = page;
		
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
			isfirst = true;
		}else{
			mb_name = (String) session.getAttribute("mb_name");
			datepicker1 = (String) session.getAttribute("datepicker1");
			datepicker2 = (String) session.getAttribute("datepicker2");
			service_status = (String) session.getAttribute("service_status");
		}
		
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		model.put("startNum", startNum);
		model.put("endNum", endNum);
		
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy.MM.dd");
		SimpleDateFormat dt2 = new SimpleDateFormat("yyyyMMdd");
		
		if (isfirst == false && Integer.parseInt(pg) >= 1) {
			
			if (mb_name != null && !mb_name.isEmpty()) {
				model.put("mb_name", mb_name);
			}
			if (datepicker1 != null && !datepicker1.isEmpty()) {
				Date date = dt1.parse(datepicker1);
				datepicker1 = dt2.format(date);
				model.put("datepicker1", datepicker1);
			}
			if (datepicker2 != null && !datepicker2.isEmpty()) {
				Date date = dt1.parse(datepicker2);
				datepicker2 = dt2.format(date);
				model.put("datepicker2", datepicker2);
			}
			if (service_status != null && !service_status.isEmpty()) {
				model.put("service_status", service_status);
			}
			
		}else {
			
			if (mb_name != null && !mb_name.isEmpty()) {
				model.put("mb_name", mb_name);
			}
			if (datepicker1 != null && !datepicker1.isEmpty()) {
				Date date = dt1.parse(datepicker1);
				datepicker1 = dt2.format(date);
				model.put("datepicker1", datepicker1);
			}
			if (datepicker2 != null && !datepicker2.isEmpty()) {
				Date date = dt1.parse(datepicker2);
				datepicker2 = dt2.format(date);
				model.put("datepicker2", datepicker2);
			}
			if (service_status != null && !service_status.isEmpty()) {
				model.put("service_status", service_status);
			}
			
		}
		
		String totalcount = clip_3_0_service.selectMainbannerInfoTotalcount(model);
		List<MainBannerDO> list = clip_3_0_service.selectMainbannerInfoList(model);
		mav.addObject("totalcount", totalcount);
		mav.addObject("list", list);
		
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		paging.makePagingHtml(Integer.parseInt(totalcount));
		
		model.put("pg", pg);
		model.put("boardPaging", paging);
		
		// session setting
		session.setAttribute("totalcount", totalcount);
		if (mb_name != null && !mb_name.isEmpty()) {
			session.setAttribute("mb_name", mb_name);
			mav.addObject("mb_name", mb_name);
		}else{
			session.setAttribute("mb_name", mb_name);
			mav.addObject("mb_name", mb_name);
		}
		if (datepicker1 != null && !datepicker1.isEmpty()) {
			session.setAttribute("datepicker1", datepicker1);
			mav.addObject("datepicker1", datepicker1);
		}else{
			session.setAttribute("datepicker1", datepicker1);
			mav.addObject("datepicker1", datepicker1);
		}
		if (datepicker2 != null && !datepicker2.isEmpty()) {
			session.setAttribute("datepicker2", datepicker2);
			mav.addObject("datepicker2", datepicker2);
		}else{
			session.setAttribute("datepicker2", datepicker2);
			mav.addObject("datepicker2", datepicker2);
		}
		if (service_status != null && !service_status.isEmpty()) {
			session.setAttribute("service_status", service_status);
			mav.addObject("service_status", service_status);
		}else{
			session.setAttribute("service_status", service_status);
			mav.addObject("service_status", service_status);
		}
		// session setting
		
		mav.setViewName("/main/index");
		mav.addObject("menu1", "형상관리");
		mav.addObject("menu2", "메인 배너 관리");
		mav.addObject("display", "../clip/point_3_0/management/main_banner_management.jsp");
		return mav;
	}
	
	/**
	 * 메인 배너 관리 상세, 신규, 수정, 순위
	 * @author jyi
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/clip3_0/mainbanner_new.do", method = RequestMethod.GET)
	public ModelAndView main_banner_management_new(HttpServletRequest request, ModelMap model
			, @RequestParam(value = "mode", required = false) String mode
			, @RequestParam(value = "mb_id", required = false) String mb_id) throws Exception {
		ModelAndView mav = new ModelAndView();
		
		if(!mode.equals("3")){
			model.put("mb_id", mb_id);
			MainBannerDO info = clip_3_0_service.selectMainbannerInfoOne(model);
			mav.addObject("data", info);
		}else{
			List<MainBannerDO> list = clip_3_0_service.selectMainbannerInfoList(model);
			mav.addObject("list", list);
		}
		
		mav.setViewName("/main/index");
		mav.addObject("menu1", "형상관리");
		mav.addObject("menu2", "메인 배너 관리");
		/** mode :: 0(신규등록), 1(상세보기), 2(수정), 3(노출 순위 설정) */
		if(mode.equals("1")){
			mav.addObject("display", "../clip/point_3_0/management/main_banner_management_detail.jsp");
		}else if(mode.equals("2")){
			mav.addObject("display", "../clip/point_3_0/management/main_banner_management_modify.jsp");
		}else if(mode.equals("3")){
			mav.addObject("display", "../clip/point_3_0/management/main_banner_management_rank.jsp");
		}else{
			mav.addObject("display", "../clip/point_3_0/management/main_banner_management_new.jsp");
		}
		
		return mav;
	}
	
	/**
	 * 룰렛 배너 구좌 관리
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW11.do", method = RequestMethod.GET)
	public ModelAndView roulette_banner_account_management() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/main/index");
		mav.addObject("menu1", "형상관리");
		mav.addObject("menu2", "룰렛 배너 구좌 관리");
		mav.addObject("display", "../clip/point_3_0/management/roulette_banner_account_management.jsp");
		return mav;
	}
	
	/***************************************************************************************************
	 * 
	 *  Ajax Controller
	 * @throws Exception 
	 * 
	 */
	
	
	
	/**
	 * 적립 예정 표인트 조회
	 * @author jyi
	 * @param searchBean
	 * @param request
	 * @param model
	 * @return ModelAndView
	 * @throws java.lang.Exception
	 */
	@SuppressWarnings("null")
	@RequestMapping(value = "/clip3_0/preSavingCheckList.do")
	public ModelAndView preSavingCheckList(SearchBean searchBean
			, HttpServletRequest request, ModelMap model
			, @RequestParam(value = "pg", required = false) String page)  throws java.lang.Exception {
		
		System.out.println(searchBean.toString());
		
		ModelAndView mav = new ModelAndView();
		HttpSession session = request.getSession();
		
		List<UserTokenInfo> userinfo = clip_3_0_service.selectUserToken(searchBean);
		
		if(userinfo == null){
			for(UserTokenInfo user : userinfo){
				user.setUserToken("");
			}
		}
//		else{
//			System.out.println("######USER_INFO_TBL DATA -> " + userinfo.toString());
//		}
		
		LinkPriceReportMessege msg = new LinkPriceReportMessege();
		String userToken = "";
		for(UserTokenInfo info : userinfo){
			msg.setuId(info.getUserToken());
			userToken = info.getUserToken();
		
			if(searchBean.getPoint_type().equals("I")){
				msg = clip_3_0_service.getNextSavingHistory(msg);
			} else{
				msg = clip_3_0_service.getNext2SavingHistory(msg);
			}
			
			for(LinkPriceReportItem item : msg.getOrder_list()){
				String itemCount = clip_3_0_service.selectItemCount(item);
				item.setItemCount(itemCount);
			}
		}
		if(searchBean.getStatus().equals("ctn"))
			searchBean.setKeywd(AES256CipherClip.AES_Decode(searchBean.getKeywd()));
		
		// session setting
		try{
			session.setAttribute("totalcount", msg.getOrder_list().size());
		}catch(NullPointerException e){
			session.setAttribute("totalcount", 0);
		}
		if (searchBean.getStatus() != null && !searchBean.getStatus().isEmpty()) {
			session.setAttribute("status", searchBean.getStatus());
		}
		if (searchBean.getKeywd() != null && !searchBean.getKeywd().isEmpty()) {
			session.setAttribute("keywd", searchBean.getKeywd());
		}
//		if (msg != null && !msg.getOrder_list().isEmpty()){
//			session.setAttribute("preSavingList", msg);
//		}
		if (userToken != null && !userToken.isEmpty()){
			session.setAttribute("userToken", userToken);
		}
		if (searchBean.getPoint_type() != null && !searchBean.getPoint_type().isEmpty()){
			session.setAttribute("point_type", searchBean.getPoint_type());
		}
		// session setting
		
		// 페이징
//		String pg = page;
//		if (pg == null || pg == ""){
//			pg = "1";
//		}
//		
//		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
//		paging.setCurrentPage(Integer.parseInt(pg));
//		paging.makePagingHtml(msg.getOrder_list().size());
//		
//		mav.addObject("pg", pg);
//		mav.addObject("boardPaging", paging);
		// 페이징 
		
		mav.addObject("searchBean", searchBean);
		mav.addObject("preSavingList", msg);
		mav.addObject("display", "../clip/point_3_0/search/shoppingsaving_pre_saving_list_search.jsp");
		mav.setViewName("/main/index");
		return mav;
		
		
	}
	
	/**
	 * 잠금화면 일차 관리
	 * point_bomb_day_info
	 * @return ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping(value = "/clip3_0/KTCPMI01.do", method = RequestMethod.POST)
	@ResponseBody
	public String lock_screen_daily_management_update(ScreenLockDAO arg) throws Exception {
		
		clip_3_0_service.setScreenLockData(arg);
		Map<String,String> map = new HashMap<String,String>();
		map.put("result", "success");
		return JSONValue.toJSONString(map);
		
	}
	
	@RequestMapping(value = "/clip3_0/KTCPMI02.do", method = RequestMethod.POST)
	@ResponseBody
	public String lock_screen_randompoint_management_update(ScreenLockRandomPointMDAO arg) throws Exception {
		
		clip_3_0_service.setLock_Screen_Randompoint(arg);
		Map<String,String> map = new HashMap<String,String>();
		map.put("result", "success");
		return JSONValue.toJSONString(map);
		
	}
	
	/**
	 * 메인배너 신규 등록
	 * @param MainBannerDO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMI03/insert.do", method = RequestMethod.POST)
	@ResponseBody
	public String main_banner_management_insert(MainBannerDO arg) throws Exception {
		
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy.MM.dd");
		SimpleDateFormat dt2 = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat dt3 = new SimpleDateFormat("yyyyMMddkkmmss");
		Date date1 = dt1.parse(arg.getDatepicker1());
		Date date2 = dt1.parse(arg.getDatepicker2());
		Date date3 = new Date(System.currentTimeMillis());
		String start_date = dt2.format(date1);
		String end_date = dt2.format(date2);
		String reg_date = dt3.format(date3);
		
		/** mb_id 생성 :: 생성날짜 + 4자리 난수 */
		Random random = new Random();
		Integer result = random.nextInt(10000)+1000;
		if(result>10000){
		    result = result - 1000;
		}
		String date = new SimpleDateFormat("yyMM").format(date3);
		String mb_id = date + result.toString();
		logger.debug("#######mb_id 생성 :: " + mb_id);
		/** mb_id 생성 :: 생성날짜 + 4자리 난수 */
		
		// 노출수위 조회
		MainBannerDO maxRank = clip_3_0_service.selectMainbannerInfoMaxRank();
		Integer rank = null;
		if(maxRank != null)
			rank = Integer.parseInt(maxRank.getRank()) + 1;
		else
			rank = 1;
		
		arg.setRank(rank.toString());
		arg.setMb_id(mb_id);
		arg.setStart_date(start_date);
		arg.setEnd_date(end_date);
		arg.setReg_date(reg_date);

		clip_3_0_service.insertMainBannerInfoTbl(arg);
		Map<String,String> map = new HashMap<String,String>();
		map.put("result", "success");
		return JSONValue.toJSONString(map);
		
	}
	
	/**
	 * 메인배너 수정
	 * @param MainBannerDO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMI03/update.do", method = RequestMethod.POST)
	@ResponseBody
	public String main_banner_management_update(MainBannerDO arg) throws Exception {
		
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy.MM.dd");
		SimpleDateFormat dt2 = new SimpleDateFormat("yyyyMMdd");
		@SuppressWarnings("unused")
		SimpleDateFormat dt3 = new SimpleDateFormat("yyyyMMddkkmmss");
		Date date1 = dt1.parse(arg.getDatepicker1());
		Date date2 = dt1.parse(arg.getDatepicker2());
		@SuppressWarnings("unused")
		Date date3 = new Date(System.currentTimeMillis());
		String start_date = dt2.format(date1);
		String end_date = dt2.format(date2);
		
		arg.setStart_date(start_date);
		arg.setEnd_date(end_date);
		arg.setMb_id(arg.getMb_id().trim());

		clip_3_0_service.updateMainBannerInfoTbl(arg);
		
		Map<String,String> map = new HashMap<String,String>();
		map.put("result", "success");
		return JSONValue.toJSONString(map);
		
	}
	
	/**
	 * 메인 배너 항목 삭제
	 * @param arg
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMI03/delete.do", method = RequestMethod.POST)
	@ResponseBody
	public String main_banner_management_delete(MainBannerDO arg) throws Exception {
		
		arg.setMb_id(arg.getMb_id().trim());

		clip_3_0_service.deleteMainBannerInfoTbl(arg);
		
		Map<String,String> map = new HashMap<String,String>();
		map.put("result", "success");
		return JSONValue.toJSONString(map);
		
	}
	
	/**
	 * 메인 배너 노출 우선순위 수정
	 * @param arg
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMI03/rankModify.do", method = RequestMethod.POST)
	@ResponseBody
	public String main_banner_management_rankModify(@RequestParam(value = "new_rank[]") List<String> rank
			, @RequestParam(value="id[]") List<String> mb_id
			, MainBannerDO arg) throws Exception {
		
		for(int i =0; i< mb_id.size(); i++){
			arg.setMb_id(mb_id.get(i));
			arg.setRank(rank.get(i));
			clip_3_0_service.updateMainBannerInfoTbl(arg);
		}
		
		Map<String,String> map = new HashMap<String,String>();
		map.put("result", "success");
		return JSONValue.toJSONString(map);
		
	}
	
	/**
	 * 쇼핑적립 관리 신규 등록
	 * @param MainBannerDO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMI04/insert.do", method = RequestMethod.POST)
	@ResponseBody
	public String shopping_saving_management_insert(LinkPriceItemInfo arg) throws Exception {
		Map<String,String> map = new HashMap<String,String>();
		SimpleDateFormat dt3 = new SimpleDateFormat("yyyyMMddkkmmss");
		Date date3 = new Date(System.currentTimeMillis());
		String reg_date = dt3.format(date3);
		
		// 노출순위 조회
		LinkPriceItemInfo maxRank = clip_3_0_service.selectLinkPriceItemInfoMaxRank();
		Integer rank = null;
		if(maxRank != null)
			rank = Integer.parseInt(maxRank.getOrd()) + 1;
		else
			rank = 1;
		
		arg.setOrd(rank.toString());
		arg.setRegdate(reg_date);
		
		String asisCheck = clip_3_0_service.selectLinkPriceItemInfoIdcheck(arg);
		if(asisCheck != null){
			if(asisCheck.equals('Y')){
				map.put("result", "IDFAIL");
				return JSONValue.toJSONString(map);
			}
		}
		
		clip_3_0_service.insertLinkPriceItemInfo(arg);
		
		map.put("result", "success");
		return JSONValue.toJSONString(map);
		
	}
	
	/**
	 * 쇼핑적립 관리 수정
	 * @param MainBannerDO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMI04/update.do", method = RequestMethod.POST)
	@ResponseBody
	public String shopping_saving_management_update(LinkPriceItemInfo arg) throws Exception {
		
		clip_3_0_service.updateLinkPriceItemInfo(arg);
		Map<String,String> map = new HashMap<String,String>();
		map.put("result", "success");
		return JSONValue.toJSONString(map);
		
	}
	
	/**
	 * 쇼핑적립 노출 우선순위 수정
	 * @param arg
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMI04/rankModify.do", method = RequestMethod.POST)
	@ResponseBody
	public String shopping_saving_management_rankModify(@RequestParam(value = "new_rank[]") List<String> rank
			, @RequestParam(value="id[]") List<String> mall_id
			, LinkPriceItemInfo arg) throws Exception {
		
		for(int i =0; i< mall_id.size(); i++){
			arg.setMall_id(mall_id.get(i));
			arg.setOrd(rank.get(i));
			clip_3_0_service.updateLinkPriceItemInfo(arg);
		}
		
		Map<String,String> map = new HashMap<String,String>();
		map.put("result", "success");
		return JSONValue.toJSONString(map);
		
	}
	
	/**
	 * 쇼핑적립 관리 항목 삭제
	 * @param arg
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMI04/delete.do", method = RequestMethod.POST)
	@ResponseBody
	public String shopping_saving_management_delete(LinkPriceItemInfo arg) throws Exception {
		
		arg.setMall_id(arg.getMall_id().trim());

		clip_3_0_service.deleteLinkPriceItemInfo(arg);
		
		Map<String,String> map = new HashMap<String,String>();
		map.put("result", "success");
		return JSONValue.toJSONString(map);
		
	}
	
	/**
	 * 포인트 전환 취소
	 */
	@RequestMapping(value = "/clip3_0/cancelPointSwap.do", method = RequestMethod.POST)
	@ResponseBody
	public String cancelPointSwap(@RequestParam(required = true) String regSource,
			@RequestParam(required = true) String userCi, @RequestParam(required = true) String transactionId,
			@RequestParam(required = true) int point, @RequestParam(required = true) String description,
			@RequestParam(required = false) String custIdOrg, @RequestParam(required = false) String approveNo,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, String> map = new HashMap<>();
		HttpClientUtil conn = null;
		int ret = 0;
		String result = "";
		// 2018.03.07
		description = "포인트 전환 취소";
		
		logger.debug("cancelPointSwap start!!! parameter regSource[" + regSource + "],userCi[" + userCi + "],transactionId[" + transactionId + "],point[" + point + "],description[" + description+ "],custIdOrg[" + custIdOrg+ "],approveNo[" + approveNo + "]");
		
		if (custIdOrg == null) {
			custIdOrg = "";
		}
		
		try {
			conn = new HttpClientUtil("POST", AD_DOMAIN + "/pointSwap/cancelMinusPoint.do");
			conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
			conn.addParameter("userCi", URLEncoder.encode(userCi,"utf-8"));
			conn.addParameter("transactionId", URLEncoder.encode(transactionId,"utf-8"));
			conn.addParameter("regSource", regSource);
			conn.addParameter("point", ""+point);
			conn.addParameter("custIdOrg", custIdOrg);
			conn.addParameter("approveNo", approveNo);
			
			ret = conn.sendRequest();
			logger.debug("RES_HTTP_STATUS : " + ret);

			if(HttpStatus.SC_OK == ret) {
				CancelPointSwapInfo info = conn.getResponseJson(CancelPointSwapInfo.class);
				String msg = getResultMessage(regSource, info.getResultMsg());
				
				logger.debug("info.getResultMsg() : " + info.getResultMsg() + ", msg : " + msg);
				
				map.put("result", info.getResult());
				map.put("resultMsg", msg);
				
			} else {
				map.put("result", "F");
				map.put("resultMsg", "HTTP Error(" + ret + ")");
			}
			
			result = map.get("result");
		} catch (Exception e) {
			conn.closeConnection();
			e.printStackTrace();
			logger.error(e.getMessage());
			map.put("result", "F");
			map.put("resultMsg", "System Error(" + e.getMessage() + ")");
			
			return JSONValue.toJSONString(map);
		} finally {
			conn.closeConnection();
		}

		if ("S".equals(result)) {
			// 포인트 차감
			try {
				String transId = userCi.substring(0, 10) + System.currentTimeMillis();
				String requester_code = "";
				
				if ("shinhan".equals(regSource)) {
					requester_code = "pointswap_shinhan";
				} else if ("hanamembers".equals(regSource)) {
					requester_code = "pointswap_hana";
				} else if ("bccard".equals(regSource)) {
					requester_code = "pointswap_bc";
				} else if ("kbpointree".equals(regSource)) {
					requester_code = "pointswap_kb";
				}
				
				
				logger.debug("cancelPointSwap minus point create trans_id : " + transId);
				conn = new HttpClientUtil("POST", API_DOMAIN + "/adv/minusPoint.do");
				conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
				
	//			if(BaseConstants.USER_KEY_USER_CI.equals(param.getUser_key_type())){
					conn.addParameter("user_ci", URLEncoder.encode(userCi,"utf-8"));
	//			} else {
	//				conn.addParameter("user_token", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
	//			}
	
				conn.addParameter("transaction_id", URLEncoder.encode(transId,"utf-8"));
				conn.addParameter("requester_code", requester_code);
				conn.addParameter("point_value", ""+point);
				conn.addParameter("description", URLEncoder.encode(description,"utf-8"));
				
				ret = conn.sendRequest();
				logger.debug("RES_HTTP_STATUS : " + ret);
	
				if(HttpStatus.SC_OK == ret) {
					map.put("result", "S");
					map.put("resultMsg", "OK");
					
				} else {
					String resMsg = conn.getResponseString();
					
					if(resMsg != null && resMsg.contains("code: 600")) {
						map.put("result", "F");
						map.put("resultMsg", "Duplicate");
					} else if (ret == 500) {
						map.put("result", "F");
						map.put("resultMsg", "System Error");
					} else {
						map.put("result", "F");
						map.put("resultMsg", "No Result");
					}
				}
				
			} catch (Exception e) {
				conn.closeConnection();
				logger.error(e.getMessage());
				map.put("result", "F");
				map.put("resultMsg", "System Error");
			} finally {
				conn.closeConnection();
			}
		}
		
		logger.debug("cancelPointSwap end!!! result[" + map.get("result") + "],resultMsg[" + map.get("resultMsg") + "]");
		
		return JSONValue.toJSONString(map);
		
	}
	
	/***************************************************************************************************
	 * 
	 *  Other Method
	 * 
	 */
	
	private boolean checkParameter(ModelMap map) {
		
		Pattern pOnlyNum = Pattern.compile("(^[0-9]*$)");
		Pattern pOnlyCtn = Pattern.compile("^01(?:0|1|[6-9])(?:\\d{3}|\\d{4})\\d{4}$");  
		Pattern pOnlyChar = Pattern.compile("^[a-zA-Z]*$");
			
		String page = (String) map.get("pg");
		String status = (String) map.get("status");
		String keywd = (String) map.get("keywd");
		String use_status = (String) map.get("use_status");
		String value = (String) map.get("value");
		String point_type = (String) map.get("point_type");
		
		if(page != null && !page.isEmpty()) {
			Matcher m = pOnlyNum.matcher(page);
			if(!m.find())
				return true;
		}
		if(status != null && !status.isEmpty()) {
			Matcher m = pOnlyChar.matcher(status);
			if(!m.find())
				return true;
		}
		if(keywd != null && !keywd.isEmpty() && status != null && !status.isEmpty()) {
			if(status.equals("ctn")) {
				Matcher m = pOnlyCtn.matcher(keywd);
				if(!m.find())
					return true;
			}
		}
		if(use_status != null && !use_status.isEmpty()) {
			Matcher m = pOnlyChar.matcher(use_status);
			if(!m.find())
				return true;
		}
		if(value != null && !value.isEmpty()) {
			Matcher m = pOnlyNum.matcher(value);
			if(!m.find())
				return true;
		}
		if(point_type != null && !point_type.isEmpty()) {
			Matcher m = pOnlyChar.matcher(point_type);
			if(!m.find())
				return true;
		}
		
		return false;
	}
	
	/**
	 *  은행 망취소 통신 후 가져오는 결과 코드에 따라 결과 메세지 리턴
	 * @author jyi
	 * @param card_kind
	 * @param resultCode
	 * @return
	 */
	private String getResultMessage(String card_kind, String resultCode) {
		String retCode = resultCode;
		int i = -1;
		
		try {
			i = Integer.parseInt(resultCode);
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error("getResultMessage integer parse error");
		}
		
		if(card_kind.equals("shinhan")){
			if(i == 0){
				retCode = shinhanResCodeMessage[i][1];
			}else if(i >= 31 && i <= 33){
				retCode = shinhanResCodeMessage[i-30][1];
			}else if(i >= 36 && i <= 36){
				retCode = shinhanResCodeMessage[i-32][1];
			}else if(i >= 94 && i <= 96){
				retCode = shinhanResCodeMessage[i-89][1];
			}else if(i >= 332 && i <= 332){
				retCode = shinhanResCodeMessage[i-324][1];
			}else if(i >= 402 && i <= 402){
				retCode = shinhanResCodeMessage[i-393][1];
			}else if(i >= 1001 && i <= 1003){
				retCode = shinhanResCodeMessage[i-991][1];
			}else if(i >= 1011 && i <= 1021){
				retCode = shinhanResCodeMessage[i-998][1];
			}else if(i >= 1029 && i <= 1029){
				retCode = shinhanResCodeMessage[i-1005][1];
			}else if(i >= 1201 && i <= 1204){
				retCode = shinhanResCodeMessage[i-1176][1];
			}else if(i >= 1301 && i <= 1303){
				retCode = shinhanResCodeMessage[i-1272][1];
			}else if(i >= 1400 && i <= 1418){
				retCode = shinhanResCodeMessage[i-1368][1];
			}else if(i >= 1501 && i <= 1510){
				retCode = shinhanResCodeMessage[i-1450][1];
			}else if(i >= 2001 && i <= 2017){
				retCode = shinhanResCodeMessage[i-1940][1];
			}else if(i >= 8888 && i <= 8888){
				retCode = shinhanResCodeMessage[i-8810][1];
			}else if(i >= 9998 && i <= 9999){
				retCode = shinhanResCodeMessage[i-9919][1];
			}
		}else if(card_kind.equals("hanamembers")){
			if(i >= 0 && i <= 108){
				retCode = hanaResCodeMessage[i][1];
			}else if(i >= 1001 && i <= 1013){
				retCode = hanaResCodeMessage[i-892][1];
			}else if(i >= 1101 && i <= 1119){
				retCode = hanaResCodeMessage[i-978][1];
			}else if(i >= 1901 && i <= 2006){
				retCode = hanaResCodeMessage[i-1769][1];
			}else if(i >= 2116 && i <= 2124){
				retCode = hanaResCodeMessage[i-1978][1];
			}else if(i >= 3001 && i <= 3006){
				retCode = hanaResCodeMessage[i-2854][1];
			}else if(i >= 3102 && i <= 3108){
				retCode = hanaResCodeMessage[i-2949][1];
			}else if(i >= 3500 && i <= 3500){
				retCode = hanaResCodeMessage[i-3340][1];
			}else if(i >= 4001 && i <= 4003){
				retCode = hanaResCodeMessage[i-3840][1];
			}else if(i >= 5001 && i <= 5065){
				retCode = hanaResCodeMessage[i-4837][1];
			}else if(i >= 6001 && i <= 6027){
				retCode = hanaResCodeMessage[i-5772][1];
			}
		}else if(card_kind.equals("bccard")) {
			if(i == 0) {
				retCode=bcResCodeMessage[i][1];
			}else if(i == 17) {
				retCode=bcResCodeMessage[i - 16][1];
			}else if(i == 19) {
				retCode=bcResCodeMessage[i - 16][1];
			}else if(i >= 1100 && i <= 1101) {
				retCode=bcResCodeMessage[i - 1097][1];
			}else if(i == 1103) {
				retCode=bcResCodeMessage[i - 1098][1];
			}else if(i == 1105) {
				retCode=bcResCodeMessage[i - 1099][1];
			}else if(i >= 1108 && i <= 1109) {
				retCode=bcResCodeMessage[i - 1101][1];
			}else if(i >= 1201 && i <= 1202) {
				retCode=bcResCodeMessage[i - 1192][1];
			}else if(i == 1204) {
				retCode=bcResCodeMessage[i - 1193][1];
			}else if(i >= 1209 && i <= 1210) {
				retCode=bcResCodeMessage[i - 1197][1];
			}else if(i >= 1213 && i <= 1215) {
				retCode=bcResCodeMessage[i - 1198][1];
			}else if(i == 1302) {
				retCode=bcResCodeMessage[i - 1184][1];
			}else if(i == 1801) {
				retCode=bcResCodeMessage[i - 1782][1];
			}else if(i == 1805) {
				retCode=bcResCodeMessage[i - 1785][1];
			}else {
				retCode="알수없는 응답코드";
			}
		}
		
		return retCode;
	}
	
	/**
	 * 하나 멤버스 오류코드표(5자리)
	 * 구분값 : (0, 0), (109, 1001), (123, 1101), (132, 1901), (138, 2116), (147, 3001), (153, 3102), (160, 3500), (161, 4001), (164, 5001), (229, 6001)
	 */
	private String hanaResCodeMessage[][] ={
			{"00000", "정상적으로 처리 되었습니다."}
			,{"00001", "이미 처리되었습니다."}
			,{"00002", "중복자료가 있습니다."}
			,{"00003", "취소할 자료가 없습니다."}
			,{"00004", "전문유형 입력 오류입니다."}
			,{"00005", "업무구분 입력 오류입니다."}
			,{"00006", "제휴사코드 입력 오류입니다."}
			,{"00007", "거래일자 입력 오류입니다."}
			,{"00008", "거래시간 입력 오류입니다."}
			,{"00009", "채널코드 입력 오류입니다."}
			,{"00010", "응답코드 입력 오류입니다."}
			,{"00011", "적립유형 입력 오류입니다."}
			,{"00012", "사용유형 입력 오류입니다."}
			,{"00013", "적립/적립취소 머니 입력 오류입니다."}
			,{"00014", "사용/사용취소 머니 입력 오류입니다."}
			,{"00015", "금액 입력 오류입니다."}
			,{"00016", "고객식별구분코드 입력 오류입니다."}
			,{"00017", "고객식별구분값 입력 오류입니다."}
			,{"00018", "카드매체구분 입력 오류입니다."}
			,{"00019", "해당 카드번호가 유효하지 않습니다."}
			,{"00020", "해당 하나멤버스 고객번호가 유효하지 않습니다"}
			,{"00021", "하나멤버스 미등록 고객입니다."}
			,{"00022", "거래사유코드 입력 오류입니다."}
			,{"00023", "유효기간 입력 오류입니다."}
			,{"00024", "가용처리구분 입력 오류입니다."}
			,{"00025", "가용화일자 입력 오류입니다."}
			,{"00026", "원승인일자 입력 오류입니다."}
			,{"00027", "원승인번호 입력 오류입니다."}
			,{"00028", "원관계사고유식별번호 입력 오류입니다."}
			,{"00029", "사용자ID 입력 오류입니다."}
			,{"00030", "본인인증구분코드 입력 오류입니다."}
			,{"00031", "비밀번호 입력 오류입니다."}
			,{"00032", "3회 비밀번호 오류로 이용불가 합니다."}
			,{"00033", "비밀번호가 틀립니다."}
			,{"00034", "사용할 머니가 부족합니다."}
			,{"00035", "지금은 사용할 수 없습니다."}
			,{"00036", "사용금액이 초과하였습니다."}
			,{"00037", "이미 사용되었습니다."}
			,{"00038", "이미 적립되었습니다."}
			,{"00039", "이미 취소되었습니다."}
			,{"00040", "하나멤버스 카드 사용이 불가능한 가맹점입니다."}
			,{"00041", "해당 가맹점에서 하나멤버스를 사용할 수 없습니다."}
			,{"00042", "하루에 한번 만 가능합니다."}
			,{"00043", "일 사용한도를 초과하였습니다."}
			,{"00044", "월 사용한도를 초과하였습니다."}
			,{"00045", "조회구분 입력 오류입니다."}
			,{"00046", "조회구분값 입력 오류입니다."}
			,{"00047", "사용할 수 없는 카드입니다."}
			,{"00048", "거래금액 입력 오류입니다."}
			,{"00049", "할인금액 입력 오류입니다."}
			,{"00050", "하나멤버스 할인금액 입력 오류입니다."}
			,{"00051", "머니적립대상금액 입력 오류입니다."}
			,{"00052", "하나멤버스 하나머니 사용 입력오류입니다."}
			,{"00053", "유효기간구분 입력 오류입니다."}
			,{"00054", "관계사고유식별번호 입력 오류입니다."}
			,{"00055", "결제일자 입력 오류입니다."}
			,{"00056", "사후적립여부 입력 오류입니다."}
			,{"00057", "미등록 카드입니다."}
			,{"00058", "제휴상태가 정상이 아닙니다."}
			,{"00059", "{0} 불가한 제휴사입니다."}
			,{"00060", "제휴사정보가 존재하지 않습니다."}
			,{"00061", "적립이 불가한 가맹점입니다."}
			,{"00062", "적용기간이 유효하지 않습니다."}
			,{"00063", "제휴계약정보가 존재하지 않습니다."}
			,{"00064", "분실신고된 카드입니다."}
			,{"00065", "해지된 카드입니다."}
			,{"00066", "거래가 불가한 연령입니다."}
			,{"00067", "사후적립 제한 일수를 초과하였습니다."}
			,{"00068", "적립총건수 입력 오류입니다."}
			,{"00069", "사용총건수 입력 오류입니다."}
			,{"00070", "원거래가 존재하지 않습니다."}
			,{"00071", "취소대상 머니가 부족합니다."}
			,{"00072", "거래가 불가한 제휴사입니다."}
			,{"00073", "{0} 항목은 필수 입력입니다."}
			,{"00074", "요청한 머니가 적립율로 계산한 머니와 다릅니다."}
			,{"00075", "1회 적립한도을 초과하였습니다."}
			,{"00076", "일 적립한도횟수를 초과하였습니다."}
			,{"00077", "일 적립한도를 초과하였습니다."}
			,{"00078", "월 적립한도를 초과하였습니다."}
			,{"00079", "1회 사용한도을 초과하였습니다."}
			,{"00080", "거래금액이 원거래와 일치하지 않습니다."}
			,{"00081", "탈회 고객입니다."}
			,{"00082", "사용최소 머니 미달입니다."}
			,{"00083", "사용머니가 사용 기준단위에 위배됩니다."}
			,{"00084", "적립최소금액 미달입니다."}
			,{"00085", "적립취소 허용 일수를 초과하였습니다."}
			,{"00086", "사용취소 허용 일수를 초과하였습니다."}
			,{"00087", "하나머니 결제여부 입력 오류입니다"}
			,{"00088", "본인인증구분값 입력 오류입니다."}
			,{"00089", "비밀번호가 등록되지 않았습니다."}
			,{"00090", "저장하시겠습니까?"}
			,{"00091", "자료가 존재하지 않습니다."}
			,{"00092", "기타오류가 발생되었습니다."}
			,{"00093", "하나멤버스 시스템 작업 중입니다"}
			,{"00094", "하나멤버스 시스템 마감작업 중입니다."}
			,{"00095", "하나멤버스 시스템 점검 중입니다."}
			,{"00096", "회선연결 실패가 발생하였습니다."}
			,{"00097", "세션키 요청 중 오류가 발생하였습니다."}
			,{"00098", "세션키 수신 중 오류가 발생하였습니다."}
			,{"00099", "데이터 암호화 중 오류가 발생하였습니다."}
			,{"00100", "데이터 복호화 중 오류가 발생하였습니다."}
			,{"00101", "데이터 버퍼 전송 중 오류가 발생하였습니다."}
			,{"00102", "데이터 버퍼 수신 중 오류가 발생하였습니다."}
			,{"00103", "데이터 수신 중 오류가 발생하였습니다."}
			,{"00104", "데이터 송신 중 오류가 발생하였습니다."}
			,{"00105", "암호화 핸드쉐이크 오류가 발생하였습니다."}
			,{"00106", "세션키 전송 중 오류가 발생하였습니다."}
			,{"00107", "데이터 전송 중 오류가 발생하였습니다."}
			,{"00108", "처리중 오류가 발생하였습니다. 잠시 후에 다시 시도하여주시기 바랍니다."}
			//109번 ~ 122번
			,{"01001", "요청구분값 오류 입니다."}
			,{"01002", "하나멤버스 고객번호가 미존재 합니다."}
			,{"01003", "필수 입력값 오류 입니다."}
			,{"01004", "이용정지 종료일자가 초과 되었습니다."}
			,{"01005", "이용정지 연속 횟수 초과 오류 입니다."}
			,{"01006", "카드번호가 미존재 합니다."}
			,{"01007", "고객 상태가 비정상 입니다."}
			,{"01008", "등록되지 않는 코드입니다."}
			,{"01009", "서비스 이용 미동의 고객입니다."}
			,{"01010", "당일 탈회고객은 당일 재가입 불가 입니다."}
			,{"01011", "적립 사용 가능 등록은 불가 합니다."}
			,{"01012", "서비스 ID가 미존재 합니다."}
			,{"01013", "14세 미만은 하나멤버스 가입이 불가 입니다."}
			//109번 ~ 122번
			//123번 ~ 131번
			,{"01101", "이미 등록된 고객입니다."}
			,{"01102", "정보 등록 오류 입니다."}
			,{"01103", "정보 변경 오류 입니다."}
			,{"01104", "정보 조회 오류 입니다."}
			,{"01105", "이미 등록된 정보 입니다. "}
			,{"01106", "정보 이력 등록 오류 입니다."}
			,{"01107", "정보 삭제 오류 입니다."}
			,{"01108", "고객번호 채번 오류 입니다."} 
			,{"01109", "고객정보가 존재하지 않습니다."}
			,{"01110", "탈회신청 내역이 존재하지 않습니다."}
			,{"01111", "가용머니가 마이너스(-) 입니다."}
			,{"01112", "가용화되지 않은 머니가 존재합니다."}
			,{"01113", "P2P 대기 머니가 있으므로 탈회 불가 입니다."}
			,{"01114", "충전 머니가 남아있습니다."}
			,{"01115", "변경할 정보가 없습니다. "}
			,{"01116", "이미 이용정지 상태 입니다. "}
			,{"01117", "등록되어 있는 이용정지 내역이 있습니다. "}
			,{"01118", "암복화 처리 에러가 발생했습니다."}
			,{"01119", "충전머니가 있으므로 탈회 불가 입니다."}
			//123번 ~ 131번
			//132번 ~ 137번
			,{"01901", "필수 입력값({0}) 오류 입니다."}
			,{"01902", "입력값({0}) 오류 입니다."}
			,{"02000", "이미 승인상태가 \"승인확정\" 상태 입니다."}
			,{"02001", "대행승인번호 중복 오류입니다."}
			,{"02002", "{0} 검증 오류입니다."}
			,{"02003", "{0} 중복 오류입니다."}
			,{"02004", "{0} 저장 오류입니다."}
			,{"02005", "선물받은 머니는 다시 선물 할 수 없습니다."}
			,{"02006", "선물 머니는 50만 이상은 할 수 없습니다."}
			//132번 ~ 137번
			//138번 ~ 146번
			,{"02116", "여러건의 자료가 존재하여 망취소를 할 수 없습니다.(적립취소만 가능)"}
			,{"02117", "소멸은 익월에만 가능합니다 .(소멸_예정_일자 오류)"}
			,{"02118", "발생_일자 월 중복 오류입니다."}
			,{"02119", "소멸내역이 존재합니다"}
			,{"02120", "취소 불가능한 거래내역입니다."}
			,{"02121", "선물하기 이관 오류입니다."}
			,{"02122", "{0} 자료가 존재하지 않습니다. "}
			,{"02123", "{0} 한도를 초과하였습니다."}
			,{"02124", "은행으로부터 응답시간이 지연되어 익일 처리 결과를 알려드리겠습니다."}
			//138번 ~ 146번
			//147번 ~ 152번
			,{"03001", "요청구분값 오류 입니다."}
			,{"03002", "해당 프로세스는 이미 처리되었습니다."}
			,{"03003", "필수 입력값 오류 입니다."}
			,{"03004", "해당 프로세스는 처리중 입니다."}
			,{"03005", "해당 정산작업은 이미 완료되었습니다."}
			,{"03006", "정산기준년월이 미존재 합니다."}
			//147번 ~ 152번
			//153번 ~ 159번
			,{"03102", "정보 등록 오류 입니다."}
			,{"03103", "정보 변경 오류 입니다."}
			,{"03104", "제휴사 정산기간 자동등록 입력 오류입니다."}
			,{"03105", "선행 작업이 미완료 상태 입니다."}
			,{"03106", "선행 마감 배치 대상 제외 입니다."}
			,{"03107", "정산처리된 기간이 존재 합니다."}
			,{"03108", "전일 마감작업이 미완료 상태 입니다."}
			//153번 ~ 159번
			//160번
			,{"03500", "마감년월 및 마감일자 정합성 오류입니다."}
			//161번 ~ 163번
			,{"04001", "필수 입력값 오류 입니다."}
			,{"04002", "데이터 길이 오류 입니다."}
			,{"04003", "선행작업({0}) 오류 입니다."}
			//161번 ~ 163번
			//164번 ~ 228번
			,{"05001", "등록 및 변경이 불가능한 채널 입니다."}
			,{"05002", "캠페인정보 미존재 오류입니다."}
			,{"05003", "종료된 캠페인정보는 수정이 불가합니다."}
			,{"05004", "현재 실행중인 캠페인정보는 변경이 불가합니다."}
			,{"05005", "캠페인시스템에서 기획된 정보는 변경이 불가합니다."}
			,{"05006", "이미 종료된 캠페인정보 입니다."}
			,{"05007", "캠페인시스템에서 기획된 정보는 쿠폰정보 등록이 불가합니다."}
			,{"05008", "오퍼정보 미존재 오류입니다."}
			,{"05009", "오퍼정보 사용기간이 아닙니다."}
			,{"05010", "쿠폰 종료일을 캠페인 종료일 이후로 설정 불가합니다."}
			,{"05011", "쿠폰 시작일을 캠페인 시작일 이전으로 설정 불가합니다."}
			,{"05012", "종료된 쿠폰정보는 수정이 불가합니다."}
			,{"05013", "현재 실행중인 캠페인쿠폰 정보는 변경이 불가합니다."}
			,{"05014", "동일 대상고객 존재 오류입니다."}
			,{"05015", "대상고객 등록은 타겟 캠페인만 가능합니다."}
			,{"05016", "캠페인쿠폰정보 미존재 오류입니다."}
			,{"05017", "사용불가 캠페인쿠폰 정보 입니다."}
			,{"05018", "사용불가 캠페인 정보 입니다."}
			,{"05019", "사용불가 오퍼 정보 입니다."}
			,{"05020", "캠페인쿠폰 발급가능 기간이 아닙니다."}
			,{"05021", "캠페인 기간이 아닙니다."}
			,{"05022", "모바일APP을 통해서만 발급이 가능한 쿠폰입니다."}
			,{"05023", "쿠폰 최대발급수량을 초과 하였습니다."}
			,{"05024", "MASS 캠페인 일 경우 쿠폰 일괄발급 처리 할 수 없습니다."}
			,{"05025", "거래정보 제공동의서 동의가 필요합니다."}
			,{"05026", "캠페인 대상고객이 아닙니다."}
			,{"05027", "발급가능 쿠폰이 존재하지 않습니다."}
			,{"05028", "쿠폰 유효종료일자가 쿠폰 적용기간을 초과하였습니다."}
			,{"05029", "고객에게 이미 발급된 쿠폰입니다."}
			,{"05030", "고객 보유쿠폰 미존재 오류입니다."}
			,{"05031", "사용완료된 쿠폰은 사용여부를 변경할 수 없습니다."}
			,{"05032", "관계사 쿠폰은 외부쿠폰을 등록할 수 없습니다."}
			,{"05033", "유효종료일자가 일괄발급일자보다 적을 수 없습니다."}
			,{"05034", "일괄발급일자가 쿠폰 적용기간보다 적거나 클 수 없습니다."}
			,{"05035", "유효종료일자가 쿠폰 적용기간보다 적거나 클 수 없습니다."}
			,{"05036", "캠페인 인터페이스 테이블 건수가 상이합니다."}
			,{"05037", "오퍼 인터페이스 테이블 건수가 상이합니다."}
			,{"05038", "캠페인쿠폰 인터페이스 테이블 건수가 상이합니다."}
			,{"05039", "쿠폰사용채널 인터페이스 테이블 건수가 상이합니다."}
			,{"05040", "캠페인대상고객 인터페이스 테이블 건수가 상이합니다."}
			,{"05041", "캠페인 인터페이스 테이블 데이터 오류입니다."}
			,{"05042", "오퍼 인터페이스 테이블 데이터 오류입니다."}
			,{"05043", "캠페인쿠폰 인터페이스 테이블 데이터 오류입니다."}
			,{"05044", "쿠폰사용채널 인터페이스 테이블 데이터 오류입니다."}
			,{"05045", "캠페인대상고객 인터페이스 테이블 데이터 오류입니다."}
			,{"05046", "서비스정보제공동의가 불필요한 쿠폰 입니다."}
			,{"05047", "이미 종료된 캠페인쿠폰 정보 입니다."}
			,{"05048", "전체가맹점은 적용대상여부를 미적용으로 등록할 수 없습니다."}
			,{"05049", "사용이 불가능한 쿠폰 입니다."}
			,{"05050", "쿠폰사용 가능기간이 아닙니다."}
			,{"05051", "이미 사용된 쿠폰 입니다."}
			,{"05052", "쿠폰사용이 불가능한 채널입니다."}
			,{"05053", "쿠폰사용 비밀번호가 맞지 않습니다."}
			,{"05055", "오퍼수량은 발급한도수량을 초과할 수 없습니다."}
			,{"05056", "유입채널 미존재 오류입니다."}
			,{"05057", "쿠폰발급채널 인터페이스 테이블 데이터 오류입니다."}
			,{"05058", "쿠폰발급채널 인터페이스 테이블 건수가 상이합니다."}
			,{"05059", "사용구분값 입력 오류입니다."}
			,{"05060", "일한도수량이 최대한도수량을 초과할 수 없습니다."}
			,{"05061", "쿠폰 일발급수량을 초과 하였습니다."}
			,{"05062", "발급이나 응모가 불가능한 이벤트 입니다."}
			,{"05063", "발급 및 응모 가능한 채널이 아닙니다."}
			,{"05064", "발송_예정_일자는 오늘일자 이후로 설정가능합니다."}
			,{"05065", "다운로드 발급이 불가한 쿠폰 입니다."}
			//164번 ~ 228번
			//229번 ~ 255번
			,{"06001", "제휴사정보 오류 입니다."}
			,{"06002", "동일 제휴사정보 존재 오류입니다."}
			,{"06003", "동일 가맹점정보 존재 오류입니다."}
			,{"06004", "시작일자는 오늘일자 이후로 설정가능합니다."}
			,{"06005", "시작일자가 종료일자보다 클 수 없습니다."}
			,{"06006", "기존계약의 계약일자와 중복됩니다.(기존 계약일자 수정필요)"}
			,{"06007", "유효시작일자가 제휴계약 시작일자보다 적을 수 없습니다."}
			,{"06008", "유효종료일자가 제휴계약 종료일자보다 클 수 없습니다."}
			,{"06009", "제휴계약정보가 존재하지 않습니다."}
			,{"06010", "제휴사정보가 존재하지 않습니다."}
			,{"06011", "가맹점정보가 존재하지 않습니다."}
			,{"06012", "해당 계약이 유효하지 않습니다."}
			,{"06013", "거래불가 제휴사 입니다."}
			,{"06014", "미개시 제휴사 입니다."}
			,{"06015", "제휴계약이 만료된 제휴사 입니다."}
			,{"06016", "거래가 불가능한 가맹점 입니다."}
			,{"06017", "영업을 시작하지 않은 가맹점 입니다."}
			,{"06018", "가맹해지된 가맹점 입니다."}
			,{"06019", "결제단계 요청이 올바르지 않습니다."}
			,{"06020", "승인확정된 데이터이므로 수정이 불가합니다."}
			,{"06021", "승인요청 시 기존 데이터 수정이 불가합니다."}
			,{"06022", "승인요청 완료 후 승인확정 처리하세요."}
			,{"06023", "승인확정 시 기존 데이터 수정이 불가합니다."}
			,{"06024", "승인요청 또는 승인확정 기간이 초과하였습니다."}
			,{"06025", "승인요청 및 승인확정 데이터는 수정이 불가합니다."}
			,{"06026", "승인요청 전 제휴_계약_룰을 등록하시기 바랍니다."}
			,{"06027", "승인요청 전 제휴_가맹_계약을 등록하시기 바랍니다."}
			//229번 ~ 255번
	};
	
	/**
	 * 신한카드 오류코드표(4자리)
	 * 구분값 : (0, 0), (1, 31), (4, 36), (5, 94), (8, 332), (9, 402), (10, 1001), (13, 1011), (24, 1029), (25, 1201), (29, 1301), (32, 1400), (51, 1501)
	 * 		,(61, 2001), (78, 8888), (79, 9998), (80, 0038)
	 */
	private String shinhanResCodeMessage[][]={
			{"0000", "정상                                                   "}
			//1번 ~ 3번
			,{"0031", "홀드원장 반영오류입니다.                                    " }
			,{"0032", "총포인트원장 반영오류입니다.                                  "}
			,{"0033", "승인거래일자 또는 승인거래시간을 확인하시기 바랍니다.               " }
			//1번 ~ 3번
			//4번
			,{"0036", "승인처리된 홀드포인트 가  0(ZERO) 입니다.                     " }
			//5번 ~ 7번
			,{"0094", "포인트캐쉬백신청내역결과반영 오류입니다.                      "}
			,{"0095", "신한은행 연계장애로 인해 처리가 불가 합니다.                  "}
			,{"0096", "포인트실시간캐쉬백 신한은행 연계처리시 TIME-OUT 이 발생 했습니다."}
			//5번 ~ 7번
			//8번
			,{"0332", "홀드 상태 승인건에 해당하는 홀드포인트정보가  미존재합니다.          " }
			//9번
			,{"0402", "처리할수없는 업무구분코드입니다.                               "}
			//10번 ~ 12번
			,{"1001", " 비회원                                                 "}
			,{"1002", " 포인트부족                                              "}
			,{"1003", " 포인트종류오류                                           "}
			//10번 ~ 12번
			//13번 ~ 23번
			,{"1011", "전문번호오류                                             " }
			,{"1012", "거래유형업무오류                                           "}
			,{"1013", "신한그룹사코드가 잘못설정되었습니다.                            "}          
			,{"1014", "전문버전이 잘못되었습니다.                                   "}  
			,{"1015", "총매출금액오류                                            "}
			,{"1016", "포인트사용일자오류                                         " }
			,{"1017", "포인트사용시간오류                                         " }
			,{"1018", "포인트사용유형오류 (사용방법)                                 "}
			,{"1019", "가맹점번호를 입력하시기바랍니다                                "}
			,{"1020", "거래고유번호오류                                           "}
			,{"1021", "거래요청자오류                                            "}
			//13번 ~ 23번
			//24번
			,{"1029", " 사용일자 미입력 오류                                      " }       
			//25번 ~ 28번
			,{"1201", "취소중복요청건                                            "}
			,{"1202", "원거래 없음                                              "}
			,{"1203", "원거래 오류건                                            " }
			,{"1204", "원거래 정보불일치건                                        " }
			//25번 ~ 28번
			//29번 ~ 31번
			,{"1301", "기요청거래건 동일한 CI값과 거래고유번호건을 재요청한경우             "}     
			,{"1302", "기사용취소성공건재요청오류                                    "}
			,{"1303", "포인트종류상세코드 값없음                                    "}      
			//29번 ~ 31번
			//32번 ~ 50번
			,{"1400", "  사용요청포인트가 0 입니다                                 " }            
			,{"1401", "  포인트_포인트사용 원장 등록시 중복                           "}           
			,{"1402", "  포인트월집계원장의 최종결제일자 정보가 존재하지 않음              " }    
			,{"1403", "  포인트월집계원장 미존재시 반영오류                           " }         
			,{"1404", " 포인트월집계미존재시 포인트사용상세원장 등록 반영오류               "}     
			,{"1405", " 포인트월집계원장 선입선출 반영오류                            " }         
			,{"1406", " 포인트사용상세원장 등록 반영오류                              "}           
			,{"1407", " 타사매출분 사용차감 포인트월집계원장 미존재시 반영오류              "}     
			,{"1408", " 타사비매출분 사용차감 포인트월집계원장 미존재시 반영오류            " }   
			,{"1409", " 선입선출 차감후 잔여분 포인트사용상세원장 등록 반영오류             "}     
			,{"1410", " 총포인트원장 잔여정보 미존재                                 "}            
			,{"1411", " 총포인트원장 사용차감 반영오류                               " }          
			,{"1412", " 사용후잔여포인트 포인트사용원장 반영오류                        " }       
			,{"1413", " IFRS회계변동내역반영 여부를 확인하세요                        " }         
			,{"1414", " 포인트IFRS회계변동내역반영시 오류                            " }          
			,{"1415", " 체크카드환급사용시에는 차감포인트와 환급포인트를 반드시 입력 해야 합니다."}
			,{"1416", " 상품서비스 수혜이력 등록  반영시 오류                      "}              
			,{"1417", " 승인계 및 마케팅팀 EAI송신 처리 오류 ..!                  "}               
			,{"1418", " 사용처명은 필수입력입니다                                "}     
			//32번 ~ 50번
			//51번 ~ 60번
			,{"1501", "적립요청 현업승인거절                                   "}
			,{"1502", "적립요청일까지 현업승인 안됨                              "}
			,{"1503", "이벤트명 미입력오류                                     "}
			,{"1504", "문서번호 미입력오류                                     "}
			,{"1505", "요청자사번 미입력오류                                   "}
			,{"1506", "승인부서 미입력오류                                     "}
			,{"1507", "승인자사번 미입력오류                                   "}
			,{"1508", "적립요청일자 미입력오류                                  "}
			,{"1509", "적립요청일자 영업일자 아님오류                             "}
			,{"1510", "적립요청일자 D+3 아님오류                               "}
			//51번 ~ 60번
			//61번 ~ 77번
			,{"2001", "고객번호 미입력 에러                                    "}
			,{"2002", "이용동의여부 미입력 에러                                 "}
			,{"2003", "이용동의철회여부 미입력 에러                              "}
			,{"2004", "고객명 미입력 에러                                     "}
			,{"2005", "법정 생년월일 성별값 미입력 에러                           "}
			,{"2006", "휴대전화번호 미입력 에러                                 "}
			,{"2007", "이용동의요청시에는 이용동의여부가 Y이어야 합니다.               "}
			,{"2008", "이용동의요청시에는 이용동의철회여부가 N이어야 합니다.            "}
			,{"2009", "서비스이용 및 마케팅동의여부 Y 입력 에러                     "}
			,{"2010", "CI값이 미입력 에러                                     "}
			,{"2011", "판클럽회원정보 insert 오류                              "}
			,{"2012", "이용동의요청인경우 이용동의여부 Y, 이용동의철회여부 N 이어야 합니다."}
			,{"2013", "판클럽회원정보변경 update 오류                           "}
			,{"2014", "이용동의철회요청시에는 이용동의여부가 N이어야 합니다.            "}
			,{"2015", "이용동의철회요청시에는 이용철회여부가 Y이어야 합니다.            "}
			,{"2016", "판클럽회원정보 탈회 update 오류                          "}
			,{"2017", "판클럽회원정보변경이력 insert 오류                        "}
			//61번 ~ 77번
			//78번
			,{"8888", "신한카드 디퍼드 미처리건 대사요청                          "}
			//79번 ~ 80번
			,{"9998", "공통모듈익셉션오류                                      "}
			,{"9999", "처리실패                                             "}
			//79번 ~ 80번
			//81번
			,{"0038", "월 100,000원 초과"}
	};
	
	/**
	 * 포인트 쿠폰 오류코드표
	 * 0~6, 7~8(11~12), 9(14), 10(19), 11~12(80~81), 13(84), 14~16(97~99)
	 */
	@SuppressWarnings("unused")
	private String couponResCodeMessage[][]={
			{"00", "완료        "}
			,{"01", "미존재쿠폰   "}
			,{"02", "비정상쿠폰번호 "}
			,{"04", "기간만료쿠폰 "}
			,{"05", "사용된 쿠폰  "}
			,{"06", "결제취소 쿠폰"}
			
			,{"11", "전문헤더오류 "}
			,{"12", "전문종료오류 "}
			
			,{"14", "업체코드 오류"}
			
			,{"19", "매장불일치   "}
			
			,{"80", "상품 종류 코드 불일치 오류"}
			,{"81", "상품 종류 코드 불일치 오류"}
			
			,{"84", "상품 종류 코드 불일치 오류"}
			
			,{"97", "가맹점 코드는 필수입니다."}
			,{"98", "입력값 오류"}
			,{"99", "기타오류   "}	
	};
	
	private String bcResCodeMessage[][] = {
			// 0
			{"0	",	"정상"}
			// 1
			, {"17",	"기사용 처리됨"}
			// 2
			, {"19	","기취소 처리됨"}
			// 3 ~ 4
			, {"1100",	"유효하지 않은 거래요청일자"}
			, {"1101",	"유효하지 않은 주체구분"}
			// 5
			, {"1103",	"유효하지않은 주체번호"}
			// 6
			, {"1105",	"유효하지 않은 카드번호"}
			// 7 ~ 8
			, {"1108",	"유효하지 않은 회원사번호"}
			, {"1109",	"유효하지 않은 포인트유형"}
			// 9 ~ 10
			, {"1201",	"유효포인트 부족"}
			, {"1202",	"최소사용가능포인트 미만"}
			// 11
			, {"1204",	"본인카드 이외 사용불가"}
			// 11 ~ 14
			, {"1209",	"연체회원"}
			, {"1210",	"사고코드등재회원"}
			, {"1211",	"유효회원아님"}
			// 15 ~ 17
			, {"1213",	"일한도초과"}
			, {"1214",	"월한도초과"}
			, {"1215",	"연한도초과"}
			// 18
			, {"1302",	"중복내역 존재"}
			// 19
			, {"1801",	"원거래없음"}
			// 20
			, {"1805",	"당일외취소불가"}
			// 21
			, {"0016", "원 거래번호 없음"}
	};
	
	private String kbResCodeMessage[][] = {
			// 0
			{"01", "미등록 CI번호"}
			// 1
			,{"17", "포인트이용취소 원래거래일련번호 미존재 또는 불일치"}
			// 2
			,{"90", "카드유효기간 오류"}
			// 3 ~ 9
			,{"93", "본인 카드가 아님"}
			,{"94", "미등록 카드번호"}
			,{"95", "고객정보제공 미동의 고객"}
			,{"96", "신청후 30 일  이전에만  정정이  가능합니다 "}
			,{"97", "회원별   거래정지인   경우   포인트이용 불가합니다"}
			,{"98", "연체 20,000 원이상인경우　포인트이용불가"}
			,{"99", " 포인트처리   오류입니다 "}
			// 10
			,{"00", "정상"}
			// 11
			,{"91", "전환포인트부족"}
	};
}