/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.clip3_0.service;

import java.awt.Color;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import com.wiz.clip.clip3_0.bean.CardPointSwapList;
import com.wiz.clip.clip3_0.bean.LinkPriceItemInfo;
import com.wiz.clip.clip3_0.bean.LinkPriceReportItem;
import com.wiz.clip.clip3_0.bean.LinkPriceReportMessege;
import com.wiz.clip.clip3_0.bean.LinkpriceShopHistDtl;
import com.wiz.clip.clip3_0.bean.MainBannerDO;
import com.wiz.clip.clip3_0.bean.PointCouponHist;
import com.wiz.clip.clip3_0.bean.PointbombInfo;
import com.wiz.clip.clip3_0.bean.ScreenLockDAO;
import com.wiz.clip.clip3_0.bean.ScreenLockRandomPointMDAO;
import com.wiz.clip.clip3_0.bean.UserTokenInfo;
import com.wiz.clip.clip3_0.dao.Clip_3_0_DAO;
import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.common.util.DateUtil;
import com.wiz.clip.common.util.HttpClientUtil;
import com.wiz.clip.manage.pointChk.bean.SearchBean;

@Service
@Transactional
public class Clip_3_0_ServiceImpl implements Clip_3_0_Service {
	
	@Value("#{config['LOCAL_IP_6']}") String LOCAL_IP_6;
	@Value("#{config['LINKPRICE_BASE_URL']}") private String LINKPRICE_BASE_URL;
	@Value("#{config['LINKPRICE_A_ID']}") private String LINKPRICE_USER;
	@Value("#{config['LINKPRICE_AUTH_KEY']}") private String LINKPRICE_PASS;
	
	private static final String RESULT_CODE_SUCCESS = "0000";
	
	@Autowired
	private Clip_3_0_DAO clip_3_0_Dao;
	
	@Autowired
	private SqlSession postgreSession;
	
	Logger logger = Logger.getLogger(Clip_3_0_ServiceImpl.class.getName());

	@Override
	public List<?> getCardpointChangeList(ModelMap obj) throws Exception {
		return clip_3_0_Dao.select_cardpoint_list(obj);
		
	}

	@Override
	public String getCardpointChangeListTotalValue(ModelMap map) throws Exception {
		return clip_3_0_Dao.select_cardpoint_list_totalcount(map);
	}

	@Override
	public String getCardpointChangeListTotalCount(ModelMap map) throws Exception {
		return clip_3_0_Dao.select_cardpoint_listcount(map);
	}

	@SuppressWarnings("finally")
	@Override
	public List<UserTokenInfo> selectUserToken(SearchBean searchBean) throws Exception {
		try{
			if(searchBean.getStatus().equals("ctn")){
				String origin_data = searchBean.getKeywd();
				String keywd = AES256CipherClip.AES_Encode(origin_data.trim());
				searchBean.setKeywd(keywd);
			}
		}catch(NullPointerException e){
			logger.debug(e.getMessage());
		}catch(Exception e) {
			logger.error(e.getMessage(), e);
		}finally{
			if(searchBean.getStatus().equals("") || searchBean.getStatus() == null) {
				return clip_3_0_Dao.selectUserTokenSearchAll(searchBean);
			}else {
				return clip_3_0_Dao.selectUserTokenSelectOne(searchBean);
			}
		}
	}

	@Override
	public String selectItemCount(LinkPriceReportItem item) throws Exception {
		return clip_3_0_Dao.selectItemCount(item);
	}
	
	@Override
	public String selectCouponListCount(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectCouponListCount(model);
	}

	@SuppressWarnings("finally")
	@Override
	public List<UserTokenInfo> selectUserCi(ModelMap model) throws Exception {
		try{
			if(model.get("status").toString().equals("ctn")){
				model.put("keywd", AES256CipherClip.AES_Encode(model.get("keywd").toString()));
			}
		}catch(NullPointerException e){
			System.out.println(e.getMessage());
		}finally{
			if(model.get("status").toString().equals("") || model.get("status") == null) {
				return clip_3_0_Dao.selectUserCiAll(model);
			}else {
				return clip_3_0_Dao.selectUserCiOne(model);
			}
		}
	}

	@Override
	public String selectCoupontotalPoint(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectCoupontotalPoint(model);
	}

	@Override
	public List<PointCouponHist> selectPointCouponHistList(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectPointCouponHistList(model);
	}

	@Override
	public String selectShopHistTotalvalue(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectShopHistTotalvalue(model);
	}

	@Override
	public List<UserTokenInfo> selectUserInfoList() throws Exception {
		return clip_3_0_Dao.selectUserInfoList();
	}

	@Override
	public LinkPriceReportMessege getNextSavingHistory(LinkPriceReportMessege param) throws Exception {
		HttpClientUtil conn = null;
		LinkPriceReportMessege result = new LinkPriceReportMessege();
		
		try {
			//http://api.linkprice.com/affiliate/openapi.php?user=A100000131&auth_key=bcb2842d247fdf4fb9b5c7568d78883e&method=report.kt_clip_list&yyyymm=201705&u_id=test1234
			conn = new HttpClientUtil("GET", LINKPRICE_BASE_URL);
			conn.addHeader("Content-type", "text/html");
			
			conn.addParameter("method", "report.kt_clip_list");
			conn.addParameter("user", LINKPRICE_USER);
			conn.addParameter("auth_key", LINKPRICE_PASS);
			conn.addParameter("yyyymm", DateUtil.getDate2String(DateUtil.addMonth(-1), "yyyyMM"));
			conn.addParameter("u_id", param.getuId());

			int ret = conn.sendRequest();
			logger.info("LinkPrice getNextSavingHistory response status : " + ret);
			if(ret == HttpStatus.SC_OK) {
				result = conn.getResponseJson(LinkPriceReportMessege.class);
				logger.debug("Res : "+result.getResultCode()+", "+result.getResultMsg());
				
				if(RESULT_CODE_SUCCESS.equals(result.getResultCode())) {
					
//					List<LinkPriceReportItem> newList = new ArrayList<LinkPriceReportItem>();
					for(LinkPriceReportItem data : result.getOrder_list()) {
						//TODO 스테이터스 체크
						logger.debug("익월 적립예정 내역 :: " + data.toString());
					}
//					result.setOrder_list(newList);
					result.setResult("S");
					logger.info("LinkPrice getNextSavingHistory Success Response");
				} else {
					result.setResult("F");
					result.setResultMsg(result.getResultMsg());
					logger.info("LinkPrice getNextSavingHistory Error Fail Response");
				}
			} else {
				result.setResult("F");
				result.setResultMsg("오류가 발생하였습니다.");
				logger.info("LinkPrice getNextSavingHistory Error HTTP : code["+ret+"] ");
			}
			
		} catch (NullPointerException e) {
			result.setResult("N");
			result.setResultMsg("적립 예정 포인트 내역이 없습니다.");
			conn.closeConnection();
			logger.info("Shopbuy NowSaving NullPointerException : message["+e.getMessage()+"] ");
		} catch (Exception e) {
			result.setResult("F");
			result.setResultMsg("오류가 발생하였습니다.");
			
			conn.closeConnection();
			logger.error(e.getMessage());
			logger.info("LinkPrice getNextSavingHistory Error System : message["+e.getMessage()+"] ");
		}
		
		return result;
	}

	@Override
	public LinkPriceReportMessege getNext2SavingHistory(LinkPriceReportMessege param) throws Exception {
		//TODO --- 아래부터가 진짜임
		HttpClientUtil conn = null;
		LinkPriceReportMessege result = new LinkPriceReportMessege();
		
		try {
			//http://api.linkprice.com/affiliate/openapi.php?user=A100000131&auth_key=bcb2842d247fdf4fb9b5c7568d78883e&method=report.kt_clip_list&yyyymm=201706&u_id=test1234
			conn = new HttpClientUtil("GET", LINKPRICE_BASE_URL);
			conn.addHeader("Content-type", "text/html");
			
			conn.addParameter("method", "report.kt_clip_list");
			conn.addParameter("user", LINKPRICE_USER);
			conn.addParameter("auth_key", LINKPRICE_PASS);
			conn.addParameter("yyyymm", DateUtil.getDate2String(DateUtil.addMonth(0), "yyyyMM"));
			conn.addParameter("u_id", param.getuId());
			
			int ret = conn.sendRequest();
			logger.info("LinkPrice getNext2SavingHistory response status : " + ret);
			
			if(ret == HttpStatus.SC_OK) {
				result = conn.getResponseJson(LinkPriceReportMessege.class);
				
				logger.debug("Res : "+result.getResultCode()+", "+result.getResultMsg());

				if(RESULT_CODE_SUCCESS.equals(result.getResultCode())) {
//					List<LinkPriceReportItem> newList = new ArrayList<LinkPriceReportItem>();
					for(LinkPriceReportItem data : result.getOrder_list()) {
						//TODO 스테이터스 체크
						logger.debug("익익월 적립예정 내역 :: " + data.toString());
					}
					result.setResult("S");
					logger.info("LinkPrice getNext2SavingHistory Success Response");
				} else {
					result.setResult("F");
					result.setResultMsg(result.getResultMsg());
					logger.info("LinkPrice getNext2SavingHistory Error Fail Response");
				}
				
			} else {
				result.setResult("F");
				result.setResultMsg("오류가 발생하였습니다.");
				logger.info("LinkPrice getNext2SavingHistory Error HTTP : code["+ret+"] ");
			}
			
		} catch (NullPointerException e) {
			result.setResult("N");
			result.setResultMsg("적립 예정 포인트 내역이 없습니다.");
			conn.closeConnection();
			logger.info("Shopbuy NowSaving NullPointerException : message["+e.getMessage()+"] ");
		} catch (Exception e) {
			result.setResult("F");
			result.setResultMsg("오류가 발생하였습니다.");
			
			conn.closeConnection();
			logger.error(e.getMessage());
			logger.info("LinkPrice getNext2SavingHistory Error System : message["+e.getMessage()+"] ");
		}

		return result;
	}
	
	/** 쇼핑 적립 상품 리스트 */
	@Override
	public List<LinkPriceItemInfo> getShoppinMallList() throws Exception {
		return clip_3_0_Dao.select_shoppin_mall_list();
	}
	
	/** 쇼핑 적립 상품 리스트 */
	@Override
	public List<LinkpriceShopHistDtl> getShoppinMallSavingDetail(LinkpriceShopHistDtl linkpriceShopHistDtl) throws Exception {
		return clip_3_0_Dao.select_shopping_mall_saving_detail();
	}

	@Override
	public String selectShopHistCount(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectShopHistCount(model);
	}

	@Override
	public List<LinkpriceShopHistDtl> selectShopHistList(ModelMap model) throws Exception {
		if(model.get("searchYear") != null && !model.get("searchYear").toString().isEmpty()){
			String year = (String) model.get("searchYear");
			String mouth = (String) model.get("searchDate");
			if(Integer.parseInt(mouth) < 10){
				mouth = "0" + mouth;
			}
			String startDate = year + mouth + "01";
			String endDate = year + mouth + "31";
			System.out.println("########startDate :: " + startDate + " || endDate :: " + endDate);
			
			model.put("startDate", startDate);
			model.put("endDate", endDate);
		}
		
		return clip_3_0_Dao.selectShopHistList(model);
	}
	
	@Override
	public List<ScreenLockDAO> getScreenLockList() throws Exception {
		return clip_3_0_Dao.select_screen_lock_list();
	}

	@Override
	public ScreenLockDAO getScreenLockListDetail(String index) throws Exception {
		return clip_3_0_Dao.select_screen_lock(index);
	}

	@Override
	public int setScreenLockData(ScreenLockDAO obj) throws Exception {
		return clip_3_0_Dao.update_screen_lock(obj);
	}

	@Override
	public ScreenLockRandomPointMDAO getScreenLockRandomPointManagment() throws Exception {
		return clip_3_0_Dao.select_getScreenLockRandomPointManagment();
	}

	@Override
	public int setLock_Screen_Randompoint(ScreenLockRandomPointMDAO arg) throws Exception {
		return clip_3_0_Dao.update_Screen_Randompoint(arg);
	}

	@Override
	public void insertMainBannerInfoTbl(MainBannerDO arg) throws Exception {
		clip_3_0_Dao.insertMainBannerInfoTbl(arg);
	}

	@Override
	public MainBannerDO selectMainbannerInfoMaxRank() throws Exception {
		return clip_3_0_Dao.selectMainbannerInfoMaxRank();
	}

	@Override
	public String selectMainbannerInfoTotalcount(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectMainbannerInfoTotalcount(model);
	}

	@Override
	public List<MainBannerDO> selectMainbannerInfoList(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectMainbannerInfoList(model);
	}

	@Override
	public MainBannerDO selectMainbannerInfoOne(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectMainbannerInfoOne(model);
	}

	@Override
	public void updateMainBannerInfoTbl(MainBannerDO arg) throws Exception {
		clip_3_0_Dao.updateMainBannerInfoTbl(arg);
	}

	@Override
	public void deleteMainBannerInfoTbl(MainBannerDO arg) throws Exception {
		clip_3_0_Dao.deleteMainBannerInfoTbl(arg);
	}

	@Override
	public String selectPointbombTotalcount(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectPointbombTotalcount(model);
	}

	@Override
	public List<PointbombInfo> selectPointbombList(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectPointbombList(model);
	}

	@Override
	public LinkPriceItemInfo selectLinkPriceItemInfoMaxRank() throws Exception {
		return clip_3_0_Dao.selectLinkPriceItemInfoMaxRank();
	}

	@Override
	public void insertLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception {
		clip_3_0_Dao.insertLinkPriceItemInfo(arg);
	}

	@Override
	public List<LinkPriceItemInfo> selectLinkPriceItemInfoList() throws Exception {
		return clip_3_0_Dao.selectLinkPriceItemInfoList();
	}

	@Override
	public LinkPriceItemInfo selectLinkPriceItemInfoOne(ModelMap model) throws Exception {
		return clip_3_0_Dao.selectLinkPriceItemInfoOne(model);
	}

	@Override
	public void updateLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception {
		clip_3_0_Dao.updateLinkPriceItemInfo(arg);
	}

	@Override
	public void deleteLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception {
		clip_3_0_Dao.deleteLinkPriceItemInfo(arg);
	}

	@Override
	public String selectLinkPriceItemInfoIdcheck(LinkPriceItemInfo arg) throws Exception {
		return clip_3_0_Dao.selectLinkPriceItemInfoIdcheck(arg);
	}

	@Override
	public LinkPriceReportMessege getNowSavingHistory(LinkPriceReportMessege param) throws Exception {
		//TODO --- 아래부터가 진짜임
		HttpClientUtil conn = null;
		LinkPriceReportMessege result = new LinkPriceReportMessege();
				
		try {
			//http://api.linkprice.com/affiliate/openapi.php?user=A100000131&auth_key=bcb2842d247fdf4fb9b5c7568d78883e&method=report.kt_clip_sum&yyyymm=201705&u_id=test1234
			conn = new HttpClientUtil("GET", LINKPRICE_BASE_URL);
			conn.addHeader("Content-type", "text/html");
			
			conn.addParameter("method", "report.kt_clip_list");
			conn.addParameter("user", LINKPRICE_USER);
			conn.addParameter("auth_key", LINKPRICE_PASS);
			conn.addParameter("yyyymm", DateUtil.getDate2String(DateUtil.addMonth(-2), "yyyyMM"));
			conn.addParameter("u_id", param.getuId());
			
			logger.info("Shopbuy NowSaving request : u_id["+param.getuId()+"]");
			int ret = conn.sendRequest();
			
			logger.info("Shopbuy NowSaving response status : " + ret);
			
			if(ret == HttpStatus.SC_OK) {
				result = conn.getResponseJson(LinkPriceReportMessege.class);
				
				logger.debug("Res : "+result.getResultCode()+", "+result.getResultMsg());
				if(RESULT_CODE_SUCCESS.equals(result.getResultCode())) {
//					List<LinkPriceReportItem> newList = new ArrayList<LinkPriceReportItem>();
					for(LinkPriceReportItem data : result.getOrder_list()) {
						//TODO 스테이터스 체크
						logger.debug("당월 적립예정 내역 :: " + data.toString());
					}
					result.setResult("S");
					logger.debug("Order sum : "+result.getOrder_sum());
					logger.info("Shopbuy NowSaving Success : retcode["+result.getResultCode()+"],order_sum["+result.getOrder_sum()+"]");
					logger.info("LinkPrice getNext2SavingHistory Success Response");
				} else {
					result.setResult("F");
					result.setResultMsg(result.getResultMsg());
					logger.info("Shopbuy NowSaving Error Fail Response : retcode["+result.getResultCode()+"] ");
				}
				
			} else {
				result.setResult("F");
				result.setResultMsg("오류가 발생하였습니다.");
				logger.info("Shopbuy NowSaving Error HTTP : code["+ret+"] ");
			}
			
		} catch (NullPointerException e) {
			result.setResult("N");
			result.setResultMsg("적립 예정 포인트 내역이 없습니다.");
			conn.closeConnection();
			logger.info("Shopbuy NowSaving NullPointerException : message["+e.getMessage()+"] ");
		} catch (Exception e) {
			result.setResult("F");
			result.setResultMsg("오류가 발생하였습니다.");
			conn.closeConnection();
			logger.info("Shopbuy NowSaving Error System : message["+e.getMessage()+"] ");
		}
		
		return result;
	}

	@Override
	public void selectExcelCardpointChangeList(ModelMap map, HttpServletResponse res, HttpServletRequest req, Model model) throws Exception {
		SXSSFWorkbook wb = new SXSSFWorkbook(100);
		final Sheet sheet = wb.createSheet();
		@SuppressWarnings("unchecked")
		final Map<String, Object> obj = (Map<String, Object>) model;
		try {
			String labels[] = (String[]) obj.get("labels");
			int columnWidth[] = (int[]) obj.get("columnWidth");// 셀크기 가로
			// 시트명
			String sheetName = (String) obj.get("sheetName");
			String fileName = (String) obj.get("filename");
			
			wb.setSheetName(0, sheetName);
			createColumnLabel(sheet, wb, labels, columnWidth);
			
			postgreSession.select("selectExcelCardpointChangeList", map, new ResultHandler() {

				@Override
				public void handleResult(ResultContext context) {
					CardPointSwapList vo = (CardPointSwapList) context.getResultObject();
					Row row = sheet.createRow(context.getResultCount());
					Cell cell = null;
					cell = row.createCell(0);
					cell.setCellValue(vo.getTrans_req_date());
					cell = row.createCell(1);
					cell.setCellValue(vo.getTrans_res_date());
					cell = row.createCell(2);
					cell.setCellValue(vo.getReg_source());
					cell = row.createCell(3);
					cell.setCellValue(vo.getTrans_id());
					cell = row.createCell(4);
					cell.setCellValue(vo.getRef_trans_id());
					cell = row.createCell(5);
					cell.setCellValue(vo.getReq_point());
					cell = row.createCell(6);
					cell.setCellValue(vo.getRes_point());
					cell = row.createCell(7);
					cell.setCellValue(vo.getResult_code());
					cell = row.createCell(8);
					cell.setCellValue(vo.getRes_code());
					cell = row.createCell(9);
					cell.setCellValue(vo.getTrans_div());
					cell = row.createCell(10);
					cell.setCellValue(vo.getPrice_point());
				}
			});
			
			// 헤더설정
			String userAgent = req.getHeader("User-Agent");
			if (userAgent.indexOf("MSIE") > -1) {
				fileName = URLEncoder.encode(fileName, "utf-8");
			} else {
				fileName = new String(fileName.getBytes("utf-8"), "iso-8859-1");
			}
			res.setHeader("Set-Cookie", "fileDownload=true; path=/");
			res.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
			res.setHeader("Content-Transfer-Encoding", "binary");
			
			wb.write(res.getOutputStream());

		} catch (Exception e) {
			logger.debug("Excel Download Exception=========================");
			logger.error(e.getMessage(), e);
			res.setHeader("Set-Cookie", "fileDownload=false; path=/");
			res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
			res.setHeader("Content-Type", "text/html; charset=utf-8");
			OutputStream out = null;
			try {
				out = res.getOutputStream();
				byte[] data = new String("fail..").getBytes();
				out.write(data, 0, data.length);
			} catch (Exception ignore) {
				ignore.printStackTrace();
			} finally {
				if (out != null)
					try {
						out.close();
					} catch (Exception ignore) {
					}
			}

		} finally {
			// 디스크 적었던 임시파일을 제거합니다.
			wb.dispose();
			try {
				wb.close();
			} catch (Exception ignore) {
			}
		}
	}

	//라벨(제목)생성
	@SuppressWarnings("deprecation")
	private void createColumnLabel(Sheet sheet, SXSSFWorkbook workbook, String[] list, int[] columnWidth) 
	{
		// 속성
		XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();
        
		Font font = workbook.createFont();
        font.setFontName("ARIAL");
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setFontHeightInPoints((short) 8);
        font.setColor(IndexedColors.WHITE.getIndex());
        
        cellStyle.setFont(font);
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyle.setFillForegroundColor(new XSSFColor(Color.decode("#2080D0")));
        cellStyle.setFillPattern((short) CellStyle.SOLID_FOREGROUND);        
        cellStyle.setBorderTop(CellStyle.BORDER_THIN);
        cellStyle.setTopBorderColor(IndexedColors.WHITE.getIndex());        
        cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyle.setLeftBorderColor(IndexedColors.WHITE.getIndex());        
        cellStyle.setBorderRight(CellStyle.BORDER_THIN);
        cellStyle.setRightBorderColor(IndexedColors.WHITE.getIndex());        
        cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
        
        // 라벨
		Row firstRow = sheet.createRow(0);
		
		short width = 265;
		 
		for (int i = 0; i < list.length; i++) {
			Cell cell = firstRow.createCell(i);
			if (columnWidth.length > 0) {
				sheet.setColumnWidth(i, (columnWidth[i] * width));
			}
			cell.setCellValue(list[i]);
			cell.setCellStyle(cellStyle);
		}		
	}
	
	@Override
	public String selectCardpointChangeCancelvalue(ModelMap map) throws Exception {
		return clip_3_0_Dao.selectCardpointChangeCancelvalue(map);
	}

	@Override
	public String selectCardpointTransId(ModelMap map) throws Exception {
		return clip_3_0_Dao.selectCardpointTransId(map);
	}

	@Override
	public String selectCardpointTotalPoint(ModelMap map) throws Exception {
		return clip_3_0_Dao.selectCardpointTotalPoint(map);
	}

}
