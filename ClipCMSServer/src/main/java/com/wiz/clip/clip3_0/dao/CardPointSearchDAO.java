package com.wiz.clip.clip3_0.dao;

public class CardPointSearchDAO {
	
	private String status;
	private String keywd;
	private String datepicker1;
	private String datepicker2;     
	private String card_com;
	private String use_status;
	private String value;
	
	public CardPointSearchDAO() {
		// TODO Auto-generated constructor stub
	}
	
	public CardPointSearchDAO(String status, String keywd, String datepicker1, String datepicker2, String card_com, String use_status, String value) {
		this.status = status;
		this.keywd = keywd;
		this.datepicker1 = datepicker1;
		this.datepicker2 = datepicker2;
		this.card_com = card_com;
		this.use_status = use_status;
		this.value = value;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getKeywd() {
		return keywd;
	}
	public void setKeywd(String keywd) {
		this.keywd = keywd;
	}
	public String getDatepicker1() {
		return datepicker1;
	}
	public void setDatepicker1(String datepicker1) {
		this.datepicker1 = datepicker1;
	}
	public String getDatepicker2() {
		return datepicker2;
	}
	public void setDatepicker2(String datepicker2) {
		this.datepicker2 = datepicker2;
	}
	public String getCard_com() {
		return card_com;
	}
	public void setCard_com(String card_com) {
		this.card_com = card_com;
	}
	public String getUse_status() {
		return use_status;
	}
	public void setUse_status(String use_status) {
		this.use_status = use_status;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}