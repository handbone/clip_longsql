package com.wiz.clip.clip3_0.bean;

public class CancelPointSwapInfo {
	private String result;
	private String resultMsg;
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
}