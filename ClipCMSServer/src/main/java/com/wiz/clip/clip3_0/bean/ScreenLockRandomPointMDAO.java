package com.wiz.clip.clip3_0.bean;

public class ScreenLockRandomPointMDAO {
	private int randompoint_totalvalue;
	private int offerwall_addpointrate;
	public int getRandompoint_totalvalue() {
		return randompoint_totalvalue;
	}
	public void setRandompoint_totalvalue(int randompoint_totalvalue) {
		this.randompoint_totalvalue = randompoint_totalvalue;
	}
	public int getOfferwall_addpointrate() {
		return offerwall_addpointrate;
	}
	public void setOfferwall_addpointrate(int offerwall_addpointrate) {
		this.offerwall_addpointrate = offerwall_addpointrate;
	}
}
