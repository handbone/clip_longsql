package com.wiz.clip.clip3_0.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkPriceReportItem {
	
	//광고주명
	private String merchant_name;
	//결제 날짜
	private String yyyymmdd;
	//결제 시간
	private String hhmiss;
	//결제 금액
	private String sales;
	//적립예정 포인트
	private String commission;
	//상품명 
	private String product_name;
	//주문코드 
	private String order_code;
	//고유값 
	private String trlog_id;
	
	//주문수량
	private String itemCount;
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	public String getItemCount() {
		return itemCount;
	}
	public void setItemCount(String itemCount) {
		this.itemCount = itemCount;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getOrder_code() {
		return order_code;
	}
	public void setOrder_code(String order_code) {
		this.order_code = order_code;
	}
	public String getTrlog_id() {
		return trlog_id;
	}
	public void setTrlog_id(String trlog_id) {
		this.trlog_id = trlog_id;
	}
	public String getMerchant_name() {
		return merchant_name;
	}
	public void setMerchant_name(String merchant_name) {
		this.merchant_name = merchant_name;
	}
	public String getYyyymmdd() {
		return yyyymmdd;
	}
	public void setYyyymmdd(String yyyymmdd) {
		this.yyyymmdd = yyyymmdd;
	}
	public String getHhmiss() {
		return hhmiss;
	}
	public void setHhmiss(String hhmiss) {
		this.hhmiss = hhmiss;
	}
	public String getSales() {
		return sales;
	}
	public void setSales(String sales) {
		this.sales = sales;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}

	
}
