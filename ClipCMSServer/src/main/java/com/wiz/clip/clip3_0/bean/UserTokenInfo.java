package com.wiz.clip.clip3_0.bean;

/**
 * user_token_info 테이블
 * @author jyi
 */
public class UserTokenInfo {

	private String userToken;
	private String userCi;
	private String regdate;
	private String regTime;
	
	//user_info_tbl
	private String custId;
	private String ctn;
	private String gaId;
	private String status;
	private String exitCi;
	private String exitDate;
	private String exitTime;
	
	@Override
	public String toString() {
		return "UserTokenInfo[ userToken = " + userToken + " | userCi = " + userCi + " | regdate = " + regdate + " | regTime = " + regTime + " | "
				+ "custId = " + custId + " | ctn = " + ctn + " | gaId = " + gaId + " | status = " + status + " | exitCi = " + exitCi + " | "
				+ "exitDate = " + exitDate + " | exitTime = " + exitTime + " ]";
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getGaId() {
		return gaId;
	}
	public void setGaId(String gaId) {
		this.gaId = gaId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getExitCi() {
		return exitCi;
	}
	public void setExitCi(String exitCi) {
		this.exitCi = exitCi;
	}
	public String getExitDate() {
		return exitDate;
	}
	public void setExitDate(String exitDate) {
		this.exitDate = exitDate;
	}
	public String getExitTime() {
		return exitTime;
	}
	public void setExitTime(String exitTime) {
		this.exitTime = exitTime;
	}
	public String getUserToken() {
		return userToken;
	}
	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}
	public String getUserCi() {
		return userCi;
	}
	public void setUserCi(String userCi) {
		this.userCi = userCi;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	
	
}
