package com.wiz.clip.clip3_0.bean;

public class CardPointSwapExcel {
	
	private String trans_div;//	거래 구분
	private String trans_req_date;//	거래 요청 일자
	private String trans_res_date;//	거래 응답 일자
	private String reg_source;//	등록 매체코드
	private String user_ci;//	사용자 CI
	private String trans_id;//	거래 고유 번호
	private String ref_trans;//_id	거래 고유 번호
	private String res_point;//	거래 요청 포인트
	private String req_point;//	거래 응답 포인트
	private String res_code;//	거래 응답 코드
	private String result_code;//	거래 결과 코드
	private String approve_no;//	승인번호
	private String approve_date;//	승인일자
	private String approve_time;//	승인시간
	private String regdate;//	등록일자
	
	public String getTrans_div() {
		return trans_div;
	}
	public void setTrans_div(String trans_div) {
		this.trans_div = trans_div;
	}
	public String getTrans_req_date() {
		return trans_req_date;
	}
	public void setTrans_req_date(String trans_req_date) {
		this.trans_req_date = trans_req_date;
	}
	public String getTrans_res_date() {
		return trans_res_date;
	}
	public void setTrans_res_date(String trans_res_date) {
		this.trans_res_date = trans_res_date;
	}
	public String getReg_source() {
		return reg_source;
	}
	public void setReg_source(String reg_source) {
		this.reg_source = reg_source;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getTrans_id() {
		return trans_id;
	}
	public void setTrans_id(String trans_id) {
		this.trans_id = trans_id;
	}
	public String getRef_trans() {
		return ref_trans;
	}
	public void setRef_trans(String ref_trans) {
		this.ref_trans = ref_trans;
	}
	public String getRes_point() {
		return res_point;
	}
	public void setRes_point(String res_point) {
		this.res_point = res_point;
	}
	public String getReq_point() {
		return req_point;
	}
	public void setReq_point(String req_point) {
		this.req_point = req_point;
	}
	public String getRes_code() {
		return res_code;
	}
	public void setRes_code(String res_code) {
		this.res_code = res_code;
	}
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public String getApprove_no() {
		return approve_no;
	}
	public void setApprove_no(String approve_no) {
		this.approve_no = approve_no;
	}
	public String getApprove_date() {
		return approve_date;
	}
	public void setApprove_date(String approve_date) {
		this.approve_date = approve_date;
	}
	public String getApprove_time() {
		return approve_time;
	}
	public void setApprove_time(String approve_time) {
		this.approve_time = approve_time;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	
	

}
