/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.onm.bean;

public class SMSSend {

	// SMS 발송기록 

	  private String sms_idx;
	  private String send_userid;
	  private String username;
	  private String msg_content;
	  private String call_ctn;
	  private String rcv_ctn;
	  private String success_yn;
	  private String response;
	  private String ipaddr;
	  private String regdate;
	  
	  
	  
	  
	  
	  
	  
	  
	public String getSms_idx() {
		return sms_idx;
	}
	public void setSms_idx(String sms_idx) {
		this.sms_idx = sms_idx;
	}
	public String getSend_userid() {
		return send_userid;
	}
	public void setSend_userid(String send_userid) {
		this.send_userid = send_userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMsg_content() {
		return msg_content;
	}
	public void setMsg_content(String msg_content) {
		this.msg_content = msg_content;
	}
	public String getCall_ctn() {
		return call_ctn;
	}
	public void setCall_ctn(String call_ctn) {
		this.call_ctn = call_ctn;
	}
	public String getRcv_ctn() {
		return rcv_ctn;
	}
	public void setRcv_ctn(String rcv_ctn) {
		this.rcv_ctn = rcv_ctn;
	}
	public String getSuccess_yn() {
		return success_yn;
	}
	public void setSuccess_yn(String success_yn) {
		this.success_yn = success_yn;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	
}
