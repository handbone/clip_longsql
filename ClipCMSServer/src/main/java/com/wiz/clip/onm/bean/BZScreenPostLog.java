/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.onm.bean;

public class BZScreenPostLog {
	
	// BZSCREEN 
	
	  private String transaction_id;
	  private String user_id;
	  private String campaign_id;
	  private String campaign_name;
	  private String event_at;
	  private String is_media;
	  private String extra;
	  private String action_type;
	  private String point;
	  private String base_point;
	  private String data;
	  private String reg_date;
	  private String reg_time ;
	  
	  
	  
	  
	  
	  
	  
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getCampaign_id() {
		return campaign_id;
	}
	public void setCampaign_id(String campaign_id) {
		this.campaign_id = campaign_id;
	}
	public String getCampaign_name() {
		return campaign_name;
	}
	public void setCampaign_name(String campaign_name) {
		this.campaign_name = campaign_name;
	}
	public String getEvent_at() {
		return event_at;
	}
	public void setEvent_at(String event_at) {
		this.event_at = event_at;
	}
	public String getIs_media() {
		return is_media;
	}
	public void setIs_media(String is_media) {
		this.is_media = is_media;
	}
	public String getExtra() {
		return extra;
	}
	public void setExtra(String extra) {
		this.extra = extra;
	}
	public String getAction_type() {
		return action_type;
	}
	public void setAction_type(String action_type) {
		this.action_type = action_type;
	}
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public String getBase_point() {
		return base_point;
	}
	public void setBase_point(String base_point) {
		this.base_point = base_point;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getReg_time() {
		return reg_time;
	}
	public void setReg_time(String reg_time) {
		this.reg_time = reg_time;
	}
	
	

}
