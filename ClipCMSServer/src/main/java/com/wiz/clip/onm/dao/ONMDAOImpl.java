/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.onm.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wiz.clip.onm.bean.BZADPostLog;
import com.wiz.clip.onm.bean.BZScreenPostLog;
import com.wiz.clip.onm.bean.SMSSend;
import com.wiz.clip.onm.bean.SearchBean;
import com.wiz.clip.onm.bean.StatConnect;


@Repository
public class ONMDAOImpl implements ONMDAO { 

	@Autowired
	private SqlSession postgreSession;
	
	Logger logger = Logger.getLogger(ONMDAOImpl.class.getName());
	
	// bzadPostLog
	@Override
	public List<BZADPostLog> bzadPostLog(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return postgreSession.selectList("ONM.bzadPostLog", searchBean);
	}
	@Override
	public int bzadPostLogCnt(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return postgreSession.selectOne("ONM.bzadPostLogCnt", searchBean);
	}

	

	// bzScreenPostLog
	@Override
	public List<BZScreenPostLog> bzScreenPostLog(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return postgreSession.selectList("ONM.bzScreenPostLog", searchBean);
	}
	
	@Override
	public int bzScreenPostLogCnt(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return postgreSession.selectOne("ONM.bzScreenPostLogCnt", searchBean);
	}

	
	
	// smsSend
	@Override
	public List<SMSSend> smsSend(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return postgreSession.selectList("ONM.smsSend", searchBean);
	}
	@Override
	public int smsSendCnt(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return postgreSession.selectOne("ONM.smsSendCnt", searchBean);
	}
	
	
	
	// statConnect
	@Override
	public List<StatConnect> statConnect(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return postgreSession.selectList("ONM.statConnect", searchBean);
	}

	@Override
	public int statConnectCnt(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return postgreSession.selectOne("ONM.statConnectCnt", searchBean);
	}

}
