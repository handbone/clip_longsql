/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.onm.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.onm.bean.BZADPostLog;
import com.wiz.clip.onm.bean.BZScreenPostLog;
import com.wiz.clip.onm.bean.SMSSend;
import com.wiz.clip.onm.bean.SearchBean;
import com.wiz.clip.onm.bean.StatConnect;
import com.wiz.clip.onm.dao.ONMDAO;

@Service
@Transactional
public class ONMServiceImpl implements ONMService { 

	
	@Autowired
	private ONMDAO onmdao;

	Logger logger = Logger.getLogger(ONMServiceImpl.class.getName());

	// bzadPostLog
	@Override
	public List<BZADPostLog> bzadPostLog(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return onmdao.bzadPostLog(searchBean);
	}
	
	@Override
	public int bzadPostLogCnt(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return onmdao.bzadPostLogCnt(searchBean);
	}
	
	
	// bzScreenPostLog
	@Override
	public List<BZScreenPostLog> bzScreenPostLog(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return onmdao.bzScreenPostLog(searchBean);
	}
	
	@Override
	public int bzScreenPostLogCnt(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return onmdao.bzScreenPostLogCnt(searchBean);
	}

	
	// smsSend
	@Override
	public List<SMSSend> smsSend(SearchBean searchBean) {
		// TODO Auto-generated method stub
		
		// 제어문을 통해 암호화
		try {
			if("send_userid".equals(searchBean.getStatus())){
				searchBean.setSend_userid(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("username".equals(searchBean.getStatus())){
				searchBean.setUsername(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("call_ctn".equals(searchBean.getStatus())){
				searchBean.setCall_ctn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("rcv_ctn".equals(searchBean.getStatus())){
				searchBean.setRcv_ctn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
			if("".equals(searchBean.getStatus())){
				searchBean.setSend_userid(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setUsername(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setCall_ctn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setRcv_ctn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return onmdao.smsSend(searchBean);
	}

	@Override
	public int smsSendCnt(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return onmdao.smsSendCnt(searchBean);
	}

	
	// statConnect
	@Override
	public List<StatConnect> statConnect(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return onmdao.statConnect(searchBean);
	}

	@Override
	public int statConnectCnt(SearchBean searchBean) {
		// TODO Auto-generated method stub
		return onmdao.statConnectCnt(searchBean);
	}

}
