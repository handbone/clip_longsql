/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/


package com.captcha;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
 
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
 
final class CaptchaServletUtil {
     
    public static void writeImage(HttpServletResponse response, BufferedImage bi) {
         
        response.setHeader("Cache-Control", "private,no-cache,no-store");
        response.setContentType("image/png");
         
        try {
            writeImage(response.getOutputStream(), bi);
        } catch (IOException e) {
            e.printStackTrace();
        }
         
    }
 
    public static void writeImage(OutputStream os, BufferedImage bi) {
         
        try {
            ImageIO.write(bi, "png", os);
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
         
    }
     
}