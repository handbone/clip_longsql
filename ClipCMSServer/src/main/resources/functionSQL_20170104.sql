-- Function: fn_make_anomaly_stat(character, character, numeric, character)

-- DROP FUNCTION fn_make_anomaly_stat(character, character, numeric, character);

CREATE OR REPLACE FUNCTION fn_make_anomaly_stat(
    curdate character,
    curtime character,
    limitpoint numeric,
    modifyaclcode character)
  RETURNS boolean AS
$BODY$
	DECLARE _limit_input_count integer;
	DECLARE _limit_output_count integer;
	DECLARE _input_point numeric;
	DECLARE _output_point numeric;
	DECLARE _reg_sources text[];
	DECLARE _i integer;

	BEGIN

		truncate table tbl_hourly_stat;

		insert into tbl_hourly_stat (p_media, p_user_ci, p_point_type, p_point_value)
		SELECT
			reg_source as p_media,
			user_ci as p_user_ci,
			point_type as p_point_type,
			SUM(point_value) as p_point_value
		FROM
			"POINT_INFO_TBL"
		WHERE
			reg_date = curDate and
			SUBSTRING(reg_time, 1, 2) = curTime
			and reg_source != modifyAclCode
		GROUP BY
			reg_source,
			user_ci,
			point_type;

		_reg_sources := array(SELECT requester_code FROM "ACL_INFO_TBL" WHERE use_status = 'Y' and requester_code != modifyAclCode);

		FOR _i IN 1 .. array_upper(_reg_sources, 1) LOOP
			INSERT INTO tbl_stat_anomaly_detection (media, stat_date, stat_hour, limit_point_value, limit_input_count, limit_output_count, input_point, output_point) VALUES (_reg_sources[_i], curDate, curTime, limitPoint, 0, 0, 0, 0);
			
			SELECT
				COALESCE(COUNT(p_user_ci),0) into _limit_input_count
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'I' and
				p_point_value >= limitPoint;

			SELECT
				COALESCE(COUNT(p_user_ci),0) into _limit_output_count
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'O' and
				p_point_value >= limitPoint;

			SELECT
				COALESCE(SUM(p_point_value),0) into _input_point
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'I';
				
			SELECT
				COALESCE(SUM(p_point_value),0) into _output_point
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'O';
				
			UPDATE
				tbl_stat_anomaly_detection
			SET
				limit_point_value = limitPoint,
				limit_input_count = _limit_input_count,
				limit_output_count = _limit_output_count,
				input_point = _input_point,
				output_point = _output_point
			WHERE
				media = _reg_sources[_i] and
				stat_date = curDate and
				stat_hour = curTime;
				
		END LOOP;
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_anomaly_stat(character, character, numeric, character)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  -- Function: fn_make_anomaly_stat2(character, character, numeric, character)

-- DROP FUNCTION fn_make_anomaly_stat2(character, character, numeric, character);

CREATE OR REPLACE FUNCTION fn_make_anomaly_stat2(
    curdate character,
    curtime character,
    limitpoint numeric,
    modifyaclcode character)
  RETURNS boolean AS
$BODY$
	BEGIN
		-- 利赋贸府
		INSERT INTO tbl_stat_anomaly_detection_2 ( media , stat_date, stat_hour, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT	
			'unUsed',
			curdate,
			curtime,
			'I' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci,
					SUM(point_value) as point
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curdate and
					SUBSTRING(reg_time, 1, 2) = curtime and
					point_type = 'I' 
					and reg_source != modifyAclCode
				GROUP BY
					user_ci,
					point_type
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci
			AND point >= limitpoint;

		-- 瞒皑贸府
		INSERT INTO tbl_stat_anomaly_detection_2 ( media, stat_date, stat_hour, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			'unUsed',
			curdate,
			curtime,
			'O' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci,
					SUM(point_value) as point
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curdate and
					SUBSTRING(reg_time, 1, 2) = curtime and
					point_type = 'O' 
					and reg_source != modifyAclCode
				GROUP BY
					user_ci,
					point_type
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci
			AND point >= limitpoint;


		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_anomaly_stat2(character, character, numeric, character)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: fn_make_top_stat(character, integer, character)

-- DROP FUNCTION fn_make_top_stat(character, integer, character);

CREATE OR REPLACE FUNCTION fn_make_top_stat(
    curdate character,
    limitcount integer,
    modifyaclcode character)
  RETURNS boolean AS
$BODY$
	BEGIN
		-- 利赋贸府
		INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			'unUsed' as media,
			curDate, 
			'I' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci, 
					SUM(point_value) as point 
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curDate and 
					point_type = 'I' 
					and reg_source != modifyAclCode
				GROUP BY user_ci 
				ORDER BY point DESC
				LIMIT limitCount
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci;

		-- 瞒皑贸府
		INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			'unUsed' as media,
			curDate, 
			'O' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci, 
					SUM(point_value) as point 
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curDate and 
					point_type = 'O' 
					and reg_source != modifyAclCode
				GROUP BY user_ci 
				ORDER BY point DESC
				LIMIT limitCount
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_top_stat(character, integer, character)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  -- Function: fn_make_top_stat2(character, integer, character)

-- DROP FUNCTION fn_make_top_stat2(character, integer, character);

CREATE OR REPLACE FUNCTION fn_make_top_stat2(
    curdate character,
    limitcount integer,
    modifyaclcode character)
  RETURNS boolean AS
$BODY$
	DECLARE _reg_sources text[];
	BEGIN

	
		_reg_sources := array(SELECT requester_code FROM "ACL_INFO_TBL" WHERE use_status = 'Y' and requester_code != modifyAclCode);


		FOR _i IN 1 .. array_upper(_reg_sources, 1) LOOP

			
			-- 利赋贸府
			INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
			SELECT
				(select requester_name from "ACL_INFO_TBL" where requester_code = a.reg_source ) as media,
				curDate, 
				'I' as point_type, 
				(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
				a.point,
				ui.user_ci, 
				ui.cust_id, 
				ui.ctn, 
				ui.ga_id 
			FROM
				( 
					SELECT
						reg_source, 
						user_ci, 
						SUM(point_value) as point 
					FROM
						"POINT_INFO_TBL"
					WHERE
						reg_source = _reg_sources[_i] and
						reg_date = curDate and 
						point_type = 'I' 
					GROUP BY reg_source, user_ci 
					ORDER BY point DESC
					LIMIT limitCount
					
				) as a,
				"USER_INFO_TBL" as ui
			WHERE
				a.user_ci = ui.user_ci;

			-- 瞒皑贸府
			INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
			SELECT
				(select requester_name from "ACL_INFO_TBL" where requester_code = a.reg_source ) as media,
				curDate, 
				'O' as point_type, 
				(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
				a.point,
				ui.user_ci, 
				ui.cust_id, 
				ui.ctn, 
				ui.ga_id 
			FROM
				(
					SELECT
						reg_source, 
						user_ci, 
						SUM(point_value) as point 
					FROM
						"POINT_INFO_TBL"
					WHERE
						reg_source = _reg_sources[_i] and
						reg_date = curDate and 
						point_type = 'O' 
					GROUP BY reg_source, user_ci 
					ORDER BY point DESC
					LIMIT limitCount
					
				) as a,
				"USER_INFO_TBL" as ui
			WHERE
				a.user_ci = ui.user_ci;

		END LOOP;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_top_stat2(character, integer, character)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_balance(text)

-- DROP FUNCTION make_daily_balance(text);

CREATE OR REPLACE FUNCTION make_daily_balance(IN curdate text)
  RETURNS TABLE(a_user bigint, t_balance numeric) AS
$BODY$
	BEGIN
	RETURN QUERY

		SELECT COUNT(point_info_idx), SUM(balance) FROM "POINT_INFO_TBL" 
		WHERE reg_date <= curDate
			AND point_info_idx IN (
						select MAX(point_info_idx) AS point_info_idx
						from "POINT_INFO_TBL" 
						WHERE reg_date <= curDate
						GROUP BY user_ci
						); 
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_balance(text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_balance2(text)

-- DROP FUNCTION make_daily_balance2(text);

CREATE OR REPLACE FUNCTION make_daily_balance2(IN curdate text)
  RETURNS TABLE(a_user bigint, t_balance numeric) AS
$BODY$
	BEGIN
	RETURN QUERY

		SELECT COUNT(point_info_idx), SUM(balance) FROM "POINT_INFO_TBL" 
		WHERE reg_date <= curDate
			AND point_info_idx IN (
						select MAX(point_info_idx) AS point_info_idx
						from "POINT_INFO_TBL" 
						WHERE reg_date <= curDate
						GROUP BY user_ci
						); 
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_balance2(text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_input(text)

-- DROP FUNCTION make_daily_input(text);

CREATE OR REPLACE FUNCTION make_daily_input(IN curdate text)
  RETURNS TABLE(i_count bigint, i_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value) 	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and point_type = 'I';
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_input(text)
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_input2(text, text)

-- DROP FUNCTION make_daily_input2(text, text);

CREATE OR REPLACE FUNCTION make_daily_input2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(i_count bigint, i_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value) 	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and point_type = 'I'
		and reg_source != pointModifyCode;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_input2(text, text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_input_unique_ci2(text, text)

-- DROP FUNCTION make_daily_input_unique_ci2(text, text);

CREATE OR REPLACE FUNCTION make_daily_input_unique_ci2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(i_unique_ci bigint) AS
$BODY$
	BEGIN
	RETURN QUERY

				select COUNT(user_ci) from (
				select user_ci
				from 	"POINT_INFO_TBL"
				where 	reg_date = curDate
				and point_type = 'I'
				and reg_source != pointModifyCode
				GROUP BY user_ci
				) A;
END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_input_unique_ci2(text, text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_io_sum2(text, text)

-- DROP FUNCTION make_daily_io_sum2(text, text);

CREATE OR REPLACE FUNCTION make_daily_io_sum2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(io_sum_count bigint) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*)	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and reg_source != pointModifyCode;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_io_sum2(text, text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_io_sum_unique_ci2(text, text)

-- DROP FUNCTION make_daily_io_sum_unique_ci2(text, text);

CREATE OR REPLACE FUNCTION make_daily_io_sum_unique_ci2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(io_sum_unique_ci bigint) AS
$BODY$
	BEGIN
	RETURN QUERY

				select COUNT(user_ci) from (
				select user_ci
				from 	"POINT_INFO_TBL"
				where 	reg_date = curDate
				and reg_source != pointModifyCode
				GROUP BY user_ci
				) A;
END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_io_sum_unique_ci2(text, text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_modify2(text, text)

-- DROP FUNCTION make_daily_modify2(text, text);

CREATE OR REPLACE FUNCTION make_daily_modify2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(m_count bigint, m_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select COUNT(*), sum(CASE WHEN point_type = 'I' THEN point_value WHEN point_type = 'O' THEN -point_value END ) 	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and reg_source = pointModifyCode;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_modify2(text, text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_modify_unique_ci2(text, text)

-- DROP FUNCTION make_daily_modify_unique_ci2(text, text);

CREATE OR REPLACE FUNCTION make_daily_modify_unique_ci2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(m_unique_ci bigint) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(user_ci) from (
			select user_ci	
			from 	"POINT_INFO_TBL"
			where 	reg_date = curDate
			and reg_source = pointModifyCode
			group by user_ci
		) A;
			END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_modify_unique_ci2(text, text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_output(text)

-- DROP FUNCTION make_daily_output(text);

CREATE OR REPLACE FUNCTION make_daily_output(IN curdate text)
  RETURNS TABLE(o_count bigint, o_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value) 	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and point_type = 'O';
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_output(text)
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_output2(text, text)

-- DROP FUNCTION make_daily_output2(text, text);

CREATE OR REPLACE FUNCTION make_daily_output2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(o_count bigint, o_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value) 	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and point_type = 'O'
		and reg_source != pointModifyCode;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_output2(text, text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_output_unique_ci2(text, text)

-- DROP FUNCTION make_daily_output_unique_ci2(text, text);

CREATE OR REPLACE FUNCTION make_daily_output_unique_ci2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(o_unique_ci bigint) AS
$BODY$
	BEGIN
	RETURN QUERY

				select COUNT(user_ci) from (
				select user_ci
				from 	"POINT_INFO_TBL"
				where 	reg_date = curDate
				and point_type = 'O'
				and reg_source != pointModifyCode
				GROUP BY user_ci
				) A;
END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_output_unique_ci2(text, text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_pre(text)

-- DROP FUNCTION make_daily_pre(text);

CREATE OR REPLACE FUNCTION make_daily_pre(IN curdate text)
  RETURNS TABLE(p_count bigint, p_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value)
   		from "POINT_HIST_TBL"
    		where status = 'W'
			and reg_date = curDate ;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_pre(text)
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_pre2(text)

-- DROP FUNCTION make_daily_pre2(text);

CREATE OR REPLACE FUNCTION make_daily_pre2(IN curdate text)
  RETURNS TABLE(p_count bigint, p_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value)
   		from "POINT_HIST_TBL"
    		where status = 'W'
			and reg_date = curDate ;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_pre2(text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  -- Function: make_daily_remove(text)

-- DROP FUNCTION make_daily_remove(text);

CREATE OR REPLACE FUNCTION make_daily_remove(IN curdate text)
  RETURNS TABLE(r_user bigint, r_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		Select count(*),sum(balance) 
  		 from "POINT_INFO_TBL" 
   		where point_hist_idx in (
			select max(p.point_hist_idx) 
			from 	"POINT_INFO_TBL" p,
				"USER_INFO_TBL" u
			where
				p.user_ci = u.exit_ci 
				and u.status = 'D'
				and u.exit_date = curDate
			group by p.user_ci);
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_remove(text)
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_remove2(text)

-- DROP FUNCTION make_daily_remove2(text);

CREATE OR REPLACE FUNCTION make_daily_remove2(IN curdate text)
  RETURNS TABLE(r_user bigint, r_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		Select count(*),sum(balance) 
  		 from "POINT_INFO_TBL" 
   		where point_hist_idx in (
			select max(p.point_hist_idx) 
			from 	"POINT_INFO_TBL" p,
				"USER_INFO_TBL" u
			where
				p.user_ci = u.exit_ci 
				and u.status = 'D'
				and u.exit_date = curDate
			group by p.user_ci);
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_remove2(text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_remove_unique_ci2(text)

-- DROP FUNCTION make_daily_remove_unique_ci2(text);

CREATE OR REPLACE FUNCTION make_daily_remove_unique_ci2(IN curdate text)
  RETURNS TABLE(r_unique_ci bigint) AS
$BODY$
	BEGIN
	RETURN QUERY
		
		select COUNT(user_ci)
		from (
			select user_ci
			from 	"USER_INFO_TBL"
			where status = 'D'
			and exit_date = curDate
			group by user_ci
			) A;
		
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_remove_unique_ci2(text)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_daily_stat(text)

-- DROP FUNCTION make_daily_stat(text);

CREATE OR REPLACE FUNCTION make_daily_stat(predate text)
  RETURNS boolean AS
$BODY$
	DECLARE
		_balance record;
		_remove record;
		_input record;
		_output record;
		_pre record;
		today date;
	BEGIN
		select to_date(predate,'YYYYMMDD') into today;
		select a_user,t_balance into _balance from make_daily_balance(predate) ;
		--RAISE NOTICE 'result = %',_balance.a_user;
		--RAISE NOTICE 'result = %',_balance.t_balance;
		select r_user,r_point into _remove from make_daily_remove(predate);
		select i_count,i_point into _input from make_daily_input(predate);
		select o_count,o_point into _output from make_daily_output(predate);
		select p_count,p_point into _pre from make_daily_pre(predate);


		insert into tbl_stat_daily_point(year,month,day,d_day,total_balance,input_point,output_point,remove_point,pre_point,input_count,output_count,remove_count,pre_count,active_user)
		values (substr(predate,1,4),substr(predate,5,2),substr(predate,7,2),date_part('dow', today),_balance.t_balance,_input.i_point,_output.o_point,_remove.r_point,_pre.p_point,_input.i_count,_output.o_count,_remove.r_user,_pre.p_count,_balance.a_user);
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION make_daily_stat(text)
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  -- Function: make_daily_stat2(text, text)

-- DROP FUNCTION make_daily_stat2(text, text);

CREATE OR REPLACE FUNCTION make_daily_stat2(
    predate text,
    pointmodifycode text)
  RETURNS boolean AS
$BODY$
	DECLARE
		_balance2 record;
		_input2 record;
		_input_unique_ci2 record;
		_io_sum2 record;
		_io_sum_unique_ci2 record;
		_modify2 record;
		_modify_unique_ci2 record;
		_output2 record;
		_output_unique_ci2 record;
		_pre2 record;
		_remove2 record;
		_remove_unique_ci2 record;
		today date;
	BEGIN
		select to_date(predate,'YYYYMMDD') into today;
		--select a_user,t_balance into _balance2 from make_daily_balance2(predate) ;
		--RAISE NOTICE 'result = %',_balance.a_user;
		--RAISE NOTICE 'result = %',_balance.t_balance;
		select i_count,i_point 	into _input2 		from make_daily_input2(predate, pointmodifycode) ;
		select i_unique_ci 	into _input_unique_ci2 	from make_daily_input_unique_ci2(predate, pointmodifycode) ;
		select io_sum_count 	into _io_sum2 		from make_daily_io_sum2(predate, pointmodifycode) ;
		select io_sum_unique_ci into _io_sum_unique_ci2 from make_daily_io_sum_unique_ci2(predate, pointmodifycode) ;
		select m_count,m_point	into _modify2 		from make_daily_modify2(predate, pointmodifycode) ;
		select m_unique_ci  	into _modify_unique_ci2	from make_daily_modify_unique_ci2(predate, pointmodifycode) ;
		select o_count,o_point 	into _output2 		from make_daily_output2(predate, pointmodifycode) ;
		select o_unique_ci	into _output_unique_ci2	from make_daily_output_unique_ci2(predate, pointmodifycode) ;
		select p_count,p_point	into _pre2		from make_daily_pre2(predate) ;
		select r_user,r_point	into _remove2		from make_daily_remove2(predate) ;
		select r_unique_ci	into _remove_unique_ci2	from make_daily_remove_unique_ci2(predate) ;




		insert into tbl_stat_daily_point2(
			year
			,month
			,day
			,d_day

			,total_balance
			,active_user

			,input_unique_ci
			,input_point
			,input_count

			,output_unique_ci
			,output_point
			,output_count

			,remove_unique_ci
			,remove_point
			,remove_count
			
			,pre_point
			,pre_count

			,modify_unique_ci
			,modify_point
			,modify_count
			
			,io_sum_count
			,io_sum_unique_ci


			)
		values (
			substr(predate,1,4)
			,substr(predate,5,2)
			,substr(predate,7,2)
			,date_part('dow', today)

			,0
			,0

			,_input_unique_ci2.i_unique_ci
			,_input2.i_point
			,_input2.i_count

			,_output_unique_ci2.o_unique_ci
			,_output2.o_point
			,_output2.o_count

			,_remove_unique_ci2.r_unique_ci
			,_remove2.r_point
			,_remove2.r_user

			,_pre2.p_point
			,_pre2.p_count
			
			,_modify_unique_ci2.m_unique_ci
			,_modify2.m_point
			,_modify2.m_count
			
			,_io_sum2.io_sum_count
			,_io_sum_unique_ci2.io_sum_unique_ci
			



			);
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_media_input(text)

-- DROP FUNCTION make_media_input(text);

CREATE OR REPLACE FUNCTION make_media_input(IN curdate text)
  RETURNS TABLE(i_media character varying, i_count bigint, i_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select reg_source,count(*),sum(point_value) 
		from 	"POINT_INFO_TBL" 	
		where 	reg_date = curDate
			and point_type = 'I'
		group by reg_source;

		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_media_input(text)
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  -- Function: make_media_output(text)

-- DROP FUNCTION make_media_output(text);

CREATE OR REPLACE FUNCTION make_media_output(IN curdate text)
  RETURNS TABLE(o_media character varying, o_count bigint, o_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select reg_source,count(*),sum(point_value) 
		from 	"POINT_INFO_TBL" 	
		where 	reg_date = curDate
			and point_type = 'O'
		group by reg_source;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_media_output(text)
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_media_pre(text)

-- DROP FUNCTION make_media_pre(text);

CREATE OR REPLACE FUNCTION make_media_pre(IN curdate text)
  RETURNS TABLE(p_media character varying, p_count bigint, p_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select reg_source,count(*),sum(point_value)
   		from "POINT_HIST_TBL"
    		where status = 'W'
			and reg_date = curDate 
		group by reg_source;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_media_pre(text)
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: make_media_stat(text)

-- DROP FUNCTION make_media_stat(text);

CREATE OR REPLACE FUNCTION make_media_stat(curdate text)
  RETURNS boolean AS
$BODY$
	DECLARE
		_input record;
		_output record;
		_pre record;
		today date;
	BEGIN
		select to_date(curDate,'YYYYMMDD') into today;
		FOR _input IN select i_media,i_count,i_point from make_media_input(curDate) LOOP
			insert into tbl_stat_media_point(year,month,day,d_day,media,point_type,point,count)
			values (substr(curDate,1,4),substr(curDate,5,2),substr(curDate,7,2),date_part('dow', today),_input.i_media,'I',_input.i_point,_input.i_count);			
		END LOOP;

		FOR _output IN select o_media,o_count,o_point from make_media_output(curDate) LOOP
			insert into tbl_stat_media_point(year,month,day,d_day,media,point_type,point,count)
			values (substr(curDate,1,4),substr(curDate,5,2),substr(curDate,7,2),date_part('dow', today),_output.o_media,'O',_output.o_point,_output.o_count);			
		END LOOP;

		FOR _pre IN select p_media,p_count,p_point from make_media_pre(curDate) LOOP
			insert into tbl_stat_media_point(year,month,day,d_day,media,point_type,point,count)
			values (substr(curDate,1,4),substr(curDate,5,2),substr(curDate,7,2),date_part('dow', today),_pre.p_media,'P',_pre.p_point,_pre.p_count);			
		END LOOP;

		
		--select i_media,i_count,i_point into _input from point.make_media_input(curDate);
		--select o_media,o_count,o_point into _output from point.make_media_output(curDate);
		--select p_media,p_count,p_point into _pre from point.make_media_pre(curDate);
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION make_media_stat(text)
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  