ALTER TABLE  "POINT_INFO_TBL" ADD  COLUMN exit_date timestamp without time zone
ALTER TABLE  "POINT_INFO_TBL" ADD  COLUMN exit_gbn character(1) 
ALTER TABLE cms_point_modify ADD  COLUMN message character varying(2056)

탈퇴한 사용자의 수와 잔액도 집계 될 수 있도록 프로시저를 변경한다.
-- Function: make_daily_remove(text)
-- DROP FUNCTION make_daily_remove(text);
CREATE OR REPLACE FUNCTION make_daily_remove(IN curdate text)
  RETURNS TABLE(r_user bigint, r_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		Select count(*),sum(balance) 
  		 from "POINT_INFO_TBL" 
   		where point_hist_idx in (
			select max(p.point_hist_idx) 
			from 	"POINT_INFO_TBL" p,
				"USER_INFO_TBL" u
			where
				p.user_ci = u.exit_ci 
				and u.status = 'D'
				and u.exit_date = curDate
			group by p.user_ci);
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_remove(text)
  OWNER TO admin;
  
  

===========================================================================================================
잔액 관련 프로시저 수정
-- Function: make_daily_balance(text)
DROP FUNCTION make_daily_balance(text);

CREATE OR REPLACE FUNCTION make_daily_balance(IN curDate text)
  RETURNS TABLE(a_user bigint, t_balance numeric) AS
$BODY$
	BEGIN
	RETURN QUERY

		SELECT COUNT(point_info_idx), SUM(balance) FROM "POINT_INFO_TBL" 
		WHERE reg_date <= curDate
			AND point_info_idx IN (
						select MAX(point_info_idx) AS point_info_idx
						from "POINT_INFO_TBL" 
						WHERE reg_date <= curDate
						GROUP BY user_ci
						); 
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  
인풋
-- Function: make_daily_input(text)
DROP FUNCTION make_daily_input(text);

CREATE OR REPLACE FUNCTION make_daily_input(IN curdate text)
  RETURNS TABLE(i_count bigint, i_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value) 	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and point_type = 'I';
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_input(text)
  OWNER TO admin;



아웃풋
-- Function: make_daily_output(text)
DROP FUNCTION make_daily_output(text);
CREATE OR REPLACE FUNCTION make_daily_output(IN curdate text)
  RETURNS TABLE(o_count bigint, o_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value) 	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and point_type = 'O';
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_output(text)
  OWNER TO admin;




메인 - 인자값 변경  
-- Function: make_daily_stat(text, text)
-- DROP FUNCTION make_daily_stat(text, text);

CREATE OR REPLACE FUNCTION make_daily_stat(
    predate text)
  RETURNS boolean AS
$BODY$
	DECLARE
		_balance record;
		_remove record;
		_input record;
		_output record;
		_pre record;
		today date;
	BEGIN
		select to_date(predate,'YYYYMMDD') into today;
		select a_user,t_balance into _balance from make_daily_balance(predate) ;
		--RAISE NOTICE 'result = %',_balance.a_user;
		--RAISE NOTICE 'result = %',_balance.t_balance;
		select r_user,r_point into _remove from make_daily_remove(predate);
		select i_count,i_point into _input from make_daily_input(predate);
		select o_count,o_point into _output from make_daily_output(predate);
		select p_count,p_point into _pre from make_daily_pre(predate);


		insert into tbl_stat_daily_point(year,month,day,d_day,total_balance,input_point,output_point,remove_point,pre_point,input_count,output_count,remove_count,pre_count,active_user)
		values (substr(predate,1,4),substr(predate,5,2),substr(predate,7,2),date_part('dow', today),_balance.t_balance,_input.i_point,_output.o_point,_remove.r_point,_pre.p_point,_input.i_count,_output.o_count,_remove.r_user,_pre.p_count,_balance.a_user);
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION make_daily_stat(text)
  OWNER TO admin;





  
  
  
  
미디어 인풋
-- Function: make_media_input(text)
DROP FUNCTION make_media_input(text);

CREATE OR REPLACE FUNCTION make_media_input(IN curdate text)
  RETURNS TABLE(i_media character varying, i_count bigint, i_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select reg_source,count(*),sum(point_value) 
		from 	"POINT_INFO_TBL" 	
		where 	reg_date = curDate
			and point_type = 'I'
		group by reg_source;

		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_media_input(text)
  OWNER TO admin;

미디어 아웃풋
-- Function: make_media_output(text)
DROP FUNCTION make_media_output(text);

CREATE OR REPLACE FUNCTION make_media_output(IN curdate text)
  RETURNS TABLE(o_media character varying, o_count bigint, o_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select reg_source,count(*),sum(point_value) 
		from 	"POINT_INFO_TBL" 	
		where 	reg_date = curDate
			and point_type = 'O'
		group by reg_source;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_media_output(text)
  OWNER TO admin;



이제 다시 구성하는 작업
일단 daily 에 있는 자료를 백업한다. 수동으로 돌리면 삭제하게 된다.
UPDATE tbl_stat_daily_point SET year = '1000' WHERE year = '2016'
11월 1일부터 현재일까지 다시 구성한다. 일별과 매체별을 3일정도만 구성한 다음에 확인하고 넘어가자.
SELECT make_daily_stat( '20161101' )

필요한경우 삭제로직
DELETE FROM tbl_stat_daily_point 
WHERE year = '2016'
AND month = '11'
AND day = '02'
  
이제 미디어쪽도 똑같은 작업을 해준다.
UPDATE tbl_stat_Media_point SET year = '1000' WHERE year = '2016'
11월 1일부터 현재일까지 다시 구성한다.
SELECT make_media_stat('20161101')

확인
SELECT * FROM tbl_stat_daily_point WHERE year = '2016' AND month = '11' AND day = '02'
SELECT * FROM tbl_stat_Media_point WHERE year = '2016' AND month = '11' AND day = '02' AND point_type = 'I'
SELECT * FROM tbl_stat_Media_point WHERE year = '2016' AND month = '11' AND day = '02' AND point_type = 'O'















--
--
--데이터가 많은곳에서 테스트를 하면 차이가 발생하고 있다.
--SELECT COUNT(point_info_idx), SUM(balance) FROM "POINT_INFO_TBL" WHERE 
--reg_date <='20161103'
--AND point_info_idx IN (
--	select MAX(point_info_idx) AS point_info_idx
--	from "POINT_INFO_TBL" 
--	WHERE reg_date <= '20161103'
--	GROUP BY user_ci
--) 
--      
--
--
--select count(*),sum(point_value) 	
--from 	"POINT_INFO_TBL"
--where 	reg_date = '20161103'
--and point_type = 'O'
--      
--
--
--01 78324470
--02 79368922 1438569 394074 43적음  
--03 81228677 2358279 498501 23적음
--04 82284290 1457506 401856 37적음
--05 83222854 1450655 511876 215적음
--06 84139788 1303071 386111 26적음


--변경하려는 쿼리의 원문 / 시간이 너무 오래 걸린다.
--SELECT SUM(B.balance) FROM (	
--	select user_ci, MAX(point_info_idx) AS point_info_idx
--	from "POINT_INFO_TBL" 
--	WHERE reg_date <='20160925'
--	GROUP BY user_ci
--) AS A LEFT OUTER JOIN "POINT_INFO_TBL" B ON A.point_info_idx = B.point_info_idx
--6607382
--SELECT SUM(B.balance) FROM (	
--	select user_ci, MAX(point_info_idx) AS point_info_idx
--	from "POINT_INFO_TBL" 
--	WHERE reg_date <='20160926'
--	GROUP BY user_ci
--) AS A LEFT OUTER JOIN "POINT_INFO_TBL" B ON A.point_info_idx = B.point_info_idx
--7199075
--select count(*),sum(point_value) 	
--from 	"POINT_INFO_TBL"
--where 	reg_date = '20160926'
--	and point_type = 'I'
--766758	
--select count(*),sum(point_value) 	
--from 	"POINT_INFO_TBL" 
--where 	reg_date = '20160926'
--	and point_type = 'O'
--175065	
	



--잔액뽑을때의 첫번째 방법 - 오래걸림
--SELECT COUNT(point_info_idx), SUM(B.balance) FROM (	
--	select user_ci, MAX(point_info_idx) AS point_info_idx
--	from "POINT_INFO_TBL" 
--	WHERE reg_date <='20160925'
--	GROUP BY user_ci
--) AS A LEFT OUTER JOIN "POINT_INFO_TBL" B ON A.point_info_idx = B.point_info_idx
--
--
--두번째 방법. 이것으로 적용한다.
--SELECT COUNT(point_info_idx), SUM(balance) FROM "POINT_INFO_TBL" WHERE point_info_idx IN (
--	select MAX(point_info_idx) AS point_info_idx
--	from "POINT_INFO_TBL" 
--	WHERE reg_date <= '20160925' 
--	GROUP BY user_ci
--) 
--AND reg_date <= '20160925'
--

  
  
  
--reg_date 가 date형이면 인덱스 걸면 빨라질 것 같은데, 컬럼하나 더 만들어서 걸려고 하니,
--이거 생성하고 > 자료 update 하는데 이간이 오래 걸릴것 같다. 그래서 멈춤
--EXPLAIN
--ALTER TABLE  "POINT_INFO_TBL" ADD  COLUMN  reg_date_timestamp timestamp without time zone
--EXPLAIN
--UPDATE "POINT_INFO_TBL" SET reg_date_timestamp = TO_DATE(reg_date,'YYYYMMDD')  WHERE <condition>;
--   
--   



=====================================================================================================================



  
  
  
  
오전 회의시간에 요청하신
구간별 적립 회원수 추출 쿼리문입니다.

==========================================================
/* 구간별 적립 회원수 추출 
사용자의 status 값은 무시함.
구간에 해당하는 사용자가 0명이면 결과값에 나타나지 않는다. */
SELECT balance, COUNT(balance) FROM (
	SELECT 
	CASE 
	WHEN balance >= 0 AND balance <= 500 THEN '0~500'
	WHEN balance > 500 AND balance <= 1000 THEN '500~1000' 
	WHEN balance > 1000 AND balance <= 1500 THEN '1000~1500'
	WHEN balance > 1500 AND balance <= 2000 THEN '1500~2000'
	WHEN balance > 2000 AND balance <= 2500 THEN '2000~2500'
	WHEN balance > 2500 AND balance <= 3000 THEN '2500~3000'
	WHEN balance > 3000 AND balance <= 3500 THEN '3000~3500'
	WHEN balance > 3500 AND balance <= 4000 THEN '3500~4000'
	WHEN balance > 4000 AND balance <= 4500 THEN '4000~4500'
	WHEN balance > 4500 AND balance <= 5000 THEN '4500~5000'
	WHEN balance > 5000 AND balance <= 5500 THEN '5000~5500'
	WHEN balance > 5500 AND balance <= 6000 THEN '5500~6000'
	WHEN balance > 6000 AND balance <= 6500 THEN '6000~6500'
	WHEN balance > 6500 AND balance <= 7000 THEN '6500~7000'
	WHEN balance > 7000 AND balance <= 7500 THEN '7000~7500'
	WHEN balance > 7500 AND balance <= 8000 THEN '7500~8000'
	WHEN balance > 8000 AND balance <= 8500 THEN '8000~8500'
	WHEN balance > 8500 AND balance <= 9000 THEN '8500~9000'
	WHEN balance > 9000 AND balance <= 9500 THEN '9000~9500'
	WHEN balance > 9500 AND balance <= 10000 THEN '9500~10000'
	WHEN balance > 10000 AND balance <= 15000 THEN '10000~15000'
	WHEN balance > 15000 AND balance <= 20000 THEN '15000~20000'
	WHEN balance > 20000 AND balance <= 25000 THEN '20000~25000'
	WHEN balance > 25000 AND balance <= 30000 THEN '25000~30000'
	WHEN balance > 30000 AND balance <= 35000 THEN '30000~35000'
	WHEN balance > 35000 AND balance <= 40000 THEN '35000~40000'
	WHEN balance > 40000 AND balance <= 45000 THEN '40000~45000'
	WHEN balance > 45000 AND balance <= 50000 THEN '45000~50000'
	WHEN balance > 50000 AND balance <= 55000 THEN '50000~55000'
	WHEN balance > 55000 AND balance <= 60000 THEN '55000~60000'
	WHEN balance > 60000 AND balance <= 65000 THEN '60000~65000'
	WHEN balance > 65000 AND balance <= 70000 THEN '65000~70000'
	WHEN balance > 70000 AND balance <= 75000 THEN '70000~75000'
	WHEN balance > 75000 AND balance <= 80000 THEN '75000~80000'
	WHEN balance > 80000 AND balance <= 85000 THEN '80000~85000'
	WHEN balance > 85000 AND balance <= 90000 THEN '85000~90000'
	WHEN balance > 90000 AND balance <= 95000 THEN '90000~95000'
	WHEN balance > 95000 AND balance <= 100000 THEN '95000~100000'
	WHEN balance > 100000 AND balance <= 1000000000000 THEN '100000~'
	END AS balance ,
	CASE 
	WHEN balance >= 0 AND balance <= 500 THEN 1
	WHEN balance > 500 AND balance <= 1000 THEN 2 
	WHEN balance > 1000 AND balance <= 1500 THEN 3
	WHEN balance > 1500 AND balance <= 2000 THEN 4
	WHEN balance > 2000 AND balance <= 2500 THEN 5
	WHEN balance > 2500 AND balance <= 3000 THEN 6
	WHEN balance > 3000 AND balance <= 3500 THEN 7
	WHEN balance > 3500 AND balance <= 4000 THEN 8
	WHEN balance > 4000 AND balance <= 4500 THEN 9
	WHEN balance > 4500 AND balance <= 5000 THEN 10
	WHEN balance > 5000 AND balance <= 5500 THEN 11
	WHEN balance > 5500 AND balance <= 6000 THEN 12
	WHEN balance > 6000 AND balance <= 6500 THEN 13
	WHEN balance > 6500 AND balance <= 7000 THEN 14
	WHEN balance > 7000 AND balance <= 7500 THEN 15
	WHEN balance > 7500 AND balance <= 8000 THEN 16
	WHEN balance > 8000 AND balance <= 8500 THEN 17
	WHEN balance > 8500 AND balance <= 9000 THEN 18
	WHEN balance > 9000 AND balance <= 9500 THEN 19
	WHEN balance > 9500 AND balance <= 10000 THEN 20
	WHEN balance > 10000 AND balance <= 15000 THEN 21
	WHEN balance > 15000 AND balance <= 20000 THEN 22
	WHEN balance > 20000 AND balance <= 25000 THEN 23
	WHEN balance > 25000 AND balance <= 30000 THEN 24
	WHEN balance > 30000 AND balance <= 35000 THEN 25
	WHEN balance > 35000 AND balance <= 40000 THEN 26
	WHEN balance > 40000 AND balance <= 45000 THEN 27
	WHEN balance > 45000 AND balance <= 50000 THEN 28
	WHEN balance > 50000 AND balance <= 55000 THEN 29
	WHEN balance > 55000 AND balance <= 60000 THEN 30
	WHEN balance > 60000 AND balance <= 65000 THEN 31
	WHEN balance > 65000 AND balance <= 70000 THEN 32
	WHEN balance > 70000 AND balance <= 75000 THEN 33
	WHEN balance > 75000 AND balance <= 80000 THEN 34
	WHEN balance > 80000 AND balance <= 85000 THEN 35
	WHEN balance > 85000 AND balance <= 90000 THEN 36
	WHEN balance > 90000 AND balance <= 95000 THEN 37
	WHEN balance > 95000 AND balance <= 100000 THEN 38
	WHEN balance > 100000 AND balance <= 1000000000000 THEN 39
	END AS rank 
	FROM "POINT_INFO_TBL"
	WHERE point_hist_idx in (
				SELECT MAX(p.point_hist_idx) 
				FROM 	"POINT_INFO_TBL" p,
					"USER_INFO_TBL" u
				WHERE 	p.user_ci = u.user_ci 
					-- AND u.status = 'A'
				GROUP BY p.user_ci)

	) A
GROUP BY balance, rank
ORDER BY rank

=========================================================

결과값 예시
"0~500";7
"9500~10000";1
"15000~20000";1
"95000~100000";1

=========================================================





point_roulette_join 업데이드 시 속도향상 status 에  인덱스를 다시 구성하느라???
현재 있는 인덱스 
-- Index: point_roulette_join_01
-- DROP INDEX point_roulette_join_01;
CREATE INDEX point_roulette_join_01 ON point_roulette_join USING btree (roulette_id COLLATE pg_catalog."default"); 
-- Index: point_roulette_join_02
-- DROP INDEX point_roulette_join_02;
CREATE INDEX point_roulette_join_02 ON point_roulette_join USING btree (roulette_id COLLATE pg_catalog."default", sdate COLLATE pg_catalog."default", status COLLATE pg_catalog."default");

테스트서버에서 수정함
DROP INDEX point_roulette_join_02;
CREATE INDEX point_roulette_join_02 ON point_roulette_join (roulette_id, sdate)

아래는 대표적인 쿼리문들이다. 코스트의 변화는 없지만, 인덱스에서 status값은 빠진상태로 인덱스를 탄다.
EXPLAIN
SELECT COUNT(idx)
		FROM point_roulette_join
		WHERE 1 = 1 
			AND ( cust_id = '20161111' OR ga_id = '20161111' OR ctn = '20161111' )
			AND roulette_id = '20161111'
			AND sdate = TO_CHAR(NOW(),'YYYYMMDD')
			AND status = 'C'

"Aggregate  (cost=8.29..8.30 rows=1 width=4)"
"  ->  Index Scan using point_roulette_join_02 on point_roulette_join  (cost=0.01..8.29 rows=1 width=4)"
"        Index Cond: (((roulette_id)::text = '20161111'::text) AND ((sdate)::text = to_char(now(), 'YYYYMMDD'::text)) AND (status = 'C'::bpchar))"
"        Filter: (((cust_id)::text = '20161111'::text) OR ((ga_id)::text = '20161111'::text) OR ((ctn)::text = '20161111'::text))"

"Aggregate  (cost=8.29..8.30 rows=1 width=4)"
"  ->  Index Scan using point_roulette_join_02 on point_roulette_join  (cost=0.01..8.29 rows=1 width=4)"
"        Index Cond: (((roulette_id)::text = '20161111'::text) AND ((sdate)::text = to_char(now(), 'YYYYMMDD'::text)))"
"        Filter: ((status = 'C'::bpchar) AND (((cust_id)::text = '20161111'::text) OR ((ga_id)::text = '20161111'::text) OR ((ctn)::text = '20161111'::text)))"



EXPLAIN
SELECT idx, roulette_id, cust_id, ga_id, ctn, user_ci, result, sdate, 
		       ipaddr, regdate, status, tr_id, save_point, save_result, modiipaddr, 
		       modidate
		FROM point_roulette_join
		WHERE roulette_id = '20161111'
		AND sdate = TO_CHAR(NOW(),'YYYYMMDD')
		AND ( cust_id = '20161111' OR ga_id = '20161111' OR ctn = '20161111'  )
		AND status = 'P'
		ORDER BY regdate DESC
		limit 1	

"Limit  (cost=8.30..8.30 rows=1 width=174)"
"  ->  Sort  (cost=8.30..8.30 rows=1 width=174)"
"        Sort Key: regdate"
"        ->  Index Scan using point_roulette_join_02 on point_roulette_join  (cost=0.01..8.29 rows=1 width=174)"
"              Index Cond: (((roulette_id)::text = '20161111'::text) AND ((sdate)::text = to_char(now(), 'YYYYMMDD'::text)) AND (status = 'P'::bpchar))"
"              Filter: (((cust_id)::text = '20161111'::text) OR ((ga_id)::text = '20161111'::text) OR ((ctn)::text = '20161111'::text))"

"Limit  (cost=8.30..8.30 rows=1 width=174)"
"  ->  Sort  (cost=8.30..8.30 rows=1 width=174)"
"        Sort Key: regdate"
"        ->  Index Scan using point_roulette_join_02 on point_roulette_join  (cost=0.01..8.29 rows=1 width=174)"
"              Index Cond: (((roulette_id)::text = '20161111'::text) AND ((sdate)::text = to_char(now(), 'YYYYMMDD'::text)))"
"              Filter: ((status = 'P'::bpchar) AND (((cust_id)::text = '20161111'::text) OR ((ga_id)::text = '20161111'::text) OR ((ctn)::text = '20161111'::text)))"



EXPLAIN
SELECT COUNT(idx)
						FROM point_roulette_join AS A LEFT OUTER JOIN point_roulette AS B 
						ON a.roulette_id = b.roulette_id 
						WHERE a.roulette_id = '20161111'
						AND a.sdate = TO_CHAR(NOW(),'YYYYMMDD')
						AND a.status = 'C'
						AND ( (TO_NUMBER(b.starttime,'99') <= TO_NUMBER(TO_CHAR(a.regdate,'HH24'),'99'))  ) 

"Aggregate  (cost=14.54..14.55 rows=1 width=4)"
"  ->  Nested Loop  (cost=0.01..14.54 rows=1 width=4)"
"        Join Filter: (to_number((b.starttime)::text, '99'::text) <= to_number(to_char(a.regdate, 'HH24'::text), '99'::text))"
"        ->  Index Scan using point_roulette_join_02 on point_roulette_join a  (cost=0.01..8.28 rows=1 width=26)"
"              Index Cond: (((roulette_id)::text = '20161111'::text) AND ((sdate)::text = to_char(now(), 'YYYYMMDD'::text)) AND (status = 'C'::bpchar))"
"        ->  Seq Scan on point_roulette b  (cost=0.00..6.24 rows=1 width=16)"
"              Filter: ((roulette_id)::text = '20161111'::text)"

"Aggregate  (cost=14.54..14.55 rows=1 width=4)"
"  ->  Nested Loop  (cost=0.01..14.54 rows=1 width=4)"
"        Join Filter: (to_number((b.starttime)::text, '99'::text) <= to_number(to_char(a.regdate, 'HH24'::text), '99'::text))"
"        ->  Index Scan using point_roulette_join_02 on point_roulette_join a  (cost=0.01..8.28 rows=1 width=26)"
"              Index Cond: (((roulette_id)::text = '20161111'::text) AND ((sdate)::text = to_char(now(), 'YYYYMMDD'::text)))"
"              Filter: (status = 'C'::bpchar)"
"        ->  Seq Scan on point_roulette b  (cost=0.00..6.24 rows=1 width=16)"
"              Filter: ((roulette_id)::text = '20161111'::text)"




EXPLAIN
UPDATE
		point_roulette_join
		SET status = 'C'
			, result= '1'
			, save_point= TO_NUMBER('1','99999999')
			, modiipaddr = '20161111'
			, modidate = NOW()
		WHERE idx = '1'

"Update on point_roulette_join  (cost=0.00..8.28 rows=1 width=149)"
"  ->  Index Scan using "POINT_ROULLETE_JOIN_pkey" on point_roulette_join  (cost=0.00..8.28 rows=1 width=149)"
"        Index Cond: (idx = 1)"

"Update on point_roulette_join  (cost=0.00..8.28 rows=1 width=149)"
"  ->  Index Scan using "POINT_ROULLETE_JOIN_pkey" on point_roulette_join  (cost=0.00..8.28 rows=1 width=149)"
"        Index Cond: (idx = 1)"


























