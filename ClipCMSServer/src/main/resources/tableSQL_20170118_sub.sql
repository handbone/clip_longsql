-- Table: "BZSCREEN_POST_TBL_EVENT"

-- DROP TABLE "BZSCREEN_POST_TBL_EVENT";

CREATE TABLE "BZSCREEN_POST_TBL_EVENT"
(
  idx serial NOT NULL,
  user_id character varying(256) NOT NULL,
  event_name character varying(256),
  reg_date character varying(8),
  reg_time character varying(6),
  event_add_point_sum bigint,
  modify_date character varying(8),
  modify_time character varying(6),
  regdate timestamp without time zone,
  moddate timestamp without time zone,
  CONSTRAINT "PK_BZSCREEN_POST_TBL_EVENT" PRIMARY KEY (idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "BZSCREEN_POST_TBL_EVENT"
  OWNER TO point;

-- Index: "BZSCREEN_POST_TBL_EVENT_01"

-- DROP INDEX "BZSCREEN_POST_TBL_EVENT_01";

CREATE INDEX "BZSCREEN_POST_TBL_EVENT_01"
  ON "BZSCREEN_POST_TBL_EVENT"
  USING btree
  (user_id COLLATE pg_catalog."default", event_name COLLATE pg_catalog."default");

-- Index: "BZSCREEN_POST_TBL_EVENT_02"

-- DROP INDEX "BZSCREEN_POST_TBL_EVENT_02";

CREATE INDEX "BZSCREEN_POST_TBL_EVENT_02"
  ON "BZSCREEN_POST_TBL_EVENT"
  USING btree
  (user_id COLLATE pg_catalog."default");

