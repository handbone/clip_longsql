-- Sequence: point.tbl_stat_receiver_info_no_seq

-- DROP SEQUENCE point.tbl_stat_receiver_info_no_seq;

CREATE SEQUENCE point.tbl_stat_receiver_info_no_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE point.tbl_stat_receiver_info_no_seq
  OWNER TO point;

-- Table: point.tbl_stat_receiver_info

-- DROP TABLE point.tbl_stat_receiver_info;

-- use_flag가 사용중 Y, 삭제시 N
-- tbl_stat_receiver_info_no_seq 로 no 필드 넣을 것.

CREATE TABLE point.tbl_stat_receiver_info
(
  no integer NOT NULL,
  receiver_tel_no character varying(255) NOT NULL,
  user_info character varying(255) NOT NULL,
  use_flag character(1) NOT NULL DEFAULT 'Y'::bpchar,
  CONSTRAINT pk_tbl_stat_receiver_info PRIMARY KEY (no)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point.tbl_stat_receiver_info
  OWNER TO point;

-- 등록
insert into point.tbl_stat_receiver_info (no, receiver_tel_no, user_info, use_flag) values (nextval('point.tbl_stat_receiver_info'), '', '', 'Y')';
-- 삭제
update point.tbl_stat_receiver_info set use_flag = 'N' where no = '';
-- 조회
select no, receiver_tel_no, user_info, use_flag from point.tbl_stat_receiver_info;
