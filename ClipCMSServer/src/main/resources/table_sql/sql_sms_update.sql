-- Sequence: point.tbl_stat_receiver_info_no_seq

-- DROP SEQUENCE tbl_stat_receiver_info_no_seq;

CREATE SEQUENCE tbl_stat_receiver_info_no_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE tbl_stat_receiver_info_no_seq
  OWNER TO point;

-- Table: tbl_stat_receiver_info

-- DROP TABLE tbl_stat_receiver_info;

-- use_flag가 사용중 Y, 삭제시 N
-- tbl_stat_receiver_info_no_seq 로 no 필드 넣을 것.

CREATE TABLE tbl_stat_receiver_info
(
  no integer NOT NULL,
  receiver_tel_no character varying(255) NOT NULL,
  user_info character varying(255) NOT NULL,
  use_flag character(1) NOT NULL DEFAULT 'Y'::bpchar,
  CONSTRAINT pk_tbl_stat_receiver_info PRIMARY KEY (no)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_receiver_info
  OWNER TO point;

-- 등록
--insert into tbl_stat_receiver_info (no, receiver_tel_no, user_info, use_flag) values (nextval('tbl_stat_receiver_info'), '', '', 'Y')';
insert into tbl_stat_receiver_info (no, receiver_tel_no, user_info, use_flag) 
values (nextval('"tbl_stat_receiver_info_no_seq"'::regclass), '010-5555-8888', '876735', 'Y');

-- 삭제
update tbl_stat_receiver_info set use_flag = 'N' where no = '';
-- 조회
select no, receiver_tel_no, user_info, use_flag from tbl_stat_receiver_info;
