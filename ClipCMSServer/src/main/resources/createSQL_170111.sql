같은 테이블 만들고
CREATE TABLE point_roulette_join_log AS  TABLE point_roulette_join;
Query returned successfully: 203814 rows affected, 460 msec execution time.

인덱스 만들어주고
CREATE INDEX point_roulette_join_log_01
  ON point_roulette_join_log
  USING btree
  (roulette_id COLLATE pg_catalog."default");
Query returned successfully with no result in 1.5 secs.

CREATE INDEX point_roulette_join_log_02
  ON point_roulette_join_log
  USING btree
  (roulette_id COLLATE pg_catalog."default", sdate COLLATE pg_catalog."default");
Query returned successfully with no result in 2.6 secs.

날짜 한번 찍어보고
SELECT TO_CHAR((NOW() - interval '3 day'),'YYYYMMDD');
20170103 now 20170106

날짜 이전 데이터는 모두 삭제
DELETE FROM point_roulette_join  WHERE sdate < TO_CHAR((NOW() - interval '3 day'),'YYYYMMDD')
Query returned successfully: 199644 rows affected, 1.0 secs execution time.

스케줄러는 0시 10분에 실행된다.
수동실행 http://localhost:8080/clipcms/point_roulette_join_log.do?preDate=20170105

cms쪽만 수정해준다(바이너리 적용) viewLog_mapper.xml 에서 join 테이블 부분 모두 join_log로 변경
