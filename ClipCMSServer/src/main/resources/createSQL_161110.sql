ALTER TABLE "ACL_INFO_TBL" ADD COLUMN regdate timestamp with time zone
UPDATE "ACL_INFO_TBL" SET regdate = NOW()
ALTER TABLE  "POINT_INFO_TBL" ADD  COLUMN exit_date timestamp without time zone
ALTER TABLE  "POINT_INFO_TBL" ADD  COLUMN exit_gbn character(1) 
ALTER TABLE cms_point_modify ADD  COLUMN message character varying(2056)


-- Function: fn_make_top_stat(character, integer)
-- DROP FUNCTION fn_make_top_stat(character, integer);
CREATE OR REPLACE FUNCTION fn_make_top_stat(
    curdate character,
    limitcount integer)
  RETURNS boolean AS
$BODY$
	BEGIN
		-- 利赋贸府
		INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			'unUsed' as media,
			curDate, 
			'I' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci, 
					SUM(point_value) as point 
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curDate and 
					point_type = 'I' 
				GROUP BY user_ci 
				ORDER BY point DESC
				LIMIT limitCount
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci;

		-- 瞒皑贸府
		INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			'unUsed' as media,
			curDate, 
			'O' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci, 
					SUM(point_value) as point 
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curDate and 
					point_type = 'O' 
				GROUP BY user_ci 
				ORDER BY point DESC
				LIMIT limitCount
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_top_stat(character, integer)
  OWNER TO point;
