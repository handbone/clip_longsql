-- Function: fn_make_anomaly_stat(character, character, numeric, character)

-- DROP FUNCTION fn_make_anomaly_stat(character, character, numeric, character);

CREATE OR REPLACE FUNCTION fn_make_anomaly_stat(
    curdate character,
    curtime character,
    limitpoint numeric,
    modifyaclcode character)
  RETURNS boolean AS
$BODY$
	DECLARE _limit_input_count integer;
	DECLARE _limit_output_count integer;
	DECLARE _input_point numeric;
	DECLARE _output_point numeric;
	DECLARE _reg_sources text[];
	DECLARE _i integer;

	BEGIN

		truncate table tbl_hourly_stat;

		insert into tbl_hourly_stat (p_media, p_user_ci, p_point_type, p_point_value)
		SELECT
			reg_source as p_media,
			user_ci as p_user_ci,
			point_type as p_point_type,
			SUM(point_value) as p_point_value
		FROM
			"POINT_INFO_TBL"
		WHERE
			reg_date = curDate and
			SUBSTRING(reg_time, 1, 2) = curTime
			and reg_source != modifyAclCode
		GROUP BY
			reg_source,
			user_ci,
			point_type;

		_reg_sources := array(SELECT requester_code FROM "ACL_INFO_TBL" WHERE use_status = 'Y' and requester_code != modifyAclCode);

		FOR _i IN 1 .. array_upper(_reg_sources, 1) LOOP
			INSERT INTO tbl_stat_anomaly_detection (media, stat_date, stat_hour, limit_point_value, limit_input_count, limit_output_count, input_point, output_point) VALUES (_reg_sources[_i], curDate, curTime, limitPoint, 0, 0, 0, 0);
			
			SELECT
				COALESCE(COUNT(p_user_ci),0) into _limit_input_count
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'I' and
				p_point_value >= limitPoint;

			SELECT
				COALESCE(COUNT(p_user_ci),0) into _limit_output_count
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'O' and
				p_point_value >= limitPoint;

			SELECT
				COALESCE(SUM(p_point_value),0) into _input_point
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'I';
				
			SELECT
				COALESCE(SUM(p_point_value),0) into _output_point
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'O';
				
			UPDATE
				tbl_stat_anomaly_detection
			SET
				limit_point_value = limitPoint,
				limit_input_count = _limit_input_count,
				limit_output_count = _limit_output_count,
				input_point = _input_point,
				output_point = _output_point
			WHERE
				media = _reg_sources[_i] and
				stat_date = curDate and
				stat_hour = curTime;
				
		END LOOP;
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_anomaly_stat(character, character, numeric, character)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: fn_make_anomaly_stat2(character, character, numeric, character)

-- DROP FUNCTION fn_make_anomaly_stat2(character, character, numeric, character);

CREATE OR REPLACE FUNCTION fn_make_anomaly_stat2(
    curdate character,
    curtime character,
    limitpoint numeric,
    modifyaclcode character)
  RETURNS boolean AS
$BODY$
	BEGIN
		-- 利赋贸府
		INSERT INTO tbl_stat_anomaly_detection_2 ( media , stat_date, stat_hour, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT	
			'unUsed',
			curdate,
			curtime,
			'I' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci,
					SUM(point_value) as point
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curdate and
					SUBSTRING(reg_time, 1, 2) = curtime and
					point_type = 'I' 
					and reg_source != modifyAclCode
				GROUP BY
					user_ci,
					point_type
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci
			AND point >= limitpoint;

		-- 瞒皑贸府
		INSERT INTO tbl_stat_anomaly_detection_2 ( media, stat_date, stat_hour, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			'unUsed',
			curdate,
			curtime,
			'O' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci,
					SUM(point_value) as point
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curdate and
					SUBSTRING(reg_time, 1, 2) = curtime and
					point_type = 'O' 
					and reg_source != modifyAclCode
				GROUP BY
					user_ci,
					point_type
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci
			AND point >= limitpoint;


		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_anomaly_stat2(character, character, numeric, character)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: fn_make_top_stat(character, integer, character)

-- DROP FUNCTION fn_make_top_stat(character, integer, character);

CREATE OR REPLACE FUNCTION fn_make_top_stat(
    curdate character,
    limitcount integer,
    modifyaclcode character)
  RETURNS boolean AS
$BODY$
	BEGIN
		-- 利赋贸府
		INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			'unUsed' as media,
			curDate, 
			'I' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci, 
					SUM(point_value) as point 
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curDate and 
					point_type = 'I' 
					and reg_source != modifyAclCode
				GROUP BY user_ci 
				ORDER BY point DESC
				LIMIT limitCount
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci;

		-- 瞒皑贸府
		INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			'unUsed' as media,
			curDate, 
			'O' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci, 
					SUM(point_value) as point 
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curDate and 
					point_type = 'O' 
					and reg_source != modifyAclCode
				GROUP BY user_ci 
				ORDER BY point DESC
				LIMIT limitCount
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_top_stat(character, integer, character)
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Function: fn_make_top_stat2(character, integer, character)

-- DROP FUNCTION fn_make_top_stat2(character, integer, character);

CREATE OR REPLACE FUNCTION fn_make_top_stat2(
    curdate character,
    limitcount integer,
    modifyaclcode character)
  RETURNS boolean AS
$BODY$
	DECLARE _reg_sources text[];
	BEGIN

	
		_reg_sources := array(SELECT requester_code FROM "ACL_INFO_TBL" WHERE use_status = 'Y' and requester_code != modifyAclCode);


		FOR _i IN 1 .. array_upper(_reg_sources, 1) LOOP

			
			-- 利赋贸府
			INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
			SELECT
				(select requester_name from "ACL_INFO_TBL" where requester_code = a.reg_source ) as media,
				curDate, 
				'I' as point_type, 
				(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
				a.point,
				ui.user_ci, 
				ui.cust_id, 
				ui.ctn, 
				ui.ga_id 
			FROM
				( 
					SELECT
						reg_source, 
						user_ci, 
						SUM(point_value) as point 
					FROM
						"POINT_INFO_TBL"
					WHERE
						reg_source = _reg_sources[_i] and
						reg_date = curDate and 
						point_type = 'I' 
					GROUP BY reg_source, user_ci 
					ORDER BY point DESC
					LIMIT limitCount
					
				) as a,
				"USER_INFO_TBL" as ui
			WHERE
				a.user_ci = ui.user_ci;

			-- 瞒皑贸府
			INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
			SELECT
				(select requester_name from "ACL_INFO_TBL" where requester_code = a.reg_source ) as media,
				curDate, 
				'O' as point_type, 
				(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
				a.point,
				ui.user_ci, 
				ui.cust_id, 
				ui.ctn, 
				ui.ga_id 
			FROM
				(
					SELECT
						reg_source, 
						user_ci, 
						SUM(point_value) as point 
					FROM
						"POINT_INFO_TBL"
					WHERE
						reg_source = _reg_sources[_i] and
						reg_date = curDate and 
						point_type = 'O' 
					GROUP BY reg_source, user_ci 
					ORDER BY point DESC
					LIMIT limitCount
					
				) as a,
				"USER_INFO_TBL" as ui
			WHERE
				a.user_ci = ui.user_ci;

		END LOOP;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_top_stat2(character, integer, character)
  OWNER TO point;
