<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){
            $('.dot1').dotdotdot({
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
            $('#goDelete').click(function(){
            	if(!confirm('삭제를 하시면 다시 복구 할 수 없습니다. 삭제 하시겠습니까?')){
            		return;
            	}
            	
            	var mall_id = '<c:out value="${data.mall_id}"/>'
                	
    				$.ajax({
    					type: "POST",
    					url: "<c:url value='/clip3_0/KTCPMI04/delete.do'/>",
    					data: {mall_id : mall_id},
    					dataType: "Json",
    					success: function (resData) {
    						console.log(resData.result);  
    						if (resData.result == "success") {
    							alert('정상 삭제 되었습니다.');
    							location.href='<c:url value="/clip3_0/KTCPMW09.do"/>';
    						}else{
    							alert('서버 통신 에러가 발생하였습니다. 잠시 후 다시 시도해주세요. ' + resDate.result);
    						}
    					},
    					error: function (e) {
    						alert('서버 통신 에러가 발생하였습니다. 잠시 후 다시 시도해주세요.');
    					}
    				});
            });
            $('#back').click(function(){
           		window.history.back();
            });
            $('#goModify').click(function(){
            	var mall_id = '<c:out value="${data.mall_id}"/>'
            	location.href="<c:url value='/clip3_0/shopping_saving_new.do?mode=2&mall_id=" + mall_id + "'/>";
            });
            
        });
        
	</script>
	<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>

<div class="top_title"><h2> 쇼핑적립 관리 - 상세</h2></div>
    <form method="post" name="mainForm" id="mainForm" action="">
        <table width="100%" class="table_style8" cellpadding="0" cellspacing="0">
            <colgroup>
                <col width="200">
                <col width="">
            </colgroup>
			<tbody>
				<tr>
					<th>상점 ID</th>
					<td>
						${data.mall_id }
					</td>
				</tr>
				<tr>
					<th>쇼핑몰명</th>
					<td>
						${data.mall_name }
					</td>
				</tr>
				<tr>
					<th>연결 URL</th>
					<td>
						http://www.${data.mall_id }.co.kr
					</td>
				</tr>
				<tr>
					<th>노출순서</th>
					<td>
						${data.ord }
					</td>
				</tr>
				<tr>
					<th>상점 이미지 경로</th>
					<td>
						리스트: ${ data.mall_image_url }
			            팝업: ${data.item_image_url }
					</td>
				</tr>
				<tr>
					<th>클립 적립율</th>
					<td>
						${data.reward } (*클립포인트 실제 적립율 입니다.)
					</td>
				</tr>
				<tr>
					<th>링크프라이스 적립율</th>
					<td>
						${data.reward }
					</td>
				</tr>
				<tr>
					<th>사용여부</th>
					<td>
						${data.use_yn }
					</td>
				</tr>
				<tr>
					<th>유의사항</th>
					<td>
						<textarea id="description" name="description" rows="10" cols="100" readOnly>${data.description }</textarea>
					</td>
				</tr>
			</tbody>
		</table>
        <div id="btn_area">
            <input type="button" id="back" name="back" class="btn_area_a" value="목록" >
            <input type="button" id="goModify" name="back" class="btn_area_a" value="수정" >
            <input type="button" id="goDelete" name="goInsert" class="btn_area_a" value="삭제">
        </div>  
    </form>

    <style type="text/css">
    input.error, textarea.error{
      border:1px dashed red;
    }
    label.error{
      margin-left:10px;
      color:red;
    }
    </style>
    
