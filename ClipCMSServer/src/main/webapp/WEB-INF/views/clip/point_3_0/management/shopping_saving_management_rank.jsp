<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){ 
        	var mb_name = '<c:out value="${searchData.mb_name}"/>';
        	var service_status = '<c:out value="${searchData.mb_name}"/>'
        	$('#mb_name').val(mb_name);
        	if(service_status != null && service_status != ''){
				$('input[name=service_status][value=' + service_status + ']').prop('checked', true);
        	}else{
        		$('input[name=service_status][value=Y]').prop('checked', true);
        	}
        	
        	
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
            $('#goSave').click(function(){
            	if(valueCheck()){
            		alert('중복된 노출값이 있습니다.');
            		return;
            	}
            	
            	var rank = [];
            	var mall_id = [];
            	
            	$('input[name=mall_id]').each(function(){
            		mall_id.push($(this).val());
            	});
            	$('input[name=ord]').each(function(){
            		rank.push($(this).val());
            	});
            	
            	console.log(mall_id, rank);
            	var FormData = { 'id': mall_id, 'new_rank': rank };
            	
				$.ajax({
					type: "POST",
					url: "<c:url value='/clip3_0/KTCPMI04/rankModify.do'/>",
// 					data: 'rank=' + rank + '&mb_id=' + mb_id,
					data: FormData,
					dataType: "JSON",
					success: function (resData) {
						console.log(resData.result);
						if (resData.result == "success") {
							location.reload();
						}else{
// 							alert('서버 통신 에러가 발생하였습니다. 잠시 후 다시 시도해주세요. ' + resDate.result);
						}
					},
					error: function (e) {
// 						alert('서버 통신 에러가 발생하였습니다. 잠시 후 다시 시도해주세요.');
					}
				});
			});
            $('#back').click(function(){
           		window.history.back();
            });
        }); 

        function valueCheck(){
        	var rank = [];
        	var error = false;
        	
        	$('input[name=ord]').each(function(e){
        		var thisRank = $(this).val();
        		for(var i = 0; i < rank.length; i++){
        			if(rank[i] == thisRank){
        				error =  true;
        				break;
        			}
        		}
        		rank.push(thisRank);
        	});
        	
        	return error;
        }
	</script>

<!--Content layout -->

<!-- [D] add -->
<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
    
<!--// [D] add -->  

	<div class="top_title"><h2> 쇼핑적립 관리 - 노출 순위 설정</h2></div>
    <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
            	<th>상점명</th>
                <th>적립율</th>
                <th>노출순서</th>
            </tr>
            <c:choose>
				<c:when test="${not empty list }">
					<c:forEach var="item" items="${list}">
						<tr>
							<td align="center">
								<a href="<c:url value='/clip3_0/shopping_saving_new.do?mode=1&mall_id=${item.mall_id }'/>">${item.mall_name }</a>
								<input id="mall_id" name="mall_id" type="hidden" value="${item.mall_id }"/>
							</td>
							<td align="center">${item.reward }</td>
							<td align="center"><input name="ord" id="ord" type="text" value="${item.ord}" style="text-align:center;"/></td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="3"> 조회값이 없습니다. </td>
					</tr>
				</c:otherwise>
			</c:choose>
        </tbody>
    </table>
    <div id="btn_area">
		<input type="button" id="back" name="back" class="btn_area_a" value="목록" >
		<input type="button" id="goSave" name="goModify" class="btn_area_a" value="저장" >
    </div>
    <style type="text/css">
    input.error, textarea.error{
      border:1px dashed red;
    }
    label.error{
      margin-left:10px;
      color:red;
    }
    </style>

