<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){ 
        	var mb_name = '<c:out value="${mb_name}"/>';
        	var service_status = '<c:out value="${service_status}"/>'
        	console.log(service_status);
        	$('#mb_name').val(mb_name);
        	if(service_status != null && service_status != ''){
				$('input[name=service_status][value=' + service_status + ']').prop('checked', true);
        	}else{
        		$('input[name=service_status][value=""]').prop('checked', true);
        	}
        	
        	
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
            $('#datepicker1').change(function(){
            	var date1 = $('#datepicker1').val();
            	var date2 = $('#datepicker2').val();
            	
            	var startDateArr = date1.split('.');
            	var endDateArr = date2.split('.');
            	
            	var startDate = new Date(startDateArr[0], startDateArr[1], startDateArr[2]);
            	var endDate = new Date(endDateArr[0], endDateArr[1], endDateArr[2]);
            	
            	if(startDate.getTime() > endDate.getTime()){
            		alert('사용 기간을 확인해주세요.');
            		$('#datepicker1').val('');
            		return;
            	}
            });
            $('#datepicker2').change(function(){
            	var date1 = $('#datepicker1').val();
            	var date2 = $('#datepicker2').val();
            	
            	var startDateArr = date1.split('.');
            	var endDateArr = date2.split('.');
            	
            	var startDate = new Date(startDateArr[0], startDateArr[1], startDateArr[2]);
            	var endDate = new Date(endDateArr[0], endDateArr[1], endDateArr[2]);
            	
            	if(startDate.getTime() > endDate.getTime()){
            		alert('사용 기간을 확인해주세요.');
            		$('#datepicker2').val('');
            		return;
            	}
            });
            $('#goSearch').click(function(){
           		$('#mainForm').submit();
            });
            $('#goRank').click(function(){
           		location.href='<c:url value="/clip3_0/mainbanner_new.do?mode=3"/>';
            });
        }); 

	</script>

<!--Content layout -->

<!-- [D] add -->
<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
    
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>
<script>
$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value="/resources/css/ui/img/calendar.gif"/>',   
    buttonImageOnly: true,
    dateFormat: 'yy.mm.dd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

 $(document).ready(function(){ 
     
     $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
     
     $("#datepicker3, #datepicker4").next().click(function () {
            
            $("#ui-datepicker-div > .ui-datepicker-calendar").hide();
            
            $("#ui-datepicker-div").position({
                my: "center top",
                at: "center bottom",
                of: $(this)
            });
        });
    
}); 
</script>
<!--// [D] add -->  

	<div class="top_title"><h2> 메인 배너 관리 - 배너 목록</h2></div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <colgroup>
            <col width="">
            <col width="">
           
        </colgroup>
        <tbody>
            <tr>
                <th></th>
                <td>
                    <a href="<c:url value='/clip3_0/mainbanner_new.do?mode=0'/>" class="btn_area_b">신규등록</a>
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <form id="mainForm" name="mainForm">
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
        <colgroup>
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
                <th>배너명</th>
                <td>
                    <input class="form_input_style2" id="mb_name" name="mb_name" style="width:200px" value="">
                </td>
            </tr>
            <tr>
                <th>겸색기간</th>
                <td>
                    <div id="datepickerDiv1">

                    	<%
							Calendar cal = Calendar.getInstance();
							cal.add(Calendar.MONTH, -1);
							Date date = cal.getTime();
							String past = new SimpleDateFormat("yyyy.MM.dd").format(date);
						%>
                       	<c:choose>
	              			<c:when test="${datepicker1 ne null}">
	              				<fmt:parseDate var="date1" value="${datepicker1}" pattern="yyyyMMdd"/>
	              				<input name="datepicker1"  type="text"  id="datepicker1"  value="<fmt:formatDate value="${date1 }" pattern="yyyy.MM.dd"/>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
	              			</c:when>
	              			<c:otherwise>
	              				<input name="datepicker1"  type="text"  id="datepicker1"  value="<%=past %>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
	              			</c:otherwise>
                   		</c:choose>
                        
                   		~
                    		
                   		<c:choose>
                   			<c:when test="${datepicker2 ne null}">
                   				<fmt:parseDate var="date2" value="${datepicker2}" pattern="yyyyMMdd"/>
                   				<input name="datepicker2" type="text" id="datepicker2"  value="<fmt:formatDate value="${date2 }" pattern="yyyy.MM.dd"/>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
                   			</c:when>
                   			<c:otherwise>
                   				<input name="datepicker2" type="text" id="datepicker2"  value="<fmt:formatDate value="<%=new java.util.Date() %>" pattern="yyyy.MM.dd"/>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
                   			</c:otherwise>
                   		</c:choose> 
                   		</div>
                </td>
            </tr>
            <tr>
                <th>서비스 상태</th>
                <td>
                    <input id="" name="service_status" value="" checked="checked" type="radio"> 전체
                    <input id="" name="service_status" value="Y" type="radio"> 사용중  
                    <input id="" name="service_status" value="N" type="radio"> 중지 
                </td>
            </tr>
        </tbody>
    </table>
    <div id="btn_area">
        <input type="button" id="goRank" name="goRank" class="btn_area_a" value="노출 순서 설정">
        <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색">
    </div>
    
    </form>
    <br>
   
    총 <font color="red">${totalcount }</font> 개의 매체가 검색되었습니다.
    <c:choose>
		<c:when test="${(not empty list) and ('0' ne totalcount) }">
			총 <font color="red">${totalcount }</font> 개의 매체가 검색되었습니다.
		</c:when>
		<c:otherwise>
			총 <font color="red">0</font> 개의 매체가 검색되었습니다.
		</c:otherwise>
   </c:choose>
    <br>
    <br>
    <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
            	<th>배너명</th>
                <th>연결 URL</th>
                <th>상태</th>
                <th>설명</th>
                <th>노출순서</th>
                <th>사용 기간</th>
                <th>등록일</th>
                <th>OS</th>
                <th>url 타입</th>
            </tr>
			<c:choose>
				<c:when test="${(not empty list) and ('0' ne totalcount) }">
					<c:forEach var="item" items="${list}">
						<tr>
							<td align="center">
								<a href="<c:url value='/clip3_0/mainbanner_new.do?mode=1&mb_id=${item.mb_id }'/>">${item.mb_name }</a>
							</td>
							<td align="center"><span class="dot2">${item.link_url }</span></td>
							<td align="center">
								<c:choose>
									<c:when test="${item.service_status eq 'Y' }">
										사용중
									</c:when>
									<c:otherwise>
										중지
									</c:otherwise>
								</c:choose>
							</td>
							<td align="center">${item.description }</td>
							<td align="center">${item.rank }</td>
							<td align="center">
								<fmt:parseDate var="date1" value="${item.start_date}" pattern="yyyy-MM-dd"/>
								<fmt:parseDate var="date2" value="${item.end_date }" pattern="yyyy-MM-dd"/>
								<fmt:formatDate value="${date1 }" pattern="yyyy.MM.dd"/> 
								~ <fmt:formatDate value="${date2 }" pattern="yyyy.MM.dd"/>
							</td>
							<td align="center">
								<fmt:parseDate var="regDate" value="${item.reg_date}" pattern="yyyy-MM-dd"/>
								<fmt:formatDate value="${regDate }" pattern="yyyy.MM.dd"/>
							</td>
							<td align="center">
								<c:choose>
									<c:when test="${item.os_type eq 'I' }">
										IOS
									</c:when>
									<c:when test="${item.os_type eq 'A' }">
										Android
									</c:when>
									<c:otherwise>
										전체
									</c:otherwise>
								</c:choose>
							</td>
							<td align="center">
								<c:choose>
									<c:when test="${item.url_type eq 'U' }">
										URL
									</c:when>
									<c:otherwise>
										직링크
									</c:otherwise>
								</c:choose>
							</td>
						</tr>					
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="7"> 조회값이 없습니다. </td>
					</tr>
				</c:otherwise>
			</c:choose>
        </tbody>
    </table>
    

