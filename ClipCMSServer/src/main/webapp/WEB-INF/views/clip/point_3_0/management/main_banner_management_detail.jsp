<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
            $('#goDelete').click(function(){
            	if(!confirm('삭제를 하시면 다시 복구 할 수 없습니다. 삭제 하시겠습니까?')){
            		return;
            	}
            	
            	var mb_id = '<c:out value="${data.mb_id}"/>'
            	
				$.ajax({
					type: "POST",
					url: "<c:url value='/clip3_0/KTCPMI03/delete.do'/>",
					data: {mb_id : mb_id},
					dataType: "Json",
					success: function (resData) {
						console.log(resData.result);  
						if (resData.result == "success") {
							alert('정상 삭제 되었습니다.');
							location.href='<c:url value="/clip3_0/KTCPMW10.do"/>';
						}else{
							alert('서버 통신 에러가 발생하였습니다. 잠시 후 다시 시도해주세요. ' + resDate.result);
						}
					},
					error: function (e) {
						alert('서버 통신 에러가 발생하였습니다. 잠시 후 다시 시도해주세요.');
					}
				});
			});
            $('#back').click(function(){
           		window.history.back();
            });
            $('#goModify').click(function(){
            	var mb_id = '<c:out value="${data.mb_id}"/>'
            	location.href = '<c:url value="/clip3_0/mainbanner_new.do?mode=2&mb_id='+ mb_id + '"/>';
            });
        }); 
		
	</script>
	<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js'/>"></script>

<div class="top_title"><h2> 메인 배너 관리 - 상세보기</h2></div>
    <form method="post" name="mainForm" id="mainForm" action="">
        <table width="100%" class="table_style8" cellpadding="0" cellspacing="0">
            <colgroup>
                <col width="200">
                <col width="">
            </colgroup>
			<tbody>
				<tr>
					<th>배너ID</th>
					<td>
						${data.mb_id }
					</td>
				</tr>
				<tr>
					<th>배너명</th>
					<td>
						${data.mb_name }
					</td>
				</tr>
				<tr>
					<th>연결 URL</th>
					<td>
						${data.link_url }
					</td>
				</tr>
				<tr>
					<th>서비스 상태</th>
					<td>
						${data.service_status }
					</td>
				</tr>
				<tr>
					<th>노출순서</th>
					<td>
						${data.rank }
					</td>
				</tr>
				<tr>
					<th>노출 os</th>
					<td>
						<c:choose>
							<c:when test="${data.os_type eq 'I' }">
								IOS
							</c:when>
							<c:when test="${data.os_type eq 'A' }">
								Android
							</c:when>
							<c:otherwise>
								전체
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<th>URL 타입</th>
					<td>
						<c:choose>
							<c:when test="${data.url_type eq 'U' }">
								URL
							</c:when>
							<c:otherwise>
								직링크
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<th>사용 기간</th>
					<td>
						<div id="datepickerDiv1">
              				<fmt:parseDate var="date1" value="${data.start_date }" pattern="yyyy-MM-dd"/>
							<fmt:formatDate value="${date1 }" pattern="yyyy.MM.dd"/>
	              			~ 
	              			<fmt:parseDate var="date2" value="${data.end_date }" pattern="yyyy-MM-dd"/>
							<fmt:formatDate value="${date2 }" pattern="yyyy.MM.dd"/>
                   		</div>
					</td>
				</tr>
				<tr>
					<th>배너 이미지</th>
					<td>
						${data.mbimg_url }
					</td>
				</tr>
				<tr>
					<th>비고</th>
					<td>
						<textarea id="description" name="description" rows="10" cols="100" readOnly>${data.description }</textarea> 
					</td>
				</tr>
			</tbody>
		</table>
        <div id="btn_area">
            <input type="button" id="back" name="back" class="btn_area_a" value="목록" >
            <input type="button" id="goModify" name="goModify" class="btn_area_a" value="수정" >
            <input type="button" id="goDelete" name="goDelete" class="btn_area_a" value="삭제">
        </div>  
    </form>

    <style type="text/css">
    input.error, textarea.error{
      border:1px dashed red;
    }
    label.error{
      margin-left:10px;
      color:red;
    }
    </style>
    
