<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){ 
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
        }); 

	</script>

<!--Content layout -->

	<div class="top_title"><h2> 일차 관리 - 목록</h2></div>
   <!-- 매체 목록 -->
   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
            	<th>일차</th>
                <th>문구</th>
                <th>연결 URL</th>
                <th>버튼 노출</th>
                <th>수정</th>
            </tr>
            <c:forEach items="${screenlockvo}" var="obj">
            		<tr>
	                <td align="center"><c:out value="${obj.day_count}"/> 일차</td>
	                <td align="left"><c:out value="${obj.content}"/></td>
	                <td align="center"><c:out value="${obj.link_url}"/></td>
	                <td align="center"><c:out value="${obj.expose_yn}"/></td>
	                <td align="center"><input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="수정" onclick="javascript:location.href='<c:url value="/clip3_0/KTCPMW12.do?index=${obj.day_count}"/>'"></td>
	            </tr>
            </c:forEach>            
        </tbody>
    </table>
