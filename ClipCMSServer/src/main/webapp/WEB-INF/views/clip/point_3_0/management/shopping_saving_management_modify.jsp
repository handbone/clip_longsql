<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){
        	var use_yn = '<c:out value="${data.use_yn}"/>';
        	if(use_yn != null && use_yn != ''){
				$('input[name=use_yn][value=' + use_yn + ']').prop('checked', true);
        	}
        	
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
            $('#goUpdate').click(function(){
            	if(valueCheck()){
            		return;
            	}
            	
				var formData = $("#mainForm").serialize();
				$.ajax({
					type: "POST",
					url: "<c:url value='/clip3_0/KTCPMI04/update.do'/>",
					data: formData,
					dataType: "Json",
					success: function (resData) {
						console.log(resData.result);  
						if (resData.result == "success") {
							alert('정상 저장 되었습니다.');
							location.href='<c:url value="/clip3_0/KTCPMW09.do"/>';
						}else{
							alert('서버 통신 에러가 발생하였습니다. 잠시 후 다시 시도해주세요. ' + resDate.result);
						}
					},
					error: function (e) {
						alert('서버 통신 에러가 발생하였습니다. 잠시 후 다시 시도해주세요.');
					}
				});
			});
            $('#back').click(function(){
            	if(confirm('작성한 내용이 저장되지 않았습니다. 취소하시겠습니까?')){
            		window.history.back();
            	}
            });
            function valueCheck(){
            	var mall_id = $('#mall_id').val();
            	var mall_name = $('#mall_name').val();
            	var description = $('#description').val();
            	
            	if(mall_id == null || mall_id == ''){
            		alert('상점 ID를 확인해주세요.');
            		$('#mall_id').focus();
            		return true;
            	}
            	if(mall_name == null || mall_name == ''){
            		alert('상점명을 확인해주세요.');
            		$('#mall_name').focus();
            		return true;
            	}
            	if(description == null || description == ''){
            		alert('유의사항을 확인해주세요.');
            		$('#description').focus();
            		return true;
            	}
            	return false;
            }
            
        }); 
		
        function valueCheck(){
        	var mall_id = $('#mall_id').val();
        	var mall_name = $('#mall_name').val();
        	var description = $('#description').val();
        	
        	if(mall_id == null || mall_id == ''){
        		alert('상점 ID를 확인해주세요.');
        		$('#mall_id').focus();
        		return true;
        	}
        	if(mall_name == null || mall_name == ''){
        		alert('상점명을 확인해주세요.');
        		$('#mall_name').focus();
        		return true;
        	}
        	if(description == null || description == ''){
        		alert('유의사항을 확인해주세요.');
        		$('#description').focus();
        		return true;
        	}
        	return false;
        }
        
	</script>
	<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>

<div class="top_title"><h2> 쇼핑적립 관리 - 수정</h2></div>
    <form method="post" name="mainForm" id="mainForm" action="">
        <table width="100%" class="table_style8" cellpadding="0" cellspacing="0">
            <colgroup>
                <col width="200">
                <col width="">
            </colgroup>
			<tbody>
				<tr>
					<th>상점 ID</th>
					<td>
						<input id="mall_id" name="mall_id" type="text" value="${data.mall_id }" readOnly/>
					</td>
				</tr>
				<tr>
					<th>쇼핑몰명</th>
					<td>
						<input id="mall_name" name="mall_name" type="text" class="form_input_style2" value="${data.mall_name }"> (255byte 이내)
					</td>
				</tr>
				<tr>
					<th>연결 URL</th>
					<td>
						<input id="link_url" name="link_url" type="text" class="form_input_style1" maxlength="2000" value="http://www.${data.mall_id }.co.kr"> 
					</td>
				</tr>
				<tr>
					<th>노출순서</th>
					<td>
						${data.ord }(노출순서는 리스트의 '노출 순서 변경'에서 가능합니다.)
					</td>
				</tr>
				<tr>
					<th>상점 이미지 경로</th>
					<td>
						리스트:<input id="mall_image_url" name="mall_image_url" value="${data.mall_image_url }" type="text" class="form_input_style1">
			            팝업: <input id="item_image_url" name="item_image_url" value="${data.item_image_url }" type="text" class="form_input_style1">
					</td>
				</tr>
				<tr>
					<th>클립 적립율</th>
					<td>
						<input id="reward" name="reward" type="text" value="${data.reward }"/>% (*클립포인트 실제 적립율 입니다.)
					</td>
				</tr>
				<tr>
					<th>링크프라이스 적립율</th>
					<td>
						클립 적립율과 같음.
					</td>
				</tr>
				<tr>
					<th>사용여부</th>
					<td>
						<input id="use_yn" name="use_yn" type="radio" value="Y"/> 사용중
						<input id="" name="" type="radio" value="N"/> 중지
					</td>
				</tr>
				<tr>
					<th>유의사항</th>
					<td>
						<textarea id="description" name="description" rows="10" cols="100">${data.description }</textarea> 
					</td>
				</tr>
			</tbody>
		</table>
        <div id="btn_area">
            <input type="button" id="back" name="back" class="btn_area_a" value="취소" >
            <input type="button" id="goUpdate" name="goInsert" class="btn_area_a" value="확인">
        </div>  
    </form>

    <style type="text/css">
    input.error, textarea.error{
      border:1px dashed red;
    }
    label.error{
      margin-left:10px;
      color:red;
    }
    </style>
    
