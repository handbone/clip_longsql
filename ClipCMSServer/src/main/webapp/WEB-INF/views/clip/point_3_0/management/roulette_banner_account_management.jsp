<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){ 
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
        }); 

	</script>


<!--Content layout -->
<!-- [D] add -->
<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
    
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>
<script>
$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value="/resources/css/ui/img/calendar.gif"/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

 $(document).ready(function(){ 
     
     $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
     
     $("#datepicker3, #datepicker4").next().click(function () {
            
            $("#ui-datepicker-div > .ui-datepicker-calendar").hide();
            
            $("#ui-datepicker-div").position({
                my: "center top",
                at: "center bottom",
                of: $(this)
            });
        });
    
}); 
</script>
<!--// [D] add -->  

	<div class="top_title"><h2> 룰렛 배너 관리</h2></div>
	
	<form id="mainForm" name="mainForm">
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
        <colgroup>
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
                <th>상태</th>
                <td align="center">
                    ON
                </td>
                <td align="right"><input type="button" id="" name="" class="btn_area_a" value="OFF"></td>
            </tr>
        </tbody>
    </table>
	<br>
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
        <colgroup>
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
                <th>겸색기간</th>
                <td>
                    <div id="datepickerDiv1">

                        <input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
                    ~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">                                
                   </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="btn_area">
        <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="조회">
    </div>
    
    </form>
    <br>
	
    <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
                <th>No.</th>
                <th>사유</th>
                <th>담당자</th>
                <th>변경값</th>
                <th>일자</th>
            </tr>            
            <tr>
                <td align="center">100</td>
                <td align="left">장애발생 해결로 재 사용</td>
                <td align="center">12345678</td>
                <td align="center">ON</td>
                <td align="center">2017.01.01</td>
            </tr>
            <tr>
                <td colspan="5"> 조회값이 없습니다. </td>
            </tr>
        </tbody>
    </table>
    <div id="btn_area">
	 	<input type="button" class="btn_area_a" value="목록" onclick="javascript:history.go(-1)">
	 	<input type="button" id="goInsert" name="goInsert" class="btn_area_a" value="저장">
	</div>   
