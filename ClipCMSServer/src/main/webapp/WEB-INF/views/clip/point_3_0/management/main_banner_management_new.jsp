<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
            $('#datepicker1').change(function(){
            	var date1 = $('#datepicker1').val();
            	var date2 = $('#datepicker2').val();
            	
            	var startDateArr = date1.split('.');
            	var endDateArr = date2.split('.');
            	
            	var startDate = new Date(startDateArr[0], startDateArr[1], startDateArr[2]);
            	var endDate = new Date(endDateArr[0], endDateArr[1], endDateArr[2]);
            	
            	if(startDate.getTime() > endDate.getTime()){
            		alert('사용 기간을 확인해주세요.');
            		$('#datepicker1').val('');
            		return;
            	}
            });
            $('#datepicker2').change(function(){
            	var date1 = $('#datepicker1').val();
            	var date2 = $('#datepicker2').val();
            	
            	var startDateArr = date1.split('.');
            	var endDateArr = date2.split('.');
            	
            	var startDate = new Date(startDateArr[0], startDateArr[1], startDateArr[2]);
            	var endDate = new Date(endDateArr[0], endDateArr[1], endDateArr[2]);
            	
            	if(startDate.getTime() > endDate.getTime()){
            		alert('사용 기간을 확인해주세요.');
            		$('#datepicker2').val('');
            		return;
            	}
            });
            $('#goInsert').click(function(){
            	if(valueCheck()){
            		return;
            	}
            	
				var formData = $("#mainForm").serialize();
				$.ajax({
					type: "POST",
					url: "<c:url value='/clip3_0/KTCPMI03/insert.do'/>",
					data: formData,
					dataType: "Json",
					success: function (resData) {
						console.log(resData.result);  
						if (resData.result == "success") {
							alert('정상 저장 되었습니다.');
							location.href='<c:url value="/clip3_0/KTCPMW10.do"/>';
						}else{
							alert('서버 통신 에러가 발생하였습니다. 잠시 후 다시 시도해주세요. ' + resDate.result);
						}
					},
					error: function (e) {
						alert('서버 통신 에러가 발생하였습니다. 잠시 후 다시 시도해주세요.');
					}
				});
			});
            $('#back').click(function(){
            	if(confirm('작성한 내용이 저장되지 않았습니다. 취소하시겠습니까?')){
            		window.history.back();
            	}
            });
        }); 
		
        function valueCheck(){
        	var mb_name = $('#mb_name').val();
        	var link_url = $('#link_url').val();
        	var service_status = $('input[name=service_status]').val();
        	var datepicker1 = $('#datepicker1').val();
        	var datepicker2 = $('#datepicker2').val();
//         	var mbimg_url = $('#mbimg_url').val();
        	var description = $('#description').val();
        	var os_type = $('input[name=os_type]').val();
        	var url_type = $('input[name=url_type]').val();
        	
        	if(mb_name == null || mb_name == ''){
        		alert('배너명을 확인해주세요.');
        		$('#mb_name').focus();
        		return true;
        	}
        	if(link_url == null || link_url == ''){
        		alert('연결URL을 확인해주세요.');
        		$('#link_url').focus();
        		return true;
        	}
        	if(service_status == null || service_status == ''){
        		alert('서비스 상태를 확인해주세요.');
        		$('input[name=service_status]').focus();
        		return true;
        	}
        	if(os_type == null || os_type == ''){
        		alert('노출 os 타입을 확인해주세요.');
        		$('input[name=os_type]').focus();
        		return true;
        	}
        	if(url_type == null || url_type == ''){
        		alert('URL 타입을 확인해주세요.');
        		$('input[name=url_type]').focus();
        		return true;
        	}
        	if(datepicker1 == null || datepicker1 == ''){
        		alert('사용기간을 확인해주세요.');
        		$('#datepicker1').focus();
        		return true;
        	}
        	if(datepicker2 == null || datepicker2 == ''){
        		alert('사용기간을 확인해주세요.');
        		$('#datepicker2').focus();        		
        		return true;
        	}
//         	if(mbimg_url == null || mbimg_url == ''){
// 				alert('배너 이미지를 확인해주세요.');
// 				$('#mbimg_url').focus();
//         		return true;
//         	}
        	if(description == null || description == ''){
        		alert('비고를 확인해주세요.');
        		$('#description').focus();
        		return true;
        	}
        	return false;
        }
        
	</script>
	<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>
	<script>
	$( function() {
	    $( "#datepicker1, #datepicker2" ).datepicker({
	    autoSize: true,
	    showOn: 'button', 
	    buttonImage: '<c:url value="/resources/css/ui/img/calendar.gif"/>',   
	    buttonImageOnly: true,
	    dateFormat: 'yy.mm.dd'});
	} );
	$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );
	
	$(document).ready(function(){ 
	     
	     $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	     
	     $("#datepicker3, #datepicker4").next().click(function () {
	            
	            $("#ui-datepicker-div > .ui-datepicker-calendar").hide();
	            
	            $("#ui-datepicker-div").position({
	                my: "center top",
	                at: "center bottom",
	                of: $(this)
	            });
	        });
	    
	}); 
	</script>

<div class="top_title"><h2> 메인 배너 관리 - 신규등록</h2></div>
    <form method="post" name="mainForm" id="mainForm" action="">
        <table width="100%" class="table_style8" cellpadding="0" cellspacing="0">
            <colgroup>
                <col width="200">
                <col width="">
            </colgroup>
			<tbody>
				<tr>
					<th>배너ID</th>
					<td>
						신규 등록시 자동으로 생성 됩니다.
					</td>
				</tr>
				<tr>
					<th>배너명</th>
					<td>
						<input id="mb_name" name="mb_name" type="text" class="form_input_style2"> (255byte 이내)
					</td>
				</tr>
				<tr>
					<th>연결 URL</th>
					<td>
						<input id="link_url" name="link_url" type="text" class="form_input_style1" maxlength="2000" value=""> 
					</td>
				</tr>
				<tr>
					<th>서비스 상태</th>
					<td>
						<input name="service_status" value="Y" type="radio" checked="checked"> 사용중
						<input name="service_status" value="N" type="radio"> 중지
					</td>
				</tr>
				<tr>
					<th>노출순서</th>
					<td>
						등록시 마지막 순서로 자동 생성 됩니다. 순서변경은 리스트의 '노출 순서 변경'에서 가능합니다.
					</td>
				</tr>
				<tr>
					<th>노출 OS</th>
					<td>
						<input name="os_type" value="D" type="radio" checked="checked"> 전체
						<input name="os_type" value="A" type="radio"> Android
						<input name="os_type" value="I" type="radio"> IOS
					</td>
				</tr>
				<tr>
					<th>URL 타입</th>
					<td>
						<input name="url_type" value="D" type="radio" checked="checked"> 직링크
						<input name="url_type" value="U" type="radio"> URL
					</td>
				</tr>
				<tr>
					<th>사용 기간</th>
					<td>
						<div id="datepickerDiv1">
							<%
								Calendar cal = Calendar.getInstance();
								cal.add(Calendar.MONTH, +1);
								Date date = cal.getTime();
								String past = new SimpleDateFormat("yyyy.MM.dd").format(date);
							%>
	                        <c:set var="past" value="<%=new java.util.Date(new java.util.Date().getTime() - 60*60*24*1000*7*4)%>"/>
              				<input name="datepicker1"  type="text"  id="datepicker1"  value="<fmt:formatDate value="<%=new java.util.Date() %>" pattern="yyyy.MM.dd"/>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
	              			~ 
	              			<input name="datepicker2" type="text" id="datepicker2"  value="<%=past %>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
                   		</div>
					</td>
				</tr>
				<tr>
					<th>배너 이미지</th>
					<td>
						<input id="mbimg_url" name="mbimg_url" value="" type="text" class="form_input_style1" >
					</td>
				</tr>
				<tr>
					<th>비고</th>
					<td>
						<textarea id="description" name="description" rows="10" cols="100"></textarea> 
					</td>
				</tr>
			</tbody>
		</table>
        <div id="btn_area">
            <input type="button" id="back" name="back" class="btn_area_a" value="목록" >
            <input type="button" id="goInsert" name="goInsert" class="btn_area_a" value="등록">
        </div>  
    </form>

    <style type="text/css">
    input.error, textarea.error{
      border:1px dashed red;
    }
    label.error{
      margin-left:10px;
      color:red;
    }
    </style>
    
