<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){ 
        	var status = '<c:out value="${status}"/>';
        	var keywd = '<c:out value="${keywd}"/>';
        	var requester_code = '<c:out value="${requester_code}"/>';
        	var searchYear = '<c:out value="${searchYear}"/>';
        	var searchDate = '<c:out value="${searchDate}"/>';
        	
        	$('#status').val(status);
        	$('#keywd').val(keywd);
        	$('#requester_code').val(requester_code);
        	if(searchYear != '' && searchYear != null){
	        	$('#searchYear').val(searchYear);
	        	$('#searchDate').val(searchDate);
        	}
//         	else{
//         		var dt = new Date();

//         		// Display the month, day, and year. getMonth() returns a 0-based number.
//         		var month = dt.getMonth()+1;
//         		var day = dt.getDate();
//         		var year = dt.getFullYear();
//         		console.log(month + '-' + day + '-' + year);
//         		$('#searchYear').val(year);
// 	        	$('#searchDate').val(month);
//         	}
        	
        	$('#goSearch').click(function(){
            	var keywd = $('#keywd').val();
//             	if(keywd == ''){
//             		alert('검색조건을 입력해주세요.');
//             		return;
//             	}
           		$('#mainForm').submit();
            });
        	
			$('#goExcel').click(function(){
				 var form = document.createElement("form");
				 form.method = "POST";
				 form.action = "<c:url value='/clip3_0/KTCPMW03/excelDownload.do'/>";  
				 document.body.appendChild(form);
				 form.submit();
            });
			
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
        }); 

	</script>

	<div class="top_title"><h2> 적립 포인트 조회</h2></div>

    
    <form id="mainForm" name="mainForm" action="<c:url value='/clip3_0/KTCPMW03.do'/>">
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="">
            <col width="">
       
        </colgroup>
        <tbody>
            <tr>
                <th>검색조건</th>
                <td>
                    <select class="form_sel_style1" name="status" id="status">
                        <option value="" selected="">전체</option>
                        <option value="user_ci">user_ci</option>
                        <option value="cust_id">cust_id</option>
                        <option value="ga_id">ga_id</option>
                        <option value="ctn">ctn</option>
                    </select>
                    <input class="form_input_style2" id="keywd" name="keywd" style="width:200px" value="">
                </td>
            </tr>
            <tr>
                <th>적립기준일</th>
                <td>
                    <select class="form_sel_style1" name="searchYear" id="searchYear">
                    	<c:forEach var="i" begin="2010" end="2020" step="1">
                    		<c:choose>
	                    		<c:when test="${i eq 2017 }">
	                    			<option value="${i }" selected>${i }</option>
	                    		</c:when>
	                    		<c:otherwise>
	                    			<option value="${i }">${i }</option>
	                    		</c:otherwise>
                    		</c:choose>
                    	</c:forEach>
                    </select>
                    <select class="form_sel_style1" name="searchDate" id="searchDate">
	                    <c:forEach var="i" begin="1" end="12" step="1">
	                    	<c:choose>
	                    		<c:when test="${i eq 9 }">
	                    			<option value="${i }" selected>${i }</option>
	                    		</c:when>
	                    		<c:otherwise>
	                    			<option value="${i }">${i }</option>
	                    		</c:otherwise>
                    		</c:choose>
                    	</c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <th>쇼핑몰 구분</th>
                <td>
                    <select class="form_sel_style1" name="requester_code" id="requester_code">
                        <option value="">전체</option>
                          <c:forEach items="${shoppinMallList}" var="list" >
		            		<option value="${list.mall_id}">${list.mall_name}</option>
		            		 
		           			</c:forEach>
                    </select>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="btn_area">
        <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색">
        <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드">
    </div>
    
    </form>
    
   <br>
   
   <c:choose>
		<c:when test="${(not empty list) and ('0' ne totalcount) }">
			조회 목록 전체 : 총 <font color="red">${totalcount} </font>건 &nbsp; &nbsp; 
			적립 예정 포인트 합계 : <font color="red"><fmt:formatNumber value="${totalvalue }"/></font>
		</c:when>
		<c:otherwise>
			조회 목록 전체 : 총 <font color="red">0</font>건 &nbsp; &nbsp; 적립 예정 포인트 합계 : <font color="red">0</font>
		</c:otherwise>
   </c:choose>
   <br>
   <br>
   <!-- 매체 목록 -->
   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
            	<th>주문일자</th>
            	<th>적립기준일</th>
                <th>쇼핑몰</th>
                <th>주문번호</th>
                <th>상품명</th>
                <th>주문수량</th>
                <th>판매금액 합계</th>
                <th>포인트</th>
                <th>정산 처리 상태</th>
            </tr>            
			<c:choose>
				<c:when test="${(not empty list) and ('0' ne totalcount) }">
					<c:forEach var="item" items="${list}">
						<tr>
							<td align="center">
								<fmt:parseDate var="date1" value="${item.day}" pattern="yyyyMMdd"/>
								<fmt:parseDate var="date2" value="${item.time }" pattern="HHmmss"/>
								<fmt:formatDate value="${date1 }" pattern="yyyy.MM.dd"/>
								<br><fmt:formatDate value="${date2 }" pattern="HH:mm:ss"/>
							</td>
							<td align="center">
								<fmt:parseDate var="date1" value="${item.regdate}" pattern="yyyy-MM-dd HH:mm:ss"/>
								<fmt:formatDate value="${date1 }" pattern="yyyy.MM.dd"/>
								<br><fmt:formatDate value="${date1 }" pattern="HH:mm:ss"/>
							</td>
							<td align="center">${item.m_name }</td>
							<td align="center">${item.o_cd }</td>
							<td align="center">${item.p_nm }</td>
							<td align="center">${item.it_cnt }</td>
							<td align="center"><fmt:formatNumber value="${item.sales }"/>원</td>
							<td align="center"><fmt:formatNumber value="${item.commission }"/>P</td>
							<td align="center">${item.status}</td>
						</tr>					
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="7"> 조회값이 없습니다. </td>
					</tr>
				</c:otherwise>
			</c:choose>
        </tbody>
    </table>
    
    <table width="100%">
		<tbody>
			<tr height="45px">
				<td align="center">
					<c:out value="${boardPaging.pagingHTML}" escapeXml="false"></c:out>
				</td>
			</tr>
		</tbody>
    </table>
    
    <script>
    //페이징
    function boardPaging(pg){
    	location.href="<c:url value='/clip3_0/KTCPMW03.do?pg="+pg+"' />";
    }
    </script>
