<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){ 
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
        }); 

	</script>


<!--Content layout -->
<!-- [D] add -->
<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
    
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>
<script>
$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value="/resources/css/ui/img/calendar.gif"/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

 $(document).ready(function(){ 
     
     $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
     
     $("#datepicker3, #datepicker4").next().click(function () {
            
            $("#ui-datepicker-div > .ui-datepicker-calendar").hide();
            
            $("#ui-datepicker-div").position({
                my: "center top",
                at: "center bottom",
                of: $(this)
            });
        });
    
}); 
</script>
<!--// [D] add -->  


	<div class="top_title"><h2> 매장 바로 쓰기 내역 조회</h2></div>

    
    <form id="mainForm" name="mainForm">
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="">
            <col width="">
       
        </colgroup>
        <tbody>
            <tr>
                <th>검색조건</th>
                <td>
                    <select class="form_sel_style1" name="status" id="status">
                        <option value="" selected="">전체</option>
                        <option value="user_ci">user_ci</option>
                        <option value="cust_id">cust_id</option>
                        <option value="ga_id">ga_id</option>
                        <option value="ctn">ctn</option>
                    </select>
                    <input class="form_input_style2" id="keywd" name="keywd" style="width:200px" value="">
                </td>
            </tr>
            <tr>
                <th>겸색기간</th>
                <td>
                    <div id="datepickerDiv1">

                        <input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
                    ~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">                                
                   </div>
                </td>
            </tr>
            <tr>
                <th>사용여부</th>
                <td>
                    <input id="" name="use_status" value="Y" checked="CHECKED" type="radio"> 전체
                    <input id="" name="use_status" value="N" type="radio"> 승인  
                    <input id="" name="use_status" value="N" type="radio"> 취소 
                    <input id="" name="use_status" value="N" type="radio"> 실패                
                </td>
            </tr>
            <tr>
                <th>쿠폰 번호</th>
                <td>
                    <select class="form_sel_style1" name="" id="">
                        <option value="" selected="">전체</option>
                    </select>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="btn_area">
        <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색">
        <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드">
    </div>
    
    </form>
    
   <br>
   
   조회 목록 전체 : 총 <font color="red">56</font>건 &nbsp; &nbsp; 사용 포인트 합계 : <font color="red">2,000</font>
   <br>
   <br>
   <!-- 매체 목록 -->
   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
            	<th>처리일자</th>
                <th>매장</th>
                <th>거래번호</th>
                <th>쿠폰번호</th>
                <th>포인트</th>
                <th>사용여부</th>
                <th>사유</th>
            </tr>            
             <tr>
                <td align="center">2017.08.07 <br> 23:59:59</td>
                <td align="center">이디야</td>
                <td align="center">004093301580</td>
                <td align="center">004093301580</td>
                <td align="center">-3,000P</td>
                <td align="center">승인</td>
                <td align="center">기타오류</td>
            </tr>
             
            <tr>
                <td colspan="7"> 조회값이 없습니다. </td>
            </tr>
        </tbody>
    </table>
    
    <table width="100%"><tbody><tr height="45px"><td align="center"><span id="currentPageSpan" class="page_btn2" onclick="boardPaging(1)">1</span></td></tr></tbody></table>
   
    
    
    <script>
    //페이징
    function boardPaging(pg){
    	location.href="/clipcms/manage/acl/aclList.do?pg="+pg+"&keywd1=&keywd2=";
    }
    </script>

