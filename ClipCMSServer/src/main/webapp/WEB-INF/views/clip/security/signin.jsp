<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${sessionScope.AgentUserId ne null}">
    <%response.sendRedirect("/clipcms/main.do"); %>
</c:if> 
<style>
.centerline {margin:20px auto;height:0px; border-top:1px solid #ddd; border-bottom:1px solid #fff; width:280px;}
</style>
<!DOCTYPE html>
<html lang="ko">
     <head>
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
	 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <title>Clip CMS</title>
  	 <link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/common.css"/>
	 <link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/ui/jquery-ui.min.css"/>
	 <link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/login.css">
	 
     <!-- JQuery -->
	 <script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/jquery-1.11.3.min.js"></script>
	 <script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/ui/jquery-ui.min.js"></script>
     <!--  sha256 -->
	<script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/sha256.js"></script>
	 <style>
		


	 </style>
     </head>

	 <body>	
		<div id="top">
	
		<p class="add_text">Clip<span> CMS</span></p>
		<p class="logo"><img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/login_logo.png"/></p>
		</div>
		<form id="form" name="loginForm" id="loginForm" action="<c:out value='${pageContext.request.contextPath}'/>/signin_check.do" method="post">
		<div id="content">	
				 <section class="container">
				    <div class="login">	    
				        <p><input type="text" name="user_id" id="user_id" value="" placeholder="Username" onkeypress="if(event.keyCode == 13){ send(); return; }"></p>
				        <input type="password"  id="user_pwd" value="" placeholder="Password" onkeypress="if(event.keyCode == 13){ send(); return; }">
				        <p class="remember_me" style="margin-top:10px;margin-left:5px">
				          <label>
				            <input type="checkbox" name=idRemember id="idRemember">
				          	Remember me on this computer
				          </label>
				        </p>
				        <p class="button" style="margin-top:10px;margin-right:24px;"><input type="button" name="commit" value="Login" onclick="send()"></p>

				        <div class="centerline"></div>
					    <ul class="save">
					    	<img src="<c:out value='${pageContext.request.contextPath}'/>/captcha.do" id="captchaImg" alt="captcha img" width="210"><br>
							<input path="captcha" id="captcha" type="text" placeholder="보안문자 입력" name="captcha" style="width:170px;height:30px;cursor:text"  onkeypress="if(event.keyCode == 13){ send(); return; }"/>
							<a onclick="imgRefresh()" id="refreshBtn"><img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/reload.png"  style="cursor:pointer;width:23px"></a>
					    </ul>
				    </div>  
				  </section>
				  
		</div><!--content-->
		
		<input type="hidden" id="idRememberCk" name="idRememberCk" value="${idRememberCk}">
		<input type="hidden" id="password" name="password" value="">
	    <input type="hidden" id="loginFlag" name="loginFlag" value="${error}">
		<input type="hidden" id="rememberId" name="rememberId" value="${id}">
		<input type="hidden" id="agentUserCode" name="agentUserCode" value="${agentUserCode}">
		
		</form>
		<div id="bottom">
			<p class="company">Copyright 2016 kt corp. All Rights Reserved.</p>
		</div>
     </body>
<script>


function imgRefresh(){
    $("#captchaImg").attr("src", "${pageContext.request.contextPath}/captcha.do?id=" + Math.random());
}

$(document).ready(function(){
	if($("#loginFlag").val() == "PWBLOCK") {
		alert("5회 연속으로 비밀번호 입력 실패하였습니다. 관리자에게 문의하세요.");
		return;
	}
	
    if($("#loginFlag").val() == 'ERROR'){
    	//alert('${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}\n다시 확인해주시기 바랍니다.\n궁굼하신사항은 담당자에게 문의하세요 ');
    	alert('아이디 또는 패스워드를 다시 확인해주시기 바랍니다.\n궁굼하신사항은 담당자에게 문의하세요 ')
    }else if($("#loginFlag").val() == "IP") {
		alert("승인된 IP와 일치하지 않습니다.");
	} else if($("#loginFlag").val() == "STATUS") {
		alert("권한이 없습니다.");
	} else if($("#loginFlag").val() == "SESSION") {
		alert("세션이 종료되었습니다.");
	}else if($("#loginFlag").val() == "CAPTCHA") {
		alert("입력하신 보안문자가 다릅니다.");
	}else if($("#loginFlag").val() == "DUPL") {
		alert("다른장소에서 로그인 하였습니다.");
	}
    
	if($('#rememberId').val()!=''&&$('#rememberId').val()!=null){
		$('#user_id').val($('#rememberId').val());
		$('input:checkbox[id="idRemember"]').attr('checked','true');
	}
	
});

function send(){
	var idRemember = $('input:checkbox[id="idRemember"]').is(":checked");
	var user_pwd = $("#user_pwd").val();
	var varFrom = document.loginForm;
	var captcha = $("#captcha").val();
	
	var regExp = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[~,!,@,#,$,*,(,),=,+,_,.,|]).*$/;
	
	if (varFrom.user_id.value.length < 1 ) {
		alert("아이디 길이는 1보다커야 합니다.");
		varFrom.user_id.focus();
		return false;
	}

	if (SearchhasProhitWordWithAlert(varFrom.user_id.value)==true){
		varFrom.user_id.value="";
		varFrom.user_id.focus();
		return false;
	}

	if (varFrom.user_pwd.value.length < 8 || varFrom.user_pwd.value.length > 16) {
		alert("패스워드 길이는 8에서 16자 이하입니다.");
		varFrom.user_pwd.focus();
		return false;
	}

/* 	if (regExp.test(user_pwd)) {
		alert("입력하신 특수문자는 입력할 수 없습니다.");
		varFrom.user_pwd.focus();
		return false;
	} */
	if (SearchhasProhitWordWithAlert(varFrom.user_pwd.value)==true){
		varFrom.user_pwd.value="";
		varFrom.user_pwd.focus();
		return false;
	}
		
	if(captcha == "") {
		alert("보안문자를 입력하세요");
		return;
	} 
		
	$("#idRememberCk").val(idRemember);
	$("#password").val(SHA256(user_pwd));
	varFrom.submit();
	return false;
}

var arWord = new Array();
arWord[0] = "INSERT,DELETE,SHUTDOWN,SELECT,DROP,LIKE,--,UPDATE,XP_,새끼,개새끼,소새끼,병신,지랄,씨팔,십팔,니기미,찌랄,지랄,쌍년,쌍놈,빙신,좆까,니기미,좆같은게,잡놈,벼엉신,바보새끼,씹새끼,씨발,시벌,씨벌,떠그랄,좆밥,쉐이,등신,싸가지,미친놈,미친넘,죽습니다,씨밸넘,존나,슈킹,SHUOKING,ONMOUSE";
function SearchhasProhitWordWithAlert(str){
	  str = str.toUpperCase();  //  대문자로 변환
	  
	   words = arWord[0];
	   var roles= words.split(',');
	   for(j=0;j < roles.length;++j){
	    word = roles[j];
	    if(word.length>0 && str.indexOf(word) != -1) {
	     alert("'"+word+"'"+"는 금지어입니다.");
	     return true;
	    }
	   }
	  return false;
}
</script>
</html>