<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
	<title>Clip_CMS</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/jquery-1.11.3.min.js"></script>
	<script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/sha256.js"></script>
</head>
<body>
<form method="post" name="myform"  action="<c:url value='/pwChangeOk.do' />" >
	<table width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
		<tr bgcolor="#E6E6E6"> 
			<td width="130" height="23" bgcolor="#F3F5FC">◈ 비밀번호 변경 안내</td>
		</tr>
		<tr>
			<td>비밀번호 사용기간이 만료 되었습니다. 개인정보 보호를 위해<br> 3개월 단위로 비밀번호 변경을 하셔야 서비스 이용 가능합니다.</td>
		</tr>
		
	</table>
	<br>
	<table width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
		<tr>
			<th width="150px" height="30px" align="left">&nbsp;&nbsp;기존비밀번호</th>
			<td><input type="password" name="oldpwd" size="20" maxlength="20"></td>
		</tr>
		<tr>
			<th height="30px" align="left">&nbsp;&nbsp;새비밀번호</th>
			<td><input type="password" name="pwd" size="20" maxlength="20"></td>
		</tr>
		<tr>
			<th height="30px" align="left">&nbsp;&nbsp;새비밀번호 확인</th>
			<td><input type="password" name="repwd" size="20" maxlength="20"></td>
		</tr>
	</table>
	<br>
	<table width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#fff">
		<tr>
			<td  align="center">
				<input type="button" value="변경" onclick="sendit()">&nbsp; 
				<input type="button" value="취소" onclick="self.close()">
			
			</td>
		</tr>
	</table>
	<input type="hidden" name="agentUserCode" value="${code }" >
	<input type="hidden" name="agentUserId" value="${id }" >
	<input type="hidden" name="oldpassword" id="oldpassword" value="" >
	<input type="hidden" name="newpassword" id="newpassword" value="" >

</form>	
<script language="javascript">
$(document).ready(function(){
    if('${result}' == 'OLD_DIFF'){
    	alert('[${id}]계정 비밀번호변경 실패 \n기본비밀번호가 다릅니다. 다시 입력해 주세요 ');
    }else if('${result}' == 'NEW_SAME'){
    	alert('[${id}]이전과 동일한 비밀번호를 사용할 수 없습니다.\n다른 비밀번호를 입력해 주세요 ');
    }else if('${result}' == 'SUCESS'){
    	alert('[${id}]계정의 비밀번호가 성공적으로 변경되었습니다.\n다시 로그인해 주세요 ');
    	self.close();
    }
});

function sendit(){
	var oldpwd = myform.oldpwd.value.trim();
	var pwd = myform.pwd.value.trim();
	var repwd = myform.repwd.value.trim();
	
	var regExp = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[~,!,@,#,$,*,(,),=,+,_,.,|]).*$/;
	
	if (oldpwd == ""){
		alert("기존비밀번호를 입력하세요");
		myform.oldpwd.focus();
		return;
	}
	
	if (pwd == ""){
		alert("비밀번호를 입력하세요");
		myform.pwd.focus();
		return;
	}else if(!regExp.test(pwd)){
		 alert("비밀번호는 8자리 이상 알파벳과 숫자  특수문자를 혼합하여 입력하셔야 합니다.\n[허용특수문자:~,!,@,#,$,*,(,),=,+,_,.,|]");
		 myform.password.focus();
		 return;
	}else if(/(\w)\1\1\1/.test(pwd) || isContinuedValue(pwd)){
        alert("비밀번호에 4자 이상의 연속 또는 반복 문자 및 숫자를 사용하실 수 없습니다."); 
        myform.password.focus();
        return false;
    }else if(pwd.search(myform.agentUserId.value.trim())>-1) {
        alert("ID가 포함된 비밀번호는 사용하실 수 없습니다."); 
        myform.password.focus();
        return false;
    }else if(pwd!=repwd){
		alert("비밀번호를 확인해주세요");
		myform.repwd.focus();
		return;
	}
	
	$("#oldpassword").val(SHA256(oldpwd));
	$("#newpassword").val(SHA256(pwd));
	document.myform.submit(); 

}
//비밀번호 문자 포함 체크
function isContinuedValue(value) {
    console.log("value = " + value);
    var intCnt1 = 0;
    var intCnt2 = 0;
    var temp0 = "";
    var temp1 = "";
    var temp2 = "";
    var temp3 = "";

    for (var i = 0; i < value.length-3; i++) {
        console.log("=========================");
        temp0 = value.charAt(i);
        temp1 = value.charAt(i + 1);
        temp2 = value.charAt(i + 2);
        temp3 = value.charAt(i + 3);

        console.log(temp0)
        console.log(temp1)
        console.log(temp2)
        console.log(temp3)

        if (temp0.charCodeAt(0) - temp1.charCodeAt(0) == 1
                && temp1.charCodeAt(0) - temp2.charCodeAt(0) == 1
                && temp2.charCodeAt(0) - temp3.charCodeAt(0) == 1) {
            intCnt1 = intCnt1 + 1;
        }

        if (temp0.charCodeAt(0) - temp1.charCodeAt(0) == -1
                && temp1.charCodeAt(0) - temp2.charCodeAt(0) == -1
                && temp2.charCodeAt(0) - temp3.charCodeAt(0) == -1) {
            intCnt2 = intCnt2 + 1;
        }
        console.log("=========================");
    }

    console.log(intCnt1 > 0 || intCnt2 > 0);
    return (intCnt1 > 0 || intCnt2 > 0);
}
</script>

</body>
</html>