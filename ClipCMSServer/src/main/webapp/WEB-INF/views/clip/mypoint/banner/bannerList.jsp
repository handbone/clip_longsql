<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<style>
.dot1 {
    width: 160px;
    height: 20px;
 /* border: 1px solid red;  */
}
.dot2 {
    width: 315px;
    height: 20px;
   /*   border: 1px solid red;  */
}​
</style>
<script src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/jquery.dotdotdot.min.js" type="text/javascript"></script>
<script>

 $(document).ready(function(){ 
    $('.dot1').dotdotdot({ 
        ellipsis: '... ', 
        wrap: 'word', 
        after: null, 
        watch: true, 
        height: 20,
       
    }); 
    $('.dot2').dotdotdot({ 
        ellipsis: '... ', 
        wrap: 'word', 
        after: null, 
        watch: true, 
        height: 20,  
       
    }); 
    if('${status}'!='')
    $("#status").val('${status}').attr("selected", "selected");
  
}); 

</script>

	<div class="top_title"><h2> MY 포인트 배너 목록</h2></div>
    
    <table width="100%" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
    	   
        </colgroup>
        <tr>
        	<th></th>
            <td>
	            <a href="<c:url value='/mypoint/banner/addBannerForm.do' />" class="btn_area_b">신규등록</a>
           </td>
            
           
        </tr>

    </table>
    
    <br/>
   <!-- 배너 목록 -->
   <table class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="50px">
            <col width="150px">
            <col width="150px">
            <col width="250px">
            <col width="50px">
            <col width="100px">
            <col style="">
            <col width="50px">
            <col width="50px">
        </colgroup>
        <tr>
        	<th>idx</th>
            <th>배너명</th>
            <th>배너 이미지</th>
            <th>사용기간</th>
            <th>노출<br/>여부</th>
            <th>링크타입</th>
            <th>링크 URL</th>
            <th>android</th>
            <th>ios</th>
        </tr>
        <c:forEach var="bannerList" items="${list}">
        <tr>
        	<td>${bannerList.idx }</td>
            <td align="center">
            	<a href="<c:url value='/mypoint/banner/bannerView.do?idx=${bannerList.idx }&page=${pg}' />">
            		<span class="dot2" >${bannerList.bannername }</span>
            	</a>
           	</td>
            <td align="center"><span class="dot2" >${bannerList.bannerimage}</span></td>
            <td align="center">${bannerList.startdate} ~ ${bannerList.enddate}</td>
            <td align="center">
            <c:choose>
				 <c:when test="${bannerList.status == 0 }">
				  N
				 </c:when>
				 <c:when test="${bannerList.status == 1 }">
				  Y
				 </c:when>
			</c:choose>  
			</td>
            <td align="center">${bannerList.linktype}</td>
            <td align="center"><span class="dot2" >${bannerList.linkurl}</span></td>
            <td align="center">
            <c:choose>
					 <c:when test="${bannerList.androidgbn != 'Y' }">
					  N
					 </c:when>
					 <c:when test="${bannerList.androidgbn == 'Y' }">
					  Y
					 </c:when>
				</c:choose>  
				</td>
            <td align="center">
            	<c:choose>
					 <c:when test="${bannerList.iosgbn != 'Y' }">
					  N
					 </c:when>
					 <c:when test="${bannerList.iosgbn == 'Y' }">
					  Y
					 </c:when>
				</c:choose>  
				</td>
        </tr>
        </c:forEach>
        
        

    </table>
    
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML }</td></tr></table>
   
    
    
    <script>
    //페이징
    function boardPaging(pg){
    	location.href="<c:url value='/mypoint/banner/bannerList.do?pg="+pg+"&status=${status}' />";
    }
    </script>
    
    