<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

<div class="top_title"><h2>배너 - 상세보기</h2></div>

<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
   	<colgroup>
       	<col width="200">
           <col width="">
       </colgroup>
   	   <tr>
       	<th>IDX</th>
           <td>
           	${bannerBean.idx}
           </td>
       </tr>
       <tr>
       	<th>배너명</th>
           <td>
           		${bannerBean.bannername}
           </td>
       </tr>
       <tr>
       	<th>배너이미지</th>
           <td>
           		${bannerBean.bannerimage}
           </td>
       </tr>
       <tr>
       	<th>타입</th>
           <td>
           		${bannerBean.linktype}  
			</td>
       </tr> 
       <tr>
       	<th>링크 URL / 실행 SCLIPT </th>
           <td>
           		${bannerBean.linkurl}  
			</td>
       </tr> 			    
       <tr>
       	<th>유효기간</th>
           <td>
           	${bannerBean.startdate}
			~ ${bannerBean.enddate}		            
            
           </td>
       </tr>
       <tr>
       	<th>노출여부</th>
           <td>
           	 	<c:choose>
				 <c:when test="${bannerBean.status == 0 }">
				  N
				 </c:when>
				 <c:when test="${bannerBean.status == 1 }">
				  Y
				 </c:when>
			</c:choose>   
           </td>
       </tr>
       <tr>
       <th>android 선택</th>
           <td>
           		<c:choose>
					 <c:when test="${bannerBean.androidgbn != 'Y' }">
					  N
					 </c:when>
					 <c:when test="${bannerBean.androidgbn == 'Y' }">
					  Y
					 </c:when>
				</c:choose>   
           </td>
       </tr>
       <tr>
       <th>ios 선택</th>
           <td>
           		<c:choose>
					 <c:when test="${bannerBean.iosgbn != 'Y' }">
					  N
					 </c:when>
					 <c:when test="${bannerBean.iosgbn == 'Y' }">
					  Y
					 </c:when>
				</c:choose>  
           </td>
       </tr>
</table>

<div id="btn_area">
 	<input type="button" id="goList" name="goList" class="btn_area_a" value="목록" >
 	<input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="수정" >
 	<input type="button" id="goDelete" name="goDelete" class="btn_area_a" value="삭제" >
 	
</div>   

<%-- <div id="btn_area">
 	<a href="<c:url value='/notice/noticeList.do?pg=${pg }' />" class="btn_area_a">목록</a>
 	<c:if test="${sessionScope.AgentLevel ne '3' }">
    	<a href="<c:url value='/notice/noticeUpdateForm.do?num=${noticeDto.board_idx }&pg=${pg }' />" class="btn_area_a">수정</a>
    	<a href="#" class="btn_area_a" onclick="notice_delete()">삭제</a>
    </c:if>
</div> --%>


<form method="post" name="mainForm" id="mainForm" >
	<input type="hidden" id="idx" name="idx" value="${bannerBean.idx}" >
	<input type="hidden" id="page" name="page" value="${pg}" >
</form>

<script>
	
	 $(document).ready(function() {
		 
		 $("#goList").bind("click",function() {
			$('#mainForm').attr('action','<c:url value="/mypoint/banner/bannerList.do" />').attr('method', 'post').submit();
		 });
		 
		 $("#goUpdate").bind("click",function() {
			 $('#mainForm').attr('action','<c:url value="/mypoint/banner/bannerUpdateForm.do" />').attr('method', 'post').submit();
			});
		 
		 $("#goDelete").bind("click",function() {
			 if(confirm('정말 삭제하시겠습니까?')) {
				 $('#mainForm').attr('action','<c:url value="/mypoint/banner/bannerDelete.do" />').attr('method', 'post').submit();
			 }
			});
	});

</script>