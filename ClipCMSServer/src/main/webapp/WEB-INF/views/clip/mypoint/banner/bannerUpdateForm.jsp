<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>
<script src="<c:url value='/resources/js/jquery.validate.min.js' />"></script>



<script>


$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yy-mm-dd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

</script>

<div class="top_title"><h2>배너 - 수정</h2></div>
<form method="post" name="mainForm" id="mainForm" action="<c:url value='/mypoint/banner/bannerUpdate.do' />" >
<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
  
   	<colgroup>
       	<col width="200">
           <col width="">
       </colgroup>     
   	   <tr>
       	<th>IDX</th>
           <td>
           		${bannerBean.idx}
           </td>
       </tr>
       <tr>
       	<th>배너명</th>
           <td>
           		<input name="bannername" id="bannername" size="50" maxlength="50" type="text"> (100byte 이내)
           </td>
       </tr>
	   <tr>
       	<th>배너 이미지 파일명</th>
           <td>
           		<input name="bannerimage" id="bannerimage" size="70" maxlength="70" type="text"> (150byte 이내)
           </td>
       </tr>
       <tr>
       	<th>타입</th>
           <td>
       			<select name="linktype" id="linktype" onchange="javascript:changeText();">
			        <option value="http" selected>http</option>
				    <option value="script">script</option>
			    </select> 
			</td>
       </tr> 
	   <tr>
       	<th id="runText">링크 URL</th>
           <td>
           		<input name="linkurl" id="linkurl" size="100" maxlength="100" type="text"> (1024byte 이내)
           </td>
       </tr>
       <tr>			    
       <tr>
       	<th>유효기간</th>
           <td>
           	<input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">		            
            
           </td>
       </tr>
       <tr>
       	<th>노출여부</th>
           <td>
           	 	<select id="status" name="status">
			        <option value="1" selected>Y</option>
				    <option value="0">N</option>
			    </select> 
           </td>
       </tr>
       <tr>
       <th>android 선택</th>
           <td>
           		<input type="checkbox" id="androidgbn" name="androidgbn" value="Y">
           		체크시 안드로이드 기기에서 호출됩니다.
           </td>
       </tr>
       <tr>
       <th>ios 선택</th>
           <td>
           		<input type="checkbox" id="iosdgbn" name="iosgbn" value="Y">
           		체크시 iOS 기기에서 호출됩니다.
           </td>
       </tr>
</table>

<div id="btn_area">
 	<input type="button" class="btn_area_a" value="취소" onclick="javascript:history.go(-1)">
 	<input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="확인" >
 	
 	<input type="hidden" id="startdate" name="startdate" >
 	<input type="hidden" id="enddate" name="enddate" >
 	<input type="hidden" id="idx" name="idx" value="${bannerBean.idx}" >
	<input type="hidden" id="page" name="page" value="${pg}" >
 	
 	
</div>   
</form>

<style type="text/css">
input.error, textarea.error{
  border:1px dashed red;
}
label.error{
  margin-left:10px;
  color:red;
}
</style>

<script>
	
	 $(document).ready(function() {
		 $("#goUpdate").bind("click",function() {
			 $("#mainForm").submit();
		});
		 
		$("#bannername").val('${bannerBean.bannername}');
		$("#bannerimage").val('${bannerBean.bannerimage}');
		 
		$("#linktype").val('${bannerBean.linktype}').attr("selected", "selected");
		 
		$("#linkurl").val('${bannerBean.linkurl}');
		 
		$("#datepicker1").val('${bannerBean.startdate}');
		$("#datepicker2").val('${bannerBean.enddate}');
		$("#startdate").val('${bannerBean.startdate}');
		$("#enddate").val('${bannerBean.enddate}');
		$("#status").val('${bannerBean.status}').attr("selected", "selected");
		 
		 if('${bannerBean.androidgbn}' == 'Y'){
			 $("input:checkbox[name='androidgbn']").attr("checked", true );
		 }
		 if('${bannerBean.iosgbn}' == 'Y'){
			 $("input:checkbox[name='iosgbn']").attr("checked", true );
		 }
		 
		changeText();
	});
 	
	 
		var changeText = function(){
			var type = $("select[name=linktype] option:selected").text();
			$("#runText").empty();
			var html = "";
			if(type == "http"){
				html = "링크 URL";
			}else{
				html = "실행 SCLIPT";
			}
			$("#runText").append(html);
		}

	
	$("#mainForm").validate({
		rules: {
			bannername: { required: true,	maxlength: 100},
			bannerimage: { required: true,	maxlength: 150},
			linkurl: { required: true,	maxlength: 1024},
			datepicker1: "required",
			datepicker2: "required",
		},
		messages: {
			
		},submitHandler: function(form) {
			
			var endDay = $("#datepicker2").val() ;
			var startDay = $("#datepicker1").val() ;
			
			if(endDay < startDay){
				alert("시작일이 종료일보다 빠를수 없습니다.");
				return;
			}
			
			$("#startdate").val(startDay);
			$("#enddate").val(endDay);
			
		    form.submit();
		}
	});

</script>
