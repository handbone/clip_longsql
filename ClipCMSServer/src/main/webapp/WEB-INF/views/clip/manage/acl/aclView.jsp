<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@	taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/jquery.validate.min.js' />"></script>

<div class="top_title"><h2>매체관리 - 상세보기</h2></div>

<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
  
   	<colgroup>
       		<col width="200">
            <col width="">
       </colgroup>
   	   <tr>
       	<th>매체명</th>
           <td>
           		${aclInfoTbl.requester_name}
           </td>
       </tr>
       <tr>
       	<th>매체코드</th>
           <td>
           		${aclInfoTbl.requester_code}
           </td>
       </tr>
       <tr>
       	<th>서비스 상태</th>
           <td>
       			${aclInfoTbl.use_status}
			</td>
       </tr> 
			    
       <tr>
       	<th>IP</th>
           <td>
           		${aclInfoTbl.ip_addr}
           </td>
       </tr>
        <tr>
       	<th>적립 제한 설정</th>
           <td>
       			${aclInfoTbl.acl_limit_yn}
			</td>
       </tr> 
        <tr>
       	<th>적립 허용 횟수</th>
           <td>
           		${aclInfoTbl.acl_limit_cnt}
           </td>
       </tr>
       <!-- [D] add -->
       <tr>
       	<th>사용 기간</th>
       <td>
       		<fmt:parseDate value="${aclInfoTbl.datepicker1}" var="noticePostDate1" pattern="yyyyMMdd"/>
			<fmt:parseDate value="${aclInfoTbl.datepicker2}" var="noticePostDate2" pattern="yyyyMMdd"/>
       		<fmt:formatDate value="${noticePostDate1}" pattern="yyyy.MM.dd"/> ~ <fmt:formatDate value="${noticePostDate2}" pattern="yyyy.MM.dd"/>
       </td>
       </tr>
       <!--// [D] add -->
       <tr>
       	<th>비고</th>
           <td>
	           	<textarea id="description" name="description" rows="10" cols="100" readOnly="readOnly"> ${aclInfoTbl.description } </textarea> 
           </td>
       </tr>
</table>

<div id="btn_area">
 	<input type="button" id="goList" name="goList" class="btn_area_a" value="목록" >
 	<input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="수정" >
 	<input type="button" id="goDelete" name="goDelete" class="btn_area_a" value="삭제" >
 	
</div>   



<form method="post" name="mainForm" id="mainForm" >
	<input type="hidden" id="requester_code" name="requester_code" value="${aclInfoTbl.requester_code}" >
	<input type="hidden" id="page" name="page" value="${pg}" >
</form>

<script>
	
	 $(document).ready(function() {
		 
		 $("#goList").bind("click",function() {
			$('#mainForm').attr('action','<c:url value="/manage/acl/aclList.do" />').attr('method', 'post').submit();
		 });
		 
		 $("#goUpdate").bind("click",function() {
			 $('#mainForm').attr('action','<c:url value="/manage/acl/aclUpdateForm.do" />').attr('method', 'post').submit();
			});
		 
		 $("#goDelete").bind("click",function() {
			 if(confirm('정말 삭제하시겠습니까?')) {
				 $('#mainForm').attr('action','<c:url value="/manage/acl/aclDelete.do" />').attr('method', 'post').submit();
			 }
			});
	});

</script>