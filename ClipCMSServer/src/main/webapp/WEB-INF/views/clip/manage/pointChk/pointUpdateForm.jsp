<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

<script src="<c:url value='/resources/js/jquery.validate.min.js' />"></script>

<script>
 $(document).ready(function(){ 

	 $("#ctnChk").bind("click", function(){
		 
		if( $("#ctn").val() == "" || $("#ctn").val() == null ){
			alert("조정대상 CTN을 입력하세요.");
			return;
		}
		
		$("#ctnChkDiv").val("POINT_HIST_TBL");
		
		window.open("", "popup", "width=700px, height=800px, left=100px, top=100px");
		document.mainForm.target = "popup";
		document.mainForm.method = "POST";
		document.mainForm.action = "<c:url value='/manage/pointChk/ctnChkPopup.do'/>";
		document.mainForm.submit();
		document.mainForm.target = "";
		
		
		 /* formData = $("#mainForm").serialize();
		 $.ajax({
	            type: "POST",
	            url: "../pointChk/ctnChk.do",
	            data: formData,
	            dataType: "Json",
	            success: function (resData) {
	                console.log(resData.result);  
	                if (resData.result == "success") {
	                	alert("가입 된 사용자 입니다.");
	                	$("#ctnChkValue").val("Y");
	                	$("#ctnChkDiv").val("POINT_HIST_TBL");
	                	$("#ctn2").val("");
	                }else if (resData.result == "One More") {
	                	alert("하나의 ctn에 서로 다른 user_ci가 검색되고 있습니다.");
	                	$("#ctnChkValue").val("N");
	                	$("#ctnChkDiv").val("POINT_HIST_TBL");
	                	$("#ctn").val("");
	                	$("#ctn2").val("");
	                } else {
	                	alert("가입되지 않은 사용자입니다.");
	                	$("#ctnChkValue").val("N");
	                	$("#ctnChkDiv").val("POINT_HIST_TBL");
	                	$("#ctn").val("");
	                	$("#ctn2").val("");
	                }
	            },
	            error: function (e) {
	            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
	            }
	        }); */
	 });
	 
	
	 $("#ctnChk2").bind("click", function(){
		 
			if( $("#ctn2").val() == "" || $("#ctn2").val() == null ){
				alert("조정대상 CTN을 입력하세요.");
				return;
			}
				
			$("#ctnChkDiv").val("USER_INFO_TBL");
			
			window.open("", "popup", "width=700px, height=800px, left=100px, top=100px");
			document.mainForm.target = "popup";
			document.mainForm.method = "POST";
			document.mainForm.action = "<c:url value='/manage/pointChk/ctnChk2Popup.do'/>";
			document.mainForm.submit();
			document.mainForm.target = "";
			
			
			 /* formData = $("#mainForm").serialize();
			 $.ajax({
		            type: "POST",
		            url: "../pointChk/ctnChk2.do",
		            data: formData,
		            dataType: "Json",
		            success: function (resData) {
		                console.log(resData.result);  
		                if (resData.result == "success") {
		                	alert("가입 된 사용자 입니다.");
		                	$("#ctnChkValue").val("Y");
		                	$("#ctnChkDiv").val("USER_INFO_TBL");
		                	$("#ctn").val($("#ctn2").val());
		                }else if (resData.result == "One More") {
		                	alert("하나의 ctn에 서로 다른 user_ci가 검색되고 있습니다.");
		                	$("#ctnChkValue").val("N");
		                	$("#ctnChkDiv").val("USER_INFO_TBL");
		                	$("#ctn").val("");
		                	$("#ctn2").val("");
		                } else {
		                	alert("가입되지 않은 사용자입니다.");
		                	$("#ctnChkValue").val("N");
		                	$("#ctnChkDiv").val("USER_INFO_TBL");
		                	$("#ctn").val("");
		                	$("#ctn2").val("");
		                }
		            },
		            error: function (e) {
		            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
		            }
		        }); */
		 });
	
	
	 
 }); 
 
 function returnUserCi(selectedUserCi){
	$("#user_ci").val(selectedUserCi);
    $("#ctnChkValue").val("Y");
  	
    if("POINT_HIST_TBL"==$("#ctnChkDiv").val()){
    	$("#ctn2").val("")
    }else { /*$("#ctnChkDiv").val("USER_INFO_TBL");*/
    	$("#ctn").val($("#ctn2").val());
    }
    
   }
 
</script>

<div class="top_title"><h2>포인트 조정</h2></div>
<form method="post" name="mainForm" id="mainForm" >
<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
  
   	<colgroup>
       		<col width="200">
            <col width="">
       </colgroup>
   	   <tr>
       	<th>조정대상 CTN 검색
       	<br><br>* 일반적인 경우에 사용가능
       	<br> ( 하나의 ctn을 한명의 user가 사용했을 경우)
       	<br>* 하나의 ctn을 여러 user가 사용했을 경우에도 사용가능</th></th>
           <td>
           		<input id="ctn2" name="ctn2" size="50" maxlength="20" type="text"> (20byte 이내) <input type="button" id="ctnChk2" name="ctnChk2" class="btn_area_a" value="사용자조회" >
           <br>* 현재 고객정보의 전화번호를 입력하세요. 
           </td>
       </tr>
       <tr>
       	<th>조정대상 CTN 검색
       	<br><br>* 한명의 user가 여러 ctn을 사용했을 경우</th>
           <td>
           		<input id="ctn" name="ctn" size="50" maxlength="20" type="text"> (20byte 이내) <input type="button" id="ctnChk" name="ctnChk" class="btn_area_a" value="사용자조회" >
           <br>* 과거에 적립된 기록이 있는 전화번호를 입력하세요.
           <br>* 과거의 모든 적립기록을 찾아보기때문에 검색시간이 1~2분 걸릴 수 있습니다.
           </td>
       </tr>
       <tr>
       	<th>선택 된 user_ci
       	   <td>
           		<input id="user_ci" name="user_ci" size="115" maxlength="256" type="text" readonly="readonly"> 
           </td>
       </tr>
       <tr>
       	<th>조정유형</th>
           <td>
           		<select class="form_sel_style1" name="point_type" id="point_type">
           			<option value="" selected>선택</option>
            		<option value="I" >적립</option>
                	<option value="O" >사용</option>
            	</select>
           </td>
       </tr>
       <tr>
       		<th>조정 포인트 금액</th>
           	<td>
           		<input id="point_value" name="point_value" size="50" maxlength="50" type="text">
           		<br>* '적립'과 '사용' 관계없이 양의 정수로 입력하세요. ( '500포인트' '사용'의 경우 : 500 을 입력합니다. )
           		<br>* 200만원을 초과하여 입력할 수 없습니다.
			</td>
       </tr>
       <tr>
       		<th>조정 사유</th>
           	<td>
           		<input id="description" name="description" size="50" maxlength="500" type="text"> (500byte 이내)
           		<br>
           		* 실제 사용자에게 보여지는 메세지 입니다.
			</td>
       </tr> 
		<tr>
       		<th>message</th>
           	<td>
           		<textarea id="message" name="message" rows="15" cols="100"></textarea>
           		(2056byte 이내)
           		<br>
           		* 사용자에게 보여지지 않습니다. 
			</td>
       </tr>
		
</table>

<div id="btn_area">
 	<input type="button" class="btn_area_a" value="취소" onclick="javascript:history.go(-1)">
 	<input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="조정" >

	<input type="hidden" id="ctnChkValue" name="ctnChkValue" value="N">
	<input type="hidden" id="ctnChkDiv" name="ctnChkDiv" value="">
	
</div>   
</form>

<style type="text/css">
input.error, textarea.error{
  border:1px dashed red;
}
label.error{
  margin-left:10px;
  color:red;
}
</style>

<script>
	
	 $(document).ready(function() {
		 $("#goUpdate").bind("click",function() {
			 $("#mainForm").submit();
		});
	});
 	

	
	$("#mainForm").validate({
		rules: {
			ctn: { required: true,	maxlength: 20},
			point_value: { required: true,	maxlength: 50, max:2000000},
			point_type: { required: true},
			description: { required: true,	maxlength: 500}
		},
		messages: {
			
		},submitHandler: function(form) {
			
			if(confirm("포인트를 조정하시겠습니까?")){
				var ctnChkValue = $("#ctnChkValue").val() ;
				
				if( ctnChkValue != "Y" ){
					alert("사용자 조회를 해주세요.");
					return;
				}
				
				
				
				 formData = $("#mainForm").serialize();
				 $.ajax({
			            type: "POST",
			            url: "../pointChk/pointUpdate.do",
			            data: formData,
			            dataType: "Json",
			            success: function (resData) {
			                alert("포인트 조정이 완료되었습니다.");
			                $(location).attr('href','<c:url value="/manage/pointChk/pointUpdateHistory.do" />'); 
			            },
			            error: function (e) {
			            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
			            }
			        });
			}
			
			
		}
	});

</script>