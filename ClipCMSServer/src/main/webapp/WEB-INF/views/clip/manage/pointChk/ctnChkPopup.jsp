<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>

 $(document).ready(function(){ 
	  
	$(".table_style1 #selectUserCi").bind("click",function() {
			opener.returnUserCi($(this).parent().parent().find("td:eq(0)").text());
			self.close();
	});
	
}); 

</script>

	
    <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
    	<col width="380px">
    	<col width="220px">
    	<col width="">
        </colgroup>
        <tr>
        	<th>User CI</th>
        	<th>ctn</th>
        	<th>선택</th>
        </tr>
        
        <c:forEach var="list" items="${list}" varStatus="status">
        <tr>
            <td align="left" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;" >${list.user_ci}</td>
            <td align="left" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;" >${list.ctn}</td>
            <td ><input type="button" id="selectUserCi" name="selectUserCi" class="btn_area_a" value="선택" ></td>
        </tr>
        </c:forEach>
        <c:if test="${fn:length(list) == 0}" >
        <tr>
        	<td colspan="2" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
    </table>
    
    <form id="mainForm" name="mainForm" >
    
    <div id="btn_area">
    <input type="hidden" id="user_ci" name="user_ci" value="" >
	</div>
	
    </form>
    
    
    
    