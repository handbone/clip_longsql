<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>

 $(document).ready(function(){ 
	  
	$('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	
	$(".table_style1 #convertCtn").unbind().bind("click",function() {
		
		$("#stringData").val($(this).parent().text().trim());
		 formData = $("#mainForm").serialize();
	 	 $.ajax({	
	            type: "POST",
	            url: "<c:url value='/decrypto.do'/>",
	            data: formData,
	            dataType: "Json",
	            success: function (resData) {
	                  alert(resData.ctn);
	            },
	            error: function (e) {
	            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
	            }
	            
	        }); 
	 });
	
}); 

</script>

	
    <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
    	<col width="">
    	<col width="">
    	<col width="">
    	<col width="">
    	<col width="">
        </colgroup>
        <tr>
        	<th>매체</th>
        	<th>날짜 : 시간</th>
        	<th>설명</th>
        	<th>ctn</th>
        	<th>포인트</th>
        </tr>
        
        <c:forEach var="list" items="${list}" varStatus="status">
        <tr>
            <td align="center">${list.media}</td>
            <td align="center"><%-- ${list.stat_date} :  --%>
            ${fn:substring(list.reg_time , 0 , 2)}:${fn:substring(list.reg_time , 2 , 4)}:${fn:substring(list.reg_time , 4 , 6)}</td>
            <td align="center" >${list.description}</td>
       		<td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;" >
       			<span title="${list.ctn}"></span>
       			<img id="convertCtn" src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/icon_title.png"> ${list.ctn}</td>
			<td align="right"> ${list.point} P</td>
		</tr>
        </c:forEach>
        <c:if test="${fn:length(list) == 0}" >
        <tr>
        	<td colspan="5" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
    </table>
    
    <form id="mainForm" name="mainForm" >
    
    <div id="btn_area">
    <input type="hidden" id="startdate" name="startdate" value="" >
    <input type="hidden" id="enddate" name="enddate" value="" >
    <input type="hidden" id="user_ci" name="user_ci" value="" >
    <input type="hidden" id="stringData" name="stringData" value="" >
    <input type="hidden" id="stat_hour" name="stat_hour" value="" >
    <input type="hidden" id="statusNum" name="statusNum" value="" >
	<input type="hidden" id="selectdate" name="selectdate" value="" >
	<input type="hidden" id="point_type" name="point_type" value="" >
	<input type="hidden" id="pg" name="pg" value="" >
	</div>
	
    </form>
    
  
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML}</td></tr></table>
    <script>
    //페이징
    function boardPaging(pg){
    	
    	$("#pg").val(pg);
    	$("#excelDownload").val("");
    	 
    	$("#selectdate").val('${searchBean.selectdate}');
    	$("#stat_hour").val('${searchBean.stat_hour}');
    	$("#startdate").val('${searchBean.startdate}');
		$("#enddate").val('${searchBean.enddate}');
		$("#point_type").val('${searchBean.point_type}');	
		$("#user_ci").val('${searchBean.user_ci}');	
		
		$('#mainForm').attr('action','<c:url value="/manage/pointChk/pointChkPopup.do" />').attr('method', 'post').submit();

    }
    </script>
    
    
    
    
    