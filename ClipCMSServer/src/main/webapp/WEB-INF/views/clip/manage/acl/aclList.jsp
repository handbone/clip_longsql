<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
.dot1 {
    width: 160px;
    height: 20px;
 /* border: 1px solid red;  */
}
.dot2 {
    width: 315px;
    height: 20px;
   /*   border: 1px solid red;  */
}​
</style>

<script src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/jquery.dotdotdot.min.js" type="text/javascript"></script>
<script>

 $(document).ready(function(){ 
    
	 $('.dot1').dotdotdot({ 
        ellipsis: '... ', 
        wrap: 'word', 
        after: null, 
        watch: true, 
        height: 20,
    }); 
    $('.dot2').dotdotdot({ 
        ellipsis: '... ', 
        wrap: 'word', 
        after: null, 
        watch: true, 
        height: 20,  
       
    }); 
    if('${status}'!='')
    $("#status").val('${status}').attr("selected", "selected");
  
    
 	// 검색
	 $("#goSearch").bind("click",function() {
		 $('#mainForm').attr('action','<c:url value="/manage/acl/aclList.do" />').attr('method', 'post').submit();
	 });
 	
 	// 검색값 설정
	 if( '' != '${use_status}'){
		$("input[name=use_status][value=" + "${use_status}" + "]").attr("checked", true);
	 }
 	
}); 

</script>

	<div class="top_title"><h2> 매체 목록</h2></div>
    
    <table width="100%" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
        </colgroup>
        <tr>
        	<th></th>
            <td>
	            <a href="<c:url value='/manage/acl/aclInsertForm.do' />" class="btn_area_b">신규등록</a>
           </td>
        </tr>
    </table>
    <br>
    
    <form id="mainForm" name="mainForm" >
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
        <tr>
        	<th>매체명</th>
        	<td>
            	<input class="form_input_style2" id="keywd1" name="keywd1" style="width:200px" value="${keywd1}">
            </td>
        </tr>
        <tr>
        	<th>매체코드</th>
        	<td>
            	<input class="form_input_style2" id="keywd2" name="keywd2" style="width:200px" value="${keywd2}">
            </td>
        </tr>
		<tr>
       	<th>서비스 상태</th>
       	<td>
           		<input id="use_status" name="use_status" value="Y" checked="CHECKED" type="radio" /> 사용중
          		 <input id="use_status" name="use_status" value="N" type="radio"/> 중지	            
           </td>
       </tr>
       <tr></tr>
    </table>
    <div id="btn_area">
    <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색" >
    </div>
    
    </form>
    
    
    
    
    
    
    
   <br>
   
   총 <font color="red">${aclListCng }</font> 개의 매체가 검색되었습니다.
   <br>
   <br>
   <!-- 매체 목록 -->
   <table class="table_style1" cellpadding="0" cellspacing="0"  style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tr>
        	<th>매체명</th>
            <th>매체 코드</th>
            <th>상태</th>
            <th>설명</th>
            <th>적립 제한</th>
            <th>적립 허용 횟수</th>
            <th>사용 기간</th>
            <!-- <th>사용기한</th> -->
        </tr>
        <c:forEach var="aclInfoTbl" items="${list}">
		<fmt:parseDate value="${aclInfoTbl.datepicker1}" var="noticePostDate1" pattern="yyyyMMdd"/>
		<fmt:parseDate value="${aclInfoTbl.datepicker2}" var="noticePostDate2" pattern="yyyyMMdd"/>
        <tr>
        	<td>${aclInfoTbl.requester_name }</td>
            <td align="center"><a href="<c:url value='/manage/acl/aclView.do?requester_code=${aclInfoTbl.requester_code }&page=${pg}' />"><span class="dot2" >${aclInfoTbl.requester_code }</span></a></td>
            <td align="center">${aclInfoTbl.use_status}</td>
            <td align="center"  style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">${aclInfoTbl.description}</td>
            <td align="center">${aclInfoTbl.acl_limit_yn}</td>
            <td align="center">${aclInfoTbl.acl_limit_cnt}</td>
            <td align="center">
            	<fmt:parseDate value="${aclInfoTbl.datepicker1}" var="noticePostDate1" pattern="yyyyMMdd"/>
				<fmt:parseDate value="${aclInfoTbl.datepicker2}" var="noticePostDate2" pattern="yyyyMMdd"/>
	       		<fmt:formatDate value="${noticePostDate1}" pattern="yyyy.MM.dd"/> ~ <fmt:formatDate value="${noticePostDate2}" pattern="yyyy.MM.dd"/>
            </td>
        </tr>
        </c:forEach>
    </table>
    
    <table width="100%">
		<tbody>
			<tr height="45px">
				<td align="center"><c:out value="${boardPaging.pagingHTML}"
						escapeXml="false"></c:out></td>
			</tr>
		</tbody>
	</table>
   
    
    
    <script>
	//페이징
	function boardPaging(pg) {
		var strHtml = '<input type="hidden" name="pg" value="' + pg + '"/>';
		$('#mainForm').append(strHtml);
		$('#mainForm').submit();
	}
    </script>