<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>


$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

 $(document).ready(function(){ 
	 
	 $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	 
	// 검색
	 $("#goSearch").bind("click",function() {
		 $("#excelDownload").val("");
		 formSubmit();
	 });
	 
	 // 엑셀 다운로드
	 $("#goExcel").bind("click",function() {
		 formSubmit("goExcel");
	 });
	 
	  $(".table_style1 #goUpdate").bind("click",function() {
		 
		 if(confirm('정말 수정하시겠습니까?')) {
			 
			 $("#area01").val($(this).parent().parent().find("td:eq(2) > input").val());
			 $("#area02").val($(this).parent().parent().find("td:eq(3) > input").val());
			 $("#area03").val($(this).parent().parent().find("td:eq(4) > input").val());
			 $("#area04").val($(this).parent().parent().find("td:eq(5) > input").val());
			 $("#area05").val($(this).parent().parent().find("td:eq(6) > input").val());
			 $("#area06").val($(this).parent().parent().find("td:eq(7) > input:eq(0)").val());
			 $("#idx").val($(this).parent().parent().find("td:eq(7) > input:eq(1)").val());
			 
			 formData = $("#mainForm").serialize();
			 $.ajax({
		            type: "POST",
		            url: "../viewLog/dailyJoinSumUpdate.do",
		            data: formData,
		            dataType: "Json",
		            success: function (resData) {
		                console.log(resData.result);  
		                if (resData.result == "success") {
		                	alert("수정 되었습니다.");
		                	location.reload();  		                	
		                } else {
		                	alert("수정 된 행이 없습니다!");
		                }

		            },
		            error: function (e) {
		            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
		            }
		        });
		 }
	 });
	 
	 function formSubmit(goExcelString){
		 
		 if( goExcelString == "goExcel"){
			 $("#excelDownload").val("Y");
		 }
		 
			 $("#startdate").val($("#datepicker1").val());
			 $("#enddate").val($("#datepicker2").val());
			 
			 if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
			 if($("#enddate").val() == ""){
			 	alert("종료일을 입력해주세요.");
				return;
			 }
			 
			 if($("#enddate").val() < $("#startdate").val()){
			 	alert("시작일이 종료일보다 빠를수 없습니다.");
				return;
			 }
				
		 $('#mainForm').attr('action','<c:url value="/roulette/viewLog/dailyJoinSum.do" />').attr('method', 'post').submit();
	 }
	 
	 
	// 검색값 설정
    if('${searchBean.roulette_id}'!=''){
  		$("#roulette_id").val('${searchBean.roulette_id}').attr("selected", "selected");
    }
 	// 검색값 설정
    $("#datepicker1").val('${searchBean.startdate}');
    $("#datepicker2").val('${searchBean.enddate}');

    if( '' == $("#datepicker1").val()){
		$("#datepicker1").val($.datepicker.formatDate('yymmdd', new Date()));
	}
	if( '' == $("#datepicker2").val()){
		$("#datepicker2").val($.datepicker.formatDate('yymmdd', new Date()));
	}
	
	
}); 

</script>

	<div class="top_title"><h2> [로그] 일자별 당첨자합계 로그</h2></div>
    <!-- 검색조건 -->
    <form id="mainForm" name="mainForm" >
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
        <tr>
        	<th>검색조건</th>
            <td>
            <select class="form_sel_style1" name="roulette_id" id="roulette_id">
            	<option value="">전체</option>
             		<c:forEach items="${channelNameList}" var="channelNameList" varStatus="status">
		 				<option value="${channelNameList.roulette_id}" >${channelNameList.channelname}</option>
			 		</c:forEach>
			 </select>
            </td>
        </tr>
		<tr>
       	<th>검색기간</th>
           <td>
           	<input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">		            
           </td>
       </tr>
       <tr></tr>
    </table>
    <div id="btn_area">
    <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색" >
    <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드" >
    </div>
    <input type="hidden" id="startdate" name="startdate" value="" >
    <input type="hidden" id="enddate" name="enddate" value="" >
    <input type="hidden" id="excelDownload" name="excelDownload" value="" >
    <input type="hidden" id="pg" name="pg" value="" >
    
   	<input type="hidden" id="idx" name="idx" value="" >
     <input type="hidden" id="area01" name="area01" value="" >
     <input type="hidden" id="area02" name="area02" value="" >
     <input type="hidden" id="area03" name="area03" value="" >
     <input type="hidden" id="area04" name="area04" value="" >
     <input type="hidden" id="area05" name="area05" value="" >
     <input type="hidden" id="area06" name="area06" value="" >
     
     
    </form>
    
    
   <br>

    <table class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
    		<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tr>
        	<th>채널명</th>
            <th>날짜</th>
            <th>경품명 1</th>
            <th>경품명 2</th>
            <th>경품명 3</th>
            <th>경품명 4</th>
            <th>경품명 5</th>
            <th>경품명 6</th>
            <th>수정</th>
        </tr>
        
        <c:forEach var="list" items="${list}">
        <tr>
        	<td align="center">${list.channelname}</td>
            <td align="center">${list.sdate}</td>
            <td align="center"><input type="text" value="${list.area01}"  maxlength="10" style="text-align:right; width:80px"></td>
            <td align="center"><input type="text" value="${list.area02}"  maxlength="10" style="text-align:right; width:80px"></td>
            <td align="center"><input type="text" value="${list.area03}"  maxlength="10" style="text-align:right; width:80px"></td>
            <td align="center"><input type="text" value="${list.area04}"  maxlength="10" style="text-align:right; width:80px"></td>
            <td align="center"><input type="text" value="${list.area05}"  maxlength="10" style="text-align:right; width:80px"></td>
            <td align="center"><input type="text" value="${list.area06}"  maxlength="10" style="text-align:right; width:80px">
            					<input type="hidden" value="${list.idx}"  ></td>
            <td align="center"><input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="수정" ></td>
        </tr>
        </c:forEach>
        <c:if test="${fn:length(list) == 0}" >
        <tr>
        	<td colspan="9" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
        
        
    </table>
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML}</td></tr></table>
    <script>
    //페이징
    function boardPaging(pg){
    	
    	 $("#pg").val(pg);
    	 $("#excelDownload").val("");
    	 
    	 $("#startdate").val($("#datepicker1").val());
		 $("#enddate").val($("#datepicker2").val());
		 
		 if($("#startdate").val() == ""){
			 	alert("시작일을 입력해주세요.");
				return;
			 }
		 if($("#enddate").val() == ""){
		 	alert("종료일을 입력해주세요.");
			return;
		 }
		 
		 if($("#enddate").val() < $("#startdate").val()){
		 	alert("시작일이 종료일보다 빠를수 없습니다.");
			return;
		 }
			
	 $('#mainForm').attr('action','<c:url value="/roulette/viewLog/dailyJoinSum.do" />').attr('method', 'post').submit();

    }
    </script>