<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

<div class="top_title"><h2>채널 - 상세보기</h2></div>

<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
   	<colgroup>
       	<col width="200">
           <col width="">
       </colgroup>
   	   <tr>
       	<th>ROULETTE ID</th>
           <td>
           	${pointRoulette.roulette_id}
           </td>
       </tr>
       <tr>
       	<th>채널명</th>
           <td>
           		${pointRoulette.channelname}
           </td>
       </tr>
       <tr>
       	<th>Requester Code</th>
           <td>
           		${pointRoulette.requester_code}
           </td>
       </tr>
       <tr>
       	<th>코드 설명</th>
           <td>
           		${pointRoulette.requester_description}
           </td>
       </tr>
       <tr>
       	<th>타입</th>
           <td>
           <c:choose>
				 <c:when test="${pointRoulette.routype == 0 }">
				  CLIP
				 </c:when>
				 <c:when test="${pointRoulette.routype == 1 }">
				  외부 APP
				 </c:when>
				 <c:when test="${pointRoulette.routype == 2 }">
				  외부 WEB
				 </c:when>
			</c:choose>  
			</td>
       </tr> 
			    
       <tr>
       	<th>유효기간</th>
           <td>
           	${pointRoulette.startdate}
			~ ${pointRoulette.enddate}		            
            
           </td>
       </tr>
       <tr>
       	<th>노출여부</th>
           <td>
           	 	<c:choose>
				 <c:when test="${pointRoulette.status == 0 }">
				  N
				 </c:when>
				 <c:when test="${pointRoulette.status == 1 }">
				  Y
				 </c:when>
			</c:choose>   
           </td>
       </tr>
       <tr>
       	<th>이미지 구분값</th>
           <td>
           		${pointRoulette.imagepath}
           </td>
       </tr>
       <tr>
       	<th>룰렛 설정<br>
       	<br>
       	<br>경품 갯수는 6개
       	<br>룰렛 바늘 기준으로
       	<br>시계반대방향으로
       	<br>1번부터 시작
       	<br>1번경품 - 11시방향
       	<br>2번경품 - 9시방향
       	<br>3번경품 - 7시방향
       	<br>4번경품 - 5시방향
       	<br>5번경품 - 3시방향
       	<br>6번경품 - 1시방향
       	<br>1번 경품은 최소값
       	<br>6번 경품은 최대값
       	<br> * 당첨자수가 모두 
       	<br> 소진되면 그 이후에는 
       	<br> 1번만 계속 당첨되게 됩니다.
       	</th>
           <td>
           		
           		<table width="100%" class="table_style11" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
  
			   	<colgroup>
			   		<col width="10%">
			       	<col width="">
			        <col width="">
			        <col width="">
			    </colgroup>
			   	   <tr>
			   	    <th>순서</th>
			       	<th>경품명</th>
			        <th>적립포인트</th>
			        <th>최대 당첨자 수</th>
			       </tr>
			       <tr>
			        <td>1번</td>
			       	<td>${pointRoulette.area01}</td>
			        <td>${pointRoulette.point01}</td>
			        <td>${pointRoulette.max01}</td>
			       </tr>
			       <tr>
			       <td>2번</td>
			       	<td>${pointRoulette.area02}</td>
			        <td>${pointRoulette.point02}</td>
			        <td>${pointRoulette.max02}</td>
			       </tr>
			       <tr>
			       <td>3번</td>
			       	<td>${pointRoulette.area03}</td>
			        <td>${pointRoulette.point03}</td>
			        <td>${pointRoulette.max03}</td>
			       </tr>
			       <tr>
			       <td>4번</td>
			       	<td>${pointRoulette.area04}</td>
			        <td>${pointRoulette.point04}</td>
			        <td>${pointRoulette.max04}</td>
			       </tr>
			       <tr>
			       <td>5번</td>
			       	<td>${pointRoulette.area05}</td>
			        <td>${pointRoulette.point05}</td>
			        <td>${pointRoulette.max05}</td>
			       </tr>
			       <tr>
			       <td>6번</td>
			       	<td>${pointRoulette.area06}</td>
			        <td>${pointRoulette.point06}</td>
			        <td>${pointRoulette.max06}</td>
			       </tr>
			</table>
           		
           </td>
       </tr>
       <tr>
       <th>시간설정 당첨값</th>
           <td>
           <c:choose>
				 <c:when test="${pointRoulette.timegbn == null || pointRoulette.timegbn == '' }">
				  N - 6번 경품을 시간설정 당첨값으로 설정되어 있지 않습니다.
				 </c:when>
				 <c:when test="${pointRoulette.timegbn == 'Y' }">
				  Y - 6번 경품을 시간설정 당첨값으로 설정되어 있습니다.
				 </c:when>
			</c:choose>  
           </td>
       </tr>
       <tr>
       <th>시간설정 당첨값 안전당첨 설정</th>
           <td>
           <c:choose>
				 <c:when test="${pointRoulette.endtime == null || pointRoulette.endtime == '' }">
				  N - 안전당첨이 설정되어 있지 않습니다. 설정했을 경우에는 중복당첨자가 생기지 않지만, 최고당첨자가 당첨되지 않을 수 있습니다.
				 </c:when>
				 <c:when test="${pointRoulette.endtime == 'Y' }">
				  Y - 안전당첨이 설정되어 있습니다. 설정했을 경우에는 중복당첨자가 생기지 않지만, 최고당첨자가 당첨되지 않을 수 있습니다.
				 </c:when>
			</c:choose>  
           </td>
       </tr>
       <tr>
       <th>개인정보 취급동의</th>
           <td>
           		<textarea id="desc01" name="desc01" rows="5" cols="100" readonly="readonly"> ${pointRoulette.desc01} </textarea> 
           </td>
       </tr>
       <tr>
       <th>3자 제공 동의</th>
           <td>
	           	<textarea id="desc02" name="desc02" rows="5" cols="100" readonly="readonly"> ${pointRoulette.desc02} </textarea> 
           </td>
       </tr>
       
</table>

<div id="btn_area">
 	<input type="button" id="goList" name="goList" class="btn_area_a" value="목록" >
 	<input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="수정" >
 	<input type="button" id="goDelete" name="goDelete" class="btn_area_a" value="삭제" >
 	
</div>   

<%-- <div id="btn_area">
 	<a href="<c:url value='/notice/noticeList.do?pg=${pg }' />" class="btn_area_a">목록</a>
 	<c:if test="${sessionScope.AgentLevel ne '3' }">
    	<a href="<c:url value='/notice/noticeUpdateForm.do?num=${noticeDto.board_idx }&pg=${pg }' />" class="btn_area_a">수정</a>
    	<a href="#" class="btn_area_a" onclick="notice_delete()">삭제</a>
    </c:if>
</div> --%>


<form method="post" name="mainForm" id="mainForm" >
	<input type="hidden" id="roulette_id" name="roulette_id" value="${pointRoulette.roulette_id}" >
	<input type="hidden" id="page" name="page" value="${pg}" >
</form>

<script>
	
	 $(document).ready(function() {
		 
		 $("#goList").bind("click",function() {
			$('#mainForm').attr('action','<c:url value="/roulette/channel/channelList.do" />').attr('method', 'post').submit();
		 });
		 
		 $("#goUpdate").bind("click",function() {
			 $('#mainForm').attr('action','<c:url value="/roulette/channel/channelUpdateForm.do" />').attr('method', 'post').submit();
			});
		 
		 $("#goDelete").bind("click",function() {
			 if(confirm('정말 삭제하시겠습니까?')) {
				 $('#mainForm').attr('action','<c:url value="/roulette/channel/channelDelete.do" />').attr('method', 'post').submit();
			 }
			});
	});

</script>