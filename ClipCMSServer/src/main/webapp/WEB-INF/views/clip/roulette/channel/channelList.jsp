<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<style>
.dot1 {
    width: 160px;
    height: 20px;
 /* border: 1px solid red;  */
}
.dot2 {
    width: 315px;
    height: 20px;
   /*   border: 1px solid red;  */
}​
</style>
<script src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/jquery.dotdotdot.min.js" type="text/javascript"></script>
<script>

 $(document).ready(function(){ 
    $('.dot1').dotdotdot({ 
        ellipsis: '... ', 
        wrap: 'word', 
        after: null, 
        watch: true, 
        height: 20,
       
    }); 
    $('.dot2').dotdotdot({ 
        ellipsis: '... ', 
        wrap: 'word', 
        after: null, 
        watch: true, 
        height: 20,  
       
    }); 
    if('${status}'!='')
    $("#status").val('${status}').attr("selected", "selected");
  
}); 

</script>

	<div class="top_title"><h2> 이벤트 채널 목록</h2></div>
    
    <table width="100%" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
    	   
        </colgroup>
        <tr>
        	<th></th>
            <td>
	            <a href="<c:url value='/roulette/channel/channelInsertForm.do' />" class="btn_area_b">신규등록</a>
           </td>
            
           
        </tr>

    </table>
    
   <br>
   <!-- 이벤트 채널 목록 -->
   <table class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="150px">
            <col width="180px">
            <col width="100px">
            <col width="200px">
            <col width="100px">
            <col style="">
        </colgroup>
        <tr>
        	<th>룰렛 아이디</th>
            <th>채널명</th>
            <th>타입</th>
            <th>사용기간</th>
            <th>노출여부</th>
            <th>등록일</th>
        </tr>
        <c:forEach var="pointRoulette" items="${list}">
        <tr>
        	<td>${pointRoulette.roulette_id }</td>
            <td align="center"><a href="<c:url value='/roulette/channel/channelView.do?roulette_id=${pointRoulette.roulette_id }&page=${pg}' />"><span class="dot2" >${pointRoulette.channelname }</span></a></td>
            <td align="center">
            <c:choose>
				 <c:when test="${pointRoulette.routype == 0 }">
				  CLIP
				 </c:when>
				 <c:when test="${pointRoulette.routype == 1 }">
				  외부 APP
				 </c:when>
				 <c:when test="${pointRoulette.routype == 2 }">
				  외부 WEB
				 </c:when>
			</c:choose>  
            </td>
            <td align="center">${pointRoulette.startdate} ~ ${pointRoulette.enddate}</td>
            <td align="center">
            <c:choose>
				 <c:when test="${pointRoulette.status == 0 }">
				  N
				 </c:when>
				 <c:when test="${pointRoulette.status == 1 }">
				  Y
				 </c:when>
			</c:choose>  
            </td>
            <td align="center">${pointRoulette.regdate}</td>
        </tr>
        </c:forEach>
        

    </table>
    
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML }</td></tr></table>
   
    
    
    <script>
    //페이징
    function boardPaging(pg){
    	location.href="<c:url value='/roulette/channel/channelList.do?pg="+pg+"&keywd=${keywd}&status=${status}' />";
    }
    </script>