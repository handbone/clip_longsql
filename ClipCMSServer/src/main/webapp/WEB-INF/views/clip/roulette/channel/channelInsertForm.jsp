<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>
<script src="<c:url value='/resources/js/jquery.validate.min.js' />"></script>
<script>


$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

$(document).ready(function(){ 
	$('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
});


</script>
<div class="top_title"><h2>채널 - 신규등록</h2></div>
<form method="post" name="mainForm" id="mainForm" action="<c:url value='/roulette/channel/channelInsert.do' />" >
<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
  
   	<colgroup>
       	<col width="200">
           <col width="">
       </colgroup>
   	   <tr>
       	<th>ROULETTE ID</th>
           <td>
           	자동생성
           </td>
       </tr>
       <tr>
       	<th>채널명</th>
           <td>
           		<input name="channelname" size="50" maxlength="50" type="text"> (100byte 이내)
           </td>
       </tr>
       <tr>
       	<th>Requester Code</th>
           <td>
           		<input id="requester_code" name="requester_code" size="50" maxlength="50" type="text"> (100byte 이내)
           </td>
       </tr>
       <tr>
       	<th>코드 설명</th>
           <td>
           		<input id="requester_description" name="requester_description" size="50" maxlength="50" type="text"> (100byte 이내)
           </td>
       </tr>
       
       <tr>
       	<th>타입</th>
           <td>
       			<select name="routype">
			        <option value="0" selected>CLIP</option>
				    <option value="1">외부 APP</option>
				    <option value="2">외부 WEB</option>
			    </select> 
			</td>
       </tr> 
			    
       <tr>
       	<th>유효기간</th>
           <td>
           	<input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">		            
            
           </td>
       </tr>
       <tr>
       	<th>노출여부</th>
           <td>
           	 	<select name="status">
			        <option value="1" selected>Y</option>
				    <option value="0">N</option>
			    </select> 
           </td>
       </tr>
       <tr>
       	<th>이미지 구분값
       
       	
       	
       	
       	</th>
           <td>
           		<input name="imagepath" size="50" maxlength="20" type="text">
           			<p>룰렛페이지를 구성하는 8개의 파일명 뒷부분에 붙을 구분값</p>
       	<p>Ex) body_01_test.png , body_02_test.png , body_03_test.png 등이 있다면 구분값은 test 를 입력하면 됨</p>
           </td>
       </tr>
       <tr>
       	<th>룰렛 설정<br>
       	<br>
       	<br>경품 갯수는 6개
       	<br>룰렛 바늘 기준으로
       	<br>시계반대방향으로
       	<br>1번부터 시작
       	<br>1번경품 - 11시방향
       	<br>2번경품 - 9시방향
       	<br>3번경품 - 7시방향
       	<br>4번경품 - 5시방향
       	<br>5번경품 - 3시방향
       	<br>6번경품 - 1시방향
       	<br>1번 경품은 최소값
       	<br>6번 경품은 최대값
       	<br> * 당첨자수가 모두 
       	<br> 소진되면 그 이후에는 
       	<br> 1번만 계속 당첨되게 됩니다.
       	</th>
           <td>
           		
           		<table width="100%" class="table_style11" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
  
			   	<colgroup>
			   		<col width="10%">
			       	<col width="">
			        <col width="">
			        <col width="">
			    </colgroup>
			   	   <tr>
			   	    <th>순서</th>
			       	<th>경품명</th>
			        <th>적립포인트</th>
			        <th>최대 당첨자 수</th>
			       </tr>
			       <tr>
			        <td>1번</td>
			       	<td><input name="area01" size="30" maxlength="50" type="text"></td>
			        <td><input name="point01" size="30" maxlength="50" type="text"></td>
			        <td><input name="max01" size="30" maxlength="50" type="text"></td>
			       </tr>
			       <tr>
			       <td>2번</td>
			       	<td><input name="area02" size="30" maxlength="50" type="text"></td>
			        <td><input name="point02" size="30" maxlength="50" type="text"></td>
			        <td><input name="max02" size="30" maxlength="50" type="text"></td>
			       </tr>
			       <tr>
			       <td>3번</td>
			       	<td><input name="area03" size="30" maxlength="50" type="text"></td>
			        <td><input name="point03" size="30" maxlength="50" type="text"></td>
			        <td><input name="max03" size="30" maxlength="50" type="text"></td>
			       </tr>
			       <tr>
			       <td>4번</td>
			       	<td><input name="area04" size="30" maxlength="50" type="text"></td>
			        <td><input name="point04" size="30" maxlength="50" type="text"></td>
			        <td><input name="max04" size="30" maxlength="50" type="text"></td>
			       </tr>
			       <tr>
			       <td>5번</td>
			       	<td><input name="area05" size="30" maxlength="50" type="text"></td>
			        <td><input name="point05" size="30" maxlength="50" type="text"></td>
			        <td><input name="max05" size="30" maxlength="50" type="text"></td>
			       </tr>
			       <tr>
			       <td>6번</td>
			       	<td><input name="area06" size="30" maxlength="50" type="text"></td>
			        <td><input name="point06" size="30" maxlength="50" type="text"></td>
			        <td><input name="max06" size="30" maxlength="50" type="text"></td>
			       </tr>
			       <!-- <tr>
			     	<td>합계</td>
			       	<td></td>
			        <td></td>
			        <td><input name="totmax" size="30" maxlength="50" type="text" readonly="readonly"></td>
			       </tr> -->
			     
			</table>
           		
           </td>
       </tr>
       <tr>
       <th>시간설정 당첨값</th>
           <td>
           		<input type="checkbox" id="timegbn" name="timegbn" value="Y" checked>
           		체크시 6번 경품을 시간설정 당첨값으로 설정합니다. 선택사항입니다.
           </td>
       </tr>
       <tr>
       <th>시간설정 당첨값 안전당첨 설정</th>
           <td>
           		<input type="checkbox" id="endtime" name="endtime" value="Y" checked>
           		체크시 6번 경품을 안전당첨으로 설정합니다. 이 경우에는 중복당첨자가 생기지 않지만, 최고당첨자가 당첨되지 않을 수 있습니다. 선택사항입니다.
           </td>
       </tr>
       <tr>
       <th>개인정보 취급동의</th>
           <td>
           		<textarea id="desc01" name="desc01" rows="5" cols="100">  </textarea> 
           </td>
       </tr>
       <tr>
       <th>3자 제공 동의</th>
           <td>
	           	<textarea id="desc02" name="desc02" rows="5" cols="100">  </textarea> 
           </td>
       </tr>
       
</table>

<div id="btn_area">
 	<input type="button" class="btn_area_a" value="취소" onclick="javascript:history.go(-1)">
 	<input type="button" id="goInsert" name="goInsert" class="btn_area_a" value="등록" >
 	
 	<input type="hidden" id="startdate" name="startdate" >
 	<input type="hidden" id="enddate" name="enddate" >
 	
</div>   
</form>

<style type="text/css">
input.error, textarea.error{
  border:1px dashed red;
}
label.error{
  margin-left:10px;
  color:red;
}
</style>

<script>
	
	 $(document).ready(function() {
		 $("#goInsert").bind("click",function() {
			 $("#mainForm").submit();
		});
	});
 	

	
	$("#mainForm").validate({
		rules: {
			channelname: { required: true,	maxlength: 100},
			requester_code: { required: true,	maxlength: 100},
			requester_description: { required: true,	maxlength: 100},
			datepicker1: "required",
			datepicker2: "required",
			imagepath: { required: true,	maxlength: 20},
			area01: { required: true,	maxlength: 50},
			area02: { required: true,	maxlength: 50},
			area03: { required: true,	maxlength: 50},
			area04: { required: true,	maxlength: 50},
			area05: { required: true,	maxlength: 50},
			area06: { required: true,	maxlength: 50},
			
			point01: { required: true,	maxlength: 50},
			point02: { required: true,	maxlength: 50},
			point03: { required: true,	maxlength: 50},
			point04: { required: true,	maxlength: 50},
			point05: { required: true,	maxlength: 50},
			point06: { required: true,	maxlength: 50}, 
			
			max01: { required: true,	maxlength: 50},
			max02: { required: true,	maxlength: 50},
			max03: { required: true,	maxlength: 50},
			max04: { required: true,	maxlength: 50},
			max05: { required: true,	maxlength: 50},
			max06: { required: true,	maxlength: 50}  
			
		},
		messages: {
			
		},submitHandler: function(form) {
			
			var endDay = $("#datepicker2").val() ;
			var startDay = $("#datepicker1").val() ;
			
			if(endDay < startDay){
				alert("시작일이 종료일보다 빠를수 없습니다.");
				return;
			}
			
			$("#startdate").val(startDay);
			$("#enddate").val(endDay);
			
			//alert($("input:checkbox[name=timegbn]:checked").val());
		    form.submit();
		}
	});

</script>