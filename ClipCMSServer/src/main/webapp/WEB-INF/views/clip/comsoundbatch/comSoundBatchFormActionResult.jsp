<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
	  <!-- 종료 플래그에 걸리지 않을때  실행 -->
	  <c:if test="${retMap.endFlag ne 'F' }">
       <c:forEach var="resultItem" items="${retMap.elist }" varStatus="status">
	       <tr style="letter-spacing:-1px" class="el">
	     	   <td align="center">${retMap.noCnt } </td>
	           <td align="center" style="height:30px;">${resultItem.phoneNum }</td>
	           <td align="center">${resultItem.retMsg } </td>
	       </tr>
       </c:forEach>
      </c:if>
       
      <input type="hidden" id="eTotalCnt" value="${retMap.eTotalCnt }">
      <input type="hidden" id="eSucCnt" value="${retMap.eSucCnt }">
      <input type="hidden" id="eFailCnt" value="${retMap.eFailCnt }">
	  <input type="hidden" id="noCnt" value="${retMap.noCnt }">      
	  <input type="hidden" id="endFlag" value="${retMap.endFlag }"> 
