<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

<div class="top_title"><h2> 사용자 상세보기</h2></div>

<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
   	<colgroup>
       	<col width="200">
           <col width="">
       </colgroup>
   	   <tr>
       	<th>ID</th>
           <td>
            ${cmsUserInfo.userid}
           </td>
       </tr>
       <tr>
       <tr>
       <th>성명</th>
           <td>
           		${cmsUserInfo.username}
           </td>
       </tr>
       <tr>
       <th>전화번호</th>
           <td>
           		${cmsUserInfo.tel}
           </td>
       </tr>
       <tr>
       <th>email</th>
           <td>
           		${cmsUserInfo.email}
           </td>
       </tr>
       <tr>
       <th>허용 IP</th>
           <td>
           		${cmsUserInfo.arrowip}
           </td>
       </tr>
       <tr>
       	<th>사용자 권한</th>
           <td>
           <c:choose>
				 <c:when test="${cmsUserInfo.userauth == 0 }">
				  슈퍼관리자
				 </c:when>
				  <c:when test="${cmsUserInfo.userauth == 2 }">
				  포인트 조정 관리자
				 </c:when>
				 <c:when test="${cmsUserInfo.userauth == 1 }">
				  일반관리자
				 </c:when>
			</c:choose> 
			</td>
       </tr> 
       <tr>
       	<th>상태</th>
           <td>
           <c:choose>
				 <c:when test="${cmsUserInfo.state == 1 }">
				 사용중
				 </c:when>
				 <c:when test="${cmsUserInfo.state == 2 }">
				 사용중 : 패스워드 변경 대상자
				 </c:when>
				 <c:when test="${cmsUserInfo.state == 3 }">
				 잠금 : 패스워드 실패 잠금상태
				 </c:when>
			</c:choose> 
			</td>
       </tr> 
       <tr>
       <th>로그인 실패 횟수</th>
           <td>
           		${cmsUserInfo.loginfailcnt}
           </td>
       </tr>
        <tr>
       <th>마지막 패스워드 변경일</th>
           <td>
           		${cmsUserInfo.passdate}
           </td>
       </tr>
       <tr>
        <th>등록일</th>
           <td>
           		${cmsUserInfo.regdate}
           </td>
       </tr>
</table>

<div id="btn_area">
 	<input type="button" id="goList" name="goList" class="btn_area_a" value="목록" >
 	<input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="수정" >
 	<input type="button" id="goDelete" name="goDelete" class="btn_area_a" value="삭제" >
</div>  

<form method="post" name="mainForm" id="mainForm" >
	<input type="hidden" id="userid" name="userid" value="${cmsUserInfo.userid}" >
	<input type="hidden" id="page" name="page" value="${pg}" >
</form>

<script>
	
$(document).ready(function() {
	 
	 $("#goList").bind("click",function() {
		$('#mainForm').attr('action','<c:url value="/user/userList.do" />').attr('method', 'post').submit();
	 });
	 
	 $("#goUpdate").bind("click",function() {
		 $('#mainForm').attr('action','<c:url value="/user/userUpdateForm.do" />').attr('method', 'post').submit();
		});
	 
	 $("#goDelete").bind("click",function() {
		 if(confirm('정말 삭제하시겠습니까?')) {
			 $('#mainForm').attr('action','<c:url value="/user/userDelete.do" />').attr('method', 'post').submit();
		 }
		});
});

</script>