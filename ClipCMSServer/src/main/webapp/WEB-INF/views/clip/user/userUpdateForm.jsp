<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/jquery.validate.min.js' />"></script>

<!--  sha256 -->
<script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/sha256.js"></script>
	
<div class="top_title"><h2> 사용자 수정</h2></div>
<form method="post" name="mainForm" id="mainForm" action="<c:url value='/user/userUpdate.do' />" >
<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
   	<colgroup>
       	<col width="200">
           <col width="">
       </colgroup>
   	   <tr>
       	<th>ID</th>
           <td>
				${cmsUserInfo.userid}
           </td>
       </tr>
       <tr>
       	<th>password</th>
           <td>
           		<input id="password" name="password" size="80" maxlength="100" type="text" value="${cmsUserInfo.password}"> (50byte 이내)
           </td>
       </tr>
       <tr>
       <th>성명</th>
           <td>
           		<input id="username" name="username" size="32" maxlength="32" type="text" value="${cmsUserInfo.username}"> (32byte 이내)
           </td>
       </tr>
       <tr>
       <th>전화번호</th>
           <td>
           		<input id="tel" name="tel" size="30" maxlength="20" type="text" value="${cmsUserInfo.tel}"> (20byte 이내)
           </td>
       </tr>
       <tr>
       <th>email</th>
           <td>
           		<input id="email" name="email" size="30" maxlength="62" type="text" value="${cmsUserInfo.email}"> (62byte 이내)
           </td>
       </tr>
       <tr>
       <th>허용 IP</th>
           <td>
           		<input id="arrowip" name="arrowip" size="60" maxlength="300" type="text" value="${cmsUserInfo.arrowip}"> (300byte 이내) - 콤마로 구분하여 여러개 설정이 가능합니다.
           </td>
       </tr>
       <tr>
       	<th>사용자 권한</th>
           <td>
       			<select id="userauth" name="userauth">
			        <option value="0" selected>슈퍼관리자</option>
			        <option value="2">포인트조정 관리자</option>
				    <option value="1">일반관리자</option>
			    </select> 
			</td>
       </tr> 
       <tr>
       	<th>상태</th>
           <td>
           <select id="state" name="state">
			        <option value="1" selected>사용중</option>
				    <option value="2">패스워드 변경 대상자</option>
				    <option value="3">패스워드 실패 잠금</option>
			</select> 
			</td>
       </tr> 
       <tr>
       <th>로그인 실패 횟수</th>
           <td>
           		<input id="loginfailcnt" name="loginfailcnt" size="30" maxlength="1" type="text" value="${cmsUserInfo.loginfailcnt}"> (1byte 이내)
           </td>
       </tr>
        <tr>
       <th>마지막 패스워드 변경일</th>
           <td>
           		<input id="passdate" name="passdate" size="30" maxlength="10" type="text" value="${cmsUserInfo.passdate}"> (10byte 이내)
           </td>
       </tr>
       
</table>

<div id="btn_area">
 	<input type="button" class="btn_area_a" value="취소" onclick="javascript:history.go(-1)">
 	<input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="확인" >
 	
</div>   

<input type="hidden" id="originPassword" name="originPassword" value="${cmsUserInfo.password}">
<input type="hidden" id="userid" name="userid" value="${cmsUserInfo.userid}"> 

</form>

<style type="text/css">
input.error, textarea.error{
  border:1px dashed red;
}
label.error{
  margin-left:10px;
  color:red;
}
</style>

<script>
	
	 $(document).ready(function() {
		 
		 if('${cmsUserInfo.userauth}'!=''){
		  		$("#userauth").val('${cmsUserInfo.userauth}').attr("selected", "selected");
		    }
		 if('${cmsUserInfo.state}'!=''){
		  		$("#state").val('${cmsUserInfo.state}').attr("selected", "selected");
		    }
		 
		 
		 $("#goUpdate").bind("click",function() {
			 $("#mainForm").submit();
		});
	});
	
	$("#mainForm").validate({
		rules: {
			userid: { required: true,	maxlength: 128},
			password: { required: true,	maxlength: 100},
			username: { required: true,	maxlength: 32},
			arrowip: { required: true,	maxlength: 300},
			tel: { required: true,	maxlength: 20},
			loginfailcnt: { required: true,	maxlength: 1},
			passdate: { required: true,	maxlength: 10}
		},
		messages: {
			
		},submitHandler: function(form) {
			
			if( $("#originPassword").val() == $("#password").val() ){

			}else{
        		var user_pwd = $("#password").val();
            	$("#password").val(SHA256(user_pwd));
        	}
        	form.submit();
		   
		}
	});

</script>