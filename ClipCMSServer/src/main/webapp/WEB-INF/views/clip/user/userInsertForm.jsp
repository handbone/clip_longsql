<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/jquery.validate.min.js' />"></script>

<!--  sha256 -->
<script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/sha256.js"></script>
	
<div class="top_title"><h2> 사용자 신규등록</h2></div>
<form method="post" name="mainForm" id="mainForm" action="<c:url value='/user/userInsert.do' />" >
<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
   	<colgroup>
       	<col width="200">
           <col width="">
       </colgroup>
   	   <tr>
       	<th>ID</th>
           <td>
           	<input id="userid" name="userid" size="30" maxlength="128" type="text"> (128byte 이내)
           </td>
       </tr>
       <tr>
       	<th>password</th>
           <td>
           		<input id="password" name="password" size="30" maxlength="50" type="text"> (50byte 이내)
           </td>
       </tr>
       <tr>
       <th>성명</th>
           <td>
           		<input id="username" name="username" size="32" maxlength="32" type="text"> (32byte 이내)
           </td>
       </tr>
       <tr>
       <th>전화번호</th>
           <td>
           		<input id="tel" name="tel" size="30" maxlength="20" type="text"> (20byte 이내)
           </td>
       </tr>
       <tr>
       <th>email</th>
           <td>
           		<input id="email" name="email" size="30" maxlength="62" type="text"> (62byte 이내)
           </td>
       </tr>
        <tr>
       <th>허용 IP</th>
           <td>
           		<input id="arrowip" name="arrowip" size="60" maxlength="300" type="text"> (300byte 이내)
           		<p>콤마로 구분하여 여러개 설정이 가능합니다.</p>
           		<p>모든 IP에서 사용하려면 *.*.*.* 을 입력하세요.</p>
           </td>
       </tr>
       
       
       <tr>
       	<th>사용자 권한</th>
           <td>
       			<select name="userauth">
			        <option value="0" selected>슈퍼관리자</option>
			        <option value="2">포인트조정 관리자</option>
				    <option value="1">일반관리자</option>
			    </select> 
			</td>
       </tr> 
</table>

<div id="btn_area">
 	<input type="button" class="btn_area_a" value="취소" onclick="javascript:history.go(-1)">
 	<input type="button" id="goInsert" name="goInsert" class="btn_area_a" value="등록" >
</div>   
</form>

<style type="text/css">
input.error, textarea.error{
  border:1px dashed red;
}
label.error{
  margin-left:10px;
  color:red;
}
</style>

<script>
	
	 $(document).ready(function() {
		 $("#goInsert").bind("click",function() {
			 $("#mainForm").submit();
		});
	});
	
	$("#mainForm").validate({
		rules: {
			userid: { required: true,	maxlength: 128},
			password: { required: true,	maxlength: 50},
			username: { required: true,	maxlength: 32},
			arrowip: { required: true,	maxlength: 300},
			tel: { required: true,	maxlength: 20}
		},
		messages: {
			
		},submitHandler: function(form) {
			
			formData = $("#mainForm").serialize();
			
			$.ajax({
	            type: "POST",
	            url: "../user/userIdCnt.do",
	            data: formData,
	            dataType: "Json",
	            success: function (resData) {
	                console.log(resData.result);  
	                if (resData.result == "success") {
	                	
	                	
	                	var user_pwd = $("#password").val();
	                	$("#password").val(SHA256(user_pwd));
	                	
	                	 form.submit();
					} else {
	                	alert("현재 사용중인 ID가 있습니다.");
	                }
	            },
	            error: function (e) {
	                alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요(02-2008-7775)");
	            }
	        });
		   
		}
	});

</script>