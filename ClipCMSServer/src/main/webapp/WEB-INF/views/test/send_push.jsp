<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
$(document).ready(function(){ 
	$('#goInsert').click(function(){
		$('#mainForm').attr('action', '<c:url value="/test/sendpush.do"/>');
		$('#mainForm').submit();
	});
	
// 	$('#goInsert').click(sendPush);
});
function sendPush(){
	var formData = $('#mainForm').serialize();
	$.ajax({
        type: "POST",
        url: "<c:url value='/test/sendpush.do'/>",
        data: formData,
        success: function (resData) {
        		alert(resData.result);
        },
        error: function(xhr,textStatus,err) {
        		console.log("readyState: " + xhr.readyState);
        	    console.log("responseText: "+ xhr.responseText);
        	    console.log("status: " + xhr.status);
        	    console.log("text status: " + textStatus);
        	    console.log("error: " + err);
        }
    });
}
</script>
</head>
<body>


<div class="top_title"><h2>Push</h2></div>

<form name="mainForm" id="mainForm" method="POST">
<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
   	<colgroup>
       	<col width="200">
           <col width="">
       </colgroup>
   	   <tr>
       	<th>TYPE</th>
           <td>
           	<input id="TYPE" name="TYPE" size="30" maxlength="128" type="text" value="CTN"> 
           </td>
       </tr>
   	   <tr>
       	<th>ID</th>
           <td>
           	<input id="ID" name="ID" size="30" maxlength="128" type="text" value="eQrCwuTmC4ZDc5OwfBXwxA=="> 
           </td>
       </tr>
   	   <tr>
       	<th>TITLE</th>
           <td>
           	<input id="TITLE" name="TITLE" size="30" maxlength="128" type="text" value="push_test"> 
           </td>
       </tr>
   	   <tr>
       	<th>CONTENT</th>
           <td>
           	<input id="CONTENT" name="CONTENT" size="30" maxlength="128" type="text" value="push_test222"> 
           </td>
       </tr>
   	   <tr>
       	<th>IMGURL</th>
           <td>
           	<input id="IMGURL" name="IMGURL" size="30" maxlength="128" type="text"> 
           </td>
       </tr>
   	   <tr>
       	<th>RANDINGURL</th>
           <td>
           	<input id="RANDINGURL" name="RANDINGURL" size="30" maxlength="128" type="text" value="KTolleh00114://point"> 
           </td>
       </tr>
   	   <tr>
       	<th>AGREECHECKYN</th>
           <td>
           	<input id="AGREECHECKYN" name="AGREECHECKYN" size="30" maxlength="128" type="text" value="N"> 
           </td>
       </tr>
   	   <tr>
       	<th>SENDSERVICETYPE</th>
           <td>
           	<input id="SENDSERVICETYPE" name="SENDSERVICETYPE" size="30" maxlength="128" type="text" value="01"> 
           </td>
       </tr>
   	   <tr>
       	<th>LINKID</th>
           <td>
           	<input id="LINKID" name="LINKID" size="30" maxlength="128" type="text" value="clippoint"> 
           </td>
       </tr>
   	   <tr>
       	<th>EXPIREDATE</th>
           <td>
           	<input id="EXPIREDATE" name="EXPIREDATE" size="30" maxlength="128" type="text"> 
           </td>
       </tr>
</table>
</form>

<div id="btn_area">
<!--  	<input type="button" id="goInsert" class="btn_area_a" value="전송" onclick="sendPush();"> -->
 	<input type="button" id="goInsert" class="btn_area_a" value="전송">
</div> 

</body>
</html>