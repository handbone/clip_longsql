<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>     
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="google-site-verification" 
             content="zArvCH1j3LugduMHKE-ozRvlSmmoysUi3eDB8zosvb8" />

  
</html>

<title>Clip CMS</title>

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/common.css"/>
<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/style.css"/>
<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/ui/jquery-ui.min.css"/>
<!-- JQuery -->
<script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/jquery-3.1.0.js"></script>
<script>
//백스페이스 방지코드
$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && (e.target.type === 'text' || e.target.type === 'password' )) ||
            nodeName === 'textarea') {
        	//do nothing
        } else {
            e.preventDefault();
        }
    }
    $(".objbox").click(function(){
  	  zindex = zindex+1;
  	 var p =  $(this).parentsUntil(".pop-layer ui-draggable ui-draggable-handle");
  	 p.css("z-index",zindex);
    });
   //드레그,마우스우측키 방지 코드 (요청사항) 
    var omitformtags=["input", "textarea", "select"]
    omitformtags=omitformtags.join("|")
    function disableselect(e){
    	if (omitformtags.indexOf(e.target.tagName.toLowerCase())==-1)
    		return false
    }
    function reEnable(){
    	return true
    }
    if (typeof document.onselectstart!="undefined")
    document.onselectstart=new Function ("return false")
    else{
    	document.onmousedown=disableselect
    	document.onmouseup=reEnable
    }

});


//home
function home(){
	location.href="<c:url value='/main.do' />";
}
//logout
function logout(){
	location.href="<c:url value='/signout.do' />";
}
</script>
<script type="text/javascript">
$( document ).ready(function() {
	//lnb 상단 날짜표기
	var d = new Date();
	var week = new Array('일', '월', '화', '수', '목', '금', '토');
	var dd = d.getFullYear() + '년' + (d.getMonth() + 1) + '월' + d.getDate() + '일&nbsp;'+ week[d.getDay()] + '요일';
	$(".title").html("<img src='<c:out value='${pageContext.request.contextPath}'/>/resources/img/icon_monitor.png'>&nbsp"+ dd);
	
	$("span[class=menuNm]:contains('${menu1}')").parents('li').attr('class','active');
	$("span[class=menuNm]:contains('${menu2}')").parents('li').attr('class','active');
});

</script>

</head>
<%-- <body style="height:100%;background-image:url(<c:out value='${pageContext.request.contextPath}'/>/resources/img/body_bg.png);background-repeat: repeat-y;"  oncontextmenu="return false" onselectstart="return false" ondragstart="return false"> 
 --%>
 <body style="height:100%;background-image:url(<c:out value='${pageContext.request.contextPath}'/>/resources/img/body_bg.png);background-repeat: repeat-y;" >

<!-- Head Area -->
<div id="head_area">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/logo.png" onclick="home()" style="cursor:pointer" class="logo">
    <ul><img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/btn_logout.png" onclick="logout()" style="cursor:pointer"></ul>
    <ul class="theme_blue">[마지막접속: ${sessionScope.LastLoginTime}]</ul>
    <ul class="theme_bold">${sessionScope.AgentUserId}님 좋은 하루 되세요</ul>
</div>
<div id="head_sub_title">
	<ul class="title"> </ul>
    <ul class="sub_title">
    	<span><img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/icon_title.png"> 
    		<c:if test="${menu1 ne null}">
	    	<li>${menu1}</li>
	    	</c:if>
	    	<c:if test="${menu2 ne null}">
	    	<li>> ${menu2}</li>
	    	</c:if>
	    	<c:if test="${menu3 ne null}">
	    	<li>> ${menu3}</li>
	    	</c:if>
	    </span>
    </ul>
    <ul class="sub_nav">
      <!--   <li>대메뉴</li>
        <li>서브메뉴</li>
        <li>3차메뉴</li> -->
    </ul>
</div>

<div style="clear:both"></div>
<div id="wrapper">
	<!-- 좌측 메뉴 -->
	<div class="sub_menu_container">
		<ul class="sub_menu">
		
			<!-- UI Object -->
			<div id="menu_v" class="menu_v">
				<ul>
				
					<li><a href="<c:url value='' />"><span class="menuNm">테스트용</span></a></li>
					<li><ul><li><a href="<c:url value='/test/send_push.do' />"><span class="menuNm">Push 테스트</span></a></li></ul></li>
				
				</ul>
			</div><!-- //UI Object -->
		</ul>
    </div><!-- sub_menu_container -->

	<div class="container">
		<!-- 컨텐츠 시작 -->
		<div id="content">

