<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>     
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="google-site-verification" 
             content="zArvCH1j3LugduMHKE-ozRvlSmmoysUi3eDB8zosvb8" />

  
</html>

<title>Clip CMS</title>

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/common.css"/>
<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/style.css"/>
<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/ui/jquery-ui.min.css"/>
<!-- JQuery -->
<script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/jquery-3.1.0.js"></script>

<!--Content layout -->
<jsp:include page="${display }"/>

