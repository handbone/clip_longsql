package com.coopnc.cardpoint.common;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Log {
	
	public static void debug(String filePath, String line){
		FileWriter fw = null;
		try {
			makeDirIfNot(filePath);
			
		    fw = new FileWriter(filePath, true);
		    fw.write(line+"\n");
		} catch(IOException ioe) {
		    System.err.println("IOException: " + ioe.getMessage());
		} finally {
			if(fw != null)
				try {
					fw.close();
				} catch (IOException e) {}
		}
	}
	
	private static void makeDirIfNot(String path) {
		File file = new File(path);
		
		if(file.exists())
			return;
		
		File dir = file.getParentFile();
		
		if(!dir.exists()) {
			dir.mkdirs();
		}
	}
	
}

