package com.coopnc.cardpoint.consts;

import java.util.concurrent.ConcurrentHashMap;

import com.coopnc.cardpoint.netty.component.ResponseFuture;

import io.netty.channel.Channel;

public class Consts {
	
	public static final String RESULT_SUCCESS = "S";
	public static final String RESULT_FAIL = "F";
	
	/**
	 * 프로퍼티 키 관리. {systemname}.{prop key} = value
	 * @author keunjini
	 */
	public static class PropKey {
		public static final String SERVER_IP = "server.ip";
		public static final String SERVER_PORT = "server.port";
		public static final String SERVER_TIMEOUT = "server.timeout";
		
		public static final String FTP_GB = "ftp.gb";
		public static final String FTP_IP = "ftp.ip";
		public static final String FTP_PORT = "ftp.port";
		public static final String FTP_ID = "ftp.id";
		public static final String FTP_PASS = "ftp.pass";
		public static final String FTP_UP_DIR = "ftp.up.dir";
		public static final String FTP_DOWN_DIR = "ftp.down.dir";
		
		public static final String LOCAL_UP_DIR = "local.up.dir";
		public static final String LOCAL_DOWN_DIR = "local.down.dir";
		
		public static final String TELE_LOG_PATH = "tele.log.path";
		public static final String TELE_SYSTEM_ID = "tele.server.system.id";
		public static final String TELE_MY_SYSTEM_ID = "tele.my.system.id";
		public static final String TELE_HAN_CHARSET = "tele.han.charset";
	}
	
	//조회(R), 사용취소(C), 망취소(N), 전환(E), 정산(S) 구분 + 신한 Fan(F)
	public static enum TrType{
        GET("R"),
        USE("U"),
        CANCEL_USE("C"),
        NET_CANCEL_USE("N"),
        
        FAN_CHECK("F"),
		
		PUT_FILE("P"),
		GET_FILE("G");
    
        private String value;
        
        TrType(String value){
        	this.value = value;
        }
        
        public String getValue(){
        	return this.value;
        }
        
        public static TrType getEnum(String code){
            for(TrType e : TrType.values()){
                if(code.equals(e.value)) return e;
            }
            return null;
        }
    }
	
	public static class HanaConsts {
		
		public static final String system_name = "hanamembers";
		
		public static int REQ_TOT_MSG_LEN = 1156;	//256 + 900
		public static int REQ_HEADER_LEN = 256;
		public static int REQ_BODY_LEN = 900;
		
		public static int RES_TOT_MSG_LEN = 1156;	//256 + 900
		public static int RES_HEADER_LEN = 256;
		public static int RES_BODY_LEN = 900;

		public static int NEETY_WORKER_NUM = 10;
		
		/*'080081 : 조회,
    	020021 : 적립, 020022 : 적립취소, 028028 : 적립 망취소
    	030031 : 사용, 030032 : 사용취소, 038038 : 사용 망취소*/
		
		public static String getServiceCode(String serviceType) {
			String code = "";
			switch (serviceType) {
			case "R":
				code = "080081";
				break;
			case "U":
				code = "030031";
				break;
			case "C":
				code = "030032";
				break;
			case "N":
				code = "038038";
				break;

			default:
				break;
			}

			return code;
		}
	}
	
	public static class KbConsts {
		
		public static final String system_name = "kbpointree";
		
		public static int REQ_TOT_MSG_LEN = 204;	//length(4)+50 + 150
		public static int REQ_HEADER_LEN = 50;
		public static int REQ_BODY_LEN = 150;
		
		public static int RES_TOT_MSG_LEN = 204;	//length(4)+50 + 150
		public static int RES_HEADER_LEN = 50;
		public static int RES_BODY_LEN = 150;

		
		public static int NEETY_WORKER_NUM = 20;
		
		/**/
		
		public static String getServiceCode(String serviceType) {
			String code = "";
			switch (serviceType) {
			case "R":
				code = "020";
				break;
			case "U":
				code = "032";	// 응답은 033으로 옴
				break;
			case "C":
				code = "032";	// 응답은 033으로 옴
				break;
			case "N":
				code = "032";	// 응답은 033으로 옴
				break;

			default:
				break;
			}

			return code;
		}
	}
	
	public static class ShinhanConsts {
		
		public static final String system_name = "shinhan";
		
		public static int REQ_TOT_MSG_LEN = 1004;	//1000
		public static int REQ_HEADER_LEN = 4;
		public static int REQ_BODY_LEN = 1000;
		
		public static int RES_TOT_MSG_LEN = 1004;	//1000
		public static int RES_HEADER_LEN = 4;
		public static int RES_BODY_LEN = 1000;

		
		public static int NEETY_WORKER_NUM = 20;
		
		public static ConcurrentHashMap<Integer, Channel> queue = new ConcurrentHashMap<Integer, Channel>();
		public static ConcurrentHashMap<String, ResponseFuture> responseMap = new ConcurrentHashMap<String, ResponseFuture>();

		/*
		71 포인트조회(탈회여부 상관없이)
		72 회원상태조회(탈회여부 등)
		20 적립
		29 적립취소
		30 사용
		39 사용취소(망취소) 
		*/
		
		public static String getServiceCode(String serviceType) {
			String code = "";
			switch (serviceType) {
			case "R":
				code = "71";
				break;
			case "U":
				code = "30";
				break;
			case "C":
				code = "39";
				break;
			case "N":
				code = "39";
				break;
			case "S":
				code = "72";
				break;
				
			default:
				break;
			}

			return code;
		}
	}
	
	public static class BcConsts {
		
		public static final String system_name = "bccard";
		
		public static int GET_REQ_MSG_LEN = 205;	//4+64 + 137
		public static int USE_TOT_MSG_LEN = 342;	//4+64 + 274
		
		public static int NEETY_WORKER_NUM = 1;

		public static String getServiceCode(String serviceType) {
			String code = "";
			switch (serviceType) {
			case "R":
				code = "63";
				break;
			case "U":
				code = "05";	//05: 사용
				break;
			case "C":
				code = "15";	//15: 사용취소
				break;
			case "N":
				code = "15";
				break;

			default:
				break;
			}

			return code;
		}
	}
}
