package com.coopnc.cardpoint.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileUtil {
	
	private static final String AL_SHA_256 = "SHA-256";
	
	/**
	 * 파일이 변경되었는지 여부 검사 -> 리턴값있음 : 변경됨, null : 변경안됨
	 * @param orgCheckSum
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static String getFileChecksumIfChange(String orgCheckSum, File file) throws IOException {
		try {
			MessageDigest shaDigest = MessageDigest.getInstance(AL_SHA_256);	 
			String checkSum = getFileChecksum(shaDigest, file);
			
			if(! checkSum.equals(orgCheckSum))
				return checkSum;

		} catch(NoSuchAlgorithmException e) {
			e.printStackTrace(); 
		}

		return null;
	}
	
	private static String getFileChecksum(MessageDigest digest, File file) throws IOException {
		FileInputStream fis = new FileInputStream(file);

		byte[] byteArray = new byte[1024];
		int bytesCount = 0;

		while ((bytesCount = fis.read(byteArray)) != -1) {
			digest.update(byteArray, 0, bytesCount);
		}

		fis.close();

		byte[] bytes = digest.digest();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}
	
	public static void append(String filePath, String line){
		BufferedWriter out = null;
		try {
		    out = new BufferedWriter(new FileWriter(filePath, true));
		    out.write(line);
		    out.newLine();
		} catch(IOException ioe) {
		    System.err.println("IOException: " + ioe.getMessage());
		} finally {
			if(out != null)
				try {
					out.close();
				} catch (IOException e) {}
		}
	}
	
	public static void appendNoLine(String filePath, String line){
		BufferedWriter out = null;
		try {
		    out = new BufferedWriter(new FileWriter(filePath, true));
		    out.write(line);
		} catch(IOException ioe) {
		    System.err.println("IOException: " + ioe.getMessage());
		} finally {
			if(out != null)
				try {
					out.close();
				} catch (IOException e) {}
		}
	}
}
