package com.coopnc.cardpoint.util;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FtpUtil {

	/**
	 * sftp get file
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @param localFilePath
	 * @param remotePath
	 * @param remoteFile
	 * @param timeOutMilliseconds
	 * @return
	 */
    public static int sftpGetFile(String host, int port, String user, String pass, String localFilePath, String remotePath, String remoteFile, int timeOutMilliseconds){
    	int result = 0;
        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(user, host, port);
            session.setPassword(pass);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect(timeOutMilliseconds);
            
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(remotePath);
            
            int fileChk = checkFileExist(channelSftp, remoteFile);
            if(fileChk == 1) {
	            byte[] buffer = new byte[4096];
	            
	            bis = new BufferedInputStream(channelSftp.get(remoteFile));
	            bos = new BufferedOutputStream(new FileOutputStream(new File(localFilePath)));
	            
	            int readCount;
	            while ((readCount = bis.read(buffer)) != -1) {
	                bos.write(buffer, 0, readCount);    
	            }
	            
	            bos.flush();
	            
	            result = 1;
            } else {
            	result = fileChk;	
            }

            File file = new File(localFilePath);
            if(!file.exists())
            	result = -1;
            else 
            	result = 1;
            
        } catch (JSchException | SftpException | IOException ex) {
            ex.printStackTrace();
            result = -2;
        } finally {
        	if(bis != null)
        		try {bis.close();} catch (Exception e){}
        	if(bos != null)
        		try {bos.close();} catch (Exception e){}
        	if(channel != null)
        		try {channel.disconnect();} catch (Exception e){}
        	if(session != null)
        		try {session.disconnect();} catch (Exception e){}
        }

    	return result;
    }

    /**
     * sftp put file
     * @param host
     * @param port
     * @param user
     * @param pass
     * @param localFilePath
     * @param remotePath
     * @param remoteFile
     * @param timeOutMilliseconds
     * @return
     */
    public static int sftpPutFile(String host, int port, String user, String pass, String localFilePath, String remotePath, String remoteFile, int timeOutMilliseconds){
    	
    	int result = 0;
    	
        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(user, host, port);
            session.setPassword(pass);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect(timeOutMilliseconds);
            
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(remotePath);

            byte[] buffer = new byte[4096];
            
            bis = new BufferedInputStream(new FileInputStream(new File(localFilePath)));
            bos = new BufferedOutputStream(channelSftp.put(remoteFile));
            
            int readCount;
            while ((readCount = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, readCount);
            }
            
            bos.flush();

            result = 1;
            
        } catch (JSchException | SftpException | IOException ex) {
            ex.printStackTrace();
        	result = -2;
        } finally {
        	if(bis != null)
        		try {bis.close();} catch (Exception e){}
        	if(bos != null)
        		try {bos.close();} catch (Exception e){}
        	if(channel != null)
        		try {channel.disconnect();} catch (Exception e){}
        	if(session != null)
        		try {session.disconnect();} catch (Exception e){}
        }

    	return result;
    }
    
    static int checkFileExist(ChannelSftp channelSftp, String fileName) {
    	try {
    	    channelSftp.lstat(fileName);
    	    return 1;
    	} catch (SftpException e){
    	    if(e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE){
    	    	return -1;
    	    } else {
    	    	return -2;
    	    }
    	}
    }
}
