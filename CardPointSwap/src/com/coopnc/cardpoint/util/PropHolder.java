package com.coopnc.cardpoint.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

public class PropHolder {
	
	private PropHolder(){}
	private static PropHolder propertyHolder;
	private static HashMap<String, String> propMap;
	
	private static final String _PROP_FILE_CHECK_KEY = "_PROP_FILE_CHECK_SUM";
	
	public static PropHolder getInstance(){
		if (propertyHolder == null) {
			propertyHolder = new PropHolder();
			propMap = new HashMap<String, String>();
		}
		
		return propertyHolder;
	}
	
	public void init(String initFilePath) throws Exception {
		try {
			File propFile = new File(initFilePath);
			
			//절대경로 파일이 존재하지 않다면 클래스패스를 통해 얻는다 
			if(!propFile.exists()) {
				propFile = new File(this.getClass().getClassLoader().getResource(initFilePath).toURI());				
			}

			String fileCheckSum = propMap.get(_PROP_FILE_CHECK_KEY);
			
			String newChecksum = FileUtil.getFileChecksumIfChange(fileCheckSum, propFile);
			if (newChecksum != null) {
				Properties properties = new Properties();
				
				propMap.put(_PROP_FILE_CHECK_KEY, newChecksum);
				properties.load(new FileInputStream(propFile));
				
				Iterator<Object> keys = properties.keySet().iterator();
		        while( keys.hasNext() ){
		            String key = (String)keys.next();
		            propMap.put(key, (String)properties.get(key));
		        }
			}

	        /*Iterator<String> keys2 = propMap.keySet().iterator();
	        while( keys2.hasNext() ){
	            String key = keys2.next();
	            System.out.println(key+" = "+propMap.get(key));
	        }*/

		} catch (FileNotFoundException fne) {
			throw fne;
		} catch (IOException ioe) {
			throw ioe;
		}
	}
	
	public String get(String key) {
        return propMap.get(key);
    }
	
	/**
	 * 회사별로 다른 코드를 조회할때 사용 예) {카드사명}.{코드명}={value}
	 * @param key
	 * @return
	 */
	public String getVar(String prefix, String key) {
        return propMap.get(prefix+"."+key);
    }
	
}
