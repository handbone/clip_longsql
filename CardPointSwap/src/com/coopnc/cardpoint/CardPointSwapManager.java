package com.coopnc.cardpoint;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.cardpoint.consts.Consts.BcConsts;
import com.coopnc.cardpoint.consts.Consts.HanaConsts;
import com.coopnc.cardpoint.consts.Consts.KbConsts;
import com.coopnc.cardpoint.consts.Consts.PropKey;
import com.coopnc.cardpoint.consts.Consts.ShinhanConsts;
import com.coopnc.cardpoint.domain.FtpFileInfo;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.handler.PointSwapService;
import com.coopnc.cardpoint.util.MessageUtil;
import com.coopnc.cardpoint.util.PropHolder;

public class CardPointSwapManager {
	
	private static Logger logger = LoggerFactory.getLogger(CardPointSwapManager.class);
	PointSwapService handler;
	
	public CardPointSwapManager(String iniFilePath) throws Exception {
		try {
			PropHolder.getInstance().init(iniFilePath);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new Exception("card point init file error!!");
		}
	}
	
	public CardPointSwapManager setHandler(PointSwapService handler){
		this.handler = handler;
		return this;
	}
	
	public void getPoint() throws Exception {
		if(handler == null)
			throw new Exception("set handler first!!");
		
		handler.getPoint();
	}
	
	public void usePoint() throws Exception {
		if(handler == null)
			throw new Exception("set handler first!!");
		
		handler.usePoint();
	}
	
	public void cancelUse() throws Exception {
		if(handler == null)
			throw new Exception("set handler first!!");
		
		handler.cancelUse();
	}
	
	public void netCancelUse() throws Exception {
		if(handler == null)
			throw new Exception("set handler first!!");
		
		handler.netCancelUse();
	}
	
	public int putRecordFile(FtpFileInfo info) throws Exception {
		if(handler == null)
			throw new Exception("set handler first!!");
		
		return handler.putRecordFile(info);
	}
	
	public int getRecordFile(FtpFileInfo info) throws Exception {
		if(handler == null)
			throw new Exception("set handler first!!");
		
		return handler.getRecordFile(info);
	}
	
	public void makeLocalRecordFile(FtpFileInfo info, List<PointSwapInfo> pointSwapInfoList){
		handler.makeLocalRecordFile(info, pointSwapInfoList);
	}
	
	public List<PointSwapInfo> parseRemoteRecordFile(FtpFileInfo info){
		return handler.parseRemoteRecordFile(info);
	}
	
	public List<PointSwapInfo> parseRemoteRecordFileDate(FtpFileInfo info, String date){
		return handler.parseRemoteRecordFileDate(info, date);
	}
	
	public void checkFanUser() throws Exception {
		if(handler == null)
			throw new Exception("set handler first!!");
		
		handler.checkFanUser();
	}

	public String makeDefaultTrId(String systemName, String date, String serialNo, String etc) throws Exception {
		String result = "";
		
		switch (systemName) {
			case HanaConsts.system_name:
				result = PropHolder.getInstance().getVar(systemName, PropKey.TELE_MY_SYSTEM_ID)+MessageUtil.getDateStr("yyyyMMdd")+MessageUtil.lpad(serialNo, 10, "0");
				break;
			case KbConsts.system_name:
				result = MessageUtil.getDateStr("yyMMdd")+MessageUtil.lpad(serialNo, 4, "0");
				break;
			case ShinhanConsts.system_name:
				result = MessageUtil.getDateStr("yyyyMMdd")+"S000"+etc+MessageUtil.lpad(serialNo, 8, "0");
				break;
			case BcConsts.system_name:
				result = "BC" + MessageUtil.getDateStr("yyMMdd")+MessageUtil.lpad(serialNo, 4, "0");
				break;
			default:
				logger.error("메시지 전송을 위한 거래아이디 생성에 실패하였습니다.");
				throw new Exception("[중요] 메시지 전송을 위한 거래아이디 생성에 실패하였습니다.");
		}
		
		return result;
	}
}
