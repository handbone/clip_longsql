package com.coopnc.cardpoint;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.handler.PointHandler_HANA;
import com.coopnc.cardpoint.handler.PointHandler_KB;
import com.coopnc.cardpoint.handler.PointHandler_SHINHAN;
import com.coopnc.cardpoint.util.MessageUtil;

public class TeleManagerTest {
	
	
	private static String INI_FILE_PATH = "pointswap.properties";
	private static Logger logger = LoggerFactory.getLogger(TeleManagerTest.class);
	
	public static void main (String[] args) {
		try {
			if(args.length > 0)
				INI_FILE_PATH = args[0];

			for (int i=0; i<100 ;i++){
				/*hana_get();
				hana_use();
				hana_cancel();
				
				kb_get();
				kb_use();
				kb_cancel();
				
				shinhan_get();
				shinhan_use();
				shinhan_cancel();
*/
				/*shinhan_get();
				shinhan_use();
				shinhan_cancel();
				
				Thread.sleep(100);
				
				System.out.println(" =========== "+i);*/
			}
			
			try{
				shinhan_get();
			} catch	(Exception e){
				
			}
			
			Thread.sleep(14000);
			
			try{
				shinhan_get();
			} catch	(Exception e){
				
			}
		
			/*hana_get();
			String tr_id = hana_use();
			hana_cancel(tr_id);
			*/
			/*hana_get();
			hana_use();
			hana_cancel();
			
			kb_get();
			kb_use();
			kb_cancel();
			
			shinhan_get();
			shinhan_use();
			shinhan_cancel();*/
			/*
			shinhan_get();
			String trId = shinhan_use();
			
			System.out.println(" trId ="+trId);
			shinhan_cancel(trId);
			hana_use();
			hana_cancel();*/

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void run(){
		try {
			CardPointSwapManager manager = new CardPointSwapManager("pointswap.properties");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// 하나 ------------------------------------
	private static void hana_get() throws Exception{
		CardPointSwapManager manager = new CardPointSwapManager("pointswap.properties");
		
		PointSwapInfo pointSwapInfo = new PointSwapInfo();
		pointSwapInfo.setUser_ci("4+G9UIWSKk3LLecZY33DOFZLCiCrdiyfuBZAMZKU4RzSxPM+7PjtQi0nBMvq/SMTYmVQWOXG63RhMmB7TPqDqA==");
		//pointSwapInfo.setReq_point(2);
		pointSwapInfo.setTrans_id(manager.makeDefaultTrId("hanamembers", "", "1", ""));
		pointSwapInfo.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
		manager.setHandler(new PointHandler_HANA(pointSwapInfo));
		manager.getPoint();
		
		logger.debug("----------------------------------------------");
		logger.debug(new String(pointSwapInfo.getRes_msg_bytes()));
		logger.debug("----------------------------------------------");
	}

	private static String hana_use() throws Exception{
		CardPointSwapManager manager = new CardPointSwapManager("pointswap.properties");
		
		PointSwapInfo pointSwapInfo = new PointSwapInfo();
		pointSwapInfo.setUser_ci("4+G9UIWSKk3LLecZY33DOFZLCiCrdiyfuBZAMZKU4RzSxPM+7PjtQi0nBMvq/SMTYmVQWOXG63RhMmB7TPqDqA==");
		pointSwapInfo.setReq_point(2);
		pointSwapInfo.setTrans_id(manager.makeDefaultTrId("hanamembers", "", "2", ""));
		pointSwapInfo.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
		manager.setHandler(new PointHandler_HANA(pointSwapInfo));
		manager.usePoint();
		
		logger.debug("----------------------------------------------");
		logger.debug(new String(pointSwapInfo.getRes_msg_bytes()));
		logger.debug("----------------------------------------------");
		
		return pointSwapInfo.getTrans_id();
	}

	private static void hana_cancel(String trid) throws Exception{
		CardPointSwapManager manager = new CardPointSwapManager("pointswap.properties");
		
		PointSwapInfo pointSwapInfo = new PointSwapInfo();
		pointSwapInfo.setUser_ci("4+G9UIWSKk3LLecZY33DOFZLCiCrdiyfuBZAMZKU4RzSxPM+7PjtQi0nBMvq/SMTYmVQWOXG63RhMmB7TPqDqA==");
		pointSwapInfo.setReq_point(2);
		pointSwapInfo.setTrans_id(manager.makeDefaultTrId("hanamembers", "", "3", ""));
		pointSwapInfo.setRef_trans_id(trid);
		/*pointSwapInfo.setApprove_date();
		pointSwapInfo.setApprove_no();*/
		pointSwapInfo.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
		manager.setHandler(new PointHandler_HANA(pointSwapInfo));
		manager.cancelUse();
		
		logger.debug("----------------------------------------------");
		logger.debug(new String(pointSwapInfo.getRes_msg_bytes()));
		logger.debug("----------------------------------------------");
	}
	
	// KB ------------------------------------
	private static void kb_get() throws Exception{
		CardPointSwapManager manager = new CardPointSwapManager("pointswap.properties");
		
		PointSwapInfo pointSwapInfo = new PointSwapInfo();
		pointSwapInfo.setUser_ci("USER_CI_0123");
		//pointSwapInfo.setReq_point(299);
		pointSwapInfo.setTrans_id("KB_TR_0001");
		pointSwapInfo.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
		manager.setHandler(new PointHandler_KB(pointSwapInfo));
		manager.getPoint();
		
		logger.debug("----------------------------------------------");
		logger.debug(new String(pointSwapInfo.getRes_msg_bytes()));
		logger.debug("----------------------------------------------");
	}

	private static void kb_use() throws Exception{
		CardPointSwapManager manager = new CardPointSwapManager("pointswap.properties");
		
		PointSwapInfo pointSwapInfo = new PointSwapInfo();
		pointSwapInfo.setUser_ci("USER_CI_0123");
		pointSwapInfo.setReq_point(4);
		pointSwapInfo.setTrans_id("KB_TR_0002");
		pointSwapInfo.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
		manager.setHandler(new PointHandler_KB(pointSwapInfo));
		manager.usePoint();
		
		logger.debug("----------------------------------------------");
		logger.debug(new String(pointSwapInfo.getRes_msg_bytes()));
		logger.debug("----------------------------------------------");
	}

	private static void kb_cancel() throws Exception{
		CardPointSwapManager manager = new CardPointSwapManager("pointswap.properties");
		
		PointSwapInfo pointSwapInfo = new PointSwapInfo();
		pointSwapInfo.setUser_ci("USER_CI_0123");
		pointSwapInfo.setReq_point(4);
		pointSwapInfo.setTrans_id("KB_TR_0003");
		pointSwapInfo.setRef_trans_id("KB_TR_0002");
		pointSwapInfo.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
		manager.setHandler(new PointHandler_KB(pointSwapInfo));
		manager.cancelUse();
		
		logger.debug("----------------------------------------------");
		logger.debug(new String(pointSwapInfo.getRes_msg_bytes()));
		logger.debug("----------------------------------------------");
	}
	
	// 신한 --------------------------------------
	private static void shinhan_get() throws Exception{
		CardPointSwapManager manager = new CardPointSwapManager(INI_FILE_PATH);
		
		PointSwapInfo pointSwapInfo = new PointSwapInfo();
		pointSwapInfo.setUser_ci("1PPAK7sm+csa+THDb9uzuRJ5bj5bRjtw1TBAnP4JmkPi5wwVBH9aJ75lqLkFzbtPcY20DzFq2Vt4IFkIHB/KZQ==");
		pointSwapInfo.setCust_id_org("51234");
		//pointSwapInfo.setReq_point(2);
		pointSwapInfo.setTrans_id(manager.makeDefaultTrId("shinhan", "", "101", "1"));
		pointSwapInfo.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
		manager.setHandler(new PointHandler_SHINHAN(pointSwapInfo));
		manager.getPoint();
		
		logger.debug("----------------------------------------------");
		logger.debug(new String(pointSwapInfo.getRes_msg_bytes()));
		logger.debug("----------------------------------------------");
	}

	private static String shinhan_use() throws Exception{
		
		CardPointSwapManager manager = new CardPointSwapManager(INI_FILE_PATH);
		
		PointSwapInfo pointSwapInfo = new PointSwapInfo();
		pointSwapInfo.setUser_ci("1PPAK7sm+csa+THDb9uzuRJ5bj5bRjtw1TBAnP4JmkPi5wwVBH9aJ75lqLkFzbtPcY20DzFq2Vt4IFkIHB/KZQ==");
		pointSwapInfo.setCust_id_org("51234");
		pointSwapInfo.setReq_point(5);
		pointSwapInfo.setTrans_id(manager.makeDefaultTrId("shinhan", "", "102", "3"));
		pointSwapInfo.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
		manager.setHandler(new PointHandler_SHINHAN(pointSwapInfo));
		manager.usePoint();

		logger.debug("----------------------------------------------");
		logger.debug(new String(pointSwapInfo.getRes_msg_bytes()));
		logger.debug("----------------------------------------------");
		
		return pointSwapInfo.getTrans_id();
	}

	private static void shinhan_cancel(String preTrId) throws Exception{
		
		CardPointSwapManager manager = new CardPointSwapManager(INI_FILE_PATH);
		PointSwapInfo pointSwapInfo = new PointSwapInfo();
		pointSwapInfo.setUser_ci("1PPAK7sm+csa+THDb9uzuRJ5bj5bRjtw1TBAnP4JmkPi5wwVBH9aJ75lqLkFzbtPcY20DzFq2Vt4IFkIHB/KZQ==");
		pointSwapInfo.setCust_id_org("51234");
		pointSwapInfo.setReq_point(5);
		pointSwapInfo.setTrans_id(manager.makeDefaultTrId("shinhan", "", "103", "3"));
		pointSwapInfo.setRef_trans_id(preTrId);
		pointSwapInfo.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
		manager.setHandler(new PointHandler_SHINHAN(pointSwapInfo));
		manager.cancelUse();
		
		logger.debug("----------------------------------------------");
		logger.debug(new String(pointSwapInfo.getRes_msg_bytes()));
		logger.debug("----------------------------------------------");
	}
	
	
}
