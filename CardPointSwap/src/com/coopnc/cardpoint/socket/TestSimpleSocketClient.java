package com.coopnc.cardpoint.socket;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class TestSimpleSocketClient {
	
	public TestSimpleSocketClient(){}
	
	public static void main(String args[]) {
      try {     
          Socket socket=new Socket("localhost",9898);
          DataOutputStream out =new DataOutputStream(socket.getOutputStream());
          BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
          out.writeUTF("Connect To Server");
          out.flush();
          
          String input = in.readLine();
          if (input == null) {
            
          }
          
          System.out.println(input.toUpperCase());
          
          out.close();
          socket.close();

        } catch(Exception ex) {
          System.out.println(ex);
        }
    }
	
	public static void send(String msg){
		
	}
	
}
