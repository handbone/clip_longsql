package com.coopnc.cardpoint.handler;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.cardpoint.consts.Consts;
import com.coopnc.cardpoint.consts.Consts.PropKey;
import com.coopnc.cardpoint.consts.Consts.ShinhanConsts;
import com.coopnc.cardpoint.consts.Consts.TrType;
import com.coopnc.cardpoint.domain.FtpFileInfo;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.netty.NettyClient_SHINHAN;
import com.coopnc.cardpoint.netty.component.ResponseFuture;
import com.coopnc.cardpoint.util.FtpUtil;
import com.coopnc.cardpoint.util.IntHolder;
import com.coopnc.cardpoint.util.MessageUtil;

public class PointHandler_SHINHAN extends PointSwapServiceHandler implements PointSwapService {

	private static Logger logger = LoggerFactory.getLogger(PointHandler_SHINHAN.class);
	
	public PointHandler_SHINHAN(PointSwapInfo pointSwapInfo) {
		this.pointSwapInfo = pointSwapInfo;
		init();
	}
	
	void init() {
		
		this.systemName = ShinhanConsts.system_name;
		this.serviceIp = this.getVarProp(PropKey.SERVER_IP);
		this.serverPort = Integer.parseInt(this.getVarProp(PropKey.SERVER_PORT));
		this.timeout = Integer.parseInt(this.getVarProp(PropKey.SERVER_TIMEOUT));
		
		//tele log file path
		if(this.getVarProp(PropKey.TELE_LOG_PATH) != null)
			this.teleLogPath = this.getVarProp(PropKey.TELE_LOG_PATH) + "/"  + "telelog_"+MessageUtil.getDateStr("yyyyMMdd") + ".log";

        /*: COP0005_33_YYYYMMDD000000_10.sam => 대사화일 
        : COP0005_33_YYYYMMDD000000_10.sam.END => 완료체크용 화일(0 BYTE) 
        + "/" + "COP0005_33_"+MessageUtil.getDateStr("yyyyMMdd")+"000000_10.sam";
        */
		
	}

	@Override
	public void getPoint() {
		pointSwapInfo.setTrans_div(TrType.GET.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_SHINHAN conn = new NettyClient_SHINHAN(serviceIp, serverPort);

			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		parseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void usePoint() {
		pointSwapInfo.setTrans_div(TrType.USE.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_SHINHAN conn = new NettyClient_SHINHAN(serviceIp, serverPort);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void cancelUse() {
		pointSwapInfo.setTrans_div(TrType.CANCEL_USE.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_SHINHAN conn = new NettyClient_SHINHAN(serviceIp, serverPort);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void netCancelUse() {
		pointSwapInfo.setTrans_div(TrType.NET_CANCEL_USE.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_SHINHAN conn = new NettyClient_SHINHAN(serviceIp, serverPort);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseResMsg();
		writeTeleLog(1);
	}
	
	@Override
	public void checkFanUser() {
		pointSwapInfo.setTrans_div(TrType.FAN_CHECK.getValue());
		
		makeFanCheckReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_SHINHAN conn = new NettyClient_SHINHAN(serviceIp, serverPort);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		parseFanCheckResMsg();
		writeTeleLog(1);
	}
	
	private void initReqMsg(int totMsgLen){
		pointSwapInfo.setReq_msg_bytes(new byte[totMsgLen]);
		
		//메세지 초기화 - 모두 스페이스로 채움
		byte[] bb = " ".getBytes();
		for (int i = 0 ; i < pointSwapInfo.getReq_msg_bytes().length ; i++){
			pointSwapInfo.getReq_msg_bytes()[i] = bb[0];
		}
	}
	
	private void makeReqMsg(){	
		IntHolder _pos = new IntHolder();			//전문 포지션
		initReqMsg(ShinhanConsts.REQ_TOT_MSG_LEN);	//신한은 1000 바이트 고정 + 4바이트 길이(1000)
		
		//--------------- 바디 : s ---------------------
		addAN("1000", _pos, 4);		//NUMBER	4	0	전문길이	LEN
		addAN("20310", _pos, 5);		//CHAR	5	0	전문번호	SRA_TG_N
		addAN(ShinhanConsts.getServiceCode(pointSwapInfo.getTrans_div()), _pos, 2);	//CHAR	2	5	거래유형업무	SRA_TS_TP_BNE
		addAN("S000", _pos, 4);				//CHAR	4	7	신한그룹사코드	SRA_SHP_CO_CD
		addAN(pointSwapInfo.getTrans_id(), _pos, 21);				//CHAR	21	11	거래고유번호	SRA_TS_UQE_N
		addAN("V001", _pos, 4);				//CHAR	4	32	전문버전	SRA_TG_VER
		addAN("", _pos, 4);					//CHAR	4	36	처리결과코드	SRA_PS_UCD
		addAN("", _pos, 100);				//CHAR	100	40	처리결과메시지	SRA_PS_UCD_MSG
		addAN(pointSwapInfo.getUser_ci(), _pos, 88);				//CHAR	88	140	아이핀연계번호	SRA_IPN_LIK_N
		addAN("L01", _pos, 3);				//CHAR	3	228	포인트종류코드	SRA_PNT_KCD
		addAN("ZG", _pos, 2);				//CHAR	2	231	포인트종류상세	SRA_PNT_KD_DL
		addN("", _pos, 10);				//NUMBER	10	233	사용가능포인트	SE_BE_PNT
		addN("", _pos, 10);				//NUMBER	10	243	총누적포인트	TO_CUM_PNT
		addN("", _pos, 10);				//NUMBER	10	253	총사용포인트	TO_SE_PNT
		addN("", _pos, 10);				//NUMBER	10	263	총소멸포인트	TO_EXI_PNT
		addN("", _pos, 10);				//NUMBER	10	273	소멸예정포인트	EXI_DU_PNT
		addAN("", _pos, 3);				//CHAR	3	283	회원상태코드	CUS_SCD
		
		if(TrType.USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
			addN(""+pointSwapInfo.getReq_point(), _pos, 10);				//NUMBER	10	286	포인트요청금액	SRA_PNT_SE_RQA
		} else if(TrType.CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())){
			addN("", _pos, 10);				//NUMBER	10	286	포인트요청금액	SRA_PNT_SE_RQA
		} else {
			addN("", _pos, 10);				//NUMBER	10	286	포인트요청금액	SRA_PNT_SE_RQA
		}
		addN("", _pos, 10);				//NUMBER	10	296	포인트승인금액	SRA_PNT_SE_APVA
		
		if(TrType.CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())
				|| TrType.NET_CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())){
			addN(""+pointSwapInfo.getReq_point(), _pos, 10);				//NUMBER	10	306	포인트취소요청금액	SRA_PNT_SE_CE_RQA
		} else {
			addN("", _pos, 10);				//NUMBER	10	306	포인트취소요청금액	SRA_PNT_SE_CE_RQA
		}
		
		addN("", _pos, 10);				//NUMBER	10	316	포인트취소승인금액	SRA_PNT_SE_CE_APVA
		
		if(TrType.GET == TrType.getEnum(pointSwapInfo.getTrans_div())) {
			addAN("", _pos, 8);				//CHAR	8	326	포인트제휴일자	SRA_PNT_SE_D
			addAN("", _pos, 6);				//CHAR	6	334	포인트제휴시간	SRA_PNT_SE_TM
		} else {
			addAN(pointSwapInfo.getTrans_req_date().substring(0, 8), _pos, 8);				//CHAR	8	326	포인트제휴일자	SRA_PNT_SE_D
			addAN(pointSwapInfo.getTrans_req_date().substring(8, 14), _pos, 6);				//CHAR	6	334	포인트제휴시간	SRA_PNT_SE_TM		
		}

		addAN("", _pos, 8);				//CHAR	8	340	포인트사용승인일자	SRA_PNT_SE_APV_D
		
		if(TrType.GET == TrType.getEnum(pointSwapInfo.getTrans_div())) {
			addAN("", _pos, 3);				//CHAR	3	348	포인트사용/적립유형코드	SRA_PNT_TCD
		} else {
			addAN("G71", _pos, 3);				//CHAR	3	348	포인트사용/적립유형코드	SRA_PNT_TCD
		}
		
		addAN("", _pos, 9);				//CHAR	9	351	포인트적립상세사유코드	SRA_PNT_ACM_RN_LCD
		addAN("FAN", _pos, 3);				//CHAR	3	360	포인트거래채널경로	SRA_PNT_TS_CHL_PH

		addAN(this.getVarProp(PropKey.TELE_MY_SYSTEM_ID), _pos, 10);				//CHAR	10	363	제휴처코드	SRA_UGL_CD
		addAN("kt CLiP", _pos, 50);				//CHAR	50	373	제휴처명	SRA_UGL_NM
		addAN(pointSwapInfo.getCust_id_org(), _pos, 10);				//CHAR	10	423	거래요청자	SRA_TS_RQK
		addN("", _pos, 10);				//NUMBER	10	433	잔여포인트	SRA_MA_PNT
		
		if(TrType.CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())
				|| TrType.NET_CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())){
			addAN(pointSwapInfo.getRef_trans_id(), _pos, 21);				//CHAR	21	443	원거래고유번호	SRA_R_TS_UQE_N
		} else {
			addAN("", _pos, 21);				//CHAR	21	443	원거래고유번호	SRA_R_TS_UQE_N
		}
		
		addAN("", _pos, 100);				//CHAR	100	464	제휴사영역(메모)	RG_MTT
		addAN("", _pos, 436);				//CHAR	436	564	필러	FER
		//--------------- 바디 : e ---------------------
		
		logger.debug(new String(pointSwapInfo.getReq_msg_bytes()));
		logger.debug("pointSwapInfo.getReq_msg_bytes().length = "+pointSwapInfo.getReq_msg_bytes().length);
	}
	
	/*
	NUMBER 	4 		전문길이 LEN '1000' 고정
	CHAR	5	0	전문번호	SRA_TG_N	N	"20710(포인트조회)
												20210(포인트적립승인요청) -> 20220(응답)
												20310(포인트사용승인요청) -> 20320(응답)"
	CHAR	2	5	거래유형업무	SRA_TS_TP_BNE	N	"71 포인트조회(탈회여부 상관없이)
												72 회원상태조회(탈회여부 등)
												20 적립
												29 적립취소
												30 사용
												39 사용취소(망취소)"
	CHAR	4	7	신한그룹사코드	SRA_SHP_CO_CD	N	"요청-S000통합리워드
												응답-S000통합리워드"
	CHAR	21	11	거래고유번호	SRA_TS_UQE_N	N	"조회: 일자(8)+그룹사코드(4)+'1'(포인트사용거래)+SEQENCE(8)
												적립: 일자(8)+그룹사코드(4)+'2'(포인트사용거래)+SEQENCE(8)
												사용: 일자(8)+그룹사코드(4)+'3'(포인트사용거래)+SEQENCE(8)
												응답시에는 요청받은 정보를 그대로 전송 EX) 20160512S000300000001
												seq8= HH24MISSFF"
	CHAR	4	32	전문버전	SRA_TG_VER	N	V001
	CHAR	4	36	처리결과코드	SRA_PS_UCD	Y	별도첨부-옆시트
	CHAR	100	40	처리결과메시지	SRA_PS_UCD_MSG	Y	처리결과코드의 오류내역을 설명
	CHAR	88	140	아이핀연계번호	SRA_IPN_LIK_N	N	FAN클럽가입회원
	CHAR	3	228	포인트종류코드	SRA_PNT_KCD	N	L01' 마이신한포인트
	CHAR	2	231	포인트종류상세	SRA_PNT_KD_DL	N	- 통합리워드 : 그룹사코드 = 'S000' then 'ZG'
	NUMBER	10	233	사용가능포인트	SE_BE_PNT	Y	사용가능한포인트 -예) 50000은 0000050000으로 표기
	NUMBER	10	243	총누적포인트	TO_CUM_PNT	Y	요청고객의 총누적포인트
	NUMBER	10	253	총사용포인트	TO_SE_PNT	Y	요청고객의 총사용포인트
	NUMBER	10	263	총소멸포인트	TO_EXI_PNT	Y	요청고객의 총소멸포인트
	NUMBER	10	273	소멸예정포인트	EXI_DU_PNT	Y	통합1단계에서는 적용사항없음-추후
	CHAR	3	283	회원상태코드	CUS_SCD	Y	"01:신규
												02:변경 혹은 재가입
												03:탈회
												99: 미가입회원"
	NUMBER	10	286	포인트요청금액	SRA_PNT_SE_RQA	N	"거래요청코드39(포인트사용취소)인경우 0000000000
														예) 50000은 0000050000으로 표기"
	NUMBER	10	296	포인트승인금액	SRA_PNT_SE_APVA	Y	"요청시 0000000000
														응답시 실제처리금액"
	NUMBER	10	306	포인트취소요청금액	SRA_PNT_SE_CE_RQA	N	"거래요청코드30(포인트사용)인경우 0000000000
														포인트사용취소시 부분사용취소불가
														예) 50000만원취소건의 경우 0000050000으로표기"
	NUMBER	10	316	포인트취소승인금액	SRA_PNT_SE_CE_APVA	Y	"요청시 0000000000
																응답시 실제처리금액"
	CHAR	8	326	포인트제휴일자	SRA_PNT_SE_D	N	계열사 사용일자
	CHAR	6	334	포인트제휴시간	SRA_PNT_SE_TM	N	계열사 사용시간
	CHAR	8	340	포인트사용승인일자	SRA_PNT_SE_APV_D	Y	주관사 포인트승인일자
	CHAR	3	348	포인트사용/적립유형코드	SRA_PNT_TCD	N	"※ 코드값
															사용방법코드(PNT_SE_MCD)
															G70 : 다이소포인트전환사용
															G71 : KT클립포인트전환사용
															G72 : 네이버포인트전환사용
															G73 : GS포인트전환사용"
	CHAR	9	351	포인트적립상세사유코드	SRA_PNT_ACM_RN_LCD	N	통합리워드앱에서 정의하는값
	CHAR	3	360	포인트거래채널경로	SRA_PNT_TS_CHL_PH	N	"FAN"-판클럽
	CHAR	10	363	제휴처코드	SRA_UGL_CD	N	"0000000000"-코드정의
	CHAR	50	373	제휴처명	SRA_UGL_NM	N	"다이소포인트전환사용
												KT클립포인트전환사용
												네이버포인트전환사용
												GS포인트전환사용"
	CHAR	10	423	거래요청자	SRA_TS_RQK	N	"거래를 발생시킨 작업명 또는 담당자사번
													혹은 배치ID/ 온라인서비스ID"
	NUMBER	10	433	잔여포인트	SRA_MA_PNT	Y	사용후잔여포인트 -예) 50000은 0000050000으로 표기
	CHAR	21	443	원거래고유번호	SRA_R_TS_UQE_N	Y	일반사용의 경우 SPACE, 취소시 거래고유번호 필수
	CHAR	100	464	제휴사영역(메모)	RG_MTT	N	"제휴사 데이터키값 관리를 위하여 확보
													사용쪽만: 사용메모에 작성예정"
	CHAR	436	564	필러	FER	N	SPACE PADDING
	 */
	
	private void parseResMsg(){
		
		try {
			pointSwapInfo.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			//체크를 위해 일단 키를 조회
			String trId = getAN(15, 21).trim();			//SRA_TS_UQE_N	
			String userCi = getAN(144, 88).trim();		//SRA_IPN_LIK_N
			String resCode = getAN(40, 4).trim();		//SRA_PS_UCD
			String resMessage = getAN(44, 100).trim();	//SRA_PS_UCD_MSG
				
			String avail_point = getN(237, 10).trim();	//SE_BE_PNT
			String use_point = getN(300, 10).trim();	//SRA_PNT_SE_APVA					//NUMBER	10	296	포인트승인금액	SRA_PNT_SE_APVA
			
			if(TrType.USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
				use_point = getN(300, 10).trim();	//SRA_PNT_SE_APVA
			} else if(TrType.CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())
					|| TrType.NET_CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())){
				use_point = getN(320, 10).trim();	//SRA_PNT_SE_CE_APVA
			}
			
			if(!pointSwapInfo.getTrans_id().equals(trId)){
				throw new Exception("Transaction id is not equal!!!");
			}
			
			String approve_no = "";	//승인번호
			String approve_date = getAN(344, 8).trim();	//	SRA_PNT_SE_APV_D 승인일자
			//String approve_time = getAN(, 10).trim();	//승인시간
	
			logger.debug(trId+" // "+userCi+" // "+ resCode + "//"+avail_point+ " // "+use_point+ " // "+approve_no+ " // "+approve_date);
			
			pointSwapInfo.setRes_code(resCode);
			if("0000".equals(pointSwapInfo.getRes_code())) {
				pointSwapInfo.setResult(Consts.RESULT_SUCCESS);
				pointSwapInfo.setRes_point(Integer.parseInt(use_point));
				pointSwapInfo.setAvail_point(Integer.parseInt(avail_point));
				
				pointSwapInfo.setApprove_no(approve_no);
				pointSwapInfo.setApprove_date(approve_date);
			} else if("0371".equals(pointSwapInfo.getRes_code())) {
				pointSwapInfo.setResult(Consts.RESULT_FAIL);
				pointSwapInfo.setRes_message("신한카드 포인트 전환은 1일 1회만 가능합니다.");
			} else {
				pointSwapInfo.setResult(Consts.RESULT_FAIL);
				pointSwapInfo.setRes_message(resMessage);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			pointSwapInfo.setResult(Consts.RESULT_FAIL);
			pointSwapInfo.setRes_message(e.getMessage());
		}
		
		//TODO 필요한 정보만 추출해서 pointSwapInfo 에 셋팅
	}
	
	private void makeFanCheckReqMsg(){	
		IntHolder _pos = new IntHolder();			//전문 포지션
		initReqMsg(ShinhanConsts.REQ_TOT_MSG_LEN);	//신한은 1000 바이트 고정
		
		//--------------- 바디 : s ---------------------
		addAN("1000", _pos, 4);		//NUMBER	4	0	전문길이	LEN
		addAN("20310", _pos, 5);		//CHAR	5	0	전문번호	SRA_TG_N
		addAN("72", _pos, 2);	//CHAR	2	5	거래유형업무	SRA_TS_TP_BNE //72 회원상태조회(탈회여부 등)
		addAN("S000", _pos, 4);				//CHAR	4	7	신한그룹사코드	SRA_SHP_CO_CD
		addAN(pointSwapInfo.getTrans_id(), _pos, 21);				//CHAR	21	11	거래고유번호	SRA_TS_UQE_N
		addAN("V001", _pos, 4);				//CHAR	4	32	전문버전	SRA_TG_VER
		addAN("", _pos, 4);					//CHAR	4	36	처리결과코드	SRA_PS_UCD
		addAN("", _pos, 100);				//CHAR	100	40	처리결과메시지	SRA_PS_UCD_MSG
		addAN(pointSwapInfo.getUser_ci(), _pos, 88);				//CHAR	88	140	아이핀연계번호	SRA_IPN_LIK_N
		addAN("L01", _pos, 3);				//CHAR	3	228	포인트종류코드	SRA_PNT_KCD
		addAN("ZG", _pos, 2);				//CHAR	2	231	포인트종류상세	SRA_PNT_KD_DL
		addN("", _pos, 10);				//NUMBER	10	233	사용가능포인트	SE_BE_PNT
		addN("", _pos, 10);				//NUMBER	10	243	총누적포인트	TO_CUM_PNT
		addN("", _pos, 10);				//NUMBER	10	253	총사용포인트	TO_SE_PNT
		addN("", _pos, 10);				//NUMBER	10	263	총소멸포인트	TO_EXI_PNT
		addN("", _pos, 10);				//NUMBER	10	273	소멸예정포인트	EXI_DU_PNT
		addAN("", _pos, 3);				//CHAR	3	283	회원상태코드	CUS_SCD
		
		addN("", _pos, 10);				//NUMBER	10	286	포인트요청금액	SRA_PNT_SE_RQA
		addN("", _pos, 10);				//NUMBER	10	296	포인트승인금액	SRA_PNT_SE_APVA
		
		addN("", _pos, 10);				//NUMBER	10	306	포인트취소요청금액	SRA_PNT_SE_CE_RQA
		addN("", _pos, 10);				//NUMBER	10	316	포인트취소승인금액	SRA_PNT_SE_CE_APVA
		
		addAN("", _pos, 8);				//CHAR	8	326	포인트제휴일자	SRA_PNT_SE_D
		addAN("", _pos, 6);				//CHAR	6	334	포인트제휴시간	SRA_PNT_SE_TM

		addAN("", _pos, 8);				//CHAR	8	340	포인트사용승인일자	SRA_PNT_SE_APV_D
		
		addAN("", _pos, 3);				//CHAR	3	348	포인트사용/적립유형코드	SRA_PNT_TCD
		addAN("", _pos, 9);				//CHAR	9	351	포인트적립상세사유코드	SRA_PNT_ACM_RN_LCD
		addAN("FAN", _pos, 3);				//CHAR	3	360	포인트거래채널경로	SRA_PNT_TS_CHL_PH

		addAN(this.getVarProp(PropKey.TELE_MY_SYSTEM_ID), _pos, 10);				//CHAR	10	363	제휴처코드	SRA_UGL_CD
		addAN("kt CLiP", _pos, 50);				//CHAR	50	373	제휴처명	SRA_UGL_NM
		addAN("", _pos, 10);				//CHAR	10	423	거래요청자	SRA_TS_RQK
		addN("", _pos, 10);				//NUMBER	10	433	잔여포인트	SRA_MA_PNT
		addAN("", _pos, 21);				//CHAR	21	443	원거래고유번호	SRA_R_TS_UQE_N
		addAN("", _pos, 100);				//CHAR	100	464	제휴사영역(메모)	RG_MTT
		addAN("", _pos, 436);				//CHAR	436	564	필러	FER
		//--------------- 바디 : e ---------------------
		
		logger.debug(new String(pointSwapInfo.getReq_msg_bytes()));
		logger.debug("pointSwapInfo.getReq_msg_bytes().length = "+pointSwapInfo.getReq_msg_bytes().length);
	}
	
	private void parseFanCheckResMsg(){
		try {
		 
			pointSwapInfo.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			//체크를 위해 일단 키를 조회
			String trId = getAN(15, 21).trim();			//SRA_TS_UQE_N	
			String userCi = getAN(144, 88).trim();		//SRA_IPN_LIK_N
			String resCode = getAN(40, 4).trim();		//SRA_PS_UCD
			String resMessage = getAN(44, 100).trim();	//SRA_PS_UCD_MSG
				
			logger.debug(trId+" // "+userCi+" // "+ resCode + "//"+resMessage);
			
			if(!pointSwapInfo.getTrans_id().equals(trId)){
				throw new Exception("Transaction id is not equal!!!");
			}
			
			pointSwapInfo.setRes_code(resCode);
			if("0000".equals(pointSwapInfo.getRes_code())) {
				pointSwapInfo.setResult("Y");
			} else if ("1001".equals(pointSwapInfo.getRes_code())) {
				pointSwapInfo.setResult("N");
				pointSwapInfo.setRes_message(resMessage);
			} else {
				pointSwapInfo.setResult(Consts.RESULT_FAIL);
				pointSwapInfo.setRes_message(resMessage);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			pointSwapInfo.setResult(Consts.RESULT_FAIL);
			pointSwapInfo.setRes_message(e.getMessage());
		}
	}
	
}
