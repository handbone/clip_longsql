package com.coopnc.cardpoint.handler;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.cardpoint.consts.Consts;
import com.coopnc.cardpoint.consts.Consts.BcConsts;
import com.coopnc.cardpoint.consts.Consts.PropKey;
import com.coopnc.cardpoint.consts.Consts.TrType;
import com.coopnc.cardpoint.domain.FtpFileInfo;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.netty.NettyClient_BC;
import com.coopnc.cardpoint.netty.component.ResponseFuture;
import com.coopnc.cardpoint.util.FileUtil;
import com.coopnc.cardpoint.util.IntHolder;
import com.coopnc.cardpoint.util.MessageUtil;

public class PointHandler_BC extends PointSwapServiceHandler implements PointSwapService {

	private static Logger logger = LoggerFactory.getLogger(PointHandler_BC.class);
	
	public PointHandler_BC(PointSwapInfo pointSwapInfo) {
		this.pointSwapInfo = pointSwapInfo;
		init();
	}
	
	void init() {
		
		this.systemName = BcConsts.system_name;
		this.serviceIp = this.getVarProp(PropKey.SERVER_IP);
		this.serverPort = Integer.parseInt(this.getVarProp(PropKey.SERVER_PORT));
		this.timeout = Integer.parseInt(this.getVarProp(PropKey.SERVER_TIMEOUT));
		
		//tele log file path
		if(this.getVarProp(PropKey.TELE_LOG_PATH) != null)
			this.teleLogPath = this.getVarProp(PropKey.TELE_LOG_PATH) + "/"  + "telelog_"+MessageUtil.getDateStr("yyyyMMdd") + ".log";

	}

	@Override
	public void getPoint() {
		pointSwapInfo.setTrans_div(TrType.GET.getValue());
		
		makeGetReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_BC conn = new NettyClient_BC(serviceIp, serverPort, 422, true);

			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		parseGetResMsg();
		writeTeleLog(1);
	}

	@Override
	public void usePoint() {
		pointSwapInfo.setTrans_div(TrType.USE.getValue());
		
		makeUseReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_BC conn = new NettyClient_BC(serviceIp, serverPort, 422, false);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseUseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void cancelUse() {
		pointSwapInfo.setTrans_div(TrType.CANCEL_USE.getValue());
		
		makeUseReqMsg();
		writeTeleLog(0);

		try {
			NettyClient_BC conn = new NettyClient_BC(serviceIp, serverPort, 422, false);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());

			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));
			//conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseUseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void netCancelUse() {
		pointSwapInfo.setTrans_div(TrType.NET_CANCEL_USE.getValue());
		
		makeUseReqMsg();
		writeTeleLog(0);

		try {
			NettyClient_BC conn = new NettyClient_BC(serviceIp, serverPort, 305, false);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseUseResMsg();
		writeTeleLog(1);
	}
	
	private void initReqMsg(int totMsgLen){
		pointSwapInfo.setReq_msg_bytes(new byte[totMsgLen]);
		
		//메세지 초기화 - 모두 스페이스로 채움
		byte[] bb = " ".getBytes();
		for (int i = 0 ; i < pointSwapInfo.getReq_msg_bytes().length ; i++){
			pointSwapInfo.getReq_msg_bytes()[i] = bb[0];
		}
	}
	
	private void initReqMsg(byte[] data){
		//메세지 초기화 - 모두 스페이스로 채움
		byte[] bb = " ".getBytes();
		for (int i = 0 ; i < data.length ; i++){
			data[i] = bb[0];
		}
	}
	
	private void makeGetReqMsg(){
		
		IntHolder _pos = new IntHolder();	//전문 포지션

		initReqMsg(BcConsts.GET_REQ_MSG_LEN);
		
		addAN("0201", _pos, 4);		//NUMBER	4	0	전문길이	LEN
		addAN("", _pos, 9);	//Transaction Code	AN	9 	4 
		addAN("BCC", _pos, 3);	//TEXT 개시문자	AN	3 	13 
		addN("6406", _pos, 4);	//전문코드	N	4 	16 
		addAN(pointSwapInfo.getTrans_id(), _pos, 12);	//거래고유번호	ANS	12 	20 
		addAN(pointSwapInfo.getTrans_req_date(), _pos, 14);	//전문전송일시	N	14 	32 
		addAN("00", _pos, 2);	//응답코드	AN	2 	46 
		addAN("63", _pos, 2);	//회원사번호	N	2 	48 
		addAN("", _pos, 16);	//예비	ANS	16 	50 
		addAN("", _pos, 2);	//조회회원사번호	ANS	2 	66
		
		addAN("8", _pos, 1);	//주체구분		1 	68 
		addAN(pointSwapInfo.getUser_ci(), _pos, 88);	//주체번호		88 	69 
		addAN("000000", _pos, 6);	//포인트유형번호		6 	157 
		addAN("63", _pos, 2);	//업무구분		2 	163 
		addAN("", _pos, 40);	//공란		40 	165
		
		/*
		NUMBER	4	0	전문길이	LEN
		Transaction Code	AN	9 	4 
		TEXT 개시문자	AN	3 	13 
		전문코드	N	4 	16 
		거래고유번호	ANS	12 	20 
		전문전송일시	N	14 	32 
		응답코드	AN	2 	46 
		회원사번호	N	2 	48 
		예비	ANS	16 	50 
		조회회원사번호	ANS	2 	66 
		주체구분		1 	68 
		주체번호		88 	69 
		포인트유형번호		6 	157 
		업무구분		2 	163 
		공란		40 	165 
		*/
		
		logger.debug("[" + new String(pointSwapInfo.getReq_msg_bytes()) + "]");
		logger.debug("pointSwapInfo.getReq_msg_bytes().length = "+pointSwapInfo.getReq_msg_bytes().length);
	}
	

	
	private void makeUseReqMsg(){
		
		IntHolder _pos = new IntHolder();	//전문 포지션

		initReqMsg(BcConsts.USE_TOT_MSG_LEN);
		
		addAN("0338", _pos, 4);		//NUMBER	4	0	전문길이	LEN
		addAN("", _pos, 9);	//Transaction Code	AN	9 	4 
		addAN("BCC", _pos, 3);	//TEXT 개시문자	AN	3 	13 
		addN("6408", _pos, 4);	//전문코드	N	4 	16 
		addAN(pointSwapInfo.getTrans_id(), _pos, 12);	//거래고유번호	ANS	12 	20 
		addAN(pointSwapInfo.getTrans_req_date(), _pos, 14);	//전문전송일시	N	14 	32 
		addAN("00", _pos, 2);	//응답코드	AN	2 	46 
		addAN("63", _pos, 2);	//회원사번호	N	2 	48 
		addAN("", _pos, 16);	//예비	ANS	16 	50 
		addAN("", _pos, 2);	//조회회원사번호	ANS	2 	66
		
		addAN("8", _pos, 1);	//주체구분		1 	68 
		addAN(pointSwapInfo.getUser_ci(), _pos, 88);	//주체번호		88 	69 
		
		if(TrType.USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
			addAN("05", _pos, 2);	//거래구분		2 	157 (05: 사용, 15: 사용취소, 01: 적립, 11: 적립취소)
		} else if(TrType.NET_CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
			addAN("15", _pos, 2);	//거래구분		2 	157 (05: 사용, 15: 사용취소, 01: 적립, 11: 적립취소)
		} else if(TrType.CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
			addAN("15", _pos, 2);	//거래구분		2 	157 (05: 사용, 15: 사용취소, 01: 적립, 11: 적립취소)
		} else {
			addAN("00", _pos, 2);	// 그 외
		}
		
		addAN("000000", _pos, 6);	//포인트유형번호		6 	159 
		addAN("2317", _pos, 4);	//포인트유형구분		4 	165 (추후 변경 필요) 
		addAN("63", _pos, 2);	//업무구분		2 	169 
		addN(""+pointSwapInfo.getReq_point(), _pos, 15);	//거래요청포인트		15 	171 
		addAN(pointSwapInfo.getTrans_id(), _pos, 17);	//거래요청번호		17 	186
		
		if(TrType.NET_CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div()) ||
				TrType.CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
			addAN(pointSwapInfo.getApprove_no(), _pos, 10);	//접수일련번호		10 	203 (취소인 경우 필수)
		} else {
			addAN("", _pos, 10);	//접수일련번호		10 	203
		}
		
		addAN("0", _pos, 1);	//인증값구분		1 	213 
		addAN("", _pos, 88);	//인증값		88 	214 
		addAN("", _pos, 3);	//회원사		3 	302 
		addAN(MessageUtil.getDateStr(new Date(), "yyyyMMdd"), _pos, 8);		//거래요청일자	8	305
		addAN("", _pos, 29);	//공란		29 	313

		/*
		NUMBER	4	0	전문길이	LEN
		Transaction Code	AN	9	4
		TEXT 개시문자	AN	3	13 
		전문코드	N	4	16 
		거래고유번호	ANS	12	20 
		전문전송일시	N	14	32 
		응답코드	AN	2	46 
		회원사번호	N	2	48 
		예비	ANS	16	50 
		조회회원사번호	ANS	2	66 
		주체구분		1 	68 
		주체번호		88 	69 
		거래구분		2 	157 
		포인트유형번호		6 	159 
		포인트유형구분		4 	165 
		업무구분		2 	169 
		거래요청포인트		15 	171 
		거래요청번호		17 	186 
		접수일련번호		10 	203 
		인증값구분		1 	213 
		인증값		88 	214 
		회원사		3 	302 
		거래요청일자	8	305
		공란		29 	313 
		*/

		logger.debug("[" + new String(pointSwapInfo.getReq_msg_bytes()) + "]");
		logger.debug("pointSwapInfo.getReq_msg_bytes().length = "+pointSwapInfo.getReq_msg_bytes().length);
	}

	
	private void parseGetResMsg(){
		try {
			pointSwapInfo.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			//체크를 위해 일단 키를 조회
			String trId = getAN(20, 12).trim();			//거래고유번호	
			String userCi = pointSwapInfo.getUser_ci();	//응답에서 오지 않음
			String resCode = getAN(205, 4).trim();		//응답코드
			String resMessage = getAN(209, 40).trim();	//응답메세지
				
			String avail_point = getAN(294, 15).trim();	//AVL_COIN
			//String use_point = getN(396, 10).trim();	//COIN
			
			//String approve_no = getAN(492, 10).trim();	//승인번호
			//String approve_date = getAN(484, 8).trim();	//승인일자
			//String approve_time = getAN(502, 10).trim();	//승인시간
	
			logger.debug(trId+" // "+userCi+" // "+ resCode + "//" + resMessage + "//"+avail_point);
			
			if(!pointSwapInfo.getTrans_id().equals(trId)){
				throw new Exception("Transaction id is not equal!!!");
			}
			
			pointSwapInfo.setRes_code(resCode);
			if("0000".equals(pointSwapInfo.getRes_code())) {
				pointSwapInfo.setResult(Consts.RESULT_SUCCESS);
				pointSwapInfo.setRes_point(0);
				pointSwapInfo.setAvail_point(Integer.parseInt(avail_point));
				
				pointSwapInfo.setApprove_no("");
				pointSwapInfo.setApprove_date("");
			} else {
				pointSwapInfo.setResult(Consts.RESULT_FAIL);
				pointSwapInfo.setRes_message(resMessage);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			pointSwapInfo.setResult(Consts.RESULT_FAIL);
			pointSwapInfo.setRes_message(e.getMessage());
		}
		
		/*
		NUMBER	4	0	전문길이	LEN
		Transaction Code	AN	9 	4 
		TEXT 개시문자	AN	3 	13 
		전문코드	N	4 	16 
		거래고유번호	ANS	12 	20 
		전문전송일시	N	14 	32 
		응답코드	AN	2 	46 
		회원사번호	N	2 	48 
		예비	ANS	16 	50 
		조회회원사번호	ANS	2 	66 
		주체구분		1 	68 
		주체번호		88 	69 
		포인트유형번호		6 	157 
		업무구분		2 	163 
		공란		40 	165 
		
		응답코드		4 	205 
		응답메시지		40 	209 
		총건수		5 	249 
		포인트유형명		40 	254 
		사용가능포인트		15 	294 
		*/
	}

	private void parseUseResMsg(){
		try {
			pointSwapInfo.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			//체크를 위해 일단 키를 조회
			String trId = getAN(20, 12).trim();			//거래고유번호
			String userCi = pointSwapInfo.getUser_ci();	//응답에서 오지 않음
			String resCode = getAN(342, 4).trim();		//응답코드
			String resMessage = getAN(346, 40).trim();	//응답메세지
				
			String avail_point = getAN(411, 15).trim();	//AVL_COIN
			String use_point = getAN(396, 15).trim();	//COIN
			
			String approve_no = getAN(386, 10).trim();	//접수일련번호
			//String approve_date = getAN(484, 8).trim();	//승인일자
			//String approve_time = getAN(502, 10).trim();	//승인시간
	
			logger.debug(trId+" // "+userCi+" // "+ resCode + "//" + resMessage + "//"+avail_point+ " // "+use_point+ " // "+approve_no);
			
			if(!pointSwapInfo.getTrans_id().equals(trId)){
				throw new Exception("Transaction id is not equal!!!");
			}
			
			pointSwapInfo.setRes_code(resCode);
			if("0000".equals(pointSwapInfo.getRes_code())) {
				pointSwapInfo.setResult(Consts.RESULT_SUCCESS);
				pointSwapInfo.setRes_point(pointSwapInfo.getReq_point());	// 사용포인트가 내려오지 않아 요청 포인트 그대로 설정
				pointSwapInfo.setAvail_point(Integer.parseInt(avail_point));
				
				pointSwapInfo.setApprove_no(approve_no);
				pointSwapInfo.setApprove_date("");
			} else {
				pointSwapInfo.setResult(Consts.RESULT_FAIL);
				pointSwapInfo.setRes_message(resMessage);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			pointSwapInfo.setResult(Consts.RESULT_FAIL);
			pointSwapInfo.setRes_message(e.getMessage());
		}
		
		
		/*
		NUMBER	4	0	전문길이	LEN
		Transaction Code	AN	9	4
		TEXT 개시문자	AN	3	13 
		전문코드	N	4	16 
		거래고유번호	ANS	12	20 
		전문전송일시	N	14	32 
		응답코드	AN	2	46 
		회원사번호	N	2	48 
		예비	ANS	16	50 
		조회회원사번호	ANS	2	66 
		주체구분		1 	68 
		주체번호		88 	69 
		거래구분		2 	157 
		포인트유형번호		6 	159 
		포인트유형구분		4 	165 
		업무구분		2 	169 
		거래요청포인트		15 	171 
		거래요청번호		17 	186 
		접수일련번호		10 	203 
		인증값구분		1 	213 
		인증값		88 	214 
		회원사		3 	302 
		거래요청일자	8	305
		공란		29 	313 

		응답코드		4 	342 
		응답메시지		40 	346 
		접수일련번호		10 	386 
		접수포인트		15 	396 
		거래후잔여포인트		15 	411 
		*/
	}

	/**
	 * 대사용 파일 작성
	 */
	@Override
	public void makeLocalRecordFile(FtpFileInfo info, List<PointSwapInfo> pointSwapInfoList){
		/*: COP0005_33_YYYYMMDD000000_10.sam => 대사화일 
        : COP0005_33_YYYYMMDD000000_10.sam.END => 완료체크용 화일(0 BYTE) 
        + "/" + "COP0005_33_"+MessageUtil.getDateStr("yyyyMMdd")+"000000_10.sam";
        */
		String localFileName = "BC_"+MessageUtil.getDateStr("yyyyMMdd")+".sam";
		info.setLocalFileName(localFileName);
		info.setLocalFilePath(this.getVarProp(PropKey.LOCAL_UP_DIR));
		String localFileFullPath = info.getLocalFilePath()+"/"+info.getLocalFileName();
		File file = new File(localFileFullPath);
		if(file.exists())
			file.delete();

		info.setServerFileName(localFileName);
		info.setServerFilePath(this.getVarProp(PropKey.FTP_UP_DIR));
		
		int recordLen = 200;

		byte[] header = new byte[recordLen];
		IntHolder _pos = new IntHolder(0);
		initReqMsg(header);

		addAN(header, "H", _pos, 1);					//Record_구분		A	1	0
		addAN(header, MessageUtil.getDateStr("yyyyMMdd"), _pos, 8);					//작업일자		AN	8	1
		//FILLER		AN	191	9
		
		FileUtil.appendNoLine(localFileFullPath, new String(header));
		
		int cnt = 0;
		for(int i = 0 ; i< pointSwapInfoList.size(); i++){

			PointSwapInfo data = pointSwapInfoList.get(i);
			
			if(!"0000".equals(data.getRes_code()))
				continue;

			byte[] body = new byte[recordLen];
			_pos.value = 0;
			initReqMsg(body);

			addAN(body, "D", _pos, 1);					//Record_구분		A	1	0
			addAN(body, data.getTrans_req_date().substring(0, 8), _pos, 8);					//처리일자		AN	8	1
			addAN(body, "000000", _pos, 6);				//포인트 유형 번호	AN	6	9
			addAN(body, "2317", _pos, 4);				//포인트 유형 구분	AN	4	15	(추후 변경 필요)
			addAN(body, data.getApprove_no(), _pos, 10);					//포인트 사용 접수 일련 번호		AN	10	19
			addAN(body, data.getTrans_id(), _pos, 17);					//거래요청번호		AN	17	29
			addN(body, ""+data.getReq_point(), _pos, 9);	//거래_포인트		N	9	46
			addAN(body, data.getUser_ci(), _pos, 88);					//CI		AN	88	55
			
			if(TrType.CANCEL_USE == TrType.getEnum(data.getTrans_div()) || TrType.NET_CANCEL_USE == TrType.getEnum(data.getTrans_div())) {
				addAN(body, "15", _pos, 2);					//거래_구분		AN	2	143
			} else if(TrType.USE == TrType.getEnum(data.getTrans_div())) {
				addAN(body, "05", _pos, 2);	
			} else {
				continue;
			}
			
			addAN(body, "1", _pos, 1);		//채널구분		AN	1	145
			// 공란	AN 54	146
			
			//FILLER		AN	538	362
			FileUtil.appendNoLine(localFileFullPath, new String(body));
			cnt ++;
		}

		byte[] tail = new byte[recordLen];
		_pos.value = 0;
		initReqMsg(tail);
		
		addAN(tail, "T", _pos, 1);					//Record_구분		A	1	0
		addN(tail, ""+cnt, _pos, 8);			//건수		N	8	1
		//FILLER		AN	191	9
		
		FileUtil.appendNoLine(localFileFullPath, new String(tail));
		
	}
	
	@Override
	public List<PointSwapInfo> parseRemoteRecordFile(FtpFileInfo ftpFileInfo) {
		String localFileName = "BJ714.F76.D"+MessageUtil.getDateStr("yyyyMMdd");
		ftpFileInfo.setLocalFileName(localFileName);
		ftpFileInfo.setLocalFilePath(this.getVarProp(PropKey.LOCAL_DOWN_DIR));
		String localFileFullPath = ftpFileInfo.getLocalFilePath()+"/"+ftpFileInfo.getLocalFileName();
		logger.info("BC file path:"+localFileFullPath);
		File file = new File(localFileFullPath);
		if(!file.exists()) {
			logger.info("BC file is not exist : " + localFileFullPath);
			return null;
		}
		
		List<PointSwapInfo> result = new ArrayList<PointSwapInfo>();
		
		BufferedInputStream br = null;
		String line = null;
//		BufferedReader fin = null;
		
		String fileName = ftpFileInfo.getLocalFileName();
		
		int totCount = 0;
		try
		{
			br = new BufferedInputStream(new FileInputStream(file));
			byte[] s = new byte[200];
			
			while (br.read(s, 0, s.length) > 0) {
				line = new String(s);
				
				if(line.startsWith("D")) {
					PointSwapInfo data = procRow(line);
					result.add(data);
					totCount++;
				}
			}
//			fin = new BufferedReader(new FileReader(localFileFullPath));
//			line = fin.readLine();
//
//			while( line != null )
//			{
//				if(line.startsWith("H")) {
//					line = fin.readLine();
//					continue;
//				}
//				
//				//System.out.println(line);
//				if(line.startsWith("T")){       
//					break;
//				}
//
//				PointSwapInfo data = procRow(line);
//				result.add(data);
//				
//				totCount++;
//				
//				line = fin.readLine();
//			}

			logger.info("parse file rows ["+ totCount +"] : "+fileName  );
			
		} catch( Exception e ) {
			e.printStackTrace();
			logger.info("error " +fileName+" : "+ e.getMessage());
		} finally {
			try {
				if (br != null) br.close();
//				if( fin != null )
//					fin.close();
			}
			catch( Exception e ) {
			}
		}
		
		return result;
	}
	
	@Override
	public List<PointSwapInfo> parseRemoteRecordFileDate(FtpFileInfo ftpFileInfo, String date) {
		String localFileName = "BJ714.F76.D"+date;
		ftpFileInfo.setLocalFileName(localFileName);
		ftpFileInfo.setLocalFilePath(this.getVarProp(PropKey.LOCAL_DOWN_DIR));
		String localFileFullPath = ftpFileInfo.getLocalFilePath()+"/"+ftpFileInfo.getLocalFileName();
		logger.info("BC file path:"+localFileFullPath);
		File file = new File(localFileFullPath);
		if(!file.exists()) {
			logger.info("BC file is not exist : " + localFileFullPath);
			return null;
		}
		
		List<PointSwapInfo> result = new ArrayList<PointSwapInfo>();
		
		BufferedInputStream br = null;
		String line = null;
//		BufferedReader fin = null;
		
		String fileName = ftpFileInfo.getLocalFileName();
		
		int totCount = 0;
		try
		{
			br = new BufferedInputStream(new FileInputStream(file));
			byte[] s = new byte[200];
			
			while (br.read(s, 0, s.length) > 0) {
				line = new String(s);
				
				if(line.startsWith("D")) {
					PointSwapInfo data = procRow(line);
					result.add(data);
					totCount++;
				}
			}
//			fin = new BufferedReader(new FileReader(localFileFullPath));
//			line = fin.readLine();
//
//			while( line != null )
//			{
//				if(line.startsWith("H")) {
//					line = fin.readLine();
//					continue;
//				}
//				
//				//System.out.println(line);
//				if(line.startsWith("T")){       
//					break;
//				}
//
//				PointSwapInfo data = procRow(line);
//				result.add(data);
//				
//				totCount++;
//				
//				line = fin.readLine();
//			}

			logger.info("parse file rows ["+ totCount +"] : "+fileName  );
			
		} catch( Exception e ) {
			e.printStackTrace();
			logger.info("error " +fileName+" : "+ e.getMessage());
		} finally {
			try {
				if (br != null) br.close();
//				if( fin != null )
//					fin.close();
			}
			catch( Exception e ) {
			}
		}
		
		return result;
	}
	
	private PointSwapInfo procRow(String line) {
		PointSwapInfo info = new PointSwapInfo();
		
		logger.info("BC procRow : [" + line + "]");
		byte[] data = line.getBytes();

		//Record_구분		A	1	0	Record_DV
		info.setTrans_req_date(getAN(data, 1, 8));	//거래_일자		AN	8	1	DE_DT
		info.setApprove_no(getAN(data, 19, 10).trim());				//멤버십_승인_번호		AN	10	19	MEB_APR_NO
		info.setTrans_id(getAN(data, 29, 17).trim());			//제휴사_거래_추적_번호		AN	17	29	CCO_DE_FLW_NO
		info.setReq_point(Integer.parseInt(getN(data, 46, 9)));  		//거래_포인트		N	9	46	DE_PNT
		info.setUser_ci(getAN(data, 55, 88));			//CI		AN	88	55	CI
		
		String trDiv = getAN(data, 143, 2);			//업무_구분_코드		AN	2	143	BIZ_DV
		if(trDiv.equals("05"))
			info.setTrans_div(TrType.USE.getValue());
		else if(trDiv.equals("15"))
			info.setTrans_div(TrType.NET_CANCEL_USE.getValue());
		else {
			info.setTrans_div(TrType.USE.getValue());
		}

		return info;
	}
	
}
