package com.coopnc.cardpoint.handler;

import java.util.List;

import com.coopnc.cardpoint.domain.FtpFileInfo;
import com.coopnc.cardpoint.domain.PointSwapInfo;

public interface PointSwapService {
	
	public void getPoint();
	
	public void usePoint();
	
	public void cancelUse();
	
	public void netCancelUse();
	
	public void makeLocalRecordFile(FtpFileInfo info, List<PointSwapInfo> pointSwapInfoList);
	
	public List<PointSwapInfo> parseRemoteRecordFile(FtpFileInfo info);
	
	public List<PointSwapInfo> parseRemoteRecordFileDate(FtpFileInfo info, String date);
	
	public int putRecordFile(FtpFileInfo info) throws Exception;
	
	public int getRecordFile(FtpFileInfo info) throws Exception;
	
	public void checkFanUser();
	
}
