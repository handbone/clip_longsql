package com.coopnc.cardpoint.handler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.cardpoint.consts.Consts;
import com.coopnc.cardpoint.consts.Consts.HanaConsts;
import com.coopnc.cardpoint.consts.Consts.PropKey;
import com.coopnc.cardpoint.consts.Consts.TrType;
import com.coopnc.cardpoint.domain.FtpFileInfo;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.netty.NettyClient_HANA;
import com.coopnc.cardpoint.netty.component.ResponseFuture;
import com.coopnc.cardpoint.util.FileUtil;
import com.coopnc.cardpoint.util.FtpUtil;
import com.coopnc.cardpoint.util.IntHolder;
import com.coopnc.cardpoint.util.MessageUtil;

public class PointHandler_HANA extends PointSwapServiceHandler implements PointSwapService {

	private static Logger logger = LoggerFactory.getLogger(PointHandler_HANA.class);
	
	public PointHandler_HANA(PointSwapInfo pointSwapInfo) {
		this.pointSwapInfo = pointSwapInfo;
		init();
	}
	
	void init() {
		
		this.systemName = HanaConsts.system_name;
		this.serviceIp = this.getVarProp(PropKey.SERVER_IP);
		this.serverPort = Integer.parseInt(this.getVarProp(PropKey.SERVER_PORT));
		this.timeout = Integer.parseInt(this.getVarProp(PropKey.SERVER_TIMEOUT));
		
		//tele log file path
		if(this.getVarProp(PropKey.TELE_LOG_PATH) != null)
			this.teleLogPath = this.getVarProp(PropKey.TELE_LOG_PATH) + "/"  + "telelog_"+MessageUtil.getDateStr("yyyyMMdd") + ".log";

	}

	@Override
	public void getPoint() {
		pointSwapInfo.setTrans_div(TrType.GET.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_HANA conn = new NettyClient_HANA(serviceIp, serverPort);

			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		parseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void usePoint() {
		pointSwapInfo.setTrans_div(TrType.USE.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_HANA conn = new NettyClient_HANA(serviceIp, serverPort);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void cancelUse() {
		pointSwapInfo.setTrans_div(TrType.CANCEL_USE.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_HANA conn = new NettyClient_HANA(serviceIp, serverPort);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());

			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void netCancelUse() {
		pointSwapInfo.setTrans_div(TrType.NET_CANCEL_USE.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_HANA conn = new NettyClient_HANA(serviceIp, serverPort);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseResMsg();
		writeTeleLog(1);
	}

	@Override
	public int putRecordFile(FtpFileInfo ftpFileInfo) throws Exception {
		
		int result = FtpUtil.sftpPutFile(this.getVarProp(PropKey.FTP_IP), 
				Integer.parseInt(this.getVarProp(PropKey.FTP_PORT)), 
				this.getVarProp(PropKey.FTP_ID), 
				this.getVarProp(PropKey.FTP_PASS), 
				ftpFileInfo.getLocalFilePath()+"/"+ftpFileInfo.getLocalFileName(), 
				ftpFileInfo.getServerFilePath(),
				ftpFileInfo.getServerFileName(), 5000);
		
		if(result == 1) {
			makeZeroFile(ftpFileInfo.getLocalFilePath()+"/"+ftpFileInfo.getLocalFileName()+".END");
	
			//0바이트 파일 작성
			int result0 = FtpUtil.sftpPutFile(this.getVarProp(PropKey.FTP_IP), 
					Integer.parseInt(this.getVarProp(PropKey.FTP_PORT)), 
					this.getVarProp(PropKey.FTP_ID), 
					this.getVarProp(PropKey.FTP_PASS), 
					ftpFileInfo.getLocalFilePath()+"/"+ftpFileInfo.getLocalFileName()+".END", 
					ftpFileInfo.getServerFilePath(),
					ftpFileInfo.getServerFileName()+".END", 5000);
		}
		
		return result;
	}
	
	private void makeZeroFile(String fileName){
		FileWriter fw = null;
		try {
			// 파일 객체 생성
	        File file = new File(fileName);
	        if(file.exists())
	        	file.delete();

	        fw = new FileWriter(new File(fileName)) ;
			fw.write(" ");
	        fw.flush();
	        
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(fw !=null)
					fw.close();
			} catch (Exception e){}
			
		}
	}

	@Override
	public int getRecordFile(FtpFileInfo ftpFileInfo) throws Exception {
			
		String standDate = ftpFileInfo.getStandDate();
		//
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date date = null;
		try {
			date = format.parse(standDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		Calendar cal= Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		String fileDate=format.format((cal.getTime()));
		
		
		String localFileName = this.getVarProp(PropKey.TELE_MY_SYSTEM_ID)+"_34_"+fileDate+"000000"+"_20.sam";
		//String localFileName = this.getVarProp(PropKey.TELE_MY_SYSTEM_ID)+"_34_"+MessageUtil.getDateStr("yyyyMMdd")+"000000"+"_20.sam";
		ftpFileInfo.setLocalFileName(localFileName);
		ftpFileInfo.setLocalFilePath(this.getVarProp(PropKey.LOCAL_DOWN_DIR));

		ftpFileInfo.setServerFileName(localFileName);
		ftpFileInfo.setServerFilePath(this.getVarProp(PropKey.FTP_DOWN_DIR));

		int result = FtpUtil.sftpGetFile(this.getVarProp(PropKey.FTP_IP), 
				Integer.parseInt(this.getVarProp(PropKey.FTP_PORT)), 
				this.getVarProp(PropKey.FTP_ID), 
				this.getVarProp(PropKey.FTP_PASS), 
				ftpFileInfo.getLocalFilePath()+"/"+ftpFileInfo.getLocalFileName(), 
				ftpFileInfo.getServerFilePath(),
				ftpFileInfo.getServerFileName(), 5000);
		
		return result;
	}
	
	private void initReqMsg(int totMsgLen){
		pointSwapInfo.setReq_msg_bytes(new byte[totMsgLen]);
		
		//메세지 초기화 - 모두 스페이스로 채움
		byte[] bb = " ".getBytes();
		for (int i = 0 ; i < pointSwapInfo.getReq_msg_bytes().length ; i++){
			pointSwapInfo.getReq_msg_bytes()[i] = bb[0];
		}
	}
	
	private void initReqMsg(byte[] data){
		//메세지 초기화 - 모두 스페이스로 채움
		byte[] bb = " ".getBytes();
		for (int i = 0 ; i < data.length ; i++){
			data[i] = bb[0];
		}
	}
	
	private void makeReqMsg(){	
		IntHolder _pos = new IntHolder();	//전문 포지션

		initReqMsg(HanaConsts.REQ_TOT_MSG_LEN);
		
		addN(Integer.toString(HanaConsts.REQ_TOT_MSG_LEN - 4), _pos, 4);	//GRAM_LNTH	전문_길이		AN	4
		
		//--------------- 헤더 ---------------------
		//'"IF_COP_101_"+전송시스템+"_"+  수신시스템	예)  "IF_COP_101_COP0001_HFG9000"
		addAN("IF_COP_101_"+this.getVarProp(PropKey.TELE_MY_SYSTEM_ID)+"_HFG9000", _pos, 40);						//IF_ID	인터페이스_ID		AN	40
		
		addAN(this.getVarProp(PropKey.TELE_MY_SYSTEM_ID), _pos, 7);				//TRS_SYS	전송_시스템		AN	7
		addAN("HFG9000", _pos, 7);				//RCV_SYS	수신_시스템		AN	7
		addAN("960", _pos, 3);						//GRAM_TP	전문_유형		AN	3
		addAN("96", _pos, 2);						//BIZ_DV	업무_구분		AN	2
		addAN("V100", _pos, 4);					//GRAM_VER	전문_버전		AN	4
		addAN(this.getVarProp(PropKey.TELE_MY_SYSTEM_ID), _pos, 7);				//CCO_C	제휴사_코드		AN	7
		addAN(MessageUtil.getDateStr("yyyyMMdd"), _pos, 8);		//AK_DT	요청_일자		AN	8
		addAN(MessageUtil.getDateStr("HHmmss"), _pos, 6);		//AK_HR	요청_시간		AN	6
		
		//'제휴사에서 UNIQUE하게 채번	예) 제휴사코드(7)+거래일자(8)+거래일련번호(10)+공백 (망 취소 시 Key값)
		addAN(pointSwapInfo.getTrans_id(), _pos, 50);		//FLW_NO	추적_번호		AN	50
		addAN("ONL", _pos, 3);					//ONL_OFF_DV	온_오프_구분		AN	3
		addAN("00000", _pos, 5);				//RSP_C	응답_코드		AN	5
		addAN("", _pos, 110);					//Filler	Filler		AN	110
		
		//--------------- 바디 ---------------------
		addAN(HanaConsts.getServiceCode(pointSwapInfo.getTrans_div()), _pos, 6);		//GRAM_TP_DV	거래_구분_코드		AN	6
		addAN(this.getVarProp(PropKey.TELE_MY_SYSTEM_ID), _pos, 20);		//MC_NO	가맹점_번호		AN	20		//"COP0008" 기프트쇼
		addAN("3", _pos, 1);						//CT_DRM_DV_C	고객_식별_구분_코드		AN	1	//"3" : CI 값 
		addAN(pointSwapInfo.getUser_ci(), _pos, 88);				//CT_DRM_DV_N	고객_식별_구분_값		AN	88
		addN(pointSwapInfo.getTrans_req_date().substring(0, 8), _pos, 8);		//DE_DT	거래_일자		N	8
		addN(pointSwapInfo.getTrans_req_date().substring(8, 14), _pos, 6);			//DE_HR	거래시간		N	6
		addAN("", _pos, 1);						//HS_CTF_DV	본인_인증_구분		AN	1	//" "(공백) - 미설정
		addAN("", _pos, 10);						//PSWD	비밀_번호		AN	10				//인증구분값이 " "이면 비밀번호 불필요
		
		logger.debug("pointSwapInfo.getTrans_div() : "+pointSwapInfo.getTrans_div());
		
		if(TrType.GET == TrType.getEnum(pointSwapInfo.getTrans_div())) {
			addN("0", _pos, 10);				//COIN	요청_포인트		N	10
			addN("0", _pos, 10);					//COIN_RV_OJ_AM	거래_금액		N	10
		} else {
			addN(""+pointSwapInfo.getReq_point(), _pos, 10);				//COIN	요청_포인트		N	10
			addN("0", _pos, 10);					//COIN_RV_OJ_AM	거래_금액		N	10	
		}
		
		if(TrType.CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div()) || TrType.NET_CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
			addAN("", _pos, 8);						//ORI_APR_DT	원_거래_승인일자		N	8
			addAN("", _pos, 10);						//ORI_APR_NO	원_거래_승인번호		AN	10
			addAN(pointSwapInfo.getRef_trans_id(), _pos, 50);						//ORI_DE_FLW_NO	원_거래_추적번호		AN	50
		} else {
			addAN("", _pos, 8);						//ORI_APR_DT	원_거래_승인일자		N	8
			addAN("", _pos, 10);						//ORI_APR_NO	원_거래_승인번호		AN	10
			addAN("", _pos, 50);						//ORI_DE_FLW_NO	원_거래_추적번호		AN	50
		}
		
		addAN("", _pos, 8);						//APR_DT	승인_일자		N	8 <-- 여기서는 SPACE로 채움
		addAN("", _pos, 10);						//APR_NO	승인_번호		AN	10	<-- 여기서는 SPACE로 채움
		addAN("", _pos, 10);						//AVL_COIN	가용_포인트		N	10 <-- 여기서는 SPACE로 채움
		addAN("", _pos, 1);						//OTP_YN	OTP 요청 여부		AN	1 <-- 여기서는 SPACE로 채움
		addAN("", _pos, 1);						//OTB_YN	OTB 요청 여부		AN	1 <-- 여기서는 SPACE로 채움
		addAN("", _pos, 642);					//FILLER	Filler		AN	642
		
		logger.debug(new String(pointSwapInfo.getReq_msg_bytes()));
		logger.debug("pointSwapInfo.getReq_msg_bytes().length = "+pointSwapInfo.getReq_msg_bytes().length);
	}
	
	/* 요청 전문
	GRAM_LNTH	전문_길이	4	0	AN
	IF_ID	인터페이스_ID	40	4	AN
	TRS_SYS	전송_시스템	7	44	AN
	RCV_SYS	수신_시스템	7	51	AN
	GRAM_TP	전문_유형	3	58	AN
	BIZ_DV	업무_구분	2	61	AN
	GRAM_VER	전문_버전	4	63	AN
	CCO_C	제휴사_코드	7	67	AN
	AK_DT	요청_일자	8	74	AN
	AK_HR	요청_시간	6	82	AN
	FLW_NO	추적_번호	50	88	AN
	ONL_OFF_DV	온_오프_구분	3	138	AN
	RSP_C	응답_코드	5	141	AN
	Filler	Filler	110	146	AN
	GRAM_TP_DV	거래_구분_코드	6	256	AN
	MC_NO	가맹점_번호	20	262	AN
	CT_DRM_DV_C	고객_식별_구분_코드	1	282	AN
	CT_DRM_DV_N	고객_식별_구분_값	88	283	AN
	DE_DT	거래_일자	8	371	N
	DE_HR	거래시간	6	379	N
	HS_CTF_DV	본인_인증_구분	1	385	AN
	PSWD	비밀_번호	10	386	AN
	COIN	요청_포인트	10	396	N
	COIN_RV_OJ_AM	거래_금액	10	406	N
	ORI_APR_DT	원_거래_승인일자	8	416	N
	ORI_APR_NO	원_거래_승인번호	10	424	AN
	ORI_DE_FLW_NO	원_거래_추적번호	50	434	AN
	APR_DT	승인_일자	8	484	N
	APR_NO	승인_번호	10	492	AN
	AVL_COIN	가용_포인트	10	502	N
	OTP_YN	OTP 요청 여부	1	512	AN
	OTB_YN	OTB 요청 여부	1	513	AN
	FILLER	Filler	642	514	AN
	*/
	
	private void parseResMsg(){
		try {
			pointSwapInfo.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			//체크를 위해 일단 키를 조회
			String trId = getAN(88, 50).trim();			//FLW_NO	
			String userCi = getAN(283, 88).trim();		//CT_DRM_DV_N
			String resCode = getAN(141, 5).trim();		//RSP_C
			String resMessage = getAN(512, 150).trim();	//RSP_MSG
				
			String avail_point = getN(502, 10).trim();	//AVL_COIN
			String use_point = getN(396, 10).trim();	//COIN
			
			String approve_no = getAN(492, 10).trim();	//승인번호
			String approve_date = getAN(484, 8).trim();	//승인일자
			String approve_time = getAN(492, 6).trim();	//승인시간
	
			logger.debug(trId+" // "+userCi+" // "+ resCode + "//"+avail_point+ " // "+use_point+ " // "+approve_no+ " // "+approve_date);
			
			if(!pointSwapInfo.getTrans_id().equals(trId)){
				throw new Exception("Transaction id is not equal!!!");
			}
			
			pointSwapInfo.setRes_code(resCode);
			if("00000".equals(pointSwapInfo.getRes_code())) {
				pointSwapInfo.setResult(Consts.RESULT_SUCCESS);
				pointSwapInfo.setRes_point(Integer.parseInt(use_point));
				pointSwapInfo.setAvail_point(Integer.parseInt(avail_point));
				
				pointSwapInfo.setApprove_no(approve_no);
				pointSwapInfo.setApprove_date(approve_date);
				pointSwapInfo.setApprove_time(approve_time);
			} else {
				pointSwapInfo.setResult(Consts.RESULT_FAIL);
				pointSwapInfo.setRes_message(resMessage);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			pointSwapInfo.setResult(Consts.RESULT_FAIL);
			pointSwapInfo.setRes_message(e.getMessage());
		}
		
		//TODO 필요한 정보만 추출해서 pointSwapInfo 에 셋팅
	}

	/*	응답 전문
	GRAM_LNTH	전문_길이	4	0
	IF_ID	인터페이스_ID	40	4
	TRS_SYS	전송_시스템	7	44
	RCV_SYS	수신_시스템	7	51
	GRAM_TP	전문_유형	3	58
	BIZ_DV	업무_구분	2	61
	GRAM_VER	전문_버전	4	63
	CCO_C	제휴사_코드	7	67
	AK_DT	요청_일자	8	74
	AK_HR	요청_시간	6	82
	FLW_NO	추적_번호	50	88
	ONL_OFF_DV	온_오프_구분	3	138
	RSP_C	응답_코드	5	141
	Filler	Filler	110	146
			256
	
	GRAM_TP_DV	거래_구분_코드	6	256
	MC_NO	가맹점_번호	20	262
	CT_DRM_DV_C	고객_식별_구분_코드	1	282
	CT_DRM_DV_N	고객_식별_구분_값	88	283
	DE_DT	거래_일자	8	371
	DE_HR	거래시간	6	379
	HS_CTF_DV	본인_인증_구분	1	385
	PSWD	비밀_번호	10	386
	COIN	요청_포인트	10	396
	COIN_RV_OJ_AM	거래_금액	10	406
	ORI_APR_DT	원_거래_승인일자	8	416
	ORI_APR_NO	원_거래_승인번호	10	424
	ORI_DE_FLW_NO	원_거래_추적번호	50	434
	APR_DT	승인_일자	8	484
	APR_NO	승인_번호	10	492
	AVL_COIN	가용_포인트	10	502
	RSP_MSG	응답_메시지	150	512
	CELL_BZMAN_NO	휴대폰_사업자_번호	3	662
	CELL_NO	휴대폰_국번	4	665
	CELL_IDV_NO	휴대폰_개별_번호	4	669
	MEB_CRDCD_PSS_YN	멤버십신용카드소지여부	1	673
	MEB_CHKCD_PSS_YN	멤버십체크카드소지여부	1	674
	HN_CRDCD_PSS_YN	하나신용카드소지여부	1	675
	HN_CHKCD_PSS_YN	하나체크카드소지여부	1	676
	OTB	OTB	16	677
	FILLER	Filler	463	693
			900	

	*/
	
	/**
	 * 대사용 파일 작성
	 */
	@Override
	public void makeLocalRecordFile(FtpFileInfo info, List<PointSwapInfo> pointSwapInfoList){
		/*: COP0005_33_YYYYMMDD000000_10.sam => 대사화일 
        : COP0005_33_YYYYMMDD000000_10.sam.END => 완료체크용 화일(0 BYTE) 
        + "/" + "COP0005_33_"+MessageUtil.getDateStr("yyyyMMdd")+"000000_10.sam";
        */
		
		String standDate = info.getStandDate();
		//
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date date = null;
		try {
			date = format.parse(standDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		Calendar cal= Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		String fileDate=format.format((cal.getTime()));
		
		//
		String localFileName = this.getVarProp(PropKey.TELE_MY_SYSTEM_ID)+"_33_"+fileDate+"000000"+"_10.sam";
		//String localFileName = this.getVarProp(PropKey.TELE_MY_SYSTEM_ID)+"_33_"+MessageUtil.getDateStr("yyyyMMdd")+"000000"+"_10.sam";
		info.setLocalFileName(localFileName);
		info.setLocalFilePath(this.getVarProp(PropKey.LOCAL_UP_DIR));
		String localFileFullPath = info.getLocalFilePath()+"/"+info.getLocalFileName();
		File file = new File(localFileFullPath);
		if(file.exists())
			file.delete();

		info.setServerFileName(localFileName);
		info.setServerFilePath(this.getVarProp(PropKey.FTP_UP_DIR));
		
		int recordLen = 900;

		byte[] header = new byte[recordLen];
		IntHolder _pos = new IntHolder(0);
		initReqMsg(header);

		addAN(header, "H", _pos, 1);					//Record_구분		A	1	0
		addAN(header, "000000000", _pos, 9);					//일련_번호		N	9	1
		addAN(header, this.getVarProp(PropKey.TELE_MY_SYSTEM_ID), _pos, 7);					//제휴사_코드		AN	7	10
		addAN(header, localFileName, _pos, 35);					//생성_파일_명		AN	35	17
		addAN(header, MessageUtil.getDateStr("yyyyMMddHHmmss"), _pos, 14);					//생성_일자		AN	14	52
		//FILLER		AN	834	66
		
		FileUtil.append(localFileFullPath, new String(header));
		
		int cnt = 0;
		for(int i = 0 ; i< pointSwapInfoList.size(); i++){

			PointSwapInfo data = pointSwapInfoList.get(i);
			
			if(!"00000".equals(data.getRes_code()))
				continue;

			byte[] body = new byte[recordLen];
			_pos.value = 0;
			initReqMsg(body);

			addAN(body, "D", _pos, 1);					//Record_구분		A	1	0
			addN(body, ""+(i+1), _pos, 9);				//일련_번호		N	9	1
			addAN(body, data.getApprove_date(), _pos, 8);					//멤버십_승인_일자		AN	8	10
			addAN(body, data.getApprove_time(), _pos, 6);					//멤버십_승인_시간		AN	6	18
			addAN(body, data.getApprove_no(), _pos, 10);					//멤버십_승인_번호		AN	10	24
			addAN(body, data.getTrans_id(), _pos, 50);					//멤버십_거래_추적_번호		AN	50	34
			addAN(body, "", _pos, 8);					//제휴사_승인_일자		AN	8	84
			addAN(body, "", _pos, 6);					//제휴사_승인_시간		AN	6	92
			addAN(body, "", _pos, 30);					//제휴사_승인_번호		AN	30	98
			addAN(body, data.getTrans_id(), _pos, 50);					//제휴사_거래_추적_번호		AN	50	128
		
			//System.out.println(" TYPE : "+data.getTrans_div());
			if(TrType.CANCEL_USE == TrType.getEnum(data.getTrans_div()) || TrType.NET_CANCEL_USE == TrType.getEnum(data.getTrans_div())) {
				addAN(body, "32", _pos, 2);					//업무_구분_코드		AN	2	178
			} else if(TrType.USE == TrType.getEnum(data.getTrans_div())) {
				addAN(body, "31", _pos, 2);	
			} else {
				continue;
			}
			
			addAN(body, data.getTrans_req_date().substring(0, 8), _pos, 8);					//거래_일자		AN	8	180
			addAN(body, data.getTrans_req_date().substring(8, 14), _pos, 6);					//거래_시간		AN	6	188
			addN(body, ""+data.getReq_point(), _pos, 12);	//거래_포인트		N	12	194
			addAN(body, data.getUser_ci(), _pos, 88);					//CI		AN	88	206
			addAN(body, "", _pos, 8);					//원_승인_일자		AN	8	294
			addAN(body, "", _pos, 10);					//원_승인_번호		AN	10	302
			
			if(TrType.CANCEL_USE == TrType.getEnum(data.getTrans_div()) || TrType.NET_CANCEL_USE == TrType.getEnum(data.getTrans_div())) {
				addAN(body, data.getRef_trans_id(), _pos, 50);					//원_추적_번호		AN	50	312
			} else {
				addAN(body, "", _pos, 50);					//원_추적_번호		AN	50	312
			}
			
			//FILLER		AN	538	362
			FileUtil.append(localFileFullPath, new String(body));
			cnt ++;
		}

		byte[] tail = new byte[recordLen];
		_pos.value = 0;
		initReqMsg(tail);
		
		addAN(tail, "T", _pos, 1);					//Record_구분		A	1	0
		addAN(tail, "999999999", _pos, 9);			//일련_번호		N	9	1
		addAN(tail, this.getVarProp(PropKey.TELE_MY_SYSTEM_ID), _pos, 7);					//제휴사_코드		AN	7	10
		addN(tail, ""+cnt, _pos, 7);					//총_Data_Record_수		N	7	17
		//FILLER		AN	876	24
		
		FileUtil.append(localFileFullPath, new String(tail));
		
	}
	
	public List<PointSwapInfo> parseRemoteRecordFile(FtpFileInfo ftpFileInfo){
		
		/*String standDate = ftpFileInfo.getStandDate();
		String localFileName = this.getVarProp(PropKey.TELE_MY_SYSTEM_ID)+"_34_"+MessageUtil.getDateStr("yyyyMMdd")+"000000"+"_20.sam";
		ftpFileInfo.setLocalFileName(localFileName);
		ftpFileInfo.setLocalFilePath(this.getVarProp(PropKey.LOCAL_DOWN_DIR));*/
		
		String localFileFullPath = ftpFileInfo.getLocalFilePath()+"/"+ftpFileInfo.getLocalFileName();
		File file = new File(localFileFullPath);
		if(!file.exists())
			return null;
		
		List<PointSwapInfo> result = new ArrayList<PointSwapInfo>();
		
		String line = null;
		BufferedReader fin = null;
		
		String fileName = ftpFileInfo.getLocalFileName();
		
		int totCount = 0;
		try
		{
			fin = new BufferedReader(new FileReader(localFileFullPath));
			line = fin.readLine();

			while( line != null )
			{
				if(line.startsWith("H")) {
					line = fin.readLine();
					continue;
				}
				
				//System.out.println(line);
				if(line.startsWith("T")){       
					break;
				}

				PointSwapInfo data = procRow(line);
				result.add(data);
				
				totCount++;
				
				line = fin.readLine();
			}

			logger.info("parse file rows ["+ totCount +"] : "+fileName  );
			
		} catch( Exception e ) {
			e.printStackTrace();
			logger.info("error " +fileName+" : "+ e.getMessage());
		} finally {
			try {
				if( fin != null )
					fin.close();
			}
			catch( Exception e ) {
			}
		}
		
		return result;
	}
	
	private PointSwapInfo procRow(String line){
		PointSwapInfo info = new PointSwapInfo();
		
		byte[] data = line.getBytes();

		//Record_구분		A	1	0	Record_DV
		info.setSerial_no(Integer.parseInt(getN(data, 1, 9)));	//일련_번호		N	9	1	SEQ_NO
		info.setApprove_date(getAN(data, 10, 8));				//멤버십_승인_일자		AN	8	10	MEB_APR_DT
		info.setApprove_time(getAN(data, 18, 6));				//멤버십_승인_시간		AN	6	18	MEB_APR_HR
		info.setApprove_no(getAN(data, 24, 10).trim());				//멤버십_승인_번호		AN	10	24	MEB_APR_NO
		//멤버십_거래_추적_번호		AN	50	34	MEB_DE_FLW_NO
		//제휴사_승인_일자		AN	8	84	CCO_APR_DT
		//제휴사_승인_시간		AN	6	92	CCO_APR_HR
		//제휴사_승인_번호		AN	30	98	CCO_APR_NO
		info.setTrans_id(getAN(data, 128, 50).trim());			//제휴사_거래_추적_번호		AN	50	128	CCO_DE_FLW_NO
		
		String trDiv = getAN(data, 178, 2);			//업무_구분_코드		AN	2	178	BIZ_DV
		if(trDiv.equals("31"))
			info.setTrans_div(TrType.USE.getValue());
		else if(trDiv.equals("32"))
			info.setTrans_div(TrType.NET_CANCEL_USE.getValue());
		else {
			info.setTrans_div(TrType.USE.getValue());
		}
		
		info.setTrans_req_date(getAN(data, 180, 14));	//거래_일자		AN	8	180	DE_DT
														//거래_시간		AN	6	188	DE_HR
		info.setReq_point(Integer.parseInt(getN(data, 194, 12)));  		//거래_포인트		N	12	194	DE_PNT
		info.setUser_ci(getAN(data, 206, 88));			//CI		AN	88	206	CI
		//원_승인_일자		AN	8	294	ORI_APR_DT
		//원_승인_번호		AN	10	302	ORI_APR_NO
		//원_추적_번호		AN	50	312	ORI_FLW_NO

		return info;
	}
	
}
