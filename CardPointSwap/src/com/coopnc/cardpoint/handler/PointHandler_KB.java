package com.coopnc.cardpoint.handler;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.cardpoint.consts.Consts;
import com.coopnc.cardpoint.consts.Consts.KbConsts;
import com.coopnc.cardpoint.consts.Consts.PropKey;
import com.coopnc.cardpoint.consts.Consts.TrType;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.netty.NettyClient_KB;
import com.coopnc.cardpoint.netty.component.ResponseFuture;
import com.coopnc.cardpoint.util.IntHolder;
import com.coopnc.cardpoint.util.MessageUtil;

public class PointHandler_KB extends PointSwapServiceHandler implements PointSwapService {

	private static Logger logger = LoggerFactory.getLogger(PointHandler_KB.class);
	
	public PointHandler_KB(PointSwapInfo pointSwapInfo) {
		this.pointSwapInfo = pointSwapInfo;
		init();
	}
	
	void init() {
		
		this.systemName = KbConsts.system_name;
		this.serviceIp = this.getVarProp(PropKey.SERVER_IP);
		this.serverPort = Integer.parseInt(this.getVarProp(PropKey.SERVER_PORT));
		this.timeout = Integer.parseInt(this.getVarProp(PropKey.SERVER_TIMEOUT));
		
		//tele log file path
		if(this.getVarProp(PropKey.TELE_LOG_PATH) != null)
			this.teleLogPath = this.getVarProp(PropKey.TELE_LOG_PATH) + "/"  + "telelog_"+MessageUtil.getDateStr("yyyyMMdd") + ".log";

	}

	@Override
	public void getPoint() {
		pointSwapInfo.setTrans_div(TrType.GET.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_KB conn = new NettyClient_KB(serviceIp, serverPort);

			ResponseFuture res = null;
			
			res = conn.send(pointSwapInfo.getReq_msg_bytes());
			
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		parseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void usePoint() {
		pointSwapInfo.setTrans_div(TrType.USE.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_KB conn = new NettyClient_KB(serviceIp, serverPort);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void cancelUse() {
		pointSwapInfo.setTrans_div(TrType.CANCEL_USE.getValue());
		
		makeReqMsg();
		writeTeleLog(0);

		try {
			NettyClient_KB conn = new NettyClient_KB(serviceIp, serverPort);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseResMsg();
		writeTeleLog(1);
	}

	@Override
	public void netCancelUse() {
		pointSwapInfo.setTrans_div(TrType.NET_CANCEL_USE.getValue());
		
		makeReqMsg();
		writeTeleLog(0);
		try {
			NettyClient_KB conn = new NettyClient_KB(serviceIp, serverPort);
			
			ResponseFuture res = conn.send(pointSwapInfo.getReq_msg_bytes());
			pointSwapInfo.setRes_msg_bytes((byte[])res.get(timeout, TimeUnit.MILLISECONDS));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		parseResMsg();
		writeTeleLog(1);
	}
	
	private void initReqMsg(int totMsgLen){
		pointSwapInfo.setReq_msg_bytes(new byte[totMsgLen]);
		
		//메세지 초기화 - 모두 스페이스로 채움
		byte[] bb = " ".getBytes();
		for (int i = 0 ; i < pointSwapInfo.getReq_msg_bytes().length ; i++){
			pointSwapInfo.getReq_msg_bytes()[i] = bb[0];
		}
	}
	
	private void makeReqMsg() {	
		IntHolder _pos = new IntHolder();	//전문 포지션

		initReqMsg(KbConsts.REQ_TOT_MSG_LEN);
		
		addAN("0200", _pos, 4);		// 전문길이

		if(TrType.GET == TrType.getEnum(pointSwapInfo.getTrans_div())) {
			/** ------------------- 조회 --------------------**/
			addAN("020", _pos, 3);		// CHAR	3 	0	전문번호
			addAN("KT", _pos, 2);	// CHAR	2 	3 	회원사코드
			addAN(pointSwapInfo.getTrans_id(), _pos, 10);		// CHAR	10 	5 	전문일련번호
			addAN(pointSwapInfo.getTrans_req_date().substring(0,8), _pos, 8);	// CHAR	8 	15 	전송일자
			addAN(pointSwapInfo.getTrans_req_date().substring(8,14), _pos, 6);	// CHAR	6 	23 	전송시간
			addAN("", _pos, 2);	// CHAR	2 	29 	응답코드
			
			addAN("", _pos, 19);	// CHAR	19 	31 	예약부
			addAN("", _pos, 13);	// CHAR	13 	50 	공백
			addAN("", _pos, 20);	// CHAR	20 	63 	카드번호
			addAN("", _pos, 1);	// CHAR	1 	83 	POINT  부호 (POINT 의   부호  + /-  )  
			addN("0", _pos, 10);	// NUM 	10 	84 	POINT  값 (Default 0)  
			addAN(pointSwapInfo.getUser_ci(), _pos, 88);	// CHAR	88 	94 	CI번호
			addAN("", _pos, 18);	// CHAR	18 	182 	공백
			
		} else {
			
			/** ------------------- 이용/취소 ----------------**/
			addAN("032", _pos, 3);	// CHAR	3 	0	전문번호
			addAN("KT", _pos, 2);	// CHAR	2 	3 	회원사코드
			addAN(pointSwapInfo.getTrans_id(), _pos, 10);	// CHAR	10 	5 	전문일련번호
			addAN(pointSwapInfo.getTrans_req_date().substring(0,8), _pos, 8);	// CHAR	8 	15 	전송일자
			addAN(pointSwapInfo.getTrans_req_date().substring(8,14), _pos, 6);	// CHAR	6 	23 	전송시간
			addAN("", _pos, 2);	// CHAR	2 	29 	응답코드
			addAN("", _pos, 19);	// CHAR	19 	31 	예약부
			
			addAN("", _pos, 20);	// CHAR	20 	50 	카드번호
			if(TrType.USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
				addAN("1", _pos, 1);	// CHAR	1 	70 	작업구분( 1. 이용 , 3. 이용취소 , 5. 과거이용취소)
			} else if(TrType.NET_CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
				addAN("3", _pos, 1);	// CHAR	1 	70 	작업구분( 1. 이용 , 3. 이용취소 , 5. 과거이용취소)
			} else if(TrType.CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
				addAN("3", _pos, 1);	// CHAR	1 	70 	작업구분( 1. 이용 , 3. 이용취소 , 5. 과거이용취소)
			} else {
				addAN("5", _pos, 1);
			}
			addN(""+pointSwapInfo.getAvail_point(), _pos, 10);	// NUM 	10 	71 	이용가능 포인트
			addN(""+pointSwapInfo.getReq_point(), _pos, 10);	// NUM 	10 	81 	이용신청 포인트
			if(TrType.NET_CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div()) || TrType.CANCEL_USE == TrType.getEnum(pointSwapInfo.getTrans_div())) {
				addAN(pointSwapInfo.getRef_trans_id(), _pos, 10);	// NUM 	10 	91 	원거래일련번호
			} else {
				addAN("", _pos, 10);	// NUM 	10 	91 	원거래일련번호
			}

			addAN(pointSwapInfo.getUser_ci(), _pos, 88);	// CHAR	88 	101 	CI번호
			addAN("", _pos, 4);	// CHAR	4 	189 	카드유효기간(YYMM)
			addAN("", _pos, 7);	// CHAR	7 	193 	공백
		}
	
		logger.debug(new String(pointSwapInfo.getReq_msg_bytes()));
		logger.debug("pointSwapInfo.getReq_msg_bytes().length = "+pointSwapInfo.getReq_msg_bytes().length);
	}
	
	/* 요청 전문
	*--    공통전문  (총 200 BYTES)	속성	길이
	포인트 조회   전문 (020/021) 
	CHAR	3 	0	전문번호
	CHAR	2 	3 	회원사코드
	CHAR	10 	5 	전문일련번호
	CHAR	8 	15 	전송일자
	CHAR	6 	23 	전송시간
	CHAR	2 	29 	응답코드
	
	CHAR	19 	31 	예약부
	CHAR	13 	50 	공백
	CHAR	20 	63 	카드번호
	CHAR	1 	83 	POINT  부호 (POINT 의   부호  + /-  )  
	NUM 	10 	84 	POINT  값 (Default 0)  
	CHAR	88 	94 	CI번호
	CHAR	18 	182 	공백
	
					
			포인트이용/취소 전문 (032/033)  	속성	길이
	CHAR	3 	0	전문번호
	CHAR	2 	3 	회원사코드
	CHAR	10 	5 	전문일련번호
	CHAR	8 	15 	전송일자
	CHAR	6 	23 	전송시간
	CHAR	2 	29 	응답코드
	CHAR	19 	31 	예약부
	
	CHAR	20 	50 	카드번호
	CHAR	1 	70 	작업구분( 1. 이용 , 3. 이용취소 , 5. 과거이용취소)
	NUM 	10 	71 	이용가능 포인트
	NUM 	10 	81 	이용신청 포인트
	NUM 	10 	91 	원거래일련번호
	CHAR	88 	101 	CI번호
	CHAR	4 	189 	카드유효기간(YYMM)
	CHAR	7 	193 	공백
	*/
	
	private void parseResMsg(){
		
		try {
			pointSwapInfo.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			//체크를 위해 일단 키를 조회
			String trId = getN(9, 10).trim();			//전문일련번호
			
			String userCi = "";
			if(TrType.GET == TrType.getEnum(pointSwapInfo.getTrans_div())) {
				userCi = getAN(98, 88).trim();		//CI번호
			} else {
				userCi = getAN(105, 88).trim();		//CI번호
			}
			
			String resCode = getAN(33, 2).trim();		//응답코드
			String plus = getAN(87, 1).trim();
			String avail_point = "";
			String use_point = "";
			if(TrType.GET == TrType.getEnum(pointSwapInfo.getTrans_div())) {
				avail_point = getN(88, 10).trim();	//이용가능 포인트
				use_point = "0";	//COIN
			} else {
				avail_point = getN(75, 10).trim();	//이용가능 포인트
				use_point = getN(85, 10).trim();	//COIN
			}

			String approve_no = "";//getAN(502, 10).trim();	//승인번호
			String approve_date = "";//getAN(484, 8).trim();	//승인일자
			//String approve_time = getAN(502, 10).trim();	//승인시간
			int availPoint = Integer.parseInt(avail_point) * (plus.equals("-") ? -1 : 1);
	
			logger.debug(trId+" // "+userCi+" // "+ resCode + "//"+availPoint+ " // "+use_point+ " // "+approve_no+ " // "+approve_date);
			
			if(!pointSwapInfo.getTrans_id().equals(trId)){
				throw new Exception("Transaction id is not equal!!!");
			}
			
			pointSwapInfo.setRes_code(resCode);
			if("00".equals(resCode)) {
				pointSwapInfo.setResult(Consts.RESULT_SUCCESS);
				pointSwapInfo.setRes_point(Integer.parseInt(use_point));
				pointSwapInfo.setAvail_point(availPoint);
				
				pointSwapInfo.setApprove_no("");
				pointSwapInfo.setApprove_date("");
			} else {
				pointSwapInfo.setResult(Consts.RESULT_FAIL);
				pointSwapInfo.setRes_message(getErrorMassage(resCode));
			}
			
			//TODO 필요한 정보만 추출해서 pointSwapInfo 에 셋팅
		} catch (Exception e) {
			logger.error(e.getMessage());
			pointSwapInfo.setResult(Consts.RESULT_FAIL);
			pointSwapInfo.setRes_message(e.getMessage());
		}	
	}

	/*	응답 코드
	< 응답코드 >
	00	성공
	01	미등록 CI번호
	17	포인트이용취소 원래거래일련번호 미존재 또는 불일치
	90	카드유효기간 오류
	93	본인 카드가 아님
	94	미등록 카드번호
	95	고객정보제공 미동의 고객
	96	신청후 30 일  이전에만  정정이  가능합니다 
	97	회원별   거래정지인   경우   포인트이용 불가합니다
	98	연체 20,000 원이상인경우　포인트이용불가
	99	 포인트처리   오류입니다 
	*/
	
	private static String getErrorMassage(String kbResultCode) {
		String msg = "";
		switch(kbResultCode) {
			case "00" : msg = "성공";
				break;
			case "01" : msg = "미등록 CI번호";
				break;
			case "17" : msg = "포인트이용취소 원래거래일련번호 미존재 또는 불일치";
				break;
			case "90" : msg = "카드유효기간 오류";
				break;
			case "93" : msg = "본인 카드가 아님";
				break;
			case "94" : msg = "미등록 카드번호";
				break;
			case "95" : msg = "고객정보제공 미동의 고객";
				break;
			case "96" : msg = "신청후 30일 이전에만 정정이 가능합니다";
				break;
			case "97" : msg = "회원별 거래정지인 경우 포인트이용 불가합니다";
				break;
			case "98" : msg = "연체 20,000 원이상인경우 포인트이용불가";
				break;
			case "99" : msg = "포인트처리 오류입니다";
				break;
			default :
				break;
		}
		
		return msg;
	}
	
}
