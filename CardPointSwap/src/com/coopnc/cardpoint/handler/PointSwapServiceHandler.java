package com.coopnc.cardpoint.handler;

import java.io.File;
import java.util.List;

import com.coopnc.cardpoint.domain.FtpFileInfo;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.util.FileUtil;
import com.coopnc.cardpoint.util.IntHolder;
import com.coopnc.cardpoint.util.MessageUtil;
import com.coopnc.cardpoint.util.PropHolder;

public class PointSwapServiceHandler implements PointSwapService {

	protected PointSwapInfo pointSwapInfo;
	
	protected String systemName;
	protected String serviceIp;
	protected int serverPort;
	protected int timeout;
	
	protected String teleLogPath;
	protected String reconcileFilePath;
	
	public PointSwapServiceHandler(){}
	
	public void getPoint(){

	}
	
	public void usePoint(){

	}
	
	public void cancelUse(){

	}
	
	public void netCancelUse(){

	}
	
	public void checkFanUser(){

	}
	
	protected String getProp(String key){
		return PropHolder.getInstance().get(key);
	}
	
	protected String getVarProp(String key){
		return PropHolder.getInstance().getVar(systemName, key);
	}

	/**
	 * msg 에 pos 위치부터 len 길이 만큼 msg를 기입한다. N 은 넘버, 1 이고 길이가 4이면 0001 빈값 0
	 * @param src
	 * @param srcPos
	 * @param msg
	 * @param len
	 */
	protected void addN(String number, IntHolder _pos, int len){
		if(number == null)
			number = "";
		
		if(number.startsWith("-")) {
			number = "-" + MessageUtil.lpad(number.substring(1), len-1, "0");
		} else {
			number = MessageUtil.lpad(number, len, "0");
		}
		
		System.arraycopy(number.getBytes(), 0, pointSwapInfo.getReq_msg_bytes(), _pos.value, len);
		
		_pos.add(len);
	}
	
	protected void addN(byte[] target, String number, IntHolder _pos, int len){
		if(number == null)
			number = "";
		
		if(number.startsWith("-")) {
			number = "-" + MessageUtil.lpad(number.substring(1), len-1, "0");
		} else {
			number = MessageUtil.lpad(number, len, "0");
		}
		
		System.arraycopy(number.getBytes(), 0, target, _pos.value, len);
		
		_pos.add(len);
	}
	
	/**
	 * src 에 pos 위치부터 len 길이 만큼 msg를 기입한다. AN 은 영문숫자, 좌정렬 기본, 빈값 스페이스
	 * @param src
	 * @param srcPos
	 * @param msg
	 * @param len
	 */
	protected void addAN(String str, IntHolder _pos, int len){	
		if(str == null)
			str = "";
		
		if("".equals(str)) {
			_pos.add(len);
			return;
		}
		
		str = MessageUtil.rpad(str, len, " ");	
		System.arraycopy(str.getBytes(), 0, pointSwapInfo.getReq_msg_bytes(), _pos.value , len);
		_pos.add(len);	
	}
	
	protected void addAN(byte[] target, String str, IntHolder _pos, int len){	
		if(str == null)
			str = "";
		
		if("".equals(str)) {
			_pos.add(len);
			return;
		}
		
		str = MessageUtil.rpad(str, len, " ");	
		System.arraycopy(str.getBytes(), 0, target, _pos.value , len);
		_pos.add(len);	
	}
	
	/**
	 * src 에 pos 위치부터 len 길이 만큼 msg를 기입한다. HAN 은 한글숫자, 좌정렬 기본, 빈값 스페이스
	 * @param src
	 * @param srcPos
	 * @param msg
	 * @param len
	 */
	protected void addHAN(String str, IntHolder _pos, int len){
		if(str == null)
			str = "";
		
		if("".equals(str)) {
			_pos.add(len);
			return;
		}
		
		str = MessageUtil.rpad(str, len, " ");
		System.arraycopy(str.getBytes(), 0, pointSwapInfo.getReq_msg_bytes(), _pos.value, len);
		_pos.add(len);
	}
	
	protected void addHAN(byte[] target, String str, IntHolder _pos, int len){
		if(str == null)
			str = "";
		
		if("".equals(str)) {
			_pos.add(len);
			return;
		}
		
		str = MessageUtil.rpad(str, len, " ");
		System.arraycopy(str.getBytes(), 0, target, _pos.value, len);
		_pos.add(len);
	}
	
	/**
	 * 응답 메세지에서 필요한 부분만 추출, N 은 넘버, 1 이고 길이가 4이면 0001 빈값 0
	 * @param pos
	 * @param len
	 * @return
	 */
	protected String getN(int pos, int len) {
		try {
			String data = MessageUtil.cutStrByByteIndexLength(pointSwapInfo.getRes_msg_bytes(), pos, len);
			return MessageUtil.removeZero(data);
		} catch (Exception e) {
			return null;
		}
	}
	
	protected String getN(byte[] msg, int pos, int len) {
		try {
			String data = MessageUtil.cutStrByByteIndexLength(msg, pos, len);
			return MessageUtil.removeZero(data);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 응답 메세지에서 필요한 부분만 추출, AN 은 영문숫자, 좌정렬 기본, 빈값 스페이스
	 * @param pos
	 * @param len
	 * @return
	 */
	protected String getAN(int pos, int len) {	
		try {
			return MessageUtil.cutStrByByteIndexLength(pointSwapInfo.getRes_msg_bytes(), pos, len);
		} catch (Exception e) {
			return null;
		}
	}
	
	protected String getAN(byte[] msg, int pos, int len) {	
		try {
			return MessageUtil.cutStrByByteIndexLength(msg, pos, len);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 응답 메세지에서 필요한 부분만 추출, HAN 은 한글숫자, 좌정렬 기본, 빈값 스페이스
	 * @param pos
	 * @param len
	 * @return
	 */
	protected String getHAN(int pos, int len){
		try {
			return MessageUtil.cutStrByByteIndexLength(pointSwapInfo.getRes_msg_bytes(), pos, len);
		} catch (Exception e) {
			return null;
		}
	}
	
	protected String getHAN(byte[] msg, int pos, int len){
		try {
			return MessageUtil.cutStrByByteIndexLength(msg, pos, len);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 전문 로그
	 * @param inoutGb (0:out, 1:in)
	 */
	protected void writeTeleLog(int inoutGb) {
		String msg = "";
		if(inoutGb == 0) {
			msg = MessageUtil.getDateStr("HH:mm:ss SSS")+" REQ["+pointSwapInfo.getTrans_id()+"]["
					+ makeTelegramString(pointSwapInfo.getReq_msg_bytes())+"]";
		} else {
			msg = MessageUtil.getDateStr("HH:mm:ss SSS")+" RES["+pointSwapInfo.getTrans_id()+"]["
					+ makeTelegramString(pointSwapInfo.getRes_msg_bytes())+"]";
		}
		
		FileUtil.append(teleLogPath, msg);
	}
	
	private String makeTelegramString(byte[] bytes){
		String ret = "";
		try {
			ret = (bytes == null || bytes.length == 0) ? "" : new String(bytes);
		} catch ( Exception e ) {
			ret = "[ERROR]" + e.getMessage();
		}
		
		return ret;
	}

	/**
	 * 대사용 파일 작성
	 */
	@Override
	public void makeLocalRecordFile(FtpFileInfo info, List<PointSwapInfo> pointSwapInfoList) {
		
	}
	

	@Override
	public List<PointSwapInfo> parseRemoteRecordFile(FtpFileInfo info) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<PointSwapInfo> parseRemoteRecordFileDate(FtpFileInfo info, String date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int putRecordFile(FtpFileInfo info) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getRecordFile(FtpFileInfo info) throws Exception{
		// TODO Auto-generated method stub
		return 0;
	}

	
}
