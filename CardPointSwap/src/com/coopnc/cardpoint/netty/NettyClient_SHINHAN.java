package com.coopnc.cardpoint.netty;

import java.net.InetSocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.cardpoint.consts.Consts.ShinhanConsts;
import com.coopnc.cardpoint.netty.component.ResponseFuture;
import com.coopnc.cardpoint.util.MessageUtil;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;

public class NettyClient_SHINHAN {

	private static Logger logger = LoggerFactory.getLogger(NettyClient_SHINHAN.class);	
	private static final EventLoopGroup workerGroup = new NioEventLoopGroup(1);

	private static Channel chan = null;

	private String host;
	private int port;
	
	private Bootstrap b = new Bootstrap();
	
	public NettyClient_SHINHAN(String host, int port) throws InterruptedException {
		this.host = host;
		this.port = port;
		init();
	}

	//isActive,isOpen
	public void init() throws InterruptedException {
		logger.debug("Initializing client and connecting to server..");
		
		b.group(workerGroup).channel(NioSocketChannel.class)
		 		.option(ChannelOption.SO_KEEPALIVE, true)
				.handler(new ChannelInitializer<SocketChannel>() {
					@Override
					protected void initChannel(SocketChannel channel) throws Exception {
						channel.pipeline().addLast("framerDecoder", new FixedLengthFrameDecoder(1004));
						channel.pipeline().addLast("decode", new ByteArrayDecoder());
						channel.pipeline().addLast("encoder", new ByteArrayEncoder());
						channel.pipeline().addLast("handler", new ShinhanChannelInboundHandler());
					}
					
					@Override
					public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
							throws Exception {
						cause.printStackTrace();
					}
				});

		try {
			if (chan == null) {
				chan = acquireChan();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class ShinhanChannelInboundHandler extends SimpleChannelInboundHandler<Object> {
		public void setResponseFuture(String id, ResponseFuture future) {
			ShinhanConsts.responseMap.put(id, future);
		}

		@Override
		protected void channelRead0(ChannelHandlerContext ctx, Object msg)
				throws Exception {
			String id = getId((byte[])msg);
			ResponseFuture responseFuture = ShinhanConsts.responseMap.get(id);
			responseFuture.set(msg);
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
			cause.printStackTrace();
			close();
		}
	}
	
	private String getId(byte[] msg) {
		String id = "";
		
		try {
			id = MessageUtil.cutStrByByteIndexLength(msg, 15, 21).trim();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return id;
	}
	
	public void close() {
		synchronized(ShinhanConsts.queue) {
			try{
				if(chan != null) {
					chan.close();
					chan = null;
				}
			} catch (Exception e){
			} finally{
				ShinhanConsts.queue.remove(1);
			}
		}
	}
	
	// wait for, or create a channel
	private Channel acquireChan() throws Exception {
		
		int cnt = 3;
	    
        do {
    		InetSocketAddress addr1 = new InetSocketAddress(host, port);
    		//InetSocketAddress addr2 = new InetSocketAddress("127.0.0.1", 52100);
        	ChannelFuture cf = b.connect(addr1);
            boolean connect = cf.await(2000);
            if (!connect) {
                throw new Exception("Unable to try connection to Server in " + 2000 + "ms");
            }

            Channel newChan =  cf.channel();
        	cnt --;
        	if(cnt <= 0)
        		throw new Exception("Unable to get connection in 3 try");
        	
            // OK, there's an available channel, make sure it is usable.
            Channel toUse = newChan;//ShinhanConsts.queue.get(1);
            if (toUse.isActive() && toUse.isWritable()) {
                // great, good to go, use it.
                return toUse;
            } else {
            	
            }

        } while (true);
        
	}

	public ResponseFuture send(final byte[] msg) throws Exception {
		final ResponseFuture responseFuture = new ResponseFuture();

	    try {
	        ChannelFuture future = chan.writeAndFlush(msg);
	        future.channel().pipeline().get(ShinhanChannelInboundHandler.class).setResponseFuture(getId(msg), responseFuture);
	        
	    } catch (Exception e) {
	        close();
	        throw e;
	    } finally {
	    	//returnChan()
	    }

	    return responseFuture;
	}
}