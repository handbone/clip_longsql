package com.coopnc.cardpoint.netty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.cardpoint.consts.Consts.BcConsts;
import com.coopnc.cardpoint.netty.component.ResponseFuture;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.util.concurrent.GenericFutureListener;

public class NettyClient_BC {
	private static Logger logger = LoggerFactory.getLogger(NettyClient_BC.class);
	
	private ChannelFuture channelFuture0 = null;
	private static final EventLoopGroup workerGroup0 = new NioEventLoopGroup(BcConsts.NEETY_WORKER_NUM);
	
	private ChannelFuture channelFuture1 = null;
	private static final EventLoopGroup workerGroup1 = new NioEventLoopGroup(BcConsts.NEETY_WORKER_NUM);
	
	/**
	 * 비씨카드는 전문 유형에 따라 전문 필드가 완전히 다름
	 */
	private boolean isGet = true;
	
	public NettyClient_BC(String host, int port, int frameLength, boolean isGet) throws InterruptedException{
		this.isGet = isGet;
		init(host, port, frameLength);
	}

	private ResponseFuture sendGet(final byte[] msg) throws Exception {
		final ResponseFuture responseFuture = new ResponseFuture();

		channelFuture0.addListener(new GenericFutureListener<ChannelFuture>() {
			@Override 
			public void operationComplete(final ChannelFuture future) throws Exception {
				future.channel().pipeline().get(BccardChannelInboundHandler.class).setResponseFuture(responseFuture);
				future.channel().writeAndFlush(msg);
				logger.debug(" writeAndFlush done !!!!! "+msg.length);
			}
		});

		return responseFuture;
	}
	
	private ResponseFuture sendUse(final byte[] msg) throws Exception {
		final ResponseFuture responseFuture = new ResponseFuture();

		channelFuture1.addListener(new GenericFutureListener<ChannelFuture>() {
			@Override 
			public void operationComplete(final ChannelFuture future) throws Exception {
				future.channel().pipeline().get(BccardChannelInboundHandler.class).setResponseFuture(responseFuture);
				future.channel().writeAndFlush(msg);
				logger.debug(" writeAndFlush done !!!!! "+msg.length);
			}
		});

		return responseFuture;
	}
	
	public ResponseFuture send(final byte[] msg) throws Exception {
		if(this.isGet) {
			return sendGet(msg);
		} else {
			return sendUse(msg);
		}
	}

	public void close() {
		try {
			if(channelFuture0.channel().isActive())
				channelFuture0.channel().close();
		} catch (Exception e) {}
		
		try {
			if(channelFuture1.channel().isActive())
				channelFuture1.channel().close();
		} catch (Exception e) {}
	}
	 
	//isActive,isOpen
	public void init(String host, int port, final int frameLength) throws InterruptedException {
		logger.debug("Initializing client and connecting to server.. host : " + host + ", port : " + port);

		if(this.isGet) {
			Bootstrap b = new Bootstrap();
			b.group(workerGroup0).channel(NioSocketChannel.class)
			 		.option(ChannelOption.SO_KEEPALIVE, false)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel channel) throws Exception {
							channel.pipeline().addLast("framerDecoder", new FixedLengthFrameDecoder(309));
							channel.pipeline().addLast("decode", new ByteArrayDecoder());
							channel.pipeline().addLast("encoder", new ByteArrayEncoder());
							channel.pipeline().addLast("handler", new BccardChannelInboundHandler());
						}

						@Override
						public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
								throws Exception {
							cause.printStackTrace();
							close();
						}
					});

			channelFuture0 = b.connect(host, port);
		} else {
			Bootstrap b = new Bootstrap();
			b.group(workerGroup1).channel(NioSocketChannel.class)
			 		.option(ChannelOption.SO_KEEPALIVE, true)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel channel) throws Exception {
							channel.pipeline().addLast("framerDecoder", new FixedLengthFrameDecoder(426));
							channel.pipeline().addLast("decode", new ByteArrayDecoder());
							channel.pipeline().addLast("encoder", new ByteArrayEncoder());
							channel.pipeline().addLast("handler", new BccardChannelInboundHandler());
						}

						@Override
						public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
								throws Exception {
							cause.printStackTrace();
							close();
						}
					});

			channelFuture1 = b.connect(host, port);			
		}
		
	}
	
	/**
	 * BC카드와의 통신은 매번 새로 접속하고 통신이 끝나면 바로 닫는다.
	 */
	class BccardChannelInboundHandler extends SimpleChannelInboundHandler<Object> {
		private ResponseFuture responseFuture;

		public void setResponseFuture(ResponseFuture future) {
			this.responseFuture = future;		
		}

		@Override
		protected void channelRead0(ChannelHandlerContext ctx, Object msg)
				throws Exception {
			byte[] bmsg = (byte[])msg;
			logger.debug("[client][in] [" + new String(bmsg) + "]");
			responseFuture.set(msg);
			ctx.close();
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
			cause.printStackTrace();
			ctx.close();
		}
	}
}