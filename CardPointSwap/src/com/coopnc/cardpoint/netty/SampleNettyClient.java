package com.coopnc.cardpoint.netty;

import com.coopnc.cardpoint.netty.component.LengthFieldBaseFrameDecoder;
import com.coopnc.cardpoint.netty.component.MyChannelInboundHandler;
import com.coopnc.cardpoint.netty.component.ResponseFuture;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.util.concurrent.GenericFutureListener;

public class SampleNettyClient {

	private ChannelFuture channelFuture = null;

	public SampleNettyClient(String host, int port) {
		reconnectIfNot(host, port);
	}

	public ResponseFuture send(final String msg) {
		final ResponseFuture responseFuture = new ResponseFuture();

		channelFuture.addListener(new GenericFutureListener<ChannelFuture>() {
			@Override 
			public void operationComplete(ChannelFuture future) throws Exception {
				channelFuture.channel().pipeline().get(MyChannelInboundHandler.class).setResponseFuture(responseFuture);
				channelFuture.channel().writeAndFlush(msg);
			}
		});

		return responseFuture;
	}

	public void close() {
		channelFuture.channel().close();
	}
	
	//isActive,isOpen
	public void reconnectIfNot(String host, int port) {
		if(channelFuture == null || !channelFuture.channel().isActive()) {
			//System.out.println("Initializing client and connecting to server..");
			EventLoopGroup workerGroup = new NioEventLoopGroup();

			Bootstrap b = new Bootstrap();
			b.group(workerGroup).channel(NioSocketChannel.class).option(ChannelOption.SO_KEEPALIVE, true)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel channel) throws Exception {
							channel.pipeline().addLast("framerDecoder", new LengthFieldBaseFrameDecoder(4096, 0, 4));
							channel.pipeline().addLast("decode", new ByteArrayDecoder());
							channel.pipeline().addLast("encoder", new ByteArrayEncoder());
							channel.pipeline().addLast("handler", new MyChannelInboundHandler());
						}
					});

			channelFuture = b.connect(host, port);
		}
	}
}