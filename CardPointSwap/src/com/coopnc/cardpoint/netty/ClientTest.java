package com.coopnc.cardpoint.netty;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.coopnc.cardpoint.netty.component.ResponseFuture;

public class ClientTest {

	// 호스트를 정의합니다. 로컬 루프백 주소를 지정합니다.
	private static final String HOST = "127.0.0.1";
	// 접속할 포트를 정의합니다.
	private static final int PORT = 9898;

	public static void main(String[] args) throws Exception {
		System.out.println("Netty promises!");

		SampleNettyClient client = new SampleNettyClient(HOST, PORT);

		ResponseFuture future = client.send("Hello server!");

		System.out.println("Doing some stuff in the meantime...1 ");

		try {
			String replyMsg = (String)future.get(10, TimeUnit.SECONDS);

			System.out.println("replyMsg_1 :  "+replyMsg);

		} catch (TimeoutException e) {
			System.out.println("Timed out..");
		}
		
		
		ResponseFuture future2 = client.send("My Name IS!");

		System.out.println("Doing some stuff in the meantime...2");

		try {
			String replyMsg = (String)future2.get(10, TimeUnit.SECONDS);

			System.out.println("replyMsg_2 :  "+replyMsg);

		} catch (TimeoutException e) {
			System.out.println("Timed out..");
		}
	}

}