package com.coopnc.cardpoint.netty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.cardpoint.consts.Consts.HanaConsts;
import com.coopnc.cardpoint.netty.component.LengthFieldBaseFrameDecoder;
import com.coopnc.cardpoint.netty.component.MyChannelInboundHandler;
import com.coopnc.cardpoint.netty.component.ResponseFuture;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.util.concurrent.GenericFutureListener;

public class NettyClient_HANA {
	private static Logger logger = LoggerFactory.getLogger(NettyClient_HANA.class);
	
	private ChannelFuture channelFuture = null;
	private static final EventLoopGroup workerGroup = new NioEventLoopGroup(HanaConsts.NEETY_WORKER_NUM);
	//private static NettyClient_HANA instance = null;
	
	public NettyClient_HANA(String host, int port) throws InterruptedException{
		
		init(host, port);
	}

	public ResponseFuture send(final byte[] msg) throws Exception {
		final ResponseFuture responseFuture = new ResponseFuture();

		channelFuture.addListener(new GenericFutureListener<ChannelFuture>() {
			@Override 
			public void operationComplete(final ChannelFuture future) throws Exception {
				future.channel().pipeline().get(HanaChannelInboundHandler.class).setResponseFuture(responseFuture);
				future.channel().writeAndFlush(msg);
				logger.debug(" writeAndFlush done !!!!! "+msg.length);
			}
		});

		return responseFuture;
	}

	public void close() {
		channelFuture.channel().close();
	}
	 
	//isActive,isOpen
	public void init(String host, int port) throws InterruptedException {
		logger.debug("Initializing client and connecting to server..");

		Bootstrap b = new Bootstrap();
		b.group(workerGroup).channel(NioSocketChannel.class)
		 		.option(ChannelOption.SO_KEEPALIVE, true)
				.handler(new ChannelInitializer<SocketChannel>() {
					@Override
					protected void initChannel(SocketChannel channel) throws Exception {
						channel.pipeline().addLast("framerDecoder", new LengthFieldBaseFrameDecoder(4096, 0, 4));
						channel.pipeline().addLast("decode", new ByteArrayDecoder());
						channel.pipeline().addLast("encoder", new ByteArrayEncoder());
						channel.pipeline().addLast("handler", new HanaChannelInboundHandler());
					}

					@Override
					public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
							throws Exception {
						cause.printStackTrace();
						close();
					}
				});

		channelFuture = b.connect(host, port);
	}
	
	class HanaChannelInboundHandler extends SimpleChannelInboundHandler<Object> {
		private ResponseFuture responseFuture;

		public void setResponseFuture(ResponseFuture future) {
			this.responseFuture = future;
		}

		@Override
		protected void channelRead0(ChannelHandlerContext ctx, Object msg)
				throws Exception {
			//byte[] bmsg = (byte[])msg;
			responseFuture.set(msg);
			ctx.close();
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
			cause.printStackTrace();
			ctx.close();
		}
	}
}