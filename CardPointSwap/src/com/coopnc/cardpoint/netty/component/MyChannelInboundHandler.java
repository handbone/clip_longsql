package com.coopnc.cardpoint.netty.component;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class MyChannelInboundHandler extends SimpleChannelInboundHandler<Object> {

	private ResponseFuture responseFuture;

	public void setResponseFuture(ResponseFuture future) {
		this.responseFuture = future;		
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		
		byte[] bmsg = (byte[])msg;
		
		System.out.println("[client][in] " + new String(bmsg));
		responseFuture.set(msg);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
}
