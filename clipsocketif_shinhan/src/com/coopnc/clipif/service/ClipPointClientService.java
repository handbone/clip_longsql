package com.coopnc.clipif.service;

import java.util.List;

import com.coopnc.clipif.bean.ClipPointMessage;
import com.coopnc.clipif.bean.PointSwapInfo;
import com.coopnc.clipif.bean.SocketRequestVo;

public interface ClipPointClientService {

	//포인트 조회
	public ClipPointMessage getPoint(SocketRequestVo param);
	
	//포인트 적립
	public ClipPointMessage plusPoint(SocketRequestVo param) throws Exception;
	
	//포인트 적립 취소
	public ClipPointMessage cancelPlusPoint(SocketRequestVo param);
	
	//포인트 차감
	public ClipPointMessage minusPoint(SocketRequestVo param);
	
	//포인트 차감 취소
	public ClipPointMessage cancelMinusPoint(SocketRequestVo param);	
	
	//포인트 내역
	public List<ClipPointMessage> getPointHistoryList(SocketRequestVo param);
	
	//포인트 전환 정보 등록
	public int insertPointSwapHist(PointSwapInfo pointSwapInfo);
	
}