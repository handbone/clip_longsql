package com.coopnc.clipif.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.coopnc.clipif.bean.PointSwapInfo;
import com.coopnc.clipif.bean.ShinhanCmsRequest;
import com.coopnc.clipif.bean.ShinhanCmsRequest.T0300;
import com.coopnc.clipif.bean.ShinhanCmsRequest.T0310;
import com.coopnc.clipif.bean.ShinhanCmsRequest.T0600;
import com.coopnc.clipif.bean.ShinhanCmsRequest.T0620;
import com.coopnc.clipif.bean.ShinhanCmsRequest.T0630;
import com.coopnc.clipif.bean.ShinhanReconcileVo;
import com.coopnc.clipif.config.BaseConstants.ShinhanCmsConsts;
import com.coopnc.clipif.mappers.PointSwapMapper;
import com.coopnc.clipif.server.ResponseFuture;
import com.coopnc.clipif.server.ShinhanCmsClient;
import com.coopnc.clipif.util.DateUtil;
import com.coopnc.clipif.util.FileUtil;
import com.coopnc.clipif.util.IntHolder;
import com.coopnc.clipif.util.MessageUtil;

@Service
public class ShinhanCmsClientServiceImpl implements ShinhanCmsClientService {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	private String SHINAHAN_CMS_SERVER_IP;
	private int SHINAHAN_CMS_SERVER_PORT;
	
	private Socket socket = null;
	private BufferedOutputStream sender = null;
	private BufferedInputStream receiver = null;

	private static final int MAX_UNIT_SIZE = 4096;	//4096 보다 큰 전문은 없다.
	private static final int UNIT_SIZE = 3040;		//단위당 3040
	private static final int MAX_UNIT_NUM = 100;	//블럭당 100 단위
	
	private String FILE_NAME = "SGR00140";	//"TKTC0001";
	private String FILE_FULL_PATH = "";
	private String SEND_FILE_PATH = "";

	private String SENDER = "";
	private String SENDER_PASS = "";
	
	@Autowired
	PointSwapMapper pointSwapMapper;
	
	@Autowired
	Environment env;

	@PostConstruct
	void init(){
		SHINAHAN_CMS_SERVER_IP = env.getProperty("shinhancms.server.ip");
		SHINAHAN_CMS_SERVER_PORT = Integer.parseInt(env.getProperty("shinhancms.server.port"));
		SEND_FILE_PATH = env.getProperty("shinhancms.file.path.send");
		
		File file = new File(SEND_FILE_PATH);
		if(!file.exists())
			file.mkdirs();
		
	}
	
	/*
	업무개시요구 (0600)	<-
	업무개시통보 (0610)	->
	
	파일정보지시(0630)	<-
	파일정보보고(0640)	->
	DATA 송신 (0320)	<-
	
	결번확인지시 (0620)	<-
	결번확인보고 (0300)	->
	결번 통보 (0310)	<-
	
	송신완료지시 (0600)	<-
	송신완료보고 (0610)	->
	
	업무종료지시 (0600)	<-
	업무종료보고(0610)	->
	*/

	@Override
	public String excuteShinhanCMSClient() {
		try {
			//시분초까지 파일명 생성
			FILE_FULL_PATH = SEND_FILE_PATH+ShinhanCmsConsts.FILE_PREFIX+"_"+MessageUtil.getDateStr("yyyyMMddHHmmss")+".dat";
			File file = new File(FILE_FULL_PATH);
			if(file.exists())
				file.delete();
			
			//clip 측 파일 작성
			String standDate = MessageUtil.getDateStr(DateUtil.addDate(-1), "yyyyMMdd"); //기준일자 : 전날
			long fileSize = makeShinhanCmsFile(standDate, FILE_FULL_PATH);
			
			logger.debug(FILE_FULL_PATH);
			
			//패킷 단위 미리 계산
			int packetCnt = ((int)(fileSize / UNIT_SIZE)) + 1;	//320 전문 packetCnt 만큼 전송
			int blockCnt = packetCnt/MAX_UNIT_NUM + 1;		//blockCnt 만큼 파일 전송 루프 (한 블럭당 100 단위만 보냄)
			
			logger.debug("fileSize : blockCnt : packetCnt = "+fileSize+" : "+blockCnt+" : "+packetCnt);
			
			//업무개시요구 (0600)	<-
			//업무개시통보 (0610)	->
			ShinhanCmsRequest res = proc0600(ShinhanCmsConsts.COMMAND_START);
			
			//파일정보지시(0630)	<-
			//파일정보보고(0640)	->
			if(!"000".equals(res.getResult_code())){
				logger.debug("ShinhanCMSClient res : "+ res.getResult_code());
				logger.debug("fail message : " + new String(res.getMessage()));
				return "F";
			}
			res = proc0630(fileSize);
			
			//---------------------------- 파일 전송 루프 s
			int totSendCnt = 0;
			for (int i = 0; i< blockCnt; i ++){
				
				int blockSendCnt = 0;
				//DATA 송신 (0320)	<-
				for (int j = 0; j< MAX_UNIT_NUM; j++){	
					if(totSendCnt == packetCnt)
						break;
					
					proc0320(i+1, j+1);
					totSendCnt ++;
					blockSendCnt ++;
				}
				
				//결번확인지시 (0620)	<-
				//결번확인보고 (0300)	->
				res = proc0620(i+1, blockSendCnt);
				T0300 res300Body = (T0300)res.getBodyObject();
				if(!res300Body.getBlock_no().equals(""+(i+1)))
					throw new Exception("Invalid Res Message Block!!");
				
				int missCnt = Integer.parseInt(res300Body.getMiss_cnt());
				String serialChk = res300Body.getMiss_confirm();
				
				int reSendCnt = 0;
				for(int j= 0; j<serialChk.length(); j++){
					if(reSendCnt == missCnt)
						break;
					
					if(serialChk.charAt(j) == '0') {
						
						//결번 통보 (0310)	<-
						res = proc0310(i+1, j+1);
						reSendCnt ++;
					}
				}
			}

			//----------------------------- 파일 전송 루프 e
			
			//송신완료지시 (0600)	<-
			//송신완료보고 (0610)	->
			res = proc0600(ShinhanCmsConsts.COMMAND_PART_CLOSE);
			
			//업무종료지시 (0600)	<-
			//업무종료보고(0610)	->
			res = proc0600(ShinhanCmsConsts.COMMAND_CLOSE);
			logger.debug("ShinhanCMSClient res : "+ res.getResult_code());
			logger.debug("final message : " + new String(res.getMessage()));
			return "F";
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return "F";
		}
	}
	
	//block 번호와 seq값으로 실제 파일 데이터를 읽어옴
	private byte[] getFileBinary(String fileFullPath, int block, int seq){
		int blockIdx = block - 1;
		int seqIdx = seq - 1;
		long pointer = UNIT_SIZE * (blockIdx*100 + seqIdx);

		return FileUtil.fileSearch(fileFullPath, pointer, UNIT_SIZE);
	}
	
	private ShinhanCmsRequest sendAndReceiveMsg(ShinhanCmsRequest request) throws Exception {
		
		ShinhanCmsClient client = new ShinhanCmsClient(SHINAHAN_CMS_SERVER_IP, SHINAHAN_CMS_SERVER_PORT);
		logger.debug("Send to Shinhan commType["+request.getComm_type()+"],size["+request.getMessage().length+"],msg[" + new String(request.getMessage()) + "]");
		
		try {
			Thread.sleep(500);
		} catch (Exception e) {}
		
		//파일 전송시는 응답은 받지 않는다.
		if(ShinhanCmsConsts.COMM_DATA_SEND.equals(request.getComm_type())
				|| ShinhanCmsConsts.COMM_MISS_NO_SEND.equals(request.getComm_type())) {

			client.send(request.getMessage(), false);
			return null;
		} else {
			ResponseFuture result = client.send(request.getMessage(), true);
			byte[] msg = (byte[])result.get(5000, TimeUnit.MILLISECONDS);
			ShinhanCmsRequest response = new ShinhanCmsRequest(msg);
			
			return response;
		}
	}
	
	/**EDI04550
	 * 전송 모듈을 netty로 변경했으므로 일단은 체크용으로 남겨둠
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	private ShinhanCmsRequest sendAndReceiveMsg1(ShinhanCmsRequest request) throws Exception {
		try {
			boolean isConnected = false;
			if(socket == null || socket.isBound() == false) {
				int retry = 3;
				for (int i = 0; i < retry; i++) {
					isConnected = reconnect(SHINAHAN_CMS_SERVER_IP, SHINAHAN_CMS_SERVER_PORT, 9000, 9000);
					
					if(isConnected)
						break;
				}
			} else {
				isConnected  = true;
			}
			
			Thread.sleep(1000);

			if(isConnected) {
				logger.debug("Send to Shinhan commType["+request.getComm_type()+"],size["+(new String(request.getMessage())).length()+"],msg[" + new String(request.getMessage()) + "]");
				
				sender.write(request.getMessage());
				sender.flush();

				//파일 전송시는 응답은 받지 않는다.
				if(ShinhanCmsConsts.COMM_DATA_SEND.equals(request.getComm_type())
						|| ShinhanCmsConsts.COMM_MISS_NO_SEND.equals(request.getComm_type())) {
					byte[] lengthF = new byte[4];
		            //receiver.read(lengthF);
		            //logger.debug(""+new String(lengthF));
		
					return null;
				}
				
				// 서버로부터 데이터 받기
				// 바이트 데이터를 전송받으면서 기록

	            int len;
	            int totlen = 0;
	            
	            byte[] lengthF = new byte[4];
	            len = receiver.read(lengthF);
	            logger.debug("read length field ... "+len);
	            
	            int realLen = Integer.parseInt(MessageUtil.removeZero(new String(lengthF)));
	            totlen = realLen + 4;
	            
	            byte[] buff = new byte[realLen]; // 4096 보다 큰 전문은 오지 않음
	            len = receiver.read(buff);
	            logger.debug("read message field ... "+len);
	            
	            byte[] message = new byte[totlen];
	            
	            System.arraycopy(lengthF, 0, message, 0, 4);
				System.arraycopy(buff, 0, message, 4, realLen);
				
				// 수신메시지 출력
				String msg = new String(message);
				String out = String.format("Response from Shinhan [%s]", msg);
				logger.debug(""+out);
				
				ShinhanCmsRequest response = new ShinhanCmsRequest(message);
				
				return response;

			} else {
				throw new Exception("Receive Invalid telegram and Close ...");
			}
		} catch (Throwable e) {
			closeQuietly();
			
			throw e;
		} finally {
			
		}

	}

	/*this.msg_len = getN(0, 4);			// 전문 길이	NUMBER	0	4
	this.work_div = getAN(4, 3); 		// A 4 3 업무구분 코드
	this.company_code = getAN(7, 2); 	// AN 7 2 회원사코드
	this.comm_type = getAN(9, 4); 		// AN 9 4 전문종별 코드
	this.trade_div = getAN(13, 1); 		// AN 13 1 거래구분 코드
	this.send_flag = getAN(14, 1); 		// A 14 1 송수신 Flag
	this.file_name = getAN(15, 8); 		// AN 15 8 파일명
	this.result_code = getAN(23, 3); 	// N 23 3 응답코드
	 */	
	private ShinhanCmsRequest proc0600(String work_mgr_info) throws Exception {

		ShinhanCmsRequest request = new ShinhanCmsRequest();
		T0600 reqBody = (T0600)request.getBodyObject(ShinhanCmsConsts.COMM_PROGRESS_REQ);
		reqBody.setComm_date(MessageUtil.getDateStr("MMddHHmmss"));
		reqBody.setWork_mgr_info(work_mgr_info);
		reqBody.setSender(SENDER);
		reqBody.setSender_pass(SENDER_PASS);
		
		request.make(ShinhanCmsConsts.TRADE_DIV_RECV, ShinhanCmsConsts.SEND_FLAG_KT, ShinhanCmsConsts.COMM_PROGRESS_REQ, FILE_NAME, "000", reqBody);

		ShinhanCmsRequest response = sendAndReceiveMsg(request);
		return response;
	}

	private ShinhanCmsRequest proc0630(long fileSize) throws Exception {
		ShinhanCmsRequest request = new ShinhanCmsRequest();
		T0630 reqBody = (T0630)request.getBodyObject(ShinhanCmsConsts.COMM_FILE_INFO_REQ);
		reqBody.setFile_name(FILE_NAME);
		reqBody.setFile_size(""+fileSize);		//총 파일 사이즈
		int comm320Size = 4+22+11+UNIT_SIZE;	//0320전문 = 길이(4)+헤더(22)+320body(11)+파일별 사이즈(3040)
		reqBody.setComm_data_size(""+comm320Size);
		
		logger.debug(reqBody.getFile_name()+"//"+reqBody.getFile_size());
		
		request.make(ShinhanCmsConsts.TRADE_DIV_RECV, ShinhanCmsConsts.SEND_FLAG_KT, ShinhanCmsConsts.COMM_FILE_INFO_REQ, FILE_NAME, "000", reqBody);

		ShinhanCmsRequest response = sendAndReceiveMsg(request);
		return response;
	}

	private ShinhanCmsRequest proc0320(int block, int unitCnt) throws Exception {
		ShinhanCmsRequest request = new ShinhanCmsRequest();
		T0310 reqBody = (T0310)request.getBodyObject(ShinhanCmsConsts.COMM_DATA_SEND);
		
		reqBody.setBlock_no(""+block);
		reqBody.setFinal_seq_no(""+unitCnt);
		
		byte[] data = getFileBinary(FILE_FULL_PATH, block, unitCnt);
		reqBody.setReal_data_size(""+data.length);
		reqBody.setReal_data(data);
		
		logger.debug("realdata ["+reqBody.getReal_data_size()+"] ["+new String(reqBody.getReal_data()+"]"));
		
		request.make(ShinhanCmsConsts.TRADE_DIV_RECV, ShinhanCmsConsts.SEND_FLAG_KT, ShinhanCmsConsts.COMM_DATA_SEND, FILE_NAME, "000", reqBody);

		return sendAndReceiveMsg(request);
	}

	private ShinhanCmsRequest proc0620(int block, int unitCnt) throws Exception {
		ShinhanCmsRequest request = new ShinhanCmsRequest();
		T0620 reqBody = (T0620)request.getBodyObject(ShinhanCmsConsts.COMM_MISS_NO_REQ);
		reqBody.setBlock_no(""+block);
		reqBody.setSeq_no(""+unitCnt);
		
		request.make(ShinhanCmsConsts.TRADE_DIV_RECV, ShinhanCmsConsts.SEND_FLAG_KT, ShinhanCmsConsts.COMM_MISS_NO_REQ, FILE_NAME, "000", reqBody);

		return sendAndReceiveMsg(request);
	}

	private ShinhanCmsRequest proc0310(int block, int unitCnt) throws Exception {
		ShinhanCmsRequest request = new ShinhanCmsRequest();
		T0310 reqBody = (T0310)request.getBodyObject(ShinhanCmsConsts.COMM_MISS_NO_SEND);
		
		reqBody.setBlock_no(""+block);
		reqBody.setFinal_seq_no(""+unitCnt);
		
		byte[] data = getFileBinary(FILE_FULL_PATH, block, unitCnt);
		reqBody.setReal_data_size(""+data.length);
		reqBody.setReal_data(data);
		
		request.make(ShinhanCmsConsts.TRADE_DIV_RECV, ShinhanCmsConsts.SEND_FLAG_KT, ShinhanCmsConsts.COMM_MISS_NO_SEND, FILE_NAME, "000", reqBody);
		
		return sendAndReceiveMsg(request);
	}
	
	private boolean reconnect(String ip, int port, int timeout, int soTimeout){
		try {
			if(socket == null || socket.isBound() == false) {
				
				if(socket != null && socket.isBound() == false)
					closeQuietly();
				
				socket = new Socket();
				
				logger.debug("Try to connect to CMS server ip["+ip+"] port["+port+"]");
				
				socket.connect(new InetSocketAddress(ip, port), timeout);
				socket.setSendBufferSize(4096);
				socket.setSoTimeout(soTimeout);
				
				sender = new BufferedOutputStream(socket.getOutputStream());
				receiver = new BufferedInputStream(socket.getInputStream());

			}

			return true;
			
		} catch (Exception e) {
			closeQuietly();
			e.printStackTrace();
			return false;
		}
	}
	
	private void closeQuietly(){
		if(receiver != null) { 
			try {receiver.close();} catch (Exception e){}
		}
		if(sender != null) {
			try {sender.close();} catch (Exception e){}
		}
		if(socket != null) {
			try {socket.close();} catch (Exception e){}
		}	
	}
	
	private long makeShinhanCmsFile(String standDate, String fileFullPath) throws Exception {
		
		//Header : s
		byte[] header = new byte[500];
		//메세지 초기화 - 모두 스페이스로 채움
		for (int i = 0 ; i < header.length ; i++){
			header[i] = ' ';
		}
		
		IntHolder idx = new IntHolder();
		addAN(header, "HD", idx, 2);	//CHAR	0	2	SRA_C_VL	신한그룹통합리워드앱구분값
		addAN(header, "0000000000", idx, 10);	//NUMBER	2	10	SRA_TS_SEQNUM	신한그룹통합리워드앱거래일련번호
		addAN(header, "21810", idx, 5);	//CHAR	12	5	SRA_TG_N	신한그룹통합리워드앱전문번호	21810(포인트거래검증요청) , 21820(포인트거래검증응답)
		addAN(header, "S000", idx, 4);	//CHAR	17	4	SRA_SHP_CO_CD	신한그룹통합리워드앱신한그룹사코드
		addAN(header, "V001", idx, 4);	//CHAR	21	4	SRA_TG_VER	신한그룹통합리워드앱전문버전
		addAN(header, standDate, idx, 8);	//CHAR	25	8	SRA_TS_CED	신한그룹통합리워드앱거래기준일자
		addAN(header, MessageUtil.getDateStr("yyyyMMddHHmmss"), idx, 14);	//CHAR	33	14	SRA_CRT_DT	신한그룹통합리워드앱생성일시
		//addAN(header, "", idx, 2);	//CHAR	47	453	SRA_FER		신한그룹통합리워드앱필러
		FileUtil.appendNoNewLine(fileFullPath, new String(header));
		//Header : e
		
		//Body : s
		PointSwapInfo param = new PointSwapInfo();
		param.setTrans_req_date(standDate);
		param.setReg_source(ShinhanCmsConsts.REG_SOURCE);
		List<PointSwapInfo> swapHistList = pointSwapMapper.selectSwapList(param);
		int listCount = swapHistList.size();

		for (int i = 0; i<listCount; i++){
			ShinhanReconcileVo data = new ShinhanReconcileVo(swapHistList.get(i), i+1);
			FileUtil.appendNoNewLine(fileFullPath, data.toString());
		}
		//Body : e
		
		
		//Tail : s
		byte[] tail = new byte[500];
		//메세지 초기화 - 모두 스페이스로 채움
		for (int i = 0 ; i < tail.length ; i++){
			tail[i] = ' ';
		}
		
		idx = new IntHolder();
		addAN(tail, "TR", idx, 2);	//CHAR	0	2	SRA_C_VL	신한그룹통합리워드앱구분값
		addAN(tail, "9999999999", idx, 10);	//NUMBER	2	10	SRA_TS_SEQNUM	신한그룹통합리워드앱거래일련번호
		addN(tail, ""+listCount, idx, 10);	//NUMBER	12	10	SRA_CT		신한그룹통합리워드앱건수
		//addAN(tail, "", idx, 478);	//CHAR	22	478	SRA_FER		신한그룹통합리워드앱필러
		FileUtil.appendNoNewLine(fileFullPath, new String(tail));
		//Tail : e
		
		return new File(fileFullPath).length();
		
	}
	
	private void addAN(byte[] msg, String str, IntHolder _pos, int len){	
		if(str == null)
			str = "";
		
		if("".equals(str)) {
			_pos.add(len);
			return;
		}
		
		str = MessageUtil.rpad(str, len, " ");	
		System.arraycopy(str.getBytes(), 0, msg, _pos.value , len);
		_pos.add(len);	
	}
	
	private void addN(byte[] msg, String number, IntHolder _pos, int len){
		if(number == null)
			number = "";
		
		if(number.startsWith("-")) {
			number = "-" + MessageUtil.lpad(number.substring(1), len-1, "0");
		} else {
			number = MessageUtil.lpad(number, len, "0");
		}
		
		System.arraycopy(number.getBytes(), 0, msg, _pos.value, len);
		
		_pos.add(len);
	}
	
}
