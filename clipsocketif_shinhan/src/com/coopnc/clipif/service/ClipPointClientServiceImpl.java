//package com.coopnc.clipif.service;
//
//import java.net.URLEncoder;
//import java.util.List;
//
//import javax.annotation.PostConstruct;
//
//import org.apache.commons.lang3.StringUtils;
//import org.apache.http.HttpStatus;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
//import org.springframework.stereotype.Component;
//
//import com.coopnc.clipif.bean.ClipPointMessage;
//import com.coopnc.clipif.bean.PointSwapInfo;
//import com.coopnc.clipif.bean.SocketRequestVo;
//import com.coopnc.clipif.config.BaseConstants;
//import com.coopnc.clipif.config.BaseMessages;
//import com.coopnc.clipif.mappers.PointSwapMapper;
//import com.coopnc.clipif.util.HttpClientUtil;
//
//@Component
//public class ClipPointClientServiceImpl implements ClipPointClientService {
//	Logger logger = Logger.getLogger(this.getClass());
//
//	private String API_DOMAIN;
//	
//	@Autowired
//	Environment env;
//
//	@PostConstruct
//	void init(){
//		API_DOMAIN = env.getProperty("API_DOMAIN");
//	}
//	
//	@Autowired
//	PointSwapMapper pointSwapMapper;
//
//	@Override
//	public ClipPointMessage getPoint(SocketRequestVo param) {
//		ClipPointMessage result = new ClipPointMessage();
//		
//		HttpClientUtil conn = null;
//		try {
//			conn = new HttpClientUtil("POST", API_DOMAIN + BaseConstants.ADV_GET_POINT, 1000);
//			conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
//			
//			if(BaseConstants.USER_KEY_USER_CI.equals(param.getUser_key_type())){
//				conn.addParameter("user_ci", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
//			} else {
//				conn.addParameter("user_token", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
//			}
//	
//			int ret = conn.sendRequest();
//			logger.debug("RES_HTTP_STATUS : " + ret);
//
//			if(HttpStatus.SC_OK == ret) {
//				result = conn.getResponseJson(ClipPointMessage.class);
//				
//				result.setResult("S");
//				result.setResultMsg(BaseMessages.getVal(""+ret));
//				
//			} else {
//				result.setResult("F");
//				result.setResultMsg(BaseMessages.getVal(""+ret));
//			}
//			
//		} catch (Exception e) {
//			conn.closeConnection();
//			logger.error(e.getMessage());
//			result.setResult("F");
//			result.setResultMsg(BaseMessages.getVal(BaseConstants.ERR_SYSTEM_ERR));
//		}
//		
//		return result;
//	}
//
//	@Override
//	public ClipPointMessage plusPoint(SocketRequestVo param) throws Exception {
//
//		ClipPointMessage result = new ClipPointMessage();
//		
//		HttpClientUtil conn = null;
//		try {
//			conn = new HttpClientUtil("POST", API_DOMAIN + BaseConstants.ADV_PLUS_POINT, 1000);
//			conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
//			
//			if(BaseConstants.USER_KEY_USER_CI.equals(param.getUser_key_type())){
//				conn.addParameter("user_ci", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
//			} else {
//				conn.addParameter("user_token", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
//			}
//
//			conn.addParameter("transaction_id", param.getTr_id());
//			conn.addParameter("requester_code", param.getRequester_code());
//			conn.addParameter("point_value", ""+param.getPoint_value());
//			conn.addParameter("description", URLEncoder.encode(param.getDescription(),BaseConstants.DEFAULT_ENCODING));
//			
//			int ret = conn.sendRequest();
//			logger.debug("RES_HTTP_STATUS : " + ret);
//
//			if(HttpStatus.SC_OK == ret) {
//				result = conn.getResponseJson(ClipPointMessage.class);
//				result.setResult("S");
//				result.setResultMsg(BaseMessages.getVal(result.getResult()));
//				
//			} else {
//				String resMsg = conn.getResponseString();
//				
//				if(resMsg != null && resMsg.contains("code: 600")) {
//					result.setResult("F");
//					result.setResultMsg(BaseMessages.getVal(BaseConstants.ERR_DUPLICATE));
//				} else {
//					result.setResult("F");
//					result.setResultMsg(BaseMessages.getVal(""+ret));
//				}
//			}
//			
//		} catch (Exception e) {
//			conn.closeConnection();
//			logger.error(e.getMessage());
//			result.setResult(BaseConstants.ERR_SYSTEM_ERR);
//			result.setResultMsg(BaseMessages.getVal(result.getResult()));
//		}
//		
//		return result;
//	}
//
//	@Override
//	public ClipPointMessage cancelPlusPoint(SocketRequestVo param) {
//		ClipPointMessage result = new ClipPointMessage();
//		
//		HttpClientUtil conn = null;
//		try {
//			conn = new HttpClientUtil("POST", API_DOMAIN + BaseConstants.ADV_PLUS_POINT_CANCEL, 1000);
//			conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
//			
//			if(BaseConstants.USER_KEY_USER_CI.equals(param.getUser_key_type())){
//				conn.addParameter("user_ci", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
//			} else {
//				conn.addParameter("user_token", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
//			}
//
//			conn.addParameter("transaction_id", param.getTr_id());
//			conn.addParameter("requester_code", param.getRequester_code());
//			conn.addParameter("point_value", ""+param.getPoint_value());
//			conn.addParameter("description", URLEncoder.encode(param.getDescription(),BaseConstants.DEFAULT_ENCODING));
//			conn.addParameter("reference_id", ""+param.getRef_tr_id());
//			
//			int ret = conn.sendRequest();
//			logger.debug("RES_HTTP_STATUS : " + ret);
//
//			if(HttpStatus.SC_OK == ret) {
//				result = conn.getResponseJson(ClipPointMessage.class);
//				result.setResult("S");
//				result.setResultMsg(BaseMessages.getVal(""+ret));
//				
//			} else {
//				String resMsg = conn.getResponseString();
//				
//				if(resMsg != null && resMsg.contains("code: 600")) {
//					result.setResult("F");
//					result.setResultMsg(BaseMessages.getVal(BaseConstants.ERR_DUPLICATE));
//				} else {
//					result.setResult("F");
//					result.setResultMsg(BaseMessages.getVal(""+ret));
//				}
//			}
//			
//		} catch (Exception e) {
//			conn.closeConnection();
//			logger.error(e.getMessage());
//			result.setResult("F");
//			result.setResultMsg(BaseMessages.getVal(BaseConstants.ERR_SYSTEM_ERR));
//		}
//		
//		return result;
//	}
//
//	@Override
//	public ClipPointMessage minusPoint(SocketRequestVo param) {
//		ClipPointMessage result = new ClipPointMessage();
//		
//		HttpClientUtil conn = null;
//		try {
//			conn = new HttpClientUtil("POST", API_DOMAIN + BaseConstants.ADV_MINUS_POINT, 1000);
//			conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
//			
//			if(BaseConstants.USER_KEY_USER_CI.equals(param.getUser_key_type())){
//				conn.addParameter("user_ci", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
//			} else {
//				conn.addParameter("user_token", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
//			}
//
//			conn.addParameter("transaction_id", param.getTr_id());
//			conn.addParameter("requester_code", param.getRequester_code());
//			conn.addParameter("point_value", ""+param.getPoint_value());
//			conn.addParameter("description", URLEncoder.encode(param.getDescription(),BaseConstants.DEFAULT_ENCODING));
//			
//			int ret = conn.sendRequest();
//			logger.debug("RES_HTTP_STATUS : " + ret);
//
//			if(HttpStatus.SC_OK == ret) {
//				result = conn.getResponseJson(ClipPointMessage.class);
//				result.setResult("S");
//				result.setResultMsg(BaseMessages.getVal(result.getResult()));
//				
//			} else {
//				String resMsg = conn.getResponseString();
//				
//				if(resMsg != null && resMsg.contains("code: 600")) {
//					result.setResult("F");
//					result.setResultMsg(BaseMessages.getVal(BaseConstants.ERR_DUPLICATE));
//				} else {
//					result.setResult("F");
//					result.setResultMsg(BaseMessages.getVal(""+ret));
//				}
//			}
//			
//		} catch (Exception e) {
//			conn.closeConnection();
//			logger.error(e.getMessage());
//			result.setResult("F");
//			result.setResultMsg(BaseMessages.getVal(BaseConstants.ERR_SYSTEM_ERR));
//		}
//		
//		return result;
//	}
//
//	@Override
//	public ClipPointMessage cancelMinusPoint(SocketRequestVo param) {
//		ClipPointMessage result = new ClipPointMessage();
//		
//		HttpClientUtil conn = null;
//		try {
//			conn = new HttpClientUtil("POST", API_DOMAIN + BaseConstants.ADV_MINUS_POINT_CANCEL, 1000);
//			conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
//			
//			if(BaseConstants.USER_KEY_USER_CI.equals(param.getUser_key_type())){
//				conn.addParameter("user_ci", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
//			} else {
//				conn.addParameter("user_token", URLEncoder.encode(param.getUser_key(),BaseConstants.DEFAULT_ENCODING));
//			}
//
//			conn.addParameter("transaction_id", param.getTr_id());
//			conn.addParameter("requester_code", param.getRequester_code());
//			conn.addParameter("point_value", ""+param.getPoint_value());
//			conn.addParameter("description", URLEncoder.encode(param.getDescription(),BaseConstants.DEFAULT_ENCODING));
//			conn.addParameter("reference_id", ""+param.getRef_tr_id());
//			
//			int ret = conn.sendRequest();
//			logger.debug("RES_HTTP_STATUS : " + ret);
//
//			if(HttpStatus.SC_OK == ret) {
//				result = conn.getResponseJson(ClipPointMessage.class);
//				result.setResult("S");
//				result.setResultMsg(BaseMessages.getVal(result.getResult()));
//				
//			} else {
//				String resMsg = conn.getResponseString();
//				
//				if(resMsg != null && resMsg.contains("code: 600")) {
//					result.setResult("F");
//					result.setResultMsg(BaseMessages.getVal(BaseConstants.ERR_DUPLICATE));
//				} else {
//					result.setResult("F");
//					result.setResultMsg(BaseMessages.getVal(""+ret));
//				}
//			}
//			
//		} catch (Exception e) {
//			conn.closeConnection();
//			logger.error(e.getMessage());
//			result.setResult("F");
//			result.setResultMsg(BaseMessages.getVal(BaseConstants.ERR_SYSTEM_ERR));
//		}
//		
//		return result;
//	}
//	
//
//	@Override
//	public List<ClipPointMessage> getPointHistoryList(SocketRequestVo param) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public int insertPointSwapHist(PointSwapInfo pointSwapInfo) {
//		pointSwapMapper.insertPointSwapHist(pointSwapInfo);
//		return pointSwapInfo.getIdx();
//	}
//}
