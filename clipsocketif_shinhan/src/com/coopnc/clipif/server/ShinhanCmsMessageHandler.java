package com.coopnc.clipif.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.clipif.bean.ShinhanCmsRequest;
import com.coopnc.clipif.service.ShinhanCmsServerService;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

@Sharable
public class ShinhanCmsMessageHandler extends ChannelInboundHandlerAdapter {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private ShinhanCmsServerService shinhanCmsServerService;
	
	public void setShinhanCmsServerService(final ShinhanCmsServerService shinhanCmsServerService){
		this.shinhanCmsServerService = shinhanCmsServerService;
	}
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		byte[] bmsg = (byte[])msg;

		logger.debug("Incoming message : [{}] ", new String(bmsg));

		procMsg(ctx, bmsg);
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
	
	private void procMsg(ChannelHandlerContext ctx, byte[] msg){

		try {
			ShinhanCmsRequest request = new ShinhanCmsRequest(msg);
			ShinhanCmsRequest response = null;
			
			logger.debug("request.getComm_type() = "+request.getComm_type());
			
			if("0600".equals(request.getComm_type()))
				response = shinhanCmsServerService.proc0600(request);
			else if("0310".equals(request.getComm_type()))
				response = shinhanCmsServerService.proc0310(request);
			else if("0320".equals(request.getComm_type()))
				response = shinhanCmsServerService.proc0320(request); 
			else if("0620".equals(request.getComm_type()))
				response = shinhanCmsServerService.proc0620(request); 
			else if("0630".equals(request.getComm_type()))
				response = shinhanCmsServerService.proc0630(request); 

			if(response !=null) {
				logger.debug("response message ["+response.getMessage().length+"] ["+new String(response.getMessage())+"]");
				ctx.writeAndFlush(response.getMessage());
			} else {
				logger.debug("response empty");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
