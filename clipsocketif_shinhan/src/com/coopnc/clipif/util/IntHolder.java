package com.coopnc.clipif.util;

public class IntHolder {
	
	public int value;
	
	public IntHolder() {}
	
	public IntHolder (int val) {
		this.value = val;
	}
	
	public int val(){
		return this.value;
	}
	
	public void add(int val){
		this.value += val;
	}
	
}
