package com.coopnc.clipif.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.MarshalException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XmlConverter {
	@SuppressWarnings("rawtypes")
	private final ConcurrentMap<Class, JAXBContext> jaxbContexts = new ConcurrentHashMap<Class, JAXBContext>(64);
	
	@SuppressWarnings("unchecked")
	public <T> T readXml(InputStream is, Object o, String charset) throws ParserConfigurationException, SAXException, IOException, Exception {
		try {
			@SuppressWarnings("rawtypes")
			Class clazz = ClassUtils.getUserClass(o);
			
			//JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			//Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

			dbf.setExpandEntityReferences(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(is, charset);

			Unmarshaller jaxbUnmarshaller = createUnmarshaller(clazz);

			return (T) jaxbUnmarshaller.unmarshal(document);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	
	public String writeXml(Object o, String charset) throws Exception {
		
		try {
			@SuppressWarnings("rawtypes")
			Class clazz = ClassUtils.getUserClass(o);
			Marshaller marshaller = createMarshaller(clazz);
			setCharset(charset, marshaller);
			
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			
			Writer stringWriter = new OutputStreamWriter(out, charset);

	        marshaller.marshal(o, stringWriter);
	        
	        String result = new String(out.toByteArray(), charset);
	        return result;
		}
		catch (MarshalException ex) {
			throw new Exception("Could not marshal [" + o + "]: " + ex.getMessage(), ex);
		}
		catch (JAXBException ex) {
			throw new Exception("Could not instantiate JAXBContext: " + ex.getMessage(), ex);
		}
	}
	
	private void setCharset(String charset, Marshaller marshaller) throws PropertyException {
		if (charset != null) {
			marshaller.setProperty(Marshaller.JAXB_ENCODING, charset);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private final Marshaller createMarshaller(Class clazz) throws Exception {
		try {
			JAXBContext jaxbContext = getJaxbContext(clazz);
			return jaxbContext.createMarshaller();
		}
		catch (JAXBException ex) {
			throw new Exception(
					"Could not create Marshaller for class [" + clazz + "]: " + ex.getMessage(), ex);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private final Unmarshaller createUnmarshaller(Class clazz) throws Exception {
		try {
			JAXBContext jaxbContext = getJaxbContext(clazz);
			return jaxbContext.createUnmarshaller();
		}
		catch (JAXBException ex) {
			throw new Exception(
					"Could not create Unmarshaller for class [" + clazz + "]: " + ex.getMessage(), ex);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private final JAXBContext getJaxbContext(Class clazz) throws Exception {
		Assert.notNull(clazz, "'clazz' must not be null");
		JAXBContext jaxbContext = this.jaxbContexts.get(clazz);
		if (jaxbContext == null) {
			try {
				jaxbContext = JAXBContext.newInstance(clazz);
				this.jaxbContexts.putIfAbsent(clazz, jaxbContext);
			}
			catch (JAXBException ex) {
				throw new Exception(
						"Could not instantiate JAXBContext for class [" + clazz + "]: " + ex.getMessage(), ex);
			}
		}
		return jaxbContext;
	}
}
