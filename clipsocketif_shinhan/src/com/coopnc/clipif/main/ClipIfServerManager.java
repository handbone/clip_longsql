package com.coopnc.clipif.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.coopnc.clipif.config.MybatisConfig;
import com.coopnc.clipif.config.SchedulerConfig;
import com.coopnc.clipif.config.SpringConfig;
import com.coopnc.clipif.server.ShinhanCmsServer;

public class ClipIfServerManager {
	
	static Logger logger = LoggerFactory.getLogger(ClipIfServerManager.class);

	static AnnotationConfigApplicationContext context;

	public static void main(String[] args) {
		logger.info("ClipIfServerManager start");
		context = new AnnotationConfigApplicationContext(
				SpringConfig.class
				,MybatisConfig.class
				,SchedulerConfig.class);
		context.registerShutdownHook();
		
		//신한CMS대사 서버 기동
		initShinhanCMSServer();
	}
	
//	private static void initClipIfServer() {
//		try {
//			ClipPointServer clipPointServer = context.getBean(ClipPointServer.class);
//			clipPointServer.start();
//			logger.info("# ClipIfServer start Success #");
//		} catch (Exception e){
//			logger.info("# ClipIfServer start Fail #");
//			logger.error(e.getMessage(), e);
//		}
//	}
	
	private static void initShinhanCMSServer() {
		try {		
			ShinhanCmsServer shinhanCmsServer = context.getBean(ShinhanCmsServer.class);
			shinhanCmsServer.start();
			logger.info("# ShinhanCMSServer start Success #");
		} catch (Exception e){
			logger.info("# ShinhanCMSServer start Fail #");
			logger.error(e.getMessage(), e);
		}
	}
}
