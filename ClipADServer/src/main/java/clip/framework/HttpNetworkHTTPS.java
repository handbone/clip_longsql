/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.framework;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class HttpNetworkHTTPS {

	//@Value("#{props['DOMAIN']}") private String DOMAIN;
	@Value("#{props['API_DOMAIN']}") private String API_DOMAIN;
	@Value("#{props['CLIP_DOMAIN']}") private String CLIP_DOMAIN;
	@Value("#{props['SMS_API']}") private String SMS_API;
	@Value("#{props['MIDAS_URL']}") private String MIDAS_URL;
	
	Logger logger = Logger.getLogger(this.getClass());
	
	public String strGetData(String strDo, String endUrl) throws java.lang.Exception {

		BufferedReader oBufReader = null;
		String strBuffer = "";
		String strRslt = "";
		
			String strEncodeUrl = API_DOMAIN + strDo;

			logger.debug("::::::::: "+strEncodeUrl+"?"+endUrl);
			
			URL oOpenURL = new URL(CLIP_DOMAIN);
			
			//HttpURLConnection httpConn = null;
			//httpConn = (HttpURLConnection) oOpenURL.openConnection();
			
			HttpsURLConnection httpConn = (HttpsURLConnection) oOpenURL.openConnection();
			HttpsURLConnection.setDefaultHostnameVerifier( new HostnameVerifier(){
			    public boolean verify(String string,SSLSession ssls) {
			     return true;
			    }
			});
			
			httpConn.setDoOutput(true);
			httpConn.setRequestMethod("POST");
			httpConn.setConnectTimeout(5000);
			httpConn.setReadTimeout(10000);
			httpConn.setRequestProperty("Content-Length", String.valueOf(endUrl.length()));
			OutputStream os = httpConn.getOutputStream();
			os.write(endUrl.getBytes());
			os.flush();
			os.close();

			oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),BaseConstant.DEFAULT_ENCODING));

			StringBuffer sb = new StringBuffer(strRslt);
			
			int i = 1;
			while ((strBuffer = oBufReader.readLine()) != null && (i < 2)) {
				if (strBuffer.length() > 1) {
					sb.append(strBuffer);
				}
			}

			oBufReader.close();
			
		return sb.toString();
	}

	
	public String strGetUserCi(String params) throws java.lang.Exception {
		
		BufferedReader oBufReader = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = CLIP_DOMAIN;
		
		logger.debug("::::::::: "+strEncodeUrl+"?"+params);
		
		URL oOpenURL = new URL(CLIP_DOMAIN);
		
		//HttpURLConnection httpConn = null;
		//httpConn = (HttpURLConnection) oOpenURL.openConnection();
		
		HttpsURLConnection httpConn = (HttpsURLConnection) oOpenURL.openConnection();
		HttpsURLConnection.setDefaultHostnameVerifier( new HostnameVerifier(){
		    public boolean verify(String string,SSLSession ssls) {
		     return true;
		    }
		});
		
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(5000);
		httpConn.setReadTimeout(10000);
		
		httpConn.setDoInput(true);
		httpConn.setRequestProperty("Accept", "application/json");
		httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		
		httpConn.setRequestProperty("Content-Length", String.valueOf(params.length()));
		OutputStream os = httpConn.getOutputStream();
		os.write(params.getBytes());
		os.flush();
		os.close();
		
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),BaseConstant.DEFAULT_ENCODING));

		StringBuffer sb = new StringBuffer(strRslt);
		
		int i = 1;
		while ((strBuffer = oBufReader.readLine()) != null && (i < 2)) {
			if (strBuffer.length() > 1) {
				sb.append(strBuffer);
			}
		}

		oBufReader.close();
			
		
		return sb.toString();
	}	
	
	/**
	 * time
	 * @return
	 */
	public static String getCurrentDate24() {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				java.util.Locale.KOREA);
		return formatter.format(new java.util.Date());
	}
	
	
	public String smsSend(String parameter) throws java.lang.Exception {

		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = SMS_API;

		logger.debug("SMS URL & Parameter : "+SMS_API + parameter);
		
		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(5000);
		httpConn.setReadTimeout(10000);
		
		httpConn.setDoInput(true);
		httpConn.setRequestProperty("Content-Length", String.valueOf(parameter.length()));
		OutputStream os = httpConn.getOutputStream();
		os.write(parameter.getBytes());
		os.flush();
		os.close();

		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));

		StringBuffer sb = new StringBuffer(strRslt);
		
		int i = 1;
		while ((strBuffer = oBufReader.readLine()) != null && (i < 2)) {
			if (strBuffer.length() > 1) {
				sb.append(strBuffer);
			}
		}

		oBufReader.close();
		
		return sb.toString();
	}


	public String sendingMidasLog(String params) throws Exception {
		BufferedReader oBufReader = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = MIDAS_URL;
		
		logger.debug("::::::::: "+strEncodeUrl+"?"+params);
		
		URL oOpenURL = new URL(strEncodeUrl);
		
		HttpsURLConnection httpConn = (HttpsURLConnection) oOpenURL.openConnection();
		HttpsURLConnection.setDefaultHostnameVerifier( new HostnameVerifier(){
		    public boolean verify(String string,SSLSession ssls) {
		     return true;
		    }
		});
		
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(5000);
		httpConn.setReadTimeout(10000);
		
		httpConn.setDoInput(true);
		httpConn.setRequestProperty("Accept", "application/json");
		httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//		httpConn.setRequestProperty("Accept-Encoding", "gzip,deflate");
//		httpConn.setRequestProperty("Connection", "Keep-Alive");
		httpConn.setRequestProperty("Content-Length", String.valueOf(params.length()));
//		httpConn.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1");
		
		OutputStream os = httpConn.getOutputStream();
		os.write(params.getBytes("utf-8"));
		os.flush();
		os.close();
		
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),BaseConstant.DEFAULT_ENCODING));

		StringBuffer sb = new StringBuffer(strRslt);
		
		int i = 1;
		while ((strBuffer = oBufReader.readLine()) != null && (i < 2)) {
			if (strBuffer.length() > 1) {
				sb.append(strBuffer);
			}
		}

		oBufReader.close();
			
		return sb.toString();
	}
	
	
}
