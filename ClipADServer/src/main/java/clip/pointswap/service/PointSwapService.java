package clip.pointswap.service;

import javax.servlet.http.HttpServletRequest;

import clip.roulette.bean.PointSwap;

public interface PointSwapService {
	
	public void testSwap() throws Exception ;
	
	public PointSwap getCardPoints(HttpServletRequest request, PointSwap param) throws Exception;
	
	public PointSwap swapCardPoints(HttpServletRequest request, PointSwap param) throws Exception;
	
	public PointSwap getCardPointsTest(HttpServletRequest request, PointSwap param) throws Exception;
	
	public PointSwap swapCardPointsTest(HttpServletRequest request, PointSwap param) throws Exception;

	public PointSwap isShinhanFan(HttpServletRequest request, PointSwap param) throws Exception;

	public int pointSwapCancelBatch() throws Exception;
	
}
