/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import clip.common.crypto.AES256CipherClip;
import clip.framework.BaseConstant;
import clip.framework.HttpNetwork;
import clip.roulette.bean.PointApicallLog;
import clip.roulette.bean.PointRoulette;
import clip.roulette.bean.PointRouletteAuth;
import clip.roulette.bean.PointRouletteConnLog;
import clip.roulette.bean.PointRouletteJoin;
import clip.roulette.bean.PointRouletteJoinSum;
import clip.roulette.bean.PointRouletteOptin;
import clip.roulette.bean.PointRouletteSetLog;
import clip.roulette.bean.RouletteDto;
import clip.roulette.dao.RouletteDao;

@Service
@Transactional
public class RouletteServiceImpl implements RouletteService {

	Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private RouletteDao rouletteDao;
	
	@Autowired
	private HttpNetwork httpNetwork;
	
	@Value("#{props['SMS_AUTH_MSG_1']}") private String SMS_AUTH_MSG_1;
	@Value("#{props['SMS_AUTH_MSG_2']}") private String SMS_AUTH_MSG_2;
	
	
	
	@Override
	public PointRoulette selectPointRoulette(String roulette_id, HttpServletRequest request) throws java.lang.Exception {
		
		// 이벤트의 정보를 가져온다
		PointRoulette pointRoulette = rouletteDao.selectPointRoulette(roulette_id);
		
		// 이벤트 기간이 끝났으면 리턴
		if ("null".equals(String.valueOf(pointRoulette))) {
			return pointRoulette;
		}
		
		// 시간구분 당첨값이 설정되어있지 않으면 시간설정로그값을 남기지 않는다.
		int setCnt = 1;
		if("Y".equals(pointRoulette.getTimegbn())){
			setCnt = rouletteDao.pointRouletteSetLogCnt(roulette_id);
		}
		
		if( setCnt == 0){
			// 없으면 만들어서 pointRoulette 에 update  
			// PointRouletteSetLog 에 insert
			PointRouletteSetLog pointRouletteSetLog = new PointRouletteSetLog();
			pointRouletteSetLog.setRoulette_id(roulette_id);
			pointRouletteSetLog.setSettime(BaseConstant.SELECTED_TIME[(int) (Math.random() * BaseConstant.SELECTED_TIME.length)]);
			pointRouletteSetLog.setSetnum(BaseConstant.SELECTED_TIME_NUM);
			pointRouletteSetLog.setIpaddr(request.getRemoteAddr());
			
			rouletteDao.insertPointRouletteSetLog(pointRouletteSetLog);
			rouletteDao.updatePointRoulette(pointRouletteSetLog);
		}
		
		
		
		
		//PointRouletteJoinSum 에 오늘 합계표가 없으면 만든다.
		PointRouletteJoin pointRouletteJoin = new PointRouletteJoin();
		pointRouletteJoin.setRoulette_id(roulette_id);
		int joinSumCnt = rouletteDao.selectPointRouletteJoinSumByDate(pointRouletteJoin);
		if( 0 == joinSumCnt){
			logger.debug("CLIPPOINTERROR 200:'pointRouletteJoinSum' today row nothing. scheduler not working.");
			PointRouletteJoinSum pointRouletteJoinSum = new PointRouletteJoinSum();
			pointRouletteJoinSum.setRoulette_id(pointRoulette.getRoulette_id());
			rouletteDao.insertPointRouletteJoinSum(pointRouletteJoinSum);
			return pointRoulette;
		}else{
			return pointRoulette;
		}
		
	}

	@Override
	public PointRoulette pointRouletteNoChkDate(String roulette_id, HttpServletRequest request)
			throws java.lang.Exception {
		// 이벤트의 정보를 가져온다
		return rouletteDao.pointRouletteNoChkDate(roulette_id);
	}
	
	// 특정 사용자가 당일 이벤트에 참여했는지 확인
	public int getDailyCntChk(RouletteDto rouletteDto) throws java.lang.Exception {
		return rouletteDao.getDailyCntChk(rouletteDto);
	}


	@Override
	public PointRouletteJoin getPointRouletteJoin(PointRoulette pointRoulette, RouletteDto rouletteDto,
			PointRouletteJoin pointRouletteJoin, HttpServletRequest request) throws java.lang.Exception {

		
		PointRoulette selectedPointRoulette = new PointRoulette();
		
		//logger.debug("PointRoulette Safe Mode : " + pointRoulette.getEndtime());
		
		if("Y".equals(pointRoulette.getEndtime())){ // 최고당첨값 안전당첨 설정이 Y이면
			// 설정값에따라 당첨자 설정하는부분을 달리한다. (마지막에 한번더 체크하는 로직도 다르다.)
			selectedPointRoulette = rouletteDao.getSelectedPointRoulette(pointRoulette);
		}else{
			// 정보를 파악하여 최고 당첨자가 아니라면 적당한 부분에 당첨시킨다.
			// 현재 사용자의 당첨값을 DB에 입력해놓고 완료시키지는 않는다.
			// 이벤트 정보를 참조하고 이벤트 당첨자 통계데이블을 참조하여 , 최고 당첨값에 해당하는 사람인지 체크하면서
			// 이벤트의 정보를 가져온다
			selectedPointRoulette = rouletteDao.getSelectedPointRoulette2(pointRoulette);
		}
		
		pointRouletteJoin.setIpaddr(request.getRemoteAddr());
		pointRouletteJoin.setStatus("P"); // 진행중

		// 1등 당첨자이면 바로 입력해준다.
		if (!"0".equals(selectedPointRoulette.getSelected_user())) {
			pointRouletteJoin.setResult("6"); // 시간설정 당첨자는 6번만 가능하다.
			pointRouletteJoin.setSave_point(selectedPointRoulette.getPoint06()); 
		} else {
			
			// 설정된 당첨자 수가 남아있는지 체크한다.
			int possibleCnt = Integer.parseInt(selectedPointRoulette.getMax01()) //[max01]확률형으로 변경
					+ Integer.parseInt(selectedPointRoulette.getMax02())
					+ Integer.parseInt(selectedPointRoulette.getMax03())
					+ Integer.parseInt(selectedPointRoulette.getMax04())
					+ Integer.parseInt(selectedPointRoulette.getMax05())
					+ Integer.parseInt(selectedPointRoulette.getMax06());
			
			
			
			// max6이 시간설정 당첨값으로 설정되어 있다면 그 숫자는 포함시키지 않는다.
			if (null != selectedPointRoulette.getTimegbn()) {
				possibleCnt = possibleCnt - Integer.parseInt(selectedPointRoulette.getMax06());
			}

			// 모두 소진되었다면 이 참가자는 area01에 당첨된다. 최소값으로 매칭해주는것이다
			if (possibleCnt <= 0) {
				pointRouletteJoin.setResult("1");
				pointRouletteJoin.setSave_point(selectedPointRoulette.getPoint01());
				
			} else {
				// 남아있는 당첨인원이 있으면 랜덤으로 당첨시킨다.
				int randomSelectNum = 0;
				int selectedMaxNum = 0;
				String timeGbn = selectedPointRoulette.getTimegbn();
				
				// 당첨인원이 남아있는 항목을 랜덤으로 선택한다.
				// 남아있는 당첨인원이 0인 항목은 제외한다 
				while (selectedMaxNum <= 0) {
					// 2~5 혹은 2~6까지의 숫자를 랜덤으로 생성한다.
					// [max01]확률형으로 변경으로 인해
					// 1~5 혹은 1~6까지의 숫자를 랜덤으로 생성한다.
					if ( null == timeGbn) {
						randomSelectNum = randomSelectNum(6,selectedPointRoulette);
					}else{
						randomSelectNum = randomSelectNum(5,selectedPointRoulette);
					}
					
					// 선택 된 숫자에 해당하는 '남아있는 당첨인원'을 반환한다
					selectedMaxNum = selectedMaxNum(randomSelectNum, selectedMaxNum, selectedPointRoulette);
				}

				//logger.debug("randomSelectNum:::"+randomSelectNum);
				
				// 포인트
				switch (randomSelectNum) {
				case 1:// [max01]확률형으로 변경으로 인해 추가됨
					pointRouletteJoin.setResult("1");
					pointRouletteJoin.setSave_point(selectedPointRoulette.getPoint01());
					break;
				case 2:
					pointRouletteJoin.setResult("2");
					pointRouletteJoin.setSave_point(selectedPointRoulette.getPoint02());
					break;
				case 3:
					pointRouletteJoin.setResult("3");
					pointRouletteJoin.setSave_point(selectedPointRoulette.getPoint03());
					break;
				case 4:
					pointRouletteJoin.setResult("4");
					pointRouletteJoin.setSave_point(selectedPointRoulette.getPoint04());
					break;
				case 5:
					pointRouletteJoin.setResult("5");
					pointRouletteJoin.setSave_point(selectedPointRoulette.getPoint05());
					break;
				case 6:
					pointRouletteJoin.setResult("6");
					pointRouletteJoin.setSave_point(selectedPointRoulette.getPoint06());
					break;
				default :
					break;
				}
			}

		}

		
		//TODO SUNNY DELETE 더미
		//pointRouletteJoin.setRoulette_id(rouletteDto.getRoulette_id());
		//pointRouletteJoin.setCust_id("ZjZxSUVNVHMvOEpnQ3dBK3ZGckNjQT09");
		
		//logger.debug("@@@@@@@@@@@@@@@@@ 8888 "+pointRouletteJoin.getRoulette_id());
		
		int resultCnt = rouletteDao.insertPointRouletteJoin(pointRouletteJoin);
		
		PointRouletteConnLog pointRouletteConnLog = new PointRouletteConnLog();
		pointRouletteConnLog.setRoulette_id(pointRouletteJoin.getRoulette_id());
		pointRouletteConnLog.setCust_id(pointRouletteJoin.getCust_id());		
		pointRouletteConnLog.setGa_id(pointRouletteJoin.getGa_id());
		pointRouletteConnLog.setCtn(pointRouletteJoin.getCtn());
		pointRouletteConnLog.setIpaddr(pointRouletteJoin.getIpaddr());
		
		// 참여로그쌓기
		if( resultCnt > 0){
			// 안쌓기로 함.
			//rouletteDao.insertPointRouletteConnLog(pointRouletteConnLog);
		}
		
		logger.debug("Selected Point ==============================");
		logger.debug("Result : " + pointRouletteJoin.getResult());
		logger.debug("Save_point : " + pointRouletteJoin.getSave_point());
		logger.debug("Status : " + pointRouletteJoin.getStatus());
		
		return pointRouletteJoin;
	}

	// 선택 된 숫자에 해당하는 '남아있는 당첨인원'을 반환한다
	private int selectedMaxNum(int temprandomSelectNum, int tempselectedMaxNum, PointRoulette selectedPointRoulette) {
		
		int randomSelectNum = temprandomSelectNum;
		int selectedMaxNum = tempselectedMaxNum;
		
		switch (randomSelectNum) {
		case 1: // [max01]확률형으로 변경으로 인해 추가됨
			selectedMaxNum = Integer.parseInt(selectedPointRoulette.getMax01());
			break;
		case 2:
			selectedMaxNum = Integer.parseInt(selectedPointRoulette.getMax02());
			break;
		case 3:
			selectedMaxNum = Integer.parseInt(selectedPointRoulette.getMax03());
			break;
		case 4:
			selectedMaxNum = Integer.parseInt(selectedPointRoulette.getMax04());
			break;
		case 5:
			selectedMaxNum = Integer.parseInt(selectedPointRoulette.getMax05());
			break;
		case 6:
			selectedMaxNum = Integer.parseInt(selectedPointRoulette.getMax06());
			break;
		default :
			break;
		}
		
		//logger.debug("randomSelectNum==========================================================");
		//logger.debug(randomSelectNum);
		
		return selectedMaxNum;
	}

	// 2~5 혹은 2~6까지의 숫자를 확률에 따라 랜덤으로 생성한다.
	// [max01]확률형으로 변경으로 인해
	// 1~5 혹은 1~6까지의 숫자를 확률에 따라 랜덤으로 생성한다.
	private int randomSelectNum(int numRang, PointRoulette selectedPointRoulette) {
		
		int result = 0;
		
		/*selectedPointRoulette.setMax01("20000");
		selectedPointRoulette.setMax02("20000");
		selectedPointRoulette.setMax03("100");
		selectedPointRoulette.setMax04("10000");
		selectedPointRoulette.setMax05("10");
		selectedPointRoulette.setMax06("5");
		logger.debug("=================== 입력값 ===================");
		logger.debug("1번경품 : 20000");
		logger.debug("2번경품 : 20000");
		logger.debug("2번경품 : 100");
		logger.debug("2번경품 : 10000");
		logger.debug("2번경품 : 10");
		logger.debug("2번경품 : 5");
		logger.debug("==============================================");*/
		
		// 확률을 만들기 위해 모두 더해준다.
		float maxInt =  Integer.parseInt(selectedPointRoulette.getMax01()) // [max01]확률형으로 변경으로 인해
				+ Integer.parseInt(selectedPointRoulette.getMax02()) 
				+ Integer.parseInt(selectedPointRoulette.getMax03()) 
				+ Integer.parseInt(selectedPointRoulette.getMax04()) 
				+ Integer.parseInt(selectedPointRoulette.getMax05())
				+ Integer.parseInt(selectedPointRoulette.getMax06());
		
		/*logger.debug("maxInt:::"+maxInt);
		logger.debug("Integer.parseInt(selectedPointRoulette.getMax01())/maxInt:::"+Float.parseFloat(selectedPointRoulette.getMax01())/maxInt);
		logger.debug("Integer.parseInt(selectedPointRoulette.getMax02())/maxInt:::"+Float.parseFloat(selectedPointRoulette.getMax02())/maxInt);
		logger.debug("Integer.parseInt(selectedPointRoulette.getMax03())/maxInt:::"+Float.parseFloat(selectedPointRoulette.getMax03())/maxInt);
		logger.debug("Integer.parseInt(selectedPointRoulette.getMax04())/maxInt:::"+Float.parseFloat(selectedPointRoulette.getMax04())/maxInt);
		logger.debug("Integer.parseInt(selectedPointRoulette.getMax05())/maxInt:::"+Float.parseFloat(selectedPointRoulette.getMax05())/maxInt);
		logger.debug("Integer.parseInt(selectedPointRoulette.getMax06())/maxInt:::"+Float.parseFloat(selectedPointRoulette.getMax06())/maxInt);
		*/
		// 확률계산
		float convertfloat01 = Float.parseFloat(selectedPointRoulette.getMax01())/maxInt;// [max01]확률형으로 변경으로 인해
		float convertfloat02 = Float.parseFloat(selectedPointRoulette.getMax02())/maxInt;
		float convertfloat03 = Float.parseFloat(selectedPointRoulette.getMax03())/maxInt;
		float convertfloat04 = Float.parseFloat(selectedPointRoulette.getMax04())/maxInt;
		float convertfloat05 = Float.parseFloat(selectedPointRoulette.getMax05())/maxInt;
		float convertfloat06 = Float.parseFloat(selectedPointRoulette.getMax06())/maxInt;
		
		/*logger.debug("convertfloat01:::"+convertfloat01);
		logger.debug("convertfloat02:::"+convertfloat02);
		logger.debug("convertfloat03:::"+convertfloat03);
		logger.debug("convertfloat04:::"+convertfloat04);
		logger.debug("convertfloat05:::"+convertfloat05);
		logger.debug("convertfloat06:::"+convertfloat06);		
		*/
		/*float resultCnt01 = 0;
		float resultCnt02 = 0;
		float resultCnt03 = 0;
		float resultCnt04 = 0;
		float resultCnt05 = 0;
		float resultCnt06 = 0; 
		*/
		/*logger.debug("반복횟수 : 50000번");
		for(int i = 0 ; i < 50000 ; i++){
			result = 0;
			maxInt = maxInt - 1;
		*/	
			
			
			
			
			
			while( result == 0 ){
				
				if(numRang == 6){
					
					int thisSelectNumber = ((int) (Math.random() * 6) + 1); // [max01]확률형으로 변경으로 인해 5를 6으로 변경, 2를 1로 변경
					switch(thisSelectNumber){
						case 1: // [max01]확률형으로 변경으로 인해 추가
							if(Math.random() < convertfloat01){
								//logger.debug("return 2");
								result = 1;
								//resultCnt01 = resultCnt01 + 1;
								return result;
							}
							break;
						case 2:
							if(Math.random() < convertfloat02){
								//logger.debug("return 2");
								result = 2;
								//resultCnt02 = resultCnt02 + 1;
								return result;
							}
							break;
						case 3:
							if(Math.random() < convertfloat03){
								//logger.debug("return 333");
								result = 3;
								//resultCnt03 = resultCnt03 + 1;
								return result;
							}
							break;
						case 4:
							if(Math.random() < convertfloat04){
								//logger.debug("return 444444");
								result = 4;
								//resultCnt04 = resultCnt04 + 1;
								return result;
							}
							
							break;
						case 5:
							if(Math.random() < convertfloat05){
								//logger.debug("return 5555555555");
								result = 5;
								//resultCnt05 = resultCnt05 + 1;
								return result;
							}
							break;
						case 6:
							if(Math.random() < convertfloat06){
								//logger.debug("return 66666666666666666");
								result = 6;
								//resultCnt06 = resultCnt06 + 1;
								return result;
							}
							break;
						default :
							return result;
					}
				}else{

					int thisSelectNumber = ((int) (Math.random() * 5) + 1);// [max01]확률형으로 변경으로 인해 4를 5로 변경, 2를 1로 변경
					switch(thisSelectNumber){
						case 1:// [max01]확률형으로 변경으로 인해 추가
							if(Math.random() < convertfloat01){
								//logger.debug("return 2");
								result = 1;
								//resultCnt01 = resultCnt01 + 1;
								return result;
							}
							break;
						case 2:
							if(Math.random() < convertfloat02){
								//logger.debug("return 2");
								result = 2;
								//resultCnt02 = resultCnt02 + 1;
								return result;
							}
							break;
						case 3:
							if(Math.random() < convertfloat03){
								//logger.debug("return 333");
								result = 3;
								//resultCnt03 = resultCnt03 + 1;
								return result;
							}
							break;
						case 4:
							if(Math.random() < convertfloat04){
								//logger.debug("return 444444");
								result = 4;
								//resultCnt04 = resultCnt04 + 1;
								return result;
							}
							
							break;
						case 5:
							if(Math.random() < convertfloat05){
								//logger.debug("return 5555555555");
								result = 5;
								//resultCnt05 = resultCnt05 + 1;
								return result;
							}
							break;
						default :
							return result;
					}
				}
			
		}
			
			
		/*}
		float totalResult = resultCnt01 + resultCnt02 + resultCnt03 + resultCnt04 + resultCnt05 + resultCnt06;
		
		logger.debug("========================= 결과 ==============================");
		logger.debug("resultCnt01 의 횟수 : "+resultCnt01 + "회");
		logger.debug("resultCnt02 의 횟수 : "+resultCnt02 + "회");
		logger.debug("resultCnt03 의 횟수 : "+resultCnt03 + "회");
		logger.debug("resultCnt04 의 횟수 : "+resultCnt04 + "회");
		logger.debug("resultCnt05 의 횟수 : "+resultCnt05 + "회");
		logger.debug("resultCnt06 의 횟수 : "+resultCnt06 + "회"); 
		
		logger.debug("resultCnt01 의 비율 : "+resultCnt01 / totalResult + "%");
		logger.debug("resultCnt02 의 비율 : "+resultCnt02 / totalResult + "%");
		logger.debug("resultCnt03 의 비율 : "+resultCnt03 / totalResult + "%");
		logger.debug("resultCnt04 의 비율 : "+resultCnt04 / totalResult + "%");
		logger.debug("resultCnt05 의 비율 : "+resultCnt05 / totalResult + "%");
		logger.debug("resultCnt06 의 비율 : "+resultCnt06 / totalResult + "%"); 	
		logger.debug("=============================================================");
		*/
		return result;
	}
	

	

	// 룰렛이벤트를 완료하는 로직
	@Override
	public int rouletteComplete(RouletteDto temprouletteDto, PointRouletteJoin pointRouletteJoin,
			HttpServletRequest request) throws java.lang.Exception {
		
		StringBuffer log = new StringBuffer();
    	log.append("\r\n===================================================================\r\n");
    	Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyy.MM.dd HH:mm:ss.SSS");
    	log.append("Request Time=" + format.format(calendar.getTime()) + "\r\n");
    	
		RouletteDto rouletteDto = temprouletteDto;
		
		// 포인트 api 연동 
		String strDo = BaseConstant.PLUS_POINT;
		
		String idString = "";
		rouletteDto = selectId(rouletteDto);
		
		StringBuffer sb = new StringBuffer(idString);
		
		
		switch (Integer.parseInt(rouletteDto.getKeytype())) {
		case 1:
			sb.append("cust_id=").append(rouletteDto.getCust_id());
			break;
		case 3:
			sb.append("cnt=").append(rouletteDto.getCtn());
			break;
		case 2:
			sb.append("ga_id=").append(rouletteDto.getGa_id());
			break;
		default :
			break;
		}
		idString = sb.toString();
		
		// 화면에서 받아오던 값을 없애고 DB에 미리 저장되어있던 것을 꺼내와서 사용한다.
		PointRouletteJoin pointRouletteJoinTemp = rouletteDao.selectPointRouletteJoin(pointRouletteJoin);
		pointRouletteJoin.setIdx(pointRouletteJoinTemp.getIdx());
		pointRouletteJoin.setSave_point(pointRouletteJoinTemp.getSave_point());
		pointRouletteJoin.setResult(pointRouletteJoinTemp.getResult());
		
		// 이벤트의 정보를 가져온다
		PointRoulette pointRoulette = rouletteDao.selectPointRoulette(pointRouletteJoin.getRoulette_id());
				
		int max06Chk = 1; // 0 이외의 값으로 초기화 한다.
		if("6".equals(pointRouletteJoin.getResult()) && "Y".equals(pointRoulette.getTimegbn())){
			
			if("Y".equals(pointRoulette.getEndtime())){ // 최고당첨값 안전당첨 설정이 Y이면
				// '나' 이후에 접속해서 6번이 당첨 된 사람이 있는지 체크한다.( P & C 모두 포함) 있으면 '나'를 꽝으로 보낸다.
				// 최고당첨값으로 접속한 사람이 중복이 되었을 경우에는 맨 마지막 사람만 당첨되는 로직이다.
				max06Chk = rouletteDao.max06ChkByEndTime(rouletteDto);
				if( max06Chk > 0 ){ // 다른 사용자가 1명이라도 있으면 꽝이다
					max06Chk = 0; // 꽝이기 때문에 0으로 바꿔준다.
				}else{ 
					max06Chk = 1; // 다른 사용자가 1명도 없으면 기존대로 당첨된다.
				}
			}else{
				// 시간 구분 당첨이든 그냥 일반 당첨이든 06번에 당첨되면 최고 당첨값을 체크한다.
				// summary 테이블을 검색하여 오늘 당첨될 인원수가 넘었는지 다시한번 체크한다.
				max06Chk = rouletteDao.max06Chk(pointRouletteJoin);
			}
		}
		logger.debug("Same time init check for 'Max06'.  : " + max06Chk);
		if( 0 == max06Chk ){ // 중복이 발생하는 상황으로 해당 당첨자는 꽝으로 보내야 한다.
			pointRouletteJoin.setSave_point("0");
			pointRouletteJoin.setResult("1");
		}
		
		
		
		//requester_code DB에서 조회해와서 설정한다.
		String requester_code = rouletteDao.selectRequesterCode(pointRouletteJoin);		
		//requester_code DB에서 조회해와서 설정한다.
		String requester_description = rouletteDao.selectRequesterDescription(pointRouletteJoin);		
		
		// tr_id(사용자의 유일한 값) 설정
		String tr_id = String.valueOf(System.currentTimeMillis());
		if("".equals(tr_id) || null == tr_id){
			throw new java.lang.Exception();
		}
		StringBuffer sb2 = new StringBuffer(tr_id);
		if (tr_id.length() < 16) {
			sb2.append(pointRouletteJoin.getIdx().substring(pointRouletteJoin.getIdx().length()-3, pointRouletteJoin.getIdx().length()));
		}
		if (tr_id.length() < 16) {
			while (sb2.length() < 16) {
				sb2.append("0");
			}
		}
		tr_id = sb2.toString();
		pointRouletteJoin.setTr_id(tr_id);
		
		String endUrl = idString
						+"&transaction_id="+pointRouletteJoin.getTr_id()
						+"&requester_code="+requester_code
						+"&point_value="+pointRouletteJoin.getSave_point()
						+"&description="+java.net.URLEncoder.encode(requester_description,BaseConstant.DEFAULT_ENCODING);
		endUrl = endUrl.replaceAll("(\r\n|\n)", "");
		endUrl = endUrl.replaceAll(System.getProperty("line.separator"), "");
		
		// Plus Point API 접속 URL
		logger.debug("Plus Point API URL : "+strDo+"?"+endUrl);
		
		String strHtmlSource = "";
		
		// 통신 후 로그쌓을 준비
		PointApicallLog pointApicallLog = new PointApicallLog();
		pointApicallLog.setCust_id(rouletteDto.getCust_id());
		pointApicallLog.setCtn(rouletteDto.getCtn());
		pointApicallLog.setGa_id(rouletteDto.getGa_id());
		pointApicallLog.setUser_ci("");
		pointApicallLog.setApiurl(strDo);
		pointApicallLog.setTr_id(pointRouletteJoin.getTr_id());
		pointApicallLog.setTr_response("0");
		pointApicallLog.setIpaddr(request.getRemoteAddr());
		
		
        
        
		if("0".equals(pointRouletteJoin.getSave_point())){
			strHtmlSource = "No Request.";
		}else{
			try{
				// 실제 통신이 일어나는 부분
				strHtmlSource = httpNetwork.strGetData(strDo,endUrl);
				logger.debug("Plus Point API URL Response : "+strHtmlSource);
			}catch (Exception e ){
				// 통신에서 에러가 발생했을때 에러 메시지를 로그로 남긴다
				pointApicallLog.setTr_response(e.getMessage());
				logger.debug("Plus Point API URL Response : "+e.getMessage());
				if(pointApicallLog.getTr_response().contains("code: 600")){
					logger.debug("CLIPPOINTERROR 305:pointPlus http api error.");
	        	}else{
	        		logger.debug("CLIPPOINTERROR 300:pointPlus http api error.");
	        	}
			}
		}
		
    	log.append("Request URL:" + request.getRequestURL().toString() + "\r\n");
    	log.append("Plus Point API URL :" + strDo+ "\r\n");
    	log.append("Plus Point API Parameter [idString]:" + idString + "\r\n");
    	log.append("Plus Point API Parameter [transaction_id]:" + pointRouletteJoin.getTr_id() + "\r\n");
    	log.append("Plus Point API Parameter [requester_code]:" + requester_code + "\r\n");
    	log.append("Plus Point API Parameter [point_value]:" + pointRouletteJoin.getSave_point()+ "\r\n");
    	log.append("Plus Point API Parameter [description]:" + requester_description + "\r\n");
    	if("".equals(strHtmlSource)){ // 통신실패 혹은 중복적립(600)
    		log.append("Plus Point API Response :" + pointApicallLog.getTr_response() + "\r\n");
    	}else{
    		log.append("Plus Point API Response :" + strHtmlSource + "\r\n");
    	}
    	log.append("Request IP=" + request.getRemoteAddr() + "\r\n");
    	Calendar calendar2 = Calendar.getInstance();
        log.append("Response Time=" + format.format(calendar2.getTime()) + "\r\n");
        log.append("===================================================================\r\n");
        logger.info(log.toString());
        
        //연동로그 입력 : 성공시에는 Tr_response = 0
        rouletteDao.insertPointApicallLog(pointApicallLog);
        
        if("".equals(strHtmlSource)){ // 통신실패 혹은 중복적립
        	if(pointApicallLog.getTr_response().contains("code: 600")){ // 중복적립
        		return 3;
        	}else{
        		return 0;
        	}
		}else{ // 성공
			
			//당첨값 확정 update
			pointRouletteJoin.setIpaddr(request.getRemoteAddr());
			int joinUpdateCnt = rouletteDao.updatePointRouletteJoin(pointRouletteJoin);
			
			int updatePointRouletteJoinSum = 0;
			
			// API로 적립이 끝났는데 우리 서버가 이상이 생겨서 당첨값이 확정이 안되면 적립을 차감한다.
			// 차감 로직은 만들지 않는다. 에러 로그는 서버에 파일로 담기는것으로 대체한다.
			if( 1 != joinUpdateCnt && !"".equals(strHtmlSource)){
				logger.debug("CLIPPOINTERROR 400:'joinUpdateCnt' is 0.");
				return 0;
			}else if(1 == joinUpdateCnt){
				// 업데이트가 정상적으로 완료됐다
				
					PointRouletteJoinSum pointRouletteJoinSum = new PointRouletteJoinSum();
					pointRouletteJoinSum.setRoulette_id(pointRouletteJoin.getRoulette_id());
					pointRouletteJoinSum.setMax_plus(pointRouletteJoin.getResult());
					updatePointRouletteJoinSum = rouletteDao.updatePointRouletteJoinSum(pointRouletteJoinSum);
				
			}
			
			
			// 업데이트 항목은 1개여야만 하지만, 오류로 인해 sum 테이블이 두개 생성되어 있다면 카운트숫자는 1보다 클 수 있다.
			if( 1 <= updatePointRouletteJoinSum && 0 != max06Chk){
				return 1;
			}else if( 1 <= updatePointRouletteJoinSum && 0 == max06Chk){
				return 2;
			}else{
				logger.debug("CLIPPOINTERROR 401:'updatePointRouletteJoinSum' update count is 0.");
				return 0;
			}
		
		}
	}
		
	@Override
	public int agreeInsert(RouletteDto rouletteDto, HttpServletRequest request) throws java.lang.Exception {
		
		PointRouletteOptin pointRouletteOptin = new PointRouletteOptin();

		if("1".equals(rouletteDto.getKeytype())){
			pointRouletteOptin.setCust_id(rouletteDto.getCust_id());
		}else if("3".equals(rouletteDto.getKeytype())){
			pointRouletteOptin.setCtn(rouletteDto.getCtn());
		}else if("2".equals(rouletteDto.getKeytype())){
			pointRouletteOptin.setGa_id(rouletteDto.getGa_id());
		}
		
		pointRouletteOptin.setRoulette_id(rouletteDto.getRoulette_id());
		pointRouletteOptin.setStatus(rouletteDto.getStatus());
		
		String USER_IP = request.getRemoteAddr();
		if (USER_IP.equalsIgnoreCase(BaseConstant.LOCAL_IP_6)) {
		    InetAddress inetAddress;
			try {
				inetAddress = InetAddress.getLocalHost();
				String ipAddress = inetAddress.getHostAddress();
			    USER_IP = ipAddress;
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		    
		}
		pointRouletteOptin.setIpaddr(USER_IP);
		
		return rouletteDao.agreeInsert(pointRouletteOptin);
	}

	@Override
	public int agreeChk(RouletteDto rouletteDto) throws java.lang.Exception {
		return rouletteDao.agreeChk(rouletteDto);
	}
	// keytype에 따라 keyid를 셋팅해준다
		public RouletteDto selectId(RouletteDto rouletteDto) throws java.lang.Exception {
			
			switch (Integer.parseInt(rouletteDto.getKeytype())) {
			case 1:
				rouletteDto.setCust_id(rouletteDto.getKeyid());
				break;
			case 3:
				//logger.debug("rouletteDto.getCtn()::::::"+rouletteDto.getKeyid());
				//rouletteDto.setCtn(Base64.encodeBase64String(AES256CipherClip.AES_Encode(rouletteDto.getKeyid()).getBytes()));
				rouletteDto.setCtn(new String(AES256CipherClip.AES_Encode(rouletteDto.getKeyid()).getBytes()));
				//logger.debug("rouletteDto.getCtn()::::::"+rouletteDto.getCtn());
				//logger.debug("rouletteDto.getCtn()::::::"+new String(AES256CipherClip.AES_Decode(rouletteDto.getCtn())));
				break;
			case 2:
				//logger.debug("rouletteDto.getGa_id()::::::"+rouletteDto.getKeyid());
				//rouletteDto.setGa_id(Base64.encodeBase64String(AES256CipherClip.AES_Encode(rouletteDto.getKeyid()).getBytes()));
				rouletteDto.setGa_id(new String(AES256CipherClip.AES_Encode(rouletteDto.getKeyid()).getBytes()));
				//logger.debug("rouletteDto.getGa_id()::::::"+rouletteDto.getGa_id());
				//logger.debug("rouletteDto.getGa_id()::::::"+new String(Base64.decodeBase64(rouletteDto.getGa_id().getBytes())));
				break;
			default : 
				break;
			}
			return rouletteDto;
		}
		
		// keytype에 따라 keyid를 셋팅해준다
		@Override
		public PointRouletteJoin selectId(RouletteDto rouletteDto, PointRouletteJoin pointRouletteJoin) throws java.lang.Exception {
			switch (Integer.parseInt(rouletteDto.getKeytype())) {
			case 1:
				pointRouletteJoin.setCust_id(rouletteDto.getKeyid());
				break;
			case 3:
				//pointRouletteJoin.setCtn(Base64.encodeBase64String(AES256CipherClip.AES_Encode(rouletteDto.getKeyid()).getBytes()));
				pointRouletteJoin.setCtn(new String(AES256CipherClip.AES_Encode(rouletteDto.getKeyid()).getBytes()));
				break;
			case 2:
				//pointRouletteJoin.setGa_id(Base64.encodeBase64String(AES256CipherClip.AES_Encode(rouletteDto.getKeyid()).getBytes()));
				pointRouletteJoin.setGa_id(new String(AES256CipherClip.AES_Encode(rouletteDto.getKeyid()).getBytes()));
				break;
			default :
				break;
			}
			return pointRouletteJoin;
		}
	
	
		@Override
		public String smsAuth(String tel, String roulette_id, HttpServletRequest request) throws java.lang.Exception {
			
			// 인증번호 생성
			String smscode = String.valueOf((int) Math.floor((9999 - 1000 + 1) * Math.random() + 1000) );
			
			// 일단 DB에 저장
			PointRouletteAuth pointRouletteAuth = new PointRouletteAuth();
			pointRouletteAuth.setCtn(tel);
			pointRouletteAuth.setRoulette_id(roulette_id);
			pointRouletteAuth.setSmscode(smscode);
			String USER_IP = request.getRemoteAddr();
			if (USER_IP.equalsIgnoreCase(BaseConstant.LOCAL_IP_6)) {
			    InetAddress inetAddress;
				try {
					inetAddress = InetAddress.getLocalHost();
					String ipAddress = inetAddress.getHostAddress();
				    USER_IP = ipAddress;
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
			    
			}
			pointRouletteAuth.setIpaddr(USER_IP);
			int result = rouletteDao.smsAuthInsert(pointRouletteAuth);
			
			// 파라미터 값 생성
			String parameter = "?p=" + tel +"&strMsg="+ SMS_AUTH_MSG_1 
								+ smscode 
								+ SMS_AUTH_MSG_2;
			
			// SMS API 호출
			String strHtmlSource = "";
			if(result > 0){
				try{
					// 실제 통신이 일어나는 부분
					strHtmlSource = httpNetwork.smsSend(parameter);
					logger.debug("SMS Response : "+strHtmlSource);
				}catch (Exception e ){
					logger.debug("CLIPPOINTERROR 301:agreeForm SMS http api error.");
					e.printStackTrace();
				}
			}
				
			if("0".equals(strHtmlSource)){
				return "success";
			}else{
				return "fail";
			}
			
		}

		@Override
		public int smsAuthChk(String tel, String roulette_id, String smscode, HttpServletRequest request) throws java.lang.Exception {
			// 코드값이 맞는지 확인
			PointRouletteAuth pointRouletteAuth = new PointRouletteAuth();
			pointRouletteAuth.setCtn(tel);
			pointRouletteAuth.setRoulette_id(roulette_id);
			pointRouletteAuth.setSmscode(smscode);
			
			return rouletteDao.smsAuthChk(pointRouletteAuth);
			
		}
	
	
	
	
					
					
					
}
