package clip.roulette.bean;

public class PointBombHist {

	private String idx;
	private String user_ci;
	private String cust_id;
	private String tr_id;
	private String direct_yn;
	private String banner_id;	

	private int point;
	private int level;
	private int base_point;
	private String regdate;
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getTr_id() {
		return tr_id;
	}
	public void setTr_id(String tr_id) {
		this.tr_id = tr_id;
	}
	public String getDirect_yn() {
		return direct_yn;
	}
	public void setDirect_yn(String direct_yn) {
		this.direct_yn = direct_yn;
	}
	public String getBanner_id() {
		return banner_id;
	}
	public void setBanner_id(String banner_id) {
		this.banner_id = banner_id;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getBase_point() {
		return base_point;
	}
	public void setBase_point(int base_point) {
		this.base_point = base_point;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}

}
