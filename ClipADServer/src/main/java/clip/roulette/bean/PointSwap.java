package clip.roulette.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class PointSwap {
	
	private String user_ci;
	private String cust_id;
	private String remoteIp;
	private String result;
	private String resultMsg;
	private String pointOver;


	private String[] cardNames;
	private String[] points;
	private String[] results;
	private boolean[] pointOvers;
	
	
	
	private CardPointInfo hanaPoint;
	
	private CardPointInfo kbPoint;
	
	private CardPointInfo shinhanPoint;
	
	private CardPointInfo bcPoint;
	
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	public String getPointOver() {
		return pointOver;
	}
	public void setPointOver(String pointOver) {
		this.pointOver = pointOver;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getRemoteIp() {
		return remoteIp;
	}
	public void setRemoteIp(String remoteIp) {
		this.remoteIp = remoteIp;
	}

	public CardPointInfo getHanaPoint() {
		return hanaPoint;
	}
	public void setHanaPoint(CardPointInfo hanaPoint) {
		this.hanaPoint = hanaPoint;
	}
	public CardPointInfo getKbPoint() {
		return kbPoint;
	}
	public void setKbPoint(CardPointInfo kbPoint) {
		this.kbPoint = kbPoint;
	}
	public CardPointInfo getShinhanPoint() {
		return shinhanPoint;
	}
	public void setShinhanPoint(CardPointInfo shinhanPoint) {
		this.shinhanPoint = shinhanPoint;
	}
	public CardPointInfo getBcPoint() {
		return bcPoint;
	}
	public void setBcPoint(CardPointInfo bcPoint) {
		this.bcPoint = bcPoint;
	}
	public String[] getCardNames() {
		return cardNames;
	}
	public void setCardNames(String[] cardNames) {
		this.cardNames = cardNames;
	}
	public String[] getPoints() {
		return points;
	}
	public void setPoints(String[] points) {
		this.points = points;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public String[] getResults() {
		return results;
	}
	public void setResults(String[] results) {
		this.results = results;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}

	public boolean[] getPointOvers() {
		return pointOvers;
	}
	public void setPointOvers(boolean[] pointOvers) {
		this.pointOvers = pointOvers;
	}
}
