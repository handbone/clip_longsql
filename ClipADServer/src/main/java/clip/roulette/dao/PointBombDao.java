/*[CLiP Point] version [v3.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.dao;

import java.util.List;
import java.util.Map;

import clip.roulette.bean.BuzzAdItem;
import clip.roulette.bean.PointBombHist;
import clip.roulette.bean.PointBombInfo;

public interface PointBombDao   {

	/**
	 * 포인트 폭탄 정보 조회
	 * @param cust_id
	 * @return
	 * @throws java.lang.Exception
	 */
	public PointBombInfo selectPointBombInfo(PointBombInfo pointBombInfo) throws java.lang.Exception;
	
	/**
	 * 포인트 폭탄 정보 인서트
	 * @param starPointInfo
	 * @return
	 * @throws java.lang.Exception
	 */
	public int insertPointBombInfo(PointBombInfo pointBombInfo)  throws java.lang.Exception;
	
	/**
	 * 포인트 폭탄 정보 업데이트
	 * @param starPointInfo
	 * @return
	 * @throws java.lang.Exception
	 */
	public int updatePointBombInfo(PointBombInfo pointBombInfo) throws java.lang.Exception;

	/**
	 * 포인트 폭탄 정보 업데이트(폭탄 결과 처리시)
	 * @param starPointInfo
	 * @return
	 * @throws java.lang.Exception
	 */
	public int updatePointBombInfoForReward(PointBombInfo pointBombInfo) throws java.lang.Exception;
	
	/**
	 * 포인트 폭탄 상세 정보 조회 (일차 정보 포함)
	 * @param PointBombInfo
	 * @return
	 * @throws java.lang.Exception
	 */
	public PointBombInfo selectPointBombDetailInfo(PointBombInfo pointBombInfo) throws java.lang.Exception;
	
	/**
	 * 포인트 폭탄 회차 정보 조회
	 * @param pointBombInfo
	 * @return
	 * @throws java.lang.Exception
	 */
	public PointBombInfo selectPointBombDayInfo(PointBombInfo pointBombInfo) throws java.lang.Exception;
	
	/**
	 * 오퍼월 광고 목록 조회 : 버즈
	 * @return
	 * @throws java.lang.Exception
	 */
	public List<BuzzAdItem> selectBuzzAdItemList() throws java.lang.Exception;
	
	/**
	 * 포인트 폭탄 결과 로그 등록
	 * @param PointBombHist
	 * @return
	 * @throws java.lang.Exception
	 */
	public int insertPointBombHist(PointBombHist pointBombHist)  throws java.lang.Exception;

	/**
	 * 포인트 폭탄 트랜잭션 아이디 중복 체크
	 * @param map
	 * @return
	 * @throws java.lang.Exception
	 */
	public int selectPointBombTRID(@SuppressWarnings("rawtypes") Map map) throws java.lang.Exception;
	
}
