package clip.integration.dao;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import clip.mypoint.bean.CouponState;

@Repository
public class PointCouponDaoImpl implements PointCouponDao {
	
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private SqlSession sqlSession;

	@Override
	public void insertCouponHistory(Map<String, Object> history) {
		sqlSession.insert("PointCoupon.insertCouponHistory", history);
	}

	@Override
	public int updateCouponBlock(CouponState obj) {
		return sqlSession.update("PointCoupon.updateCouponBlock", obj);
	}

	@Override
	public CouponState selectFailCouponCheck(String user_ci) {
		return sqlSession.selectOne("PointCoupon.selectFailCouponCheck", user_ci);
	}

	@Override
	public int insertCouponBlock(String user_ci) {
		return sqlSession.insert("PointCoupon.insertCouponBlock", user_ci);
	}

}
