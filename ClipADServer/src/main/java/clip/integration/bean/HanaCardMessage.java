package clip.integration.bean;

import com.coopnc.cardpoint.domain.PointSwapInfo;

public class HanaCardMessage extends PointSwapInfo {
	private boolean isPointOver = false;
	
	private boolean isCountOver = false;

	public boolean isPointOver() {
		return isPointOver;
	}

	public void setPointOver(boolean isPointOver) {
		this.isPointOver = isPointOver;
	}

	public boolean isCountOver() {
		return isCountOver;
	}

	public void setCountOver(boolean isCountOver) {
		this.isCountOver = isCountOver;
	}
}
