package clip.integration.service;

import java.util.List;

import clip.integration.bean.ClipPointMessage;

public interface ClipPointClientService {

	//포인트 조회
	public ClipPointMessage getPoint(ClipPointMessage param);
	
	//포인트 적립
	public ClipPointMessage plusPoint(ClipPointMessage param) throws Exception;
	
	//포인트 차감
	public ClipPointMessage minusPoint(ClipPointMessage param);
	
	//포인트 내역
	public List<ClipPointMessage> getPointHistoryList(ClipPointMessage param);
	
	//포인트 적립 취소
	public ClipPointMessage cancelPlusPoint(ClipPointMessage param);
	
	//포인트 차감 취소
	public ClipPointMessage cancelMinusPoint(ClipPointMessage param);
	
}