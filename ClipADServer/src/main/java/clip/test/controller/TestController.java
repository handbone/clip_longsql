package clip.test.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/test")
public class TestController {
	Logger log = Logger.getLogger(this.getClass());

	/** 포인트 모으기 메인화면 **/
	@RequestMapping(value="/clippointMain.do")
	public ModelAndView clippointMain ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointMain");	
		
		
		
		return mv;
	}
	/** 포인트 모으기 포인트전환 **/
	@RequestMapping(value="/clippointTransform.do")
	public ModelAndView clippointTransform ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointTransform");	
		
		
		
		return mv;
	}
	/** 포인트 모으기 포인트전환완료 **/
	@RequestMapping(value="/clippointTransformConfirm.do")
	public ModelAndView clippointTransformConfirm ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointTransformConfirm");	
		
		
		
		return mv;
	}
	/** 포인트 모으기 구매적립 **/
	@RequestMapping(value="/buyEarn.do")
	public ModelAndView buyEarn ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/buyEarn");	
		
		
		
		return mv;
	}
	/** 포인트 모으기 구매적립내역 **/
	@RequestMapping(value="/buyEarnList.do")
	public ModelAndView buyEarnList ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/buyEarnList");	
		
		
		
		return mv;
	}
	/** 포인트 모으기 구매적립내역 - 없음 **/
	@RequestMapping(value="/buyEarnNolist.do")
	public ModelAndView buyEarnNolist ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/buyEarnNolist");	
		
		
		
		return mv;
	}
	/** 포인트 모으기 구매적립 이용내역 **/
	/*@RequestMapping(value="/buyEarnUseList.do")
	public ModelAndView buyEarnUseList ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/buyEarnUseList");	
		
		
		
		return mv;
	}*/
	/** 포인트 모으기 포인트 쿠폰등록 **/
	@RequestMapping(value="pointCoupon.do")
	public ModelAndView pointCoupon ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/pointCoupon");	
		
		
		
		return mv;
	}
	/** 포인트 모으기 포인트 쿠폰등록 완료 **/
	@RequestMapping(value="/pointCouponComplete.do")
	public ModelAndView pointCouponComplete ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/pointCouponComplete");	
		
		return mv;
	}
	/** 포인트 쓰기 오프라인 간편 결제 **/
	@RequestMapping(value="/offlineEasyPay.do")
	public ModelAndView offlineEasyPay ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/offlineEasyPay");	
		
		
		
		return mv;
	}
	/** 포인트 쓰기 브랜드별 상세 **/
	@RequestMapping(value="/offlineEasyPayDetail.do")
	public ModelAndView offlineEasyPayDetail ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/offlineEasyPayDetail");	
		
		
		
		return mv;
	}
	/** 포인트 쓰기 오프라인 간편 결제 바코트 **/
	/*@RequestMapping(value="/clippointMain.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointMain");	
		
		
		
		return mv;
	}*/
	/** 포인트 이용내역 있음 **/
	/*@RequestMapping(value="/clippointMain.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointMain");	
		
		
		
		return mv;
	}*/
	/** 포인트 이용내역 없음 **/
	/*@RequestMapping(value="/clippointMain.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointMain");	
		
		
		
		return mv;
	}*/
	/** 포인트 포인트 폭탄 터트리기 **/
	/*@RequestMapping(value="/clippointMain.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointMain");	
		
		
		
		return mv;
	}*/
	/** 포인트 폭탄 결과 **/
	/*@RequestMapping(value="/clippointMain.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointMain");	
		
		
		
		return mv;
	}*/
	/** 포인트 폭탄 광고목록 **/
	/*@RequestMapping(value="/clippointMain.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointMain");	
		
		
		
		return mv;
	}*/
	/** 기존룰렛 돌리기 **/
	/*@RequestMapping(value="/clippointMain.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointMain");	
		
		
		
		return mv;
	}*/
	/** 기존룰렛 결과 (레이어) **/
	/*@RequestMapping(value="/clippointMain.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/test/clippointMain");	
		
		
		
		return mv;
	}*/

}
