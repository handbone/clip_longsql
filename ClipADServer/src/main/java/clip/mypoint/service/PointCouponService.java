package clip.mypoint.service;

import javax.servlet.http.HttpServletRequest;

import clip.mypoint.bean.CouponState;
import clip.mypoint.bean.PointCouponBean;

public interface PointCouponService {
	
	public PointCouponBean checkCoupon(PointCouponBean param, HttpServletRequest request) throws Exception;
	
	public CouponState checkFailCouponCheck(String user_ci) throws Exception;

	public void changeCouponBlock(CouponState obj) throws Exception;

	public void putCouponUserInfo(String user_ci) throws Exception;
	
}
