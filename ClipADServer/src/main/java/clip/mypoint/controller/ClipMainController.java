package clip.mypoint.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import clip.common.crypto.AES256CipherClip_Ext;
import clip.framework.BaseController;
import clip.integration.bean.ClipContract2Message;
import clip.integration.service.ClipClientService;
import clip.mypoint.bean.MainbannerInfoBean;
import clip.mypoint.bean.MidasBean;
import clip.mypoint.bean.MyPointBean;
import clip.mypoint.service.MyPointService;
	
@Controller
public class ClipMainController extends BaseController {
	Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name="myPointService")
	private MyPointService myPointService;
	
	@Resource(name="clipClientService")
	private ClipClientService clipClientService;
	
	
	/** 포인트 메인 화면 **/
	@RequestMapping(value="/pointmain/main.do")
	public ModelAndView clipMain(HttpServletRequest request, Model model) throws java.lang.Exception {
		
		String cust_id = (String) request.getParameter("cust_id");
		String ctn = (String) request.getParameter("ctn");
		String gaid = (String) request.getParameter("gaid");
		@SuppressWarnings("unused")
		String offerwall = (String) request.getParameter("offerwall");
		
		String user_ci = (String) request.getParameter("user_ci");	
		//user token 추가
		String user_token = (String) request.getParameter("user_token");
		
		ModelAndView mv = new ModelAndView();

		handOverUserKey(request, mv);
		
		String pageType = (String) request.getParameter("pageType");
		if(StringUtils.isNotEmpty(pageType)) {
			mv.addObject("pageType", pageType);
		}
		
		// 2017.10.31 jyi
		MyPointBean myPointBean = new MyPointBean();
		MyPointBean result = new MyPointBean();
		List<MainbannerInfoBean> bannerList = null;
		
		myPointBean.setUser_ci(user_ci);
		myPointBean.setCtn(ctn);
		myPointBean.setCust_id(cust_id);
		myPointBean.setGaid(gaid);
		
		try {
			/** TODO User_ci 조회 */
			if(user_ci == null || user_ci.equals("")) {
				result = myPointService.getUserCi(myPointBean);
				user_ci = result.getUser_ci();
				mv.addObject("user_ci", user_ci.replaceAll("[\r\n]", ""));
			
			}
			
			/**  user_ci 가 존재 할 경우 user_token 조회  */
			if(user_ci != null  && StringUtils.isNotEmpty(user_ci)  ) {
				result.setUser_token(myPointService.getUserToken(user_ci));
				user_token = result.getUser_token();
				mv.addObject("user_token", user_token.replaceAll("[\r\n]", ""));
			}
			
			/** TODO 포인트 조회 통합 */
			if(!(cust_id == null || cust_id.equals(""))) {
				result = myPointService.getPoint(myPointBean);
				log.info("result : "+ result.toString());
				
			}
		}catch(Exception e) {
			result.setResResult("E");
			result.setBalance("-1");
		}
		
		try {
			/** TODO mainbanner 조회 */
			String userAgent = request.getHeader("User-Agent");
	//		bannerList = myPointService.getMainbannerInfo();
			if(userAgent.contains("iPhone") || userAgent.contains("ipod") || userAgent.contains("ipad")) {
				bannerList = myPointService.getMainbannerInfoIOS();
	//			mv.addObject("device", 'I');
			} else {
				bannerList = myPointService.getMainbannerInfoAndroid();
	//			mv.addObject("device", 'A');
			}
			//CLIP2.0 url -> CLIP3.0 url formating
			for(MainbannerInfoBean banner : bannerList) {
				String url = banner.getLink_url().replace("http://clippointad.clipwallet.co.kr", "");
				banner.setLink_url(url);
			}
		}catch(Exception e) {
			log.debug("3.0MAIN BANNER SELECT ERROR");
		}
		
		mv.setViewName("/clip/pointmain/main");
		mv.addObject("pointBean", result);
		mv.addObject("bannerList", bannerList);
		
		return mv;
	}
	
	/*
	포인트메인 모으기탭 : KTolleh00114://point/menu/?spec=gather
	
	포인트모으기(세부메뉴) :
	
	카드 포인트 가져오기 : KTolleh00114://point/menu/?spec=swap
	참여 적립 : KTolleh00114://point/menu/?spec=offerwall
	쇼핑 적립 : KTolleh00114://point/menu/?spec=shopbuy
	금융 혜택 적립 : KTolleh00114://point/menu/?spec=fevent
	포인트 쿠폰 등록 : KTolleh00114://point/menu/?spec=pcoupon&no={쿠폰번호}
	
	포인트메인 사용하기탭 : KTolleh00114://point/menu/?spec=use
	
	포인트사용하기(세부메뉴)
	
	모바일 쿠폰 구매 : KTolleh00114://point/menu/?spec=giftshow
	오프라인 간편 결제 : KTolleh00114://point/menu/?spec=pointpay
	VIP Zone : KTolleh00114://point/menu/?spec=viplounge
	포인트메인 이용내역탭 : KTolleh00114://point/menu/?spec=history
	*/
	
	/**
	 * clip 메뉴별 직링크
	 * @param request
	 * @param response
	 * @return
	 * @throws java.lang.Exception
	 */
	@RequestMapping(value="/directLink.do")
	public String directLink(RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {

		@SuppressWarnings("unchecked")
		Enumeration<String> en = request.getParameterNames();
		
		String spec = (String) request.getParameter("spec");
		String no = (String) request.getParameter("no");
		
		if(StringUtils.isNotEmpty(no)) 
			no = AES256CipherClip_Ext.AES_Decode(no);
		
		log.debug("spec="+spec+"&no="+no);
		
		StringBuffer buf = new StringBuffer();
		boolean isFirst = true;
        for ( ; en.hasMoreElements() ; )
        {
        	String name = en.nextElement();
        	
        	if(name.equals("spec")) {
        		redirectAttributes.addFlashAttribute(name, (String)request.getParameter(name));
        		continue;
        	}
        	
        	if(name.equals("no") && StringUtils.isNotEmpty(no)) {
        		redirectAttributes.addFlashAttribute(name, no);
        		continue;
        	}
        	
        	String encoded = java.net.URLEncoder.encode((String)request.getParameter(name), "UTF-8");
        	
        	if(isFirst) {
        		buf.append("?"+name+"="+encoded);
        		isFirst = false;
        	} else {
        		buf.append("&"+name+"="+encoded);
        	}
        }
        	
        log.debug("parameter :: " + buf.toString());
        
        return "redirect:/pointmain/main.do"+buf.toString();
	}
	
	@RequestMapping(value = "/contractCheck.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ModelAndView contractCheck(@RequestBody ClipContract2Message param,
			HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		mav.addObject("result", "S");
		
		mav.addObject("contractInfo", clipClientService.getContract2(param));  // lee
//		ClipContract2Message obj = new ClipContract2Message();
//		obj.setResult("N");
//		mav.addObject("contractInfo", obj);

		return mav;
	}
	
	
	/** 포인트 메인 화면 **/
	@RequestMapping(value="/pointmain/mainTest.do")
	public ModelAndView clipMainTest(HttpServletRequest request, Model model, HttpSession session) throws java.lang.Exception {
		
		String cust_id = (String) request.getParameter("cust_id");
		String ctn = (String) request.getParameter("ctn");
		String gaid = (String) request.getParameter("gaid");
		@SuppressWarnings("unused")
		String offerwall = (String) request.getParameter("offerwall");
		
		String user_ci = (String) request.getParameter("user_ci");	
		//user token 추가
		String user_token = (String) request.getParameter("user_token");
		
		ModelAndView mv = new ModelAndView();

		handOverUserKey(request, mv);	
		mv.addObject("debug", true);
		
		String pageType = (String) request.getParameter("pageType");
		if(StringUtils.isNotEmpty(pageType)) {
			mv.addObject("pageType", pageType);
		}
		
		// 2017.10.31 jyi
		MyPointBean myPointBean = new MyPointBean();
		MyPointBean result = new MyPointBean();
		List<MainbannerInfoBean> bannerList = null;
		
		myPointBean.setUser_ci(user_ci);
		myPointBean.setCtn(ctn);
		myPointBean.setCust_id(cust_id);
		myPointBean.setGaid(gaid);
		
		try {
			/** TODO User_ci 조회 */
			if(user_ci == null || user_ci.equals("")) {
				result = myPointService.getUserCi(myPointBean);
				user_ci = result.getUser_ci();
				mv.addObject("user_ci", user_ci.replaceAll("[\r\n]", ""));
			
			}
			
			/**  user_ci 가 존재 할 경우 user_token 조회  */
			if(user_ci != null  && StringUtils.isNotEmpty(user_ci)  ) {
				result.setUser_token(myPointService.getUserToken(user_ci));
				user_token = result.getUser_token();
				mv.addObject("user_token", user_token.replaceAll("[\r\n]", ""));
			}
			
			/** TODO 포인트 조회 통합 */
			if(!(cust_id == null || cust_id.equals(""))) {
				result = myPointService.getPoint(myPointBean);
				log.info("result : "+ result.toString());
				
			}
		}catch(Exception e) {
			result.setResResult("E");
			result.setBalance("-1");
		}
	
		try {
			/** TODO mainbanner 조회 */
			String userAgent = request.getHeader("User-Agent");
	//		bannerList = myPointService.getMainbannerInfo();
			if(userAgent.contains("iPhone") || userAgent.contains("ipod") || userAgent.contains("ipad")) {
				bannerList = myPointService.getMainbannerInfoIOS();
	//			mv.addObject("device", 'I');
			} else {
				bannerList = myPointService.getMainbannerInfoAndroid();
	//			mv.addObject("device", 'A');
			}
			//CLIP2.0 url -> CLIP3.0 url formating
			for(MainbannerInfoBean banner : bannerList) {
				String url = banner.getLink_url().replace("http://clippointad.clipwallet.co.kr", "");
				banner.setLink_url(url);
			}
		}catch(Exception e) {
			log.debug("3.0MAIN BANNER SELECT ERROR");
		}
		
		mv.setViewName("/clip/pointmain/main_debug");
		mv.addObject("pointBean", result);
		mv.addObject("bannerList", bannerList);
		
		return mv;
	}
	
	/**
	 * clip 메뉴별 직링크 테스트 
	 * @param request
	 * @param response
	 * @return
	 * @throws java.lang.Exception
	 */
	@RequestMapping(value="/pointmain/directLinkTest.do")
	public String directLinkTest(RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws java.lang.Exception {

		@SuppressWarnings("unchecked")
		Enumeration<String> en = request.getParameterNames();
		
		String spec = (String) request.getParameter("spec");
		String no = (String) request.getParameter("no");
		
		if(StringUtils.isNotEmpty(no)) 
			no = AES256CipherClip_Ext.AES_Decode(no);		
		log.debug("spec="+spec+"&no="+no);
		
		StringBuffer buf = new StringBuffer();
		boolean isFirst = true;
        for ( ; en.hasMoreElements() ; )
        {
	        	String name = en.nextElement();
	        	
	        	if(name.equals("spec")) {
	        		redirectAttributes.addFlashAttribute(name, (String)request.getParameter(name));
	        		continue;
	        	}
	        	
	        	if(name.equals("no") && StringUtils.isNotEmpty(no)) {
	        		redirectAttributes.addFlashAttribute(name, no);
	        		continue;
	        	}
	        	
	        	String encoded = java.net.URLEncoder.encode((String)request.getParameter(name), "UTF-8");
	        	
	        	if(isFirst) {
	        		buf.append("?"+name+"="+encoded);
	        		isFirst = false;
	        	} else {
	        		buf.append("&"+name+"="+encoded);
	        	}
        }
        	
        log.debug("parameter :: " + buf.toString());
        
		return "redirect:/pointmain/mainTest.do"+buf.toString();
	}
	
	@RequestMapping(value="/pointmain/sendMidas.do")
	public @ResponseBody Map<String, Object> sendingMidas(MidasBean param, HttpServletRequest request) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
		/** TODO 마이더스 통계 데이터 전송 */
		log.debug("SENDING_MIDAS START");
		log.debug("MIDAS_PARAM :: " + param.toString());
		
		int resultCode = myPointService.sendMidasLog(param, request);
		map.put("resultCode", resultCode);
		
		return map;
	}
}
