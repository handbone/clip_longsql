<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/pointCommon.js"></script>

<!-- bx slider lib -->
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script>


<!-- to-be -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>

<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">

<script type="text/javascript">
$(function() {
	 
	common.invoker.invoke("myPoint"); 
	
	$(document).ready(function(){
		
		
		
	});
	
});		
</script>
</head>
<body>
<form id="mainForm" name="mainForm" action="">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden"  id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden"  id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden"  id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden"  id="mid" name="mid" value="${mid}"/>
	<input type="hidden"  id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden"  id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden"  id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden"  id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
</form>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">오프라인 간편 결제</h1>
		<a href="#" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="section">
			<h2 class="clippoint_top_title">
				오프라인 매장에서도 포인트로<br>
				알뜰하게 구매하세요!
			</h2>
			<!-- CLIP 포인트 -->
			<a href="#" class="clippoint_box">
				<span class="title">CLIP 포인트</span>
				<strong class="point usepointValue"><span id="mypoint">0</span><span>P</span></strong>
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<div class="section barcode_off">
			<div class="ad_area">
				<a href=""><img src="../images/temp/@ad_img2.png" alt=""></a>
			</div>
			<div class="section_inner">
				<a href="#" class="btn_point_pay">CLIP 포인트 결제 하기</a>
			</div>
		</div>
		<div class="section barcode_on">
			<div class="barcode_area">
				<img src="../images/temp/@barcode.png" alt="">
			</div>
		</div>
		<div class="section">
			<ul class="info_txt">
				<li>
					<strong>[이용안내]</strong>
					<span>-전국 이디야매장에서 사용가능</span>
					<span>-현금영수증 발행가능</span>
				</li>
				<li>
					<strong>[유의사항]</strong>
					<span>-주의해주세요</span>
				</li>
				<li>
					<strong>[고객센터번호]</strong>
					<span>02-9999-9999</span>
				</li>
				<li>
					<strong>[구매불가매장 및 아이템]</strong>
					<span>-이수점,상도점 사용불가</span>
				</li>
				<li>
					<strong>[환불정책]</strong>
					<span>-환불은 구매처에서 가능합니다.</span>
					<span>-문제시 고객센터로 문의 주세요.</span>
				</li>
				<li>
					<strong>[법적고지]</strong>
					<span>-사용상 문제시 이디야에서 처리 가능합니다.</span>
				</li>
			</ul>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->
</body>
</html>