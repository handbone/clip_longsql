<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" session="true"%>

<div class="layer_pop_wrap pop_default" id="alert_popup" style="display: none;">
	<div class="deem"></div>
	<div class="layer_pop">
		<div class="lp_hd">
			<strong class="title">알림</strong>
		</div>
		<div class="lp_ct">
			<!-- point_tf_cont -->
			<div class="point_tf_cont">
				<div class="inr ex_txt" id="notice_word">
					
				</div>
			</div>
			<!-- // point_tf_cont -->
			<div class="btn_multi_box">
				<button type="button" class="btn_lp_cancel" id="alert_type" onclick="javascript:layerAct.close('layer_pop_wrap');">취소</button>
				<button type="submit" class="btn_lp_confirm" onclick="javascript:popup_confirm_Act()">확인</button>
				<input type="hidden" name="popup_type" id="popup_type">
			</div>
		</div>
	</div>
</div>
