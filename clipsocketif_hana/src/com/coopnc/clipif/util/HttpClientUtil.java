
package com.coopnc.clipif.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.codehaus.jackson.map.ObjectMapper;

public class HttpClientUtil {
	
	private static Logger logger = LoggerFactory.getLogger(HttpClientUtil.class);
	
	private static final String USER_AGENT = "CLiP_SOCK_IF";
	private static final String MULTIPART_BOUNDARY = "--77e2ef111322-0-89-1-56d23f21e8ead-";
	private static final String CHARSET_NAME = "UTF-8";
	
	private InputStream is = null;
	
	private String uri;
	private HttpResponse response;

	private Map<String, Object> body;
	
	private List<NameValuePair> fileFullPaths;
	
	private List<NameValuePair> headers;
	private List<NameValuePair> parameters;

	private String httpMethod;
	
	private HttpClient client;
	private int sockTimeout = 1000;
	private int connReqTimeout = 1000;
	
	private RequestConfig requestConfig(int timeout){
		return RequestConfig.custom().setConnectionRequestTimeout(connReqTimeout)
				.setConnectTimeout(timeout)
				.setSocketTimeout(sockTimeout)
				.build();
	}

	public HttpClientUtil(String httpMethod, String uri, int timeout) throws Exception {
		this.uri = uri;
		
		logger.debug("this.uri : "+this.uri);
		
		this.httpMethod = httpMethod;
		this.connReqTimeout = timeout;
		
		if(uri.startsWith("https"))
			setHttps(); 
		else
			this.client = HttpClientBuilder.create().build();
	}
	
	private void setHttps() throws Exception{
		HttpClientBuilder b = HttpClientBuilder.create();
		 
	    // setup a Trust Strategy that allows all certificates.
	    //
	    SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
	        public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
	            return true;
	        }
	    }).build();
	    b.setSslcontext( sslContext);
	 
	    // don't check Hostnames, either.
	    //      -- use SSLConnectionSocketFactory.getDefaultHostnameVerifier(), if you don't want to weaken
	    @SuppressWarnings("deprecation")
		HostnameVerifier hostnameVerifier = SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
	 
	    // here's the special part:
	    //      -- need to create an SSL Socket Factory, to use our weakened "trust strategy";
	    //      -- and create a Registry, to register it.
	    //
	    SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, hostnameVerifier);
	    Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
	            .register("http", PlainConnectionSocketFactory.getSocketFactory())
	            .register("https", sslSocketFactory)
	            .build();
	 
	    // now, we create connection-manager using our Registry.
	    //      -- allows multi-threaded use
	    PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager( socketFactoryRegistry);
	    b.setConnectionManager( connMgr);
	    
	    client = b.build();
	}

	public void addHeader(String name, String value){
		if(headers == null)
			headers = new ArrayList<NameValuePair>();
		
		headers.add(new BasicNameValuePair(name, value));
	}
	
	public void addParameter(String name, String value) {
		if(parameters == null)
			parameters = new ArrayList<NameValuePair>();
		
		parameters.add(new BasicNameValuePair(name, value));
	}
	
	public void addBody(String bodyName, Object body){
		if(this.body == null)
			this.body = new HashMap<String, Object>();
		
		this.body.put(bodyName, body);
	}
	
	public void addFile(String partName, String fileFullPath){
		if(fileFullPaths == null)
			fileFullPaths = new ArrayList<NameValuePair>();
		
		fileFullPaths.add(new BasicNameValuePair(partName, fileFullPath));
	}

	public int sendRequest() throws Exception {
		int responseCode = -1;

		try {
			
			if("POST".equalsIgnoreCase(httpMethod)) {
				responseCode = sendPost("params");
			} else if("PUT".equalsIgnoreCase(httpMethod)) {
				responseCode = sendPut("params");
			} else if("GET".equalsIgnoreCase(httpMethod)) {
				responseCode = sendGet();
			} else if("DELETE".equalsIgnoreCase(httpMethod)) {
				responseCode = sendDelete();
			} else {
				throw new Exception("지원하지 않는 HTTP 메쏘드.");
			}
			
        	if (responseCode == HttpServletResponse.SC_OK) {
        		
        	} else {
        		//throw new Exception("네트워크 오류입니다.");
        	}
        	
        } catch (Exception e) {
        	closeConnection();
        	throw e;
        } finally {

        }
        
        return responseCode;
	}
	
	public int sendXml() throws Exception {
		int responseCode = -1;
        
		try {
			addHeader("Content-type", "text/xml; charset="+CHARSET_NAME.toLowerCase());
			if("POST".equalsIgnoreCase(httpMethod)) {
				responseCode = sendPost("xml");
			} else if("PUT".equalsIgnoreCase(httpMethod)) {
				responseCode = sendPut("xml");
			} else {
				throw new Exception("지원하지 않는 HTTP 메쏘드.");
			}
			
        	if (responseCode == HttpServletResponse.SC_OK) {
        		
        	} else {
        		//throw new Exception("네트워크 오류입니다.");
        	}
        	
        } catch (Exception e) {
        	closeConnection();
        	throw e;
        } finally {
        }
        
        return responseCode;
	}
	
	public int sendJson() throws Exception {
		int responseCode = -1;
        
		try {
			addHeader("Content-type", "application/json; charset="+CHARSET_NAME.toLowerCase());
			if("POST".equalsIgnoreCase(httpMethod)) {
				responseCode = sendPost("json");
			} else if("PUT".equalsIgnoreCase(httpMethod)) {
				responseCode = sendPut("json");
			} else {
				throw new Exception("지원하지 않는 HTTP 메쏘드.");
			}
			
        	if (responseCode == HttpServletResponse.SC_OK) {
        		
        	} else {
        		//throw new Exception("네트워크 오류입니다.");
        	}
        	
        } catch (Exception e) {
        	closeConnection();
        	throw e;
        } finally {
        }
        
        return responseCode;
	}
	
	/**
	 * 
	 * @param header
	 * @param parameter
	 * @param body
	 * @return
	 * @throws Exception
	 */
	public int sendFile() throws Exception {
		int responseCode = -1;
        
		try {
			addHeader("Content-type", "multipart/form-data;boundary="+MULTIPART_BOUNDARY);
			responseCode = sendMultiPart();
			
        	if (responseCode == HttpServletResponse.SC_OK) {
        		
        	} else {
        		//throw new Exception("네트워크 오류입니다.");
        	}
        	
        } catch (Exception e) {
        	closeConnection();
        	throw e;
        } finally {
        }
        
        return responseCode;
	}

	public void closeConnection(){
		if(response != null) {
			try {
				EntityUtils.consume(response.getEntity());
			} catch (Exception e) {}
		}
	}
	
	public String getResponseString() throws Exception {
		return this.getResponseString(CHARSET_NAME);
	}
	
	public String getResponseString(String charset) throws Exception {
		return EntityUtils.toString(response.getEntity(), charset);
	}
	
	public <T> T getResponseXml(T clazz) throws Exception {
		return getResponseXml(clazz, CHARSET_NAME);
	}
	
	public <T> T getResponseXml(T clazz, String charset) throws Exception {
		is = response.getEntity().getContent();
		XmlConverter xml = new XmlConverter();
		return xml.readXml(is, clazz, charset);
	}

	public <T> T getResponseJson(Class<T> clazz) throws Exception {
		is = response.getEntity().getContent();
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(is, clazz);
	}
	
	public List<String> getHeaders(String headerName) {
		
		if(!response.containsHeader(headerName))
			return null;
		
		Header[] headers = response.getHeaders(headerName);
		List<String> temp = new ArrayList<String>();
		for(Header header : headers) {
			temp.add(header.getValue());
		}
		
		return temp;
	}
	
	public String getHeader(String headerName) {
		
		if(!response.containsHeader(headerName))
			return null;
		
		Header[] headers = response.getHeaders(headerName);

		return headers[0].getValue();
	}
	
	public void getResponseBinary(OutputStream os) throws Exception {
		try {
			is = response.getEntity().getContent();
			IOUtils.copy(is, os);
		} catch (Exception e) {
			closeConnection();
        	throw e;
        } 
	}
	
	public void getResponseLargeBinary(OutputStream os) throws Exception {
		try {
			is = response.getEntity().getContent();
			IOUtils.copyLarge(is, os);
		} catch (Exception e) {
			closeConnection();
        	throw e;
        } 
	}
	
	public int getBytes(byte[] byteBuffer) throws Exception {
		ByteArrayOutputStream baos = null;
		
		int nLength = 0;
		
		try {
			is = response.getEntity().getContent();
			baos = new ByteArrayOutputStream();
	        
	        nLength = is.read(byteBuffer);
	        if (nLength > 0) {
	        	baos.write(byteBuffer, 0, nLength);
	        }

		} catch (Exception e) {
			closeConnection();
        	throw e;
        } finally {
        	try {
	        	if (nLength == 0) {
	        		if(is != null) {
	        			is.close();
		        	}
	        	}
	        	if(baos != null) {
        			baos.close();
	        	}
        	} catch (Exception e) {
        		closeConnection();
        		throw e ;
        	}
        }
        
        return nLength;
	}
	
	private int sendGet() throws ClientProtocolException, IOException, URISyntaxException{
		HttpGet request = new HttpGet();
		request.setConfig(requestConfig(2000));
	 
		//add defualt header
		request.addHeader("User-Agent", USER_AGENT);
		
		//add custom header
		if(headers != null && headers.size() > 0)
			for (NameValuePair header : headers) {
				request.addHeader(header.getName(), header.getValue());
			}
		
		
		URIBuilder ub = new URIBuilder(uri);
		//add params to uri
		if(parameters != null && parameters.size() > 0) {
			ub.addParameters(parameters);
		}

		URI uri = ub.build();
		
		logger.debug("final url : "+uri.toString());
		request.setURI(uri);

		response = client.execute(request);

		return response.getStatusLine().getStatusCode();
	}
	
	
	private int sendDelete() throws ClientProtocolException, IOException, URISyntaxException{
		HttpDelete request = new HttpDelete();
		request.setConfig(requestConfig(2000));
	 
		//add defualt header
		request.addHeader("User-Agent", USER_AGENT);
		
		//add custom header
		if(headers != null && headers.size() > 0)
			for (NameValuePair header : headers) {
				request.addHeader(header.getName(), header.getValue());
			}
		
		
		URIBuilder ub = new URIBuilder(uri);
		//add params to uri
		if(parameters != null && parameters.size() > 0) {
			ub.addParameters(parameters);
		}

		URI uri = ub.build();
		request.setURI(uri);

		response = client.execute(request);

		return response.getStatusLine().getStatusCode();
	}
	
	
	private int sendPost(String sendContent) throws Exception{
		HttpPost post = new HttpPost(uri);
		post.setConfig(requestConfig(2000));
	 
		//add defualt header
		post.setHeader("User-Agent", USER_AGENT);
		
		//add custom header
		if(headers != null && headers.size() > 0)
			for (NameValuePair header : headers) {
				post.addHeader(header.getName(), header.getValue());
			}
		
		if(parameters != null && parameters.size() > 0) {
			
			if("json".equals(sendContent)) {
				ObjectMapper om = new ObjectMapper();
				
				Map<String, Object> paramMap = new HashMap<String, Object>();
				
				for (NameValuePair params : parameters) {
					paramMap.put(params.getName(), params.getValue());
				}

				String jsonData = om.writeValueAsString(paramMap);
				StringEntity entity = new StringEntity(jsonData, CHARSET_NAME);
				
				post.setEntity(entity);
			} else if("params".equals(sendContent)){
				post.setEntity(new UrlEncodedFormEntity(parameters, CHARSET_NAME));
			}

		} else if (body != null) {
			
			Set<String> keySet = body.keySet();
			
			for (String key : keySet) {
				Object obj = body.get(key);
				if(obj instanceof String) {
					StringEntity entity = new StringEntity((String)obj, CHARSET_NAME);
					post.setEntity(entity);
				} else {
					String xml = new XmlConverter().writeXml(obj,  CHARSET_NAME);
					
					StringEntity entity = new StringEntity(xml, CHARSET_NAME); 
					post.setEntity(entity);
				}
				
				break;
			}
		}
			
		response = client.execute(post);
		
		return response.getStatusLine().getStatusCode();
	}

	private int sendPut(String sendContent) throws Exception{
		HttpPut post = new HttpPut(uri);
		post.setConfig(requestConfig(2000));
		
		//add defualt header
		post.setHeader("User-Agent", USER_AGENT);
		
		//add custom header
		if(headers != null && headers.size() > 0)
			for (NameValuePair header : headers) {
				post.addHeader(header.getName(), header.getValue());
			}
		
		if(parameters != null && parameters.size() > 0) {
			
			if("json".equals(sendContent)) {
				ObjectMapper om = new ObjectMapper();
				
				Map<String, Object> paramMap = new HashMap<String, Object>();
				
				for (NameValuePair params : parameters) {
					paramMap.put(params.getName(), params.getValue());
				}

				String jsonData = om.writeValueAsString(paramMap);
				StringEntity entity = new StringEntity(jsonData, CHARSET_NAME);
				
				post.setEntity(entity);
			} else if("params".equals(sendContent)){
				post.setEntity(new UrlEncodedFormEntity(parameters, CHARSET_NAME));
			}
			
		} else if (body != null) {
			Set<String> keySet = body.keySet();
			
			for (String key : keySet) {
				Object obj = body.get(key);
				if(obj instanceof String) {
					StringEntity entity = new StringEntity((String)obj, CHARSET_NAME);
					post.setEntity(entity);
				} else {
					String xml = new XmlConverter().writeXml(obj,  CHARSET_NAME);
					
					StringEntity entity = new StringEntity(xml, CHARSET_NAME); 
					post.setEntity(entity);
				}
				
				break;
			}
		}
			
		response = client.execute(post);
		return response.getStatusLine().getStatusCode();
	}
	
	private int sendMultiPart() throws ClientProtocolException, IOException{
		HttpPost post = new HttpPost(uri);
		post.setConfig(requestConfig(2000));
		
		// add header
		post.setHeader("User-Agent", USER_AGENT);

		//add custom header
		if(headers != null && headers.size() > 0)
			for (NameValuePair header : headers) {
				post.addHeader(header.getName(), header.getValue());
			}
		
		//MultipartEntityBuilder를 다음과 같이 선언
		MultipartEntityBuilder meb = MultipartEntityBuilder.create();

		//Builder 설정하기.
		//선언할때 넣는게 아니라 선언 후 메소드로 설정한다.
		meb.setBoundary(MULTIPART_BOUNDARY);
		meb.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		meb.setCharset(Charset.forName(CHARSET_NAME));

		if(parameters != null && parameters.size() > 0) {
			//문자열을 보내려면 addPart와 StringBody가 아닌 addTextBody를 사용한다.
			for (NameValuePair param : parameters) {
				meb.addTextBody(param.getName(), param.getValue());
			}
		}
		
		if(body != null) {
			//문자열을 보내려면 addPart와 StringBody가 아닌 addTextBody를 사용한다.
			Set<String> keySet = body.keySet();
			
			for (String key : keySet) {
				Object obj = body.get(key);
				if(obj instanceof String) {
					meb.addBinaryBody(key, ((String)obj).getBytes());
				}
			}
		}

		if(fileFullPaths != null) {			
			for (NameValuePair param : fileFullPaths) {
				meb.addPart(param.getName(), new FileBody(new File(param.getValue()))); 
			}	
		}

		//HttpEntity를 빌드하고 HttpPost 객체에 삽입한다.
		HttpEntity entity = meb.build();

		post.setEntity(entity);

		response = client.execute(post);

		return response.getStatusLine().getStatusCode();
	}

}
