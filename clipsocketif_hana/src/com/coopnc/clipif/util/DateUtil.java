package com.coopnc.clipif.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	/**
	 * <pre>
	 * Calendar 인스턴스를 넘긴다. 
	 * </pre>
	 * @return Calendar
	 */
	public static Calendar getCalendar() {
		return Calendar.getInstance();
	}

	/**
	 * <pre>
	 * Calendar객체를 반환한다.
	 * 입력된 String타입 날짜를 원하는타입으로 Calendar를 반환한다.
	 * String>Calendar
	 * ex)
	 *  포맷 예제
	 * "yyyy.MM.dd G 'at' HH:mm:ss z" => 2001.07.04 AD at 12:08:56 PDT  
	 *	"EEE, MMM d, ''yy"  => Wed, Jul 4, '01  
	 *	"h:mm a"   => 12:08 PM  
	 *	"hh 'o''clock' a, zzzz"   => 12 o'clock PM, Pacific Daylight Time  
	 * "K:mm a, z"  => 0:08 PM, PDT  
	 *	"yyyyy.MMMMM.dd GGG hh:mm aaa"   => 02001.July.04 AD 12:08 PM  
	 *	"EEE, d MMM yyyy HH:mm:ss Z"  =>  Wed, 4 Jul 2001 12:08:56 -0700  
	 *	"yyMMddHHmmssZ"  => 010704120856-0700  
	 * </pre>
	 * @param day 입력한 날짜 문자열
	 * @param format 변결할 포맷
	 * @return 결과 Calendar
	 */
	public static Calendar getCalendar(String day, String format) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(getDate(day, format));
		return cal;
	}

	/**
	 * <pre>
	 * 유효한 날짜 형식인지 체크하고 입력 된 날짜 형식으로 변환함.
	 * String>Date
	 * ex)
	 * 포맷 예제
	 * "yyyy.MM.dd G 'at' HH:mm:ss z" => 2001.07.04 AD at 12:08:56 PDT  
	 *	"EEE, MMM d, ''yy"  => Wed, Jul 4, '01  
	 *	"h:mm a"   => 12:08 PM  
	 *	"hh 'o''clock' a, zzzz"   => 12 o'clock PM, Pacific Daylight Time  
	 * "K:mm a, z"  => 0:08 PM, PDT  
	 *	"yyyyy.MMMMM.dd GGG hh:mm aaa"   => 02001.July.04 AD 12:08 PM  
	 *	"EEE, d MMM yyyy HH:mm:ss Z"  =>  Wed, 4 Jul 2001 12:08:56 -0700  
	 *	"yyMMddHHmmssZ"  => 010704120856-0700  
	 * </pre>
	 * @param str 날짜 문자열
	 * @param dateFormat 입련된 날짜형식
	 * @return date 결과 Date 
	 */
	public static Date getDate(String str, String dateFormat) {
		Date date = null;

		try {
			if (str == null || str.length() != dateFormat.length()) {
				return date;
			}
			//Date validation
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			sdf.setLenient(false);

			try {
				date = sdf.parse(str);
			} catch (ParseException pe) {
				date = new Date();
			}
			
		} catch (Exception e) {
			date = new Date();
		}

		return date;
	}

	/**
	 * <pre>
	 * Date 인스턴스를 넘긴다. 
	 * </pre>
	 * @return Date 
	 */
	public static Date getDate() {
		return new Date();
	}

	/**
	 * <pre>
	 * 입력된 Date Type을  원하는 DateFormat으로  반환한다.
	 * Date>String
	 * ex)
	 * 
	 * 포멧예제 
	 * "yyyy.MM.dd G 'at' HH:mm:ss z" => 2001.07.04 AD at 12:08:56 PDT  
	 *	"EEE, MMM d, ''yy"  => Wed, Jul 4, '01  
	 *	"h:mm a"   => 12:08 PM  
	 *	"hh 'o''clock' a, zzzz"   => 12 o'clock PM, Pacific Daylight Time  
	 * "K:mm a, z"  => 0:08 PM, PDT  
	 *	"yyyyy.MMMMM.dd GGG hh:mm aaa"   => 02001.July.04 AD 12:08 PM  
	 *	"EEE, d MMM yyyy HH:mm:ss Z"  =>  Wed, 4 Jul 2001 12:08:56 -0700  
	 *	"yyMMddHHmmssZ"  => 010704120856-0700  
	 * </pre>
	 * @param date Date
	 * @param format 출력할 Date 형식
	 * @return 변환한 날짜형식
	 */
	public static String getDate2String(Date date, String format) {
		if (date != null) {
			DateFormat df = new SimpleDateFormat(format, Locale.ENGLISH);
			return df.format(date);
		}

		return null;
	}
	public static String getDate2String(Date date, String format, Locale locale ) {
		if (date != null) {
			DateFormat df = new SimpleDateFormat(format, locale);
			return df.format(date);
		}

		return null;
	}
	
	/**
	 * <pre>
	 * 오늘 일자를 입력된 Date Type을  원하는 DateFormat으로 반환하는 기능을 제공한다.
	 * Date>String
	 * ex)
	 * 
	 * 포멧예제 
	 * "yyyy.MM.dd G 'at' HH:mm:ss z" => 2001.07.04 AD at 12:08:56 PDT  
	 *	"EEE, MMM d, ''yy"  => Wed, Jul 4, '01  
	 *	"h:mm a"   => 12:08 PM  
	 *	"hh 'o''clock' a, zzzz"   => 12 o'clock PM, Pacific Daylight Time  
	 * "K:mm a, z"  => 0:08 PM, PDT  
	 *	"yyyyy.MMMMM.dd GGG hh:mm aaa"   => 02001.July.04 AD 12:08 PM  
	 *	"EEE, d MMM yyyy HH:mm:ss Z"  =>  Wed, 4 Jul 2001 12:08:56 -0700  
	 *	"yyMMddHHmmssZ"  => 010704120856-0700  
	 * </pre>
	 * @param format 출력할 Date 형식
	 * @return String 변환한 날짜형식
	 */
	public static String getToday(String format, Locale locale) {
		return getDate2String(getDate(), format, locale);
	}
	
	public static String getToday(String format) {
		return getDate2String(getDate(), format);
	}

	/**
	 * 내일의 날짜
	 * @return
	 */
	public static Date getTomorrow(){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
		return cal.getTime();
	}
	
	/**
	 * 내일의 날짜
	 * @return
	 */
	public static String getTomorrow(String format){
		return getDate2String(getTomorrow(), format);
	}
	
	/**
	 * term 일 만큼 더한 날짜
	 * @return
	 */
	public static Date addDate(int term){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, term);
		return cal.getTime();
	}
	
	/**
	 * 주어진 날짜에서 term 일 만큼 더한 날짜
	 * @return
	 */
	public static Date addDate(Date orgDate, int term){
		// 날짜 더하기
        Calendar cal = Calendar.getInstance();
        cal.setTime(orgDate);
        cal.add(Calendar.DATE, term);
        
        return cal.getTime();
	}
	
	/**
	 * term 월 만큼 더한 날짜
	 * @param term
	 * @return
	 */
	public static Date addMonth(int term){
		Calendar cal = Calendar.getInstance(); 
		cal.add(Calendar.MONTH, term);
		return cal.getTime();
	}
	
	/**
	 * term 일 만큼 더한 날짜
	 * @return
	 */
	public static Date addMinute(int term){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, term);
		return cal.getTime();
	}
}
