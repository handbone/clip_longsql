/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.coopnc.clipif.mappers;

import java.util.List;

import com.coopnc.clipif.bean.PointSwapCancelVo;
import com.coopnc.clipif.bean.PointSwapInfo;

public interface PointSwapMapper {
	
	public List<PointSwapInfo> selectSwapList(PointSwapInfo pointSwapInfo) throws Exception;
	
	public void insertPointSwapCancelInfo(PointSwapCancelVo pointSwapCancelVo);
	
	public void insertPointSwapHist(PointSwapInfo pointSwapInfo);
}
