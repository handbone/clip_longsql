package com.coopnc.clipif.bean;

public class FtpFileInfo {
	
	private String standDate;
	
	private String localFilePath;
	
	private String localFileName;
	
	private String serverFilePath;	
	
	private String serverFileName;

	public String getLocalFilePath() {
		return localFilePath;
	}

	public void setLocalFilePath(String localFilePath) {
		this.localFilePath = localFilePath;
	}

	public String getLocalFileName() {
		return localFileName;
	}

	public void setLocalFileName(String localFileName) {
		this.localFileName = localFileName;
	}

	public String getServerFilePath() {
		return serverFilePath;
	}

	public void setServerFilePath(String serverFilePath) {
		this.serverFilePath = serverFilePath;
	}

	public String getServerFileName() {
		return serverFileName;
	}

	public void setServerFileName(String serverFileName) {
		this.serverFileName = serverFileName;
	}

	public String getStandDate() {
		return standDate;
	}

	public void setStandDate(String standDate) {
		this.standDate = standDate;
	}
	
}
