package com.coopnc.clipif.config;

public class BaseMessages {
	
	 public static final String MSG_SUCCESS = "OK";
	 //error
	 public static final String MSG_ERR_INVALID_PARAM = "Invalid Parameter";
	 public static final String MSG_ERR_AUTH_FAIL = "Auth Fail";
	 public static final String MSG_ERR_NO_RESULT = "No Result";
	 public static final String MSG_ERR_INVALID_METHOD = "Invalid Method";
	 public static final String MSG_ERR_SYSTEM_ERR = "System Error";
	 public static final String MSG_ERR_DUPLICATE = "Duplicate Request";
	 
	public static String getVal(String code) {
		String msg = "";
		
		switch (code) {
			case BaseConstants.SUCCESS:
				msg = MSG_SUCCESS;
				break;
			case BaseConstants.ERR_INVALID_PARAM:
				msg = MSG_ERR_INVALID_PARAM;
				break;
			case BaseConstants.ERR_AUTH_FAIL:
				msg = MSG_ERR_AUTH_FAIL;
				break;
			case BaseConstants.ERR_NO_RESULT:
				msg = MSG_ERR_NO_RESULT;
				break;
			case BaseConstants.ERR_INVALID_METHOD:
				msg = MSG_ERR_INVALID_METHOD;
				break;
			case BaseConstants.ERR_SYSTEM_ERR:
				msg = MSG_ERR_SYSTEM_ERR;
				break;
			case BaseConstants.ERR_DUPLICATE:
				msg = MSG_ERR_DUPLICATE;
				break;
			default:
				break;
		}
 
		 return msg;
	 }
	 
}
