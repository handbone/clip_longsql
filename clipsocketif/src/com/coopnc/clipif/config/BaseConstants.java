package com.coopnc.clipif.config;

import java.util.concurrent.ConcurrentHashMap;

import io.netty.channel.Channel;

public class BaseConstants {
	
	/** Default Encoding (UTF-8) */
	public static final String	DEFAULT_ENCODING		= "utf-8";

	/** Default domain */
	//public static final String	DOMAIN					= "http://tclipps.giftishow.co.kr/";//"http://admin.fortune.daum.net:12005"; //"http://m.fortune.daum.net";
	public static final String	DOMAIN					= "http://clippointsvctb.clipwallet.co.kr/";//"http://admin.fortune.daum.net:12005"; //"http://m.fortune.daum.net";

	/** Default domain */
	public static final String	API_DOMAIN				= "http://clippointsvctb.clipwallet.co.kr/";
//	public static final String	API_DOMAIN				= "http://192.168.151.199:8180/";
	
	public static final int CLIP_IF_TOT_MSG_LEN = 504;	//500 + 4
	public static final int CLIP_IF_MSG_LEN = 500;	//500 + 4
	
	/**********************************
	 *****       이하 고도화	  *****
	 **********************************/
	 
	//CLiP 포인트 서버 URL
	// 조회 시 사용자가 없으면 가입하지 않는 url
	 public static final String ADV_GET_POINT = "/adv/getPoint2.do";
	 // 조회 시 사용자가 없으면 가입하는 url
//	 public static final String ADV_GET_POINT = "/adv/getPoint.do";
	 public static final String ADV_PLUS_POINT = "/adv/plusPoint.do";
	 public static final String ADV_PLUS_POINT_CANCEL = "/adv/plusPointCancel.do";
	 public static final String ADV_MINUS_POINT = "/adv/minusPoint.do";
	 public static final String ADV_MINUS_POINT_CANCEL = "/adv/minusPointCancel.do";

	 //동의 여부 체크시 서비스 ID
	 public static final String AGREEMENT_SERVICE_ID_HANA = "hanamembers";
	 public static final String AGREEMENT_SERVICE_ID_KB = "shinhancard";
	 public static final String AGREEMENT_SERVICE_ID_SHINHAN = "kbcard";
	 
	 public static final String REQ_TYPE_GET 			= "10"; //조회
	 public static final String REQ_TYPE_PLUS 			= "20"; //적립
	 public static final String REQ_TYPE_PLUS_CANCEL 	= "21"; //적립 취소
	 public static final String REQ_TYPE_MINUS 			= "30"; //차감
	 public static final String REQ_TYPE_MINUS_CANCEL 	= "31"; //차감 취소

	 public static final String USER_KEY_USER_CI = "1";
	 public static final String USER_KEY_USER_TOKEN = "2";
	 
	 public static final String SUCCESS = "200";
	 //error
	 public static final String ERR_INVALID_PARAM = "400";
	 public static final String ERR_AUTH_FAIL = "401";
	 public static final String ERR_NO_RESULT = "404";
	 public static final String ERR_INVALID_METHOD = "405";
	 public static final String ERR_SYSTEM_ERR = "500";
	 public static final String ERR_DUPLICATE = "600";
	 
	 public static final String REQUESTER_CODE_SHINHAN = "shinhancard";
	 public static final String REQUESTER_CODE_HANA = "hanamembers";
	 
	 public static final class ShinhanCmsConsts {
		 
		 public static ConcurrentHashMap<Integer, Channel> queue = new ConcurrentHashMap<Integer, Channel>();

		 public static final String REG_SOURCE = "shinhan";
		 
		 public static final String COMPANY_CODE = "02";
		 public static final String WORK_DIV = "EFT";
		 
		 //TODO 수정
		 public static final String FILE_PREFIX = "TKTC0001";
		 
		 /*0320 : 일괄 전송 처리 Data
		 0300 : 결번자료 확인 응답
		 0310 : 결번자료 전송
		 0600 : 업무개시 요구
		 0610 : 업무개시 응답
		 0620 : 결번전문 확인
		 0630 : 파일 정보 관리
		 0640 : 파일 정보 관리 응답*/
		 
		 public static final String COMM_DATA_SEND = "0320";
		 public static final String COMM_MISS_NO_RES = "0300";
		 public static final String COMM_MISS_NO_SEND = "0310";
		 public static final String COMM_PROGRESS_REQ = "0600";
		 public static final String COMM_PROGRESS_RES = "0610";
		 public static final String COMM_MISS_NO_REQ = "0620";
		 public static final String COMM_FILE_INFO_REQ = "0630";
		 public static final String COMM_FILE_INFO_RES = "0640";

		 public static final String TRADE_DIV_SEND = "S";
		 public static final String TRADE_DIV_RECV = "R";
		 
		 public static final String SEND_FLAG_SHINHAN = "B";
		 public static final String SEND_FLAG_KT = "C";

		 //600 전문 - 업무관리 : 001-> 003 -> 004 으로 진행
		 public static final String COMMAND_START = "001";		// 개별업무 개시
		 public static final String COMMAND_NEXT = "002";		// 다음파일 송신 - 우리는 쓰지 않음
		 public static final String COMMAND_PART_CLOSE = "003";	// 개별업무 종료
		 public static final String COMMAND_CLOSE = "004";		// 전체업무종료

		 /*
		 000	 정상
		 090	 시스템 장애
		 310	 송신자명 오류
		 320	 송신자 암호오류
		 630	 기전송 완료SDFSDF
		 631	 해당기관 미등록 업무
		 632	 비정상 파일명
		 633	 비정상 전문 BYTE 수
		 800	 FORMAT 오류
		 */
		 
		 public static final String RESULT_CODE_SUCCESS = "000";
		 public static final String ERR_CODE_SYSTEM_ERR = "090";
		 public static final String ERR_CODE_USER_NAME = "310";
		 public static final String ERR_CODE_PASSWORD = "320";
		 public static final String ERR_CODE_COMPLETE = "630";
		 
		 public static final String ERR_CODE_UNREGIST_WORK = "631";
		 public static final String ERR_CODE_INVALID_FILENAME = "632";
		 public static final String ERR_CODE_INVALID_FILEBYTE = "633";
		 public static final String ERR_CODE_INVALID_FORMAT = "800";
		 
	 }
	 
}
