package com.coopnc.clipif.server;

import java.net.InetSocketAddress;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.coopnc.clipif.service.ClipPointClientService;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;

@Component
public class ClipPointServer {
	
	private int port;
	private int bossCount;
	private int workerCount;
	
	@Autowired
	Environment env;
	
	@Autowired
	ClipPointClientService clipPointClientService;

	ClipPointMessageHandler SERVICE_HANDLER = null;
	
	@PostConstruct
	void init(){
		SERVICE_HANDLER = new ClipPointMessageHandler();
		SERVICE_HANDLER.setClipPointClientService(clipPointClientService);
		
		bossCount = Integer.parseInt(env.getProperty("clipif.server.boss.thread.count"));
		workerCount = Integer.parseInt(env.getProperty("clipif.server.worker.thread.count"));
		port = Integer.parseInt(env.getProperty("clipif.server.port"));
	}

	@Async
	public void start() {

		EventLoopGroup bossGroup = new NioEventLoopGroup(bossCount);
		EventLoopGroup workerGroup = new NioEventLoopGroup(workerCount);
		
		InetSocketAddress sock = new InetSocketAddress(port);

		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
					.childHandler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel ch) throws Exception {
							ChannelPipeline pipeline = ch.pipeline();
							pipeline.addLast("framerDecoder", new FixedLengthFrameDecoder(504));
							pipeline.addLast("decode", new ByteArrayDecoder());
							pipeline.addLast("encoder", new ByteArrayEncoder());
							pipeline.addLast(SERVICE_HANDLER);
						}
					});

			ChannelFuture channelFuture = b.bind(sock).sync();
			channelFuture.channel().closeFuture().sync();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}
	
}
