package com.coopnc.clipif.bean;

import com.coopnc.clipif.util.IntHolder;
import com.coopnc.clipif.util.MessageUtil;

public class BaseSocketMsg {
	
	protected byte[] message;
	
	protected byte[] header;
	protected byte[] body;
	
	static final byte[] B_SPACE = " ".getBytes();
	
	protected void initMsg(int totMsgLen){
		message = new byte[totMsgLen];
		
		//메세지 초기화 - 모두 스페이스로 채움
		for (int i = 0 ; i < message.length ; i++){
			message[i] = B_SPACE[0];
		}
	}
	
	protected void initMsg(byte[] data){
		//메세지 초기화 - 모두 스페이스로 채움
		for (int i = 0 ; i < data.length ; i++){
			data[i] = B_SPACE[0];
		}
	}
	
	/**
	 * msg 에 pos 위치부터 len 길이 만큼 msg를 기입한다. N 은 넘버, 1 이고 길이가 4이면 0001 빈값 0
	 * @param src
	 * @param srcPos
	 * @param msg
	 * @param len
	 */
	protected void addN(String number, IntHolder _pos, int len){
		if(number == null)
			number = "";
		
		if(number.startsWith("-")) {
			number = "-" + MessageUtil.lpad(number.substring(1), len-1, "0");
		} else {
			number = MessageUtil.lpad(number, len, "0");
		}
		
		System.arraycopy(number.getBytes(), 0, message, _pos.value, len);
		
		_pos.add(len);
	}
	
	protected void addN(byte[] msg, String number, IntHolder _pos, int len){
		if(number == null)
			number = "";
		
		if(number.startsWith("-")) {
			number = "-" + MessageUtil.lpad(number.substring(1), len-1, "0");
		} else {
			number = MessageUtil.lpad(number, len, "0");
		}
		
		System.arraycopy(number.getBytes(), 0, msg, _pos.value, len);
		
		_pos.add(len);
	}
	
	/**
	 * src 에 pos 위치부터 len 길이 만큼 msg를 기입한다. AN 은 영문숫자, 좌정렬 기본, 빈값 스페이스
	 * @param src
	 * @param srcPos
	 * @param msg
	 * @param len
	 */
	protected void addAN(String str, IntHolder _pos, int len){	
		if(str == null)
			str = "";
		
		if("".equals(str)) {
			_pos.add(len);
			return;
		}
		
		str = MessageUtil.rpad(str, len, " ");	
		System.arraycopy(str.getBytes(), 0, message, _pos.value , len);
		_pos.add(len);	
	}
	
	protected void addAN(byte[] msg, String str, IntHolder _pos, int len){	
		if(str == null)
			str = "";
		
		if("".equals(str)) {
			_pos.add(len);
			return;
		}
		
		str = MessageUtil.rpad(str, len, " ");	
		System.arraycopy(str.getBytes(), 0, msg, _pos.value , len);
		_pos.add(len);	
	}
	
	/**
	 * src 에 pos 위치부터 len 길이 만큼 msg를 기입한다. HAN 은 한글숫자, 좌정렬 기본, 빈값 스페이스
	 * @param src
	 * @param srcPos
	 * @param msg
	 * @param len
	 */
	protected void addHAN(String str, IntHolder _pos, int len){
		if(str == null)
			str = "";
		
		if("".equals(str)) {
			_pos.add(len);
			return;
		}
		
		str = MessageUtil.rpad(str, len, " ");
		System.arraycopy(str.getBytes(), 0, message, _pos.value, len);
		_pos.add(len);
	}
	
	protected void addHAN(byte[] msg, String str, IntHolder _pos, int len){
		if(str == null)
			str = "";
		
		if("".equals(str)) {
			_pos.add(len);
			return;
		}
		
		str = MessageUtil.rpad(str, len, " ");
		System.arraycopy(str.getBytes(), 0, msg, _pos.value, len);
		_pos.add(len);
	}
	
	/**
	 * 응답 메세지에서 필요한 부분만 추출, N 은 넘버, 1 이고 길이가 4이면 0001 빈값 0
	 * @param pos
	 * @param len
	 * @return
	 */
	protected String getN(int pos, int len) {
		try {
			String data = MessageUtil.cutStrByByteIndexLength(message, pos, len).trim();
			return MessageUtil.removeZero(data);
		} catch (Exception e) {
			return null;
		}
	}
	
	protected String getN(byte[] msg, int pos, int len) {
		try {
			String data = MessageUtil.cutStrByByteIndexLength(msg, pos, len).trim();
			return MessageUtil.removeZero(data);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 응답 메세지에서 필요한 부분만 추출, AN 은 영문숫자, 좌정렬 기본, 빈값 스페이스
	 * @param pos
	 * @param len
	 * @return
	 */
	protected String getAN(int pos, int len) {	
		try {
			return MessageUtil.cutStrByByteIndexLength(message, pos, len).trim();
		} catch (Exception e) {
			return null;
		}
	}
	
	protected String getAN(byte[] msg, int pos, int len) {	
		try {
			return MessageUtil.cutStrByByteIndexLength(msg, pos, len).trim();
		} catch (Exception e) {
			return null;
		}
	}
	
	protected String getAN(byte[] msg, int pos, int len, boolean gb) {	
		try {
			return MessageUtil.cutStrByByteIndexLength(msg, pos, len);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 응답 메세지에서 필요한 부분만 추출, HAN 은 한글숫자, 좌정렬 기본, 빈값 스페이스
	 * @param pos
	 * @param len
	 * @return
	 */
	protected String getHAN(int pos, int len){
		try {
			return MessageUtil.cutStrByByteIndexLength(message, pos, len).trim();
		} catch (Exception e) {
			return null;
		}
	}
	
	protected String getHAN(byte[] msg, int pos, int len){
		try {
			return MessageUtil.cutStrByByteIndexLength(msg, pos, len).trim();
		} catch (Exception e) {
			return null;
		}
	}
	
	protected byte[] getByte(int pos, int len) {	
		try {
			return MessageUtil.cutByteByByteIndexLength(message, pos, len);
		} catch (Exception e) {
			return null;
		}
	}

}
