package com.coopnc.clipif.server;

import java.nio.charset.Charset;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.clipif.bean.ClipPointMessage;
import com.coopnc.clipif.bean.PointSwapInfo;
import com.coopnc.clipif.bean.SocketRequestVo;
import com.coopnc.clipif.bean.SocketResponseVo;
import com.coopnc.clipif.config.BaseConstants;
import com.coopnc.clipif.config.BaseMessages;
import com.coopnc.clipif.service.ClipPointClientService;
import com.coopnc.clipif.util.MessageUtil;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

@Sharable
public class ClipPointMessageHandler extends ChannelInboundHandlerAdapter {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	/*private final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		channels.add(ctx.channel());
	}*/
	
	private ClipPointClientService clipPointClientService;
	
	public void setClipPointClientService(final ClipPointClientService clipPointClientService){
		this.clipPointClientService = clipPointClientService;
	}
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		byte[] bmsg = (byte[])msg;
		
		String str0 = new String(bmsg, Charset.defaultCharset());
		logger.debug("Message Received [{}] ", str0);

		procMsg(ctx, str0.getBytes());
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
	
	private void procMsg(ChannelHandlerContext ctx, byte[] msg){

		//ClipPointMessage clipMsg = new ClipPointMessage();
		//clipMsg.setUser_ci("+SFFO1G3ARtr5oTXp+laSCGFqofSp9YzyTdaRKirdm4igRehvnN4oKDM2hqB9djs+pf4f1PqUvlocxyJliM0fA==");

		try {

			SocketRequestVo reqMsg = new SocketRequestVo(msg);
			
			if(!BaseConstants.SUCCESS.equals(requestMsgCheck(reqMsg))){
				SocketResponseVo resMsg = new SocketResponseVo(reqMsg);
				resMsg.setDatetime(MessageUtil.getDateStr("yyyyMMddHHmmss"));
				
				logger.debug("Invalid Request Message Received !! code[{}],resMsg[{}] ", reqMsg.getResult_code(), reqMsg.getResult_msg());
				ctx.writeAndFlush(resMsg.makeResponseMsg());

				return;
			}
			
			logger.debug("Message Info requester[{}],reqType[{}],trId[{}],user[{}],point[{}]"
					,reqMsg.getRequester_code(),reqMsg.getReq_type(),reqMsg.getTr_id(),reqMsg.getUser_key(),reqMsg.getPoint_value());
			
			ClipPointMessage result = null;
			
			if(BaseConstants.REQ_TYPE_GET.equals(reqMsg.getReq_type())){
				result = clipPointClientService.getPoint(reqMsg);
			} else if(BaseConstants.REQ_TYPE_PLUS.equals(reqMsg.getReq_type())){
				result = clipPointClientService.plusPoint(reqMsg);
			} else if(BaseConstants.REQ_TYPE_PLUS_CANCEL.equals(reqMsg.getReq_type())){
				result = clipPointClientService.cancelPlusPoint(reqMsg);
			} else if(BaseConstants.REQ_TYPE_MINUS.equals(reqMsg.getReq_type())){
				result = clipPointClientService.minusPoint(reqMsg);
			} else if(BaseConstants.REQ_TYPE_MINUS_CANCEL.equals(reqMsg.getReq_type())){
				result = clipPointClientService.cancelMinusPoint(reqMsg);
			} else {
				//requestMsgCheck 에서 체크하므로 여기는 들어오지 않는다. 
			}
			
			logger.debug("Result from Clip Point Server : result[{}],use_ci[{}] ",result.getResult(),result.getUser_ci());
			
			//하나 멤버스의 경우 거래 이력을 남겨 대사 정보를 처리한다.
			if((BaseConstants.REQ_TYPE_PLUS.equals(reqMsg.getReq_type()) 
					|| BaseConstants.REQ_TYPE_PLUS_CANCEL.equals(reqMsg.getReq_type()))
					&& "hanamembers".equals(reqMsg.getRequester_code())) {
				
				PointSwapInfo pointSwapInfo = new PointSwapInfo();

				pointSwapInfo.setReg_source(reqMsg.getRequester_code());
				pointSwapInfo.setTrans_div(getTransDiv(reqMsg.getReq_type()));
				pointSwapInfo.setTrans_id(reqMsg.getTr_id());
				pointSwapInfo.setRef_trans_id(reqMsg.getRef_tr_id());
				pointSwapInfo.setReq_point(Integer.parseInt(reqMsg.getPoint_value()));

				pointSwapInfo.setUser_ci(reqMsg.getUser_key());
				pointSwapInfo.setTrans_req_date(reqMsg.getDatetime());
				pointSwapInfo.setTrans_res_date(MessageUtil.getDateStr("yyyyMMddHHmmss"));
				pointSwapInfo.setResult(result.getResult());
				
				if("S".equals(result.getResult())) {
					pointSwapInfo.setRes_point(Integer.parseInt(reqMsg.getPoint_value()));
					pointSwapInfo.setApprove_no(result.getApprove_no());
					pointSwapInfo.setApprove_time(result.getDatetime().substring(8));
					pointSwapInfo.setApprove_date(result.getDatetime().substring(0, 8));
				}
				
				clipPointClientService.insertPointSwapHist(pointSwapInfo);
			}
			
			SocketResponseVo resMsg = new SocketResponseVo(reqMsg);
			
			if(BaseConstants.REQ_TYPE_GET.equals(reqMsg.getReq_type())){
				if(reqMsg.getUser_key().equals(result.getUser_ci())
						|| reqMsg.getUser_key().equals(result.getUser_token())){
					resMsg.setResult_code(BaseConstants.SUCCESS);
					resMsg.setResult_msg(BaseMessages.getVal(resMsg.getResult_code()));

					resMsg.setPoint_value(result.getBalance());
					resMsg.setApprove_no(result.getApprove_no());
					resMsg.setDatetime(result.getDatetime());
				} else {
					resMsg.setDatetime(MessageUtil.getDateStr("yyyyMMddHHmmss"));
					resMsg.setResult_code(result.getResult());
					resMsg.setResult_msg(BaseMessages.getVal(resMsg.getResult_code()));
				}
			} else {
				
				logger.debug("--------------------reqMsg.getTr_id() [{}]", reqMsg.getTr_id());
				logger.debug("--------------------result.getTransaction_id() [{}]", result.getTransaction_id());
				
				if(reqMsg.getTr_id().equals(result.getTransaction_id())){
					resMsg.setResult_code(BaseConstants.SUCCESS);
					resMsg.setResult_msg(BaseMessages.getVal(resMsg.getResult_code()));
					
					resMsg.setPoint_value(result.getBalance());
					resMsg.setApprove_no(result.getApprove_no());
					
					logger.debug("--------------------resMsg.getApprove_no() [{}]", resMsg.getApprove_no());
					logger.debug("--------------------resMsg.setPoint_value() [{}]", resMsg.getPoint_value());
					
					resMsg.setDatetime(result.getDatetime());
				} else {
					resMsg.setDatetime(MessageUtil.getDateStr("yyyyMMddHHmmss"));
					resMsg.setResult_code(result.getResult());
					resMsg.setResult_msg(BaseMessages.getVal(resMsg.getResult_code()));
				}
			}
			byte[] rst = resMsg.makeResponseMsg();
			logger.debug("Response Message Write : code[{}] msg[{}]", resMsg.getResult_msg(), new String(rst));
			ctx.writeAndFlush(rst);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
	}
	
	private String getTransDiv(String reqType){
		
		String result = "";
		
		switch(reqType) {
			case "20" :
				result = "U";
				break;
			case "21" :
				result = "N";
				break;
			case "30" :
				result = "U";
				break;
			case "31" :
				result = "N";
				break;
		}
		
		return result;
	}
	
	private String requestMsgCheck(SocketRequestVo reqMsg){
		
		reqMsg.setResult_code(BaseConstants.SUCCESS);
		reqMsg.setResult_msg("OK");
		
		if(StringUtils.isEmpty(reqMsg.getUser_key())) {
			reqMsg.setResult_code(BaseConstants.ERR_INVALID_PARAM);
			reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : user_key");
		} else if(StringUtils.isEmpty(reqMsg.getTr_id())){
			reqMsg.setResult_code(BaseConstants.ERR_INVALID_PARAM);
			reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : tr_id");
		} else if(StringUtils.isEmpty(reqMsg.getRequester_code())){

			if(BaseConstants.REQUESTER_CODE_SHINHAN.equals(reqMsg.getRequester_code()) == false
					&& BaseConstants.REQUESTER_CODE_HANA.equals(reqMsg.getRequester_code()) == false) {
				reqMsg.setResult_code(BaseConstants.ERR_AUTH_FAIL);
				reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : requester_code");
			} else {
				reqMsg.setResult_code(BaseConstants.ERR_INVALID_PARAM);
				reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : requester_code");
			}

		} else {
			//기본 파라미터 정상이고 추가 체크
			
			if(BaseConstants.REQ_TYPE_GET.equals(reqMsg.getReq_type())){
		
			} else if(BaseConstants.REQ_TYPE_PLUS.equals(reqMsg.getReq_type())){	
				if(StringUtils.isEmpty(reqMsg.getPoint_value())){
					reqMsg.setResult_code(BaseConstants.ERR_INVALID_PARAM);
					reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : point_value");
				}
			} else if(BaseConstants.REQ_TYPE_PLUS_CANCEL.equals(reqMsg.getReq_type())){
				if(StringUtils.isEmpty(reqMsg.getRef_tr_id())){
					reqMsg.setResult_code(BaseConstants.ERR_INVALID_PARAM);
					reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : ref_tr_id");
				}
				if(StringUtils.isEmpty(reqMsg.getPoint_value())){
					reqMsg.setResult_code(BaseConstants.ERR_INVALID_PARAM);
					reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : point_value");
				}
			} else if(BaseConstants.REQ_TYPE_MINUS.equals(reqMsg.getReq_type())){
				if(StringUtils.isEmpty(reqMsg.getPoint_value())){
					reqMsg.setResult_code(BaseConstants.ERR_INVALID_PARAM);
					reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : point_value");
				}
			} else if(BaseConstants.REQ_TYPE_MINUS_CANCEL.equals(reqMsg.getReq_type())){
				if(StringUtils.isEmpty(reqMsg.getRef_tr_id())){
					reqMsg.setResult_code(BaseConstants.ERR_INVALID_PARAM);
					reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : ref_tr_id");
				}
				if(StringUtils.isEmpty(reqMsg.getPoint_value())) {
					reqMsg.setResult_code(BaseConstants.ERR_INVALID_PARAM);
					reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : point_value");
				}
			} else {
				reqMsg.setResult_code(BaseConstants.ERR_INVALID_PARAM);
				reqMsg.setResult_msg(BaseMessages.getVal(reqMsg.getResult_code())+" : req_type");
			}
			
		}

		return reqMsg.getResult_code();
	}
}
