package com.coopnc.clipif.server;

import java.net.InetSocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.clipif.config.BaseConstants.ShinhanCmsConsts;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;

public class ShinhanCmsClient {

	private static Logger logger = LoggerFactory.getLogger(ShinhanCmsClient.class);	
	//private ChannelFuture channelFuture = null;
	
	private static final EventLoopGroup workerGroup = new NioEventLoopGroup(1);

	//private static Deque<Channel> queue = new LinkedList<>();
	private static Channel chan;

	private String host;
	private int port;
	
	private Bootstrap b = new Bootstrap();
	
	public ShinhanCmsClient(String host, int port) throws InterruptedException {
		this.host = host;
		this.port = port;
		init();
	}

	//isActive,isOpen
	public void init() throws InterruptedException {
//		logger.debug("Initializing client and connecting to server..");
		
		b.group(workerGroup).channel(NioSocketChannel.class)
		 		.option(ChannelOption.SO_KEEPALIVE, true)
				.handler(new ChannelInitializer<SocketChannel>() {
					@Override
					protected void initChannel(SocketChannel channel) throws Exception {
						channel.pipeline().addLast("framerDecoder", new LengthFieldBaseFrameDecoder(9999, 0, 4));
						channel.pipeline().addLast("decode", new ByteArrayDecoder());
						channel.pipeline().addLast("encoder", new ByteArrayEncoder());
						channel.pipeline().addLast("handler", new ShinhanChannelInboundHandler());
					}
					
					@Override
					public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
							throws Exception {
						cause.printStackTrace();
					}
				});

	}

	class ShinhanChannelInboundHandler extends SimpleChannelInboundHandler<Object> {
		private ResponseFuture responseFuture;

		public void setResponseFuture(ResponseFuture future) {
			this.responseFuture = future;
		}

		@Override
		protected void channelRead0(ChannelHandlerContext ctx, Object msg)
				throws Exception {
			responseFuture.set(msg);
			
			returnChan();
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
			cause.printStackTrace();
			close();
		}
	}
	
	public void close() {
		synchronized(ShinhanCmsConsts.queue) {
			try{
				if(chan != null)
					chan.close();
			} catch (Exception e){
			} finally{
				ShinhanCmsConsts.queue.remove(1);
			}
		}
	}
	
	// wait for, or create a channel
	private Channel acquireChan() throws Exception {
		
		int cnt = 3;
	    
        do {
            
        	if(ShinhanCmsConsts.queue.isEmpty()) {
        		// no idle channels, and we have space for another
                // create a channel, and add it to the queue.
                // hopefully it will be there when we go around the loop.

        		InetSocketAddress addr1 = new InetSocketAddress(host, port);
        		//InetSocketAddress addr2 = new InetSocketAddress("127.0.0.1", 52100);
            	ChannelFuture cf = b.connect(addr1);
                boolean connect = cf.await(2000);
                if (!connect) {
                    throw new Exception("Unable to try connection to Server in " + 2000 + "ms");
                }

                Channel newChan =  cf.channel();

                ShinhanCmsConsts.queue.put(1, newChan);
            } else {
                // otherwise wait a second, and loop again.
                // if a channel is returned in the interim, we will be notified.
            	if(cnt < 3)
            		ShinhanCmsConsts.queue.wait(1000);
            }
            
        	cnt --;
        	if(cnt <= 0)
        		throw new Exception("Unable to get connection in 3 try");
        	
            // OK, there's an available channel, make sure it is usable.
            Channel toUse = ShinhanCmsConsts.queue.get(1);
            if (toUse.isActive() && toUse.isWritable()) {
                // great, good to go, use it.
                return toUse;
            } else {
            	
            }

        } while (true);
        
	}

	private void returnChan() {
	    synchronized(ShinhanCmsConsts.queue) {
	    	ShinhanCmsConsts.queue.put(1,chan);
	    	ShinhanCmsConsts.queue.notifyAll();
	    }
	}

	public ResponseFuture send(final byte[] msg, boolean canRead) throws Exception {
		
		if(canRead) {
			final ResponseFuture responseFuture = new ResponseFuture();

			synchronized(ShinhanCmsConsts.queue) {
			    try {
			    	chan = acquireChan();
			        ChannelFuture future = chan.writeAndFlush(msg);
			        future.channel().pipeline().get(ShinhanChannelInboundHandler.class).setResponseFuture(responseFuture);
			        
			    } catch (Exception e) {
			        close();
			        throw e;
			    } finally {
			    	//returnChan()
			    }
			}

		    return responseFuture;
		} else {
			synchronized(ShinhanCmsConsts.queue) {
			    try {
			    	chan = acquireChan();
			    	chan.writeAndFlush(msg);
			    } catch (Exception e) {
			        close();
			        throw e;
			    } finally {
			    	returnChan();
			    }
			}
			
			return null;
		}

	}
}