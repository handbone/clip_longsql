package com.coopnc.clipif.server;

import java.net.InetSocketAddress;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.coopnc.clipif.service.ShinhanCmsServerService;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;

@Component
public class ShinhanCmsServer {
	
	private int port;
	private int bossCount;
	private int workerCount;
	
	@Autowired
	Environment env;
	
	@Autowired
	ShinhanCmsServerService shinhanCmsServerService;

	ShinhanCmsMessageHandler SERVICE_HANDLER = null;
	
	@PostConstruct
	void init(){
		SERVICE_HANDLER = new ShinhanCmsMessageHandler();
		SERVICE_HANDLER.setShinhanCmsServerService(shinhanCmsServerService);
		
		bossCount = Integer.parseInt(env.getProperty("shinhancms.local.boss.thread.count"));
		workerCount = Integer.parseInt(env.getProperty("shinhancms.local.worker.thread.count"));
		port = Integer.parseInt(env.getProperty("shinhancms.local.port"));
	}

	@Async
	public void start() {

		EventLoopGroup bossGroup = new NioEventLoopGroup(bossCount);
		EventLoopGroup workerGroup = new NioEventLoopGroup(workerCount);
		
		InetSocketAddress sock = new InetSocketAddress(port);

		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
					.childHandler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel ch) throws Exception {
							ChannelPipeline pipeline = ch.pipeline();
							pipeline.addLast("framerDecoder", new LengthFieldBaseFrameDecoder(4096, 0, 4));
							pipeline.addLast("decode", new ByteArrayDecoder());
							pipeline.addLast("encoder", new ByteArrayEncoder());
							pipeline.addLast(SERVICE_HANDLER);
						}
					});

			ChannelFuture channelFuture = b.bind(sock).sync();
			channelFuture.channel().closeFuture().sync();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}
	
}
