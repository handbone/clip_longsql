package com.coopnc.clipif.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coopnc.clipif.config.BaseConstants.ShinhanCmsConsts;
import com.coopnc.clipif.util.IntHolder;
import com.coopnc.clipif.util.MessageUtil;

public class ShinhanCmsRequest extends BaseSocketMsg {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public ShinhanCmsRequest(byte[] reqMsg){
		this.message = reqMsg;
		
		this.msg_len = getN(0, 4);			// 전문 길이	NUMBER	0	4
		this.work_div = getAN(4, 3); 		// A 4 3 업무구분 코드
		this.company_code = getAN(7, 2); 	// AN 7 2 회원사코드
		this.comm_type = getAN(9, 4); 		// AN 9 4 전문종별 코드
		this.trade_div = getAN(13, 1); 		// AN 13 1 거래구분 코드
		this.send_flag = getAN(14, 1); 		// A 14 1 송수신 Flag
		this.file_name = getAN(15, 8); 		// AN 15 8 파일명
		this.result_code = getAN(23, 3); 	// N 23 3 응답코드
		
		//길이필드(4) 값 - 헤더(22) = 바디 길이
		int body_size = Integer.parseInt(this.msg_len) - 22;
		
		logger.debug("BODY size : ["+body_size+"/"+this.message.length +"]");
		
		this.body = getByte(26, body_size);
		
		logger.debug("BODY : "+new String(this.body));
		
		parseBody();
	}
	
	public byte[] getMessage(){
		return this.message;
	}
	
	public byte[] getHeader() throws Exception {
		return MessageUtil.cutByteByByteIndexLength(this.message, 4, 22);
	}
	
	public void make(ShinhanCmsRequest request, String commType, String resultCode ,Object obj) {
		
		this.make(request.getTrade_div(), request.getSend_flag(), commType, request.getFile_name(), resultCode ,obj);
	}
	
	public void make(String tradeDiv, String sendFlag, String commType, String fileName, String resultCode ,Object obj) {
		
		//헤더 부분 완성
		this.work_div = ShinhanCmsConsts.WORK_DIV; 			// A 4 3 업무구분 코드
		this.company_code = ShinhanCmsConsts.COMPANY_CODE; 	// AN 7 2 회원사코드
		this.comm_type = commType; 		// AN 9 4 전문종별 코드
		this.trade_div = tradeDiv; 		// AN 13 1 거래구분 코드 R, S
		this.send_flag = sendFlag; 		// A 14 1 송수신 Flag	B(신한), C(기관)
		this.file_name = fileName; 		// AN 15 8 파일명
		this.result_code = resultCode; 	// N 23 3 응답코드
		
		IntHolder idx = new IntHolder();
		this.header = MessageUtil.getSpaceFillBytes(22);
		
		addAN(this.header, this.work_div, idx, 3);
		addAN(this.header, this.company_code, idx, 2);
		addAN(this.header, this.comm_type, idx, 4);
		addAN(this.header, this.trade_div, idx, 1);
		addAN(this.header, this.send_flag, idx, 1);
		addAN(this.header, this.file_name, idx, 8);
		addAN(this.header, this.result_code, idx, 3);

		//바디 부분 완성
		if(obj instanceof T0600) {
			T0600 bodyData = (T0600)obj;
			this.body = bodyData.make();		
		} else if (obj instanceof T0620) {
			T0620 bodyData = (T0620)obj;
			this.body = bodyData.make();
		} else if (obj instanceof T0630) {
			T0630 bodyData = (T0630)obj;
			this.body = bodyData.make();
		} else if (obj instanceof T0300) {
			T0300 bodyData = (T0300)obj;
			this.body = bodyData.make();
		} else if (obj instanceof T0310) {
			T0310 bodyData = (T0310)obj;
			this.body = bodyData.make();
		} else {
			// 이곳으로 오면 안됨.
		}
		
		//풀메세지 셋팅
		int totLen = 22 + this.body.length;
		String lenField = MessageUtil.lpad(""+totLen, 4, "0");
		this.message = new byte[totLen + 4];
		
		System.arraycopy(lenField.getBytes(), 0, this.message, 0, 4);
		System.arraycopy(this.header, 0, this.message, 4, this.header.length);
		System.arraycopy(this.body, 0, this.message, this.header.length + 4, this.body.length);

	}
	
	private Object bodyObject;
	
	public Object getBodyObject(String comm_type) {
		Object obj = null;
		switch(comm_type) {
			case ShinhanCmsConsts.COMM_DATA_SEND :
				obj = new T0310();
				break;
			case ShinhanCmsConsts.COMM_MISS_NO_RES : 
				obj = new T0300();
				break;
			case ShinhanCmsConsts.COMM_MISS_NO_SEND : 
				obj = new T0310();
				break;
			case ShinhanCmsConsts.COMM_PROGRESS_REQ : 
				obj = new T0600();
				break;
			case ShinhanCmsConsts.COMM_PROGRESS_RES : 
				obj = new T0600();
				break;
			case ShinhanCmsConsts.COMM_MISS_NO_REQ : 
				obj = new T0620();
				break;
			case ShinhanCmsConsts.COMM_FILE_INFO_REQ : 
				obj = new T0630();
				break;
			case ShinhanCmsConsts.COMM_FILE_INFO_RES : 
				obj = new T0630();
				break;
			default :
				;
		}
		return obj;
	}
	
	public ShinhanCmsRequest(){

	}
	
	/*
	//헤더 26
	N	0	4	전문SIZE
	A	4	3	업무구분 코드
	AN	7	2	회원사코드
	AN	9	4	전문종별 코드
	AN	13	1	거래구분 코드
	A	14	1	송수신 Flag
	AN	15	8	파일명
	N	23	3	응답코드
	
	//0600-0610
	N	10	전문전송 일시	
	N	3	업무관리 정보	
	AN	20	송신자명	
	AN	16	송신자 암호	
	
	//0620
	N	4	BLOCK-NO	
	N	3	최종 Sequence-No	
	
	//0630-0640
	AN	8	이름	
	N	12	크기	
	N	4	320 전문 Byte 크기	
	
	//0300
	N	4	BLOCK-NO	
	N	3	최종 Sequence-No	
	N	3	결번 개수	
	N	1	결번 확인
	
	//0310-0320			
	N	4	BLOCK-NO	
	N	3	최종  Sequence-No	
	N	4	실 DATA  BYTE수	
	AN	V	파일 내역
	*/

	private String msg_len = ""; 		// N 0 4 전문SIZE
	private String work_div = ""; 		// A 4 3 업무구분 코드
	private String company_code = ""; 	// AN 7 2 회원사코드
	private String comm_type = ""; 		// AN 9 4 전문종별 코드
	private String trade_div = ""; 		// AN 13 1 거래구분 코드
	private String send_flag = ""; 		// A 14 1 송수신 Flag
	private String file_name = ""; 		// AN 15 8 파일명
	private String result_code = ""; 	// N 23 3 응답코드
	
	public void parseBody(){

		switch(this.comm_type) {
			case ShinhanCmsConsts.COMM_DATA_SEND :
				T0310 data0 = new T0310();
				data0.parse(super.body);
				bodyObject = data0;
				break;
			case ShinhanCmsConsts.COMM_MISS_NO_RES : 
				T0300 data1 = new T0300();
				data1.parse(super.body);
				bodyObject = data1;
				break;
			case ShinhanCmsConsts.COMM_MISS_NO_SEND : 
				T0310 data2 = new T0310();
				data2.parse(super.body);
				bodyObject = data2;
				break;
			case ShinhanCmsConsts.COMM_PROGRESS_REQ : 
				T0600 data3 = new T0600();
				data3.parse(super.body);
				bodyObject = data3;
				break;
			case ShinhanCmsConsts.COMM_PROGRESS_RES : 
				T0600 data4 = new T0600();
				data4.parse(super.body);
				bodyObject = data4;
				break;
			case ShinhanCmsConsts.COMM_MISS_NO_REQ : 
				T0620 data5 = new T0620();
				data5.parse(super.body);
				bodyObject = data5;
				break;
			case ShinhanCmsConsts.COMM_FILE_INFO_REQ : 
				T0630 data6 = new T0630();
				data6.parse(super.body);
				bodyObject = data6;
				break;
			case ShinhanCmsConsts.COMM_FILE_INFO_RES : 
				T0630 data7 = new T0630();
				data7.parse(super.body);
				bodyObject = data7;
				break;
			default :
				;
		}
	}
	
	public class T0600 {
		
		//0600 - 0610
		/*
		전문전송 일시	N	10
		업무관리 정보	N	3
		송신자명	AN	20
		송신자 암호	AN	16
		*/
		
		public void parse(byte[] body){
			
			logger.debug("["+new String(body)+"]");
			
			comm_date = getAN(body, 0, 10);
			work_mgr_info = getAN(body, 10, 3);
			sender = getAN(body, 13, 20);
			sender_pass = getAN(body, 33, 16);
		}
		
		public byte[] make(){
			IntHolder _pos = new IntHolder();	//전문 포지션
			byte[] body = MessageUtil.getSpaceFillBytes(49);	//10+3+20+16
			
			addAN(body, comm_date, _pos, 10);
			
			logger.debug("T0600 work_mgr_info = "+work_mgr_info);
			addAN(body, work_mgr_info, _pos, 3);
			addAN(body, sender, _pos, 20);
			addAN(body, sender_pass, _pos, 16);
			
			return body;
		}
		
		private String comm_date;
		private String work_mgr_info;
		private String sender;
		private String sender_pass;
		
		public String getComm_date() {
			return comm_date;
		}
		public void setComm_date(String comm_date) {
			this.comm_date = comm_date;
		}
		public String getWork_mgr_info() {
			return work_mgr_info;
		}
		public void setWork_mgr_info(String work_mgr_info) {
			this.work_mgr_info = work_mgr_info;
		}
		public String getSender() {
			return sender;
		}
		public void setSender(String sender) {
			this.sender = sender;
		}
		public String getSender_pass() {
			return sender_pass;
		}
		public void setSender_pass(String sender_pass) {
			this.sender_pass = sender_pass;
		}
	}
	
	public class T0620 {
		//0620

		/*
		BLOCK-NO	N	4
		최종 Sequence-No	N	3
		*/
		
		public void parse(byte[] body){
			block_no = getN(body, 0, 4);
			seq_no = getN(body, 4, 3);
		}
		
		public byte[] make(){
			IntHolder _pos = new IntHolder();	//전문 포지션
			byte[] body = MessageUtil.getSpaceFillBytes(7);	//10+3+20+16
			
			addN(body, block_no, _pos, 4);
			addN(body, seq_no, _pos, 3);
			
			return body;
		}
		
		private String block_no;
		private String seq_no;
		
		public String getBlock_no() {
			return block_no;
		}
		public void setBlock_no(String block_no) {
			this.block_no = block_no;
		}
		public String getSeq_no() {
			return seq_no;
		}
		public void setSeq_no(String seq_no) {
			this.seq_no = seq_no;
		}

	}

	public class T0630 {
		//0630 - 0640
		
		/*
		파일정보내역
		
		이름	AN	8
		크기	N	12
		320 전문 Byte 크기	N	4
		*/
		
		public void parse(byte[] body){
			file_name = getAN(body, 0, 8);
			file_size = getN(body, 8, 12);
			comm_data_size = getN(body, 20, 4);
		}
		
		public byte[] make(){
			IntHolder _pos = new IntHolder();	//전문 포지션
			byte[] body = MessageUtil.getSpaceFillBytes(24);	//10+3+20+16
			
			addAN(body, file_name, _pos, 8);
			addN(body, file_size, _pos, 12);
			addN(body, comm_data_size, _pos, 4);
			
			return body;
		}
		
		private String file_name;
		private String file_size;
		private String comm_data_size;
		
		public String getFile_name() {
			return file_name;
		}
		public void setFile_name(String file_name) {
			this.file_name = file_name;
		}
		public String getFile_size() {
			return file_size;
		}
		public void setFile_size(String file_size) {
			this.file_size = file_size;
		}
		public String getComm_data_size() {
			return comm_data_size;
		}
		public void setComm_data_size(String comm_data_size) {
			this.comm_data_size = comm_data_size;
		}

	}
	
	public class T0300 {
		
		//0300
		/*
		BLOCK-NO	N	4
		최종 Sequence-No	N	3
		결번 개수	N	3
		결번 확인	N	1
		*/
		
		public void parse(byte[] body){
			block_no = getN(body, 0, 4);
			final_seq_no = getN(body, 4, 3);
			miss_cnt = getN(body, 7, 3);
			miss_confirm = getAN(body, 10, body.length - 10);
		}
		
		public byte[] make(){
			IntHolder _pos = new IntHolder();	//전문 포지션
			byte[] body = MessageUtil.getSpaceFillBytes(10 + miss_confirm.getBytes().length);	//10+3+20+16
			
			addN(body, block_no, _pos, 4);
			addN(body, final_seq_no, _pos, 3);
			addN(body, miss_cnt, _pos, 3);
			addAN(body, miss_confirm, _pos, miss_confirm.getBytes().length);
			
			return body;
		}
		
		private String block_no;
		private String final_seq_no;
		private String miss_cnt;
		private String miss_confirm;
		public String getBlock_no() {
			return block_no;
		}
		public void setBlock_no(String block_no) {
			this.block_no = block_no;
		}
		public String getFinal_seq_no() {
			return final_seq_no;
		}
		public void setFinal_seq_no(String final_seq_no) {
			this.final_seq_no = final_seq_no;
		}
		public String getMiss_cnt() {
			return miss_cnt;
		}
		public void setMiss_cnt(String miss_cnt) {
			this.miss_cnt = miss_cnt;
		}
		public String getMiss_confirm() {
			return miss_confirm;
		}
		public void setMiss_confirm(String miss_confirm) {
			this.miss_confirm = miss_confirm;
		}

	}

	public class T0310 {
		//0320
		/*
		BLOCK-NO	N	4
		최종  Sequence-No	N	3
		실 DATA  BYTE수	N	4
		파일 내역	AN	V
		*/
		
		public void parse(byte[] body){
			block_no = getN(body, 0, 4);
			final_seq_no = getN(body, 4, 3);
			real_data_size = getN(body, 7, 4);
			try {
				real_data = MessageUtil.cutByteByByteIndexLength(body, 11, Integer.parseInt(real_data_size));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public byte[] make(){
			IntHolder _pos = new IntHolder();	//전문 포지션
			byte[] body = new byte[11 + real_data.length];	//10+3+20+16
			
			addN(body, block_no, _pos, 4);
			addN(body, final_seq_no, _pos, 3);
			addN(body, real_data_size, _pos, 4);
			System.arraycopy(real_data, 0, body, _pos.value, real_data.length);
			
			return body;
		}
		
		private String block_no;
		private String final_seq_no;
		private String real_data_size;
		private byte[] real_data;
		
		public String getBlock_no() {
			return block_no;
		}
		public void setBlock_no(String block_no) {
			this.block_no = block_no;
		}
		public String getFinal_seq_no() {
			return final_seq_no;
		}
		public void setFinal_seq_no(String final_seq_no) {
			this.final_seq_no = final_seq_no;
		}
		public String getReal_data_size() {
			return real_data_size;
		}
		public void setReal_data_size(String real_data_size) {
			this.real_data_size = real_data_size;
		}
		public byte[] getReal_data() {
			return real_data;
		}
		public void setReal_data(byte[] real_data) {
			this.real_data = real_data;
		}
		
	}

	public String getMsg_len() {
		return msg_len;
	}

	public void setMsg_len(String msg_len) {
		this.msg_len = msg_len;
	}

	public String getWork_div() {
		return work_div;
	}

	public void setWork_div(String work_div) {
		this.work_div = work_div;
	}

	public String getCompany_code() {
		return company_code;
	}

	public void setCompany_code(String company_code) {
		this.company_code = company_code;
	}

	public String getTrade_div() {
		return trade_div;
	}

	public void setTrade_div(String trade_div) {
		this.trade_div = trade_div;
	}

	public String getSend_flag() {
		return send_flag;
	}

	public void setSend_flag(String send_flag) {
		this.send_flag = send_flag;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getResult_code() {
		return result_code;
	}

	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

	public String getComm_type() {
		return comm_type;
	}

	public void setComm_type(String comm_type) {
		this.comm_type = comm_type;
	}

	public Object getBodyObject() {
		return bodyObject;
	}

	public void setBodyObject(Object bodyObject) {
		this.bodyObject = bodyObject;
	}
}
