package com.coopnc.clipif.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShinhanCmsMgrVo {
	private String fileName;
	private long fileSIze;
	private long incomeSize;
	
	private String fileFullPath;
	
	private String procDate;
	private String progress;
	private int unitSize;	

	private List<PointSwapInfo> swapHistList;
	private Map<Integer, ShinhanCmsBlockInfoVo> blockInfo = new HashMap<Integer, ShinhanCmsBlockInfoVo>();

	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getProcDate() {
		return procDate;
	}
	public void setProcDate(String procDate) {
		this.procDate = procDate;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}

	public List<PointSwapInfo> getSwapHistList() {
		return swapHistList;
	}
	public void setSwapHistList(List<PointSwapInfo> swapHistList) {
		this.swapHistList = swapHistList;
	}
	public long getFileSIze() {
		return fileSIze;
	}
	public void setFileSIze(long fileSIze) {
		this.fileSIze = fileSIze;
	}
	public long getIncomeSize() {
		return incomeSize;
	}
	public void setIncomeSize(long incomeSize) {
		this.incomeSize = incomeSize;
	}
	public Map<Integer, ShinhanCmsBlockInfoVo> getBlockInfo() {
		return blockInfo;
	}
	public void setBlockInfo(HashMap<Integer, ShinhanCmsBlockInfoVo> blockInfo) {
		this.blockInfo = blockInfo;
	}
	public int getUnitSize() {
		return unitSize;
	}
	public void setUnitSize(int unitSize) {
		this.unitSize = unitSize;
	}
	public String getFileFullPath() {
		return fileFullPath;
	}
	public void setFileFullPath(String fileFullPath) {
		this.fileFullPath = fileFullPath;
	}
	
}
