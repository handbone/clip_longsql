package com.coopnc.clipif.bean;

public class PointSwapInfo {

	private int idx;
	
	private String trans_div; 	//조회(R), 취소(C), 전환(E), 정산(S) 구분
	private String trans_req_date;
	private String trans_res_date;
	private String reg_source;
	private String user_ci;
	private String cust_id_org;
	private String trans_id;
	private String ref_trans_id;		//취소시 참조 아이디

	private int req_point;		//요청한 포인트 
	private int res_point;		//응답한 포인트
	
	private int avail_point;	//사용시에만 남은 포인트 체크
		
	private String file_name;
	private String res_code;
	private String res_message;
	
	private String result;

	private byte[] req_msg_bytes;
	private byte[] res_msg_bytes;

	private FtpFileInfo ftpFileInfo;
	
	//이하 일대사용 추가 멤버
	private String approve_no;		//승인번호
	private String approve_date;	//승인일자
	private String approve_time;	//승인시간
	private int serial_no;
	
	public String getTrans_div() {
		return trans_div;
	}
	public void setTrans_div(String trans_div) {
		this.trans_div = trans_div;
	}
	public String getTrans_req_date() {
		return trans_req_date;
	}
	public void setTrans_req_date(String trans_req_date) {
		this.trans_req_date = trans_req_date;
	}
	public String getTrans_res_date() {
		return trans_res_date;
	}
	public void setTrans_res_date(String trans_res_date) {
		this.trans_res_date = trans_res_date;
	}
	public String getReg_source() {
		return reg_source;
	}
	public void setReg_source(String reg_source) {
		this.reg_source = reg_source;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getTrans_id() {
		return trans_id;
	}
	public void setTrans_id(String trans_id) {
		this.trans_id = trans_id;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getRes_code() {
		return res_code;
	}
	public void setRes_code(String res_code) {
		this.res_code = res_code;
	}
	public byte[] getReq_msg_bytes() {
		return req_msg_bytes;
	}
	public void setReq_msg_bytes(byte[] req_msg_bytes) {
		this.req_msg_bytes = req_msg_bytes;
	}
	public byte[] getRes_msg_bytes() {
		return res_msg_bytes;
	}
	public void setRes_msg_bytes(byte[] res_msg_bytes) {
		this.res_msg_bytes = res_msg_bytes;
	}
	public FtpFileInfo getFtpFileInfo() {
		return ftpFileInfo;
	}
	public void setFtpFileInfo(FtpFileInfo ftpFileInfo) {
		this.ftpFileInfo = ftpFileInfo;
	}
	public int getReq_point() {
		return req_point;
	}
	public void setReq_point(int req_point) {
		this.req_point = req_point;
	}
	public String getRes_message() {
		return res_message;
	}
	public void setRes_message(String res_message) {
		this.res_message = res_message;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public int getRes_point() {
		return res_point;
	}
	public void setRes_point(int res_point) {
		this.res_point = res_point;
	}
	public String getRef_trans_id() {
		return ref_trans_id;
	}
	public void setRef_trans_id(String ref_trans_id) {
		this.ref_trans_id = ref_trans_id;
	}
	public String getApprove_no() {
		return approve_no;
	}
	public void setApprove_no(String approve_no) {
		this.approve_no = approve_no;
	}
	public String getApprove_date() {
		return approve_date;
	}
	public void setApprove_date(String approve_date) {
		this.approve_date = approve_date;
	}
	public String getApprove_time() {
		return approve_time;
	}
	public void setApprove_time(String approve_time) {
		this.approve_time = approve_time;
	}
	public int getAvail_point() {
		return avail_point;
	}
	public void setAvail_point(int avail_point) {
		this.avail_point = avail_point;
	}
	public String getCust_id_org() {
		return cust_id_org;
	}
	public void setCust_id_org(String cust_id_org) {
		this.cust_id_org = cust_id_org;
	}
	public int getSerial_no() {
		return serial_no;
	}
	public void setSerial_no(int serial_no) {
		this.serial_no = serial_no;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}

	
}
