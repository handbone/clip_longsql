package com.coopnc.clipif.bean;

import com.coopnc.clipif.config.BaseConstants;
import com.coopnc.clipif.util.IntHolder;
import com.coopnc.clipif.util.MessageUtil;

public class SocketResponseVo extends BaseSocketMsg {
	
	public SocketResponseVo(SocketRequestVo socketRequestVo){
		this.msg_len = ""+BaseConstants.CLIP_IF_MSG_LEN;
		this.req_type = socketRequestVo.getReq_type();
		this.tr_id = socketRequestVo.getTr_id();
		this.requester_code = socketRequestVo.getRequester_code();
		this.user_key_type = socketRequestVo.getUser_key_type();
		this.user_key = socketRequestVo.getUser_key();
		this.datetime = MessageUtil.getDateStr("yyyyMMddHHmmss");
		this.req_point_value = socketRequestVo.getPoint_value();
		
		this.result_code = socketRequestVo.getResult_code();
		this.result_msg = socketRequestVo.getResult_msg();
	}
	
	/*
	필드명(한글)	데이터 Type	idx	길이
	전문 길이	NUMBER	0	4
	요청 유형	CHAR	4	2
	요청자 코드	CHAR	6	20
	거래 아이디	CHAR	26	50
	처리 일시	CHAR	76	14
	사용자 구분 방법	CHAR	90	1
	사용자 구분 값	CHAR	91	88
	처리 결과	CHAR	179	3
	처리 결과 메시지	CHAR	182	150
	승인번호	CHAR	332	10
	요청 포인트	NUMBER	342	10
	현재 포인트	NUMBER	352	10
	예비	CHAR	362	142
			504	504
	*/
	
	public byte[] makeResponseMsg(){
		super.initMsg(BaseConstants.CLIP_IF_TOT_MSG_LEN);
		
		IntHolder idx= new IntHolder(0); 
		super.addN(this.msg_len,idx ,4);          //전문 길이	NUMBER	0	4
		super.addAN(this.req_type,idx ,2);         //요청 유형	CHAR	4	2
		super.addAN(this.requester_code,idx ,20);   //요청자 코드	CHAR	6	20
		super.addAN(this.tr_id,idx ,50);            //거래 아이디	CHAR	26	50
		super.addAN(this.datetime,idx ,14);         //처리 일시	CHAR	76	14
		super.addAN(this.user_key_type,idx ,1);    //사용자 구분 방법	CHAR	90	1
		super.addAN(this.user_key,idx ,88);         //사용자 구분 값	CHAR	91	88
		super.addAN(this.result_code,idx ,3);      //처리 결과	CHAR	179	3
		super.addAN(this.result_msg,idx , 150);       //처리 결과 메시지	CHAR	182	150
		super.addAN(this.approve_no,idx , 10);       //승인번호	CHAR	332	10
		super.addN(this.req_point_value,idx ,10);  //요청 포인트	NUMBER	342	10
		super.addN(this.point_value,idx ,10);      //현재 포인트	NUMBER	352	10
		//항상 빈값이므로 생략 : super.addAN(this.filler,idx ,142);           //예비	CHAR	362	142
		
		return super.message;
	}
	
	private String msg_len = "";
	private String req_type = "";
	private String requester_code = "";
	private String tr_id = "";
	private String datetime = "";
	private String user_key_type = "";
	private String user_key = "";
	private String result_code = "";
	private String result_msg = "";
	private String approve_no = "";
	private String req_point_value = "";
	private String point_value = "";
	private String filler = "";
	
	public String getMsg_len() {
		return msg_len;
	}
	public void setMsg_len(String msg_len) {
		this.msg_len = msg_len;
	}
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getRequester_code() {
		return requester_code;
	}
	public void setRequester_code(String requester_code) {
		this.requester_code = requester_code;
	}
	public String getTr_id() {
		return tr_id;
	}
	public void setTr_id(String tr_id) {
		this.tr_id = tr_id;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public String getUser_key_type() {
		return user_key_type;
	}
	public void setUser_key_type(String user_key_type) {
		this.user_key_type = user_key_type;
	}
	public String getUser_key() {
		return user_key;
	}
	public void setUser_key(String user_key) {
		this.user_key = user_key;
	}
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public String getResult_msg() {
		return result_msg;
	}
	public void setResult_msg(String result_msg) {
		this.result_msg = result_msg;
	}
	public String getApprove_no() {
		return approve_no;
	}
	public void setApprove_no(String approve_no) {
		this.approve_no = approve_no;
	}
	public String getReq_point_value() {
		return req_point_value;
	}
	public void setReq_point_value(String req_point_value) {
		this.req_point_value = req_point_value;
	}
	public String getPoint_value() {
		return point_value;
	}
	public void setPoint_value(String point_value) {
		this.point_value = point_value;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
}
