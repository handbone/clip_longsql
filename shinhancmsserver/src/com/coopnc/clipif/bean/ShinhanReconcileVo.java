package com.coopnc.clipif.bean;

import com.coopnc.clipif.util.IntHolder;

public class ShinhanReconcileVo extends BaseSocketMsg {

	public ShinhanReconcileVo(PointSwapInfo swapInfo, int seq){
		this.sra_c_vl = "DT";
		this.sra_ts_seqnum = ""+seq;	//1부터 증가
		
		if (swapInfo.getTrans_div().equals("U")) {
			this.sra_ts_tp_bne_cd = "30";	//포인트사용승인30, 포인트사용취소승인39
		} else if (swapInfo.getTrans_div().equals("N")) {
			this.sra_ts_tp_bne_cd = "39";	//포인트사용승인30, 포인트사용취소승인39
		} else {
			this.sra_ts_tp_bne_cd = "";	//포인트사용승인30, 포인트사용취소승인39
		}
		
		this.sra_ts_uqe_n = swapInfo.getTrans_id();
		this.sra_ps_ucd = swapInfo.getRes_code();		//주관사 거래 처리 결과	, 예를 들어 포인트사용승인 정상인경우 0000
		
		this.sra_pnt_se_apv_pnt = ""+swapInfo.getReq_point();	//거래 포인트
		this.sra_pnt_tpl_ri_pnt = ""+swapInfo.getRes_point();	//실제처리포인트
		this.sra_r_ts_uqe_n = swapInfo.getRef_trans_id();
		this.sra_ipn_lik_n = swapInfo.getUser_ci();
		this.sra_pnt_kcd = "L01";	//'L01' 마이신한포인트, 향후 포인트유형추가 예정
		this.sra_pnt_kd_lcd = "ZG";	//- 통합리워드 : 그룹사코드 = 'S000' then 'ZG'
		this.sra_pnt_se_tcd = "G71";	//G71 : KT클립포인트전환사용
		this.sra_pnt_acm_tcd = "";	//1차에서는 예비라함
		this.sra_pnt_ec_tcd = "";	//1차에서는 예비라함
		this.sra_ts_rqk = "";//swapInfo.getCust_id_org();
		this.sra_pnt_ts_vrf_ru = "";
		
		/*
		계열사검증결과 하단 코드 참조
		4001 거래유형업무오류
		4002 처리결과코드오류
		4003 원거래고유번호오류
		4004 포인트종류 상이
		4005 포인트적립유형오류
		4006 포인트적립유형오류
		4007 포인트정산유형오류
		4008 거래요청자오류
		4101 거래요청금액오류
		4102 CI값상이오류
		4103 거래처리금액오류
		4201 포인트거래미요청건
		*/
		
		this.sra_ig_clnn = "";		//우리는 해당 없음 (신한카드는 필수항목 통합고객번호로 거래 대사)
		this.sra_fer = "";			//filler
	}
	
	public ShinhanReconcileVo(byte[] data){

		//body
		this.sra_c_vl			= getAN(data, 0, 2);	// 0	2	CHAR	SRA_C_VL	신한그룹통합리워드앱구분값                       
		this.sra_ts_seqnum		= getN(data, 2, 10);	// 2	10	NUMBER	SRA_TS_SEQNUM	신한그룹통합리워드앱거래일련번호                 
		this.sra_ts_tp_bne_cd	= getAN(data, 12, 2);	// 12	2	CHAR	SRA_TS_TP_BNE_CD	신한그룹통합리워드앱거래유형업무코드     
		this.sra_ts_uqe_n		= getAN(data, 14, 21);	// 14	21	CHAR	SRA_TS_UQE_N	신한그룹통합리워드앱거래고유번호                 
		this.sra_ps_ucd			= getAN(data, 35, 4);	// 35	4	CHAR	SRA_PS_UCD	신한그룹통합리워드앱처리결과코드                 
		this.sra_pnt_se_apv_pnt	= getN(data, 39, 10);	// 39	10	NUMBER	SRA_PNT_SE_APV_PNT	신한그룹통합리워드앱포인트사용승인포인트 
		this.sra_pnt_tpl_ri_pnt	= getN(data, 49, 10);	// 49	10	NUMBER	SRA_PNT_TPL_RI_PNT	신한그룹통합리워드앱포인트거래처리포인트 
		this.sra_r_ts_uqe_n		= getAN(data, 59, 20);	// 59	21	CHAR	SRA_R_TS_UQE_N	신한그룹통합리워드앱원거래고유번호               
		this.sra_ipn_lik_n		= getAN(data, 80, 88);	// 80	88	CHAR	SRA_IPN_LIK_N	신한그룹통합리워드앱아이핀연계번호               
		this.sra_pnt_kcd		= getAN(data, 168, 3);	// 168	3	CHAR	SRA_PNT_KCD	신한그룹통합리워드앱포인트종류코드               
		this.sra_pnt_kd_lcd		= getAN(data, 171, 2);	// 171	2	CHAR	SRA_PNT_KD_LCD	신한그룹통합리워드앱포인트종류상세코드           
		this.sra_pnt_se_tcd		= getAN(data, 173, 3);	// 173	3	CHAR	SRA_PNT_SE_TCD	신한그룹통합리워드앱포인트사용유형코드           
		this.sra_pnt_acm_tcd	= getAN(data, 176, 6);	// 176	6	CHAR	SRA_PNT_ACM_TCD	신한그룹통합리워드앱포인트적립유형코드           
		this.sra_pnt_ec_tcd		= getAN(data, 182, 9);	// 182	9	CHAR	SRA_PNT_EC_TCD	신한그룹통합리워드앱포인트정산유형코드           
		this.sra_ts_rqk			= getAN(data, 191, 10);	// 191	10	CHAR	SRA_TS_RQK	신한그룹통합리워드앱거래요청자                   
		this.sra_pnt_ts_vrf_ru	= getAN(data, 201, 4);	// 201	4	CHAR	SRA_PNT_TS_VRF_RU	신한그룹통합리워드앱포인트거래검증결과   
		this.sra_ig_clnn = "";				// 205	13	CHAR	SRA_IG_CLNN	신한그룹통합리워드앱통합고객번호                 
		this.sra_fer 	= "";				// 218	282	CHAR	SRA_FER	신한그룹통합리워드앱필러
		
	}
	
	public String toString(){
		message = new byte[500];
		
		//메세지 초기화 - 모두 스페이스로 채움
		for (int i = 0 ; i < message.length ; i++){
			message[i] = B_SPACE[0];
		}
		
		IntHolder idx = new IntHolder(0);
		addAN(this.sra_c_vl, idx ,2);			// 0	2	CHAR	SRA_C_VL	신한그룹통합리워드앱구분값                      
		addN(this.sra_ts_seqnum, idx ,10);		// 2	10	NUMBER	SRA_TS_SEQNUM	신한그룹통합리워드앱거래일련번호                
		addAN(this.sra_ts_tp_bne_cd, idx ,2);	// 12	2	CHAR	SRA_TS_TP_BNE_CD	신한그룹통합리워드앱거래유형업무코드    
		addAN(this.sra_ts_uqe_n, idx ,21);		// 14	21	CHAR	SRA_TS_UQE_N	신한그룹통합리워드앱거래고유번호                
		addAN(this.sra_ps_ucd, idx ,4);			// 35	4	CHAR	SRA_PS_UCD	신한그룹통합리워드앱처리결과코드                
		addN(this.sra_pnt_se_apv_pnt, idx ,10);	// 39	10	NUMBER	SRA_PNT_SE_APV_PNT	신한그룹통합리워드앱포인트사용승인포인트
		addN(this.sra_pnt_tpl_ri_pnt, idx ,10);	// 49	10	NUMBER	SRA_PNT_TPL_RI_PNT	신한그룹통합리워드앱포인트거래처리포인트
		addAN(this.sra_r_ts_uqe_n, idx ,21);		// 59	21	CHAR	SRA_R_TS_UQE_N	신한그룹통합리워드앱원거래고유번호              
		addAN(this.sra_ipn_lik_n, idx ,88);		// 80	88	CHAR	SRA_IPN_LIK_N	신한그룹통합리워드앱아이핀연계번호              
		addAN(this.sra_pnt_kcd, idx ,3);		// 168	3	CHAR	SRA_PNT_KCD	신한그룹통합리워드앱포인트종류코드              
		addAN(this.sra_pnt_kd_lcd, idx ,2);		// 171	2	CHAR	SRA_PNT_KD_LCD	신한그룹통합리워드앱포인트종류상세코드          
		addAN(this.sra_pnt_se_tcd, idx ,3);		// 173	3	CHAR	SRA_PNT_SE_TCD	신한그룹통합리워드앱포인트사용유형코드          
		addAN(this.sra_pnt_acm_tcd, idx ,6);		// 176	6	CHAR	SRA_PNT_ACM_TCD	신한그룹통합리워드앱포인트적립유형코드          
		addAN(this.sra_pnt_ec_tcd, idx ,9);		// 182	9	CHAR	SRA_PNT_EC_TCD	신한그룹통합리워드앱포인트정산유형코드          
		addAN(this.sra_ts_rqk, idx ,10);		// 191	10	CHAR	SRA_TS_RQK	신한그룹통합리워드앱거래요청자                  
		addAN(this.sra_pnt_ts_vrf_ru, idx ,4);		// 201	4	CHAR	SRA_PNT_TS_VRF_RU	신한그룹통합리워드앱포인트거래검증결과  
		addAN(this.sra_ig_clnn, idx ,13);		// 205	13	CHAR	SRA_IG_CLNN	신한그룹통합리워드앱통합고객번호                
		addAN(this.sra_fer, idx ,282);			// 218	282	CHAR	SRA_FER	신한그룹통합리워드앱필러  
		
		return new String(message);
	}
	
	/*
	Type	idx	길이	필드명(영문)	필드명(한글)
	CHAR	0	2	SRA_C_VL	신한그룹통합리워드앱구분값
	NUMBER	2	10	SRA_TS_SEQNUM	신한그룹통합리워드앱거래일련번호
	CHAR	12	5	SRA_TG_N	신한그룹통합리워드앱전문번호
	CHAR	17	4	SRA_SHP_CO_CD	신한그룹통합리워드앱신한그룹사코드
	CHAR	21	4	SRA_TG_VER	신한그룹통합리워드앱전문버전
	CHAR	25	8	SRA_TS_CED	신한그룹통합리워드앱거래기준일자
	CHAR	33	14	SRA_CRT_DT	신한그룹통합리워드앱생성일시
	CHAR	47	453	SRA_FER		신한그룹통합리워드앱필러
	
	0	2	CHAR	SRA_C_VL	신한그룹통합리워드앱구분값
	2	10	NUMBER	SRA_TS_SEQNUM	신한그룹통합리워드앱거래일련번호
	12	2	CHAR	SRA_TS_TP_BNE_CD	신한그룹통합리워드앱거래유형업무코드
	14	21	CHAR	SRA_TS_UQE_N	신한그룹통합리워드앱거래고유번호
	35	4	CHAR	SRA_PS_UCD	신한그룹통합리워드앱처리결과코드
	39	10	NUMBER	SRA_PNT_SE_APV_PNT	신한그룹통합리워드앱포인트사용승인포인트
	49	10	NUMBER	SRA_PNT_TPL_RI_PNT	신한그룹통합리워드앱포인트거래처리포인트
	59	21	CHAR	SRA_R_TS_UQE_N	신한그룹통합리워드앱원거래고유번호
	80	88	CHAR	SRA_IPN_LIK_N	신한그룹통합리워드앱아이핀연계번호
	168	3	CHAR	SRA_PNT_KCD	신한그룹통합리워드앱포인트종류코드
	171	2	CHAR	SRA_PNT_KD_LCD	신한그룹통합리워드앱포인트종류상세코드
	173	3	CHAR	SRA_PNT_SE_TCD	신한그룹통합리워드앱포인트사용유형코드
	176	6	CHAR	SRA_PNT_ACM_TCD	신한그룹통합리워드앱포인트적립유형코드
	182	9	CHAR	SRA_PNT_EC_TCD	신한그룹통합리워드앱포인트정산유형코드
	191	10	CHAR	SRA_TS_RQK	신한그룹통합리워드앱거래요청자
	201	4	CHAR	SRA_PNT_TS_VRF_RU	신한그룹통합리워드앱포인트거래검증결과
	205	13	CHAR	SRA_IG_CLNN	신한그룹통합리워드앱통합고객번호
	218	282	CHAR	SRA_FER	신한그룹통합리워드앱필러

	
	CHAR	0	2	SRA_C_VL	신한그룹통합리워드앱구분값
	NUMBER	2	10	SRA_TS_SEQNUM	신한그룹통합리워드앱거래일련번호
	NUMBER	12	10	SRA_CT		신한그룹통합리워드앱건수
	CHAR	22	478	SRA_FER		신한그룹통합리워드앱필러
	*/
	
	//헤더	
	private String sra_c_vl;
	private String sra_ts_seqnum;
	private String sra_tg_n;
	private String sra_shp_co_cd;
	private String sra_tg_ver;
	private String sra_ts_ced;
	private String sra_crt_dt;
	private String sra_fer;
	
	//body
	//private String sra_c_vl;
	//private String sra_ts_seqnum;
	private String sra_ts_tp_bne_cd;
	private String sra_ts_uqe_n;
	private String sra_ps_ucd;
	private String sra_pnt_se_apv_pnt;
	private String sra_pnt_tpl_ri_pnt;
	private String sra_r_ts_uqe_n;
	private String sra_ipn_lik_n;
	private String sra_pnt_kcd;
	private String sra_pnt_kd_lcd;
	private String sra_pnt_se_tcd;
	private String sra_pnt_acm_tcd;
	private String sra_pnt_ec_tcd;
	private String sra_ts_rqk;
	private String sra_pnt_ts_vrf_ru;
	private String sra_ig_clnn;
	//private String sra_fer;
	
	//테일
	//private String sra_c_vl;
	//private String sra_ts_seqnum;
	private String sra_ct;
	//private String sra_fer;

	public String getSra_c_vl() {
		return sra_c_vl;
	}

	public void setSra_c_vl(String sra_c_vl) {
		this.sra_c_vl = sra_c_vl;
	}

	public String getSra_ts_seqnum() {
		return sra_ts_seqnum;
	}

	public void setSra_ts_seqnum(String sra_ts_seqnum) {
		this.sra_ts_seqnum = sra_ts_seqnum;
	}

	public String getSra_tg_n() {
		return sra_tg_n;
	}

	public void setSra_tg_n(String sra_tg_n) {
		this.sra_tg_n = sra_tg_n;
	}

	public String getSra_shp_co_cd() {
		return sra_shp_co_cd;
	}

	public void setSra_shp_co_cd(String sra_shp_co_cd) {
		this.sra_shp_co_cd = sra_shp_co_cd;
	}

	public String getSra_tg_ver() {
		return sra_tg_ver;
	}

	public void setSra_tg_ver(String sra_tg_ver) {
		this.sra_tg_ver = sra_tg_ver;
	}

	public String getSra_ts_ced() {
		return sra_ts_ced;
	}

	public void setSra_ts_ced(String sra_ts_ced) {
		this.sra_ts_ced = sra_ts_ced;
	}

	public String getSra_crt_dt() {
		return sra_crt_dt;
	}

	public void setSra_crt_dt(String sra_crt_dt) {
		this.sra_crt_dt = sra_crt_dt;
	}

	public String getSra_fer() {
		return sra_fer;
	}

	public void setSra_fer(String sra_fer) {
		this.sra_fer = sra_fer;
	}

	public String getSra_ts_tp_bne_cd() {
		return sra_ts_tp_bne_cd;
	}

	public void setSra_ts_tp_bne_cd(String sra_ts_tp_bne_cd) {
		this.sra_ts_tp_bne_cd = sra_ts_tp_bne_cd;
	}

	public String getSra_ts_uqe_n() {
		return sra_ts_uqe_n;
	}

	public void setSra_ts_uqe_n(String sra_ts_uqe_n) {
		this.sra_ts_uqe_n = sra_ts_uqe_n;
	}

	public String getSra_ps_ucd() {
		return sra_ps_ucd;
	}

	public void setSra_ps_ucd(String sra_ps_ucd) {
		this.sra_ps_ucd = sra_ps_ucd;
	}

	public String getSra_pnt_se_apv_pnt() {
		return sra_pnt_se_apv_pnt;
	}

	public void setSra_pnt_se_apv_pnt(String sra_pnt_se_apv_pnt) {
		this.sra_pnt_se_apv_pnt = sra_pnt_se_apv_pnt;
	}

	public String getSra_pnt_tpl_ri_pnt() {
		return sra_pnt_tpl_ri_pnt;
	}

	public void setSra_pnt_tpl_ri_pnt(String sra_pnt_tpl_ri_pnt) {
		this.sra_pnt_tpl_ri_pnt = sra_pnt_tpl_ri_pnt;
	}

	public String getSra_r_ts_uqe_n() {
		return sra_r_ts_uqe_n;
	}

	public void setSra_r_ts_uqe_n(String sra_r_ts_uqe_n) {
		this.sra_r_ts_uqe_n = sra_r_ts_uqe_n;
	}

	public String getSra_ipn_lik_n() {
		return sra_ipn_lik_n;
	}

	public void setSra_ipn_lik_n(String sra_ipn_lik_n) {
		this.sra_ipn_lik_n = sra_ipn_lik_n;
	}

	public String getSra_pnt_kcd() {
		return sra_pnt_kcd;
	}

	public void setSra_pnt_kcd(String sra_pnt_kcd) {
		this.sra_pnt_kcd = sra_pnt_kcd;
	}

	public String getSra_pnt_kd_lcd() {
		return sra_pnt_kd_lcd;
	}

	public void setSra_pnt_kd_lcd(String sra_pnt_kd_lcd) {
		this.sra_pnt_kd_lcd = sra_pnt_kd_lcd;
	}

	public String getSra_pnt_se_tcd() {
		return sra_pnt_se_tcd;
	}

	public void setSra_pnt_se_tcd(String sra_pnt_se_tcd) {
		this.sra_pnt_se_tcd = sra_pnt_se_tcd;
	}

	public String getSra_pnt_acm_tcd() {
		return sra_pnt_acm_tcd;
	}

	public void setSra_pnt_acm_tcd(String sra_pnt_acm_tcd) {
		this.sra_pnt_acm_tcd = sra_pnt_acm_tcd;
	}

	public String getSra_pnt_ec_tcd() {
		return sra_pnt_ec_tcd;
	}

	public void setSra_pnt_ec_tcd(String sra_pnt_ec_tcd) {
		this.sra_pnt_ec_tcd = sra_pnt_ec_tcd;
	}

	public String getSra_ts_rqk() {
		return sra_ts_rqk;
	}

	public void setSra_ts_rqk(String sra_ts_rqk) {
		this.sra_ts_rqk = sra_ts_rqk;
	}

	public String getSra_pnt_ts_vrf_ru() {
		return sra_pnt_ts_vrf_ru;
	}

	public void setSra_pnt_ts_vrf_ru(String sra_pnt_ts_vrf_ru) {
		this.sra_pnt_ts_vrf_ru = sra_pnt_ts_vrf_ru;
	}

	public String getSra_ig_clnn() {
		return sra_ig_clnn;
	}

	public void setSra_ig_clnn(String sra_ig_clnn) {
		this.sra_ig_clnn = sra_ig_clnn;
	}

	public String getSra_ct() {
		return sra_ct;
	}

	public void setSra_ct(String sra_ct) {
		this.sra_ct = sra_ct;
	}
}
