package com.coopnc.clipif.bean;

public class SocketRequestVo extends BaseSocketMsg {
	
	/*
	필드명(한글)	데이터 Type	idx	길이
	전문 길이	NUMBER	0	4
	요청 유형	CHAR	4	2
	요청자 코드	CHAR	6	20
	거래 아이디	CHAR	26	50
	요청 일시	CHAR	76	14
	사용자 구분 방법	CHAR	90	1
	사용자 구분 값	CHAR	91	88
	요청 포인트	NUMBER	179	10
	취소 거래 아이디	CHAR	189	50
	내용	CHAR	239	150
	예비	CHAR	389	115
			504	504
	*/
	
	private void parseMsg(byte[] reqMsg){
		this.message = reqMsg;
		
		this.msg_len = getN(0, 4);			//전문 길이	NUMBER	0	4
		this.req_type = getN(4, 2);			//요청 유형	CHAR	4	2
		this.requester_code = getN(6, 20);	//요청자 코드	CHAR	6	20
		this.tr_id = getN(26, 50);			//거래 아이디	CHAR	26	50
		this.datetime = getN(76, 14);			//요청 일시	CHAR	76	14
		this.user_key_type = getN(90, 1);	//사용자 구분 방법	CHAR	90	1
		this.user_key = getN(91, 88);		//사용자 구분 값	CHAR	91	88
		this.point_value = getN(179, 10);		//요청 포인트	NUMBER	179	10
		this.ref_tr_id = getN(189, 50);		//취소 거래 아이디	CHAR	189	50
		this.description = getN(239, 150);		//내용	CHAR	239	150
		this.filler = getN(389, 115);		//예비	CHAR	389	115
	}
	
	public SocketRequestVo(byte[] reqMsg){
		parseMsg(reqMsg);
	}

	private String msg_len = "";
	private String req_type = "";
	private String requester_code = "";
	private String tr_id = "";
	private String datetime = "";
	private String user_key_type = "";
	private String user_key = "";
	private String point_value = "";
	private String ref_tr_id = "";
	private String description = "";
	private String filler = "";
	
	private String result_code;
	private String result_msg;
	
	public String getMsg_len() {
		return msg_len;
	}
	public void setMsg_len(String msg_len) {
		this.msg_len = msg_len;
	}
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getRequester_code() {
		return requester_code;
	}
	public void setRequester_code(String requester_code) {
		this.requester_code = requester_code;
	}
	public String getTr_id() {
		return tr_id;
	}
	public void setTr_id(String tr_id) {
		this.tr_id = tr_id;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public String getUser_key_type() {
		return user_key_type;
	}
	public void setUser_key_type(String user_key_type) {
		this.user_key_type = user_key_type;
	}
	public String getUser_key() {
		return user_key;
	}
	public void setUser_key(String user_key) {
		this.user_key = user_key;
	}
	public String getPoint_value() {
		return point_value;
	}
	public void setPoint_value(String point_value) {
		this.point_value = point_value;
	}
	public String getRef_tr_id() {
		return ref_tr_id;
	}
	public void setRef_tr_id(String ref_tr_id) {
		this.ref_tr_id = ref_tr_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

	public String getResult_msg() {
		return result_msg;
	}

	public void setResult_msg(String result_msg) {
		this.result_msg = result_msg;
	}
}
