package com.coopnc.clipif.bean;

import java.util.HashMap;
import java.util.Map;

public class ShinhanCmsBlockInfoVo {
	
	private int blockNo;
	private int maxSeq;
	private byte[] seqs;
	
	private Map<Integer, byte[]> data = new HashMap<Integer, byte[]>();
	
	public int getBlockNo() {
		return blockNo;
	}
	public void setBlockNo(int blockNo) {
		this.blockNo = blockNo;
	}
	public byte[] getSeqs() {
		return seqs;
	}
	public void setSeqs(byte[] seqs) {
		this.seqs = seqs;
	}
	public void addData(int seq, byte[] data){
		if(seq > maxSeq)
			maxSeq = seq;
		
		this.data.put(seq, data);
	}
	public boolean chkData(int seq){
		if(this.data.containsKey(seq))
			return true;
		else
			return false;
	}
	public int getMaxSeq() {
		return maxSeq;
	}
	public void setMaxSeq(int maxSeq) {
		this.maxSeq = maxSeq;
	}
	public Map<Integer, byte[]> getData() {
		return data;
	}
	public void setData(Map<Integer, byte[]> data) {
		this.data = data;
	}
}
