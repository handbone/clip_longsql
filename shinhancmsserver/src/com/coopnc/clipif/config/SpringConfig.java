package com.coopnc.clipif.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com.coopnc.clipif")
@PropertySource("file:${CLIP_SOCK_IF_HOME}/config/server.properties")
public class SpringConfig {
	/*@Bean
	public HelloWorld helloWorld() {
		return new HelloWorld();
	}*/
}
