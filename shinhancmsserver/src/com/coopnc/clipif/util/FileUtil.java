package com.coopnc.clipif.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.io.FileUtils;

public class FileUtil {
	
	private static final String AL_SHA_256 = "SHA-256";
	
	/**
	 * 파일이 변경되었는지 여부 검사 -> 리턴값있음 : 변경됨, null : 변경안됨
	 * @param orgCheckSum
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static String getFileChecksumIfChange(String orgCheckSum, File file) throws IOException {
		try {
			MessageDigest shaDigest = MessageDigest.getInstance(AL_SHA_256);	 
			String checkSum = getFileChecksum(shaDigest, file);
			
			if(! checkSum.equals(orgCheckSum))
				return checkSum;

		} catch(NoSuchAlgorithmException e) {
			e.printStackTrace(); 
		}

		return null;
	}
	
	private static String getFileChecksum(MessageDigest digest, File file) throws IOException {
		FileInputStream fis = new FileInputStream(file);

		byte[] byteArray = new byte[1024];
		int bytesCount = 0;

		while ((bytesCount = fis.read(byteArray)) != -1) {
			digest.update(byteArray, 0, bytesCount);
		}

		fis.close();

		byte[] bytes = digest.digest();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}
	
	public static void append(String filePath, String line){
		BufferedWriter out = null;
		try {
		    out = new BufferedWriter(new FileWriter(filePath, true));
		    out.write(line);
		    out.newLine();
		} catch(IOException ioe) {
		    System.err.println("IOException: " + ioe.getMessage());
		} finally {
			if(out != null)
				try {
					out.close();
				} catch (IOException e) {}
		}
	}
	
	public static void appendNoNewLine(String filePath, String line){
		BufferedWriter out = null;
		try {
		    out = new BufferedWriter(new FileWriter(filePath, true));
		    out.write(line);
		} catch(IOException ioe) {
		    System.err.println("IOException: " + ioe.getMessage());
		} finally {
			if(out != null)
				try {
					out.close();
				} catch (IOException e) {}
		}
	}
	
	public static void append(String filePath, byte[] data){
		FileOutputStream out = null;
		try {
			FileUtils.writeByteArrayToFile(new File(filePath), data, true);
		    
		} catch(IOException ioe) {
		    System.err.println("IOException: " + ioe.getMessage());
		} finally {
			if(out != null)
				try {
					out.close();
				} catch (IOException e) {}
		}
	}
	
	public static byte[] fileSearch(String fileFullPath, long pointer, int len) {
		
		RandomAccessFile raf = null;
		byte[] data = null;
		try {
			raf = new RandomAccessFile(fileFullPath, "r");
			
			if(raf.length() <= pointer) {
				return null;
			}
			
			long reqLen = (pointer+len);
			if(raf.length() < reqLen) {
				long realSize = raf.length() - pointer; 
				data = new byte[(int)realSize];
			} else {
				data = new byte[len];
			}

			raf.seek(pointer);
			raf.read(data);
			
		} catch (Exception e) {
			
		} finally {
			if(raf !=null) {
				try {
					raf.close();
				} catch (Exception e) {}
			}
		}
		
		return data;
	}

}
