package com.coopnc.clipif.util;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MessageUtil {

	public static String zeroPad(int length, String toPad) {
		int numberOfZeroes = length - toPad.length();
		for (int counter = 0; counter < numberOfZeroes; counter++) {
			toPad = "0" + toPad;
		}
		return toPad;
	}

	public static String lpad(String data, int count, String addString) {
		String tmpData = (data == null) ? "" : data;
		if (tmpData.length() >= count) {
			return tmpData;
		} else {
			return lpad(addString + tmpData, count, addString);
		}
	}

	public static String rpad(String data, int count, String addString) {
		String tmpData = (data == null) ? "" : data;
		if (tmpData.length() >= count) {
			return tmpData;
		} else {
			return rpad(tmpData + addString, count, addString);
		}
	}

	public static String cutFirstStrInByte(String str, int endIndex, String charsetName) {
		StringBuffer sbStr = new StringBuffer(endIndex);
		int iTotal = 0;
		for (char c : str.toCharArray()) {
			try {
				iTotal += String.valueOf(c).getBytes(charsetName).length;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			if (iTotal > endIndex) {
				break;
			}
			sbStr.append(c);
		}
		return sbStr.toString();
	}

	public static String[] getPushSMSStr(String str, int byteLength, String encoding, int encodeNeedByteLength)
			throws UnsupportedEncodingException {
		if (str == null)
			return null;

		if (byteLength < 0)
			return null;

		String[] strInfo = new String[2];
		String url = str.substring(str.lastIndexOf("http"), str.length()); // URL

		if (url.indexOf("\r\n") > -1) {
			url = url.substring(0, url.indexOf("\r\n"));
			strInfo[1] = url;
			strInfo[0] = cutStrByByteLength(
					str.substring(0, str.indexOf(url))
							+ str.substring(str.indexOf(url) + url.length() + "\r\n".length(), str.length()),
					byteLength, encoding, encodeNeedByteLength); // Message
		} else if (url.indexOf("\n") > -1) {
			url = url.substring(0, url.indexOf("\n"));
			strInfo[1] = url;
			strInfo[0] = cutStrByByteLength(
					str.substring(0, str.indexOf(url))
							+ str.substring(str.indexOf(url) + url.length() + "\n".length(), str.length()),
					byteLength, encoding, encodeNeedByteLength); // Message
		} else {
			strInfo[1] = url;
			strInfo[0] = cutStrByByteLength(str.substring(0, str.indexOf(url)), byteLength, encoding,
					encodeNeedByteLength); // Message
		}

		return strInfo;
	}

	public static String getNormalSMSStr(String str, int byteLength, String encoding, int encodeNeedByteLength)
			throws UnsupportedEncodingException {
		if (str == null)
			return null;

		if (byteLength < 0)
			return null;

		if (str.getBytes(encoding).length <= byteLength)
			return str;

		str = cutStrByByteLength(str, byteLength, encoding, encodeNeedByteLength);

		return str;
	}

	public static String cutStrByByteLength(String str, int byteLength, String encoding, int encodeNeedByteLength)
			throws UnsupportedEncodingException {
		if (byteLength < 0)
			return str;

		byte[] buf = str.getBytes(encoding);

		boolean lastByteIsASCII = false;
		int index = 0;
		boolean isCut = false;

		if (buf.length < byteLength) {
			index = buf.length;
		} else {
			isCut = true;
			for (; index < byteLength;) {
				if (buf[index] < 0) {
					index = index + encodeNeedByteLength;
					lastByteIsASCII = false;
				} else {
					index++;
					lastByteIsASCII = true;
				}
			}
		}

		if (!lastByteIsASCII && index != byteLength && isCut) {
			while (encodeNeedByteLength-- > 0) {
				index--;
			}
		}

		byte[] newBuf = new byte[index];
		System.arraycopy(buf, 0, newBuf, 0, index);
		str = new String(newBuf, encoding);

		return str;
	}

	public static String cutStrByByteIndexLength(String str, String encoding, int idx, int len) throws Exception {
		if (str == null || "".equals(str))
			return "";

		byte[] buf = str.getBytes(encoding);

		byte[] newBuf = new byte[len];
		System.arraycopy(buf, idx, newBuf, 0, len);
		str = new String(newBuf, encoding);

		return str;
	}

	public static String cutStrByByteIndexLength(String str, int idx, int len) throws Exception {
		if (str == null || "".equals(str))
			return "";

		byte[] buf = str.getBytes();

		byte[] newBuf = new byte[len];
		System.arraycopy(buf, idx, newBuf, 0, len);
		str = new String(newBuf);

		return str;
	}

	public static String cutStrByByteIndexLength(byte[] data, int idx, int len) throws Exception {
		byte[] newBuf = new byte[len];
		System.arraycopy(data, idx, newBuf, 0, len);
		return new String(newBuf);
	}

	public static String cutStrByByteIndexLength(byte[] data, String encoding, int idx, int len) throws Exception {
		byte[] newBuf = new byte[len];
		System.arraycopy(data, idx, newBuf, 0, len);
		return new String(newBuf, encoding);
	}

	public static byte[] cutByteByByteIndexLength(byte[] data, int idx, int len) throws Exception {
		byte[] newBuf = new byte[len];
		System.arraycopy(data, idx, newBuf, 0, len);
		return newBuf;
	}

	public static String strCut(String szText, int nLength) {
		String r_val = szText;
		int oF = 0, oL = 0, rF = 0, rL = 0;
		int nLengthPrev = 0;
		try {
			byte[] bytes = r_val.getBytes("UTF-8");

			int j = 0;
			if (nLengthPrev > 0)
				while (j < bytes.length) {
					if ((bytes[j] & 0x80) != 0) {
						oF += 2;
						rF += 3;
						if (oF + 2 > nLengthPrev) {
							break;
						}
						j += 3;
					} else {
						if (oF + 1 > nLengthPrev) {
							break;
						}
						++oF;
						++rF;
						++j;
					}
				}
			j = rF;
			while (j < bytes.length) {
				if ((bytes[j] & 0x80) != 0) {
					if (oL + 2 > nLength) {
						break;
					}
					oL += 2;
					rL += 3;
					j += 3;
				} else {
					if (oL + 1 > nLength) {
						break;
					}
					++oL;
					++rL;
					++j;
				}
			}
			r_val = new String(bytes, rF, rL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return r_val;
	}

	public static boolean getByteLength(String str, int chklength) {
		int strLength = 0;
		boolean lengthCheck = false;
		char tempChar[] = new char[str.length()];

		for (int i = 0; i < tempChar.length; i++) {
			tempChar[i] = str.charAt(i);
			if (tempChar[i] < 128) {
				strLength++;
			} else {
				strLength += 2;
			}

			if (strLength > chklength) {
				lengthCheck = true;
				break;
			}
		}

		return lengthCheck;
	}

	public static String rTrim(String str) {

		if (str == null)
			return "";

		int len = str.length();

		while (str.charAt(len - 1) == ' ') {
			if (len == 1) {
				if ("".equals(str))
					return "";
				else
					return str;
			}
			str = str.substring(0, len - 1);
			len--;
		}

		return str;
	}

	/**
	 * 
	 * Method Name : <br>
	 * Method Description :
	 * @param buffer
	 * @return
	 * @throws Exception
	 */
	public static String BinaryToHexString(byte buffer[]) throws Exception {
		StringBuffer buf = new StringBuffer();
		int nValue;

		try {
			for (int i = 0; i < buffer.length; i++) {
				nValue = (int) buffer[i] & 0x00ff;
				if (nValue < 16)
					buf.append("0");
				buf.append(Integer.toHexString(nValue).toUpperCase());
			}
		} catch (Exception e) {
			throw e;
		} finally {
		}

		return buf.toString();
	}

	public static byte[] HexStringToBinary(String HexString) throws Exception {
		if (HexString == null || "".equals(HexString))
			return null;
		byte ret[] = new byte[HexString.length() / 2];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = (byte) Integer.parseInt(HexString.substring(2 * i, 2 * i + 2), 16);
		}

		return ret;
	}

	public static String byteToHex(byte b) {
		char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		char[] array = { hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] };
		return new String(array);
	}

	public static String getRandomId() {
		Calendar cal = Calendar.getInstance();
		Date currentTime = cal.getTime();
		StringBuffer randomPushId = new StringBuffer();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String formatTime = df.format(currentTime);
		int randomNum_1 = (int) Math.round(1000 * Math.random());
		int randomNum_2 = (int) Math.round(1000 * Math.random());
		int randomNum_3 = (int) Math.round(1000 * Math.random());

		randomPushId.append(formatTime);
		randomPushId.append(lPadding("" + randomNum_1, 5, '0'));
		randomPushId.append(lPadding("" + randomNum_2, 5, '0'));
		randomPushId.append(lPadding("" + randomNum_3, 5, '0'));

		return randomPushId.toString();
	}

	public static String getSystime() {
		Calendar cal = Calendar.getInstance();
		Date currentTime = cal.getTime();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		return df.format(currentTime);
	}

	public static String getSystimeMili() {
		Calendar cal = Calendar.getInstance();
		Date currentTime = cal.getTime();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return df.format(currentTime);
	}
	
	public static String lPadding(String s, int size, char paddingChar) {
		int gap = size - s.length();
		if (gap <= 0) {
			return s;
		}
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < gap; i++) {
			sb.append(paddingChar);
		}

		sb.append(s);
		return sb.toString();
	}

	public static String getStringFromByteArray(byte[] org, int index, int length) {
		byte[] tar = new byte[length];
		System.arraycopy(org, index, tar, 0, length);
		return new String(tar);
	}

	public static String replaceStringDataOfByteArray(byte[] org, int index, String src) {
		byte[] tar = new byte[org.length];

		System.arraycopy(org, 0, tar, 0, org.length);
		System.arraycopy(src.getBytes(), 0, tar, index, src.getBytes().length);

		return new String(tar);
	}

	public static byte[] replaceByteOfByte(byte[] org, int index, byte[] rpl) {
		byte[] tar = new byte[org.length];

		System.arraycopy(org, 0, tar, 0, org.length);
		System.arraycopy(rpl, 0, tar, index, rpl.length);

		return tar;
	}

	public String[] split(String str, String separator) {
		String[] returnValue;
		int index = str.indexOf(separator);
		if (index == -1) {
			returnValue = new String[] { str };
		} else {
			List<String> strList = new ArrayList<String>();
			int oldIndex = 0;
			while (index != -1) {
				String subStr = str.substring(oldIndex, index);
				strList.add(subStr);
				oldIndex = index + separator.length();
				index = str.indexOf(separator, oldIndex);
			}
			if (oldIndex != str.length()) {
				strList.add(str.substring(oldIndex));
			}
			returnValue = strList.toArray(new String[strList.size()]);
		}
		return returnValue;
	}

	public static String getDepoMask(String data) {
		if (data == null || data.length() < 8) {
			return "";
		} else {
			return data.substring(0, 3) + "-" + data.substring(3, 5) + "**"
					+ data.substring(data.length() - 2, data.length());
		}
	}

	public static String getCommaData(String data, int term) {
		String rst = "";

		String minus = "";
		if (data.lastIndexOf("-") > -1) {
			minus = "-";
			data = data.substring(1, data.length());
		}

		String extra = "";
		int extIndex = data.lastIndexOf(".");

		if (extIndex > -1) {
			extra = data.substring(extIndex);
			data = data.substring(0, extIndex);
		}

		int length = data.length();
		int mok = length / term;

		if (mok == 0) {
			return data + extra;
		} else {
			for (int i = 0; i <= mok; i++) {
				int ei = length - i * term;
				int si = length - (i + 1) * term;

				if (ei <= 0)
					break;

				if (si < 0)
					si = 0;

				if (i == 0)
					rst = data.substring(si, ei);
				else
					rst = data.substring(si, ei) + "," + rst;
			}
		}

		return minus + rst + extra;
	}
	
	/**
	 * 주어진 날짜를 스트링으로 반환
	 * @param date
	 * @param oformat
	 * @return
	 */
	public static String getDateStr(Date date, String oformat){
		SimpleDateFormat df = new SimpleDateFormat(oformat);
		return df.format(date);
	}
	
	/**
	 * 현재 날짜를 스트링으로 반환
	 * @param oformat
	 * @return
	 */
	public static String getDateStr(String oformat) {
		Calendar cal = Calendar.getInstance();
		Date currentTime = cal.getTime();
		return getDateStr(currentTime, oformat);
	}
	
	public static String removeZero(String str) {
		if(str == null || "".equals(str) || "0".equals(str))
			return "0";
		
		if(!str.startsWith("0"))
			return str;
		
		int idx = 0;
		for (int i = 0; i < str.length()-1 ; i++){
			if(str.charAt(i) != '0')
				break;
			idx ++;
		}
		
		if(idx > 0)
			return str.substring(idx);
		else
			return str;
		
	}
	
	public static byte[] getSpaceFillBytes(int totMsgLen){
		byte[] result = new byte[totMsgLen];
		
		//메세지 초기화 - 모두 스페이스로 채움
		byte[] bb = " ".getBytes();
		for (int i = 0 ; i < result.length ; i++){
			result[i] = bb[0];
		}
		
		return result;
	}
}
