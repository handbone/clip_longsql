package com.coopnc.clipif.scheduler;

import java.io.File;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.coopnc.clipif.service.ShinhanCmsClientService;

@Component
public class Scheduler {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
	ShinhanCmsClientService shinhanCmsClientService;
     
    @Scheduled(cron = "0 10 2 * * *")
    public void shinhanCMSClient(){
        try {
    		logger.info("[ShinhanCMSClient] Start");
    		
    		//TODO 제거 테스트용
//    		Map<String, String> env = System.getenv();
//    		String chkFilePath = env.get("CLIP_SOCK_IF_HOME");
//    		logger.info("runcheck : "+chkFilePath+"/CHK.txt");
//    		File file = new File(chkFilePath+"/CHK.txt");
//    		if(file.exists() == false){
//    			return;
//    		}
    		
    		shinhanCmsClientService.excuteShinhanCMSClient();
    		logger.info("[ShinhanCMSClient] END : ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    // 10분마다 실행
    // CHK.txt 파일이 있으면 실행(일대사 전송이 안됐을 경우 보내게 하기 위한 로직)
    @Scheduled(cron = "0 */10 * * * *")
    public void shinhan10MinuteCMSClient(){
        try {
    		logger.info("[shinhan10MinuteCMSClient] Start");
    		
    		//TODO 제거 테스트용
    		Map<String, String> env = System.getenv();
    		String chkFilePath = env.get("CLIP_SOCK_IF_HOME");
    		logger.info("runcheck : "+chkFilePath+"/CHK.txt");
    		File file = new File(chkFilePath+"/CHK.txt");
    		if(file.exists() == false){
    			return;
    		}
    		
    		shinhanCmsClientService.excuteShinhanCMSClient();
    		logger.info("[shinhan10MinuteCMSClient] END : ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
