package com.coopnc.clipif.service;

import com.coopnc.clipif.bean.ShinhanCmsRequest;

public interface ShinhanCmsServerService {
	
	/*
	업무개시요구 (0600)	->
	업무개시통보 (0610)	<-
	
	파일정보지시(0630)	->
	파일정보보고(0640)	<-	
	DATA 송신 (0320)	->
	
	결번확인지시 (0620)	->
	결번확인보고 (0300)	<-
	결번 통보 (0310)	->
	
	송신완료지시 (0600)	->
	송신완료보고 (0610)	<-
	
	업무종료지시 (0600)	->
	업무종료보고(0610)	<-
	*/

	public ShinhanCmsRequest proc0600(ShinhanCmsRequest request);
	
	public ShinhanCmsRequest proc0630(ShinhanCmsRequest request);
	
	public ShinhanCmsRequest proc0320(ShinhanCmsRequest request);
	
	public ShinhanCmsRequest proc0620(ShinhanCmsRequest request);
	
	public ShinhanCmsRequest proc0310(ShinhanCmsRequest request);
	
}
