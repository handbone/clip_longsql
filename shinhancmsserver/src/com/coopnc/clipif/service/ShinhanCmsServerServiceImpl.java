package com.coopnc.clipif.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.coopnc.clipif.bean.PointSwapCancelVo;
import com.coopnc.clipif.bean.PointSwapInfo;
import com.coopnc.clipif.bean.ShinhanCmsBlockInfoVo;
import com.coopnc.clipif.bean.ShinhanCmsMgrVo;
import com.coopnc.clipif.bean.ShinhanCmsRequest;
import com.coopnc.clipif.bean.ShinhanCmsRequest.T0300;
import com.coopnc.clipif.bean.ShinhanCmsRequest.T0310;
import com.coopnc.clipif.bean.ShinhanCmsRequest.T0600;
import com.coopnc.clipif.bean.ShinhanCmsRequest.T0620;
import com.coopnc.clipif.bean.ShinhanCmsRequest.T0630;
import com.coopnc.clipif.bean.ShinhanReconcileVo;
import com.coopnc.clipif.config.BaseConstants.ShinhanCmsConsts;
import com.coopnc.clipif.mappers.PointSwapMapper;
import com.coopnc.clipif.util.DateUtil;
import com.coopnc.clipif.util.FileUtil;
import com.coopnc.clipif.util.MessageUtil;
import com.coopnc.clipif.util.SyncMap;

@Service
public class ShinhanCmsServerServiceImpl implements ShinhanCmsServerService {
	Logger logger = LoggerFactory.getLogger(this.getClass());	
	SyncMap<String, ShinhanCmsMgrVo> commCmsMap = null;
	
	private String SENDER = "";
	private String SENDER_PASS = "";

	private String RECV_FILE_PATH;
	
	@Autowired
	PointSwapMapper pointSwapMapper;
	
	@Autowired
	Environment env;
	
	@PostConstruct
	public void init(){
		commCmsMap = new SyncMap<String, ShinhanCmsMgrVo>(3600000);	//유효 시간 1시간 이면 넉넉
		
		RECV_FILE_PATH = env.getProperty("shinhancms.file.path.recv");
		
		File file = new File(RECV_FILE_PATH);
		if(!file.exists())
			file.mkdirs();
		
	}

	/*
	업무개시요구 (0600)	->
	업무개시통보 (0610)	<-
	
	파일정보지시(0630)	->
	파일정보보고(0640)	<-	
	DATA 송신 (0320)	->
	
	결번확인지시 (0620)	->
	결번확인보고 (0300)	<-
	결번 통보 (0310)	->
	
	송신완료지시 (0600)	->
	송신완료보고 (0610)	<-
	
	업무종료지시 (0600)	->
	업무종료보고(0610)	<-
	*/

	@Override
	public ShinhanCmsRequest proc0600(ShinhanCmsRequest request) {
		T0600 reqBody = (T0600)request.getBodyObject();

		ShinhanCmsMgrVo mgr = null;
		String result = ShinhanCmsConsts.RESULT_CODE_SUCCESS;
		
		switch (reqBody.getWork_mgr_info()) {
			case "001" :	
				mgr = new ShinhanCmsMgrVo();
				mgr.setProcDate(MessageUtil.getDateStr("yyyyMMddHHmmss"));
				mgr.setProgress(reqBody.getWork_mgr_info());
				commCmsMap.put(MessageUtil.getDateStr("yyyyMMdd"), mgr);
				break;
			case "002" :
				mgr = commCmsMap.get(MessageUtil.getDateStr("yyyyMMdd"));
				mgr.setProgress(reqBody.getWork_mgr_info());
				break;
			case "003" :
				mgr = commCmsMap.get(MessageUtil.getDateStr("yyyyMMdd"));
				mgr.setProgress(reqBody.getWork_mgr_info());
				break;
			case "004" :
				//처리 완료가 오면 파일 작성하고 종료
				try {
					long fileSize = makeFile();
					if(fileSize > 0) {
						procRecvFile();
					}
				} catch (Exception e) {
					result = ShinhanCmsConsts.ERR_CODE_SYSTEM_ERR;
				}
				
				commCmsMap.remove(MessageUtil.getDateStr("yyyyMMdd"));
				
				break;
			default : ;
		}

		ShinhanCmsRequest response = new ShinhanCmsRequest();
		T0600 resBody = (T0600)response.getBodyObject(ShinhanCmsConsts.COMM_PROGRESS_RES);
		
		resBody.setComm_date(MessageUtil.getDateStr("MMddHHmmss"));
		resBody.setWork_mgr_info(reqBody.getWork_mgr_info());
		resBody.setSender(SENDER);
		resBody.setSender_pass(SENDER_PASS);
		
		response.make(request, ShinhanCmsConsts.COMM_PROGRESS_RES, result, resBody);
		
		return response;
	}

	//파일정보지시
	@Override
	public ShinhanCmsRequest proc0630(ShinhanCmsRequest request) {
		T0630 reqBody = (T0630)request.getBodyObject();
		ShinhanCmsMgrVo mgr = commCmsMap.get(MessageUtil.getDateStr("yyyyMMdd"));
		
		mgr.setFileName(reqBody.getFile_name());
		mgr.setFileSIze(Long.parseLong(reqBody.getFile_size()));
		//이전에 못받은 데이터를 받기 위해서 시분초까지 파일명 생성
		mgr.setFileFullPath(RECV_FILE_PATH+reqBody.getFile_name() + MessageUtil.getDateStr("yyyyMMddHHmmss"));
		//reqBody.getComm_data_size(); 310 전문사이즈는 전문 자체에서만 판단 가능하므로 여기서는 무시함

		ShinhanCmsRequest response = new ShinhanCmsRequest();
		String resComm = ShinhanCmsConsts.COMM_FILE_INFO_RES;
		T0630 resBody = (T0630)response.getBodyObject(resComm);
		
		resBody.setFile_name(reqBody.getFile_name());
		resBody.setFile_size(reqBody.getFile_size());
		resBody.setComm_data_size(reqBody.getComm_data_size());
		
		response.make(request, resComm, ShinhanCmsConsts.RESULT_CODE_SUCCESS, resBody);
		
		return response;
	}

	public ShinhanCmsRequest proc0320(ShinhanCmsRequest request) {
		T0310 reqBody = (T0310)request.getBodyObject();
		ShinhanCmsMgrVo mgr = commCmsMap.get(MessageUtil.getDateStr("yyyyMMdd"));
		
		int blockNo = Integer.parseInt(reqBody.getBlock_no());
		int finalSeq = Integer.parseInt(reqBody.getFinal_seq_no());

		ShinhanCmsBlockInfoVo blockInfo = mgr.getBlockInfo().get(blockNo);
		
		if (blockInfo == null) {
			blockInfo = new ShinhanCmsBlockInfoVo();
		}
		
		blockInfo.setBlockNo(blockNo);
		blockInfo.setSeqs(new byte[finalSeq]);
		mgr.getBlockInfo().put(blockNo, blockInfo);

		int size = Integer.parseInt(reqBody.getReal_data_size());
		mgr.setIncomeSize(mgr.getIncomeSize() + size);
		
		byte[] temp = new byte[size];
		System.arraycopy(reqBody.getReal_data(), 0, temp, 0, size);
		
		blockInfo.addData(finalSeq, temp);
		
		return null;
	}

	//결번확인지시
	@Override
	public ShinhanCmsRequest proc0620(ShinhanCmsRequest request) {
		ShinhanCmsMgrVo mgr = commCmsMap.get(MessageUtil.getDateStr("yyyyMMdd"));
		
		T0620 reqBody = (T0620)request.getBodyObject();

		int blockNo = Integer.parseInt(reqBody.getBlock_no());
		int finalSeq = Integer.parseInt(reqBody.getSeq_no());
		ShinhanCmsBlockInfoVo blockInfo = mgr.getBlockInfo().get(blockNo);
		
		ShinhanCmsRequest response = new ShinhanCmsRequest();
		String resComm = ShinhanCmsConsts.COMM_MISS_NO_RES;
		T0300 resBody = (T0300)response.getBodyObject(resComm);
		
		resBody.setBlock_no(reqBody.getBlock_no());
		resBody.setFinal_seq_no(reqBody.getSeq_no());
		
		int misscnt = 0;
		if(finalSeq == 0) {
			resBody.setMiss_cnt("0");
			resBody.setMiss_confirm("");
		} else {
			byte[] seqs = new byte[finalSeq];
			//메세지 초기화 - 모두 0으로 채움
			byte bb = '0';
			for (int i = 0 ; i < finalSeq ; i++) {
				seqs[i] = bb;
			}
			
			for (int i = 1; i<= finalSeq ; i++) {
				if(blockInfo.chkData(i))
					seqs[i-1] = '1';
				else
					misscnt ++;
			}
			
			resBody.setMiss_cnt(""+misscnt);
			resBody.setMiss_confirm(new String(seqs));
		}

		response.make(request, resComm, ShinhanCmsConsts.RESULT_CODE_SUCCESS, resBody);
		
		return response;
	}

	@Override
	public ShinhanCmsRequest proc0310(ShinhanCmsRequest request) {
		T0310 reqBody = (T0310)request.getBodyObject();
		int blockNo = Integer.parseInt(reqBody.getBlock_no());
		
		ShinhanCmsMgrVo mgr = commCmsMap.get(MessageUtil.getDateStr("yyyyMMdd"));
		ShinhanCmsBlockInfoVo blockInfo = mgr.getBlockInfo().get(blockNo);
		
		blockInfo.setBlockNo(blockNo);
		blockInfo.setSeqs(new byte[Integer.parseInt(reqBody.getFinal_seq_no())]);

		int size = Integer.parseInt(reqBody.getReal_data_size());
		mgr.setIncomeSize(mgr.getIncomeSize() + size);
		
		blockInfo.addData(Integer.parseInt(reqBody.getFinal_seq_no()), reqBody.getReal_data());
		
		return null;
	}
	
	private long makeFile() {
		
		ShinhanCmsMgrVo mgr = commCmsMap.get(MessageUtil.getDateStr("yyyyMMdd"));
		
		File file = new File(mgr.getFileFullPath());
		if(file.exists())
			file.delete();
		
		for (int i=1; i<10000; i++){
			ShinhanCmsBlockInfoVo blockInfo = mgr.getBlockInfo().get(i);
			
			if(blockInfo == null)
				break;
			
			Map<Integer, byte[]> data = blockInfo.getData();
			
			for (int j=1; j<101; j++){
				byte[] unit = data.get(j);
				
				if(unit == null)
					break;
				
				FileUtil.append(mgr.getFileFullPath(), unit);
			}
		}
		
		file = new File(mgr.getFileFullPath());
		
		return file.length();
	}
	
	private void procRecvFile() {
		
		ShinhanCmsMgrVo mgr = commCmsMap.get(MessageUtil.getDateStr("yyyyMMdd"));
		
		File file = new File(mgr.getFileFullPath());
		
		logger.debug("procRecvFile start");

		if (file.canRead()) {
			logger.debug("procRecvFile canRead");
			BufferedInputStream br = null;
			try {
				br = new BufferedInputStream(new FileInputStream(file));
				byte[] s = new byte[500];
				
				//List<ShinhanReconcileVo> list = new ArrayList<ShinhanReconcileVo>();
				Map<String, ShinhanReconcileVo> map = new HashMap<String, ShinhanReconcileVo>();
				
				//개행문자없이 전체가 붙어오므로 500바이트씩 읽어서 사용
				while (br.read(s, 0, s.length) > 0) {
					ShinhanReconcileVo data = new ShinhanReconcileVo(s);
					
					if("DT".equals(data.getSra_c_vl()))
						map.put(data.getSra_ts_uqe_n(), data);
				}
				
				PointSwapInfo param = new PointSwapInfo();
				param.setTrans_req_date(MessageUtil.getDateStr(DateUtil.addDate(-1), "yyyyMMdd"));
				param.setReg_source(ShinhanCmsConsts.REG_SOURCE);
				List<PointSwapInfo> swapHistList = pointSwapMapper.selectSwapList(param);
				int listCount = swapHistList.size();

				logger.debug("shinhan swapHistList size = " + map.size());
				logger.debug("swapHistList size = " + listCount);
				
				for (int i = 0; i<listCount; i++){
					PointSwapInfo pointInfo = swapHistList.get(i);
					
					if(map.get(pointInfo.getTrans_id()) != null){
						try {
							//pre_idx 로 키를 잡고 바로 넣어버린다. 중복이면 안들어가도록
							PointSwapCancelVo cancelInfo = new PointSwapCancelVo();
							cancelInfo.setReg_source(ShinhanCmsConsts.REG_SOURCE);
							cancelInfo.setReg_date(MessageUtil.getDateStr("yyyyMMdd"));
							cancelInfo.setOld_swap_idx(pointInfo.getIdx());
							cancelInfo.setStatus(0);
							pointSwapMapper.insertPointSwapCancelInfo(cancelInfo);
						} catch (Exception e) {
							logger.error(e.getMessage(), e);
						}
					}
				}
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			} finally {
				try {
					if (br != null)
						br.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}

		}
	}
	
}
